from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from array import *

from .models import SalatTaxUser, Business, Business_Partner, SalatTaxRole, Modules, Revenue_Sharing

import re

# wand
from wand.image import Image as Image_wand
from wand.color import Color as Wand_Color

#for url request
import requests
import xml.etree.ElementTree as ET

# Python logging package
import logging
import json
import os, sys
import traceback
import base64
import os.path
from PIL import Image

from .index import *
from .path import *
from .mail import *

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)



@csrf_exempt
def partner_onboard(request):
	log.info('partner_onboard')
	result={}

	try:

		req_data = request.POST.get('data')
		req_data = json.loads(req_data)

		#log.info(req_data)

		bank_proof= request.POST.get('bank_proof')
		bank_proof = json.loads(bank_proof)

		#log.info(bank_proof)

		partner_business=req_data['partner_business']
		primary_business=req_data['primary_business']
		pan=req_data['pan']
		name=req_data['name']
		email=req_data['email']
		mobile=req_data['mobile']
		address=req_data['address']
		pincode=req_data['pincode']
		city=req_data['city']
		state=req_data['state']
		username=req_data['user_name']
		password=req_data['password']
		
		ifsc=req_data['ifsc']
		account_no=req_data['account_no']
		account_type=req_data['account_type']
		bank_holder_name=req_data['bank_holder_name']

		role = 'partner'
		user_exist = 1
		partner_create_flag=0

		if not ( User.objects.filter(username=username,email=email).exists() ):
			#insert data into User model 
			if not User.objects.filter(username=username).exists():
				User.objects.create_user(username, email, password)
				user_exist=0
			else:
				user_exist = 1

			if user_exist==0:
			
				atuser = User.objects.get(username=username)

				ref_code=generate_referral_code('P')
				log.info(ref_code)
				while SalatTaxUser.objects.filter(referral_code=ref_code).exists():
					ref_code=generate_referral_code('P')
					log.info(ref_code)

				if not SalatTaxUser.objects.filter(user=atuser).exists():
					SalatTaxUser.objects.create(
						user = atuser,
						email = email,
						referral_code=ref_code,
					).save()

				#create instance of User model base on username
				salattaxUser = SalatTaxUser.objects.get(user=atuser)

				#create instance of SalatTaxRole model base on rolename
				
				role_instance = SalatTaxRole.objects.get(rolename=role)

				#update remaining data of SalatTaxUser model
				# salattaxUser.ip_address=ip
				salattaxUser.role = role_instance
				salattaxUser.save()

				##Get Main Business
				main_business_instance = Business.objects.get(main_business_name='salattax')

				Business_Partner_Inst=Business_Partner.objects.filter(main_business=main_business_instance)

				partnerId=''

				if not Business_Partner_Inst.exists():
					partnerId='0';
				else:
					Business_Partner_Inst=Business_Partner.objects.filter(main_business=main_business_instance).latest("id")
					last_partner_id=Business_Partner_Inst.partner_id
					last_partner_id=int(last_partner_id)
					partnerId=str(last_partner_id+1)

				##Create Partner
				if not (Business_Partner.objects.filter(user_id=salattaxUser.id).exists()):

					partner_create = Business_Partner(user_id=salattaxUser,
						main_business=main_business_instance,
						partner_business_name=partner_business,
						partner_id=partnerId,
						status='Inactive',
						name=name,
						pan=pan,
						email=email,
						mobile=mobile,
						primary_business=primary_business,
						address=address,
						city=city,
						state=state,
						pincode=pincode,
						ifsc=ifsc,
						account_no=account_no,
						account_type=account_type,
						bank_holder_name=bank_holder_name)

					partner_create.save()

					partner_inst=Business_Partner.objects.filter(id=partner_create.id)

					partner_id = partner_inst[0].id
					result['partner_id']=partner_id

					Module_obj=Modules.objects.get(module_name='Return Filing')
					ModuleId=Module_obj.module_Id

					partner_revenue=Revenue_Sharing(business_partner_id=partner_inst[0],
						module_id=Module_obj,
						partner_share='80')
					partner_revenue.save()

					try:

						###Bank Proof Upload
						if bank_proof!='':
							filename=bank_proof['filename']
							log.info(filename)
							a,ext=filename.split(".")
							filetype=ext.lower()
							log.info(filetype)
							base64file=bank_proof['base64']

							business_folder=static_path+'/Business/'+'B'+str(main_business_instance.id)

							path_exist='N'
							if os.path.exists(business_folder):
								path_exist='Y'
							else:
								os.makedirs(business_folder)
								path_exist='Y'

							if path_exist=='Y':
								bank_proof_file = 'partner'+str(partner_inst[0].id)+'_bank_proof'

								file_decoded = base64.b64decode(base64file)

								infile = business_folder+'/'+bank_proof_file+'.'+filetype

								with open(infile, "wb") as fh:
									fh.write(file_decoded)

								if filetype=='pdf':
									pdf_file=business_folder+'/'+bank_proof_file+'.'+filetype

									#result=convert_pdf(pdf_file,business_folder,bank_proof_file+'.png',resolution=150)

								else:
									if filetype!='png':
										##convert any image to png format
										f, e = os.path.splitext(infile)
										outfile = f + ".png"
										if infile != outfile:
											try:
												Image.open(infile).save(outfile, "png")
												os.unlink(infile)
												log.info("Converted")
											except IOError:
												log.info("cannot convert")

									outfile = business_folder+'/'+bank_proof_file+'.png'
									if os.path.exists(outfile):
										partner_obj=Business_Partner.objects.filter(id=partner_create.id).latest('id')
										partner_obj.bank_proof=bank_proof_file+'.png'
										partner_obj.save()

					except Exception as ex:
						log.error('Error in Partner Bank Proof Upload : '+request.get_host()+': '+traceback.format_exc())

					partner_create_flag=1

					partner_onboard_mail_to_partner(partner_id)
					partner_onboard_mail_to_salat(partner_id)

		else:
			user_exist = 1

		if user_exist==0:
			result['signup_status']= 'success'
		else:
			result['signup_status']= 'failed'

		result['user_exist']= user_exist

		result['partner_create']=partner_create_flag
		result['status']="Success"
		log.info(result)
			
		return HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		result['status']="Error"
		log.error('Error in Partner Onboarding : '+request.get_host()+': '+traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type='application/json')

def convert_pdf(filename,folder,output_filename, resolution=150):
	try:
		# read_file(filename)
		with Image_wand(filename=filename) as img:
			return pdftoimage(filename,folder,output_filename,resolution)
	except Exception as ex:
		log.error('Error in conversion pdf to img: %s' % ex)

def pdftoimage(filename,folder,output_filename,resolution):
	try:
		all_pages = Image_wand(filename=filename, resolution=resolution)

		for i, page in enumerate(all_pages.sequence):
			if i==0:
				with Image_wand(page) as img:   
					img.format = 'png'
					img.background_color = Wand_Color('white')
					img.alpha_channel = 'remove'
					image_filename = os.path.splitext(os.path.basename(filename))[0]
					image_filename = '{}.png'.format(image_filename)
					image_filename = os.path.join(path, image_filename)
					log.info(str(i+1)+' PDF pages converted to image')
					img.save(filename=image_filename)

		return True

	except Exception as ex:
		log.error('Error in conversion pdf to img logic: %s' % ex)
		return False

@csrf_exempt
def check_user_isexist(request):
	log.info('check_user_isexist')
	result={}

	try:

		username = request.POST.get('user_name')
		log.info(username)
		role = 'partner'
		user_exist = 1
		partner_create_flag=0
	 
		if not User.objects.filter(username=username).exists():
			user_exist=0
		else:
			user_exist = 1
			log.info('User name already exist')

		result['user_exist']= user_exist

		result['status']="Success"
			
		return HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		result['status']="Error"
		log.error('Error in check user name isexist : '+request.get_host()+': '+traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def login_user(request):
	if request.method=='POST':
		result={}
		try:
			username=request.POST.get('username')
			password=request.POST.get('password')

			login(request, username)

			result['status']= 'Success'
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')