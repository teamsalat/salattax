from .models import Personal_Details,Address_Details,Client_fin_info1,House_Property_Details
from .models import Other_Income_Common,VI_Deductions,Client_fin_info,Exempt_Income,tds_tcs,tax_paid
from .models import SalatTaxUser,computation,Employer_Details, Bank_Details, State_Code, Income
from .models import Shares_LT,Equity_LT, Shares_ST,Equity_ST,Donation_Details
from .models import Debt_MF_LT,Debt_MF_ST,Listed_Debentures_LT,Listed_Debentures_ST
from .models import Variables_80d,Revenue_Sharing,Partner_Settlement,Partner_Deposit_Balance,SalatTaxUser,Invoice_Details,Module_Subscription,Modules,Business_Partner,RFPaymentDetails,Offer,tax_variables,RFChargesDetails,Property_Owner_Details, Return_Details,Original_return_Details,refunds
import json

import requests
import logging
import re
stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)
from .path import *

from datetime import date, datetime
from dateutil.relativedelta import relativedelta

import traceback

def json_serial(obj):
	if isinstance(obj, (datetime, date)):
		return obj.isoformat()
	raise TypeError ("Type %s not serializable" % type(obj))

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance,FY='2018-2019').exists():
		Return_Details_instance=Return_Details.objects.get(P_id=personal_instance,FY='2018-2019')
		R_ID = Return_Details_instance.R_id
	else:
		log.info('R_ID Entry not exists')
		# for rd in Return_Details.objects.filter(P_id=personal_instance):
		# 	log.info(rd.R_id)

	return R_ID


def get_itr_varibles(client_id, tag_name):
	result={}
	result['client_id'] = client_id
	result['tag_name'] = tag_name
	result['status'] = 'pending'
	R_ID = get_rid(client_id)
	result['R_ID'] = R_ID
	personal_instance = Personal_Details.objects.get(P_Id=client_id)
	# log.info(R_ID)

	if tag_name=='personal_information':
		result['fname'] = ''
		result['fathername'] = ''
		result['mname'] = ''
		result['lname'] = ''
		result['PAN'] = ''
		temp_date = ''
		result['dob'] = ''
		result['mobile'] = ''
		result['aadhar_no'] = ''
		for client in Personal_Details.objects.filter(P_Id = client_id):
			if len(client.name.split('|')) >= 3:
				result['fname'] = client.name.split('|')[0]
				result['mname'] = client.name.split('|')[1]
				result['lname'] = client.name.split('|')[2]
			else:
				result['fname'] = client.name.split('|')[0]
				result['lname'] = client.name.split('|')[1]
			result['PAN'] = client.pan
			result['fathername'] = client.father_name or ''
			result['temp_date'] = json_serial(client.dob)
			# result['dob'] = client.dob.strftime('%d-%m-%Y')
			result['dob'] = client.dob.strftime('%Y-%m-%d')
			result['mobile'] = client.mobile
			result['aadhar_no'] = client.aadhar_no or ''

	if tag_name == 'address_detail':
		ResidenceNo, ResidenceName, RoadOrStreet, LocalityOrArea, city, state, PinCode = ('',)*7
		Employer_category, residential_status = ('',)*2
		Add_statecode = 19
		state_list = State_Code.objects.all()
		for a in Address_Details.objects.filter(p_id=personal_instance):
			ResidenceNo = a.flat_door_block_no
			ResidenceName = a.name_of_premises_building
			RoadOrStreet = a.road_street_postoffice
			LocalityOrArea = a.area_locality
			city = a.town_city
			state = a.state
			state_code_flag = 0
			for s in state_list:
				if re.search(s.state, a.state, re.IGNORECASE):
					state_code_flag = 1
					Add_statecode = s.code
			if state_code_flag == 0:
				Add_statecode = 19
			PinCode = a.pincode

		if Employer_Details.objects.filter(P_id=personal_instance).exists():
			for client in Employer_Details.objects.filter(P_id = personal_instance):
				Employer_category = client.Employer_category
				residential_status = client.residential_status
		result['ResidenceNo'] = ResidenceNo
		result['ResidenceName'] = ResidenceName
		result['RoadOrStreet'] = RoadOrStreet
		result['LocalityOrArea'] = LocalityOrArea
		result['city'] = city
		result['state'] = state
		result['PinCode'] = PinCode
		result['Add_statecode'] = Add_statecode
		result['Employer_category'] = Employer_category
		result['residential_status'] = residential_status

	if tag_name == 'bank_detail':
		bank_name = []
		IFSC = []
		acc_no = []
		acc_type = []
		no_of_bank = Bank_Details.objects.filter(P_id=personal_instance).count()
		if Bank_Details.objects.filter(P_id=personal_instance).exists():
			for bank in Bank_Details.objects.filter(P_id = personal_instance):
				bank_name.append(bank.Default_bank)
				IFSC.append(bank.IFSC_code)
				acc_no.append(bank.Account_no)
				acc_type.append(bank.Account_type)
		result['no_of_bank'] = no_of_bank
		result['bank_name'] = bank_name
		result['IFSC'] = IFSC
		result['acc_no'] = acc_no
		result['acc_type'] = acc_type

	if tag_name == 'hp_detail':
		state_list = State_Code.objects.all()
		address = []
		standard_deduction = []

		HP_type_of_hp = []
		HP_Rent_Received = []
		HP_municipal_tax = []
		HP_add = []
		HP_city = []
		HP_state = []
		HP_pin = []
		HP_Interest = []
		HP_Is_coOwned = []
		HP_co_owner_name = []
		HP_co_owner_pan = []
		HP_coOwned_per_s = []
		HP_per_share = []
		HP_ICU_house_p = []
		IncomeOfHP = []
		ICU_house_p = 0
		ICU_house_p_display = 0

		proportionate_annual_value_arr=[]
		less_standard_deduction_arr=[]
		ICU_house_p_latest=[]
		assessee_share_arr=[]
		try:
			for a in House_Property_Details.objects.filter(R_Id = R_ID):
				add=(a.Name_of_the_Premises_Building_Village or '')+' '+(a.Road_Street_Post_Office or '')
				add = add+' '+(a.Area_Locality or '')+' '+(a.Town_City or '')+', '+(a.State or '')
				address.append(add)
				sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
				standard_deduction.append( sd )

				AOfPO = (float(a.Your_percentage_of_share_in_Property)/100) * (a.Rent_Received - a.Property_Tax)
				IncomeOfHP.append(AOfPO - ((0.3*AOfPO)+a.Interest_on_Home_loan))
				ICU_house_p_display += float(a.Rent_Received or 0)-float(a.Property_Tax or 0)-float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )-float(a.Interest_on_Home_loan or 0)
				ICU_house_p += AOfPO - ((0.3*AOfPO)+a.Interest_on_Home_loan)
				HP_Rent_Received.append(a.Rent_Received)
				HP_municipal_tax.append(a.Property_Tax)
				HP_Interest.append(a.Interest_on_Home_loan)
				HP_type_of_hp.append(a.Type_of_Hosue_Property)
				HP_add.append(a.Name_of_the_Premises_Building_Village)
				HP_city.append(a.Town_City)

				# ##########################
				proportionate_annual_value=AOfPO

				assessee_share_arr.append(a.Your_percentage_of_share_in_Property)

				proportionate_annual_value_arr.append(proportionate_annual_value)

				less_standard_deduction= (float(30)/100)*proportionate_annual_value

				less_standard_deduction_arr.append(less_standard_deduction)

				income_from_HP=proportionate_annual_value-float(a.Interest_on_Home_loan)-less_standard_deduction
				
				ICU_house_p_latest.append(income_from_HP)

				# ##########################
				state_code_flag = 0
				for s in state_list:
					if re.search(s.state, a.State, re.IGNORECASE):
						state_code_flag = 1
						HP_state.append(s.code)
				if state_code_flag == 0:
					HP_state.append(100)
				HP_pin.append(a.PIN_Code)
				if Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=a.Property_Id).exists():
					HP_Is_coOwned.append('yes')
					for pod in Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=a.Property_Id):
						HP_coOwned_per_s.append( pod.Percentage_Share_in_Property )
						HP_co_owner_name.append( pod.Name_of_Co_Owner )
						HP_co_owner_pan.append( pod.PAN_of_Co_owner )
				else:
					HP_Is_coOwned.append('no')
					HP_coOwned_per_s.append( '' )
					HP_co_owner_name.append( '' )
					HP_co_owner_pan.append( '' )
				HP_per_share.append(a.Your_percentage_of_share_in_Property)
				HP_ICU_house_p.append(abs(a.Rent_Received or 0)-float(a.Property_Tax or 0)-float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )-abs(a.Interest_on_Home_loan or 0))
		
		except Exception as ex:
			log.info('Error in get all data HP '+traceback.format_exc())

		result['address'] = address
		result['standard_deduction'] = standard_deduction

		result['HP_type_of_hp'] = HP_type_of_hp
		result['HP_Rent_Received'] = HP_Rent_Received
		result['HP_municipal_tax'] = HP_municipal_tax
		result['HP_add'] = HP_add
		result['HP_city'] = HP_city
		result['HP_state'] = HP_state
		result['HP_pin'] = HP_pin
		result['HP_Interest'] = HP_Interest
		result['HP_Is_coOwned'] = HP_Is_coOwned
		result['HP_co_owner_name'] = HP_co_owner_name
		result['HP_co_owner_pan'] = HP_co_owner_pan
		result['HP_coOwned_per_s'] = HP_coOwned_per_s
		result['HP_per_share'] = HP_per_share
		result['HP_ICU_house_p'] = HP_ICU_house_p
		result['IncomeOfHP'] = IncomeOfHP
		result['ICU_house_p'] = ICU_house_p
		result['ICU_house_p_display'] = ICU_house_p_display

		result['proportionate_annual_value'] = proportionate_annual_value_arr
		result['less_standard_deduction'] = less_standard_deduction_arr
		result['ICU_house_p_latest'] = ICU_house_p_latest
		result['assessee_share'] = assessee_share_arr

	if tag_name == 'other_i_common':
		Interest_Savings, Other_Income, Interest_Income, FD, family_pension,post_office_interest = (0,)*6
		for a in Other_Income_Common.objects.filter(R_Id = R_ID):
			Interest_Savings = float(a.Interest_Savings or 0)
			Other_Income = float(a.Other_Income or 0)
			Interest_Income = float(a.Other_Interest or 0)
			FD = float(a.FD or 0)
			family_pension = float(a.family_pension or 0)
			post_office_interest = float(a.post_office_interest or 0)
		result['Interest_Savings'] = Interest_Savings
		result['Other_Income'] = Other_Income
		result['Interest_Income'] = Interest_Income
		result['FD'] = FD
		result['family_pension'] = family_pension
		result['post_office_interest'] = post_office_interest


	if tag_name == 'exempt_i':
		dividend, agri_income, other_exempt_income, Tax_Free_Interest = (0,)*4
		for e in Exempt_Income.objects.filter(R_Id = R_ID):
			dividend = float(e.Tax_Free_Dividend or 0)
			agri_income = float(e.Agriculture_Income_Id or 0)
			other_exempt_income = e.Other_exempt_income_Id or 0
			Tax_Free_Interest = float(e.Tax_Free_Interest or 0)
		result['dividend'] = dividend
		result['agri_income'] = agri_income
		result['other_exempt_income'] = other_exempt_income
		result['Tax_Free_Interest'] = Tax_Free_Interest

	if tag_name == 'salary':
		state_list = State_Code.objects.all()
		no_of_company, PerquisitesValue, ProfitsInSalary, DeductionUs16, sal, ICU_head_sal  = (0,)*6
		company_name = []
		nature_of_employer=[]
		c_add = []
		state_code = []
		c_pincode = []
		c_city = []
		c_salary = []
		calculated_salary_arr = []
		c_professionTax = []
		c_entertainment = []
		c_HRA = []
		c_LTA = []
		c_otherAllowance = []
		c_PerquisitesValue = []
		c_profit_lieu = []
		c_salary_as_per_provision=[]
		for ED in Employer_Details.objects.filter(P_id=personal_instance):
			no_of_company += 1
			company_name.append(ED.Name_of_employer)
			nature_of_employer.append(ED.Employer_category)
			c_city.append(ED.Employer_city)
			c_add.append(ED.Address_of_employer)
			state_code_flag = 0
			for s in state_list:
				if re.search(s.state, ED.Address_of_employer, re.IGNORECASE):
					state_code_flag = 1
					state_code.append(s.code)
			if state_code_flag == 0:
				state_code.append(19)
			c_pincode.append(ED.Employer_pin)
			# for c in Client_fin_info1.objects.filter(client_id = client_id,company_name=ED.Name_of_employer).exclude(flag=2):
				# c_salary.append(float(c.salary_as_per_provision or 0))
			calculated_salary = 0
			for I in Income.objects.filter(R_Id=R_ID,company_name=ED.Name_of_employer):
				# c_salary.append(float(I.Salary or 0) )-abs(float(i.Form16_HRA or 0)-float(i.LTA or 0) - float(i.Other_allowances or 0) - float(i.Profession_Tax or 0) - float(i.Entertainment_Allowance or 0) )
				c_salary.append(float(I.Salary or 0)-abs(float(I.Form16_HRA or 0)-float(I.LTA or 0) - float(I.Other_allowances or 0) ) )
				calculated_salary = float(I.Salary or 0)+abs(float(I.Value_of_perquisites or 0)+float(I.Profits_in_lieu_of_salary or 0))-abs(float(I.Other_allowances or 0) )
				calculated_salary_arr.append(calculated_salary)
				c_salary_as_per_provision.append(float(I.Salary or 0))
				c_PerquisitesValue.append( float(I.Value_of_perquisites or 0) )
				c_profit_lieu.append( float(I.Profits_in_lieu_of_salary or 0) )
				c_professionTax.append( float(I.Profession_Tax or 0) )
				c_entertainment.append( float(I.Entertainment_Allowance or 0) )
				c_HRA.append( float(I.Form16_HRA or 0) )
				c_LTA.append( float(I.LTA or 0) )
				c_otherAllowance.append( float(I.Other_allowances or 0) )

				PerquisitesValue += float(I.Value_of_perquisites or 0)
				ProfitsInSalary += float(I.Profits_in_lieu_of_salary or 0)
				DeductionUs16 += float(I.Profession_Tax or 0)
				# sal = float(I.Salary or 0)
				sal = calculated_salary
				ICU_head_sal += calculated_salary - float(I.Profession_Tax or 0) - float(I.Entertainment_Allowance or 0)

		result['company_name'] = company_name
		result['c_add'] = c_add
		result['NatureOfEmployment'] = nature_of_employer
		result['state_code'] = state_code
		result['c_pincode'] = c_pincode
		result['c_city'] = c_city
		result['c_salary'] = c_salary
		result['calculated_salary_arr'] = calculated_salary_arr
		result['c_professionTax'] = c_professionTax
		result['c_entertainment'] = c_entertainment
		result['c_HRA'] = c_HRA
		result['c_LTA'] = c_LTA
		result['c_otherAllowance'] = c_otherAllowance
		result['c_salary_as_per_provision']=c_salary_as_per_provision
		result['c_PerquisitesValue'] = c_PerquisitesValue
		result['c_profit_lieu'] = c_profit_lieu
		result['no_of_company'] = no_of_company
		result['PerquisitesValue'] = PerquisitesValue
		result['ProfitsInSalary'] = ProfitsInSalary
		result['DeductionUs16'] = DeductionUs16
		result['sal'] = sal
		result['ICU_head_sal'] = ICU_head_sal

	if tag_name == 'deduction':
		d_80c ,d_80ccc ,d_80ccd ,d_80g ,d_80dd ,d_80u ,d_80d ,d_80e ,d_80tta ,d_80ddb = (0,)*10
		d_80ee, d_80gg ,d_80gga ,d_80ggc ,d_80rrb ,d_80qqb ,d_80ccg, d_80ccd1, d_80ccd2 = (0,)*9
		d_80ttb = (0,)*1
		for a in VI_Deductions.objects.filter(R_Id = R_ID):
			d_80c = float(a.VI_80_C or 0)
			d_80ccc = float(a.VI_80_CCC or 0)
			d_80ccd = float(a.VI_80_CCD_1B or 0)
			d_80ccd1 = float(a.VI_80_CCD_1 or 0)
			d_80ccd2 = float(a.VI_80_CCD_1 or 0)
			d_80g = float(a.VI_80_G or 0)
			d_80dd = float(a.VI_80_DD or 0)
			d_80u = float(a.VI_80_U or 0)
			d_80d = float(a.VI_80_D or 0)
			d_80e = float(a.VI_80_E or 0)
			d_80tta = float(a.VI_80_TTA or 0)
			d_80ttb = float(a.VI_80_TTB or 0)
			d_80ddb = float(a.VI_80_DDB or 0)
			d_80ee = float(a.VI_80_EE or 0)
			d_80gg = float(a.VI_80_GG or 0)
			d_80gga = float(a.VI_80_GGA or 0)
			d_80ggc = float(a.VI_80_GGC or 0)
			d_80rrb = float(a.VI_80_RRB or 0)
			d_80qqb = float(a.VI_80_QQB or 0)
			d_80ccg = float(a.VI_80_CCG or 0)
		result['d_80c'] = d_80c
		result['d_80ccc'] = d_80ccc
		result['d_80ccd'] = d_80ccd
		result['d_80ccd1'] = d_80ccd1
		result['d_80ccd2'] = d_80ccd2
		result['d_80g'] = d_80g
		result['d_80dd'] = d_80dd
		result['d_80u'] = d_80u
		result['d_80d'] = d_80d
		result['d_80e'] = d_80e
		result['d_80tta'] = d_80tta
		result['d_80ttb'] = d_80ttb
		result['d_80ddb'] = d_80ddb
		result['d_80ee'] = d_80ee
		result['d_80gg'] = d_80gg
		result['d_80gga'] = d_80gga
		result['d_80ggc'] = d_80ggc
		result['d_80rrb'] = d_80rrb
		result['d_80qqb'] = d_80qqb
		result['d_80ccg'] = d_80ccg

	if tag_name == 'computation':
		ttp, rebate, educess, gtl, salary, GTI, total_deduction, Tx, Tx1, surcharge,taxable_income  = (0,)*11
		interest_234A,interest_234B,interest_234C,interest_234F,total_interest=(0,)*5
		for a in computation.objects.filter(R_Id = R_ID):
			ttp = float(a.taxable_income or 0)
			rebate = float(a.rebate or 0)
			educess = float(a.add_education_cess or 0)
			gtl = float(a.gross_total_income or 0)
			salary = float(a.salary_income or 0)
			GTI = float(a.gross_total_income or 0)
			total_deduction = float(a.total_deduction or 0)
			taxable_income = float(a.taxable_income or 0)
			Tx = float(a.IT_normal_rates or 0)
			Tx1 = float(a.IT_special_rates or 0)
			surcharge = float(a.surcharge or 0)
			interest_234A = float(a.interest_234A or 0)
			interest_234B = float(a.interest_234B or 0)
			interest_234C = float(a.interest_234C or 0)
			interest_234F = float(a.interest_234F or 0)
			total_interest = float(a.interest_on_tax or 0)

		result['ttp'] = ttp
		result['rebate'] = rebate
		result['educess'] = educess
		result['gtl'] = gtl
		result['salary'] = salary
		result['GTI'] = GTI
		result['total_deduction'] = total_deduction
		result['taxable_income'] = taxable_income
		result['Tx'] = Tx
		result['Tx1'] = Tx1
		result['interest_234A'] = interest_234A
		result['interest_234B'] = interest_234B
		result['surcharge'] = surcharge
		result['interest_234C'] = interest_234C
		result['interest_234F'] = interest_234F
		result['total_interest'] = total_interest

	if tag_name == 'tax_paid':
		adv_t, sa_t, tax_paid_count, taxes_paid_total_tax = (0,)*4
		tp_bsr_code = []
		tp_deposit_date = []
		tp_challan_no = []
		tp_total_tax = []

		return_year = 0
		if Return_Details.objects.filter(R_id=R_ID).exists():
			Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
			return_year = Return_Details_instance.FY
			return_year = return_year.split('-')[1]

		date = datetime(int(return_year) ,03,31)
		date = date.replace(tzinfo=None)
		if tax_paid.objects.filter(R_Id=R_ID).exists():
			for tax in tax_paid.objects.filter(R_Id = R_ID):
				if date >tax.deposit_date.replace(tzinfo=None) :
					adv_t += tax.total_tax or 0
				else:
					sa_t += tax.total_tax or 0
				tax_paid_count += 1
				tp_bsr_code.append(tax.BSR_code or 0)
				tp_deposit_date.append(tax.deposit_date.strftime("%Y-%m-%d") or 0)
				tp_challan_no.append(tax.challan_serial_no or 0)
				tp_total_tax.append(tax.total_tax or 0)
				taxes_paid_total_tax += tax.total_tax or 0

		result['date'] = date
		result['adv_t'] = adv_t
		result['sa_t'] = sa_t
		result['tax_paid_count'] = tax_paid_count
		result['tp_bsr_code'] = tp_bsr_code
		result['tp_deposit_date'] = tp_deposit_date
		result['tp_challan_no'] = tp_challan_no
		result['tp_total_tax'] = tp_total_tax
		result['taxes_paid_total_tax'] = taxes_paid_total_tax

	if tag_name == 'tds_tcs':
		return_year = 0
		if Return_Details.objects.filter(R_id=R_ID).exists():
			Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
			return_year = Return_Details_instance.FY
			return_year = return_year.split('-')[1]

		tds_count, total_tds1, tcs_count, total_tcs, tds2_count, total_tds2, tcs_value, tds_value = (0,)*8
		tds3_count, total_tds3 = (0,)*2
		tds_dc_name = []
		tds_amt_paid = []
		tds_deducted = []
		tds_TAN_PAN = []
		tcs_dc_name = []
		tcs_amt_paid = []
		tcs_deducted = []
		tcs_TAN_PAN = []
		tds2_dc_name = []
		tds2_amt_paid = []
		tds2_tds_deducted = []
		tds2_TAN_PAN = []
		tds2_section = []
		tds3_dc_name = []
		tds3_amt_paid = []
		tds3_tds_deducted = []
		tds3_TAN_PAN = []
		tds3_section = []
		PAN = personal_instance.pan

		if tds_tcs.objects.filter(R_Id=R_ID,year=return_year).exists():
			for tds in tds_tcs.objects.filter(R_Id = R_ID,year=return_year):
				if tds.part == 'B':
					tcs_value += tds.tds_tcs_deposited
					tcs_count += 1
					tcs_TAN_PAN.append(tds.TAN_PAN)
					tcs_dc_name.append(tds.Deductor_Collector_Name)
					tcs_amt_paid.append(tds.amount_paid)
					tcs_deducted.append(tds.tax_deducted)
					total_tcs += tds.tax_deducted
				else:
					tds_value += tds.tds_tcs_deposited
				if( "IB" in tds.section):
					tds3_count += 1
					tds3_section.append(tds.section)
					total_tds3 += tds.tax_deducted
					tds3_TAN_PAN.append(tds.TAN_PAN)
					tds3_dc_name.append(tds.Deductor_Collector_Name)
					tds3_amt_paid.append(tds.amount_paid)
					tds3_tds_deducted.append(tds.tax_deducted)

			for tds in tds_tcs.objects.filter(R_Id = R_ID,year=return_year,section='192'):
				tds_count += 1
				total_tds1 += tds.tax_deducted
				tds_TAN_PAN.append(tds.TAN_PAN)
				tds_dc_name.append(tds.Deductor_Collector_Name)
				tds_amt_paid.append(tds.amount_paid)
				tds_deducted.append(tds.tax_deducted)
			for tds2 in tds_tcs.objects.filter(R_Id = R_ID,year=return_year).exclude(section='192'):
				tds2_count += 1
				total_tds2 += tds2.tax_deducted
				tds2_section.append(tds.section)
				tds2_TAN_PAN.append(tds2.TAN_PAN)
				tds2_dc_name.append(tds2.Deductor_Collector_Name)
				tds2_amt_paid.append(tds2.amount_paid)
				tds2_tds_deducted.append(tds2.tax_deducted)

		result['tds_count'] = tds_count
		result['total_tds1'] = total_tds1
		result['tcs_count'] = tcs_count
		result['total_tcs'] = total_tcs
		result['tds2_count'] = tds2_count
		result['total_tds2'] = total_tds2
		result['tds3_count'] = tds3_count
		result['total_tds3'] = total_tds3
		result['tcs_value'] = tcs_value
		result['tds_value'] = tds_value
		result['tds_dc_name'] = tds_dc_name
		result['tds_amt_paid'] = tds_amt_paid
		result['tds_deducted'] = tds_deducted
		result['tds_TAN_PAN'] = tds_TAN_PAN
		result['tcs_dc_name'] = tcs_dc_name
		result['tcs_amt_paid'] = tcs_amt_paid
		result['tcs_deducted'] = tcs_deducted
		result['tcs_TAN_PAN'] = tcs_TAN_PAN
		result['tds2_dc_name'] = tds2_dc_name
		result['tds2_amt_paid'] = tds2_amt_paid
		result['tds2_tds_deducted'] = tds2_tds_deducted
		result['tds2_TAN_PAN'] = tds2_TAN_PAN
		result['tds2_section'] = tds2_section
		result['tds3_dc_name'] = tds3_dc_name
		result['tds3_amt_paid'] = tds3_amt_paid
		result['tds3_tds_deducted'] = tds3_tds_deducted
		result['tds3_TAN_PAN'] = tds3_TAN_PAN
		result['tds3_section'] = tds3_section

	if tag_name == 'capital_gain':
		st_shares_tpv, st_shares_tsv, st_shares_gain= (0,)*3
		lt_shares_tpv, lt_shares_tsv, lt_shares_gain,lt_shares_FMV= (0,)*4
		st_equity_tpv, st_equity_tsv, st_equity_gain = (0,)*3
		lt_equity_tpv, lt_equity_tsv, lt_equity_gain,lt_equity_FMV = (0,)*4
		st_debt_MF_tpv, st_debt_MF_tsv, st_debt_MF_gain = (0,)*3
		lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
		st_listed_d_tpv, st_listed_d_tsv, st_listed_d_gain = (0,)*3
		lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3

		if Shares_ST.objects.filter(R_Id=R_ID).exists():
			Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
			st_shares_tpv= Shares_ST_instance.Cost_of_Acquisition
			st_shares_tsv= Shares_ST_instance.Total_Sell_Value
			st_shares_gain= Shares_ST_instance.Expenditure

		if Shares_LT.objects.filter(R_Id=R_ID).exists():
			Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
			lt_shares_tpv= Shares_LT_instance.Cost_of_Acquisition
			lt_shares_tsv= Shares_LT_instance.Total_Sell_Value
			lt_shares_gain= Shares_LT_instance.Expenditure
			lt_shares_FMV=Shares_LT_instance.totalFMV

		if Equity_ST.objects.filter(R_Id=R_ID).exists():
			Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
			st_equity_tpv= Equity_ST_instance.Cost_of_Acquisition
			st_equity_tsv= Equity_ST_instance.Total_Sell_Value
			st_equity_gain= Equity_ST_instance.Expenditure

		if Equity_LT.objects.filter(R_Id=R_ID).exists():
			Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
			lt_equity_tpv= Equity_LT_instance.Cost_of_Acquisition
			lt_equity_tsv= Equity_LT_instance.Total_Sell_Value
			lt_equity_gain= Equity_LT_instance.Expenditure
			lt_equity_FMV= Equity_LT_instance.totalFMV

		if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
			Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
			st_debt_MF_tpv= Debt_MF_ST_instance.Cost_of_Acquisition
			st_debt_MF_tsv= Debt_MF_ST_instance.Total_Sell_Value
			st_debt_MF_gain= Debt_MF_ST_instance.Expenditure

		if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
			Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
			lt_debt_MF_tpv= Debt_MF_LT_instance.Cost_of_Acquisition
			lt_debt_MF_tsv= Debt_MF_LT_instance.Total_Sell_Value
			lt_debt_MF_gain= Debt_MF_LT_instance.Expenditure

		if Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
			Listed_Debentures_ST_instance = Listed_Debentures_ST.objects.get(R_Id=R_ID)
			st_listed_d_tpv= Listed_Debentures_ST_instance.Cost_of_Acquisition
			st_listed_d_tsv= Listed_Debentures_ST_instance.Total_Sell_Value
			st_listed_d_gain= Listed_Debentures_ST_instance.Expenditure

		if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
			Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
			lt_listed_d_tpv= Listed_Debentures_LT_instance.Cost_of_Acquisition
			lt_listed_d_tsv= Listed_Debentures_LT_instance.Total_Sell_Value
			lt_listed_d_gain= Listed_Debentures_LT_instance.Expenditure
		result['st_shares_tpv'] = st_shares_tpv
		result['st_shares_tsv'] = st_shares_tsv
		result['st_shares_gain'] = st_shares_gain
		result['lt_shares_tpv'] = lt_shares_tpv
		result['lt_shares_tsv'] = lt_shares_tsv
		result['lt_shares_gain'] = lt_shares_gain
		result['lt_shares_FMV'] = lt_shares_FMV
		result['st_equity_tpv'] = st_equity_tpv
		result['st_equity_tsv'] = st_equity_tsv
		result['st_equity_gain'] = st_equity_gain
		result['lt_equity_tpv'] = lt_equity_tpv
		result['lt_equity_tsv'] = lt_equity_tsv
		result['lt_equity_gain'] = lt_equity_gain
		result['lt_equity_FMV'] = lt_equity_FMV
		result['st_debt_MF_tpv'] = st_debt_MF_tpv
		result['st_debt_MF_tsv'] = st_debt_MF_tsv
		result['st_debt_MF_gain'] = st_debt_MF_gain
		result['lt_debt_MF_tpv'] = lt_debt_MF_tpv
		result['lt_debt_MF_tsv'] = lt_debt_MF_tsv
		result['lt_debt_MF_gain'] = lt_debt_MF_gain
		result['st_listed_d_tpv'] = st_listed_d_tpv
		result['st_listed_d_tsv'] = st_listed_d_tsv
		result['st_listed_d_gain'] = st_listed_d_gain
		result['lt_listed_d_tpv'] = lt_listed_d_tpv
		result['lt_listed_d_tsv'] = lt_listed_d_tsv
		result['lt_listed_d_gain'] = lt_listed_d_gain

	if tag_name == 'return_detail':
		ReturnFileSec1, ReturnType1, ReceiptNo1, OrigRetFiledDate1 = ('',)*4
		FY, AY = ('',)*2
		if Return_Details.objects.filter(R_id=R_ID).exists():
			Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
			FY = Return_Details_instance.FY
			AY = Return_Details_instance.AY

		# log.info(FY)
		# log.info(client_id)
		if Original_return_Details.objects.filter(P_id=personal_instance,FY=FY).exists():
			or_details_instance=Original_return_Details.objects.get(P_id=personal_instance,FY=FY)
			ReturnFileSec1 = '17'
			ReturnType1 = 'R'
			ReceiptNo1 = or_details_instance.Receipt_no_original_return
			OrigRetFiledDate1 = or_details_instance.Date_of_original_return_filing.strftime('%Y-%m-%d')
		else:
			ReturnFileSec1 = '11'
			ReturnType1 = 'O'
		# log.info(ReturnFileSec1)
		result['FY'] = FY
		result['AY'] = AY
		result['ReturnFileSec1'] = ReturnFileSec1
		result['ReturnType1'] = ReturnType1
		result['ReceiptNo1'] = ReceiptNo1
		result['OrigRetFiledDate1'] = OrigRetFiledDate1

	if tag_name == 'income_detail':
		calculated_salary, sal, PerquisitesValue, ProfitsInSalary, DeductionUs16, ICU_head_sal = (0,)*6
		entertainment_a, profession_t, PerquisitesValue, ProfitsInSalary, DeductionUs16 = (0,)*5
		form16_hra, lta, other_allowances = (0,)*3
		if Income.objects.filter(R_Id=R_ID).exists():
			for i in Income.objects.filter(R_Id=R_ID):
				calculated_salary = float(i.Salary or 0)+abs(float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0))-abs(float(i.Other_allowances or 0) )
				PerquisitesValue += float(i.Value_of_perquisites or 0)
				ProfitsInSalary += float(i.Profits_in_lieu_of_salary or 0)
				DeductionUs16 += float(i.Profession_Tax or 0)+float(i.Entertainment_Allowance or 0)
				entertainment_a += float(i.Entertainment_Allowance or 0)
				profession_t += float(i.Profession_Tax or 0)
				form16_hra += float(i.Form16_HRA or 0)
				lta += float(i.LTA or 0)
				other_allowances += float(i.Other_allowances or 0)
				sal += float(i.Salary or 0)
				ICU_head_sal += calculated_salary

		result['calculated_salary'] = calculated_salary
		result['sal'] = sal
		result['form16_hra'] = form16_hra
		result['lta'] = lta
		result['other_allowances'] = other_allowances
		result['PerquisitesValue'] = PerquisitesValue
		result['ProfitsInSalary'] = ProfitsInSalary
		result['DeductionUs16'] = DeductionUs16
		result['entertainment_a'] = entertainment_a
		result['profession_t'] = profession_t
		result['ICU_head_sal'] = ICU_head_sal
				
	if tag_name == 'donation':
		d_name = []
		d_address = []
		d_city = []
		d_state_code = []
		d_pin_code = []
		d_pan = []
		d_amt = []
		d_percent = []
		total_donation, donation_count = (0,)*2
		if Donation_Details.objects.filter(R_Id=R_ID).exists():
			for d in Donation_Details.objects.filter(R_Id = R_ID):
				state_code=''
				donation_count += 1
				d_name.append( d.donee_name or '')
				d_address.append( d.address or '')
				d_city.append( d.city_town or '')

				if d.state_code!='':
					if State_Code.objects.filter(state=d.state_code).exists():
						state_code=State_Code.objects.get(state=d.state_code).code
						if len(state_code)==1:
							state_code='0'+state_code

				d_state_code.append(state_code)
				d_pin_code.append( d.pin_code or '')
				d_pan.append( d.donee_pan or '')
				d_amt.append( float(d.amt_of_donation or 0) )
				d_percent.append( float(d.donation_percent or 0) )
				total_donation += float(d.amt_of_donation or 0)

		result['d_name'] = d_name
		result['d_address'] = d_address
		result['d_city'] = d_city
		result['d_state_code'] = d_state_code
		result['d_pin_code'] = d_pin_code
		result['d_pan'] = d_pan
		result['d_amt'] = d_amt
		result['d_percent'] = d_percent
		result['donation_count'] = donation_count
		result['total_donation'] = total_donation

	result['status'] = 'finish'
	# log.info(result)
	return result


def determine_ITR(R_ID):
	ITR=''
	log.info('=====determine_ITR starts =====')
	try:
			no_HP, dividend, other_exempt_income, taxable_income, capital_gain = (0,)*5
			no_HP = House_Property_Details.objects.filter(R_Id=R_ID).count()

			if Exempt_Income.objects.filter(R_Id = R_ID).exists():
				for client in Exempt_Income.objects.filter(R_Id = R_ID):
					dividend = client.Tax_Free_Dividend or 0
					other_exempt_income = client.Other_exempt_income_Id or 0

			if computation.objects.filter(R_Id = R_ID).exists():
				for client in computation.objects.filter(R_Id = R_ID):
					taxable_income = client.taxable_income or 0

			if Shares_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Shares_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Equity_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Equity_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			# House Property < 2 OR (Dividend Income + Any other Exempt Income) < 5000 
			# OR Taxable Income < Rs. 50,00,000/-OR No Capital Gain then ITR - 1 else ITR 2
			condition_itr2 = 0

			if no_HP < 2:
				ITR = 1
			else:
				condition_itr2 = 1
			if (dividend + other_exempt_income) < 5000:
				ITR = 1
			else:
				condition_itr2 = 1
			if taxable_income < 5000000:
				ITR = 1
			else:
				condition_itr2 = 1
			if capital_gain == 0:
				ITR = 1
			else:
				condition_itr2 = 1
			if condition_itr2 == 1:
				ITR = 2

	except Exception as ex:
		log.info(ex)
	log.info(ITR)
	log.info('=====determine_ITR ends =====')
	return ITR


def calculate_age(client_id):
	age=''
	try:
		for client in Personal_Details.objects.filter(P_Id = client_id):
			if client.dob is None:
				dob = ''
			else:
				dob = client.dob.strftime('%d-%m-%Y')

			if dob == '':
				age = 50
			else:
				age1 = dob.split('-')[2]
				age = 2019 - int(age1)

		return age
	except Exception as ex:
		log.info('Error in caculating age '+ex)

def create_xml_health_premium_block(client_id):
	c=''
	try:
		log.info('================xml health premium block starts===========')
		R_ID=get_rid(client_id)
		self_citizen_type=''

		if calculate_age(client_id)<60:
			self_citizen_type='OC'
		elif calculate_age(client_id)>60:
			self_citizen_type='SC'

		parent_citizen_type=get_parent_age(client_id)

		health_premium_amount=0
		health_premium_code=0

		health_checkup_amount=0
		health_checkup_code=0

		medical_expenditure_amount=0
		medical_expenditure_amount=0

		data1={}
		if Variables_80d.objects.filter(R_Id=R_ID).exists():
			data=Variables_80d.objects.get(R_Id=R_ID)
			self_health_checkup_amount=data.self_health_checkup
			parent_health_checkup_amount=data.parent_health_checkup
			health_checkup_amount=self_health_checkup_amount+parent_health_checkup_amount
			
			self_insurance_premium_amount=data.self_insurance_premium
			parent_insurance_premium_amount=data.parent_insurance_premium
			health_premium_amount=self_insurance_premium_amount+parent_insurance_premium_amount
				
			self_medical_expenditure_amount=data.self_medical_expenditure
			parent_medical_expenditure_amount=data.parent_medical_expenditure
			medical_expenditure_amount=self_medical_expenditure_amount+parent_medical_expenditure_amount

			# log.info(self_health_checkup_amount)
			data1['self_health_checkup_amount']=self_health_checkup_amount
			data1['parent_health_checkup_amount']=parent_health_checkup_amount
			data1['self_insurance_premium_amount']=self_insurance_premium_amount
			data1['parent_insurance_premium_amount']=parent_insurance_premium_amount
			data1['self_medical_expenditure_amount']=self_medical_expenditure_amount
			data1['parent_medical_expenditure_amount']=parent_medical_expenditure_amount

		c += '<ITRForm:Section80DHealthInsPremium>\n'

		# health premium schema
		if health_premium_amount!=0:
			health_premium_code=get_health_premium_code(data1,self_citizen_type,parent_citizen_type)
			log.info(health_premium_code)
			c += '<ITRForm:HealthInsurancePremium>'+health_premium_code+'</ITRForm:HealthInsurancePremium>\n'
		
		c += '<ITRForm:Sec80DHealthInsurancePremiumUsr>'+str(health_premium_amount)+'</ITRForm:Sec80DHealthInsurancePremiumUsr>\n'
		
		# medical expenditure schema
		if medical_expenditure_amount!=0:
			medical_expenditure_code=get_medical_expenditure_code(data1,self_citizen_type,parent_citizen_type)
			log.info(medical_expenditure_code)
			c += '<ITRForm:MedicalExpenditure>'+medical_expenditure_code+'</ITRForm:MedicalExpenditure>\n'

		c += '<ITRForm:Sec80DMedicalExpenditureUsr>'+str(medical_expenditure_amount)+'</ITRForm:Sec80DMedicalExpenditureUsr>\n'
		
		# preventive health checkup schema
		if health_checkup_amount!=0:
			health_checkup_code=get_health_checkup_code(data1,self_citizen_type,parent_citizen_type)
			log.info(health_checkup_code)
			c += '<ITRForm:PreventiveHealthCheckUp>'+health_checkup_code+'</ITRForm:PreventiveHealthCheckUp>\n'
		
		c += '<ITRForm:Sec80DPreventiveHealthCheckUpUsr>'+str(health_checkup_amount)+'</ITRForm:Sec80DPreventiveHealthCheckUpUsr>\n'

		c += '</ITRForm:Section80DHealthInsPremium>\n'

		log.info(c)
		log.info('================xml health premium block end===========')
		return c

	except Exception as ex:
		log.info('Error in xml health premium block '+traceback.format_exc())
		return c


def get_parent_age(client_id):
	parent_citizen_type='NA'
	try:
		if Personal_Details.objects.filter(P_Id=client_id).exists():
			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			parent_citizen_type=personal_instance.dependent_parent_age

		if parent_citizen_type==None:
			parent_citizen_type='NA'

	except Exception as ex:
		log.info('Error in getting parent citizen type '+ex)

	return parent_citizen_type



# health insurance premium
# 1-self and family
# 2-self(senior citizen) & family
# 3-parents
# 4-parents(senior citizen)
# 5-self and family including parents
# 6-self and family includinn senior citizen parents
# 7-self(senier citizen) & family including senier citizen parents
senier_citizen=['SC','SSC']
def get_health_premium_code(data,self_citizen_type,parent_citizen_type):
	health_premium_code=''

	log.info(data)
	log.info('self_citizen_type '+self_citizen_type)
	log.info('parent_citizen_type '+parent_citizen_type)

	self_health_checkup_amount=data['self_insurance_premium_amount']
	parent_health_checkup_amount=data['parent_insurance_premium_amount']

	log.info('self_insurance_premium_amount '+str(self_health_checkup_amount))
	log.info('parent_insurance_premium_amount '+str(parent_health_checkup_amount))

	try:
		if data:
			if self_health_checkup_amount!=0 and parent_health_checkup_amount!=0 and self_citizen_type in senier_citizen and  parent_citizen_type in senier_citizen:
				log.info('7-self(senier citizen) & family including senier citizen parents')
				health_premium_code='7'

			elif (self_health_checkup_amount!=0 and self_citizen_type=='OC') and (parent_citizen_type in senier_citizen and parent_health_checkup_amount!=0):
				log.info('6-self and family including senior citizen parents')
				health_premium_code='6'

			elif (self_health_checkup_amount!=0 and self_citizen_type=='OC') and (parent_citizen_type=='OC' and parent_health_checkup_amount!=0):
				log.info('5-self and family including parents')
				health_premium_code='5'

			elif (self_health_checkup_amount==0) and (parent_citizen_type in senier_citizen and parent_health_checkup_amount!=0):
				log.info('4-parents(senior citizen)')
				health_premium_code='4'

			elif (self_health_checkup_amount==0) and (parent_citizen_type=='OC' and parent_health_checkup_amount!=0):
				log.info('3-parents')
				health_premium_code='3'

			elif (self_health_checkup_amount!=0 and self_citizen_type in senier_citizen) and (parent_health_checkup_amount==0):
				log.info('2-self(senior citizen) & family')
				health_premium_code='2'

			elif (self_health_checkup_amount!=0 and self_citizen_type=='OC') and (parent_health_checkup_amount==0):
				log.info('self and family')
				health_premium_code='1'

	except Exception as ex:
		log.info('Error in getting health premium code '+traceback.format_exc())
	return health_premium_code


# medical expenditure
# 1-self and family(senior citizen)
# 2-parents(senior citizen)
# 3.self and family including parents(senior citizen)
def get_medical_expenditure_code(data,self_citizen_type,parent_citizen_type):
	medical_expenditure_code=''
	try:
		log.info('self_citizen_type '+self_citizen_type)
		log.info('parent_citizen_type '+parent_citizen_type)

		self_medical_expenditure_amount=data['self_medical_expenditure_amount']
		parent_medical_expenditure_amount=data['parent_medical_expenditure_amount']

		log.info('self_medical_expenditure_amount '+str(self_medical_expenditure_amount))
		log.info('parent_medical_expenditure_amount '+str(parent_medical_expenditure_amount))
		
		if (self_citizen_type in senier_citizen and self_medical_expenditure_amount!=0) and (parent_citizen_type in senier_citizen and parent_medical_expenditure_amount!=0):
			log.info('3.self and family including parents(senior citizen)')
			medical_expenditure_code='3'

		elif self_citizen_type in senier_citizen and self_medical_expenditure_amount!=0:
			log.info('1-self and family(senior citizen)')
			medical_expenditure_code='1'

		elif parent_citizen_type in senier_citizen and parent_medical_expenditure_amount!=0:
			log.info('2-parents(senior citizen)')
			medical_expenditure_code='2'

	except Exception as ex:
		log.info('Error in getting medical_expenditure_code '+traceback.format_exc())
	return medical_expenditure_code


# preventive health chekup
# 1-self and family
# 2-parents
# 3- self and family and parents
def get_health_checkup_code(data,self_citizen_type,parent_citizen_type):
	health_checkup_code=''
	try:
		log.info('self_citizen_type '+self_citizen_type)
		log.info('parent_citizen_type '+parent_citizen_type)

		self_health_checkup_amount=data['self_health_checkup_amount']
		parent_health_checkup_amount=data['parent_health_checkup_amount']

		log.info('self_health_checkup_amount '+str(self_health_checkup_amount))
		log.info('parent_health_checkup_amount '+str(parent_health_checkup_amount))
		
		if (self_citizen_type=='OC' and self_health_checkup_amount!=0) and (parent_citizen_type=='OC' and parent_health_checkup_amount!=0):
			log.info('3- self and family and parents')
			health_checkup_code='3'

		elif (parent_citizen_type=='OC' and parent_health_checkup_amount!=0):
			log.info('2-parents')
			health_checkup_code='2'

		elif (self_citizen_type=='OC' and self_health_checkup_amount!=0):
			log.info('1-self and family')
			health_checkup_code='1'

	except Exception as ex:
		log.info('Error in getting medical_expenditure_code '+traceback.format_exc())
	return health_checkup_code

