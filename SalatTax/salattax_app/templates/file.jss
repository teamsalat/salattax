var app=angular.module('myApp', ['naif.base64'])

app.config(function ($interpolateProvider) {


    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});

app.controller('ctrl', function($scope, $http, $window, $rootScope){

  $scope.onChange = function (e, fileList) {
    alert(JSON.stringify($scope.files));
    alert('this is on-change handler!');
  };
  
  $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
    alert(JSON.stringify(fileObj));
    alert('this is handler for file reader onload event!');
  };

  var uploadedCount = 0;

  $scope.files = [];
})