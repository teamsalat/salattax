
app.config(function ($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('review_controller', ['$scope','$rootScope','$timeout','$compile','$location', function($scope,$rootScope,$timeout,$compile,$location,CONFIG){

	$scope.pricing={};

	$scope.pricing.expert_consultation_charge=0
	$scope.pricing.discount=0
	$scope.pricing.referral_code=''
	$scope.pricing.mode_of_payment='Online'

	var client_id = 1;
	var pan = '';
	var taxes_paid_al_height = 130;
	var adv_self_tax = 0;
	$scope.btn_disable = true ;
	$scope.col_shares = false;
	$scope.col_equity = false;
	$scope.col_debt = false;
	$scope.col_listed = false;
	var selected_fy = '';
	$scope.standard_deduction = false;

	

	$('.loader').show();
	$(window).load(function() {

		console.log('ID : '+$scope.user.id);
    	console.log('Client ID : '+$scope.user.client_id);
    	$rootScope.client_id=$scope.user.client_id
    	client_id=$scope.user.client_id
		// $scope.RF_charges($scope.pricing.RF_charges)

		// $scope.user.business_partner_id='1'
	 //    $scope.user.role='Partner'

		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);

				$scope.user.business_partner_id=response.business_partner_id
				$scope.user.role=response.role

				if(response.client_id != 0 && $scope.user.id == ''){
					// client_id = response.client_id;
					// email = response.email;
					// $scope.get_review();
				}
				else if($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				if($scope.user.id != ''){
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						async:false,
						data:{'username':$scope.user.id},
						success: function(response){
							console.log(response);
							// client_id = response.client_id;
							$.ajax({
								url:'/get_return_details/',
								type:'POST',
								dataType: 'json',
								async:false,
								data:{'client_id':client_id},
								success: function(response){
									console.log(response);
									$scope.return_detail_fy = response.return_year;
									selected_fy = response.return_year;
								}
							});
							email = response.email;
							$scope.get_review();
							$scope.get_RF_charges_details()
							$scope.get_invoice_details()
						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
			}
		});
		setTimeout(function(){
			if(taxes_paid_al_height == 130){
				$scope.taxes_paid_al = { 'display': 'none' };
				$scope.$apply();
			}
		}, 3000);
	});

	$scope.get_invoice_details=function(){

		$.ajax({

				url:'/get_invoice_details/',
				type:'POST',
				async:false,
				dataType: 'json',
				data:{'client_id':client_id,'total_amount_payable':$scope.pricing.total_amount_payable,'expert_consultation':$scope.pricing.expert_consultation_charge},
				success: function(response){
					console.log(response)

					$scope.payment_status=response.status
					$scope.pricing.total_amount_paid=response.total_amount_paid
					$scope.pricing.net_amount_payable=response.net_amount_payable

				},error:function(response){
					console.log(response)
				}
		})

	}

	$scope.RF_charges=function(charges){
		console.log(charges)

		// [
		// 	{
		// 		"id": 1, 
		// 		"return_complexity": "Basic Return Filing Charge", 
		// 		"charges": 249
		// 	}, 
		// 	{
		// 		"id": 2, 
		// 		"return_complexity": "Form16", 
		// 		"charges": 200
		// 	}, 
		// 	{   "id": 3, 
		// 		"return_complexity": "House Property", 
		// 		"charges": 200
		// 	}, 
		// 	{	"id": 4, 
		// 		"return_complexity": "Capital Gain - Shares", 
		// 		"charges": 300
		// 	}, 
		// 	{	"id": 5, 
		// 		"return_complexity": "Capital Gain - Mutual Funds", 
		// 		"charges": 300
		// 	}, 
		// 	{	"id": 6, 
		// 		"return_complexity": "Expert Review", 
		// 		"charges": 500
		// 	}, 
		// 	{	
		// 		"id": 7, 
		// 		"return_complexity": "Email Support", 
		// 		"charges": 0
		// 	}, 
		// 	{	
		// 		"id": 8, 
		// 		"return_complexity": "Chat Support", 
		// 		"charges": 0
		// 	}, 
		// 	{	
		// 		"id": 9, 
		// 		"return_complexity": "Phone Support", 
		// 		"charges": 100
		// 	}, 
		// 	{	
		// 		"id": 10, "return_complexity": "Express Return Filing", 
		// 		"charges": 750
		// 	}
		// ]

	}

	$scope.convert_to_int=function(value){

		if(value!=undefined && value!=0){
			return parseInt(value.toString().replace(/,/g,''))
		}else if(value==undefined){
			return 0
		}else{
			return parseInt(value)
		}

	}


	pricing_modal_show='Y'
	$scope.automation='N'

	$scope.pay_to_IT = function(){

		// $scope.tax_payable='110'
		$.ajax({
				url:'/get_payment_info/',
				type:'POST',
				async:false,
				dataType: 'json',
				data:{'client_id':client_id,'option':'efiling'},
				success: function(response){
					console.log(response);
					if (response.paid == 1 && $scope.payment_status!=1) {

						if($scope.convert_to_int($scope.tax_payable)==0){

							$scope.modal_msg='You do not need to pay any tax to the Income Tax Department. Now just click on Save and Submit and your job is done.'
							$scope.createsweetalert('',$scope.modal_msg,'info')

						}else if($scope.convert_to_int($scope.tax_payable)<0){

							$scope.modal_msg='You are entitled for a refund of amount Rs. '+$scope.money_format(Math.abs($scope.convert_to_int($scope.tax_payable)))+' subject to approval by Income Tax Department. The refund shall get credited to your bank account provided in this return. Now just click on Save and Submit and your job is done for now.'
							$scope.createsweetalert('Good news!',$scope.modal_msg,'info')

						}else if($scope.convert_to_int($scope.tax_payable)>0){

							payment_link="https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp"
							// window.open('https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp')
							$scope.modal_msg='You are required to pay Rs. '+$scope.tax_payable+' to Income Tax Department. Please make the payment and mention the following details on this page.'
							
							$scope.swal_with_link('',$scope.modal_msg,'info',payment_link)

							$scope.automation='Y'
						}

						pricing_modal_show='N'

					}else{

						$scope.modal_msg='Please pay e-filing charges'
						$scope.createsweetalert('',$scope.modal_msg,'')
					}
				
				}
		});
		// $scope.tax_payable='72,000'
		
		// $('#pay_salat').modal('show');

	}

	$("#pay_salat").on("hidden.bs.modal", function () {

		if(pricing_modal_show=='Y')
        	$('#pricing').modal('show');
    });




    $scope.generate_xml_validation=function(){

    	$.ajax({
				
				url:'/get_payment_info/',
				type:'POST',
				async:false,
				dataType: 'json',
				data:{'client_id':client_id,'option':'efiling'},
				success: function(response){
					console.log(response);
					if (response.paid == 1 && $scope.payment_status!=1) 
					{


				    	if($scope.automation=='Y'){

				    		console.log($scope.pricing.BSR_code)
				    		if(($scope.pricing.BSR_code!='' && $scope.pricing.BSR_code!=undefined) && ($scope.pricing.date_of_deposit!='' && $scope.pricing.date_of_deposit!=undefined) && ($scope.pricing.challan_no!='' && $scope.pricing.challan_no!=undefined) && ($scope.pricing.amount_paid!='' && $scope.pricing.amount_paid!=undefined)){

				    			$scope.payment_details_error=''
				    			$('#payment_details_error').hide();

				    			var data={
						    		'BSR_code':$scope.pricing.BSR_code,
						    		'date_of_deposit':$scope.pricing.date_of_deposit,
						    		'challan_no':$scope.pricing.challan_no,
						    		'amount_paid':$scope.pricing.amount_paid,
						    		'major_head':'21',
						    		'minor_head':'300'
						    	}

						    	var data=JSON.stringify(data)

						    	console.log(data)


						    	$.ajax({
									url:'/pay_to_IT_api/',
									type:'POST',
									dataType: 'json',
									async:false,
									data:{'client_id':client_id,'data':data},
									success: function(response){
										console.log(response);
										if(response.status==0){
											$scope.calculate_tax_payable();

										}
										
									},error:function(response){
										console.log(response);
									}
								});

								$scope.generate_xml();

				    		}else{

				    			$scope.payment_details_error='Please fill above details'
				    			$('#payment_details_error').show();
				    		}

				    
				    	}else{
				    		$scope.generate_xml();
				    	}

				    }else{

						$scope.modal_msg='Please pay salat e-filing charges'
						$scope.createsweetalert('',$scope.modal_msg,'')

					}
				}
		});


    	// $scope.$apply();

    }


	$scope.generate_xml = function(type,value,$event){

		var tax_payable=$scope.convert_to_int($scope.tax_payable)

		if(tax_payable==0  || tax_payable<0){

			console.log(tax_payable)
			$scope.xml_download = {"display" : "none"};

			$.ajax({
				url:'/generate_xml/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id},
				success: function(response){
					console.log(response);

					$scope.createswalgif()
					if(response.status == 'success'){
						// $scope.createsweetalert('Success','message','success')
						$scope.modal_msg='Success'
						$scope.user_tracking('submitted','Review - Save & File Return');
						
						$scope.xml_link = $scope.url+'/static/xml/'+response.zip_file;
						$scope.xml_download = {"display" : "block"};

						// "AYHPT6350R_ITR-2_2019_N_1234.xml"
						$scope.ITR_type='ITR'+response.ITR

						$.ajax({
							url:'/return_filing_success_mail/',
							type:'POST',
							dataType: 'json',
							data:{'client_id':client_id,'xml_path':response.zip_file,'ITR_type':$scope.ITR_type},
							success: function(response){
								console.log(response);
							},error:function(response){
								console.log(response);
							}
						})

					}
					else{
						
						// $scope.createsweetalert('Error','message','error')
						$scope.user_tracking('error in data',response.status);

						$.ajax({
							url:'/return_filing_failure_mail/',
							type:'POST',
							dataType: 'json',
							data:{'client_id':client_id,'business_partner_id':$scope.user.business_partner_id,'error':JSON.stringify(response)},
							success: function(response){
								console.log(response);
							},error:function(response){
								console.log(response);
							}
						})
					}
						
					/*$.ajax({
						url:'/html_to_pdf/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
						}
					});*/
					$scope.$apply();
				},error:function(response){

					$.ajax({
							url:'/return_filing_failure_mail/',
							type:'POST',
							dataType: 'json',
							data:{'client_id':client_id,'business_partner_id':$scope.user.business_partner_id,'error':JSON.stringify(response)},
							success: function(response){
								console.log(response);
							},error:function(response){
								console.log(response);
							}
					})

				}
			});


		}else{

			if(tax_payable>0){

				payment_link="https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp"
				// window.open('https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp')
				$scope.modal_msg='You are required to pay Rs. '+$scope.tax_payable+' to Income Tax Department. Please make the payment and mention the following details on this page.'
							
				$scope.swal_with_link('',$scope.modal_msg,'info',payment_link)

				$scope.automation='Y'
			}


		}

	}

	$scope.html_to_pdf=function(){

		$('.loader').show();
		$.ajax({
						url:'/html_to_pdf/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){


							console.log(response);

							console.log(response.flag)

							if(response.flag==0){
								$scope.pdf_link=response.file
								// $scope.pdf_download=true
								$scope.$apply()

								setTimeout(function() {
								    document.getElementById('pdf_link').click()        
								 }, 0);

								// $timeout(function() {
								// 	angular.element(document.getElementById('pdf_link')).trigger('click')
								// })
							}

							$('.loader').hide();
								
						},error:function(response){
							$('.loader').hide();
						}
					});
	}

	$scope.calculate_tax_payable_again=function()
	{
		if($scope.automation=='Y')
		{
			console.log($scope.pricing.BSR_code)
			if(($scope.pricing.BSR_code!='' && $scope.pricing.BSR_code!=undefined) && ($scope.pricing.date_of_deposit!='' && $scope.pricing.date_of_deposit!=undefined) && ($scope.pricing.challan_no!='' && $scope.pricing.challan_no!=undefined) && ($scope.pricing.amount_paid!='' && $scope.pricing.amount_paid!=undefined))
			{

				$scope.payment_details_error=''
				$('#payment_details_error').hide();

				var data={
						    'BSR_code':$scope.pricing.BSR_code,
						    'date_of_deposit':$scope.pricing.date_of_deposit,
						    'challan_no':$scope.pricing.challan_no,
						    'amount_paid':$scope.pricing.amount_paid,
						    'major_head':'21',
						    'minor_head':'300'
				}

			    var data=JSON.stringify(data)

				console.log(data)


				$.ajax({
									url:'/pay_to_IT_api/',
									type:'POST',
									dataType: 'json',
									async:false,
									data:{'client_id':client_id,'data':data},
									success: function(response){
										console.log(response);
										if(response.status==0){

											$scope.calculate_tax_payable();
											$scope.createsweetalert('Success','message','success')

										}
										
									},error:function(response){

										$scope.createsweetalert('Error','message','error')
										console.log(response);
									}
				});


			}else{

				    $scope.payment_details_error='Please fill above details'
				    $('#payment_details_error').show();
		    }

				    
	    }

	}

	$scope.swal_with_link=function(title,message,status,link){

		swal({

	                title:title,
	                text:message,
	                type: status,
	                showCancelButton: true,
	                closeOnConfirm: false,
	                html: false,
	                confirmButtonText: "Payment Link",
	               
	               
	     }, function(isConfirm){

	        if(isConfirm){
	         
	          window.open(link)
	        
	        }

	    })
	}

	$scope.createsweetalert=function(title,message,status){

		console.log(title)
	
		swal({
		      title: title,
		      text: message,
		      type: status,
		      showCancelButton: false,
		      closeOnConfirm: false,
		      showConfirmButton:true,
		}, function() {

		     swal.close();
		    
		})

	  // alert(msg)

	}

	$scope.createswalgif=function(){

		var img='/static/img/ezgif.com-resize.gif'

		swal({
	        title: "",
	        customClass: 'success-swal-wide',
	        text: "<img src='" + img + "' style='width: 100%;height: 100%;'>",
	        html: true,
	        showConfirmButton: false,
	        allowOutsideClick: true
	    });

	    $timeout(function(){

		  		swal({

	              title: "What Next?",
	              customClass: 'success-swal-wide',
	              text: "Our team will upload your return on income tax portal in next 48 hours and you will be notified via email. In case you have chosen priority return filing you get to skip the queue and return will be filed in just 2 hours. <p>Your ITRV will be emailed to you with instruction on how to verify the return.</p><p>In the meanwhile <a href="+$location.protocol()+"://"+$location.host()+"/tax_report"+" target='_blank' style='text-decoration: underline;color:#089CE4'>click here</a> to check your Tax Leakage for free.</p><p>Tax Leakage means the excess tax you could have saved with proper tax planning.</p>",
	              // showConfirmButton: false,
	              allowOutsideClick: true,
	              html: true,
	            
	        	});

	    },5000);

	
	}


	$scope.toTitleCase=function(str) {
	        return str.replace(
	            /\w\S*/g,
	            function(txt) {
	                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	            }
	        );
	}


	$scope.get_review = function(type,value,$event){
		$.ajax({
			url:'/get_personal_info/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				// console.log(response);
				$scope.client_name = $scope.toTitleCase(response.name);
				$scope.client_PAN = response.PAN;
				$scope.client_DOB = response.dob;
				$scope.client_address = $scope.toTitleCase(response.address);
				// $scope.$apply();
			}
		});

		$.ajax({
			url:'/get_statement_income/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				var ele1 = angular.element( document.querySelector( '#statement_company' ));
				var ele2 = angular.element( document.querySelector( '#statement_salary' ));
				var ele3 = angular.element( document.querySelector( '#statement_less_tax' ));
				var ele4 = angular.element( document.querySelector( '#entertainment_allowance' ));

				if(response.no_of_company == 0)
					$scope.statement_income_div = {'display':'none'};

				for (var i = 0; i < response.no_of_company; i++){
					var c_text = "<td>"+ $scope.toTitleCase(response.name[i])+"</td>";
					var temp = $compile(c_text)($scope);
					ele1.append( temp );

					// format_salary = money_format(response.salary[i],'');
					format_salary = $scope.money_format(response.salary[i],'');
					var c_text = "<td>"+format_salary+"</td>";
					var temp = $compile(c_text)($scope);
					ele2.append( temp );

					format_less_tax = $scope.money_format(response.less_tax[i],'');
					var c_text = "<td>"+format_less_tax+"</td>";
					var temp = $compile(c_text)($scope);
					ele3.append( temp );

					entertainment_allowance = $scope.money_format(response.entertainment_allowance[i],'');
					var c_text = "<td>"+entertainment_allowance+"</td>";
					var temp = $compile(c_text)($scope);
					ele4.append( temp );
				}

				if(response.entertainment_allowance.includes(5000)){
					entertainment_allowance=5000
				}else{
					entertainment_allowance=0
				}

				if(selected_fy == '2018-2019'){
					$scope.standard_deduction = true;
					$scope.ICU_head_salary = $scope.money_format(response.ICU_head_sal-40000-entertainment_allowance);
				}
				else{
					$scope.standard_deduction = false;
					$scope.ICU_head_salary = $scope.money_format(response.ICU_head_sal);
				}
				// $scope.$apply();
			}
		});

		$.ajax({
			url:'/get_house_income/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				$scope.style_income_house = { 'height': '550px' };
				var ele1 = angular.element( document.querySelector( '#hp_type' ));
				var ele2 = angular.element( document.querySelector( '#hp_add' ));
				var ele3 = angular.element( document.querySelector( '#hp_rent' ));
				var ele4 = angular.element( document.querySelector( '#hp_municipal' ));
				var ele8 = angular.element( document.querySelector( '#hp_assessee_share' ));
				var ele9 = angular.element( document.querySelector( '#hp_proportionate_annual_value' ));
				var ele5 = angular.element( document.querySelector( '#hp_interest' ));
				var ele6 = angular.element( document.querySelector( '#hp_deduction' ));
				var ele7 = angular.element( document.querySelector( '#hp_net' ));
				if(response.no_of_property == 0)
					$scope.style_income_house = {'display':'none'};

				for (var i = 0; i < response.no_of_property; i++){
					if(i==2){
						$scope.style_income_house = { 'height': '500px' };
					}
					if (response.Type_of_House_Property[i] == 'let_out')
						var c_text = "<td class='capital_width'>Let Out</td>";
					else if (response.Type_of_House_Property[i] == 'self_occupied')
						var c_text = "<td class='capital_width'>Self Occupied</td>";
					else
						var c_text = "<td class='capital_width'>"+$scope.toTitleCase(response.Type_of_House_Property[i])+"</td>";
					var temp = $compile(c_text)($scope);
					ele1.append( temp );

					var c_text = "<td>"+$scope.toTitleCase(response.address[i])+"</td>";
					var temp = $compile(c_text)($scope);
					ele2.append( temp );

					format_rent = $scope.money_format(response.rent_r[i],'');
					format_municipal_tax = $scope.money_format(response.municipal_tax[i],'');
					
					assessee_share = $scope.money_format(response.assessee_share[i],'');
					
					proportionate_annual_value = $scope.money_format(response.proportionate_annual_value[i],'');

					format_interest_payble = $scope.money_format(response.interest_payble[i],'');
					format_standard_deduction = $scope.money_format(response.standard_deduction[i],'');
					format_net_income = $scope.money_format(response.ICU_house_p_latest[i],'');
					var c_text = "<td>"+format_rent+"</td>";
					var temp = $compile(c_text)($scope);
					ele3.append( temp );

					var c_text = "<td>"+format_municipal_tax+"</td>";
					var temp = $compile(c_text)($scope);
					ele4.append( temp );

					var c_text = "<td>"+assessee_share+"</td>";
					var temp = $compile(c_text)($scope);
					ele8.append( temp );

					var c_text = "<td>"+proportionate_annual_value+"</td>";
					var temp = $compile(c_text)($scope);
					ele9.append( temp );

					var c_text = "<td>"+format_interest_payble+"</td>";
					var temp = $compile(c_text)($scope);
					ele5.append( temp );

					var c_text = "<td>"+format_standard_deduction+"</td>";
					var temp = $compile(c_text)($scope);
					ele6.append( temp );

					var c_text = "<td>"+format_net_income+"</td>";
					var temp = $compile(c_text)($scope);
					ele7.append( temp );
				}

				$scope.ICU_house_property = $scope.money_format(response.ICU_house_p);
				// $scope.$apply();
			}
		});

		$.ajax({
			url:'/get_CG_income/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				if(response.shares == 1){
					$scope.st_shares = $scope.money_format(response.st_shares_gain,'');
					$scope.lt_shares = $scope.money_format(response.lt_shares_gain,'');
					$scope.col_shares = true;
				}
				if(response.equity == 1){
					$scope.st_equity = $scope.money_format(response.st_equity_gain,'');
					$scope.lt_equity = $scope.money_format(response.lt_equity_gain,'');
					$scope.col_equity = true;
				}
				if(response.debt_MF == 1){
					$scope.st_debt = $scope.money_format(response.st_debt_MF_gain,'');
					$scope.lt_debt = $scope.money_format(response.lt_debt_MF_gain,'');
					$scope.col_debt = true;
				}
				if(response.listed_d == 1){
					$scope.st_listed = $scope.money_format(response.st_listed_d_gain,'');
					$scope.lt_listed = $scope.money_format(response.lt_listed_d_gain,'');
					$scope.col_listed = true;
				}
				if(response.shares==0 && response.equity==0 && response.debt_MF==0 && response.listed_d==0){
					$scope.capital_gain_div = {'display':'none'};
				}
				else
					$scope.capital_gain_div = {'display':'block'};

				$scope.col_total=true;
				$scope.lt_total=$scope.money_format(response.LT_Total)
				$scope.st_total=$scope.money_format(response.ST_Total)

				$scope.Income_chargeable_CG_at_applicable_rates=$scope.money_format(response.Income_chargeable_CG_at_applicable_rates)
				$scope.Income_chargeable_CG_at_special_rates=$scope.money_format(response.Income_chargeable_CG_at_special_rates)
				$scope.carry_forword_losses=$scope.money_format(response.carry_forword_losses)
				// $scope.$apply();
			}
		});


		$.ajax({
			url:'/get_statement_income_other_source/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				$scope.other_II = $scope.money_format(response.Interest_Income);
				$scope.other_OI = $scope.money_format(response.Other_Income);
				$scope.other_SBI = $scope.money_format(response.Interest_Savings);
				$scope.other_DE = $scope.money_format(response.dividend);
				$scope.other_FD = $scope.money_format(response.FD);
				$scope.ICU_other_sources = $scope.money_format(response.ICU_other_s);
				// $scope.$apply();
			}
		});

		
		
		$.ajax({
			url:'/get_deduction/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				$scope.deduction_80C = $scope.money_format(response.d_80c);
				$scope.deduction_80CCD = $scope.money_format(response.d_80ccd);
				$scope.deduction_80G = $scope.money_format(response.d_80g);
				$scope.deduction_80DD = $scope.money_format(response.d_80dd);
				$scope.deduction_80U = $scope.money_format(response.d_80u);
				$scope.deduction_80D = $scope.money_format(response.d_80d);
				$scope.deduction_80E = $scope.money_format(response.d_80e);
				$scope.deduction_80TTA = $scope.money_format(response.d_80tta);
				$scope.citizen_type=response.citizen_type
				$scope.deduction_80TTB = $scope.money_format(response.d_80ttb);
				$scope.Total_deduction = $scope.money_format(response.total_deduction);
				// $scope.$apply();
			}
		});

		$.ajax({
			url:'/get_26AS_tds/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				var container = angular.element( document.querySelector( '#tax_al_paid' ));
				var total_tds_paid = 0;
				var j = 0;
				for (var i = 0; i < response.no_of_tds; i++){
					TAN = response.tan_pan[i];
					name_employer = response.d_c_name[i];
					income_under_head = response.amt_paid[i];
					tax_deducted = response.tds_deposited[i];
					//change condition section == 192 is 'Tax Deducted at Source on Salary'
					// if(response.section[i] == '192' || response.section[i]=='194A'){
						j += 1;
						// if(j==1)
							// taxes_paid_al_height += 160;

						// taxes_paid_al_height += 48;
						markup = "<tr><td>"+j+"</td><td>"+TAN+"</td><td>"+name_employer+"</td><td>"+income_under_head+"</td><td>"+tax_deducted+"</td></tr>";
						// var temp = $compile(markup)($scope);
						// container.append( temp );
						total_tds_paid += tax_deducted;
					// }
				}
				if(j == 0){
					$scope.taxes_paid_al_div = {'display':'none'};
				}else
					taxes_paid_al_height += 1;

				$scope.taxes_paid_al = { 'height': taxes_paid_al_height+'px' };
				$scope.total_tds_paid = $scope.money_format(total_tds_paid);
				// $scope.$apply();
			}
		});

		$.ajax({
			url:'/get_26AS_taxes/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'r_id':client_id},
			success: function(response){
				// console.log(response);
				var container = angular.element( document.querySelector( '#self_tax_paid' ));
				for (var i = 0; i < response.no_of_tax; i++){
					// if(i ==0)
					// 	taxes_paid_al_height += 150;

					deposit_date = response.d_date[i];
					BSR_code = response.bsr_code[i];
					challan_no = response.challan_no[i];
					total_tax = response.tax_p[i];
					j = i+1;
					// taxes_paid_al_height += 40;
					//change condition section == 192 is 'Tax Deducted at Source on Salary'
					markup = "<tr><td>"+j+"</td><td>"+deposit_date+"</td><td>"+BSR_code+"</td><td>"+challan_no+"</td><td>"+total_tax+"</td></tr>";
					// var temp = $compile(markup)($scope);
					// container.append( temp );
				}
				if(response.no_of_tax == 0){
					$scope.adv_tax_div = {'display':'none'};
				}
				else{
					taxes_paid_al_height += 1;
					$scope.taxes_paid_al = { 'height': taxes_paid_al_height+'px' };
					$scope.total_adv_tax_paid = $scope.money_format(response.total_adv_tax_paid);
				}

				$scope.total_tax=$scope.money_format($scope.convert_to_int($scope.total_tds_paid)+$scope.convert_to_int($scope.total_adv_tax_paid))
				// $scope.$apply();


			}
		});

		$scope.calculate_tax_payable();

		$scope.$apply();

	}

	$scope.calculate_tax_payable=function(){

		$.ajax({
			url:'/cal_income_chargeable/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id,'pan':pan},
			success: function(response){
				console.log(response);
				$scope.GTI = $scope.money_format(response.GTI);
				$scope.ICU_capital_gain = $scope.money_format(response.ICU_CG);

				// Math.ceil(N / 10) * 10;
				var taxable_income=$scope.money_format(Math.max(0,Math.round((response.TI)/10)*10))
				$scope.taxable_income = taxable_income
				$scope.IT_normal_rates = $scope.money_format(response.Tx);
				$scope.IT_special_rates = $scope.money_format(response.Tx1);
				$scope.rebate = $scope.money_format(response.rebate);
				$scope.surcharge = $scope.money_format(response.surcharge);
				$scope.education_cess = $scope.money_format(response.education_cess);
				$scope.tax_with_cess = $scope.money_format(response.tax_with_cess);
				$scope.balance_tax = $scope.money_format(Math.round((response.balance_tax_FE)/10)*10);
				$scope.add_interest = $scope.money_format(response.Interest_on_FE);
				$scope.tax_payable = $scope.money_format(Math.round((response.Tax_payable_FE)/10)*10); // balance_tax + add_interest
				
				if($scope.convert_to_int($scope.tax_payable)>=0){
					$scope.tax_payable_label='Y'
					$scope.refund_label='N'
				}else if($scope.convert_to_int($scope.tax_payable)<0){
					$scope.refund_label='Y'
					$scope.tax_payable_label='N'
				}
				// if(response.balance_tax + 0 <= 0)
				// 	$scope.pay_directly_to_IT = {"display" : "none"};

				// $scope.$apply();
			}
		});
	}
	$scope.readNcontinue = function($event){
		if($scope.user.i_agree)
			$scope.btn_disable = false ;
		else
			$scope.btn_disable = true ;
	}

	$scope.money_format = function(val){

		if(val!=undefined){

				val = val.toString();
				var n = val.indexOf(".");
				var x = "";
				if(n>=0)
					x = val.split(".")[0];
				else
					x = val;

				if(val=='NA' || val=='')
					return 0

				if(!isNaN(x)){
					// x = x.toString();
					var n_minus = x.indexOf("-");
					if(n_minus>=0)
						x = x.replace("-","");
					var lastThree = x.substring(x.length-3);
					var otherNumbers = x.substring(0,x.length-3);
					if(otherNumbers != '')
					    lastThree = ',' + lastThree;
					var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
					if(n_minus>=0)
						return "-"+res;
					else
						return res;
				}else
					return val;

		}else{
			return ''
		}
	
	}


	$scope.hide_terms_and_condition_error=function(value){
		if(value=='true'){

			$scope.terms_and_conditions_error=''
			$('#terms_and_conditions_error').hide()

		}
	}


	$scope.RF_charges_details=function(pricing){

		console.log($scope.pricing.terms_and_conditions)
		if($scope.pricing.terms_and_conditions=='true'){

			$scope.terms_and_conditions_error=''
			$('#terms_and_conditions_error').hide()

			var data=JSON.stringify(pricing)
			// console.log(data)

			$.ajax({

					url:'/RF_payment_details_api/',
					type:'POST',
					dataType: 'json',
					async:false,
					data:{'data':data,'client_id':client_id,'b_id':$scope.user.business_partner_id,'role':$scope.user.role},
					success: function(response){
						
						$('#pricing_api_error').hide()

						if($scope.pricing.net_amount_payable==0)
							amount_pay=$scope.pricing.total_amount_payable
						else
							amount_pay=$scope.pricing.net_amount_payable

						console.log(JSON.stringify(response));
						if(response.status==0){
							if($scope.pricing.mode_of_payment=='Online'){
								window.location.href = redirect_url+'/subscribe_payumoney/'+client_id+'amt'+amount_pay+'amt'+'efiling';
							}else{
								if(response.partner_share_flag==0){
									$scope.pricing_api_msg=response.partner_share_status
									$scope.createsweetalert('Success',$scope.pricing_api_msg,'success')
									
								}else if(response.partner_share_flag==1){
									// $('#pricing_api_error').show()
                                    $scope.pricing_api_error=response.partner_share_status
									$scope.createsweetalert('Ooops !',$scope.pricing_api_error,'info')
								}

								$timeout(function() {
										$('#pricing').modal('hide')
								}, 3000);
								// $('#pricing').modal('hide')
							}
						}
					

					},error:function(response){

						// console.log('error')
						console.log(JSON.stringify(response));

					}

			})

		}else{
			$scope.terms_and_conditions_error='Please select terms & conditions policy'
			$('#terms_and_conditions_error').show()
		}
		

	}

	$scope.referral_code=false;
	$scope.get_RF_charges_details=function(){

		var charges_object=JSON.parse($scope.pricing.RF_charges)
		console.log(charges_object[0].charges)
		$scope.pricing.basic_return_filing_charge=charges_object[0].charges

		$.ajax({
					url:'/get_details_for_pricing/',
					type:'POST',
					dataType: 'json',
					async:false,
					data:{'client_id':client_id},
					success: function(response){
						
						console.log(JSON.stringify(response));

						console.log(response.data);

						if(response.data.length===0){

							$scope.pricing.no_of_companies_hide_show='no'
							$scope.pricing.no_of_house_properties_hide_show='no'
							$scope.capital_gains='no'
							
						}else{

							$scope.pricing.no_of_companies_hide_show=response.data[0].salary_income
							$scope.pricing.no_of_house_properties_hide_show=response.data[0].house_property
							$scope.capital_gains=response.data[0].capital_gain
						}

						
						// no_of_companies
						$scope.pricing.no_of_companies=response.no_of_companies

						if($scope.pricing.no_of_companies==2){
							$scope.pricing.no_of_companies_charge=charges_object[1].charges
						}else if($scope.pricing.no_of_companies==3){
							$scope.pricing.no_of_companies_charge=2*charges_object[1].charges
						}else{
							$scope.pricing.no_of_companies_charge=0
						}

						// no_of_house_properties
						$scope.pricing.no_of_house_properties=response.no_of_house_properties
							
						if($scope.pricing.no_of_house_properties==2){
							$scope.pricing.no_of_house_properties_charge=charges_object[2].charges
						}else if($scope.pricing.no_of_house_properties==3){
							$scope.pricing.no_of_house_properties_charge=2*charges_object[2].charges
						}else{
							$scope.pricing.no_of_house_properties_charge=0
						}

						// capital gain
						if($scope.capital_gains=='yes'){

							if(response.shares==true){
								$scope.pricing.CG_shares_charge=charges_object[3].charges
							}else{
								$scope.pricing.CG_shares_charge=0
							}

							if(response.MF==true){
								$scope.pricing.CG_MF_charge=charges_object[4].charges
							}else{
								$scope.pricing.CG_MF_charge=0
							}
							
						}else{
							$scope.pricing.CG_shares_charge=0
							$scope.pricing.CG_MF_charge=0
						}

						$scope.pricing.expert_review_charge_db=charges_object[5].charges
						$scope.pricing.expert_review_charge=0

						$scope.pricing.email_support_charge=charges_object[6].charges

						$scope.pricing.phone_support_charge_db=charges_object[8].charges
						$scope.pricing.phone_support_charge=0

						$scope.pricing.chat_support_charge=charges_object[7].charges


						$scope.pricing.priority_RF_charge_db=charges_object[9].charges
						$scope.pricing.priority_RF_charge=0


						$scope.pricing.total_return_filing_charges=$scope.total_return_filing_charges()
							
						if(response.referred_by_code!='' && response.referred_by_code!=null){
							$scope.pricing.referral_code=response.referred_by_code
							$scope.calculate_discount($scope.pricing.referral_code,$scope.pricing.total_return_filing_charges)
							$scope.referral_code=true;
						}else{
							$scope.pricing.total_amount_payable=$scope.pricing.total_return_filing_charges
						}
					
						$('.loader').hide();
					},error:function(response){
						console.log(response);
					}
		});

	}

	$scope.expert_review_charges=function(value){
		if(value=='yes'){
			$scope.pricing.expert_review_charge=$scope.pricing.expert_review_charge_db
		}else{
			$scope.pricing.expert_review_charge=0
		}

		$scope.pricing.total_return_filing_charges=$scope.total_return_filing_charges()
		$scope.pricing.total_amount_payable=parseInt($scope.pricing.total_return_filing_charges)+parseInt($scope.pricing.expert_consultation_charge)
		$scope.calculate_discount($scope.pricing.referral_code,$scope.pricing.total_return_filing_charges)
		$scope.get_invoice_details();
	}

	$scope.phone_support_charges=function(value){
		if(value=='yes'){
			$scope.pricing.phone_support_charge=$scope.pricing.phone_support_charge_db
		}else{
			$scope.pricing.phone_support_charge=0
		}

		$scope.pricing.total_return_filing_charges=$scope.total_return_filing_charges()
		$scope.pricing.total_amount_payable=parseInt($scope.pricing.total_return_filing_charges)+parseInt($scope.pricing.expert_consultation_charge)
		$scope.calculate_discount($scope.pricing.referral_code,$scope.pricing.total_return_filing_charges)
		$scope.get_invoice_details();
	}


	$scope.priority_RF_charges=function(value){
		if(value=='yes'){
			$scope.pricing.priority_RF_charge=$scope.pricing.priority_RF_charge_db
		}else{
			$scope.pricing.priority_RF_charge=0
		}

		$scope.pricing.total_return_filing_charges=$scope.total_return_filing_charges()
		$scope.pricing.total_amount_payable=parseInt($scope.pricing.total_return_filing_charges)+parseInt($scope.pricing.expert_consultation_charge)
		$scope.calculate_discount($scope.pricing.referral_code,$scope.pricing.total_return_filing_charges)
		$scope.get_invoice_details();
	}

	$scope.total_return_filing_charges=function(){
		total_return_filing_charges=($scope.pricing.basic_return_filing_charge
													+$scope.pricing.no_of_companies_charge
													+$scope.pricing.no_of_house_properties_charge
													+$scope.pricing.CG_shares_charge
													+$scope.pricing.CG_MF_charge
													+$scope.pricing.expert_review_charge
													+$scope.pricing.email_support_charge
													+$scope.pricing.phone_support_charge
													+$scope.pricing.chat_support_charge
													+$scope.pricing.priority_RF_charge)

		return total_return_filing_charges

	}

	$scope.total_payable_amount=function(){

		var total=(parseInt($scope.pricing.total_return_filing_charges)
				  -parseInt($scope.pricing.discount))
				  +parseInt($scope.pricing.expert_consultation_charge)
		return total
	}


	$scope.expert_consultaion_charge_calculation=function(){

		// console.log($scope.pricing.expert_consultation_charge)
		if($scope.pricing.expert_consultation_charge!='' && $scope.pricing.expert_consultation_charge!=undefined){
			$scope.pricing.total_amount_payable=parseInt($scope.pricing.total_amount_payable)+parseInt($scope.pricing.expert_consultation_charge)
			// console.log('if')
		}else{
			// console.log('else')
			$scope.pricing.total_amount_payable=(parseInt($scope.pricing.total_return_filing_charges)
				  -parseInt($scope.pricing.discount))
			$scope.pricing.expert_consultation_charge=0
		}

		$scope.get_invoice_details();

	}


	$scope.calculate_discount=function(discount_code,total_amount){

		if(discount_code!=undefined && discount_code!=''){

			$.ajax({

					url:'/calculate_discount/',
					type:'POST',
					dataType: 'json',
					async:false,
					data:{'discount_code':discount_code,'total_amount':total_amount},
					success: function(response){
						
						console.log(JSON.stringify(response));
						if(response.status==0){

							$scope.referral_code_error=''
							$('#referral_code_error').hide()
							$scope.pricing.discount=response.data


						}else if(response.status==1){

							$scope.referral_code_error=response.data
							$scope.pricing.discount=0
							$('#referral_code_error').show()

						}

						

					},error:function(response){

						// console.log('error')
						console.log(JSON.stringify(response));

						$scope.referral_code_error=error
						$('#referral_code_error').show()

						$scope.pricing.discount=0
					}

			})

			// $scope.$apply();
			// console.log($scope.pricing.discount)
			$scope.pricing.total_amount_payable=$scope.total_payable_amount()

			$scope.get_invoice_details()

		}
			// alert(discount_code)
			

	}

}]);
	