from .get_all_data import *
from .models import TenantDetails,allowances,SalaryBreakUp,PerquisiteBreakUp,ProfitinLieuBreakUp
from .serializer import TenantDetailsSerializer,PropertyOwnerDetailsSerializer,allowancesSerializer,SalaryBreakupSerializer,PerquisiteBreakUpSerializer,ProfitinLieuBreakUpSerializer

C_ID=0
R_ID=0
citizen_type,age=('',)*2

CYLA_setoffs={}

CG_special_rates={}

common={}

CYLA_common={}

AMT_common={}

CFL_common={}

TotIncUnderHeadSalaries,NetSalary,standard_deduction=(0,)*3

StclSetoff15Per_global,StclSetoffAppRate_global,LtclSetOff10Per_global,LtclSetOff20Per_global=(0,)*4

TotalIncomeChargeableUnHP,TotOthSrcNoRaceHorse,TotHPlossCurYr,TotHPlossCurYrSetoff,TotOthSrcLossNoRaceHorseSetoff,Total_CurrentAYloss=(0,)*6

CYLA_S_IncOfCurYrAfterSetOff,CYLA_HP_IncOfCurYrAfterSetOff=(0,)*2
IncOfCurYrUnderThatHead,InStcg15Per_CurrYrCapGain,InStcgAppRate_CurrYrCapGain,InLtcg10Per_CurrYrCapGain,InLtcg20Per_CurrYrCapGain=(0,)*5
block_content =''
fname, mname, lname, PAN, dob, address, mobile, AadhaarCardNo,capacity,status = ('',)*10

ResidenceNo, ResidenceName, RoadOrStreet, LocalityOrArea, city, state, PinCode = ('',)*7
email, Employer_category, residential_status = ('',)*3

sal, ICU_head_sal, r, m, sd, i, ICU_house_p, Interest_Income, Interest_Savings, FD = (0,)*10
Other_Income, dividend, ICU_other_s, GTI, TI, Tx = (0,)*6

interest_234A,interest_234B,interest_234C,interest_234F,total_interest=(0,)*5

Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
total_adv_tax_paid, balance_tax, other_exempt_income = (0,)*3
salary, PerquisitesValue, ProfitsInSalary, DeductionUs16 = (0,)*4
total_deduction,taxable_income,agri_income, no_of_company = (0,)*4

d_80c ,d_80ccc ,d_80ccd ,d_80g ,d_80dd ,d_80u ,d_80d ,d_80e ,d_80tta ,d_80ttb,d_80ddb ,d_80ee = (0,)*12
d_80gg ,d_80gga ,d_80ggc ,d_80rrb ,d_80qqb ,d_80ccg, d_80ccd1 = (0,)*7

ttp ,rebate ,educess ,gtl  = (0,)*4

adv_t, sa_t, tds_value, tcs_value = (0,)*4

CG_share_equity_LT,total_CG = (0,)*2

share_equity_cost_acq,share_equity_sell_value,share_equity_gain = (0,)*3

tds_count, total_tds1, tcs_count, total_tcs, tds2_count, total_tds2, tds3_count, total_tds3,CreationDate = (0,)*9

tds_dc_name = []
tds_amt_paid = []
tds_deducted = []
tds_TAN_PAN = []
tcs_dc_name = []
tcs_amt_paid = []
tcs_deducted = []
tcs_TAN_PAN = []
tds2_dc_name = []
tds2_amt_paid = []
tds2_tds_deducted = []
tds2_TAN_PAN = []
tds2_section = []

tds3_dc_name = []
tds3_amt_paid = []
tds3_tds_deducted = []
tds3_TAN_PAN = []
tds3_section = []

tp_bsr_code = []
tp_deposit_date = []
tp_challan_no = []
tp_total_tax = []

st_shares_tpv, st_shares_tsv, st_shares_gain= (0,)*3
lt_shares_tpv, lt_shares_tsv, lt_shares_gain, lt_shares_FMV= (0,)*4
st_equity_tpv, st_equity_tsv, st_equity_gain = (0,)*3
lt_equity_tpv, lt_equity_tsv, lt_equity_gain,lt_equity_FMV= (0,)*4
st_debt_MF_tpv, st_debt_MF_tsv, st_debt_MF_gain = (0,)*3
lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
st_listed_d_tpv, st_listed_d_tsv, st_listed_d_gain = (0,)*3
lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3

Add_statecode,no_of_bank,no_of_HP,share_equity_sell_value,Tax_Free_Interest,tax_paid_count = (0,)*6

bank_name,IFSC,acc_no,acc_type = (0,)*4

HP_type_of_hp,HP_Rent_Received,HP_municipal_tax,HP_add,HP_city,HP_state,HP_pin,HP_Interest = (0,)*8

HP_Is_coOwned,HP_co_owner_name,HP_co_owner_pan,HP_coOwned_per_s,HP_per_share,HP_per_share,HP_ICU_house_p,IncomeOfHP = (0,)*8

ReturnFileSec1,ReturnType1,ReceiptNo1,OrigRetFiledDate1=(0,)*4

company_name,c_add,state_code,c_pincode,c_city,NatureOfEmployment=(0,)*6
c_professionTax,c_entertainment,c_HRA,c_LTA,c_otherAllowance=(0,)*5
c_PerquisitesValue,c_profit_lieu,PerquisitesValue,ProfitsInSalary=(0,)*4
DeductionUs16,sal,ICU_head_sal,calculated_salary_arr,c_salary,c_salary_as_per_provision=(0,)*6

d_name = []
d_address = []
d_city = []
d_state_code = []
d_pin_code = []
d_pan = []
d_amt = []
d_percent = []
total_donation, donation_count = (0,)*2

AggregateIncome=0

def xml_content2(client_id,request,software_id):
    global block_content
    global R_ID
    global CYLA_setoffs
    global TotIncUnderHeadSalaries,NetSalary,standard_deduction,CreationDate
    
    global TotalIncomeChargeableUnHP
    global TotOthSrcNoRaceHorse,TotHPlossCurYr,TotHPlossCurYrSetoff,TotOthSrcLossNoRaceHorseSetoff,Total_CurrentAYloss
    global CYLA_S_IncOfCurYrAfterSetOff,CYLA_HP_IncOfCurYrAfterSetOff
    global IncOfCurYrUnderThatHead,InStcg15Per_CurrYrCapGain,InStcgAppRate_CurrYrCapGain,InLtcg10Per_CurrYrCapGain,InLtcg20Per_CurrYrCapGain
    
    global StclSetoff15Per_global,StclSetoffAppRate_global,LtclSetOff10Per_global,LtclSetOff20Per_global

    global AggregateIncome

    global interest_234A,interest_234B,interest_234C,interest_234F,total_interest

    R_ID = get_rid(client_id)

    global C_ID
    C_ID=client_id

    log.info('==================ITR2 XMl Generation starts====================')

    block_content = '<?xml version="1.0" encoding="ISO-8859-1"?>\n'
    block_content += '<ITRETURN:ITR xmlns:ITRETURN="http://incometaxindiaefiling.gov.in/main" xmlns:ITR2FORM="http://incometaxindiaefiling.gov.in/ITR2" xmlns:ITRForm="http://incometaxindiaefiling.gov.in/master" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'
    block_content += '<ITR2FORM:ITR2>\n'
    now = datetime.now()
    CreationDate = now.strftime("%Y-%m-%d")

    AssessmentYear = 0
    if Return_Details.objects.filter(R_id=R_ID).exists():
        Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
        AssessmentYear = Return_Details_instance.AY
        AssessmentYear = AssessmentYear.split('-')[0]
    # AssessmentYear = now.strftime("%Y")

    global Add_statecode,no_of_bank,no_of_HP,share_equity_sell_value,Tax_Free_Interest,tax_paid_count

    block_content +=CreationInfo(software_id,CreationDate)

    block_content +=Form_ITR2(AssessmentYear)

    global fname, mname, lname, PAN, dob, address, mobile, AadhaarCardNo,capacity,status

    state_list = State_Code.objects.all()

    pi_arr = get_itr_varibles(client_id,'personal_information')
    # for client in Personal_Details.objects.filter(P_Id = client_id):
        # fname = client.name.split('|')[0]
        # mname = client.name.split('|')[1]
        # lname = client.name.split('|')[2]
        # PAN = client.pan
        # temp_date = json_serial(client.dob)
        # dob = temp_date.split('T')[0]
        # mobile = client.mobile
        # AadhaarCardNo = client.aadhar_no or ''

    global citizen_type,age
    fname = pi_arr['fname']
    mname = pi_arr['mname']
    lname = pi_arr['lname']
    PAN = pi_arr['PAN']
    dob =  pi_arr['dob']

    age=calculate_age(client_id)

    if age<60:
        citizen_type='OC'
    elif age>60 and age<=90:
        citizen_type='SC'
    elif age>90:
        citizen_type='SSC'

    mobile = pi_arr['mobile']
    AadhaarCardNo = pi_arr['aadhar_no']

    personal_instance = Personal_Details.objects.get(P_Id=client_id)
    global ResidenceNo, ResidenceName, RoadOrStreet, LocalityOrArea, city, state, PinCode
    global email, Employer_category, residential_status

    address_arr = get_itr_varibles(client_id,'address_detail')
    # for a in Address_Details.objects.filter(p_id=personal_instance):
        # ResidenceNo = a.flat_door_block_no
        # ResidenceName = a.name_of_premises_building
        # RoadOrStreet = a.road_street_postoffice
        # LocalityOrArea = a.area_locality
        # city = a.town_city
        # state = a.state
        # state_code_flag = 0
        # for s in state_list:
        #   if re.search(s.state, a.state, re.IGNORECASE):
        #       state_code_flag = 1
        #       Add_statecode = s.code
        # if state_code_flag == 0:
        #   Add_statecode = 19
        # PinCode = a.pincode

    ResidenceNo = address_arr['ResidenceNo']
    ResidenceName = address_arr['ResidenceName']
    RoadOrStreet = address_arr['RoadOrStreet']
    LocalityOrArea = address_arr['LocalityOrArea']
    city = address_arr['city']
    state = address_arr['state']
    PinCode = address_arr['PinCode']
    Add_statecode = address_arr['Add_statecode']
    Employer_category = address_arr['Employer_category']
    residential_status = address_arr['residential_status']

    for a in SalatTaxUser.objects.filter(id=client_id):
        email = a.email

    global bank_name,IFSC,acc_no,acc_type

    bank_arr = get_itr_varibles(client_id,'bank_detail')
    no_of_bank = bank_arr['no_of_bank']
    bank_name = bank_arr['bank_name']
    IFSC = bank_arr['IFSC']
    acc_no = bank_arr['acc_no']
    acc_type = bank_arr['acc_type']

    pan = PAN
    global sal, ICU_head_sal, r, m, sd, i, ICU_house_p, Interest_Income, Interest_Savings, FD 
    global Other_Income, dividend, ICU_other_s, GTI, TI, Tx
    global Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid
    global total_adv_tax_paid, balance_tax, other_exempt_income
    global salary, PerquisitesValue, ProfitsInSalary, DeductionUs16
    global total_deduction,taxable_income,agri_income, no_of_company

    global HP_type_of_hp,HP_Rent_Received,HP_municipal_tax,HP_add,HP_city,HP_state,HP_pin,HP_Interest
    global HP_Is_coOwned,HP_co_owner_name,HP_co_owner_pan,HP_coOwned_per_s,HP_per_share,HP_per_share,HP_ICU_house_p,IncomeOfHP

    hp_arr = get_itr_varibles(client_id,'hp_detail')
    ICU_house_p = hp_arr['ICU_house_p']
    HP_type_of_hp = hp_arr['HP_type_of_hp']
    HP_Rent_Received = hp_arr['HP_Rent_Received']
    HP_municipal_tax = hp_arr['HP_municipal_tax']
    HP_add = hp_arr['HP_add']
    HP_city = hp_arr['HP_city']
    HP_state = hp_arr['HP_state']
    HP_pin = hp_arr['HP_pin']
    HP_Interest = hp_arr['HP_Interest']
    HP_Is_coOwned = hp_arr['HP_Is_coOwned']
    HP_co_owner_name = hp_arr['HP_co_owner_name']
    HP_co_owner_pan = hp_arr['HP_co_owner_pan']
    HP_coOwned_per_s = hp_arr['HP_coOwned_per_s']
    HP_per_share = hp_arr['HP_per_share']
    HP_ICU_house_p = hp_arr['HP_ICU_house_p']
    IncomeOfHP = hp_arr['IncomeOfHP']

    other_i_arr = get_itr_varibles(client_id,'other_i_common')
    Interest_Savings = other_i_arr['Interest_Savings']
    Other_Income = other_i_arr['Other_Income']
    Interest_Income = other_i_arr['Interest_Income']
    FD = other_i_arr['FD']
    
    exempt_i_arr = get_itr_varibles(client_id,'exempt_i')
    dividend = exempt_i_arr['dividend']
    agri_income = exempt_i_arr['agri_income']
    other_exempt_income = exempt_i_arr['other_exempt_income']
    Tax_Free_Interest = exempt_i_arr['Tax_Free_Interest']

    ICU_other_s = Interest_Income + Interest_Savings + Other_Income

    # Income chargeable under the head Salaries (S)
    # no_of_company = Client_fin_info1.objects.filter(client_id=client_id).exclude(flag=2).count()
    no_of_HP = House_Property_Details.objects.filter(R_Id=R_ID).count()
    log.info('ITR-2 : ')

    global company_name,NatureOfEmployment,c_add,state_code,c_pincode,c_city
    global c_professionTax,c_entertainment,c_HRA,c_LTA,c_otherAllowance
    global c_PerquisitesValue,c_profit_lieu,PerquisitesValue,ProfitsInSalary
    global DeductionUs16,sal,ICU_head_sal,calculated_salary_arr,c_salary,c_salary_as_per_provision

    salary_arr = get_itr_varibles(client_id,'salary')
    no_of_company = salary_arr['no_of_company']
    company_name = salary_arr['company_name']
    NatureOfEmployment=salary_arr['NatureOfEmployment']
    c_add = salary_arr['c_add']
    state_code = salary_arr['state_code']
    c_pincode = salary_arr['c_pincode']
    c_city = salary_arr['c_city']
    c_salary = salary_arr['c_salary']
    c_professionTax = salary_arr['c_professionTax']
    c_entertainment = salary_arr['c_entertainment']
    c_HRA = salary_arr['c_HRA']
    c_LTA = salary_arr['c_LTA']
    c_otherAllowance = salary_arr['c_otherAllowance']
    c_salary_as_per_provision=salary_arr['c_salary_as_per_provision']
    c_PerquisitesValue = salary_arr['c_PerquisitesValue']
    c_profit_lieu = salary_arr['c_profit_lieu']
    PerquisitesValue = salary_arr['PerquisitesValue']
    ProfitsInSalary = salary_arr['ProfitsInSalary']
    DeductionUs16 = salary_arr['DeductionUs16']
    sal = salary_arr['sal']
    ICU_head_sal = salary_arr['ICU_head_sal']
    calculated_salary_arr = salary_arr['calculated_salary_arr']

    global d_80c ,d_80ccc ,d_80ccd ,d_80g ,d_80dd ,d_80u ,d_80d ,d_80e ,d_80tta,d_80ttb ,d_80ddb ,d_80ee
    global d_80gg ,d_80gga ,d_80ggc ,d_80rrb ,d_80qqb ,d_80ccg, d_80ccd1

    deduction_arr = get_itr_varibles(client_id,'deduction')
    d_80c = deduction_arr['d_80c']
    d_80ccc = deduction_arr['d_80ccc']
    d_80ccd = deduction_arr['d_80ccd']
    d_80ccd1 = deduction_arr['d_80ccd1']
    d_80g = deduction_arr['d_80g']
    d_80dd = deduction_arr['d_80dd']
    d_80u = deduction_arr['d_80u']
    d_80d = deduction_arr['d_80d']
    d_80e = deduction_arr['d_80e']
    d_80tta = deduction_arr['d_80tta']
    d_80ttb = deduction_arr['d_80ttb']
    d_80ddb = deduction_arr['d_80ddb']
    d_80ee = deduction_arr['d_80ee']
    d_80gg = deduction_arr['d_80gg']
    d_80gga = deduction_arr['d_80gga']
    d_80ggc = deduction_arr['d_80ggc']
    d_80rrb = deduction_arr['d_80rrb']
    d_80qqb = deduction_arr['d_80qqb']
    d_80ccg = deduction_arr['d_80ccg']

    global ttp ,rebate ,educess ,gtl
    computation_arr = get_itr_varibles(client_id,'computation')
    ttp = computation_arr['ttp']
    rebate = computation_arr['rebate']
    educess = computation_arr['educess']
    gtl = computation_arr['gtl']
    salary = computation_arr['salary']
    GTI = computation_arr['GTI']
    total_deduction = computation_arr['total_deduction']
    taxable_income = computation_arr['taxable_income']
    Tx = computation_arr['Tx']
    Tx1 = computation_arr['Tx1']
    surcharge = computation_arr['surcharge']
    interest_234A= computation_arr['interest_234A']
    interest_234B= computation_arr['interest_234B']
    interest_234C= computation_arr['interest_234C']
    interest_234F= computation_arr['interest_234F']
    total_interest= computation_arr['total_interest']

    global d_name
    global d_address
    global d_city
    global d_state_code
    global d_pin_code
    global d_pan
    global d_amt
    global d_percent
    global total_donation, donation_count
    donation_arr = get_itr_varibles(client_id,'donation')
    d_name = donation_arr['d_name']
    d_address = donation_arr['d_address']
    d_city = donation_arr['d_city']
    d_state_code = donation_arr['d_state_code']
    d_pin_code = donation_arr['d_pin_code']
    d_pan = donation_arr['d_pan']
    d_amt = donation_arr['d_amt']
    d_percent = donation_arr['d_percent']
    total_donation = donation_arr['total_donation']
    donation_count = donation_arr['donation_count']

    # date = datetime(2018,03,31)
    # date = date.replace(tzinfo=None)
    global adv_t, sa_t, tds_value, tcs_value 
    tax_paid_count = 0
    global tp_bsr_code
    global tp_deposit_date
    global tp_challan_no
    global tp_total_tax

    date = datetime(2018,03,31)
    date = date.replace(tzinfo=None)

    tax_paid_arr = get_itr_varibles(client_id,'tax_paid')

    adv_t = tax_paid_arr['adv_t']
    sa_t = tax_paid_arr['sa_t']
    tax_paid_count = tax_paid_arr['tax_paid_count']
    tp_bsr_code = tax_paid_arr['tp_bsr_code']
    tp_deposit_date = tax_paid_arr['tp_deposit_date']
    tp_challan_no = tax_paid_arr['tp_challan_no']
    tp_total_tax = tax_paid_arr['tp_total_tax']

    global tds_count, total_tds1, tcs_count, total_tcs, tds2_count, total_tds2, tds3_count, total_tds3
    global tds_dc_name 
    global tds_amt_paid
    global tds_deducted
    global tds_TAN_PAN
    global tcs_dc_name
    global tcs_amt_paid
    global tcs_deducted
    global tcs_TAN_PAN
    global tds2_dc_name
    global tds2_amt_paid
    global tds2_tds_deducted
    global tds2_TAN_PAN
    global tds2_section

    global tds3_dc_name
    global tds3_amt_paid
    global tds3_tds_deducted
    global tds3_TAN_PAN
    global tds3_section

    tds_tcs_arr = get_itr_varibles(client_id,'tds_tcs')
    tds_count = tds_tcs_arr['tds_count']
    total_tds1 = tds_tcs_arr['total_tds1']
    tcs_count = tds_tcs_arr['tcs_count']
    total_tcs = tds_tcs_arr['total_tcs']
    tds2_count = tds_tcs_arr['tds2_count']
    total_tds2 = tds_tcs_arr['total_tds2']
    tds3_count = tds_tcs_arr['tds3_count']
    total_tds3 = tds_tcs_arr['total_tds3']
    tcs_value = tds_tcs_arr['tcs_value']
    tds_value = tds_tcs_arr['tds_value']
    tds_dc_name = tds_tcs_arr['tds_dc_name']
    tds_amt_paid = tds_tcs_arr['tds_amt_paid']
    tds_deducted = tds_tcs_arr['tds_deducted']
    tds_TAN_PAN = tds_tcs_arr['tds_TAN_PAN']
    tcs_dc_name = tds_tcs_arr['tcs_dc_name']
    tcs_amt_paid = tds_tcs_arr['tcs_amt_paid']
    tcs_deducted = tds_tcs_arr['tcs_deducted']
    tcs_TAN_PAN = tds_tcs_arr['tcs_TAN_PAN']
    tds2_dc_name = tds_tcs_arr['tds2_dc_name']
    tds2_amt_paid = tds_tcs_arr['tds2_amt_paid']
    tds2_tds_deducted = tds_tcs_arr['tds2_tds_deducted']
    tds2_TAN_PAN = tds_tcs_arr['tds2_TAN_PAN']
    tds2_section = tds_tcs_arr['tds2_section']
    tds3_dc_name = tds_tcs_arr['tds3_dc_name']
    tds3_amt_paid = tds_tcs_arr['tds3_amt_paid']
    tds3_tds_deducted = tds_tcs_arr['tds3_tds_deducted']
    tds3_TAN_PAN = tds_tcs_arr['tds3_TAN_PAN']
    tds3_section = tds_tcs_arr['tds3_section']

    TI = float(GTI - d_80c - d_80d - d_80e - d_80tta)

    capital_gain_arr = get_itr_varibles(client_id,'capital_gain')
    global st_shares_tpv, st_shares_tsv, st_shares_gain
    global lt_shares_tpv, lt_shares_tsv, lt_shares_gain,lt_shares_FMV
    global st_equity_tpv, st_equity_tsv, st_equity_gain
    global lt_equity_tpv, lt_equity_tsv, lt_equity_gain,lt_equity_FMV
    global st_debt_MF_tpv, st_debt_MF_tsv, st_debt_MF_gain
    global lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain
    global st_listed_d_tpv, st_listed_d_tsv, st_listed_d_gain
    global lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain

    st_shares_tpv = capital_gain_arr['st_shares_tpv']
    st_shares_tsv = capital_gain_arr['st_shares_tsv']
    st_shares_gain = capital_gain_arr['st_shares_gain']
    lt_shares_tpv = capital_gain_arr['lt_shares_tpv']
    lt_shares_tsv = capital_gain_arr['lt_shares_tsv']
    lt_shares_gain = capital_gain_arr['lt_shares_gain']
    lt_shares_FMV = capital_gain_arr['lt_shares_FMV']
    st_equity_tpv = capital_gain_arr['st_equity_tpv']
    st_equity_tsv = capital_gain_arr['st_equity_tsv']
    st_equity_gain = capital_gain_arr['st_equity_gain']
    lt_equity_tpv = capital_gain_arr['lt_equity_tpv']
    lt_equity_tsv = capital_gain_arr['lt_equity_tsv']
    lt_equity_gain = capital_gain_arr['lt_equity_gain']
    lt_equity_FMV = capital_gain_arr['lt_equity_FMV']
    st_debt_MF_tpv = capital_gain_arr['st_debt_MF_tpv']
    st_debt_MF_tsv = capital_gain_arr['st_debt_MF_tsv']
    st_debt_MF_gain = capital_gain_arr['st_debt_MF_gain']
    lt_debt_MF_tpv = capital_gain_arr['lt_debt_MF_tpv']
    lt_debt_MF_tsv = capital_gain_arr['lt_debt_MF_tsv']
    lt_debt_MF_gain = capital_gain_arr['lt_debt_MF_gain']
    st_listed_d_tpv = capital_gain_arr['st_listed_d_tpv']
    st_listed_d_tsv = capital_gain_arr['st_listed_d_tsv']
    st_listed_d_gain = capital_gain_arr['st_listed_d_gain']
    lt_listed_d_tpv = capital_gain_arr['lt_listed_d_tpv']
    lt_listed_d_tsv = capital_gain_arr['lt_listed_d_tsv']
    lt_listed_d_gain = capital_gain_arr['lt_listed_d_gain']

    global CG_share_equity_LT,total_CG

    CG_share_equity_LT = lt_shares_gain + lt_equity_gain
    total_CG = st_shares_gain + lt_shares_gain + st_equity_gain + lt_equity_gain
    total_CG += st_debt_MF_gain + lt_debt_MF_gain + st_listed_d_gain + lt_listed_d_gain

    global share_equity_cost_acq,share_equity_sell_value,share_equity_gain

    share_equity_cost_acq = st_shares_tpv + st_equity_tpv
    share_equity_sell_value = st_shares_tsv + st_equity_tsv
    share_equity_gain = st_shares_gain + st_equity_gain

    global ReturnFileSec1,ReturnType1,ReceiptNo1,OrigRetFiledDate1

    return_detail_arr = get_itr_varibles(client_id,'return_detail')
    ReturnFileSec1 = return_detail_arr['ReturnFileSec1']
    ReturnType1 = return_detail_arr['ReturnType1']
    ReceiptNo1 = return_detail_arr['ReceiptNo1']
    OrigRetFiledDate1 = return_detail_arr['OrigRetFiledDate1']

    status = 'I'
    capacity = 'S'
    if PAN[3] == 'P':
        status == 'I'
        capacity = 'S'
    if PAN[3] == 'H':
        status == 'H'
        capacity = 'K'


    # PartA_GEN1_param={
    #   'fname':fname,
    #   'mname':mname,
    #   'lname':lname,
    #   'PAN':PAN,
    #   'ResidenceNo':ResidenceNo,
    #   'ResidenceName':ResidenceName,
    #   'RoadOrStreet':RoadOrStreet,
    #   'LocalityOrArea':LocalityOrArea,
    #   'city':city,
    #   'Add_statecode':Add_statecode,
    #   'PinCode':PinCode,
    #   'mobile':mobile,
    #   'dob':dob,
    #   'Employer_category':Employer_category,
    #   'status':status,
    #   'AadhaarCardNo':AadhaarCardNo,
    #   'ReturnFileSec1':ReturnFileSec1,
    #   'ReceiptNo1':ReceiptNo1,
    #   'OrigRetFiledDate1':OrigRetFiledDate1,
    #   'residential_status':residential_status

    # }

    block_content += PartA_GEN1()

    if no_of_bank!=0:
        block_content += ScheduleS()

    if no_of_HP!=0:
        block_content += ScheduleHP()
    else:
        block_content += NoHP()

    log.info('ITR - 2 : ')

    block_content += ScheduleCGFor23()

    block_content += ScheduleOS()

    block_content += ScheduleCYLA()

    block_content += ScheduleBFLA()

    block_content += ScheduleCFL()

    block_content += ScheduleVIA()

    block_content += Schedule80G()

    block_content += ScheduleAMT()

    block_content += ScheduleSI()

    block_content += ScheduleEI()

    block_content += PartB_TI()

    block_content += ScheduleIT()

    block_content += ScheduleTDS1()

    block_content += ScheduleTDS2()

    block_content += ScheduleTCS()

    block_content += ScheduleTDS3()
    
    block_content += Verification()

    block_content += '</ITR2FORM:ITR2>\n'
    block_content += '</ITRETURN:ITR>\n'

    # log.info(block_content)
    
    return block_content

def CreationInfo(software_id,CreationDate):
    content=''
    log.info('===== CreationInfo Block starts =====')
    try:
        content += '<ITRForm:CreationInfo>\n'
        # content += '<ITRForm:SWVersionNo>907</ITRForm:SWVersionNo>\n'
        content += '<ITRForm:SWVersionNo>R1</ITRForm:SWVersionNo>\n'
        # content += '<ITRForm:SWCreatedBy>SW10001530</ITRForm:SWCreatedBy>\n'
        content += '<ITRForm:SWCreatedBy>'+software_id+'</ITRForm:SWCreatedBy>\n'
        content += '<ITRForm:XMLCreatedBy>'+software_id+'</ITRForm:XMLCreatedBy>\n'
        content += '<ITRForm:XMLCreationDate>'+CreationDate+'</ITRForm:XMLCreationDate>\n'
        content += '<ITRForm:IntermediaryCity>Delhi</ITRForm:IntermediaryCity>\n'
        content += '<ITRForm:Digest>-</ITRForm:Digest>\n'
        content += '</ITRForm:CreationInfo>\n'
    except Exception as ex:
        log.info('Error in createtioninfo ITR2 '+traceback.format_exc())
    log.info('===== CreationInfo Block ends =====')
    return content

def Form_ITR2(AssessmentYear):
    content=''
    log.info('===== Form_ITR2 Block starts =====')
    try:
        content += '<ITRForm:Form_ITR2>\n'
        content += '<ITRForm:FormName>ITR-2</ITRForm:FormName>\n'
        # content += '<ITRForm:Description>For Indls having Income from Salary, Pension, family pension and Interest</ITRForm:Description>\n'
        content += '<ITRForm:Description>For Individuals and HUFs not having income from profits and gains of business or profession</ITRForm:Description>\n'
        content += '<ITRForm:AssessmentYear>'+AssessmentYear+'</ITRForm:AssessmentYear>\n'
        content += '<ITRForm:SchemaVer>Ver1.0</ITRForm:SchemaVer>\n'
        content += '<ITRForm:FormVer>Ver1.0</ITRForm:FormVer>\n'
        content += '</ITRForm:Form_ITR2>\n'
    except Exception as ex:
        log.info('Error in Form_ITR2 ITR2 '+traceback.format_exc())
    log.info('===== Form_ITR2 Block ends =====')
    return content


def PartA_GEN1():
    content=''
    log.info('===== PartA_GEN1 Block starts =====')
    try:
        content += '<ITRForm:PartA_GEN1>\n'
        content += '<ITRForm:PersonalInfo>\n'
        content += '<ITRForm:AssesseeName>\n'
        content += '<ITRForm:FirstName>'+fname+'</ITRForm:FirstName>\n'
        if mname != '':
            content += '<ITRForm:MiddleName>'+mname+'</ITRForm:MiddleName>\n'
        content += '<ITRForm:SurNameOrOrgName>'+lname+'</ITRForm:SurNameOrOrgName>\n'
        content += '</ITRForm:AssesseeName>\n'
        content += '<ITRForm:PAN>'+PAN+'</ITRForm:PAN>\n'
        content += '<ITRForm:Address>\n'
        content += '<ITRForm:ResidenceNo>'+ResidenceNo+'</ITRForm:ResidenceNo>\n'
        content += '<ITRForm:ResidenceName>'+ResidenceName+'</ITRForm:ResidenceName>\n'
        if RoadOrStreet!='':
            content += '<ITRForm:RoadOrStreet>'+RoadOrStreet+'</ITRForm:RoadOrStreet>\n'
        else:
            content += '<ITRForm:RoadOrStreet/>\n'
        content += '<ITRForm:LocalityOrArea>'+LocalityOrArea+'</ITRForm:LocalityOrArea>\n'
        content += '<ITRForm:CityOrTownOrDistrict>'+city+'</ITRForm:CityOrTownOrDistrict>\n'
        content += '<ITRForm:StateCode>'+str(Add_statecode)+'</ITRForm:StateCode>\n'
        content += '<ITRForm:CountryCode>91</ITRForm:CountryCode>\n'
        content += '<ITRForm:PinCode>'+PinCode+'</ITRForm:PinCode>\n'
        content += '<ITRForm:CountryCodeMobile>91</ITRForm:CountryCodeMobile>\n'
        content += '<ITRForm:MobileNo>'+mobile+'</ITRForm:MobileNo>\n'
        content += '<ITRForm:EmailAddress>'+email+'</ITRForm:EmailAddress>\n'
        # content += '<ITRForm:EmailAddress>fabienboillon@gmail.com</ITRForm:EmailAddress>\n'

        # content += '<ITRForm:Phone>\n'
        # content += '<ITRForm:STDcode>91</ITRForm:STDcode>\n'
        # content += '<ITRForm:PhoneNo>'+mobile+'</ITRForm:PhoneNo>\n'
        # content += '</ITRForm:Phone>\n'
        content += '</ITRForm:Address>\n'
        content += '<ITRForm:DOB>'+dob+'</ITRForm:DOB>\n'
        # if Employer_category=='Other' or Employer_category=='' :
        #   content += '<ITRForm:EmployerCategory>OTH</ITRForm:EmployerCategory>\n'
        # else:
        #   content += '<ITRForm:EmployerCategory>'+Employer_category+'</ITRForm:EmployerCategory>\n'
        # change : (done)
        content += '<ITRForm:Status>'+status+'</ITRForm:Status>\n'
        if len(AadhaarCardNo)>5:
            content += '<ITRForm:AadhaarCardNo>'+str(AadhaarCardNo)+'</ITRForm:AadhaarCardNo>\n'
        content += '</ITRForm:PersonalInfo>\n'
        content += '<ITRForm:FilingStatus>\n'
        # content += '<ITRForm:DesigOfficerWardorCircle>WARD 4 (4), PUNE</ITRForm:DesigOfficerWardorCircle>\n'
        
        # only for revised return filing
        ReturnFileSec1=ReturnFileSec()
        content += '<ITRForm:ReturnFileSec>'+ReturnFileSec1+'</ITRForm:ReturnFileSec>\n'
        
        if ReturnFileSec1=='17':
            # content += '<ITRForm:ReturnType>'+ReturnType1+'</ITRForm:ReturnType>\n'
            content += '<ITRForm:ReceiptNo>'+ReceiptNo1+'</ITRForm:ReceiptNo>\n'
            content += '<ITRForm:OrigRetFiledDate>'+OrigRetFiledDate1+'</ITRForm:OrigRetFiledDate>\n'
        
        if 1==0:    
            content += '<ITRForm:NoticeDateUnderSec>'+residential_status+'</ITRForm:NoticeDateUnderSec>\n'  
        
        content += '<ITRForm:ResidentialStatus>'+residential_status+'</ITRForm:ResidentialStatus>\n'
        content += '<ITRForm:ConditionsResStatus>1</ITRForm:ConditionsResStatus>\n'
        content += '<ITRForm:BenefitUs115HFlg>N</ITRForm:BenefitUs115HFlg>\n'
        # content += '<ITRForm:NotifiedUs94AFlg>N</ITRForm:NotifiedUs94AFlg>\n'
        # # change :
        # # TR - Tax Refundable, TP - Tax Payable, NT - Nil Tax Balance
        # if ( Tx+Tx1 + surcharge + educess ) - (tds_value+tcs_value+adv_t+sa_t) <0: 
        #   content += '<ITRForm:TaxStatus>TR</ITRForm:TaxStatus>\n'
        # else:
        #   content += '<ITRForm:TaxStatus>NT</ITRForm:TaxStatus>\n'

        content += '<ITRForm:AsseseeRepFlg>N</ITRForm:AsseseeRepFlg>\n'
        content += '<ITRForm:PortugeseCC5A>N</ITRForm:PortugeseCC5A>\n'
        content += '<ITRForm:CompDirectorPrvYrFlg>N</ITRForm:CompDirectorPrvYrFlg>\n'
        content += '<ITRForm:HeldUnlistedEqShrPrYrFlg>N</ITRForm:HeldUnlistedEqShrPrYrFlg>\n'
        content += '</ITRForm:FilingStatus>\n'
        content += '</ITRForm:PartA_GEN1>\n'

    except Exception as ex:
        log.info('Error in PartA_GEN1 ITR2 '+traceback.format_exc())
    log.info('===== PartA_GEN1 Block ends =====')
    return content

def PartB_TI():
    content=''
    log.info('===== PartB_TI Block starts =====')
    try:
        content += '<ITRForm:PartB-TI>\n'

        global AMT_common
        response = requests.post(
            salat_url+'/get_CG_income/',
            data={'client_id': C_ID},
        )

        CG_special_rates=response.json()
        #salary_calculation
        log.info('ICU_head_sal '+str(ICU_head_sal))
        log.info('sum(c_otherAllowance) '+str(sum(c_otherAllowance)))

        # TotalGrossSalary-AllwncExtentExemptUs10
        NetSalary=int(sum(calculated_salary_arr))

        log.info('NetSalary '+str(NetSalary))

        # DeductionUs16ia+Profession Tax + Enterta
        DeductionUs16ia=40000.0
        log.info('Standard Deductions '+str(DeductionUs16ia))
        log.info('Profession Tax '+str(sum(c_professionTax)))
        log.info('Entertainment '+str(sum(c_entertainment)))
        DeductionUS16=DeductionUs16ia+sum(c_professionTax)+sum(c_entertainment)

        log.info('DeductionUS16 '+str(DeductionUS16))
        TotIncUnderHeadSalaries=NetSalary-DeductionUS16

        if ICU_head_sal>0:
            content += '<ITRForm:Salaries>'+str(int(TotIncUnderHeadSalaries))+'</ITRForm:Salaries>\n'
        else:
            content += '<ITRForm:Salaries>0</ITRForm:Salaries>\n'

        if ICU_house_p>0:
            content += '<ITRForm:IncomeFromHP>'+str(int(ICU_house_p) )+'</ITRForm:IncomeFromHP>\n'
        else:
            content += '<ITRForm:IncomeFromHP>0</ITRForm:IncomeFromHP>\n'
        
        content += '<ITRForm:CapGain>\n'
        content += '<ITRForm:ShortTerm>\n'
        content += '<ITRForm:ShortTerm15Per>'+str(int(st_shares_gain+st_equity_gain))+'</ITRForm:ShortTerm15Per>\n'
        content += '<ITRForm:ShortTerm30Per>0</ITRForm:ShortTerm30Per>\n'
        content += '<ITRForm:ShortTermAppRate>'+str(int(st_debt_MF_gain+st_listed_d_gain))+'</ITRForm:ShortTermAppRate>\n'
        content += '<ITRForm:ShortTermSplRateDTAA>0</ITRForm:ShortTermSplRateDTAA>\n'
        content += '<ITRForm:TotalShortTerm>'+str(int(st_shares_gain+st_equity_gain+st_debt_MF_gain+st_listed_d_gain))+'</ITRForm:TotalShortTerm>\n'
        content += '</ITRForm:ShortTerm>\n'
        
        content += '<ITRForm:LongTerm>\n'
        content += '<ITRForm:LongTerm10Per>'+str(int(lt_listed_d_gain+lt_shares_gain))+'</ITRForm:LongTerm10Per>\n'
        content += '<ITRForm:LongTerm20Per>'+str(int( lt_debt_MF_gain ) )+'</ITRForm:LongTerm20Per>\n'
        content += '<ITRForm:LongTermSplRateDTAA>0</ITRForm:LongTermSplRateDTAA>\n'
        content += '<ITRForm:TotalLongTerm>'+str(int( lt_shares_gain+lt_listed_d_gain+lt_debt_MF_gain ) )+'</ITRForm:TotalLongTerm>\n'
        content += '</ITRForm:LongTerm>\n'
        
        content += '<ITRForm:TotalCapGains>'+str(int(st_shares_gain+st_equity_gain+st_debt_MF_gain+st_listed_d_gain + lt_listed_d_gain+lt_debt_MF_gain+lt_shares_gain))+'</ITRForm:TotalCapGains>\n'
        content += '</ITRForm:CapGain>\n'

        content += '<ITRForm:IncFromOS>\n'

        ICU_other_s=int(Interest_Income + Interest_Savings + FD+Other_Income)
        content += '<ITRForm:OtherSrcThanOwnRaceHorse>'+str(ICU_other_s)+'</ITRForm:OtherSrcThanOwnRaceHorse>\n'
        content += '<ITRForm:IncChargblSplRate>0</ITRForm:IncChargblSplRate>\n'
        content += '<ITRForm:FromOwnRaceHorse>0</ITRForm:FromOwnRaceHorse>\n'
        content += '<ITRForm:TotIncFromOS>'+str(ICU_other_s)+'</ITRForm:TotIncFromOS>\n'
        content += '</ITRForm:IncFromOS>\n'

        # if ICU_other_s+Tx1 > 0:
        #   TotIncFromOS = ICU_other_s+Tx1
        # else:
        #   TotIncFromOS = 0
        Total_CG=int(st_shares_gain+st_equity_gain+st_debt_MF_gain+st_listed_d_gain + lt_listed_d_gain+lt_debt_MF_gain+lt_shares_gain)
        
        log.info('Income from Salary '+str(TotIncUnderHeadSalaries))
        log.info('Income from HP '+str(max(0,ICU_house_p)))
        log.info('Income from CG '+str(max(0,Total_CG)))
        log.info('Income from Other '+str(ICU_other_s))
        log.info('Other income '+str(Other_Income))

        xml_TotalTI=int(TotIncUnderHeadSalaries+max(0,ICU_house_p)+max(0,Total_CG)+ ICU_other_s)

        content += '<ITRForm:TotalTI>'+str(xml_TotalTI)+'</ITRForm:TotalTI>\n'
        
        log.info('TOtal TI '+str(xml_TotalTI))

        CurrentYearLoss=abs(min(0,int(ICU_house_p)))

        content += '<ITRForm:CurrentYearLoss>'+str(CurrentYearLoss)+'</ITRForm:CurrentYearLoss>\n'
        
        log.info('CurrentYearLoss '+str(CurrentYearLoss))
        # change : BalanceAfterSetoffLosses = TotalTI-CurrentYearLoss Done
        # content += '<ITRForm:BalanceAfterSetoffLosses>'+str(int(taxable_income) )+'</ITRForm:BalanceAfterSetoffLosses>\n'
        # TotalTI - CurrentYearLoss
        BalanceAfterSetoffLosses=xml_TotalTI-CurrentYearLoss

        content += '<ITRForm:BalanceAfterSetoffLosses>'+str(int(BalanceAfterSetoffLosses) )+'</ITRForm:BalanceAfterSetoffLosses>\n'
        
        log.info('BalanceAfterSetoffLosses '+str(BalanceAfterSetoffLosses))

        BroughtFwdLossesSetoff=0
        content += '<ITRForm:BroughtFwdLossesSetoff>'+str(BroughtFwdLossesSetoff)+'</ITRForm:BroughtFwdLossesSetoff>\n'
        # BalanceAfterSetoffLosses - BroughtFwdLossesSetoff
        GrossTotalIncome=BalanceAfterSetoffLosses- BroughtFwdLossesSetoff
        content += '<ITRForm:GrossTotalIncome>'+str(int(GrossTotalIncome) )+'</ITRForm:GrossTotalIncome>\n'
        
        log.info('GrossTotalIncome '+str(GrossTotalIncome))

        # T866
        # IncChargeableTaxSplRates=st_shares_gain + st_equity_gain +lt_shares_gain + lt_equity_gain + lt_debt_MF_gain + lt_listed_d_gain
        IncChargeableTaxSplRates=CG_special_rates['Income_chargeable_CG_at_special_rates']
        IncChargeTaxSplRate111A112=IncChargeableTaxSplRates
        content += '<ITRForm:IncChargeTaxSplRate111A112>'+str(IncChargeTaxSplRate111A112)+'</ITRForm:IncChargeTaxSplRate111A112>\n'
        
        log.info('IncChargeTaxSplRate111A112 '+str(IncChargeTaxSplRate111A112))
        
        usrdeduction = d_80c+d_80ccc+d_80ccd+d_80d+d_80dd+d_80ddb+d_80e+d_80ee+d_80g+d_80gg+d_80gga+d_80ggc
        usrdeduction += d_80u+d_80rrb+d_80qqb+d_80ccg+d_80tta+d_80ttb

        TotalChapVIADeductions=int(usrdeduction)

        # H796
        DeductionsUnderScheduleVIA=total_deduction
        content += '<ITRForm:DeductionsUnderScheduleVIA>'+str(int(DeductionsUnderScheduleVIA))+'</ITRForm:DeductionsUnderScheduleVIA>\n'
        
        log.info('DeductionsUnderScheduleVIA '+str(DeductionsUnderScheduleVIA))
    
        log.info('TotalChapVIADeductions '+str(TotalChapVIADeductions))
        # GrossTotIncome -TotalChapVIADeductions
        TotalIncome=round((GrossTotalIncome-DeductionsUnderScheduleVIA),-1)
        content += '<ITRForm:TotalIncome>'+str(int(TotalIncome) )+'</ITRForm:TotalIncome>\n'
        
        log.info('TotalIncome '+str(TotalIncome))
        # T 866
        IncChargeableTaxSplRates=IncChargeTaxSplRate111A112
        content += '<ITRForm:IncChargeableTaxSplRates>'+str(IncChargeableTaxSplRates)+'</ITRForm:IncChargeableTaxSplRates>\n'
        
        # H 871
        # NetAgriIncOrOthrIncRule7
        NetAgricultureIncomeOrOtherIncomeForRate=int(agri_income)
        content += '<ITRForm:NetAgricultureIncomeOrOtherIncomeForRate>'+str(NetAgricultureIncomeOrOtherIncomeForRate)+'</ITRForm:NetAgricultureIncomeOrOtherIncomeForRate>\n'
        # content += '<ITRForm:AggregateIncome>'+str(int(salary+agri_income) )+'</ITRForm:AggregateIncome>\n'
        
        # TotalIncome - IncChargeableTaxSplRates + NetAgricultureIncomeOrOtherIncomeForRate
        AggregateIncome=TotalIncome-IncChargeableTaxSplRates+NetAgricultureIncomeOrOtherIncomeForRate
        content += '<ITRForm:AggregateIncome>'+str(int(AggregateIncome))+'</ITRForm:AggregateIncome>\n'
        
        log.info('AggregateIncome '+str(AggregateIncome))

        # H739 + H740+ H741 + H742
        LossesOfCurrentYearCarriedFwd=CG_special_rates['carry_forword_losses']
        content += '<ITRForm:LossesOfCurrentYearCarriedFwd>'+str(LossesOfCurrentYearCarriedFwd)+'</ITRForm:LossesOfCurrentYearCarriedFwd>\n'
        
        log.info('LossesOfCurrentYearCarriedFwd '+str(LossesOfCurrentYearCarriedFwd))
        # TotalIncome
        AMT_common['TotalIncome']=TotalIncome
        content += '<ITRForm:DeemedIncomeUs115JC>'+str(int(TotalIncome))+'</ITRForm:DeemedIncomeUs115JC>\n'
        content += '</ITRForm:PartB-TI>\n'

        log.info('===== PartB_TI Block ends =====')

        content += PartB_TTI(AggregateIncome)
    except Exception as ex:
        log.info('Error in PartB_TI ITR2 '+traceback.format_exc())
    
    return content


def slab_level_calculation(TI):
    log.info('===== slab level calculation starts =====')
    Tx=0
    try:
        if age < 60:
            log.info('Not Senior Citizen')
            if ( TI>1000000):
                Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
            if ( TI>500000 ):
                if TI<1000000:
                    Tx = ( (TI-500000)*0.2 ) + 12500
            if ( TI>250000 ):
                if TI<500000:
                    Tx = ( (TI-250000)*0.05 )
            if ( TI<250000 ):
                Tx = 0
        else:
            log.info('Senior Citizen')
            if ( TI>1000000):
                Tx = ( (TI-1000000)*0.3 ) + 100000 + 10000
            if ( TI>500000 ):
                if TI<1000000:
                    Tx = ( (TI-500000)*0.2 ) + 10000
            if ( TI>300000 ):
                if TI<500000:
                    Tx = ( (TI-300000)*0.05 )
            if ( TI<300000 ):
                Tx = 0
    except Exception as ex:
        log.info('Error in calculate slab level calculation '+traceback.format_exc())
    log.info('Tx '+str(Tx))
    log.info('round Tx '+str(round(Tx)))
    log.info('nearest to round 10 Tx '+str(round(round(Tx),-1)))
    log.info('===== slab level calculation ends =====')
    return round(Tx)

def PartB_TTI(AggregateIncome):

    content=''
    log.info('===== PartB_TTI Block starts =====')
    try:
        content += '<ITRForm:PartB_TTI>\n'

        content += '<ITRForm:TaxPayDeemedTotIncUs115JC>0</ITRForm:TaxPayDeemedTotIncUs115JC>\n'
        content += '<ITRForm:Surcharge>0</ITRForm:Surcharge>\n'
        content += '<ITRForm:HealthEduCess>0</ITRForm:HealthEduCess>\n'
        content += '<ITRForm:TotalTaxPayablDeemedTotInc>0</ITRForm:TotalTaxPayablDeemedTotInc>\n'

        content += '<ITRForm:ComputationOfTaxLiability>\n'
        content += '<ITRForm:TaxPayableOnTI>\n'
        
        log.info('AggregateIncome '+str(AggregateIncome))
        # TaxAtNormalRatesOnAggrInc_func(AggregateIncome)
        TaxAtNormalRatesOnAggrInc=round(slab_level_calculation(AggregateIncome))
        content += '<ITRForm:TaxAtNormalRatesOnAggrInc>'+str(int(TaxAtNormalRatesOnAggrInc) )+'</ITRForm:TaxAtNormalRatesOnAggrInc>\n'
       
        log.info('TaxAtNormalRatesOnAggrInc '+str(TaxAtNormalRatesOnAggrInc))

        content += '<ITRForm:TaxAtSpecialRates>'+str(int(Tx1) )+'</ITRForm:TaxAtSpecialRates>\n'
        # (RebateOnAgriInc -> Assuming 0, Rebate87A -> calculated) (Done)
        
        log.info('TaxAtSpecialRates '+str(int(Tx1)))

        # calculate tax as per slab for (H96 + choose from (2.5 lacs, 3 lac, 5 lac))
        if citizen_type=='OC':
            slab_amount=250000
        elif citizen_type=='SC':
            slab_amount=300000
        elif citizen_type=='SSC':
            slab_amount=500000

        # calculate tax as per slab for (H96 + choose from (2.5 lacs, 3 lac, 5 lac))
        RebateOnAgriInc=0
        content += '<ITRForm:RebateOnAgriInc>'+str(RebateOnAgriInc)+'</ITRForm:RebateOnAgriInc>\n'
        
        log.info('RebateOnAgriInc '+str(int(RebateOnAgriInc)))
        # content += '<ITRForm:TaxPayableOnTotInc>'+str(int(ttp))+'</ITRForm:TaxPayableOnTotInc>\n'
        # change : TaxPayableOnTotInc = TaxAtNormalRatesOnAggrInc + TaxAtSpecialRates - RebateOnAgriInc
        TaxPayableOnTotInc=round(TaxAtNormalRatesOnAggrInc+int(Tx1)+RebateOnAgriInc)
        log.info('TaxPayableOnTotInc '+str(TaxPayableOnTotInc))
        content += '<ITRForm:TaxPayableOnTotInc>'+str(int(TaxPayableOnTotInc))+'</ITRForm:TaxPayableOnTotInc>\n'
        content += '</ITRForm:TaxPayableOnTI>\n'
        
        rebate=0
        if int(AggregateIncome) < 350000 :
            rebate = 2500
        if int(AggregateIncome) <= 300000:
            rebate = Tx

        content += '<ITRForm:Rebate87A>'+str(int(rebate) )+'</ITRForm:Rebate87A>\n'
       
        log.info('Rebate87A '+str(rebate))

        TaxPayableOnRebate=TaxPayableOnTotInc-int(rebate)
        content += '<ITRForm:TaxPayableOnRebate>'+str(int(TaxPayableOnRebate))+'</ITRForm:TaxPayableOnRebate>\n'
        
        log.info('TaxPayableOnRebate '+str(TaxPayableOnRebate))

        content += '<ITRForm:Surcharge25ofSI>0</ITRForm:Surcharge25ofSI>\n'

        # Surcharge(SH)
        surcharge=0
        if AggregateIncome>5000000 and TI<10000000:
            surcharge = round((Tx + Tx1)*0.1)
        if AggregateIncome>10000000:
            surcharge = round((Tx + Tx1)*0.15)

        content += '<ITRForm:SurchargeOnAboveCrore>'+str(int(surcharge) )+'</ITRForm:SurchargeOnAboveCrore>\n'
        content += '<ITRForm:TotalSurcharge>'+str(int(surcharge) )+'</ITRForm:TotalSurcharge>\n'
       
        edu_cess_per = 0.04
        educess = round((TaxAtNormalRatesOnAggrInc + Tx1 - rebate)*edu_cess_per)
        content += '<ITRForm:EducationCess>'+str(int(educess) )+'</ITRForm:EducationCess>\n'
       
        log.info('EducationCess '+str(educess))

        # content += '<ITRForm:GrossTaxLiability>'+str(int(ttp+rebate+educess) )+'</ITRForm:GrossTaxLiability>\n'
        # TotalTaxPayable - Rebate87A + EducationCes + TotalSurcharge
        GrossTaxLiability=(TaxAtNormalRatesOnAggrInc+Tx1-rebate+surcharge+educess)

        log.info('GrossTaxLiability '+str(GrossTaxLiability))

        content += '<ITRForm:GrossTaxLiability>'+str(int(GrossTaxLiability))+'</ITRForm:GrossTaxLiability>\n'
        content += '<ITRForm:GrossTaxPayable>'+str(int(GrossTaxLiability))+'</ITRForm:GrossTaxPayable>\n'

        content += '<ITRForm:CreditUS115JD>0</ITRForm:CreditUS115JD>\n'
        # content += '<ITRForm:GrossTaxPayable>'+str(int( Tx+Tx1 + surcharge + educess ) )+'</ITRForm:GrossTaxPayable>\n'
        content += '<ITRForm:TaxPayAfterCreditUs115JD>'+str(int(GrossTaxLiability))+'</ITRForm:TaxPayAfterCreditUs115JD>\n'
        content += '<ITRForm:TaxRelief>\n'
        content += '<ITRForm:Section89>0</ITRForm:Section89>\n'
        content += '<ITRForm:Section90>0</ITRForm:Section90>\n'
        content += '<ITRForm:Section91>0</ITRForm:Section91>\n'
        content += '<ITRForm:TotTaxRelief>0</ITRForm:TotTaxRelief>\n'
        content += '</ITRForm:TaxRelief>\n'
        # content += '<ITRForm:NetTaxLiability>'+str(int((ttp+rebate+educess)-0) )+'</ITRForm:NetTaxLiability>\n'
        # GrossTaxLiability - TotTaxRelief
        AMT_common['GrossTaxLiability']=GrossTaxLiability
        NetTaxLiability=GrossTaxLiability
        content += '<ITRForm:NetTaxLiability>'+str(int(round(NetTaxLiability)))+'</ITRForm:NetTaxLiability>\n'
        # 0 is Section89

        log.info('interest_234A '+str(interest_234A))
        log.info('interest_234B '+str(interest_234B))
        log.info('interest_234C '+str(interest_234C))
        log.info('interest_234F '+str(interest_234F))
        log.info('total_interest '+str(total_interest))

        content += '<ITRForm:IntrstPay>\n'
        content += '<ITRForm:IntrstPayUs234A>'+str(int(interest_234A))+'</ITRForm:IntrstPayUs234A>\n'
        content += '<ITRForm:IntrstPayUs234B>'+str(int(interest_234B))+'</ITRForm:IntrstPayUs234B>\n'
        content += '<ITRForm:IntrstPayUs234C>'+str(int(interest_234C))+'</ITRForm:IntrstPayUs234C>\n'
        content += '<ITRForm:LateFilingFee234F>'+str(int(interest_234F))+'</ITRForm:LateFilingFee234F>\n'
        content += '<ITRForm:TotalIntrstPay>'+str(int(total_interest))+'</ITRForm:TotalIntrstPay>\n'
        content += '</ITRForm:IntrstPay>\n'


        AggregateTaxInterestLiability=NetTaxLiability+total_interest
        content += '<ITRForm:AggregateTaxInterestLiability>'+str(int(round(AggregateTaxInterestLiability)) + 0)+'</ITRForm:AggregateTaxInterestLiability>\n'
        content += '</ITRForm:ComputationOfTaxLiability>\n'
        

        # tax paid
        content += '<ITRForm:TaxPaid>\n'
        content += '<ITRForm:TaxesPaid>\n'
        content += '<ITRForm:AdvanceTax>'+str(int(adv_t) )+'</ITRForm:AdvanceTax>\n'
        content += '<ITRForm:TDS>'+str(int(tds_value) )+'</ITRForm:TDS>\n'
        content += '<ITRForm:TCS>'+str(int(tcs_value) )+'</ITRForm:TCS>\n'
        content += '<ITRForm:SelfAssessmentTax>'+str(int(sa_t) )+'</ITRForm:SelfAssessmentTax>\n'
        content += '<ITRForm:TotalTaxesPaid>'+str(int(tds_value+tcs_value+adv_t+sa_t) )+'</ITRForm:TotalTaxesPaid>\n'
        content += '</ITRForm:TaxesPaid>\n'
        # if (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t)>0:
        if ( Tx+Tx1 -rebate+  surcharge + educess ) - (tds_value+tcs_value+adv_t+sa_t) >0:
            content += '<ITRForm:BalTaxPayable>'+str(int( ( Tx+Tx1 -rebate+ surcharge + educess ) - (tds_value+tcs_value+adv_t+sa_t) ))+'</ITRForm:BalTaxPayable>\n'
            # content += '<ITRForm:BalTaxPayable>'+str(int( (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t) ))+'</ITRForm:BalTaxPayable>\n'
        else:
            content += '<ITRForm:BalTaxPayable>0</ITRForm:BalTaxPayable>\n'
        # if(TotTaxPlusIntrstPay - TotalTaxesPaid >0, TotTaxPlusIntrstPay - TotalTaxesPaid, 0)
        content += '</ITRForm:TaxPaid>\n'
        
        log.info('refun due '+str(( Tx+Tx1-rebate + surcharge + educess+total_interest ) - (tds_value+tcs_value+adv_t+sa_t)))
        # refund
        content += '<ITRForm:Refund>\n'
        # if (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t)<0:
        if ( Tx+Tx1-rebate + surcharge + educess+total_interest ) - (tds_value+tcs_value+adv_t+sa_t) < 0:
            refund_due=abs(  ( Tx+Tx1-rebate + surcharge + educess+total_interest ) - (tds_value+tcs_value+adv_t+sa_t)  )
            content += '<ITRForm:RefundDue>'+str(int(round(round(refund_due),-1)))+'</ITRForm:RefundDue>\n'
        else:
            content += '<ITRForm:RefundDue>0</ITRForm:RefundDue>\n'
        content += '<ITRForm:BankAccountDtls>\n'
        if no_of_bank >= 1:
            # content += '<ITRForm:BankDtlsFlag>Y</ITRForm:BankDtlsFlag>\n'
            # content += '<ITRForm:PriBankDetails>\n'
            # # for x in xrange(0,no_of_bank):
            # # content += '<ITRForm:IFSCCode>'+str(IFSC[0])+'</ITRForm:IFSCCode>\n'
            # content += '<ITRForm:IFSCCode>'+str(bank_name[0])+'</ITRForm:IFSCCode>\n'
            # # content += '<ITRForm:BankName>'+str(bank_name[0])+'</ITRForm:BankName>\n'
            # content += '<ITRForm:BankName>'+str(IFSC[0])+'</ITRForm:BankName>\n'
            # content += '<ITRForm:BankAccountNo>'+str(acc_no[0])+'</ITRForm:BankAccountNo>\n'
            # content += '</ITRForm:PriBankDetails>\n'
            for x in xrange(0,no_of_bank):
                content += '<ITRForm:AddtnlBankDetails>\n'
                content += '<ITRForm:IFSCCode>'+str(IFSC[x])+'</ITRForm:IFSCCode>\n'
                content += '<ITRForm:BankName>'+str(bank_name[x])+'</ITRForm:BankName>\n'
                content += '<ITRForm:BankAccountNo>'+str(acc_no[x])+'</ITRForm:BankAccountNo>\n'
                if x==0:
                    content += '<ITRForm:UseForRefund>true</ITRForm:UseForRefund>\n'
                else:
                    content += '<ITRForm:UseForRefund>false</ITRForm:UseForRefund>\n'
                content += '</ITRForm:AddtnlBankDetails>\n'
        else:
            content += '<ITRForm:BankDtlsFlag>N</ITRForm:BankDtlsFlag>\n'
            content += '<ITRForm:PriBankDetails>\n'
            content += '<ITRForm:IFSCCode></ITRForm:IFSCCode>\n'
            content += '<ITRForm:BankName></ITRForm:BankName>\n'
            content += '<ITRForm:BankAccountNo></ITRForm:BankAccountNo>\n'
            content += '</ITRForm:PriBankDetails>\n'
        content += '</ITRForm:BankAccountDtls>\n'
        content += '</ITRForm:Refund>\n'
        content += '<ITRForm:AssetOutIndiaFlag>NO</ITRForm:AssetOutIndiaFlag>\n'
        content += '</ITRForm:PartB_TTI>\n'

    except Exception as ex:
        log.info('Error in PartB_TTI ITR2 '+traceback.format_exc())
    log.info('===== PartB_TTI Block ends =====')
    return content

def TaxAtNormalRatesOnAggrInc_func(AggregateIncome):
    TaxAtNormalRatesOnAggrInc_amount=0
    try:
        log.info(AggregateIncome)
    except Exception as ex:
        log.info('Error in TaxAtNormalRatesOnAggrInc_func ITR2 '+traceback.format_exc())
    return TaxAtNormalRatesOnAggrInc_amount


def Verification():
    content=''
    log.info('===== Verification Block starts =====')
    try:
        content += '<ITRForm:Verification>\n'
        content += '<ITRForm:Declaration>\n'
        content += '<ITRForm:AssesseeVerName>'+fname+' '+lname+'</ITRForm:AssesseeVerName>\n'
        if mname =='':
            content += '<ITRForm:FatherName>'+lname+'</ITRForm:FatherName>\n'
        else:
            content += '<ITRForm:FatherName>'+mname+'</ITRForm:FatherName>\n'
        content += '<ITRForm:AssesseeVerPAN>'+PAN+'</ITRForm:AssesseeVerPAN>\n'
        content += '</ITRForm:Declaration>\n'
        # change : capacity_tab (Done)
        content += '<ITRForm:Capacity>'+capacity+'</ITRForm:Capacity>\n'
        # content += '<ITRForm:Place>'+city+'</ITRForm:Place>\n'
        # content += '<ITRForm:Date>'+CreationDate+'</ITRForm:Date>\n'
        content += '</ITRForm:Verification>\n'

    except Exception as ex:
        log.info('Error in Verification ITR2 '+traceback.format_exc())
    log.info('===== Verification Block ends =====')
    return content

def ScheduleS():
    content=''
    log.info('===== ScheduleS Block starts =====')
    try:
        TotalGrossSalary=0
        content += '<ITRForm:ScheduleS>\n'
        for x in xrange(0,no_of_company):
            content += '<ITRForm:Salaries>\n'
            content += '<ITRForm:NameOfEmployer>'+company_name[x]+'</ITRForm:NameOfEmployer>\n'
            content += '<ITRForm:NatureOfEmployment>'+NatureOfEmployment_func(NatureOfEmployment[x])+'</ITRForm:NatureOfEmployment>\n'
            content += '<ITRForm:AddressDetail>\n'
            content += '<ITRForm:AddrDetail>'+c_add[x]+'</ITRForm:AddrDetail>\n'
            content += '<ITRForm:CityOrTownOrDistrict>'+c_city[x]+'</ITRForm:CityOrTownOrDistrict>\n'
            content += '<ITRForm:StateCode>'+str(state_code[x])+'</ITRForm:StateCode>\n'
            content += '<ITRForm:PinCode>'+str(c_pincode[x])+'</ITRForm:PinCode>\n'
            # added on 9 Aug
            # content += '<ITRForm:ZipCode>'+str(c_pincode[x])+'</ITRForm:ZipCode>\n'
            content += '</ITRForm:AddressDetail>\n'
            content += '<ITRForm:Salarys>\n'
            # change :  take calculated salary (done)
            GrossSalary=GrossSalary_func(c_salary_as_per_provision[x],c_PerquisitesValue[x],c_profit_lieu[x])
            TotalGrossSalary += GrossSalary

            content += '<ITRForm:GrossSalary>'+str(GrossSalary)+'</ITRForm:GrossSalary>\n'
            content += '<ITRForm:Salary>'+str(int(c_salary_as_per_provision[x]))+'</ITRForm:Salary>\n'
            
            # # salary breakup
            salary_breakup=get_salary_breakup(company_name[x])
            # log.info(salary_breakup)
            content += '<ITRForm:NatureOfSalary>\n'
            ignore_variable=['R_Id','company_name']
            unusable_value=['null',0,None]
            for breakup_data in salary_breakup:
                if breakup_data not in ignore_variable and salary_breakup[breakup_data] not in unusable_value:
                    SBNatureDesc=SalaryBreakupNatureDesc(breakup_data)
                    content += '<ITRForm:OthersIncDtls>\n'
                    content += '<ITRForm:NatureDesc>'+SBNatureDesc+'</ITRForm:NatureDesc>\n'
                    if SBNatureDesc=='OTH':
                        content += '<ITRForm:OthNatOfInc>OTHERS</ITRForm:OthNatOfInc>\n'
                    SBOthAmount=check_null(salary_breakup[breakup_data])
                    content += '<ITRForm:OthAmount>'+SBOthAmount+'</ITRForm:OthAmount>\n'
                    content += '</ITRForm:OthersIncDtls>\n'

            if int(c_HRA[x])!=0:
                content += '<ITRForm:OthersIncDtls>\n'
                content += '<ITRForm:NatureDesc>4</ITRForm:NatureDesc>\n'
                content += '<ITRForm:OthAmount>'+str(int(c_HRA[x]))+'</ITRForm:OthAmount>\n'
                content += '</ITRForm:OthersIncDtls>\n'
            # salary breakup LTA
            if int(c_LTA[x])!=0:
                content += '<ITRForm:OthersIncDtls>\n'
                content += '<ITRForm:NatureDesc>5</ITRForm:NatureDesc>\n'
                content += '<ITRForm:OthAmount>'+str(int(c_LTA[x]))+'</ITRForm:OthAmount>\n'
                content += '</ITRForm:OthersIncDtls>\n'

            content += '</ITRForm:NatureOfSalary>\n'

            content += '<ITRForm:ValueOfPerquisites>'+str(int(c_PerquisitesValue[x]) )+'</ITRForm:ValueOfPerquisites>\n'
            # perqusite breakup
            perquisites_breakup=get_perquisites_breakup(company_name[x])
            # log.info(perquisites_breakup)

            if int(c_PerquisitesValue[x])!=0 and perquisites_breakup:
                content += '<ITRForm:NatureOfPerquisites>\n'
                for breakup_data in perquisites_breakup:
                    if breakup_data not in ignore_variable and perquisites_breakup[breakup_data] not in unusable_value:
                        PBNatureDesc=PerquisitesBreakupNatureDesc(breakup_data)
                        content += '<ITRForm:OthersIncDtls>\n'
                        content += '<ITRForm:NatureDesc>'+PBNatureDesc+'</ITRForm:NatureDesc>\n'
                        if PBNatureDesc=='OTH':
                            content += '<ITRForm:OthNatOfInc>OTHERS</ITRForm:OthNatOfInc>\n'
                        PBOthAmount=check_null(perquisites_breakup[breakup_data])
                        content += '<ITRForm:OthAmount>'+PBOthAmount+'</ITRForm:OthAmount>\n'
                        content += '</ITRForm:OthersIncDtls>\n'
                content += '</ITRForm:NatureOfPerquisites>\n'

            
            content += '<ITRForm:ProfitsinLieuOfSalary>'+str(int(c_profit_lieu[x]) )+'</ITRForm:ProfitsinLieuOfSalary>\n'
            # profit in lieu breakup
            profit_lieu_breakup=get_profit_lieu_breakup(company_name[x])
            # log.info(profit_lieu_breakup)

            if int(c_profit_lieu[x])!=0 and profit_lieu_breakup:
                content += '<ITRForm:NatureOfProfitInLieuOfSalary>\n'
                for breakup_data in profit_lieu_breakup:
                    if breakup_data not in ignore_variable and profit_lieu_breakup[breakup_data] not in unusable_value:
                        PLBNatureDesc=ProfitLieuBreakupNatureDesc(breakup_data)
                        content += '<ITRForm:OthersIncDtls>\n'
                        content += '<ITRForm:NatureDesc>'+PLBNatureDesc+'</ITRForm:NatureDesc>\n'
                        if PLBNatureDesc=='OTH':
                            content += '<ITRForm:OthNatOfInc>OTHERS</ITRForm:OthNatOfInc>\n'
                        PLBOthAmount=check_null(profit_lieu_breakup[breakup_data])
                        content += '<ITRForm:OthAmount>'+PLBOthAmount+'</ITRForm:OthAmount>\n'
                        content += '</ITRForm:OthersIncDtls>\n'
                content += '</ITRForm:NatureOfProfitInLieuOfSalary>\n'
            
            
            content += '</ITRForm:Salarys>\n'
            content += '</ITRForm:Salaries>\n'

        content += '<ITRForm:TotalGrossSalary>'+str(TotalGrossSalary)+'</ITRForm:TotalGrossSalary>\n'
        
        content += '<ITRForm:AllwncExtentExemptUs10>'+str(int(sum(c_otherAllowance)))+'</ITRForm:AllwncExtentExemptUs10>\n'
        

        total_allowances_breakup=get_total_allowances_breakup()
        # log.info(total_allowances_breakup)

        other_allowance=[]
        ELE_allowance=[]
        LTA_allowance=[]
        HRA_allowance=[]
        content += '<ITRForm:AllwncExemptUs10>\n'
        for data in total_allowances_breakup:
            other_allowance.append(int(data['other_allowance']))
            ELE_allowance.append(int(data['earned_leave_encashment']))
            LTA_allowance.append(int(data['lta']))
            HRA_allowance.append(int(data['hra']))
        
        log.info('HRA '+str(HRA_allowance))
        log.info('LTA '+str(LTA_allowance))
        log.info('Earned leave encashment '+str(ELE_allowance))
        log.info('other_allowance '+str(other_allowance))

        # 10(5)     :   Sec 10(5)-Leave Travel allowance
        if int(sum(LTA_allowance))!=0:
            content += '<ITRForm:AllwncExemptUs10Dtls>\n'
            content += '<ITRForm:SalNatureDesc>10(5)</ITRForm:SalNatureDesc>\n'
            content += '<ITRForm:SalOthAmount>'+str(int(sum(LTA_allowance)))+'</ITRForm:SalOthAmount>\n'
            content += '</ITRForm:AllwncExemptUs10Dtls>\n'

        # 10(13A)   :   Sec 10(13A)-House Rent Allowance
        if int(sum(HRA_allowance))!=0:
            content += '<ITRForm:AllwncExemptUs10Dtls>\n'
            content += '<ITRForm:SalNatureDesc>10(13A)</ITRForm:SalNatureDesc>\n'
            content += '<ITRForm:SalOthAmount>'+str(int(sum(HRA_allowance)))+'</ITRForm:SalOthAmount>\n'
            content += '</ITRForm:AllwncExemptUs10Dtls>\n'

        # 10(10AA): Sec 10(10AA)-Earned leave encashment
        if sum(ELE_allowance)!=0:
            content += '<ITRForm:AllwncExemptUs10Dtls>\n'
            content += '<ITRForm:SalNatureDesc>10(10AA)</ITRForm:SalNatureDesc>\n'
            content += '<ITRForm:SalOthAmount>'+str(sum(ELE_allowance))+'</ITRForm:SalOthAmount>\n'
            content += '</ITRForm:AllwncExemptUs10Dtls>\n'

        # OTH   :   Any Other
        if sum(other_allowance)!=0:
            content += '<ITRForm:AllwncExemptUs10Dtls>\n'
            content += '<ITRForm:SalNatureDesc>OTH</ITRForm:SalNatureDesc>\n'
            content += '<ITRForm:SalOthNatOfInc>Other</ITRForm:SalOthNatOfInc>\n'
            content += '<ITRForm:SalOthAmount>'+str(sum(other_allowance))+'</ITRForm:SalOthAmount>\n'
            content += '</ITRForm:AllwncExemptUs10Dtls>\n'

        content += '</ITRForm:AllwncExemptUs10>\n'

        NetSalary=int(sum(calculated_salary_arr))
        # DeductionUs16ia+Profession Tax + Enterta
        DeductionUs16ia=40000.0
        DeductionUS16=DeductionUs16ia+sum(c_professionTax)+sum(c_entertainment)

        TotIncUnderHeadSalaries=NetSalary-DeductionUS16

        content += '<ITRForm:NetSalary>'+str(int(NetSalary))+'</ITRForm:NetSalary>\n'
        content += '<ITRForm:DeductionUS16>'+str(int(DeductionUS16))+'</ITRForm:DeductionUS16>\n'
        content += '<ITRForm:DeductionUnderSection16ia>'+str(int(DeductionUs16ia))+'</ITRForm:DeductionUnderSection16ia>\n'
        content += '<ITRForm:EntertainmntalwncUs16ii>'+str(int(sum(c_entertainment)))+'</ITRForm:EntertainmntalwncUs16ii>\n'
        content += '<ITRForm:ProfessionalTaxUs16iii>'+str(int(sum(c_professionTax)))+'</ITRForm:ProfessionalTaxUs16iii>\n'
        
        content += '<ITRForm:TotIncUnderHeadSalaries>'+str(int(TotIncUnderHeadSalaries))+'</ITRForm:TotIncUnderHeadSalaries>\n'
    
        content += '</ITRForm:ScheduleS>\n'

    except Exception as ex:
        log.info('Error in ScheduleS ITR2 '+traceback.format_exc())
    # log.info(content)
    log.info('===== ScheduleS Block ends =====')
    return content

def ScheduleHP():
    content=''
    global TotalIncomeChargeableUnHP
    log.info('===== ScheduleHP Block starts =====')
    try:
        content += '<ITRForm:ScheduleHP>\n'
        for hp in xrange(0,no_of_HP):
            content += '<ITRForm:PropertyDetails>\n'
            content += '<ITRForm:HPSNo>'+str(hp+1)+'</ITRForm:HPSNo>\n'
            content += '<ITRForm:AddressDetailWithZipCode>\n'
            content += '<ITRForm:AddrDetail>'+HP_add[hp]+'</ITRForm:AddrDetail>\n'
            content += '<ITRForm:CityOrTownOrDistrict>'+HP_city[hp]+'</ITRForm:CityOrTownOrDistrict>\n'
            content += '<ITRForm:StateCode>'+str(HP_state[hp])+'</ITRForm:StateCode>\n'
            content += '<ITRForm:CountryCode>91</ITRForm:CountryCode>\n'
            content += '<ITRForm:PinCode>'+HP_pin[hp]+'</ITRForm:PinCode>\n'
            content += '</ITRForm:AddressDetailWithZipCode>\n'

            content += '<ITRForm:PropertyOwner>SE</ITRForm:PropertyOwner>\n'
            # content += '<ITRForm:PropertyOwnerOther>SP</ITRForm:PropertyOwnerOther>\n'
            
            co_owner = get_co_owner(str(hp+1))
            log.info(co_owner)
            if HP_Is_coOwned[hp] == 'yes' and co_owner:
                content += '<ITRForm:PropCoOwnedFlg>YES</ITRForm:PropCoOwnedFlg>\n'
                content += '<ITRForm:AsseseeShareProperty>'+str(HP_per_share[hp])+'</ITRForm:AsseseeShareProperty>\n'
                for data in co_owner:
                    content += '<ITRForm:CoOwners>\n'
                    content += '<ITRForm:CoOwnersSNo>'+str(data['co_owner_no'])+'</ITRForm:CoOwnersSNo>\n'
                    content += '<ITRForm:NameCoOwner>'+data['Name_of_Co_Owner']+'</ITRForm:NameCoOwner>\n'
                    if data['PAN_of_Co_owner']!='':
                        content += '<ITRForm:PAN_CoOwner>'+data['PAN_of_Co_owner']+'</ITRForm:PAN_CoOwner>\n'
                    content += '<ITRForm:PercentShareProperty>'+str(data['Percentage_Share_in_Property'])+'</ITRForm:PercentShareProperty>\n'
                    content += '</ITRForm:CoOwners>\n'
            else:
                content += '<ITRForm:PropCoOwnedFlg>NO</ITRForm:PropCoOwnedFlg>\n'
                content += '<ITRForm:AsseseeShareProperty>'+str(HP_per_share[hp])+'</ITRForm:AsseseeShareProperty>\n'

            tenant = get_tenant(str(hp+1))
            log.info(tenant)
            if HP_type_of_hp[hp] == 'let_out' and tenant:
                content += '<ITRForm:ifLetOut>Y</ITRForm:ifLetOut>\n'
                for data in tenant:
                    content += '<ITRForm:TenantDetails>\n'
                    content += '<ITRForm:TenantSNo>'+str(data['tenant_no'])+'</ITRForm:TenantSNo>\n'
                    content += '<ITRForm:NameofTenant>'+data['tenant_name']+'</ITRForm:NameofTenant>\n'
                    content += '</ITRForm:TenantDetails>\n'
            else:
                content += '<ITRForm:ifLetOut>N</ITRForm:ifLetOut>\n'

            content += '<ITRForm:Rentdetails>\n'

            AnnualLetableValue=int(HP_Rent_Received[hp])
            content += '<ITRForm:AnnualLetableValue>'+str(AnnualLetableValue)+'</ITRForm:AnnualLetableValue>\n'
            
            log.info('AnnualLetableValue '+str(AnnualLetableValue))

            RentNotRealized=0
            content += '<ITRForm:RentNotRealized>'+str(RentNotRealized)+'</ITRForm:RentNotRealized>\n'
            
            content += '<ITRForm:LocalTaxes>'+str(int(HP_municipal_tax[hp]))+'</ITRForm:LocalTaxes>\n'
            
            log.info('LocalTaxes '+str(int(HP_municipal_tax[hp])))

            # LocalTaxes + RentNotRealized
            TotalUnrealizedAndTax=int(HP_municipal_tax[hp])+RentNotRealized
            content += '<ITRForm:TotalUnrealizedAndTax>'+str(int(HP_municipal_tax[hp]))+'</ITRForm:TotalUnrealizedAndTax>\n'
            
            log.info('TotalUnrealizedAndTax '+str(int(HP_municipal_tax[hp])))

            # AnnualLetableValue - TotalUnrealizedAndTax
            BalanceALV=AnnualLetableValue-TotalUnrealizedAndTax
            content += '<ITRForm:BalanceALV>'+str(int(HP_Rent_Received[hp] - HP_municipal_tax[hp]))+'</ITRForm:BalanceALV>\n'
            
            log.info('BalanceALV '+str(BalanceALV))

            log.info('Assesse Property share '+str(HP_per_share[hp]))

            log.info('int(HP_per_share[hp])/100 '+str(float(HP_per_share[hp])/100))

            # BalanceALV * AsseseeShareProperty

            AnnualOfPropOwned = float(HP_per_share[hp])/100 * (BalanceALV)
            content += '<ITRForm:AnnualOfPropOwned>'+str(int(AnnualOfPropOwned))+'</ITRForm:AnnualOfPropOwned>\n'
            
            log.info('AnnualOfPropOwned '+str(int(AnnualOfPropOwned)))

            ThirtyPercentOfBalance=int( 0.3 * AnnualOfPropOwned )
            content += '<ITRForm:ThirtyPercentOfBalance>'+str(ThirtyPercentOfBalance)+'</ITRForm:ThirtyPercentOfBalance>\n'
            
            log.info('ThirtyPercentOfBalance '+str(ThirtyPercentOfBalance))

            # if HP_Interest[hp]>20000:
            #   IntOnBorwCap=20000
            #   content += '<ITRForm:IntOnBorwCap>20000</ITRForm:IntOnBorwCap>\n'
            #   TotalDeduct=int((0.3 * AnnualOfPropOwned)+IntOnBorwCap)
            #   content += '<ITRForm:TotalDeduct>'+str(TotalDeduct)+'</ITRForm:TotalDeduct>\n'
            # else:
            #   IntOnBorwCap=int(HP_Interest[hp])
            #   content += '<ITRForm:IntOnBorwCap>'+str(IntOnBorwCap)+'</ITRForm:IntOnBorwCap>\n'
            #   TotalDeduct=int((0.3 * AnnualOfPropOwned)+IntOnBorwCap)
            #   content += '<ITRForm:TotalDeduct>'+str(TotalDeduct)+'</ITRForm:TotalDeduct>\n'
            
            
            # Assessees Share * (Rent Received - Less Municipal Taxes)

            log.info('(HP_Rent_Received[hp]-HP_municipal_tax[hp]) '+str((HP_Rent_Received[hp]-HP_municipal_tax[hp])))

            proportionate_annual_value=(float(HP_per_share[hp])/100) * (HP_Rent_Received[hp]-HP_municipal_tax[hp])

            less_standard_deduction= (float(30)/100)*proportionate_annual_value

            log.info('proportionate_annual_value '+str(proportionate_annual_value))

            log.info('less_standard_deduction '+str(less_standard_deduction))

            income_from_HP=proportionate_annual_value-HP_Interest[hp]-less_standard_deduction
           
            log.info('income_from_HP '+str(income_from_HP))

            IntOnBorwCap=int(HP_Interest[hp])
            content += '<ITRForm:IntOnBorwCap>'+str(IntOnBorwCap)+'</ITRForm:IntOnBorwCap>\n'
            
            TotalDeduct=ThirtyPercentOfBalance+IntOnBorwCap
            content += '<ITRForm:TotalDeduct>'+str(TotalDeduct)+'</ITRForm:TotalDeduct>\n'
            # log.info('ThirtyPercentOfBalance '+str(ThirtyPercentOfBalance))

            log.info('IntOnBorwCap '+str(IntOnBorwCap))

            log.info('TotalDeduct '+str(TotalDeduct))

            ArrearsUnrealizedRentRcvd=0
            content += '<ITRForm:ArrearsUnrealizedRentRcvd>'+str(ArrearsUnrealizedRentRcvd)+'</ITRForm:ArrearsUnrealizedRentRcvd>\n'
            

            log.info('ArrearsUnrealizedRentRcvd '+str(ArrearsUnrealizedRentRcvd))
            # AnnualOfPropOwned - TotalDeduct + ArrearsUnrealizedRentRcvd
            IncomeOfHP=AnnualOfPropOwned-TotalDeduct+ArrearsUnrealizedRentRcvd
            content += '<ITRForm:IncomeOfHP>'+str(int(IncomeOfHP))+'</ITRForm:IncomeOfHP>\n'
            
            TotalIncomeChargeableUnHP += int(IncomeOfHP)
            log.info('IncomeOfHP '+str(IncomeOfHP))

            content += '</ITRForm:Rentdetails>\n'
            content += '</ITRForm:PropertyDetails>\n'

        # content += '<ITRForm:RentOfEarlierYrSec25AandAA>0</ITRForm:RentOfEarlierYrSec25AandAA>\n'
        # TotalIncomeChargeableUnHP=int(ICU_house_p) 
        # content += '<ITRForm:TotalIncomeChargeableUnHP>'+str(TotalIncomeChargeableUnHP)+'</ITRForm:TotalIncomeChargeableUnHP>\n'
        content += '<ITRForm:PassThroghIncome>0</ITRForm:PassThroghIncome>\n'
        
        content += '<ITRForm:TotalIncomeChargeableUnHP>'+str(TotalIncomeChargeableUnHP)+'</ITRForm:TotalIncomeChargeableUnHP>\n'
        
        log.info('TotalIncomeChargeableUnHP '+str(TotalIncomeChargeableUnHP))

        content += '</ITRForm:ScheduleHP>\n'
    except Exception as ex:
        log.info('Error in ScheduleHP ITR2 '+traceback.format_exc())
    log.info('===== ScheduleHP Block ends =====')
    return content

def NoHP():
    content=''
    content += '<ITRForm:ScheduleHP>\n'
    content += '<ITRForm:PassThroghIncome>0</ITRForm:PassThroghIncome>\n'
    content += '<ITRForm:TotalIncomeChargeableUnHP>0</ITRForm:TotalIncomeChargeableUnHP>\n'
    content += '</ITRForm:ScheduleHP>\n'
    return content

def ScheduleCGFor23():
    content=''
    global CYLA_setoffs
    log.info('===== ScheduleCGFor23 Block starts =====')
    try:
        # Capital_Gain start (ScheduleCGFor23 ==> ShortTermCapGainFor23)
        content += '<ITRForm:ScheduleCGFor23>\n'
        if 1 == 1:
            content += '<ITRForm:ShortTermCapGainFor23>\n'
            # ignore
            content += '<ITRForm:SaleofLandBuild>\n'
            content += '<ITRForm:SaleofLandBuildDtls>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:PropertyValuation>0</ITRForm:PropertyValuation>\n'
            content += '<ITRForm:FullConsideration50C>0</ITRForm:FullConsideration50C>\n'
            # content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            # content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:Balance>0</ITRForm:Balance>\n'
            content += '<ITRForm:DeductionUs54B>0</ITRForm:DeductionUs54B>\n'
            content += '<ITRForm:STCGonImmvblPrprty>0</ITRForm:STCGonImmvblPrprty>\n'
            content += '</ITRForm:SaleofLandBuildDtls>\n'
            content += '</ITRForm:SaleofLandBuild>\n'

            content += '<ITRForm:EquityMFonSTT>\n'
            content += '<ITRForm:MFSectionCode>1A</ITRForm:MFSectionCode>\n'
            # Cost_of_Acquisition 
            content += '<ITRForm:EquityMFonSTTDtls>\n'

            content += '<ITRForm:FullConsideration>'+str(int(share_equity_sell_value) )+'</ITRForm:FullConsideration>\n'
            
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>'+str(int(share_equity_cost_acq) )+'</ITRForm:AquisitCost>\n'
            
            ImproveCost=0
            content += '<ITRForm:ImproveCost>'+str(ImproveCost)+'</ITRForm:ImproveCost>\n'
            
            ExpOnTrans=0
            content += '<ITRForm:ExpOnTrans>'+str(ImproveCost)+'</ITRForm:ExpOnTrans>\n'
            
            TotalDedn=int(share_equity_cost_acq)+ImproveCost+ExpOnTrans
            content += '<ITRForm:TotalDedn>'+str(TotalDedn)+'</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'

            BalanceCG=int(share_equity_sell_value-TotalDedn)
            content += '<ITRForm:BalanceCG>'+str(BalanceCG)+'</ITRForm:BalanceCG>\n'
            
            LossSec94of7Or94of8=0
            content += '<ITRForm:LossSec94of7Or94of8>'+str(LossSec94of7Or94of8)+'</ITRForm:LossSec94of7Or94of8>\n'
            
            ST_CapgainonAssets_Share_Equity=BalanceCG-LossSec94of7Or94of8
            content += '<ITRForm:CapgainonAssets>'+str(ST_CapgainonAssets_Share_Equity)+'</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:EquityMFonSTTDtls>\n'
            content += '</ITRForm:EquityMFonSTT>\n'


            content += '<ITRForm:EquityMFonSTT>\n'
            content += '<ITRForm:MFSectionCode>5AD1biip</ITRForm:MFSectionCode>\n'
            content += '<ITRForm:EquityMFonSTTDtls>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '<ITRForm:LossSec94of7Or94of8>0</ITRForm:LossSec94of7Or94of8>\n'
            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:EquityMFonSTTDtls>\n'
            content += '</ITRForm:EquityMFonSTT>\n'


            content += '<ITRForm:NRITransacSec48Dtl>\n'
            content += '<ITRForm:NRItaxSTTPaid>0</ITRForm:NRItaxSTTPaid>\n'
            content += '<ITRForm:NRItaxSTTNotPaid>0</ITRForm:NRItaxSTTNotPaid>\n'
            content += '</ITRForm:NRITransacSec48Dtl>\n'


            content += '<ITRForm:NRISecur115AD>\n'
            content += '<ITRForm:FullValueConsdRecvUnqshr>0</ITRForm:FullValueConsdRecvUnqshr>\n'
            content += '<ITRForm:FairMrktValueUnqshr>0</ITRForm:FairMrktValueUnqshr>\n'
            content += '<ITRForm:FullValueConsdSec50CA>0</ITRForm:FullValueConsdSec50CA>\n'
            content += '<ITRForm:FullValueConsdOthUnqshr>0</ITRForm:FullValueConsdOthUnqshr>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '<ITRForm:LossSec94of7Or94of8>0</ITRForm:LossSec94of7Or94of8>\n'
            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:NRISecur115AD>\n'
            # debt & listed ST
            # LT debenture Proviso112SectionCode (done)
            # debt MF & liq MF LT SaleofAssetNA (done)
            # Total TotScheduleCGFor23 (all ST & LT)
            # ScheduleSI --> calculate Spcl rate
            # 1A ==> shares & equity ST (done)
            # 21 ==> debt MF LT (done)
            # 22 ==> list deb LT (done)
            content += '<ITRForm:SaleOnOtherAssets>\n'
            content += '<ITRForm:FullValueConsdRecvUnqshr>0</ITRForm:FullValueConsdRecvUnqshr>\n'
            content += '<ITRForm:FairMrktValueUnqshr>0</ITRForm:FairMrktValueUnqshr>\n'
            
            FullValueConsdSec50CA=0
            content += '<ITRForm:FullValueConsdSec50CA>'+str(FullValueConsdSec50CA)+'</ITRForm:FullValueConsdSec50CA>\n'
            
            FullValueConsdOthUnqshr=int(st_debt_MF_tsv+st_listed_d_tsv) 
            content += '<ITRForm:FullValueConsdOthUnqshr>'+str(FullValueConsdOthUnqshr)+'</ITRForm:FullValueConsdOthUnqshr>\n'
            
            FullConsideration=FullValueConsdOthUnqshr+FullValueConsdSec50CA
            content += '<ITRForm:FullConsideration>'+str(FullConsideration)+'</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'

            AquisitCost=int(st_debt_MF_tpv+st_listed_d_tpv) 
            content += '<ITRForm:AquisitCost>'+str(AquisitCost)+'</ITRForm:AquisitCost>\n'
            
            ImproveCost=0
            content += '<ITRForm:ImproveCost>'+str(ImproveCost)+'</ITRForm:ImproveCost>\n'
            
            ExpOnTrans=0
            content += '<ITRForm:ExpOnTrans>'+str(ExpOnTrans)+'</ITRForm:ExpOnTrans>\n'
            
            TotalDedn=AquisitCost+ImproveCost+ExpOnTrans
            content += '<ITRForm:TotalDedn>'+str(TotalDedn)+'</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            
            BalanceCG=FullConsideration-TotalDedn
            content += '<ITRForm:BalanceCG>'+str(BalanceCG)+'</ITRForm:BalanceCG>\n'
            
            LossSec94of7Or94of8=0
            content += '<ITRForm:LossSec94of7Or94of8>0</ITRForm:LossSec94of7Or94of8>\n'
            
            ST_CapgainonAssets_Debentures_Debt=BalanceCG+LossSec94of7Or94of8
            content += '<ITRForm:CapgainonAssets>'+str(ST_CapgainonAssets_Debentures_Debt)+'</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:SaleOnOtherAssets>\n'
            # change : add tag : UnutilizedStcgFlag (Y - Yes,N - No, X - Not Applicable) (done)
            content += '<ITRForm:UnutilizedStcgFlag>X</ITRForm:UnutilizedStcgFlag>\n'
            content += '<ITRForm:TotalAmtDeemedStcg>0</ITRForm:TotalAmtDeemedStcg>\n'
            content += '<ITRForm:PassThrIncNatureSTCG>0</ITRForm:PassThrIncNatureSTCG>\n'
            content += '<ITRForm:PassThrIncNatureSTCG15Per>0</ITRForm:PassThrIncNatureSTCG15Per>\n'
            content += '<ITRForm:PassThrIncNatureSTCG30Per>0</ITRForm:PassThrIncNatureSTCG30Per>\n'
            content += '<ITRForm:PassThrIncNatureSTCGAppRate>0</ITRForm:PassThrIncNatureSTCGAppRate>\n'

            # ignore
            # content += '<ITRForm:NRICgDTAA>'
            # content += '<ITRForm:NRITaxUsDTAAStcgType>\n'
            # content += '<ITRForm:NRIDTAADtls>'

            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'
            # content += '<ITRForm:ItemNoincl></ITRForm:ItemNoincl>'
            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'
            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'
            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'
            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'
            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'
            # content += '<ITRForm:DTAAamt></ITRForm:DTAAamt>'


            # content += '</ITRForm:NRIDTAADtls>'
            # content += '</ITRForm:NRITaxUsDTAAStcgType>\n'
            # content += '</ITRForm:NRICgDTAA>'

            
            content += '<ITRForm:TotalAmtNotTaxUsDTAAStcg>0</ITRForm:TotalAmtNotTaxUsDTAAStcg>\n'
            content += '<ITRForm:TotalAmtTaxUsDTAAStcg>0</ITRForm:TotalAmtTaxUsDTAAStcg>\n'
            
            TotalSTCG=int(share_equity_sell_value-share_equity_cost_acq+(st_debt_MF_gain+st_listed_d_gain)) 
            content += '<ITRForm:TotalSTCG>'+str(TotalSTCG)+'</ITRForm:TotalSTCG>\n'
            content += '</ITRForm:ShortTermCapGainFor23>\n'
        # LTCGWhereSTTPaid = share E_MF lt 
        # EquityMFonSTTDtls  ==> FullConsideration = share $MF st
        # LongTermCapGain23
        if 1 == 1:
            content += '<ITRForm:LongTermCapGain23>\n'

            log.info('========= SaleofLandBuild starts ========')

            content += '<ITRForm:SaleofLandBuild>\n'
            content += '<ITRForm:SaleofLandBuildDtls>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:PropertyValuation>0</ITRForm:PropertyValuation>\n'
            content += '<ITRForm:FullConsideration50C>0</ITRForm:FullConsideration50C>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '<ITRForm:Balance>0</ITRForm:Balance>\n'
            # content += '<ITRForm:ExemptionOrDednUs54>0</ITRForm:ExemptionOrDednUs54>\n'   
            
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54B</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54EC</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54F</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54GB</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54EE</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'

            content += '<ITRForm:LTCGonImmvblPrprty>0</ITRForm:LTCGonImmvblPrprty>\n'
            # content += '<ITRForm:TrnsfImmblPrprty>0</ITRForm:TrnsfImmblPrprty>\n'
            content += '</ITRForm:SaleofLandBuildDtls>\n'
            content += '</ITRForm:SaleofLandBuild>\n'

            log.info('========= SaleofLandBuild ends ========')

            log.info('========= SaleofBondsDebntr starts ========')

            content += '<ITRForm:SaleofBondsDebntr>\n'
            # content += '<ITRForm:EquityOrUnitSec54TypeDebn112>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            # content += '<ITRForm:DeductionUs54F>0</ITRForm:DeductionUs54F>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54F</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'

            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            # content += '</ITRForm:EquityOrUnitSec54TypeDebn112>\n'
            content += '</ITRForm:SaleofBondsDebntr>\n'

            log.info('========= SaleofBondsDebntr ends ========')

            log.info('========= Proviso112Applicable starts ========')
            
            content += '<ITRForm:Proviso112Applicable>\n'
            content += '<ITRForm:Proviso112SectionCode>22</ITRForm:Proviso112SectionCode>\n'
            
            content += '<ITRForm:Proviso112Applicabledtls>\n'
            # content += '<ITRForm:EquityOrUnitSec54TypeDebn112>\n'
            
            FullConsideration=int(lt_listed_d_tsv)
            content += '<ITRForm:FullConsideration>'+str(FullConsideration)+'</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            
            AquisitCost=int(lt_listed_d_tpv)
            content += '<ITRForm:AquisitCost>'+str(AquisitCost)+'</ITRForm:AquisitCost>\n'
            
            ImproveCost=0
            content += '<ITRForm:ImproveCost>'+str(ImproveCost)+'</ITRForm:ImproveCost>\n'
            
            ExpOnTrans=0
            content += '<ITRForm:ExpOnTrans>'+str(ExpOnTrans)+'</ITRForm:ExpOnTrans>\n'
            
            TotalDedn=AquisitCost+ImproveCost+ExpOnTrans
            content += '<ITRForm:TotalDedn>'+str(TotalDedn)+'</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            
            BalanceCG = FullConsideration-TotalDedn
            content += '<ITRForm:BalanceCG>'+str(BalanceCG)+'</ITRForm:BalanceCG>\n'
            
            DeductionUs54F=0
            # content += '<ITRForm:DeductionUs54F>0</ITRForm:DeductionUs54F>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54F</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'
            
            LT_CapgainonAssets_Debenture=BalanceCG-DeductionUs54F
            content += '<ITRForm:CapgainonAssets>'+str(LT_CapgainonAssets_Debenture)+'</ITRForm:CapgainonAssets>\n'
            
            # content += '</ITRForm:EquityOrUnitSec54TypeDebn112>\n'

            content += '</ITRForm:Proviso112Applicabledtls>\n'
            content += '</ITRForm:Proviso112Applicable>\n'

            log.info('========= Proviso112Applicable ends ========')

            content += '<ITRForm:Proviso112Applicable>\n'
            content += '<ITRForm:Proviso112SectionCode>5ACA1b</ITRForm:Proviso112SectionCode>\n'
            content += '<ITRForm:Proviso112Applicabledtls>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54F</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:Proviso112Applicabledtls>\n'
            content += '</ITRForm:Proviso112Applicable>\n'

            # ======================================================
            log.info('========= SaleOfEquityShareUs112A starts ========')
            content += '<ITRForm:SaleOfEquityShareUs112A>\n'
            # content += '<ITRForm:EquityShareUs112A>\n'

            # Add all sell value for Equity MF + Shares for relevant FY
            FullConsideration=int(lt_shares_tsv)+int(lt_equity_tsv)
            content += '<ITRForm:FullConsideration>'+str(FullConsideration)+'</ITRForm:FullConsideration>\n'

            content += '<ITRForm:DeductSec48Sec112A>\n'
            # content += '<ITRForm:DeductSec48Us112A>\n'

            # Add all purchase value for Equity MF + Shares for relevant FY
            AquisitCost=int(lt_shares_tpv)+int(lt_equity_tpv)

            # Aggregate of all FMVs  
            FairMarketValue=int(lt_shares_FMV+lt_equity_FMV)

            log.info('LT Share and equity FairMarketValue '+str(FairMarketValue))

            # Add all sell value for Equity MF + Shares for relevant FY
            FullValueConsdrtn=FullConsideration

            # Min (FairMarketValue,FullValueConsdrtn)
            LTCAAcquiredBf=min (FairMarketValue,FullValueConsdrtn)

            # Max (AquisitCost,LTCAAcquiredBf)
            CostOfAquisitionWoInd=max (AquisitCost,LTCAAcquiredBf)

            content += '<ITRForm:CostOfAquisitionWoInd>'+str(CostOfAquisitionWoInd)+'</ITRForm:CostOfAquisitionWoInd>\n'
            
            content += '<ITRForm:AquisitCost>'+str(AquisitCost)+'</ITRForm:AquisitCost>\n'
            
            content += '<ITRForm:LTCAAcquiredBf>'+str(LTCAAcquiredBf)+'</ITRForm:LTCAAcquiredBf>\n'

            content += '<ITRForm:FairMarketValue>'+str(FairMarketValue)+'</ITRForm:FairMarketValue>\n'

            content += '<ITRForm:FullValueConsdrtn>'+str(FullValueConsdrtn)+'</ITRForm:FullValueConsdrtn>\n'

            ImproveCost=0
            content += '<ITRForm:ImproveCost>'+str(ImproveCost)+'</ITRForm:ImproveCost>\n'

            ExpOnTrans=0
            content += '<ITRForm:ExpOnTrans>'+str(ExpOnTrans)+'</ITRForm:ExpOnTrans>\n'

            # '= CostOfAquisitionWoInd + ImproveCost + ExpOnTrans
            TotalDedn=CostOfAquisitionWoInd+ImproveCost+ExpOnTrans
            content += '<ITRForm:TotalDedn>'+str(TotalDedn)+'</ITRForm:TotalDedn>\n'

            # content += '</ITRForm:DeductSec48Us112A>\n'
            content += '</ITRForm:DeductSec48Sec112A>\n'

            # '= FullConsideration - TotalDedn
            BalanceCG=FullConsideration-TotalDedn
            content += '<ITRForm:BalanceCG>'+str(BalanceCG)+'</ITRForm:BalanceCG>\n'

            LessLTCGThresholdLimit=100000
            # content += '<ITRForm:LessLTCGThresholdLimit>'+str(LessLTCGThresholdLimit)+'</ITRForm:LessLTCGThresholdLimit>\n'

            DeductionUs54F=0
            # content += '<ITRForm:DeductionUs54F>'+str(DeductionUs54F)+'</ITRForm:DeductionUs54F>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionSecCode>54F</ITRForm:ExemptionSecCode>\n'
            content += '<ITRForm:ExemptionAmount>0</ITRForm:ExemptionAmount>\n'
            content += '</ITRForm:ExemptionOrDednUs54Dtls>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'

            # '= BalanceCG - DeductionUs54F
            LT_CapgainonAssets_Equity_MF=BalanceCG-DeductionUs54F
            content += '<ITRForm:CapgainonAssets>'+str(LT_CapgainonAssets_Equity_MF)+'</ITRForm:CapgainonAssets>\n'

            # content += '</ITRForm:EquityShareUs112A>\n'
            content += '</ITRForm:SaleOfEquityShareUs112A>\n'

            log.info('========= SaleOfEquityShareUs112A ends ========')

            log.info('========= NRIProvisoSec48 starts ========')

            content += '<ITRForm:NRIProvisoSec48>\n'
            # content += '<ITRForm:NRISaleofForeignAsset>\n'

            # SaleonSpecAsset=0
            # content += '<ITRForm:SaleonSpecAsset>'+str(SaleonSpecAsset)+'</ITRForm:SaleonSpecAsset>\n'

            # content += '<ITRForm:DednSpecAssetus115>0</ITRForm:DednSpecAssetus115>\n'
            # content += '<ITRForm:BalonSpeciAsset>0</ITRForm:BalonSpeciAsset>\n'
            # content += '<ITRForm:SaleOtherSpecAsset>0</ITRForm:SaleOtherSpecAsset>\n'
            # content += '<ITRForm:DednOtherSpecAssetus115>0</ITRForm:DednOtherSpecAssetus115>\n'
            # content += '<ITRForm:BalOtherthanSpecAsset>0</ITRForm:BalOtherthanSpecAsset>\n'

            # content += '</ITRForm:NRISaleofForeignAsset>\n'
            content += '<ITRForm:LTCGWithoutBenefit>0</ITRForm:LTCGWithoutBenefit>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '</ITRForm:NRIProvisoSec48>\n'

            log.info('========= NRIProvisoSec48 ends ========')



            log.info('========= NRIOnSec112and115 starts ========')

            # content += '<ITRForm:NRIOnSec112and115>0</ITRForm:NRIOnSec112and115>\n'
            content += '<ITRForm:NRIOnSec112and115>\n'
            content += '<ITRForm:NRIOnSec112and115Dtls>\n'
            content += '<ITRForm:SectionCode>21ciii</ITRForm:SectionCode>\n'
            content += '<ITRForm:FullValueConsdRecvUnqshr>0</ITRForm:FullValueConsdRecvUnqshr>\n'
            content += '<ITRForm:FairMrktValueUnqshr>0</ITRForm:FairMrktValueUnqshr>\n'
            content += '<ITRForm:FullValueConsdSec50CA>0</ITRForm:FullValueConsdSec50CA>\n'
            content += '<ITRForm:FullValueConsdOthUnqshr>0</ITRForm:FullValueConsdOthUnqshr>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:NRIOnSec112and115Dtls>\n'
            content += '<ITRForm:NRIOnSec112and115Dtls>\n'
            content += '<ITRForm:SectionCode>5AC1c</ITRForm:SectionCode>\n'
            content += '<ITRForm:FullValueConsdRecvUnqshr>0</ITRForm:FullValueConsdRecvUnqshr>\n'
            content += '<ITRForm:FairMrktValueUnqshr>0</ITRForm:FairMrktValueUnqshr>\n'
            content += '<ITRForm:FullValueConsdSec50CA>0</ITRForm:FullValueConsdSec50CA>\n'
            content += '<ITRForm:FullValueConsdOthUnqshr>0</ITRForm:FullValueConsdOthUnqshr>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:NRIOnSec112and115Dtls>\n'
            content += '<ITRForm:NRIOnSec112and115Dtls>\n'
            content += '<ITRForm:SectionCode>5ADiii</ITRForm:SectionCode>\n'
            content += '<ITRForm:FullValueConsdRecvUnqshr>0</ITRForm:FullValueConsdRecvUnqshr>\n'
            content += '<ITRForm:FairMrktValueUnqshr>0</ITRForm:FairMrktValueUnqshr>\n'
            content += '<ITRForm:FullValueConsdSec50CA>0</ITRForm:FullValueConsdSec50CA>\n'
            content += '<ITRForm:FullValueConsdOthUnqshr>0</ITRForm:FullValueConsdOthUnqshr>\n'
            content += '<ITRForm:FullConsideration>0</ITRForm:FullConsideration>\n'
            content += '<ITRForm:DeductSec48>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:ImproveCost>0</ITRForm:ImproveCost>\n'
            content += '<ITRForm:ExpOnTrans>0</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'
            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'
            content += '</ITRForm:NRIOnSec112and115Dtls>\n'
            content += '</ITRForm:NRIOnSec112and115>\n'
            log.info('========= NRIOnSec112and115 ends ========')


            log.info('========= NRISaleOfEquityShareUs112A starts ========')
            content += '<ITRForm:NRISaleOfEquityShareUs112A>\n'
            # content += '<ITRForm:EquityShareUs112A>\n'

            FullConsideration=0
            content += '<ITRForm:FullConsideration>'+str(FullConsideration)+'</ITRForm:FullConsideration>\n'

            content += '<ITRForm:DeductSec48Sec112A>\n'
            # content += '<ITRForm:DeductSec48Us112A>\n'

            content += '<ITRForm:CostOfAquisitionWoInd>0</ITRForm:CostOfAquisitionWoInd>\n'
            content += '<ITRForm:AquisitCost>0</ITRForm:AquisitCost>\n'
            content += '<ITRForm:LTCAAcquiredBf>0</ITRForm:LTCAAcquiredBf>\n'
            content += '<ITRForm:FairMarketValue>0</ITRForm:FairMarketValue>\n'
            content += '<ITRForm:FullValueConsdrtn>0</ITRForm:FullValueConsdrtn>\n'
            ImproveCost=0
            content += '<ITRForm:ImproveCost>'+str(ImproveCost)+'</ITRForm:ImproveCost>\n'
            ExpOnTrans=0
            content += '<ITRForm:ExpOnTrans>'+str(ExpOnTrans)+'</ITRForm:ExpOnTrans>\n'
            content += '<ITRForm:TotalDedn>0</ITRForm:TotalDedn>\n'

            # content += '</ITRForm:DeductSec48Us112A>\n'
            content += '</ITRForm:DeductSec48Sec112A>\n'

            content += '<ITRForm:BalanceCG>0</ITRForm:BalanceCG>\n'
            # content += '<ITRForm:LessLTCGThresholdLimit>0</ITRForm:LessLTCGThresholdLimit>\n'
            # content += '<ITRForm:DeductionUs54F>0</ITRForm:DeductionUs54F>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'

            content += '<ITRForm:CapgainonAssets>0</ITRForm:CapgainonAssets>\n'

            # content += '</ITRForm:EquityShareUs112A>\n'
            content += '</ITRForm:NRISaleOfEquityShareUs112A>\n'

            log.info('========= NRISaleOfEquityShareUs112A ends ========')


            log.info('========= NRISaleofForeignAsset starts ========')
            # content += '<ITRForm:NRISaleofForeignAsset>0</ITRForm:NRISaleofForeignAsset>\n'
            content += '<ITRForm:NRISaleofForeignAsset>\n'
            content += '<ITRForm:SaleonSpecAsset>0</ITRForm:SaleonSpecAsset>\n'
            content += '<ITRForm:DednSpecAssetus115>0</ITRForm:DednSpecAssetus115>\n'
            content += '<ITRForm:BalonSpeciAsset>0</ITRForm:BalonSpeciAsset>\n'
            content += '<ITRForm:SaleOtherSpecAsset>0</ITRForm:SaleOtherSpecAsset>\n'
            content += '<ITRForm:DednOtherSpecAssetus115>0</ITRForm:DednOtherSpecAssetus115>\n'
            content += '<ITRForm:BalOtherthanSpecAsset>0</ITRForm:BalOtherthanSpecAsset>\n'
            content += '</ITRForm:NRISaleofForeignAsset>\n'
            log.info('========= NRISaleofForeignAsset ends ========')

            log.info('========= SaleofAssetNA starts ========')
            content += '<ITRForm:SaleofAssetNA>\n'
            # content += '<ITRForm:EquityOrUnitSec54Type>\n'

            content += '<ITRForm:FullValueConsdRecvUnqshr>0</ITRForm:FullValueConsdRecvUnqshr>\n'
            content += '<ITRForm:FairMrktValueUnqshr>0</ITRForm:FairMrktValueUnqshr>\n'
            
            FullValueConsdSec50CA=0
            content += '<ITRForm:FullValueConsdSec50CA>'+str(FullValueConsdSec50CA)+'</ITRForm:FullValueConsdSec50CA>\n'

            # Aggregate of Debt MF Sale Value
            FullValueConsdOthUnqshr=int(lt_debt_MF_tsv)
            content += '<ITRForm:FullValueConsdOthUnqshr>'+str(FullValueConsdOthUnqshr)+'</ITRForm:FullValueConsdOthUnqshr>\n'
            
            # '= FullValueConsdOthUnqshr + FullValueConsdSec50CA
            FullConsideration=FullValueConsdOthUnqshr+FullValueConsdSec50CA
            content += '<ITRForm:FullConsideration>'+str(FullConsideration)+'</ITRForm:FullConsideration>\n'

            content += '<ITRForm:DeductSec48>\n'

            # '= Aggregate of Debt MF Purchase Value
            AquisitCost=int(lt_debt_MF_tpv)
            content += '<ITRForm:AquisitCost>'+str(AquisitCost)+'</ITRForm:AquisitCost>\n'
            
            ImproveCost=0
            content += '<ITRForm:ImproveCost>'+str(ImproveCost)+'</ITRForm:ImproveCost>\n'
            
            ExpOnTrans=0
            content += '<ITRForm:ExpOnTrans>'+str(ExpOnTrans)+'</ITRForm:ExpOnTrans>\n'

            TotalDedn=AquisitCost+ImproveCost+ExpOnTrans
            content += '<ITRForm:TotalDedn>'+str(TotalDedn)+'</ITRForm:TotalDedn>\n'
            content += '</ITRForm:DeductSec48>\n'

            # '= FullConsideration - TotalDedn
            BalanceCG=FullConsideration-TotalDedn
            content += '<ITRForm:BalanceCG>'+str(BalanceCG)+'</ITRForm:BalanceCG>\n'
            
            DeductionUs54=0
            # content += '<ITRForm:DeductionUs54>'+str(DeductionUs54)+'</ITRForm:DeductionUs54>\n'
            content += '<ITRForm:ExemptionOrDednUs54>\n'
            content += '<ITRForm:ExemptionGrandTotal>0</ITRForm:ExemptionGrandTotal>\n'
            content += '</ITRForm:ExemptionOrDednUs54>\n'

            # '= BalanceCG - DeductionUs54F
            LT_CapgainonAssets_Debt_MF=BalanceCG-DeductionUs54
            content += '<ITRForm:CapgainonAssets>'+str(LT_CapgainonAssets_Debt_MF)+'</ITRForm:CapgainonAssets>\n'

            # content += '</ITRForm:EquityOrUnitSec54Type>\n'
            content += '</ITRForm:SaleofAssetNA>\n'
            log.info('========= SaleofAssetNA ends ========')


            log.info('========= UnutilizedLtcgFlag starts ========')
            content += '<ITRForm:UnutilizedLtcgFlag>X</ITRForm:UnutilizedLtcgFlag>\n'
            log.info('========= UnutilizedLtcgFlag ends ========')

            # ignore
            # log.info('========= UnutilizedCg starts ========')
            # content += '<ITRForm:UnutilizedCg>\n'
            # content += '<ITRForm:UnutilizedCgPrvYrLtcg>\n'
            # content += '<ITRForm:UnutilizedCgPrvYrDtls>\n'

            # content += '<ITRForm:PrvYrInWhichAsstTrnsfrd>X</ITRForm:PrvYrInWhichAsstTrnsfrd>\n'
            # content += '<ITRForm:SectionClmd>X</ITRForm:SectionClmd>\n'
            # content += '<ITRForm:YrInWhichAssetAcq>X</ITRForm:YrInWhichAssetAcq>\n'
            # content += '<ITRForm:AmtUtilized>X</ITRForm:AmtUtilized>\n'
            # content += '<ITRForm:AmtUnutilized>X</ITRForm:AmtUnutilized>\n'

            # content += '</ITRForm:UnutilizedCgPrvYrDtls>\n'
            # content += '</ITRForm:UnutilizedCgPrvYrLtcg>\n'
            # content += '</ITRForm:UnutilizedCg>\n'
            # log.info('========= UnutilizedCg ends ========')

            # log.info('========= AmtDeemedLtcg starts ========')
            # content += '<ITRForm:AmtDeemedLtcg>0</ITRForm:AmtDeemedLtcg>\n'
            # log.info('========= AmtDeemedLtcg ends ========')

            log.info('========= TotalAmtDeemedLtcg starts ========')
            content += '<ITRForm:TotalAmtDeemedLtcg>0</ITRForm:TotalAmtDeemedLtcg>\n'
            log.info('========= TotalAmtDeemedLtcg ends ========')

            log.info('========= PassThrIncNatureLTCG starts ========')
            content += '<ITRForm:PassThrIncNatureLTCG>0</ITRForm:PassThrIncNatureLTCG>\n'
            log.info('========= PassThrIncNatureLTCG ends ========')

            log.info('========= PassThrIncNatureLTCG10Per starts ========')
            content += '<ITRForm:PassThrIncNatureLTCG10Per>0</ITRForm:PassThrIncNatureLTCG10Per>\n'
            log.info('========= PassThrIncNatureLTCG10Per ends ========')

            log.info('========= PassThrIncNatureLTCG20Per starts ========')
            content += '<ITRForm:PassThrIncNatureLTCG20Per>0</ITRForm:PassThrIncNatureLTCG20Per>\n'
            log.info('========= PassThrIncNatureLTCG20Per ends ========')

            # ignore
            '''
            log.info('========= NRICgDTAA starts ========')
            content += '<ITRForm:PassThrIncNatureLTCG20Per>0</ITRForm:PassThrIncNatureLTCG20Per>\n'
            log.info('========= NRICgDTAA ends ========')
            '''

            log.info('========= TotalAmtNotTaxUsDTAALtcg starts ========')
            content += '<ITRForm:TotalAmtNotTaxUsDTAALtcg>0</ITRForm:TotalAmtNotTaxUsDTAALtcg>\n'
            log.info('========= TotalAmtNotTaxUsDTAALtcg ends ========')

            log.info('========= TotalAmtTaxUsDTAALtcg starts ========')
            content += '<ITRForm:TotalAmtTaxUsDTAALtcg>0</ITRForm:TotalAmtTaxUsDTAALtcg>\n'
            log.info('========= TotalAmtTaxUsDTAALtcg ends ========')

            log.info('========= TotalLTCG starts ========')
            # ' = CapgainonAssets Equity + CapgainonAssets Debt MF+ CapgainonAssets Debenture
            log.info('LT_CapgainonAssets_Equity_MF '+str(LT_CapgainonAssets_Equity_MF))
            log.info('LT_CapgainonAssets_Debt_MF '+str(LT_CapgainonAssets_Debt_MF))
            log.info('LT_CapgainonAssets_Debenture '+str(LT_CapgainonAssets_Debenture))
            
            TotalLTCG=LT_CapgainonAssets_Equity_MF+LT_CapgainonAssets_Debt_MF+LT_CapgainonAssets_Debenture
            content += '<ITRForm:TotalLTCG>'+str(TotalLTCG)+'</ITRForm:TotalLTCG>\n'
            log.info('========= TotalLTCG ends ========')


            content += '</ITRForm:LongTermCapGain23>\n'
            

        log.info('========= TotScheduleCGFor23 starts ========')
        # TotalLTCG + TotalSTCG
        log.info('TotalLTCG ' +str(TotalLTCG))
        log.info('TotalSTCG ' +str(TotalSTCG))

        response = requests.post(
            salat_url+'/get_CG_income/',
            data={'client_id': C_ID},
        )

        CG_special_rates=response.json()

        TotScheduleCGFor23=CG_special_rates['Income_chargeable_CG_at_applicable_rates']

        log.info('TotScheduleCGFor23 '+str(TotScheduleCGFor23))

        content += '<ITRForm:TotScheduleCGFor23>'+str(TotScheduleCGFor23)+'</ITRForm:TotScheduleCGFor23>\n'
        log.info('========= TotScheduleCGFor23 ends ========')

        log.info('========= DeducClaimInfo starts ========')
        content += '<ITRForm:DeducClaimInfo>\n'
        content += '<ITRForm:TotDeductClaim>0</ITRForm:TotDeductClaim>\n'
        content += '</ITRForm:DeducClaimInfo>\n'
        log.info('========= DeducClaimInfo ends ========')


        log.info('========= CurrYrLosses starts ========')
        content += '<ITRForm:CurrYrLosses>\n'

        log.info('========CurrYrLosses => InLossSetOff starts===')
        content += '<ITRForm:InLossSetOff>\n'
        content += '<ITRForm:TypeOfCapGain>LOSS_TO_BE_SET_OFF</ITRForm:TypeOfCapGain>\n'

        # abs[min(0, Shares_ST_CG) + min(0, EMF_ST_CG)]
        Shares_ST_CG=int(st_shares_gain)
        EMF_ST_CG=int(st_equity_gain)
        E4 = StclSetoff15Per=abs(min(0, Shares_ST_CG) + min(0, EMF_ST_CG))
        content += '<ITRForm:StclSetoff15Per>'+str(StclSetoff15Per)+'</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        
        log.info('StclSetoff15Per = E4'+str(StclSetoff15Per))
        # abs(min(0, DebtMF_ST_CG) + min(0, Debenture_ST_CG))
        DebtMF_ST_CG=int(st_debt_MF_gain)
        Debenture_ST_CG=int(st_listed_d_gain)
        G4 = StclSetoffAppRate=abs(min(0, DebtMF_ST_CG) + min(0, Debenture_ST_CG))
        content += '<ITRForm:StclSetoffAppRate>'+str(StclSetoffAppRate)+'</ITRForm:StclSetoffAppRate>\n'
        
        log.info('StclSetoffAppRate = G4'+str(StclSetoffAppRate))

        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'

        # abs(min(0, Shares_LT_CG) + min(0, EMF_LT_CG) + min(0, Debenture_ST_CG))
        Shares_LT_CG=int(lt_shares_gain)
        EMF_LT_CG=int(lt_equity_gain)

        # log.info('Shares_LT_CG '+str(Shares_LT_CG))
        # log.info('EMF_LT_CG '+str(EMF_LT_CG))
        # log.info('Debenture_ST_CG '+str(Debenture_ST_CG))
        # I4 = 
        I4 = LtclSetOff10Per=abs(min(0, Shares_LT_CG) + min(0, EMF_LT_CG) + min(0, Debenture_ST_CG))
        content += '<ITRForm:LtclSetOff10Per>'+str(LtclSetOff10Per)+'</ITRForm:LtclSetOff10Per>\n'

        log.info('LtclSetOff10Per = I4 '+str(LtclSetOff10Per))
        # abs(min(0, DebtMF_LT_CG))
        DebtMF_LT_CG=int(lt_debt_MF_gain)
        J4 = LtclSetOff20Per=abs(min(0, DebtMF_LT_CG))
        content += '<ITRForm:LtclSetOff20Per>'+str(LtclSetOff20Per)+'</ITRForm:LtclSetOff20Per>\n'

        log.info('LtclSetOff20Per = J4 '+str(LtclSetOff20Per))

        content += '<ITRForm:LtclSetOffDTAARate>0</ITRForm:LtclSetOffDTAARate>\n'

        content += '</ITRForm:InLossSetOff>\n'

        log.info('========CurrYrLosses => InLossSetOff ends===')

        log.info('========CurrYrLosses => InStcg15Per starts===')
        content += '<ITRForm:InStcg15Per>\n'
        content += '<ITRForm:TypeOfCapGain>STCG_PER_15</ITRForm:TypeOfCapGain>\n'

        # H433 max (0,Short Term CapgainonAssets Shares & Equity MF)
        D5 = CurrYearIncome= max (0,ST_CapgainonAssets_Share_Equity)
        content += '<ITRForm:CurrYearIncome>'+str(CurrYearIncome)+'</ITRForm:CurrYearIncome>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'

        log.info('CurrYearIncome = D5 '+str(CurrYearIncome))
        # H445 max(0, Short Term CapgainonAssets Debentures + Debt MF's)
        D7 =max(0, ST_CapgainonAssets_Debentures_Debt)
        log.info('D7 '+str(D7))

        # H457 max(0, Long Term CapgainonAssets Equity Shares + Debentures + Equity MF's)
        D9 =max(0,(LT_CapgainonAssets_Equity_MF+LT_CapgainonAssets_Debenture))
        log.info('D9 '+str(D9))

        # H466 max(0, Long Term CapgainonAssets Debt MF's)
        D10=max(0, LT_CapgainonAssets_Debt_MF)

        set_offs=get_setoffs(D5,D7,D9,D10,E4,G4,I4,J4)
        log.info('set_offs '+str(set_offs))

        # d - Refer Sheet 7 calculation
        StclSetoffAppRate=set_offs['setoff_d']
        content += '<ITRForm:StclSetoffAppRate>'+str(StclSetoffAppRate)+'</ITRForm:StclSetoffAppRate>\n'

        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        
        # min(0,CurrYearIncome - StclSetoffAppRate)
        InStcg15Per_CurrYrCapGain=CurrYearIncome - StclSetoffAppRate
        content += '<ITRForm:CurrYrCapGain>'+str(InStcg15Per_CurrYrCapGain)+'</ITRForm:CurrYrCapGain>\n'

        content += '</ITRForm:InStcg15Per>\n'
        log.info('========CurrYrLosses => InStcg15Per ends===')


        log.info('========CurrYrLosses => InStcg30Per starts===')
        content += '<ITRForm:InStcg30Per>\n'
        content += '<ITRForm:TypeOfCapGain>STCG_PER_30</ITRForm:TypeOfCapGain>\n'
        content += '<ITRForm:CurrYearIncome>0</ITRForm:CurrYearIncome>\n'
        content += '<ITRForm:StclSetoff15Per>0</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoffAppRate>0</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        content += '<ITRForm:CurrYrCapGain>0</ITRForm:CurrYrCapGain>\n'
        content += '</ITRForm:InStcg30Per>\n'
        log.info('========CurrYrLosses => InStcg30Per ends===')


        log.info('========CurrYrLosses => InStcgAppRate starts===')
        content += '<ITRForm:InStcgAppRate>\n'
        content += '<ITRForm:TypeOfCapGain>STCG_PER_APP_RATE</ITRForm:TypeOfCapGain>\n'
        
        CurrYearIncome=D7
        content += '<ITRForm:CurrYearIncome>'+str(CurrYearIncome)+'</ITRForm:CurrYearIncome>\n'
        
        StclSetoff15Per=set_offs['setoff_a']
        content += '<ITRForm:StclSetoff15Per>'+str(StclSetoff15Per)+'</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        # content += '<ITRForm:StclSetoffAppRate>0</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        
        # min(0, CurrYearIncome - StclSetoff15Per)
        InStcgAppRate_CurrYrCapGain=CurrYearIncome - StclSetoff15Per
        content += '<ITRForm:CurrYrCapGain>'+str(InStcgAppRate_CurrYrCapGain)+'</ITRForm:CurrYrCapGain>\n'
        content += '</ITRForm:InStcgAppRate>\n'
        log.info('========CurrYrLosses => InStcgAppRate ends===')


        log.info('========CurrYrLosses => InStcgDTAARate starts===')
        content += '<ITRForm:InStcgDTAARate>\n'
        content += '<ITRForm:TypeOfCapGain>STCG_PER_DTAA_RATE</ITRForm:TypeOfCapGain>\n'
        content += '<ITRForm:CurrYearIncome>0</ITRForm:CurrYearIncome>\n'
        content += '<ITRForm:StclSetoff15Per>0</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        content += '<ITRForm:StclSetoffAppRate>0</ITRForm:StclSetoffAppRate>\n'
        # content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        content += '<ITRForm:CurrYrCapGain>0</ITRForm:CurrYrCapGain>\n'
        content += '</ITRForm:InStcgDTAARate>\n'
        log.info('========CurrYrLosses => InStcgDTAARate ends===')


        log.info('========CurrYrLosses => InLtcg10Per starts===')
        content += '<ITRForm:InLtcg10Per>\n'
        content += '<ITRForm:TypeOfCapGain>LTCG_PER_10</ITRForm:TypeOfCapGain>\n'
        
        CurrYearIncome=D9
        content += '<ITRForm:CurrYearIncome>'+str(CurrYearIncome)+'</ITRForm:CurrYearIncome>\n'
        
        StclSetoff15Per=set_offs['setoff_b']
        content += '<ITRForm:StclSetoff15Per>'+str(StclSetoff15Per)+'</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        
        StclSetoffAppRate=set_offs['setoff_e']
        content += '<ITRForm:StclSetoffAppRate>'+str(StclSetoffAppRate)+'</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        
        LtclSetOff20Per=set_offs['setoff_h']
        content += '<ITRForm:LtclSetOff20Per>'+str(LtclSetOff20Per)+'</ITRForm:LtclSetOff20Per>\n'
        content += '<ITRForm:LtclSetOffDTAARate>0</ITRForm:LtclSetOffDTAARate>\n'

        #min(0, CurrYearIncome - StclSetoff15Per - StclSetoffAppRate - LtclSetOff20Per)
        InLtcg10Per_CurrYrCapGain=CurrYearIncome - (StclSetoff15Per + StclSetoffAppRate + LtclSetOff20Per)
        content += '<ITRForm:CurrYrCapGain>'+str(InLtcg10Per_CurrYrCapGain)+'</ITRForm:CurrYrCapGain>\n'
        content += '</ITRForm:InLtcg10Per>\n'
        log.info('========CurrYrLosses => InLtcg10Per ends===')




        log.info('========CurrYrLosses => InLtcg20Per starts===')
        content += '<ITRForm:InLtcg20Per>\n'
        content += '<ITRForm:TypeOfCapGain>LTCG_PER_20</ITRForm:TypeOfCapGain>\n'
        
        CurrYearIncome=D10
        content += '<ITRForm:CurrYearIncome>'+str(CurrYearIncome)+'</ITRForm:CurrYearIncome>\n'
        
        StclSetoff15Per=set_offs['setoff_c']
        content += '<ITRForm:StclSetoff15Per>'+str(StclSetoff15Per)+'</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        
        StclSetoffAppRate=set_offs['setoff_f']
        content += '<ITRForm:StclSetoffAppRate>'+str(StclSetoffAppRate)+'</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        
        LtclSetOff10Per=set_offs['setoff_g']
        content += '<ITRForm:LtclSetOff10Per>'+str(LtclSetOff10Per)+'</ITRForm:LtclSetOff10Per>\n'
        content += '<ITRForm:LtclSetOffDTAARate>0</ITRForm:LtclSetOffDTAARate>\n'
        
        #min(0, CurrYearIncome - StclSetoff15Per - StclSetoffAppRate - LtclSetOff10Per)
        InLtcg20Per_CurrYrCapGain=CurrYearIncome - (StclSetoff15Per + StclSetoffAppRate + LtclSetOff10Per)
        content += '<ITRForm:CurrYrCapGain>'+str(InLtcg20Per_CurrYrCapGain)+'</ITRForm:CurrYrCapGain>\n'
        content += '</ITRForm:InLtcg20Per>\n'
        log.info('========CurrYrLosses => InLtcg20Per ends===')



        log.info('========CurrYrLosses => InLtcgDTAARate starts===')
        content += '<ITRForm:InLtcgDTAARate>\n'
        content += '<ITRForm:TypeOfCapGain>LTCG_PER_DTAA_RATE</ITRForm:TypeOfCapGain>\n'
        content += '<ITRForm:CurrYearIncome>0</ITRForm:CurrYearIncome>\n'
        content += '<ITRForm:StclSetoff15Per>0</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        content += '<ITRForm:StclSetoffAppRate>0</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        content += '<ITRForm:LtclSetOff10Per>0</ITRForm:LtclSetOff10Per>\n'
        content += '<ITRForm:LtclSetOff20Per>0</ITRForm:LtclSetOff20Per>\n'
        content += '<ITRForm:CurrYrCapGain>0</ITRForm:CurrYrCapGain>\n'
        content += '</ITRForm:InLtcgDTAARate>\n'
        log.info('========CurrYrLosses => InLtcgDTAARate ends===')



        log.info('========CurrYrLosses => TotLossSetOff starts===')
        content += '<ITRForm:TotLossSetOff>\n'
        content += '<ITRForm:TypeOfCapGain>TOT_LOSS_SET_OFF</ITRForm:TypeOfCapGain>\n'
        
        
        StclSetoff15Per=set_offs['setoff_a']+set_offs['setoff_b']+set_offs['setoff_c']
        content += '<ITRForm:StclSetoff15Per>'+str(StclSetoff15Per)+'</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        
        StclSetoffAppRate=set_offs['setoff_d']+set_offs['setoff_e']+set_offs['setoff_f']
        content += '<ITRForm:StclSetoffAppRate>'+str(StclSetoffAppRate)+'</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        
        LtclSetOff10Per=set_offs['setoff_g']
        content += '<ITRForm:LtclSetOff10Per>'+str(LtclSetOff10Per)+'</ITRForm:LtclSetOff10Per>\n'
        
        LtclSetOff20Per=set_offs['setoff_h']
        content += '<ITRForm:LtclSetOff20Per>'+str(LtclSetOff20Per)+'</ITRForm:LtclSetOff20Per>\n'

        content += '<ITRForm:LtclSetOffDTAARate>0</ITRForm:LtclSetOffDTAARate>\n'
        
        content += '</ITRForm:TotLossSetOff>\n'
        log.info('========CurrYrLosses => TotLossSetOff ends===')



        log.info('========CurrYrLosses => LossRemainSetOff starts===')
        content += '<ITRForm:LossRemainSetOff>\n'
        content += '<ITRForm:TypeOfCapGain>LOSS_REMAIN_SET_OFF</ITRForm:TypeOfCapGain>\n'

        global CFL_common
        
        # min(0, H425 - (a+b+c))
        StclSetoff15Per_global = StclSetoff15Per=max(0, E4 -(set_offs['setoff_a']+set_offs['setoff_b']+set_offs['setoff_c']))
        content += '<ITRForm:StclSetoff15Per>'+str(StclSetoff15Per)+'</ITRForm:StclSetoff15Per>\n'
        content += '<ITRForm:StclSetoff30Per>0</ITRForm:StclSetoff30Per>\n'
        
        CFL_common['StclSetoff15Per_global']=StclSetoff15Per_global
        # min(0, H427 - (d+e+f))
        StclSetoffAppRate_global = StclSetoffAppRate=max(0, G4 - (set_offs['setoff_d']+set_offs['setoff_e']+set_offs['setoff_f']))
        content += '<ITRForm:StclSetoffAppRate>'+str(StclSetoffAppRate)+'</ITRForm:StclSetoffAppRate>\n'
        content += '<ITRForm:StclSetoffDTAARate>0</ITRForm:StclSetoffDTAARate>\n'
        
        CFL_common['StclSetoffAppRate_global']=StclSetoffAppRate_global
        # min(0, H429 - g)
        LtclSetOff10Per_global=LtclSetOff10Per=max(0, I4 - set_offs['setoff_g'])
        content += '<ITRForm:LtclSetOff10Per>'+str(LtclSetOff10Per)+'</ITRForm:LtclSetOff10Per>\n'
        
        CFL_common['LtclSetOff10Per_global']=LtclSetOff10Per_global
        # min(0, H430 - h)
        LtclSetOff20Per_global=LtclSetOff20Per=max(0, J4 -set_offs['setoff_h'])
        content += '<ITRForm:LtclSetOff20Per>'+str(LtclSetOff20Per)+'</ITRForm:LtclSetOff20Per>\n'

        CFL_common['LtclSetOff20Per_global']=LtclSetOff20Per_global
        content += '<ITRForm:LtclSetOffDTAARate>0</ITRForm:LtclSetOffDTAARate>\n'
        
        content += '</ITRForm:LossRemainSetOff>\n'
        log.info('========CurrYrLosses => LossRemainSetOff ends===')
        

        content += '</ITRForm:CurrYrLosses>\n'
        log.info('========= CurrYrLosses ends ========')

        log.info('========= CYLA Setoff calculation starts ========')

        NetSalary=int(sum(calculated_salary_arr))
        # DeductionUs16ia+Profession Tax + Enterta
        DeductionUs16ia=40000.0
        DeductionUS16=DeductionUs16ia+sum(c_professionTax)+sum(c_entertainment)

        TotIncUnderHeadSalaries=NetSalary-DeductionUS16

        IncOfCurYrUnderThatHead=max(0, TotIncUnderHeadSalaries)

        global common


        common['IncOfCurYrUnderThatHead']=IncOfCurYrUnderThatHead

        log.info('IncOfCurYrUnderThatHead '+str(IncOfCurYrUnderThatHead))

        E6=int(IncOfCurYrUnderThatHead)

        common['InStcg15Per_CurrYrCapGain']=InStcg15Per_CurrYrCapGain

        E8=InStcg15Per_CurrYrCapGain

        common['InStcgAppRate_CurrYrCapGain']=InStcgAppRate_CurrYrCapGain

        E10=InStcgAppRate_CurrYrCapGain

        common['InLtcg10Per_CurrYrCapGain']=InLtcg10Per_CurrYrCapGain

        E12=InLtcg10Per_CurrYrCapGain

        common['InLtcg20Per_CurrYrCapGain']=InLtcg20Per_CurrYrCapGain

        E13=InLtcg20Per_CurrYrCapGain

        TotOthSrcNoRaceHorse=int(Interest_Income + Interest_Savings + FD + Other_Income)

        E15=max(0, TotOthSrcNoRaceHorse)

        common['TotOthSrcNoRaceHorse']=E15

        common['TotalIncomeChargeableUnHP']=TotalIncomeChargeableUnHP

        log.info('TotalIncomeChargeableUnHP '+str(TotalIncomeChargeableUnHP))
        TotHPlossCurYr=abs(min(0,TotalIncomeChargeableUnHP))

        common['TotHPlossCurYr']=TotHPlossCurYr

        CYLA_setoffs=get_CYLA_setoff(TotHPlossCurYr,E6,E8,E10,E12,E13,E15)

        log.info(common)
        log.info(CYLA_setoffs)

        # H437
        # IncOfCurYrUnderThatHead=E8
        
        # b- Refer CYLA Sheet
        CYLA_setoff_b=CYLA_setoffs['CYLA_setoff_b']
        HPlossCurYrSetoff=CYLA_setoff_b
        
        # i- Refer CYLA Sheet
        CYLA_setoff_i=CYLA_setoffs['CYLA_setoff_i']
        OthSrcLossNoRaceHorseSetoff=CYLA_setoff_i

        log.info('IncOfCurYrUnderThatHead '+str(IncOfCurYrUnderThatHead))
        
        # short term 10 %
        # H437 - b - i
        H669=InStcg15Per_CurrYrCapGain-HPlossCurYrSetoff-OthSrcLossNoRaceHorseSetoff

        # # # BFLA short term app rate
        # H449 - c - j
        IncOfCurYrUndHeadFromCYLA=InStcgAppRate_CurrYrCapGain-CYLA_setoffs['CYLA_setoff_c']-CYLA_setoffs['CYLA_setoff_j']
        BFlossPrevYrUndSameHeadSetoff=0
        H675=IncOfCurYrUndHeadFromCYLA-BFlossPrevYrUndSameHeadSetoff


        # BFLA long term 10%
        # H464 - d - k
        IncOfCurYrUndHeadFromCYLA=InLtcg10Per_CurrYrCapGain-CYLA_setoffs['CYLA_setoff_d']-CYLA_setoffs['CYLA_setoff_k']
        BFlossPrevYrUndSameHeadSetoff=0
        H681=IncOfCurYrUndHeadFromCYLA-BFlossPrevYrUndSameHeadSetoff


        # # BFLA long term 20%
        # H473 - e - l
        IncOfCurYrUndHeadFromCYLA=InLtcg20Per_CurrYrCapGain-CYLA_setoffs['CYLA_setoff_e']-CYLA_setoffs['CYLA_setoff_l']
        BFlossPrevYrUndSameHeadSetoff=0
        H684=IncOfCurYrUndHeadFromCYLA-BFlossPrevYrUndSameHeadSetoff
    
        log.info('========= CYLA Setoff calculation ends ========')

        log.info('========= AccruOrRecOfCG starts ========')
        content += '<ITRForm:AccruOrRecOfCG>\n'


        log.info('========= AccruOrRecOfCG => ShortTermUnder15Per starts ======')
        content += '<ITRForm:ShortTermUnder15Per>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
        
 
        log.info('H669 '+str(H669))
        # IncOfCurYrAfterSetOffBFLosses  (H437 - b - i) - H668
        Up16Of3To31Of3=H669
        content += '<ITRForm:Up16Of3To31Of3>'+str(Up16Of3To31Of3)+'</ITRForm:Up16Of3To31Of3>\n'
        
        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'
        content += '</ITRForm:ShortTermUnder15Per>\n'
        log.info('========= AccruOrRecOfCG => ShortTermUnder15Per ends ========')


        log.info('========= AccruOrRecOfCG => ShortTermUnder30Per starts ========')
        content += '<ITRForm:ShortTermUnder30Per>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
        content += '<ITRForm:Up16Of3To31Of3>0</ITRForm:Up16Of3To31Of3>\n'

        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'
        content += '</ITRForm:ShortTermUnder30Per>\n'
        log.info('========= AccruOrRecOfCG => ShortTermUnder30Per ends ========')



        log.info('========= AccruOrRecOfCG => ShortTermUnderAppRate starts ========')
        content += '<ITRForm:ShortTermUnderAppRate>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'

        log.info('H675 '+str(H675))
        # IncOfCurYrAfterSetOffBFLosses  (H437 - b - i) - H668
        Up16Of3To31Of3=H675
        content += '<ITRForm:Up16Of3To31Of3>'+str(Up16Of3To31Of3)+'</ITRForm:Up16Of3To31Of3>\n'

        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'
        content += '</ITRForm:ShortTermUnderAppRate>\n'
        log.info('========= AccruOrRecOfCG => ShortTermUnderAppRate ends ========')



        log.info('========= AccruOrRecOfCG => ShortTermUnderDTAARate starts ========')
        content += '<ITRForm:ShortTermUnderDTAARate>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
        content += '<ITRForm:Up16Of3To31Of3>0</ITRForm:Up16Of3To31Of3>\n'

        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'

        content += '</ITRForm:ShortTermUnderDTAARate>\n'
        log.info('========= AccruOrRecOfCG => ShortTermUnderDTAARate ends ========')



        log.info('========= AccruOrRecOfCG => LongTermUnder10Per starts ========')
        content += '<ITRForm:LongTermUnder10Per>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'

        log.info('H681 '+str(H681))
        Up16Of3To31Of3=H681
        content += '<ITRForm:Up16Of3To31Of3>'+str(Up16Of3To31Of3)+'</ITRForm:Up16Of3To31Of3>\n'

        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'
        content += '</ITRForm:LongTermUnder10Per>\n'
        log.info('========= AccruOrRecOfCG => LongTermUnder10Per ends ========')


        log.info('========= AccruOrRecOfCG => LongTermUnder20Per starts ========')
        content += '<ITRForm:LongTermUnder20Per>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
        log.info('H684 '+str(H684))
        Up16Of3To31Of3=H684
        content += '<ITRForm:Up16Of3To31Of3>'+str(Up16Of3To31Of3)+'</ITRForm:Up16Of3To31Of3>\n'

        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'
        content += '</ITRForm:LongTermUnder20Per>\n'
        log.info('========= AccruOrRecOfCG => LongTermUnder20Per ends ========')



        log.info('========= AccruOrRecOfCG => LongTermUnderDTAARate starts ========')
        content += '<ITRForm:LongTermUnderDTAARate>\n'
        # content += '<ITRForm:DateRangeType>\n'
        content += '<ITRForm:DateRange>\n'

        content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
        content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
        content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
        content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
        content += '<ITRForm:Up16Of3To31Of3>0</ITRForm:Up16Of3To31Of3>\n'

        content += '</ITRForm:DateRange>\n'
        # content += '</ITRForm:DateRangeType>\n'
        content += '</ITRForm:LongTermUnderDTAARate>\n'
        log.info('========= AccruOrRecOfCG => LongTermUnderDTAARate ends ========')


        content += '</ITRForm:AccruOrRecOfCG>\n'
        log.info('========= AccruOrRecOfCG ends ========')


        content += '</ITRForm:ScheduleCGFor23>\n'

        # content += ScheduleCYLA()
        # Capital_Gain END
    except Exception as ex:
        log.info('Error in ScheduleCGFor23 ITR2 '+traceback.format_exc())
    log.info('===== ScheduleCGFor23 Block ends =====')
    return content

def ScheduleCYLA():
    global CYLA_setoffs,common
    # global IncOfCurYrUnderThatHead
    content=''
    log.info('===== ScheduleCYLA Block starts =====')
    try:
            content += '<ITRForm:ScheduleCYLA>\n'
            content += '<ITRForm:Salary>\n'

            content += '<ITRForm:IncCYLA>\n'
            # 

            # DeductionUs16ia=40000.0
            # DeductionUS16=DeductionUs16ia+sum(c_professionTax)+sum(c_entertainment)

            # TotIncUnderHeadSalaries=NetSalary-DeductionUS16

            # IncOfCurYrUnderThatHead=max(0, TotIncUnderHeadSalaries)

            # log.info('TotIncUnderHeadSalaries '+str(TotIncUnderHeadSalaries))
            log.info('common '+str(common))

            global CYLA_common

            log.info('IncOfCurYrUnderThatHead '+str(common['IncOfCurYrUnderThatHead']))

            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(int(common['IncOfCurYrUnderThatHead']))+'</ITRForm:IncOfCurYrUnderThatHead>\n'
        
            log.info(CYLA_setoffs)
            # a - Refer CYLA Sheet
            CYLA_setoff_a=CYLA_setoffs['CYLA_setoff_a']
            HPlossCurYrSetoff=CYLA_setoff_a
            if ICU_house_p<0:
                content += '<ITRForm:HPlossCurYrSetoff>'+str(abs(int(ICU_house_p)))+'</ITRForm:HPlossCurYrSetoff>\n'
            else:
                content += '<ITRForm:HPlossCurYrSetoff>0</ITRForm:HPlossCurYrSetoff>\n'
            

            # g- Refer CYLA Sheet
            CYLA_setoff_g=CYLA_setoffs['CYLA_setoff_g']
            OthSrcLossNoRaceHorseSetoff=CYLA_setoff_g
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>'+str(int(OthSrcLossNoRaceHorseSetoff))+'</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            
            # D614 - D615 - D616
            CYLA_S_IncOfCurYrAfterSetOff=int(common['IncOfCurYrUnderThatHead'])-CYLA_setoff_a-CYLA_setoff_g
            CYLA_common['CYLA_S_IncOfCurYrAfterSetOff']=CYLA_S_IncOfCurYrAfterSetOff
            
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(CYLA_S_IncOfCurYrAfterSetOff)+'</ITRForm:IncOfCurYrAfterSetOff>\n'
           
            
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:Salary>\n'


            content += '<ITRForm:HP>\n'
            content += '<ITRForm:IncCYLA>\n'
            
            # max(0, TotalIncomeChargeableUnHP)
            IncOfCurYrUnderThatHead=max(0,common['TotalIncomeChargeableUnHP'])
            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(int(IncOfCurYrUnderThatHead))+'</ITRForm:IncOfCurYrUnderThatHead>\n'
            
            # h- Refer CYLA Sheet
            CYLA_setoff_h=CYLA_setoffs['CYLA_setoff_h']
            OthSrcLossNoRaceHorseSetoff=CYLA_setoff_h
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>'+str(int(OthSrcLossNoRaceHorseSetoff))+'</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            
            # H618 - H619
            CYLA_HP_IncOfCurYrAfterSetOff=IncOfCurYrUnderThatHead-OthSrcLossNoRaceHorseSetoff
            CYLA_common['CYLA_HP_IncOfCurYrAfterSetOff']=CYLA_HP_IncOfCurYrAfterSetOff
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(int(CYLA_HP_IncOfCurYrAfterSetOff))+'</ITRForm:IncOfCurYrAfterSetOff>\n'
            
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:HP>\n'
            

            content += '<ITRForm:STCG15Per>\n'
            content += '<ITRForm:IncCYLA>\n'

            # H437 
            log.info('InStcg15Per_CurrYrCapGain '+str(common['InStcg15Per_CurrYrCapGain']))
            IncOfCurYrUnderThatHead=common['InStcg15Per_CurrYrCapGain']
            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(int(IncOfCurYrUnderThatHead))+'</ITRForm:IncOfCurYrUnderThatHead>\n'
            
            # b- Refer CYLA Sheet
            CYLA_setoff_b=CYLA_setoffs['CYLA_setoff_b']
            HPlossCurYrSetoff=CYLA_setoff_b
            content += '<ITRForm:HPlossCurYrSetoff>'+str(int(HPlossCurYrSetoff))+'</ITRForm:HPlossCurYrSetoff>\n'
            
            # i- Refer CYLA Sheet
            CYLA_setoff_i=CYLA_setoffs['CYLA_setoff_i']
            OthSrcLossNoRaceHorseSetoff=CYLA_setoff_i
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>'+str(int(OthSrcLossNoRaceHorseSetoff))+'</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            
            # H437 - b - i
            IncOfCurYrAfterSetOff=IncOfCurYrUnderThatHead-HPlossCurYrSetoff-OthSrcLossNoRaceHorseSetoff
            CYLA_common['CYLA_STCG15Per_IncOfCurYrAfterSetOff']=IncOfCurYrAfterSetOff
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(int(IncOfCurYrAfterSetOff))+'</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:STCG15Per>\n'
            

            content += '<ITRForm:STCG30Per>\n'
            content += '<ITRForm:IncCYLA>\n'
            content += '<ITRForm:IncOfCurYrUnderThatHead>0</ITRForm:IncOfCurYrUnderThatHead>\n'
            content += '<ITRForm:HPlossCurYrSetoff>0</ITRForm:HPlossCurYrSetoff>\n'
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>0</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            CYLA_common['CYLA_STCG30Per_IncOfCurYrAfterSetOff']=0
            content += '<ITRForm:IncOfCurYrAfterSetOff>0</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:STCG30Per>\n'
            

            content += '<ITRForm:STCGAppRate>\n'
            content += '<ITRForm:IncCYLA>\n'

            # H449
            log.info('InStcg15Per_CurrYrCapGain '+str(common['InStcgAppRate_CurrYrCapGain']))
            IncOfCurYrUnderThatHead=common['InStcgAppRate_CurrYrCapGain']
            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(int(IncOfCurYrUnderThatHead))+'</ITRForm:IncOfCurYrUnderThatHead>\n'
            
            # c- Refer CYLA Sheet
            CYLA_setoff_c=CYLA_setoffs['CYLA_setoff_c']
            HPlossCurYrSetoff=CYLA_setoff_c
            content += '<ITRForm:HPlossCurYrSetoff>'+str(int(HPlossCurYrSetoff))+'</ITRForm:HPlossCurYrSetoff>\n'
            
            # J- Refer CYLA Sheet
            CYLA_setoff_j=CYLA_setoffs['CYLA_setoff_j']
            OthSrcLossNoRaceHorseSetoff=CYLA_setoff_j
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>'+str(int(OthSrcLossNoRaceHorseSetoff))+'</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            
            # H449 - c - j
            IncOfCurYrAfterSetOff=IncOfCurYrUnderThatHead-HPlossCurYrSetoff-OthSrcLossNoRaceHorseSetoff
            CYLA_common['CYLA_STCGAppRate_IncOfCurYrAfterSetOff']=IncOfCurYrAfterSetOff
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(int(IncOfCurYrAfterSetOff))+'</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:STCGAppRate>\n'


            content += '<ITRForm:STCGDTAARate>\n'
            content += '<ITRForm:IncCYLA>\n'
            content += '<ITRForm:IncOfCurYrUnderThatHead>0</ITRForm:IncOfCurYrUnderThatHead>\n'
            content += '<ITRForm:HPlossCurYrSetoff>0</ITRForm:HPlossCurYrSetoff>\n'
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>0</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            CYLA_common['CYLA_STCGDTAARate_IncOfCurYrAfterSetOff']=0
            content += '<ITRForm:IncOfCurYrAfterSetOff>0</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:STCGDTAARate>\n'
            

            content += '<ITRForm:LTCG10Per>\n'
            content += '<ITRForm:IncCYLA>\n'

            # H464
            log.info('InLtcg10Per_CurrYrCapGain '+str(common['InLtcg10Per_CurrYrCapGain']))
            IncOfCurYrUnderThatHead=common['InLtcg10Per_CurrYrCapGain']
            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(int(IncOfCurYrUnderThatHead))+'</ITRForm:IncOfCurYrUnderThatHead>\n'
            
            # d - Refer CYLA Sheet
            CYLA_setoff_d=CYLA_setoffs['CYLA_setoff_d']
            HPlossCurYrSetoff=CYLA_setoff_d
            content += '<ITRForm:HPlossCurYrSetoff>'+str(int(HPlossCurYrSetoff))+'</ITRForm:HPlossCurYrSetoff>\n'
            
            # k - Refer CYLA Sheet
            CYLA_setoff_k=CYLA_setoffs['CYLA_setoff_k']
            OthSrcLossNoRaceHorseSetoff=CYLA_setoff_k
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>'+str(int(OthSrcLossNoRaceHorseSetoff))+'</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            
            # H464 - d - k
            IncOfCurYrAfterSetOff=IncOfCurYrUnderThatHead-HPlossCurYrSetoff-OthSrcLossNoRaceHorseSetoff
            CYLA_common['CYLA_LTCG10Per_IncOfCurYrAfterSetOff']=IncOfCurYrAfterSetOff
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(int(IncOfCurYrAfterSetOff))+'</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:LTCG10Per>\n'
            

            content += '<ITRForm:LTCG20Per>\n'
            content += '<ITRForm:IncCYLA>\n'

            # H473
            log.info('InLtcg20Per_CurrYrCapGain '+str(common['InLtcg20Per_CurrYrCapGain']))
            IncOfCurYrUnderThatHead=common['InLtcg20Per_CurrYrCapGain']
            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(int(IncOfCurYrUnderThatHead))+'</ITRForm:IncOfCurYrUnderThatHead>\n'
            
            # e - Refer CYLA Sheet
            CYLA_setoff_e=CYLA_setoffs['CYLA_setoff_e']
            HPlossCurYrSetoff=CYLA_setoff_e
            content += '<ITRForm:HPlossCurYrSetoff>'+str(int(HPlossCurYrSetoff))+'</ITRForm:HPlossCurYrSetoff>\n'
            
            # l - Refer CYLA Sheet
            CYLA_setoff_l=CYLA_setoffs['CYLA_setoff_l']
            OthSrcLossNoRaceHorseSetoff=CYLA_setoff_l
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>'+str(int(OthSrcLossNoRaceHorseSetoff))+'</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            
            # H473 - e - l
            IncOfCurYrAfterSetOff=IncOfCurYrUnderThatHead-HPlossCurYrSetoff-OthSrcLossNoRaceHorseSetoff
            CYLA_common['CYLA_LTCG20Per_IncOfCurYrAfterSetOff']=IncOfCurYrAfterSetOff
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(int(IncOfCurYrAfterSetOff))+'</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:LTCG20Per>\n'


            content += '<ITRForm:LTCGDTAARate>\n'
            content += '<ITRForm:IncCYLA>\n'
            content += '<ITRForm:IncOfCurYrUnderThatHead>0</ITRForm:IncOfCurYrUnderThatHead>\n'
            content += '<ITRForm:HPlossCurYrSetoff>0</ITRForm:HPlossCurYrSetoff>\n'
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>0</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            CYLA_common['CYLA_LTCGDTAARate_IncOfCurYrAfterSetOff']=0
            content += '<ITRForm:IncOfCurYrAfterSetOff>0</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:LTCGDTAARate>\n'


            content += '<ITRForm:OthSrcExclRaceHorse>\n'
            content += '<ITRForm:IncCYLA>\n'
            # max (0, TotOthSrcNoRaceHorse)
            IncOfCurYrUnderThatHead=max(0, common['TotOthSrcNoRaceHorse'])
            content += '<ITRForm:IncOfCurYrUnderThatHead>'+str(IncOfCurYrUnderThatHead)+'</ITRForm:IncOfCurYrUnderThatHead>\n'
            
            # f - Refer CYLA Sheet
            CYLA_setoff_f=CYLA_setoffs['CYLA_setoff_f']
            HPlossCurYrSetoff=CYLA_setoff_f
            content += '<ITRForm:HPlossCurYrSetoff>'+str(HPlossCurYrSetoff)+'</ITRForm:HPlossCurYrSetoff>\n'
            
            # H649 - f
            IncOfCurYrAfterSetOff=IncOfCurYrUnderThatHead-HPlossCurYrSetoff
            CYLA_common['CYLA_OthSrcExclRaceHorse_IncOfCurYrAfterSetOff']=IncOfCurYrAfterSetOff
            content += '<ITRForm:IncOfCurYrAfterSetOff>'+str(IncOfCurYrAfterSetOff)+'</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:OthSrcExclRaceHorse>\n'


            content += '<ITRForm:OthSrcRaceHorse>\n'
            content += '<ITRForm:IncCYLA>\n'
            content += '<ITRForm:IncOfCurYrUnderThatHead>0</ITRForm:IncOfCurYrUnderThatHead>\n'
            content += '<ITRForm:HPlossCurYrSetoff>0</ITRForm:HPlossCurYrSetoff>\n'
            content += '<ITRForm:OthSrcLossNoRaceHorseSetoff>0</ITRForm:OthSrcLossNoRaceHorseSetoff>\n'
            CYLA_common['CYLA_OthSrcRaceHorse_IncOfCurYrAfterSetOff']=0
            content += '<ITRForm:IncOfCurYrAfterSetOff>0</ITRForm:IncOfCurYrAfterSetOff>\n'
            content += '</ITRForm:IncCYLA>\n'
            content += '</ITRForm:OthSrcRaceHorse>\n'


            content += '<ITRForm:TotalCurYr>\n'

            # abs(min(0,TotalIncomeChargeableUnHP))
            content += '<ITRForm:TotHPlossCurYr>'+str(common['TotHPlossCurYr'])+'</ITRForm:TotHPlossCurYr>\n'
        
            # abs(min(0,TotOthSrcNoRaceHorse))
            TotOthSrcLossNoRaceHorse=abs(min(0,TotOthSrcNoRaceHorse))
            content += '<ITRForm:TotOthSrcLossNoRaceHorse>'+str(TotOthSrcLossNoRaceHorse)+'</ITRForm:TotOthSrcLossNoRaceHorse>\n'
            content += '</ITRForm:TotalCurYr>\n'

            
            content += '<ITRForm:TotalLossSetOff>\n'
            # a+b+c+d+e+f
            if ICU_house_p<0:
                TotHPlossCurYrSetoff=CYLA_setoffs['CYLA_setoff_a']+CYLA_setoffs['CYLA_setoff_b']+CYLA_setoffs['CYLA_setoff_c']+CYLA_setoffs['CYLA_setoff_d']+CYLA_setoffs['CYLA_setoff_e']+CYLA_setoffs['CYLA_setoff_f']
                content += '<ITRForm:TotHPlossCurYrSetoff>'+str(int(TotHPlossCurYrSetoff))+'</ITRForm:TotHPlossCurYrSetoff>\n'
            else:
                TotHPlossCurYrSetoff=0
                content += '<ITRForm:TotHPlossCurYrSetoff>'+str(int(TotHPlossCurYrSetoff))+'</ITRForm:TotHPlossCurYrSetoff>\n'
            
            CYLA_common['TotHPlossCurYrSetoff']=TotHPlossCurYrSetoff
            log.info('a+b+c+d+e+f '+str(TotHPlossCurYrSetoff))
            # g+h+i+j+k+l
            TotOthSrcLossNoRaceHorseSetoff=CYLA_setoffs['CYLA_setoff_g']+CYLA_setoffs['CYLA_setoff_h']+CYLA_setoffs['CYLA_setoff_i']+CYLA_setoffs['CYLA_setoff_j']+CYLA_setoffs['CYLA_setoff_k']+CYLA_setoffs['CYLA_setoff_l']
          
            content += '<ITRForm:TotOthSrcLossNoRaceHorseSetoff>'+str(TotOthSrcLossNoRaceHorseSetoff)+'</ITRForm:TotOthSrcLossNoRaceHorseSetoff>\n'
            content += '</ITRForm:TotalLossSetOff>\n'

            log.info('g+h+i+j+k+l '+str(TotOthSrcLossNoRaceHorseSetoff))

            content += '<ITRForm:LossRemAftSetOff>\n'
            #H653 - H655 
            BalHPlossCurYrAftSetoff=0
            content += '<ITRForm:BalHPlossCurYrAftSetoff>'+str(BalHPlossCurYrAftSetoff)+'</ITRForm:BalHPlossCurYrAftSetoff>\n'
            
            # H654 - H656
            BalOthSrcLossNoRaceHorseAftSetoff=0
            content += '<ITRForm:BalOthSrcLossNoRaceHorseAftSetoff>'+str(BalOthSrcLossNoRaceHorseAftSetoff)+'</ITRForm:BalOthSrcLossNoRaceHorseAftSetoff>\n'
            content += '</ITRForm:LossRemAftSetOff>\n'
            
            content += '</ITRForm:ScheduleCYLA>\n'

    except Exception as ex:
        log.info('Error in ScheduleCYLA ITR2 '+traceback.format_exc())
    log.info('===== ScheduleCYLA Block ends =====')
    return content

def ScheduleBFLA():
    global CYLA_setoffs,CYLA_common
    content=''
    log.info('===== ScheduleBFLA Block starts =====')
    try:
            content += '<ITRForm:ScheduleBFLA>\n'

            content += '<ITRForm:Salary>\n'
            content += '<ITRForm:IncBFLA>\n'


            log.info('CYLA_common '+str(CYLA_common))
            # D614 - D615 - D616
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(int(CYLA_common['CYLA_S_IncOfCurYrAfterSetOff']))+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            # D614 - D615 - D616
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(int(CYLA_common['CYLA_S_IncOfCurYrAfterSetOff']))+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:Salary>\n'

            salary_IncOfCurYrUndHeadFromCYLA=CYLA_S_IncOfCurYrAfterSetOff

            content += '<ITRForm:HP>\n'
            content += '<ITRForm:IncBFLA>\n'
            
            # H618 - H619
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(CYLA_common['CYLA_HP_IncOfCurYrAfterSetOff'])+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            BFlossPrevYrUndSameHeadSetoff=0
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>'+str(BFlossPrevYrUndSameHeadSetoff)+'</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            
            # (H618 - H619)-H665
            IncOfCurYrAfterSetOffBFLosses=CYLA_common['CYLA_HP_IncOfCurYrAfterSetOff']-BFlossPrevYrUndSameHeadSetoff
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(IncOfCurYrAfterSetOffBFLosses)+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:HP>\n'

            HP_IncOfCurYrUndHeadFromCYLA=CYLA_HP_IncOfCurYrAfterSetOff

            content += '<ITRForm:STCG15Per>\n'
            content += '<ITRForm:IncBFLA>\n'

            # currentyrloss_InStcg15Per - b - i
            log.info(CYLA_setoffs)
            IncOfCurYrUndHeadFromCYLA=CYLA_common['CYLA_STCG15Per_IncOfCurYrAfterSetOff']
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(IncOfCurYrUndHeadFromCYLA)+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            BFlossPrevYrUndSameHeadSetoff=0
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>'+str(BFlossPrevYrUndSameHeadSetoff)+'</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            
            IncOfCurYrAfterSetOffBFLosses=CYLA_common['CYLA_STCG15Per_IncOfCurYrAfterSetOff']-BFlossPrevYrUndSameHeadSetoff
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(IncOfCurYrAfterSetOffBFLosses)+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:STCG15Per>\n'

            STCG15Per_IncOfCurYrUndHeadFromCYLA=IncOfCurYrUndHeadFromCYLA

            content += '<ITRForm:STCG30Per>\n'
            content += '<ITRForm:IncBFLA>\n'
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>0</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>0</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>0</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:STCG30Per>\n'

            content += '<ITRForm:STCGAppRate>\n'
            content += '<ITRForm:IncBFLA>\n'
            # H449 - c - j
            IncOfCurYrUndHeadFromCYLA=CYLA_common['CYLA_STCGAppRate_IncOfCurYrAfterSetOff']
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(IncOfCurYrUndHeadFromCYLA)+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            BFlossPrevYrUndSameHeadSetoff=0
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>'+str(BFlossPrevYrUndSameHeadSetoff)+'</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            
            IncOfCurYrAfterSetOffBFLosses=IncOfCurYrUndHeadFromCYLA-BFlossPrevYrUndSameHeadSetoff
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(IncOfCurYrAfterSetOffBFLosses)+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:STCGAppRate>\n'

            content += '<ITRForm:STCGDTAARate>\n'
            content += '<ITRForm:IncBFLA>\n'
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>0</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>0</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>0</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:STCGDTAARate>\n'

            STCGAppRate_IncOfCurYrUndHeadFromCYLA=IncOfCurYrUndHeadFromCYLA

            content += '<ITRForm:LTCG10Per>\n'
            content += '<ITRForm:IncBFLA>\n'

            # H464 - d - k
            IncOfCurYrUndHeadFromCYLA=CYLA_common['CYLA_LTCG10Per_IncOfCurYrAfterSetOff']
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(IncOfCurYrUndHeadFromCYLA)+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            BFlossPrevYrUndSameHeadSetoff=0
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>'+str(BFlossPrevYrUndSameHeadSetoff)+'</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            
            IncOfCurYrAfterSetOffBFLosses=IncOfCurYrUndHeadFromCYLA-BFlossPrevYrUndSameHeadSetoff
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(IncOfCurYrAfterSetOffBFLosses)+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:LTCG10Per>\n'

            LTCG10Per_IncOfCurYrUndHeadFromCYLA=IncOfCurYrUndHeadFromCYLA

            content += '<ITRForm:LTCG20Per>\n'
            content += '<ITRForm:IncBFLA>\n'
            # H473 - e - l
            IncOfCurYrUndHeadFromCYLA=CYLA_common['CYLA_LTCG20Per_IncOfCurYrAfterSetOff']
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(IncOfCurYrUndHeadFromCYLA)+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            BFlossPrevYrUndSameHeadSetoff=0
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>'+str(BFlossPrevYrUndSameHeadSetoff)+'</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            
            IncOfCurYrAfterSetOffBFLosses=IncOfCurYrUndHeadFromCYLA-BFlossPrevYrUndSameHeadSetoff
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(IncOfCurYrAfterSetOffBFLosses)+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:LTCG20Per>\n'

            LTCG20Per_IncOfCurYrUndHeadFromCYLA=IncOfCurYrUndHeadFromCYLA

            content += '<ITRForm:LTCGDTAARate>\n'
            content += '<ITRForm:IncBFLA>\n'
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>0</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>0</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>0</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:LTCGDTAARate>\n'


            content += '<ITRForm:OthSrcExclRaceHorse>\n'
            content += '<ITRForm:IncBFLA>\n'

            # H649 - f
            IncOfCurYrUndHeadFromCYLA=CYLA_common['CYLA_OthSrcExclRaceHorse_IncOfCurYrAfterSetOff']
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>'+str(IncOfCurYrUndHeadFromCYLA)+'</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            
            IncOfCurYrAfterSetOffBFLosses=IncOfCurYrUndHeadFromCYLA
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>'+str(IncOfCurYrAfterSetOffBFLosses)+'</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:OthSrcExclRaceHorse>\n'

            OthSrcExclRaceHorse_IncOfCurYrUndHeadFromCYLA=IncOfCurYrUndHeadFromCYLA

            content += '<ITRForm:OthSrcRaceHorse>\n'
            content += '<ITRForm:IncBFLA>\n'
            content += '<ITRForm:IncOfCurYrUndHeadFromCYLA>0</ITRForm:IncOfCurYrUndHeadFromCYLA>\n'
            content += '<ITRForm:BFlossPrevYrUndSameHeadSetoff>0</ITRForm:BFlossPrevYrUndSameHeadSetoff>\n'
            content += '<ITRForm:IncOfCurYrAfterSetOffBFLosses>0</ITRForm:IncOfCurYrAfterSetOffBFLosses>\n'
            content += '</ITRForm:IncBFLA>\n'
            content += '</ITRForm:OthSrcRaceHorse>\n'

            content += '<ITRForm:TotalBFLossSetOff>\n'
            content += '<ITRForm:TotBFLossSetoff>0</ITRForm:TotBFLossSetoff>\n'
            content += '</ITRForm:TotalBFLossSetOff>\n'

            # Sum of H6 to H15 Refer Sheet CYLA
            # log.info('salary_IncOfCurYrUndHeadFromCYLA '+str(salary_IncOfCurYrUndHeadFromCYLA))
            # log.info('HP_IncOfCurYrUndHeadFromCYLA '+str(HP_IncOfCurYrUndHeadFromCYLA))
            # log.info('STCG15Per_IncOfCurYrUndHeadFromCYLA '+str(STCG15Per_IncOfCurYrUndHeadFromCYLA))
            # log.info('STCGAppRate_IncOfCurYrUndHeadFromCYLA '+str(STCGAppRate_IncOfCurYrUndHeadFromCYLA))
            # log.info('LTCG10Per_IncOfCurYrUndHeadFromCYLA '+str(LTCG10Per_IncOfCurYrUndHeadFromCYLA))
            # log.info('LTCG20Per_IncOfCurYrUndHeadFromCYLA '+str(LTCG20Per_IncOfCurYrUndHeadFromCYLA))
            # log.info('OthSrcExclRaceHorse_IncOfCurYrUndHeadFromCYLA '+str(OthSrcExclRaceHorse_IncOfCurYrUndHeadFromCYLA))
            IncomeOfCurrYrAftCYLABFLA=CYLA_common['TotHPlossCurYrSetoff']

            # {'CYLA_OthSrcRaceHorse_IncOfCurYrAfterSetOff': 0, 'CYLA_STCG30Per_IncOfCurYrAfterSetOff': 0, 'CYLA_STCGDTAARate_IncOfCurYrAfterSetOff': 0, 'CYLA_HP_IncOfCurYrAfterSetOff': 0, 'CYLA_STCG15Per_IncOfCurYrAfterSetOff': 10000, 'CYLA_S_IncOfCurYrAfterSetOff': 2497930.0, 'TotHPlossCurYrSetoff': 94570, 'CYLA_OthSrcExclRaceHorse_IncOfCurYrAfterSetOff': 21000, 'CYLA_LTCG20Per_IncOfCurYrAfterSetOff': 35000, 'CYLA_LTCG10Per_IncOfCurYrAfterSetOff': 152000, 'CYLA_STCGAppRate_IncOfCurYrAfterSetOff': 10000, 'CYLA_LTCGDTAARate_IncOfCurYrAfterSetOff': 0}
            IncomeOfCurrYrAftCYLABFLA=CYLA_common['CYLA_OthSrcRaceHorse_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_STCG30Per_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_STCGDTAARate_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_HP_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_STCG15Per_IncOfCurYrAfterSetOff']+int(CYLA_common['CYLA_S_IncOfCurYrAfterSetOff'])+CYLA_common['CYLA_OthSrcExclRaceHorse_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_LTCG20Per_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_LTCG10Per_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_STCGAppRate_IncOfCurYrAfterSetOff']+CYLA_common['CYLA_LTCGDTAARate_IncOfCurYrAfterSetOff']
            
            log.info('IncomeOfCurrYrAftCYLABFLA '+str(IncomeOfCurrYrAftCYLABFLA))

            content += '<ITRForm:IncomeOfCurrYrAftCYLABFLA>'+str(int(IncomeOfCurrYrAftCYLABFLA))+'</ITRForm:IncomeOfCurrYrAftCYLABFLA>\n'
            content += '</ITRForm:ScheduleBFLA>\n'

            # content += ScheduleCFL()

    except Exception as ex:
        log.info('Error in ScheduleBFLA ITR2 '+traceback.format_exc())
    log.info('===== ScheduleBFLA Block ends =====')
    return content


def ScheduleCFL():
    content=''
    global CFL_common
    log.info('===== ScheduleCFL Block starts =====')
    try:
            content += '<ITRForm:ScheduleCFL>\n'
            content += '<ITRForm:TotalOfBFLossesEarlierYrs>\n'
            content += '<ITRForm:LossSummaryDetail>\n'
            content += '<ITRForm:HPLossCF>0</ITRForm:HPLossCF>\n'
            content += '<ITRForm:STCGLossCF>0</ITRForm:STCGLossCF>\n'
            content += '<ITRForm:LTCGLossCF>0</ITRForm:LTCGLossCF>\n'
            content += '<ITRForm:OthSrcLossRaceHorseCF>0</ITRForm:OthSrcLossRaceHorseCF>\n'
            content += '</ITRForm:LossSummaryDetail>\n'
            content += '</ITRForm:TotalOfBFLossesEarlierYrs>\n'

            content += '<ITRForm:AdjTotBFLossInBFLA>\n'
            content += '<ITRForm:LossSummaryDetail>\n'
            content += '<ITRForm:HPLossCF>0</ITRForm:HPLossCF>\n'
            content += '<ITRForm:STCGLossCF>0</ITRForm:STCGLossCF>\n'
            content += '<ITRForm:LTCGLossCF>0</ITRForm:LTCGLossCF>\n'
            content += '<ITRForm:OthSrcLossRaceHorseCF>0</ITRForm:OthSrcLossRaceHorseCF>\n'
            content += '</ITRForm:LossSummaryDetail>\n'
            content += '</ITRForm:AdjTotBFLossInBFLA>\n'

            content += '<ITRForm:CurrentAYloss>\n'
            content += '<ITRForm:LossSummaryDetail>\n'

            log.info(CFL_common)

            # F18 of Sheet CYLA
            # F5-F17
            # HP_set_off-(a+b+c+d+e+f)
            HPLossCF=TotHPlossCurYr-TotHPlossCurYrSetoff
            content += '<ITRForm:HPLossCF>'+str(HPLossCF)+'</ITRForm:HPLossCF>\n'

            # H492 + H494
            STCGLossCF=CFL_common['StclSetoff15Per_global']-CFL_common['StclSetoffAppRate_global']
            content += '<ITRForm:STCGLossCF>'+str(STCGLossCF)+'</ITRForm:STCGLossCF>\n'
            
            # H496 + H497
            LTCGLossCF=CFL_common['LtclSetOff10Per_global']-CFL_common['LtclSetOff20Per_global']
            content += '<ITRForm:LTCGLossCF>'+str(LTCGLossCF)+'</ITRForm:LTCGLossCF>\n'

            OthSrcLossRaceHorseCF=0
            content += '<ITRForm:OthSrcLossRaceHorseCF>0</ITRForm:OthSrcLossRaceHorseCF>\n'
            content += '</ITRForm:LossSummaryDetail>\n'
            content += '</ITRForm:CurrentAYloss>\n'

            log.info('HPLossCF '+str(HPLossCF))
            log.info('STCGLossCF '+str(STCGLossCF))
            log.info('LTCGLossCF '+str(LTCGLossCF))
            log.info('OthSrcLossRaceHorseCF '+str(OthSrcLossRaceHorseCF))

            Total_CurrentAYloss=int(HPLossCF)+int(STCGLossCF)+int(LTCGLossCF)+int(OthSrcLossRaceHorseCF)

            log.info('Total_CurrentAYloss '+str(Total_CurrentAYloss))

            content += '<ITRForm:TotalLossCFSummary>\n'
            content += '<ITRForm:LossSummaryDetail>\n'
            content += '<ITRForm:HPLossCF>'+str(HPLossCF)+'</ITRForm:HPLossCF>\n'
            content += '<ITRForm:STCGLossCF>'+str(STCGLossCF)+'</ITRForm:STCGLossCF>\n'
            content += '<ITRForm:LTCGLossCF>'+str(LTCGLossCF)+'</ITRForm:LTCGLossCF>\n'
            content += '<ITRForm:OthSrcLossRaceHorseCF>0</ITRForm:OthSrcLossRaceHorseCF>\n'
            content += '</ITRForm:LossSummaryDetail>\n'
            content += '</ITRForm:TotalLossCFSummary>\n'

            content += '</ITRForm:ScheduleCFL>\n'
    except Exception as ex:
        log.info('Error in ScheduleCFL ITR2 '+traceback.format_exc())
    log.info('===== ScheduleCFL Block ends =====')
    return content


def ScheduleOS():
    content=''
    log.info('===== ScheduleOS Block starts =====')
    try:
        # OthersGrossDtls as it is
        if 1==1:
            content += '<ITRForm:ScheduleOS>\n'

            log.info('===== ScheduleOS => IncOthThanOwnRaceHorse starts =====')
            content += '<ITRForm:IncOthThanOwnRaceHorse>\n'

            # IntrstFrmSavingBank+ IntrstFrmTermDeposit + IntrstFrmIncmTaxRefund + IntrstFrmOthers
            InterestGross=int(Interest_Income + Interest_Savings + FD)
            IntrstFrmOthers=int(Other_Income)

            log.info('InterestGross '+str(InterestGross))
            log.info('IntrstFrmOthers '+str(IntrstFrmOthers))
            # InterestGross + IntrstFrmOthers
            GrossIncChrgblTaxAtAppRate=InterestGross+IntrstFrmOthers
            
            log.info('GrossIncChrgblTaxAtAppRate '+str(GrossIncChrgblTaxAtAppRate))

            content += '<ITRForm:GrossIncChrgblTaxAtAppRate>'+str(GrossIncChrgblTaxAtAppRate)+'</ITRForm:GrossIncChrgblTaxAtAppRate>\n'

            content += '<ITRForm:DividendGross>0</ITRForm:DividendGross>\n'
            
            content += '<ITRForm:InterestGross>'+str(GrossIncChrgblTaxAtAppRate)+'</ITRForm:InterestGross>\n'
            
            IntrstFrmSavingBank=int(Interest_Savings)
            log.info('IntrstFrmSavingBank '+str(IntrstFrmSavingBank))
            content += '<ITRForm:IntrstFrmSavingBank>'+str(IntrstFrmSavingBank)+'</ITRForm:IntrstFrmSavingBank>\n'
            
            IntrstFrmTermDeposit=int(FD)
            log.info('IntrstFrmTermDeposit '+str(IntrstFrmTermDeposit))
            content += '<ITRForm:IntrstFrmTermDeposit>'+str(IntrstFrmTermDeposit)+'</ITRForm:IntrstFrmTermDeposit>\n'
            
            content += '<ITRForm:IntrstFrmIncmTaxRefund>0</ITRForm:IntrstFrmIncmTaxRefund>\n'
            
            content += '<ITRForm:NatofPassThrghIncome>0</ITRForm:NatofPassThrghIncome>\n'

            content += '<ITRForm:IntrstFrmOthers>'+str(IntrstFrmOthers)+'</ITRForm:IntrstFrmOthers>\n'
            
            content += '<ITRForm:RentFromMachPlantBldgs>0</ITRForm:RentFromMachPlantBldgs>\n'
            content += '<ITRForm:Tot562x>0</ITRForm:Tot562x>\n'
            content += '<ITRForm:Aggrtvaluewithoutcons562x>0</ITRForm:Aggrtvaluewithoutcons562x>\n'
            content += '<ITRForm:Immovpropwithoutcons562x>0</ITRForm:Immovpropwithoutcons562x>\n'
            content += '<ITRForm:Immovpropinadeqcons562x>0</ITRForm:Immovpropinadeqcons562x>\n'
            content += '<ITRForm:Anyotherpropwithoutcons562x>0</ITRForm:Anyotherpropwithoutcons562x>\n'
            content += '<ITRForm:Anyotherpropinadeqcons562x>0</ITRForm:Anyotherpropinadeqcons562x>\n'
            content += '<ITRForm:FamilyPension>0</ITRForm:FamilyPension>\n'
            content += '<ITRForm:AnyOtherIncome>0</ITRForm:AnyOtherIncome>\n'
            # content += '<ITRForm:OthersInc>0</ITRForm:OthersInc>\n'
            content += '<ITRForm:IncChargeableSpecialRates>0</ITRForm:IncChargeableSpecialRates>\n'
            content += '<ITRForm:LtryPzzlChrgblUs115BB>0</ITRForm:LtryPzzlChrgblUs115BB>\n'
            content += '<ITRForm:IncChrgblUs115BBE>0</ITRForm:IncChrgblUs115BBE>\n'
            content += '<ITRForm:CashCreditsUs68>0</ITRForm:CashCreditsUs68>\n'
            content += '<ITRForm:UnExplndInvstmntsUs69>0</ITRForm:UnExplndInvstmntsUs69>\n'
            content += '<ITRForm:UnExplndMoneyUs69A>0</ITRForm:UnExplndMoneyUs69A>\n'
            content += '<ITRForm:UnDsclsdInvstmntsUs69B>0</ITRForm:UnDsclsdInvstmntsUs69B>\n'
            content += '<ITRForm:UnExplndExpndtrUs69C>0</ITRForm:UnExplndExpndtrUs69C>\n'
            content += '<ITRForm:AmtBrwdRepaidOnHundiUs69D>0</ITRForm:AmtBrwdRepaidOnHundiUs69D>\n'
            # content += '<ITRForm:TaxAccumulatedBalRecPF>0</ITRForm:TaxAccumulatedBalRecPF>\n'


            # ===============================================================
            
            content += '<ITRForm:OthersGross>0</ITRForm:OthersGross>\n'
            content += '<ITRForm:PassThrIncOSChrgblSplRate>0</ITRForm:PassThrIncOSChrgblSplRate>\n'

            
            content += '<ITRForm:IncChargblSplRateOS>\n'
            content += '<ITRForm:TotalAmtTaxUsDTAASchOs>0</ITRForm:TotalAmtTaxUsDTAASchOs>\n'
            content += '</ITRForm:IncChargblSplRateOS>\n'


            content += '<ITRForm:Deductions>\n'
            Expenses=0

            content += '<ITRForm:Expenses>'+str(Expenses)+'</ITRForm:Expenses>\n'
             
            content += '<ITRForm:DeductionUs57iia>0</ITRForm:DeductionUs57iia>\n'

            content += '<ITRForm:Depreciation>0</ITRForm:Depreciation>\n'
            TotDeductions=Expenses

            content += '<ITRForm:TotDeductions>'+str(TotDeductions)+'</ITRForm:TotDeductions>\n'
            content += '</ITRForm:Deductions>\n'
            
            content += '<ITRForm:AmtNotDeductibleUs58>0</ITRForm:AmtNotDeductibleUs58>\n'
            content += '<ITRForm:ProfitChargTaxUs59>0</ITRForm:ProfitChargTaxUs59>\n'
            
            # InterestGross - TotDeductions
            BalanceNoRaceHorse=GrossIncChrgblTaxAtAppRate - TotDeductions
            content += '<ITRForm:BalanceNoRaceHorse>'+str(BalanceNoRaceHorse)+'</ITRForm:BalanceNoRaceHorse>\n'
            
            content += '</ITRForm:IncOthThanOwnRaceHorse>\n'
            log.info('===== ScheduleOS => IncOthThanOwnRaceHorse ends =====')

            TotOthSrcNoRaceHorse=BalanceNoRaceHorse
            content += '<ITRForm:TotOthSrcNoRaceHorse>'+str(TotOthSrcNoRaceHorse)+'</ITRForm:TotOthSrcNoRaceHorse>\n'

            # content += '<ITRForm:IncFromOwnHorse>0</ITRForm:IncFromOwnHorse>\n'
            content += '<ITRForm:IncFromOwnHorse>\n'
            content += '<ITRForm:Receipts>0</ITRForm:Receipts>\n'
            content += '<ITRForm:DeductSec57>0</ITRForm:DeductSec57>\n'
            content += '<ITRForm:AmtNotDeductibleUs58>0</ITRForm:AmtNotDeductibleUs58>\n'
            content += '<ITRForm:ProfitChargTaxUs59>0</ITRForm:ProfitChargTaxUs59>\n'
            content += '<ITRForm:BalanceOwnRaceHorse>0</ITRForm:BalanceOwnRaceHorse>\n'
            content += '</ITRForm:IncFromOwnHorse>\n'
        
            IncChargeable=BalanceNoRaceHorse
            content += '<ITRForm:IncChargeable>'+str(IncChargeable)+'</ITRForm:IncChargeable>\n'

            content += '<ITRForm:DividendIncUs115BBDA>\n'
            # content += '<ITRForm:DateRangeType>0</ITRForm:DateRangeType>\n'
            content += '<ITRForm:DateRange>\n'
            content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
            content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
            content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
            content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
            content += '<ITRForm:Up16Of3To31Of3>0</ITRForm:Up16Of3To31Of3>\n'
            content += '</ITRForm:DateRange>\n'
            content += '</ITRForm:DividendIncUs115BBDA>\n'

            content += '<ITRForm:IncFrmLottery>\n'
            # content += '<ITRForm:DateRangeType>0</ITRForm:DateRangeType>\n'
            content += '<ITRForm:DateRange>\n'
            content += '<ITRForm:Upto15Of6>0</ITRForm:Upto15Of6>\n'
            content += '<ITRForm:Upto15Of9>0</ITRForm:Upto15Of9>\n'
            content += '<ITRForm:Up16Of9To15Of12>0</ITRForm:Up16Of9To15Of12>\n'
            content += '<ITRForm:Up16Of12To15Of3>0</ITRForm:Up16Of12To15Of3>\n'
            content += '<ITRForm:Up16Of3To31Of3>0</ITRForm:Up16Of3To31Of3>\n'
            content += '</ITRForm:DateRange>\n'
            content += '</ITRForm:IncFrmLottery>\n'

            content += '</ITRForm:ScheduleOS>\n'

    except Exception as ex:
        log.info('Error in ScheduleOS ITR2 '+traceback.format_exc())
    log.info('===== ScheduleOS Block ends =====')
    return content

def ScheduleVIA():
    global d_80d
    content=''
    log.info('===== ScheduleVIA Block starts =====')
    try:
        content += '<ITRForm:ScheduleVIA>\n'
        content += '<ITRForm:UsrDeductUndChapVIA>\n'

        log.info('d_80d '+str(d_80d))
        
        usrdeduction = d_80c+d_80ccc+d_80ccd+d_80d+d_80dd+d_80ddb+d_80e+d_80ee+d_80g+d_80gg+d_80gga+d_80ggc
        usrdeduction += d_80u+d_80rrb+d_80qqb+d_80ccg+d_80tta+d_80ttb
        
        content += '<ITRForm:Section80C>'+str(int(d_80c) )+'</ITRForm:Section80C>\n'
        content += '<ITRForm:Section80CCC>'+str(int(d_80ccc) )+'</ITRForm:Section80CCC>\n'
        content += '<ITRForm:Section80CCDEmployeeOrSE>0</ITRForm:Section80CCDEmployeeOrSE>\n'
        content += '<ITRForm:Section80CCD1B>'+str(int(d_80ccd) )+'</ITRForm:Section80CCD1B>\n'
        content += '<ITRForm:Section80CCDEmployer>'+str(int(d_80ccd1) )+'</ITRForm:Section80CCDEmployer>\n'
        
        content += create_xml_health_premium_block(C_ID)
        # content += '<ITRForm:Section80DHealthInsPremium>\n'
        # # content += '<ITRForm:HealthInsurancePremium>1</ITRForm:HealthInsurancePremium>\n'
        # content += '<ITRForm:Sec80DHealthInsurancePremiumUsr>0</ITRForm:Sec80DHealthInsurancePremiumUsr>\n'
        # content += '<ITRForm:Sec80DMedicalExpenditureUsr>0</ITRForm:Sec80DMedicalExpenditureUsr>\n'
        # content += '<ITRForm:PreventiveHealthCheckUp>1</ITRForm:PreventiveHealthCheckUp>\n'
        # content += '<ITRForm:Sec80DPreventiveHealthCheckUpUsr>'+str(int(d_80d) )+'</ITRForm:Sec80DPreventiveHealthCheckUpUsr>\n'
        # content += '</ITRForm:Section80DHealthInsPremium>\n'

        content += '<ITRForm:Section80DD>'+str(int(d_80dd) )+'</ITRForm:Section80DD>\n'
        content += '<ITRForm:Section80DDB>'+str(int(d_80ddb) )+'</ITRForm:Section80DDB>\n'
        content += '<ITRForm:Section80E>'+str(int(d_80e) )+'</ITRForm:Section80E>\n'
        content += '<ITRForm:Section80EE>'+str(int(d_80ee) )+'</ITRForm:Section80EE>\n'
        content += '<ITRForm:Section80G>'+str(int(d_80g) )+'</ITRForm:Section80G>\n'
        content += '<ITRForm:Section80GG>'+str(int(d_80gg) )+'</ITRForm:Section80GG>\n'
        content += '<ITRForm:Section80GGA>'+str(int(d_80gga) )+'</ITRForm:Section80GGA>\n'
        content += '<ITRForm:Section80GGC>'+str(int(d_80ggc) )+'</ITRForm:Section80GGC>\n'
        content += '<ITRForm:Section80U>'+str(int(d_80u) )+'</ITRForm:Section80U>\n'
        content += '<ITRForm:Section80RRB>'+str(int(d_80rrb) )+'</ITRForm:Section80RRB>\n'
        content += '<ITRForm:Section80QQB>'+str(int(d_80qqb) )+'</ITRForm:Section80QQB>\n'
        content += '<ITRForm:Section80CCG>'+str(int(d_80ccg) )+'</ITRForm:Section80CCG>\n'
        content += '<ITRForm:Section80TTA>'+str(int(d_80tta) )+'</ITRForm:Section80TTA>\n'
        content += '<ITRForm:Section80TTB>'+str(int( d_80ttb ) )+'</ITRForm:Section80TTB>\n'
        # content += '<ITRForm:TotalChapVIADeductions>'+str(total_deduction)+'</ITRForm:TotalChapVIADeductions>\n'
        content += '<ITRForm:TotalChapVIADeductions>'+str(int(usrdeduction) )+'</ITRForm:TotalChapVIADeductions>\n'
        content += '</ITRForm:UsrDeductUndChapVIA>\n'
        content += '<ITRForm:DeductUndChapVIA>\n'
        limited_deduction = 0
        if d_80c>150000:
            limited_deduction += 150000
            content += '<ITRForm:Section80C>150000</ITRForm:Section80C>\n'
        else:
            limited_deduction += d_80c
            content += '<ITRForm:Section80C>'+str(int(d_80c) )+'</ITRForm:Section80C>\n'
        content += '<ITRForm:Section80CCC>'+str(int(d_80ccc) )+'</ITRForm:Section80CCC>\n'
        content += '<ITRForm:Section80CCDEmployeeOrSE>0</ITRForm:Section80CCDEmployeeOrSE>\n'
        limited_deduction += d_80ccc
        if d_80ccd>50000:
            content += '<ITRForm:Section80CCD1B>50000</ITRForm:Section80CCD1B>\n'
            limited_deduction += 50000
        else:
            content += '<ITRForm:Section80CCD1B>'+str(int(d_80ccd) )+'</ITRForm:Section80CCD1B>\n'
            limited_deduction += d_80ccd
        content += '<ITRForm:Section80CCDEmployer>'+str(int(d_80ccd1) )+'</ITRForm:Section80CCDEmployer>\n'
        
        if age<60:
            var_80D_Self_Limit=25000
        elif age>60:
            var_80D_Self_Limit=50000

        parent_citizen_type=get_parent_age(C_ID)
        if parent_citizen_type=='OC':
            var_80D_parents_Limit=25000
        elif parent_citizen_type=='SC' or parent_citizen_type=='SSC':
            var_80D_parents_Limit=50000
        else:
            var_80D_parents_Limit=0

        var_80D_Limit=var_80D_Self_Limit  + var_80D_parents_Limit
        log.info('var_80D_Limit '+str(var_80D_Limit))

        d_80d_with_limit=min(int(d_80d),var_80D_Limit)
        
        content += '<ITRForm:Section80D>'+str(int(d_80d_with_limit))+'</ITRForm:Section80D>\n'
        limited_deduction += d_80d

        log.info('d_80d_with_limit '+str(d_80d_with_limit))

        if d_80dd > 75000:
            content += '<ITRForm:Section80DD>75000</ITRForm:Section80DD>\n'
            limited_deduction += 75000
        else:
            content += '<ITRForm:Section80DD>'+str(int(d_80dd) )+'</ITRForm:Section80DD>\n'
            limited_deduction += d_80dd
        content += '<ITRForm:Section80DDB>'+str(int(d_80ddb) )+'</ITRForm:Section80DDB>\n'
        content += '<ITRForm:Section80E>'+str(int(d_80e) )+'</ITRForm:Section80E>\n'
        content += '<ITRForm:Section80EE>'+str(int(d_80ee) )+'</ITRForm:Section80EE>\n'
        content += '<ITRForm:Section80G>'+str(int(d_80g) )+'</ITRForm:Section80G>\n'
        content += '<ITRForm:Section80GG>'+str(int(d_80gg) )+'</ITRForm:Section80GG>\n'
        content += '<ITRForm:Section80GGA>'+str(int(d_80gga) )+'</ITRForm:Section80GGA>\n'
        content += '<ITRForm:Section80GGC>'+str(int(d_80ggc) )+'</ITRForm:Section80GGC>\n'
        limited_deduction += d_80ddb + d_80e + d_80ee + d_80g + d_80gg + d_80gga + d_80ggc
        if d_80u > 125000:
            content += '<ITRForm:Section80U>125000</ITRForm:Section80U>\n'
            limited_deduction += 125000
        else:
            content += '<ITRForm:Section80U>'+str(int(d_80u) )+'</ITRForm:Section80U>\n'
            limited_deduction += d_80u
        content += '<ITRForm:Section80RRB>'+str(int( d_80rrb) )+'</ITRForm:Section80RRB>\n'
        content += '<ITRForm:Section80QQB>'+str(int( d_80qqb) )+'</ITRForm:Section80QQB>\n'
        content += '<ITRForm:Section80CCG>'+str(int( d_80ccg) )+'</ITRForm:Section80CCG>\n'
        limited_deduction += d_80rrb + d_80qqb + d_80ccg
        if d_80tta > 10000:
            content += '<ITRForm:Section80TTA>10000</ITRForm:Section80TTA>\n'
            limited_deduction += 10000
        else:
            content += '<ITRForm:Section80TTA>'+str(int( d_80tta ) )+'</ITRForm:Section80TTA>\n'
            limited_deduction += d_80tta

        content += '<ITRForm:Section80TTB>'+str(int( d_80ttb ) )+'</ITRForm:Section80TTB>\n'
        content += '<ITRForm:TotalChapVIADeductions>'+str(int( limited_deduction ) )+'</ITRForm:TotalChapVIADeductions>\n'
        content += '</ITRForm:DeductUndChapVIA>\n'
        content += '</ITRForm:ScheduleVIA>\n'

    except Exception as ex:
        log.info('Error in ScheduleVIA ITR2 '+traceback.format_exc())
    log.info('===== ScheduleVIA Block ends =====')
    return content


def Schedule80G():
    content=''
    EligibleDonationAmt=0
    DonationAmt=0
    TotalEligibleDonationAmt=0
    total_donation_amount=0
    total_eligible_amount=0
    log.info('===== Schedule80G Block starts =====')
    try:
        content += '<ITRForm:Schedule80G>\n'
        if int(total_donation) > 0 and 100.0 in d_percent:
            content += '<ITRForm:Don100Percent>\n'
            for x in xrange(0,donation_count):
                if d_percent[x]==100.0:
                    content += '<ITRForm:DoneeWithPan>\n'
                    content += '<ITRForm:DoneeWithPanName>'+str(d_name[x])+'</ITRForm:DoneeWithPanName>\n'
                    content += '<ITRForm:DoneePAN>'+str(d_pan[x])+'</ITRForm:DoneePAN>\n'
                    content += '<ITRForm:AddressDetail>\n'
                    content += '<ITRForm:AddrDetail>'+str(d_address[x])+'</ITRForm:AddrDetail>\n'
                    content += '<ITRForm:CityOrTownOrDistrict>'+str(d_city[x])+'</ITRForm:CityOrTownOrDistrict>\n'
                    content += '<ITRForm:StateCode>'+str(d_state_code[x])+'</ITRForm:StateCode>\n'
                    content += '<ITRForm:PinCode>'+str(d_pin_code[x])+'</ITRForm:PinCode>\n'
                    content += '</ITRForm:AddressDetail>\n'
                    content += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
                    content += '<ITRForm:DonationAmtOtherMode>'+str(int(d_amt[x]))+'</ITRForm:DonationAmtOtherMode>\n'
                    content += '<ITRForm:DonationAmt>'+str(int(d_amt[x]))+'</ITRForm:DonationAmt>\n'
                    
                    DonationAmt += int(d_amt[x])
                    TotalEligibleDonationAmt += int(d_amt[x])
                    content += '<ITRForm:EligibleDonationAmt>'+str(int(d_amt[x]))+'</ITRForm:EligibleDonationAmt>\n'
                    content += '</ITRForm:DoneeWithPan>\n'
            
            total_donation_amount += DonationAmt
            total_eligible_amount += TotalEligibleDonationAmt
            content += '<ITRForm:TotDon100PercentCash>0</ITRForm:TotDon100PercentCash>\n'
            content += '<ITRForm:TotDon100PercentOtherMode>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotDon100PercentOtherMode>\n'
            content += '<ITRForm:TotDon100Percent>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotDon100Percent>\n'
            content += '<ITRForm:TotEligibleDon100Percent>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotEligibleDon100Percent>\n'
            content += '</ITRForm:Don100Percent>\n'


        if int(total_donation) > 0 and 50.0 in d_percent:
            content += '<ITRForm:Don50PercentNoApprReqd>\n'
            for x in xrange(0,donation_count):
                if d_percent[x]==50.0:
                    content += '<ITRForm:DoneeWithPan>\n'
                    content += '<ITRForm:DoneeWithPanName>'+str(d_name[x])+'</ITRForm:DoneeWithPanName>\n'
                    content += '<ITRForm:DoneePAN>'+str(d_pan[x])+'</ITRForm:DoneePAN>\n'
                    content += '<ITRForm:AddressDetail>\n'
                    content += '<ITRForm:AddrDetail>'+str(d_address[x])+'</ITRForm:AddrDetail>\n'
                    content += '<ITRForm:CityOrTownOrDistrict>'+str(d_city[x])+'</ITRForm:CityOrTownOrDistrict>\n'
                    content += '<ITRForm:StateCode>'+str(d_state_code[x])+'</ITRForm:StateCode>\n'
                    content += '<ITRForm:PinCode>'+str(d_pin_code[x])+'</ITRForm:PinCode>\n'
                    content += '</ITRForm:AddressDetail>\n'
                    content += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
                    content += '<ITRForm:DonationAmtOtherMode>'+str(2*int(d_amt[x]))+'</ITRForm:DonationAmtOtherMode>\n'
                    content += '<ITRForm:DonationAmt>'+str(2*int(d_amt[x]))+'</ITRForm:DonationAmt>\n'
                    
                    DonationAmt += 2*int(d_amt[x])
                    TotalEligibleDonationAmt += int(d_amt[x])
                    content += '<ITRForm:EligibleDonationAmt>'+str(int(d_amt[x]))+'</ITRForm:EligibleDonationAmt>\n'
                    content += '</ITRForm:DoneeWithPan>\n'
            
            total_donation_amount += DonationAmt
            total_eligible_amount += TotalEligibleDonationAmt
            content += '<ITRForm:TotDon50PercentNoApprReqdCash>0</ITRForm:TotDon50PercentNoApprReqdCash>\n'
            content += '<ITRForm:TotDon50PercentNoApprReqdOtherMode>'+str(2*int(TotalEligibleDonationAmt))+'</ITRForm:TotDon50PercentNoApprReqdOtherMode>\n'
            content += '<ITRForm:TotDon50PercentNoApprReqd>'+str(2*int(TotalEligibleDonationAmt))+'</ITRForm:TotDon50PercentNoApprReqd>\n'
            content += '<ITRForm:TotEligibleDon50Percent>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotEligibleDon50Percent>\n'
            content += '</ITRForm:Don50PercentNoApprReqd>\n'

        content += '<ITRForm:TotalDonationsUs80GCash>0</ITRForm:TotalDonationsUs80GCash>\n'
        content += '<ITRForm:TotalDonationsUs80GOtherMode>'+str(total_donation_amount)+'</ITRForm:TotalDonationsUs80GOtherMode>\n'
        content += '<ITRForm:TotalDonationsUs80G>'+str(total_donation_amount)+'</ITRForm:TotalDonationsUs80G>\n'
        content += '<ITRForm:TotalEligibleDonationsUs80G>'+str(total_eligible_amount)+'</ITRForm:TotalEligibleDonationsUs80G>\n'
        content += '</ITRForm:Schedule80G>\n'

        log.info('===== Schedule80G Block ends =====')

        log.info('===== Schedule80GGA Block starts =====')

        if int(d_80gga) > 0:
            content += '<ITRForm:Schedule80GGA>\n'
            content += '<ITRForm:DonationDtlsSciRsrchRuralDev>\n'
            content += '<ITRForm:RelevantClauseUndrDedClaimed>0</ITRForm:RelevantClauseUndrDedClaimed>\n'
            content += '<ITRForm:NameOfDonee>0</ITRForm:NameOfDonee>\n'
            content += '<ITRForm:AddressDetail>\n'
            content += '<ITRForm:AddrDetail>0</ITRForm:AddrDetail>\n'
            content += '<ITRForm:CityOrTownOrDistrict>0</ITRForm:CityOrTownOrDistrict>\n'
            content += '<ITRForm:StateCode>0</ITRForm:StateCode>\n'
            content += '<ITRForm:PinCode>0</ITRForm:PinCode>\n'
            content += '</ITRForm:AddressDetail>\n'
            content += '<ITRForm:DoneePAN>0</ITRForm:DoneePAN>\n'
            content += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
            content += '<ITRForm:DonationAmtOtherMode>0</ITRForm:DonationAmtOtherMode>\n'
            content += '<ITRForm:DonationAmt>0</ITRForm:DonationAmt>\n'
            content += '<ITRForm:EligibleDonationAmt>0</ITRForm:EligibleDonationAmt>\n'
            content += '</ITRForm:DonationDtlsSciRsrchRuralDev>\n'
            content += '<ITRForm:TotalDonationAmtCash80GGA>0</ITRForm:TotalDonationAmtCash80GGA>\n'
            content += '<ITRForm:TotalDonationAmtOtherMode80GGA>0</ITRForm:TotalDonationAmtOtherMode80GGA>\n'
            content += '<ITRForm:TotalDonationsUs80GGA>0</ITRForm:TotalDonationsUs80GGA>\n'
            content += '<ITRForm:TotalEligibleDonationAmt80GGA>0</ITRForm:TotalEligibleDonationAmt80GGA>\n'
            content += '</ITRForm:Schedule80GGA>\n'

        log.info('===== Schedule80GGA Block starts =====')

    except Exception as ex:
        log.info('Error in Schedule80G ITR2 '+traceback.format_exc())
    
    return content


def ScheduleSI():
    content=''
    log.info('===== ScheduleSI Block starts =====')
    try:
        if 1==1:
            content += '<ITRForm:ScheduleSI>\n'
            TotSplRateIncTax = 0

            content += '<ITRForm:SplCodeRateTax>\n'
            content += '<ITRForm:SecCode>1</ITRForm:SecCode>\n'
            content += '<ITRForm:SplRatePercent>1</ITRForm:SplRatePercent>\n'
            content += '<ITRForm:SplRateInc>0</ITRForm:SplRateInc>\n'
            content += '<ITRForm:SplRateIncTax>0</ITRForm:SplRateIncTax>\n'
            content += '</ITRForm:SplCodeRateTax>\n'

            # if (st_shares_gain+st_equity_gain != 0):
            content += '<ITRForm:SplCodeRateTax>\n'
            content += '<ITRForm:SecCode>1A</ITRForm:SecCode>\n<ITRForm:SplRatePercent>15</ITRForm:SplRatePercent>\n'
            if (st_shares_gain+st_equity_gain <= 0):
                st_share_equity_special_rates=0
                content += '<ITRForm:SplRateInc>0</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>0</ITRForm:SplRateIncTax>\n'
            else:
                st_share_equity_special_rates=0.15 * (st_shares_gain+st_equity_gain)
                content += '<ITRForm:SplRateInc>'+str(int( st_shares_gain+st_equity_gain) )+'</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>'+str(int(st_share_equity_special_rates))+'</ITRForm:SplRateIncTax>\n'
            content += '</ITRForm:SplCodeRateTax>\n'

            log.info('st_share_equity_special_rates '+str(st_share_equity_special_rates))
            TotSplRateIncTax += st_share_equity_special_rates

            # if (lt_debt_MF_gain != 0):
            content += '<ITRForm:SplCodeRateTax>\n'
            content += '<ITRForm:SecCode>21</ITRForm:SecCode>\n<ITRForm:SplRatePercent>20</ITRForm:SplRatePercent>\n'
            if (lt_debt_MF_gain <= 0):
                lt_debt_MF_special_rates=0
                content += '<ITRForm:SplRateInc>0</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>0</ITRForm:SplRateIncTax>\n'
            else:
                lt_debt_MF_special_rates=0.20 * (lt_debt_MF_gain)
                content += '<ITRForm:SplRateInc>'+str(int(lt_debt_MF_gain) )+'</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>'+str(int(lt_debt_MF_special_rates) )+'</ITRForm:SplRateIncTax>\n'
            content += '</ITRForm:SplCodeRateTax>\n'

            log.info('lt_debt_MF_special_rates '+str(lt_debt_MF_special_rates))
            TotSplRateIncTax += lt_debt_MF_special_rates
            
            # if (lt_listed_d_gain != 0):
            content += '<ITRForm:SplCodeRateTax>\n'
            content += '<ITRForm:SecCode>22</ITRForm:SecCode>\n<ITRForm:SplRatePercent>10</ITRForm:SplRatePercent>\n'
           
            if (lt_listed_d_gain <= 0):
                lt_debentures_special_rates=0
                content += '<ITRForm:SplRateInc>0</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>0</ITRForm:SplRateIncTax>\n'
            else:
                lt_debentures_special_rates=0.10 * (lt_listed_d_gain)
                content += '<ITRForm:SplRateInc>'+str(int(lt_listed_d_gain) )+'</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>'+str(int(lt_debentures_special_rates) )+'</ITRForm:SplRateIncTax>\n'
            content += '</ITRForm:SplCodeRateTax>\n'

            log.info('lt_debentures_special_rates '+str(lt_debentures_special_rates))
            TotSplRateIncTax += lt_debentures_special_rates


            # if (lt_listed_d_gain != 0):
            content += '<ITRForm:SplCodeRateTax>\n'
            content += '<ITRForm:SecCode>2A</ITRForm:SecCode>\n<ITRForm:SplRatePercent>10</ITRForm:SplRatePercent>\n'
           
            if ((lt_shares_gain+lt_equity_gain) <= 0):
                log.info('LT shares and equity less than or equal 0')
                lt_shares_equity_special_rates=0
                content += '<ITRForm:SplRateInc>0</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>0</ITRForm:SplRateIncTax>\n'
            else:
                log.info('LT shares and equity greater than 0')
                lt_shares_equity_special_rates=0.10 * ((lt_shares_gain+lt_equity_gain)-100000)
                content += '<ITRForm:SplRateInc>'+str(int(lt_shares_gain+lt_equity_gain) )+'</ITRForm:SplRateInc>\n'
                content += '<ITRForm:SplRateIncTax>'+str(int(lt_shares_equity_special_rates) )+'</ITRForm:SplRateIncTax>\n'
            content += '</ITRForm:SplCodeRateTax>\n'

            log.info('LT shares and equity '+str(int(lt_shares_gain+lt_equity_gain)))

            if (int(lt_shares_gain+lt_equity_gain) > 100000):
                TotSplRateIncTax += lt_shares_equity_special_rates

            ScheduleSI_code_arr=[   
                    # {'code':'1','percent':'1'},
                    {'code':'21ciii','percent':'10'},
                    {'code':'5BB','percent':'30'},
                    {'code':'5ADii','percent':'30'},
                    {'code':'5ADiiiP','percent':'10'},

                    {'code':'DTAASTCG','percent':'1'},
                    {'code':'DTAALTCG','percent':'1'},
                    {'code':'DTAAOS','percent':'1'},

                    {'code':'5AD1biip','percent':'15'},

                    {'code':'5A1ai','percent':'20'},
                    {'code':'5A1aii','percent':'20'},

                    {'code':'5A1aiia','percent':'5'},
                    {'code':'5A1aiiaa','percent':'5'},
                    {'code':'5A1aiiab','percent':'5'},
                    {'code':'5A1aiiac','percent':'5'},

                    {'code':'5A1aiii','percent':'20'},

                    {'code':'5A1bA','percent':'10'},
                    # {'code':'5A1bB','percent':'10'},
                    {'code':'5AC1ab','percent':'10'},
                    {'code':'5AC1c','percent':'10'},
                    {'code':'5ACA1a','percent':'10'},
                    {'code':'5ACA1b','percent':'10'},

                    {'code':'5AD1i','percent':'20'},
                    {'code':'5AD1iP','percent':'5'},
                
                
                    {'code':'5ADiii','percent':'10'},
                
                    {'code':'5BBA','percent':'20'},
                    {'code':'5BBC','percent':'30'},

                    {'code':'5BBDA','percent':'10'},
                    {'code':'5BBE','percent':'60'},
                    {'code':'5BBF','percent':'10'},
                    {'code':'5BBG','percent':'10'},
                    {'code':'5Ea','percent':'20'},
                    {'code':'5Eacg','percent':'20'},
                    {'code':'5Eb','percent':'10'},
                
                    {'code':'PTI_STCG15P','percent':'15'},
                    {'code':'PTI_STCG30P','percent':'30'},
                    {'code':'PTI_LTCG10P','percent':'10'},
                    {'code':'PTI_LTCG20P','percent':'20'},
                    {'code':'PTI_5A1ai','percent':'20'},
                    {'code':'PTI_5A1aii','percent':'20'},
                    {'code':'PTI_5A1aiia','percent':'5'},

                    {'code':'PTI_5A1aiiaa','percent':'5'},
                    {'code':'PTI_5A1aiiab','percent':'5'},
                    {'code':'PTI_5A1aiiac','percent':'5'},
                    {'code':'PTI_5A1aiii','percent':'20'},
                    {'code':'PTI_5A1bA','percent':'10'},
                    # {'code':'PTI_5A1bB','percent':'10'},

                    {'code':'PTI_5AC1ab','percent':'10'},
                    {'code':'PTI_5ACA1a','percent':'10'},
                    {'code':'PTI_5AD1i','percent':'20'},
                    {'code':'PTI_5AD1iP','percent':'5'},
                    # {'code':'PTI_5BB','percent':'30'},
                    {'code':'PTI_5BBA','percent':'20'},
                    {'code':'PTI_5BBC','percent':'30'},
                    {'code':'PTI_5BBDA','percent':'10'},
                    # {'code':'PTI_5BBE','percent':'60'},

                    {'code':'PTI_5BBF','percent':'10'},
                    {'code':'PTI_5BBG','percent':'10'},
                    {'code':'PTI_5Ea','percent':'20'} 
                ]
            
            log.info(len(ScheduleSI_code_arr))

            for data in ScheduleSI_code_arr:
                # log.info(data)
                content += '<ITRForm:SplCodeRateTax>\n'
                content += '<ITRForm:SecCode>'+data['code']+'</ITRForm:SecCode>\n<ITRForm:SplRatePercent>'+data['percent']+'</ITRForm:SplRatePercent>\n'
                content += '<ITRForm:SplRateInc>0</ITRForm:SplRateInc>\n<ITRForm:SplRateIncTax>0</ITRForm:SplRateIncTax>\n'
                content += '</ITRForm:SplCodeRateTax>\n'

            log.info('TotSplRateIncTax '+str(TotSplRateIncTax))

            content += '<ITRForm:TotSplRateIncTax>'+str(int(TotSplRateIncTax) )+'</ITRForm:TotSplRateIncTax>\n'
            content += '</ITRForm:ScheduleSI>\n'

    except Exception as ex:
        log.info('Error in Form_ITR2 ITR2 '+traceback.format_exc())
    log.info('===== ScheduleSI Block ends =====')
    return content

def ScheduleEI():
    content=''
    log.info('===== ScheduleEI Block starts =====')
    try:
        if int(Tax_Free_Interest)>0 or int(dividend)>0 or int(CG_share_equity_LT)>0 or int(agri_income)>0 or int(other_exempt_income)>0:
            content += '<ITRForm:ScheduleEI>\n'
            # change : tax free interest (done)
            # content += '<ITRForm:InterestInc>'+str(int(ICU_head_sal))+'</ITRForm:InterestInc>\n'
            content += '<ITRForm:InterestInc>'+str(int(Tax_Free_Interest))+'</ITRForm:InterestInc>\n'
            
            log.info('Tax_Free_Interest '+str(int(Tax_Free_Interest)))

            if dividend>1000000:
                content += '<ITRForm:DividendInc>1000000</ITRForm:DividendInc>\n'
            else:
                content += '<ITRForm:DividendInc>'+str(int(dividend) )+'</ITRForm:DividendInc>\n'
            
            log.info('DividendInc '+str(int(dividend) ))

            # content += '<ITRForm:LTCGWhereSTTPaid>'+str(int(CG_share_equity_LT) )+'</ITRForm:LTCGWhereSTTPaid>\n'
            
            log.info('LTCGWhereSTTPaid '+str(int(CG_share_equity_LT)))

            # GrossAgriRecpt
            GrossAgriRecpt=int(agri_income)
            content += '<ITRForm:GrossAgriRecpt>'+str(GrossAgriRecpt)+'</ITRForm:GrossAgriRecpt>\n'
            
            log.info('GrossAgriRecpt '+str(int(GrossAgriRecpt)))

            content += '<ITRForm:ExpIncAgri>0</ITRForm:ExpIncAgri>\n'
            
            content += '<ITRForm:UnabAgriLossPrev8>0</ITRForm:UnabAgriLossPrev8>\n'
            
            NetAgriIncOrOthrIncRule7=GrossAgriRecpt

            log.info('NetAgriIncOrOthrIncRule7 '+str(int(NetAgriIncOrOthrIncRule7)))

            content += '<ITRForm:NetAgriIncOrOthrIncRule7>'+str(NetAgriIncOrOthrIncRule7)+'</ITRForm:NetAgriIncOrOthrIncRule7>\n'
            
            content += '<ITRForm:OthersInc>\n'
            content += '<ITRForm:OthersIncDtls>\n'
            content += '<ITRForm:NatureDesc>10(12)</ITRForm:NatureDesc>\n'
            content += '<ITRForm:OthAmount>'+str(int(other_exempt_income))+'</ITRForm:OthAmount>\n'
            content += '</ITRForm:OthersIncDtls>\n'
            content += '</ITRForm:OthersInc>\n'

            content += '<ITRForm:Others>'+str(int(other_exempt_income))+'</ITRForm:Others>\n'


            # content += '<ITRForm:Others>'+str(int(other_exempt_income) )+'</ITRForm:Others>\n'
            # content += '<ITRForm:TotalExemptInc>'+str(int(ICU_head_sal+dividend+CG_share_equity_LT+other_exempt_income) )+'</ITRForm:TotalExemptInc>\n'
            content += '<ITRForm:IncNotChrgblToTax>0</ITRForm:IncNotChrgblToTax>\n'
            content += '<ITRForm:PassThrIncNotChrgblTax>0</ITRForm:PassThrIncNotChrgblTax>\n'
            content += '<ITRForm:TotalExemptInc>'+str(int(Tax_Free_Interest+dividend+ agri_income + other_exempt_income) )+'</ITRForm:TotalExemptInc>\n'
            
            log.info('TotalExemptInc '+str(int(Tax_Free_Interest+dividend+ agri_income + other_exempt_income) ))

            content += '</ITRForm:ScheduleEI>\n'

    except Exception as ex:
        log.info('Error in ScheduleEI ITR2 '+traceback.format_exc())
    log.info('===== ScheduleEI Block ends =====')
    return content

def ScheduleIT():
    content=''
    TotalTaxPayments=0
    log.info('===== ScheduleIT Block starts =====')
    try:
        if tax_paid_count != 0:
            content += '<ITRForm:ScheduleIT>\n'
            log.info(tp_bsr_code)
            for x in range(len(tp_bsr_code)):
                content += '<ITRForm:TaxPayment>\n'
                if len(str(tp_bsr_code[x]))==6:
                    BSRCode='0'+str(tp_bsr_code[x])
                else:
                    BSRCode=tp_bsr_code[x]
                content += '<ITRForm:BSRCode>'+str(BSRCode)+'</ITRForm:BSRCode>\n'
                content += '<ITRForm:DateDep>'+str(tp_deposit_date[x])+'</ITRForm:DateDep>\n'
                content += '<ITRForm:SrlNoOfChaln>'+str(tp_challan_no[x])+'</ITRForm:SrlNoOfChaln>\n'
                content += '<ITRForm:Amt>'+str(tp_total_tax[x])+'</ITRForm:Amt>\n'
                TotalTaxPayments += int(tp_total_tax[x])
                content += '</ITRForm:TaxPayment>\n'
            
            log.info('TotalTaxPayments '+str(TotalTaxPayments))
            content += '<ITRForm:TotalTaxPayments>'+str(int(TotalTaxPayments) )+'</ITRForm:TotalTaxPayments>\n'
            content += '</ITRForm:ScheduleIT>\n'

    except Exception as ex:
        log.info('Error in ScheduleIT ITR2 '+traceback.format_exc())
    log.info('===== ScheduleIT Block ends =====')
    return content

def ScheduleTDS1():
    content=''
    log.info('===== ScheduleTDS1 Block starts =====')
    try:
        # TDS1 all == 192
        # TDS2 all != 192
        if tds_count!=0:
            content += '<ITRForm:ScheduleTDS1>\n'
            for x in xrange(0,tds_count):
                content += '<ITRForm:TDSonSalary>\n'
                content += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
                content += '<ITRForm:TAN>'+tds_TAN_PAN[x]+'</ITRForm:TAN>\n'
                content += '<ITRForm:EmployerOrDeductorOrCollecterName>'+str(tds_dc_name[x])+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
                content += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
                content += '<ITRForm:IncChrgSal>'+str(int(tds_amt_paid[x]) )+'</ITRForm:IncChrgSal>\n'
                content += '<ITRForm:TotalTDSSal>'+str(int(tds_deducted[x]) )+'</ITRForm:TotalTDSSal>\n'
                content += '</ITRForm:TDSonSalary>\n'
            content += '<ITRForm:TotalTDSonSalaries>'+str(int(total_tds1) )+'</ITRForm:TotalTDSonSalaries>\n'
            content += '</ITRForm:ScheduleTDS1>\n'

    except Exception as ex:
        log.error('Error in ScheduleTDS1 ITR2 '+traceback.format_exc())
    log.info('===== ScheduleTDS1 Block ends =====')
    return content

def ScheduleTDS2():
    content=''
    log.info('===== ScheduleTDS2 Block starts =====')
    try:
        if tds2_count!=0:
            content += '<ITRForm:ScheduleTDS2>\n'
            for x in xrange(0,tds2_count):
                content += '<ITRForm:TDSOthThanSalaryDtls>\n'
                content += '<ITRForm:TDSCreditName>S</ITRForm:TDSCreditName>\n'

                # content += '<ITRForm:PANofOtherPerson>S</ITRForm:PANofOtherPerson>\n'
                content += '<ITRForm:TANOfDeductor>'+tds2_TAN_PAN[x]+'</ITRForm:TANOfDeductor>\n'
                # content += '<ITRForm:DeductedYr>S</ITRForm:DeductedYr>\n'
                # content += '<ITRForm:BroughtFwdTDSAmt>S</ITRForm:BroughtFwdTDSAmt>\n'

                # content += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
                # content += '<ITRForm:TAN>'+tds2_TAN_PAN[x]+'</ITRForm:TAN>\n'
                # content += '<ITRForm:EmployerOrDeductorOrCollecterName>'+tds2_dc_name[x]+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
                # content += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
                
                content += '<ITRForm:TaxDeductCreditDtls>\n'
                content += '<ITRForm:TaxDeductedOwnHands>'+str(int(tds2_tds_deducted[x]) )+'</ITRForm:TaxDeductedOwnHands>\n'
                # content += '<ITRForm:TaxDeductedIncome>0</ITRForm:TaxDeductedIncome>\n'
                # content += '<ITRForm:TaxDeductedTDS>0</ITRForm:TaxDeductedTDS>\n'
                # content += '<ITRForm:TaxClaimedOwnHands>0</ITRForm:TaxClaimedOwnHands>\n'
                # content += '<ITRForm:TaxClaimedIncome>0</ITRForm:TaxClaimedIncome>\n'
                # content += '<ITRForm:TaxClaimedTDS>0</ITRForm:TaxClaimedTDS>\n'
                # content += '<ITRForm:TaxClaimedSpouseOthPrsnPAN>0</ITRForm:TaxClaimedSpouseOthPrsnPAN>\n'
                content += '</ITRForm:TaxDeductCreditDtls>\n'
                
                content += '<ITRForm:GrossAmount>'+str(int(tds2_amt_paid[x]) )+'</ITRForm:GrossAmount>\n'
                
                TDSHeadOfIncome(tds2_section[x])
                log.info(tds2_section[x])
                content += '<ITRForm:HeadOfIncome>'+TDSHeadOfIncome(tds2_section[x])+'</ITRForm:HeadOfIncome>\n'
                # content += '<ITRForm:AmtCarriedFwd>5000</ITRForm:AmtCarriedFwd>\n'
                
                content += '</ITRForm:TDSOthThanSalaryDtls>\n'

            content += '<ITRForm:TotalTDSonOthThanSals>'+str(int(total_tds2) )+'</ITRForm:TotalTDSonOthThanSals>\n'
            content += '</ITRForm:ScheduleTDS2>\n'

    except Exception as ex:
        log.info('Error in ScheduleTDS2 ITR2 '+traceback.format_exc())
    log.info('===== ScheduleTDS2 Block ends =====')
    return content

def ScheduleTCS():
    content=''
    log.info('===== ScheduleTCS Block starts =====')
    try:
        if tcs_count>0:
            content += '<ITRForm:ScheduleTCS>\n'
            for x in xrange(0,tcs_count):
                content += '<ITRForm:TCS>\n'
                
                content += '<ITRForm:TAN>'+tcs_TAN_PAN[x]+'</ITRForm:TAN>\n'
                content += '<ITRForm:EmployerOrDeductorOrCollecterName>'+tcs_dc_name[x]+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
                # c += '<ITRForm:AmtTaxCollected>'+str(tcs_amt_paid[x])+'</ITRForm:AmtTaxCollected>\n'
                # c += '<ITRForm:CollectedYr>2017</ITRForm:CollectedYr>\n'
                content += '<ITRForm:TotalTCS>'+str(tcs_amt_paid[x])+'</ITRForm:TotalTCS>\n'
                content += '<ITRForm:AmtTCSClaimedThisYear>'+str(tcs_amt_paid[x])+'</ITRForm:AmtTCSClaimedThisYear>\n'
                content += '<ITRForm:AmtClaimedBySpouse>S</ITRForm:AmtClaimedBySpouse>\n'
                content += '</ITRForm:TCS>\n'
            content += '<ITRForm:TotalSchTCS>'+str(total_tcs)+'</ITRForm:TotalSchTCS>\n'
            content += '</ITRForm:ScheduleTCS>\n'

    except Exception as ex:
        log.info('Error in ScheduleTCS ITR2 '+traceback.format_exc())
    log.info('===== ScheduleTCS Block ends =====')
    return content

def ScheduleTDS3():
    content=''
    log.info('===== ScheduleTDS3 Block starts =====')
    try:
        if tds3_count!=0:
            content += '<ITRForm:ScheduleTDS3>\n'
            for x in xrange(0,tds3_count):
                content += '<ITRForm:TDS3onOthThanSalDtls>\n'
                content += '<ITRForm:TDSCreditName>S</ITRForm:TDSCreditName>\n'

                # content += '<ITRForm:PANofOtherPerson>S</ITRForm:PANofOtherPerson>\n'
                content += '<ITRForm:TANOfDeductor>'+tds3_TAN_PAN[x]+'</ITRForm:TANOfDeductor>\n'
                # content += '<ITRForm:DeductedYr>S</ITRForm:DeductedYr>\n'
                # content += '<ITRForm:BroughtFwdTDSAmt>S</ITRForm:BroughtFwdTDSAmt>\n'

                
                content += '<ITRForm:TaxDeductCreditDtls>\n'
                content += '<ITRForm:TaxDeductedOwnHands>'+str(int(tds3_tds_deducted[x]) )+'</ITRForm:TaxDeductedOwnHands>\n'
                # content += '<ITRForm:TaxDeductedIncome>0</ITRForm:TaxDeductedIncome>\n'
                # content += '<ITRForm:TaxDeductedTDS>0</ITRForm:TaxDeductedTDS>\n'
                # content += '<ITRForm:TaxClaimedOwnHands>0</ITRForm:TaxClaimedOwnHands>\n'
                # content += '<ITRForm:TaxClaimedIncome>0</ITRForm:TaxClaimedIncome>\n'
                # content += '<ITRForm:TaxClaimedTDS>0</ITRForm:TaxClaimedTDS>\n'
                # content += '<ITRForm:TaxClaimedSpouseOthPrsnPAN>0</ITRForm:TaxClaimedSpouseOthPrsnPAN>\n'
                content += '</ITRForm:TaxDeductCreditDtls>\n'
                
                content += '<ITRForm:GrossAmount>'+str(int(tds3_amt_paid[x]) )+'</ITRForm:GrossAmount>\n'
                content += '<ITRForm:HeadOfIncome>'+TDSHeadOfIncome(tds3_section[x])+'</ITRForm:HeadOfIncome>\n'
                # content += '<ITRForm:AmtCarriedFwd>5000</ITRForm:AmtCarriedFwd>\n'
                
                content += '</ITRForm:TDS3onOthThanSalDtls>\n'
            content += '</ITRForm:ScheduleTDS3>\n'

    except Exception as ex:
        log.info('Error in ScheduleTDS3 ITR2 '+traceback.format_exc())
    log.info('===== ScheduleTDS3 Block ends =====')
    return content

def PartB_TI_Total_Income():
    result={}
    TotalIncome=0
    AggregateIncome=0
    try:
        NetSalary=int(sum(calculated_salary_arr))
        log.info('NetSalary '+str(NetSalary))

        # DeductionUs16ia+Profession Tax + Enterta
        DeductionUs16ia=40000.0
        log.info('Standard Deductions '+str(DeductionUs16ia))
        log.info('Profession Tax '+str(sum(c_professionTax)))
        log.info('Entertainment '+str(sum(c_entertainment)))

        DeductionUS16=DeductionUs16ia+sum(c_professionTax)+sum(c_entertainment)

        log.info('DeductionUS16 '+str(DeductionUS16))

        TotIncUnderHeadSalaries=NetSalary-DeductionUS16

        log.info('TotIncUnderHeadSalaries '+str(TotIncUnderHeadSalaries))

        Total_CG=int(st_shares_gain+st_equity_gain+st_debt_MF_gain+st_listed_d_gain + lt_listed_d_gain+lt_debt_MF_gain+lt_shares_gain)
        
        log.info('ICU_house_p '+str(ICU_house_p))

        log.info('Total_CG '+str(Total_CG))
        ICU_other_s=int(Interest_Income + Interest_Savings + FD + Other_Income)
        xml_TotalTI=int(TotIncUnderHeadSalaries+max(0,ICU_house_p)+max(0,Total_CG)+ ICU_other_s)
        
        log.info('ICU_other_s '+str(ICU_other_s))

        log.info('Other_Income '+str(Other_Income))

        log.info('xml_TotalTI '+str(xml_TotalTI))
        CurrentYearLoss=abs(min(0,int(ICU_house_p)))

        log.info('CurrentYearLoss '+str(CurrentYearLoss))
        BalanceAfterSetoffLosses=xml_TotalTI-CurrentYearLoss

        log.info('BalanceAfterSetoffLosses '+str(BalanceAfterSetoffLosses))
        BroughtFwdLossesSetoff=0
        GrossTotalIncome=BalanceAfterSetoffLosses- BroughtFwdLossesSetoff

        log.info('GrossTotalIncome '+str(GrossTotalIncome))
        DeductionsUnderScheduleVIA=total_deduction

        log.info('DeductionsUnderScheduleVIA '+str(DeductionsUnderScheduleVIA))
        TotalIncome=GrossTotalIncome-DeductionsUnderScheduleVIA

        log.info('TotalIncome '+str(TotalIncome))
        response = requests.post(
            salat_url+'/get_CG_income/',
            data={'client_id': C_ID},
        )

        CG_special_rates=response.json()

        IncChargeableTaxSplRates=CG_special_rates['Income_chargeable_CG_at_special_rates']
        IncChargeTaxSplRate111A112=IncChargeableTaxSplRates

        IncChargeableTaxSplRates=IncChargeTaxSplRate111A112

        log.info('IncChargeableTaxSplRates '+str(IncChargeableTaxSplRates))
        NetAgricultureIncomeOrOtherIncomeForRate=int(agri_income)

        log.info('NetAgricultureIncomeOrOtherIncomeForRate '+str(NetAgricultureIncomeOrOtherIncomeForRate))
        AggregateIncome=TotalIncome-IncChargeableTaxSplRates+NetAgricultureIncomeOrOtherIncomeForRate

        log.info('AggregateIncome '+str(AggregateIncome))
    except Exception as ex:
        log.info('Error in PartB_TI_Total_Income clculation '+str(traceback.format_exc()))
    
    result['TotalIncome']=round(TotalIncome,-1)
    result['AggregateIncome']=AggregateIncome
    return result


def PartB_TTI_Tax_Liability(AggregateIncome):
    GrossTaxLiability=0
    try:
        TaxAtNormalRatesOnAggrInc=slab_level_calculation(AggregateIncome)
   
        log.info('TaxAtSpecialRates '+str(int(Tx1)))

        # calculate tax as per slab for (H96 + choose from (2.5 lacs, 3 lac, 5 lac))
        if citizen_type=='OC':
            slab_amount=250000
        elif citizen_type=='SC':
            slab_amount=300000
        elif citizen_type=='SSC':
            slab_amount=500000

        RebateOnAgriInc=0
     
        TaxPayableOnTotInc=TaxAtNormalRatesOnAggrInc+int(Tx1)+RebateOnAgriInc
        log.info('TaxPayableOnTotInc '+str(TaxPayableOnTotInc))
        
        rebate=0
        if int(AggregateIncome) < 350000 :
            rebate = 2500
        if int(AggregateIncome) <= 300000:
            rebate = Tx
       
        log.info('Rebate87A '+str(rebate))

        TaxPayableOnRebate=TaxPayableOnTotInc-int(rebate)
        
        log.info('TaxPayableOnRebate '+str(TaxPayableOnRebate))

        # Surcharge(SH)
        surcharge=0
        if AggregateIncome>5000000 and TI<10000000:
            surcharge = (Tx + Tx1)*0.1
        if AggregateIncome>10000000:
            surcharge = (Tx + Tx1)*0.15

        log.info('surcharge '+str(surcharge))
       
        edu_cess_per = 0.04
        educess = (TaxAtNormalRatesOnAggrInc + Tx1 - rebate)*edu_cess_per
       
        log.info('EducationCess '+str(educess))

        log.info('GrossTaxLiability = TaxAtNormalRatesOnAggrInc + Tx1 + surcharge + educess')

        GrossTaxLiability=TaxAtNormalRatesOnAggrInc+Tx1-rebate+surcharge+educess

        log.info('GrossTaxLiability '+str(GrossTaxLiability))
        return GrossTaxLiability
    except Exception as ex:
        log.info('Error in PartB_TTI_Tax_Liability clculation '+str(traceback.format_exc()))

def ScheduleAMT():
    log.info('===== ScheduleAMT Block starts =====')
    content=''
    global AMT_common
    try:
        log.info(AMT_common)
        log.info('PartB_TI_Total_Income '+str(PartB_TI_Total_Income()))

        content += '<ITRForm:ScheduleAMT>\n'

        data=PartB_TI_Total_Income()
        content += '<ITRForm:TotalIncItemPartBTI>'+str(int(data['TotalIncome']))+'</ITRForm:TotalIncItemPartBTI>\n'
        content += '<ITRForm:DeductionClaimUndrAnySec>0</ITRForm:DeductionClaimUndrAnySec>\n'
        content += '<ITRForm:AdjustedUnderSec115JC>'+str(int(data['TotalIncome']))+'</ITRForm:AdjustedUnderSec115JC>\n'
        content += '<ITRForm:TaxPayableUnderSec115JC>0</ITRForm:TaxPayableUnderSec115JC>\n'
        content += '</ITRForm:ScheduleAMT>\n'

        content += '<ITRForm:ScheduleAMTC>\n'
        content += '<ITRForm:TaxSection115JC>0</ITRForm:TaxSection115JC>\n'

        TaxOthProvisions=round(PartB_TTI_Tax_Liability(data['AggregateIncome']))
        content += '<ITRForm:TaxOthProvisions>'+str(int(TaxOthProvisions))+'</ITRForm:TaxOthProvisions>\n'
        content += '<ITRForm:AmtTaxCreditAvailable>'+str(int(TaxOthProvisions))+'</ITRForm:AmtTaxCreditAvailable>\n'
        
        AS_yr=['2013-14','2014-15', '2015-16', '2016-17', '2017-18', '2018-19']
        for year in AS_yr:
            content += '<ITRForm:ScheduleAMTCDtls>\n'
            content += '<ITRForm:AssYr>'+year+'</ITRForm:AssYr>\n'
            content += '<ITRForm:Gross>0</ITRForm:Gross>\n'
            content += '<ITRForm:AmtCreditSetOfEy>0</ITRForm:AmtCreditSetOfEy>\n'
            content += '<ITRForm:AmtCreditBalBroughtFwd>0</ITRForm:AmtCreditBalBroughtFwd>\n'
            content += '<ITRForm:AmtCreditUtilized>0</ITRForm:AmtCreditUtilized>\n'
            content += '<ITRForm:BalAmtCreditCarryFwd>0</ITRForm:BalAmtCreditCarryFwd>\n'
            content += '</ITRForm:ScheduleAMTCDtls>\n'
        
        content += '<ITRForm:CurrAssYr>2019-20</ITRForm:CurrAssYr>\n'
        content += '<ITRForm:CurrYrAmtCreditFwd>0</ITRForm:CurrYrAmtCreditFwd>\n'
        content += '<ITRForm:CurrYrCreditCarryFwd>0</ITRForm:CurrYrCreditCarryFwd>\n'
        content += '<ITRForm:TotAMTGross>0</ITRForm:TotAMTGross>\n'
        content += '<ITRForm:TotSetOffEys>0</ITRForm:TotSetOffEys>\n'
        content += '<ITRForm:TotBalBF>0</ITRForm:TotBalBF>\n'
        content += '<ITRForm:TotAmtCreditUtilisedCY>0</ITRForm:TotAmtCreditUtilisedCY>\n'
        content += '<ITRForm:TotBalAMTCreditCF>0</ITRForm:TotBalAMTCreditCF>\n'
        content += '<ITRForm:TaxSection115JD>0</ITRForm:TaxSection115JD>\n'
        content += '<ITRForm:AmtLiabilityAvailable>0</ITRForm:AmtLiabilityAvailable>\n'
        content += '</ITRForm:ScheduleAMTC>\n'

    except Exception as ex:
        log.error('Error in ScheduleAMT ITR2 '+traceback.format_exc())
    log.info('===== ScheduleAMT Block ends =====')
    return content

def TDSHeadOfIncome(section):
    log.info(section)
    TDSHeadOfIncome_code=''
    try:
        if section in '194I':
            TDSHeadOfIncome_code='HP'
        elif section=='194A':
            TDSHeadOfIncome_code='OS'
        elif section=='194A':
            TDSHeadOfIncome_code='OS'
        elif section=='194':
            TDSHeadOfIncome_code='EI'

    except Exception as ex:
        log.error('Error in get TDSHeadOfIncome '+traceback.format_exc())
    return TDSHeadOfIncome_code


def ReturnFileSec():
    ReturnFileSec_code=''
    try:
        now = datetime.now()
        current_date = now.strftime("%Y-%m-%d")

        return_filing_date = datetime.strptime(current_date, '%Y-%m-%d').date()
        return_filing_due_date1 = datetime.strptime(return_filing_due_date, '%Y-%m-%d').date()

        if return_filing_date<=return_filing_due_date1:
            ReturnFileSec_code='11'
        elif return_filing_date>return_filing_due_date1:
            ReturnFileSec_code='12'

    except Exception as ex:
        log.info('Error in calculate ReturnFileSec '+traceback.format_exc())
    return ReturnFileSec_code

def NatureOfEmployment_func(Employer_category):
    log.info('Employer_category '+str(Employer_category))
    NatureOfEmployment_code=''
    try:
        if Employer_category=='Other':
            NatureOfEmployment_code='OTH'
        else:
            NatureOfEmployment_code=Employer_category
    except Exception as ex:
        log.info('Error in calculate ReturnFileSec '+traceback.format_exc())
    return NatureOfEmployment_code

def GrossSalary_func(salary,perquisites,profit_in_lieu):
    log.info('salary amount '+str(salary))
    log.info('perquisites amount '+str(perquisites))
    log.info('profit_in_lieu amount '+str(profit_in_lieu))

    GrossSalary_amount=0
    try:
        GrossSalary_amount=int(salary)+int(perquisites)+int(profit_in_lieu)
    except Exception as ex:
        log.info('Error in calculate ReturnFileSec '+traceback.format_exc())
    return GrossSalary_amount

def get_salary_breakup(company_name):
    salary_breakup_json={}
    try:
        salary_breakup_data=SalaryBreakUp.objects.filter(R_Id=R_ID,company_name=company_name)
        if salary_breakup_data.exists():
            salary_breakup_serializer=SalaryBreakupSerializer(salary_breakup_data,many='true')
            salary_breakup_json=salary_breakup_serializer.data[0]
        else:
            pass
    except Exception as ex:
        log.info('Error in getting salary breakup '+traceback.format_exc())
    return salary_breakup_json

def SalaryBreakupNatureDesc(key):
    SalaryBreakupNatureDesc_code=''
    try:
        """DB fields{
                "R_Id": 0, 
                "company_name": "ACCENTURE", 
                "basic_salary": 0, 
                "conveyance_allowance": 0, 
                "annuity_or_pension": null, 
                "commuted_pension": null, 
                "gratuity": null, 
                "fees_commission": null, 
                "advance_of_salary": null, 
                "leave_encashment": 0, 
                "others": 20000
            }"""

        """XML fields
        1 - Basic Salary
        2 - Dearness Allowance
        3 - Conveyance Allowance
        4 - House Rent Allowance
        5 - Leave Travel Allowance
        6 - Children Education Allowance
        7 - Other Allowance
        8 - The contribution made  by the Employer  towards  pension scheme as referred u/s 80CCD
        9 - Amount deemed to be income under rule 11 of Fourth Schedule
        10 - Amount deemed to be income under rule 6 of Fourth Schedule
        11 - Annuity or pension
        12 - Commuted Pension
        13 - Gratuity
        14 - Fees/ commission
        15 - Advance of salary
        16 - Leave Encashment
        OTH - Others
        """
        if key=='basic_salary':
            SalaryBreakupNatureDesc_code='1'
        elif key=='conveyance_allowance':
            SalaryBreakupNatureDesc_code='3'
        elif key=='annuity_or_pension':
            SalaryBreakupNatureDesc_code='11'
        elif key=='commuted_pension':
            SalaryBreakupNatureDesc_code='12'
        elif key=='gratuity':
            SalaryBreakupNatureDesc_code='13'
        elif key=='fees_commission':
            SalaryBreakupNatureDesc_code='14'
        elif key=='advance_of_salary':
            SalaryBreakupNatureDesc_code='15'
        elif key=='leave_encashment':
            SalaryBreakupNatureDesc_code='16'
        elif key=='others':
            SalaryBreakupNatureDesc_code='OTH'

    except Exception as ex:
        log.info('Error in getting SalaryBreakupNatureDesc '+traceback.format_exc())
    return SalaryBreakupNatureDesc_code

def get_perquisites_breakup(company_name):
    perquisites_breakup_json={}
    try:
        perquisites_breakup_data=PerquisiteBreakUp.objects.filter(R_Id=R_ID,company_name=company_name)
        if perquisites_breakup_data.exists():
            log.info('perquisites entry exists')
            perquisites_breakup_serializer=PerquisiteBreakUpSerializer(perquisites_breakup_data,many='true')
            perquisites_breakup_json=perquisites_breakup_serializer.data[0]
        else:
            log.info('perquisites entry not exists')
            pass
    except Exception as ex:
        log.info('Error in getting perquisites_breakup '+traceback.format_exc())
    return perquisites_breakup_json

def PerquisitesBreakupNatureDesc(key):
    PerquisitesBreakupNatureDesc_code=''
    try:
        """
        OrderedDict(
        [('R_Id', 0), 
        ('company_name', u'ACCENTURE'), 
        ('accommodation', 0), 
        ('cars_or_other_automotive', 0), 
        ('utilitiy_bills', 0), 
        ('concessional_travel', 0), 
        ('free_meals', 0), 
        ('free_education', 0),
         ('other_benefits', 1000)]
         )

        """
        """xml fields
        1 -  Accommodation
        2 - Cars / Other Automotive
        3 - Sweeper, gardener, watchman or personal attendant
        4 - Gas, electricity, water
        5 - Interest free or concessional loans
        6 - Holiday expenses
        7 - Free or concessional travel
        8 - Free meals
        9 - Free education
        10 - Gifts, vouchers, etc.
        11 - Credit card expenses
        12 - Club expenses
        13 - Use of movable assets by employees
        14 - Transfer of assets to employee
        15 - Value of any other benefit/amenity/service/privilege
        16 - Stock options (non-qualified options)
        17 - Tax paid by employer on non-monetary perquisite
        OTH - Other benefits or amenities

        """
        if key=='accommodation':
            PerquisitesBreakupNatureDesc_code='1'
        elif key=='cars_or_other_automotive':
            PerquisitesBreakupNatureDesc_code='2'
        elif key=='utilitiy_bills':
            PerquisitesBreakupNatureDesc_code='4'
        elif key=='concessional_travel':
            PerquisitesBreakupNatureDesc_code='7'
        elif key=='free_meals':
            PerquisitesBreakupNatureDesc_code='8'
        elif key=='free_education':
            PerquisitesBreakupNatureDesc_code='9'
        elif key=='other_benefits':
            PerquisitesBreakupNatureDesc_code='OTH'
    except Exception as ex:
        log.info('Error in getting PerquisitesBreakupNatureDesc '+traceback.format_exc())
    return PerquisitesBreakupNatureDesc_code

def get_profit_lieu_breakup(company_name):
    profit_lieu_breakup_json={}
    try:
        profit_lieu_breakup_data=ProfitinLieuBreakUp.objects.filter(R_Id=R_ID,company_name=company_name)
        if profit_lieu_breakup_data.exists():
            profit_lieu_breakup_serializer=ProfitinLieuBreakUpSerializer(profit_lieu_breakup_data,many='true')
            profit_lieu_breakup_json=profit_lieu_breakup_serializer.data[0]
            log.info('ProfitinLieuBreakUp entry exists')
        else:
            log.info('ProfitinLieuBreakUp entry not exists')
    except Exception as ex:
        log.info('Error in getting profit_lieu_breakup '+traceback.format_exc())
    return profit_lieu_breakup_json

def ProfitLieuBreakupNatureDesc(key):
    ProfitLieuBreakupNatureDesc_code=''
    try:
        """db fields
         OrderedDict([
         ('R_Id', 0), 
         ('company_name', u'INFOSYS'), 
         ('termination_compensation', 0), 
         ('keyman_insurance', 0), 
         ('other_profit_in_lieu_salary', 0), ('other', 100)
         ])

        """
        """XML Fields
        1 - Compensation due/received by an assessee from his employer or former employer in connection with the  termination of his employment or modification thereto
        2 - Any payment due/received by an assessee from his employer or a former employer or from a provident or other fund, sum received under Keyman Insurance Policy including Bonus thereto
        3 - Any amount due/received by assessee from any person before joining or after cessation of employment with that person
        OTH - Any Other
        """
        if key=='termination_compensation':
            ProfitLieuBreakupNatureDesc_code='1'
        elif key=='keyman_insurance':
            ProfitLieuBreakupNatureDesc_code='2'
        elif key=='other_profit_in_lieu_salary':
            ProfitLieuBreakupNatureDesc_code='3'
        elif key=='other':
            ProfitLieuBreakupNatureDesc_code='OTH'

    except Exception as ex:
        log.info('Error in getting ProfitLieuBreakupNatureDesc '+traceback.format_exc())
    return ProfitLieuBreakupNatureDesc_code

def get_total_allowances_breakup():
    total_allowances_breakup_json={}
    try:
        total_allowances_breakup_data=allowances.objects.filter(R_id=R_ID)
        if total_allowances_breakup_data.exists():
            total_allowances_breakup_serializer=allowancesSerializer(total_allowances_breakup_data,many='true')
            total_allowances_breakup_json=total_allowances_breakup_serializer.data
            log.info('total_allowances_breakup entry exists')
        else:
            log.info('total_allowances_breakup entry not exists')
    except Exception as ex:
        log.info('Error in getting total_allowances_breakup_json '+traceback.format_exc())
    return total_allowances_breakup_json

def AllowancesBreakupNatureDesc(key):
    AllowancesBreakupNatureDesc_code=''
    try:
        pass
    except Exception as ex:
        log.info('Error in getting AllowancesBreakupNatureDesc '+traceback.format_exc())
    return AllowancesBreakupNatureDesc_code

def get_co_owner(Property_Id):
    co_owner_json={}
    try:
        co_owner_data=Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=Property_Id)
        if co_owner_data.exists():
            co_owner_serializer=PropertyOwnerDetailsSerializer(co_owner_data,many='true')
            co_owner_json=co_owner_serializer.data
    except Exception as ex:
        log.info('Error in getting co_owner details '+traceback.format_exc())
    return co_owner_json

def get_tenant(property_id):
    tenant_json={}
    try:
        tenant_data=TenantDetails.objects.filter(R_Id=R_ID,Property_Id=property_id)
        if tenant_data.exists():
            tenant_data_serializer=TenantDetailsSerializer(tenant_data,many='true')
            tenant_json=tenant_data_serializer.data
    except Exception as ex:
        log.info('Error in getting tenant details '+traceback.format_exc())
    return tenant_json


def get_setoffs(D5,D7,D9,D10,E4,G4,I4,J4):
    log.info('===============Setoff calculation starts========')
    global taxable_income
    result={}
    setoff_a,setoff_b,setoff_c,setoff_d,setoff_e,setoff_f,setoff_g,setoff_h= (0,)*8
    
    try:
        log.info('D5 '+str(D5))
        log.info('D7 '+str(D7))
        log.info('D9 '+str(D9))
        log.info('D10 '+str(D10))
        log.info('E4 '+str(E4))
        log.info('G4 '+str(G4))
        log.info('I4 '+str(I4))
        log.info('J4 '+str(J4))
        
        AR=taxable_income

        log.info('AR '+str(AR))
        # taxable Income
        # If (AR > 20%) then
        if int(AR) >= 100000:
            log.info('AR greater than or equal 100000')
            # setoff_a calculation
            # If (D7 < E4 & D7 > 0) then
                # ( a = D7
                #   E4 = E4 - D7
                # )

            # If (D7 > E4 & E4 > 0) then
                # ( a = E4
                #   D7 = D7 - E4
                #   E4 = 0
                # )
            if D7 < E4 and D7 > 0:
                setoff_a=D7
                E4 = E4 - D7

            if D7 < E4 and E4 > 0:
                setoff_a=E4
                D7 = D7 - E4
                E4 = 0

            # If (D10 > I4 & I4 >0) then
            #    ( g = I4
            #    D10 = D10 - I4
            #    I4 = 0
            #    )


            #    If (D10 > G4 & G4 >0) then
            #    ( f = G4
            #    D10 = D10 - G4
            #    G4 = 0
            #    )
            #   If (D10 > E4 & E4 >0) then
            #    ( c = E4
            #    D10 = D10 - E4
            #    E4 = 0
            #    )
            if D10 > I4 and I4 > 0:
                setoff_g=I4
                D10 = D10 - I4
                I4=0

                if D10 > G4 and G4 > 0:
                    setoff_f=G4
                    D10=D10-G4
                    G4=0

                if D10 > E4 and E4 > 0:
                    setoff_c=E4
                    D10=D10-E4
                    E4=0

            # If (D10 < I4 & D10 >0) then
            # ( g = D10
            #   I4 = I4 - D10
            #   D10 = 0
            # )
            if D10 < I4 and D10 > 0:
                setoff_g=D10
                I4=I4-D10
                D10=0

            # If (D5 > G4 & G4 >0) then
            # ( d = G4
            #   D5 = D5 - G4
            #   G4 = 0
            # ) 
            if D5 > G4 and G4 > 0:
                setoff_d=G4
                D5=D5-G4
                G4=0

            # If (D5 < G4 & D5 >0) then
            # ( d = D5
            #   G4 = G4 - D5  
            # )
            if D5 < G4 and D5 > 0:
                setoff_d=D5
                G4=G4-D5

            # If (D9 > J4 & J4 > 0) then
            #   ( h = J4
            #     D9 = D9 - J4
            #    )

            #    If (D9 > G4 & G4 >0) then
            #     ( e = G4
            #       D9 = D9 - G4
            #       G4 = 0
            #     )

            #    If (D9 > E4 & E4 > 0) then
            #     ( b = E4
            #       D9 = D9 - E4
            #       E4 = 0
            #     )
            if D9 > J4 and J4 > 0:
                setoff_h=J4
                D9=D9-J4

                if D9 > G4 and G4 > 0:
                    setoff_e=G4
                    D9=D9-G4
                    G4=0

                if D9 > E4 and E4 > 0:
                    setoff_b=E4
                    D9=D9-E4
                    E4=0

            # If(D9 < J4 & D9 > 0) then
            #    ( h = D9
            #      D9 = 0
            #     )
            if D9 < J4 and D9 > 0:
                setoff_h=D9
                D9=0
            
        elif AR < 100000:
            log.info('AR is less than 100000')
            # If (D10 > I4 & I4 >0) then
            #    ( g = I4
            #    D10 = D10 - I4
            #    I4 = 0
            #    )


            #    If (D10 > G4 & G4 >0) then
            #    ( f = G4
            #    D10 = D10 - G4
            #    G4 = 0
            #    )
              
            #   If (D10 > E4 & E4 >0) then
            #    ( c = E4
            #    D10 = D10 - E4
            #    E4 = 0
            #    )
            if D10 > I4 and I4 > 0:
                setoff_g=I4
                D10 = D10 - I4
                I4=0

                if D10 > G4 and G4 > 0:
                    setoff_f=G4
                    D10=D10-G4
                    G4=0

                if D10 > E4 and E4 > 0:
                    setoff_c=E4
                    D10=D10-E4
                    E4=0

            # If (D10 < I4 & D10 >0) then
            # ( g = D10
            #   I4 = I4 - D10
            #   D10 = 0
            # )
            if D10 < I4 and D10 > 0:
                setoff_g=D10
                I4=I4-D10
                D10=0


            # If (D5 > G4 & G4 >0) then
            # ( d = G4
            #   D5 = D5 - G4
            #   G4 = 0
            # ) 
            if D5 > G4 and G4 > 0:
                setoff_d=G4
                D5=D5-G4
                G4=0

            # If (D5 < G4 & D5 >0) then
            # ( d = D5
            #   G4 = G4 - D5  
            # ) 
            if D5 < G4 and D5 > 0:
                setoff_d=D5
                G4=G4-D5

            # If (D9 > J4 & J4 > 0) then
            #   ( h = J4
            #     D9 = D9 - J4
            #    )

            #    If (D9 > G4 & G4 >0) then
            #     ( e = G4
            #       D9 = D9 - G4
            #       G4 = 0
            #     )

            #    If (D9 > E4 & E4 > 0) then
            #     ( b = E4
            #       D9 = D9 - E4
            #       E4 = 0
            #     )
            if D9 > J4 and J4 > 0:
                setoff_h=J4
                D9=D9-J4

                if D9 > G4 and G4 > 0:
                    setoff_e=G4
                    D9=D9-G4
                    G4=0

                if D9 > E4 and E4 > 0:
                    setoff_b=E4
                    D9=D9-E4
                    E4=0

            # If(D9 < J4 & D9 > 0) then
            #    ( h = D9
            #      D9 = 0
            #     )
            if D9 < J4 and D9 > 0:
                setoff_h=D9
                D9=0

            # If (D7 < E4 & D7 > 0) then
            #   ( a = D7
            #     E4 = E4 - D7
            #   )

            # If (D7 > E4 & E4 > 0) then
            #   ( a = E4
            #     D7 = D7 - E4
            #     E4 = 0
            #   )

            if D7 < E4 and D7 > 0:
                setoff_a=D7
                E4 = E4 - D7

            if D7 < E4 and E4 > 0:
                setoff_a=E4
                D7 = D7 - E4
                E4 = 0

        result['setoff_a']=setoff_a
        result['setoff_b']=setoff_b
        result['setoff_c']=setoff_c
        result['setoff_d']=setoff_d
        result['setoff_e']=setoff_e
        result['setoff_f']=setoff_f
        result['setoff_g']=setoff_g
        result['setoff_h']=setoff_h
    except Exception as ex:
        log.info('Error in get_setoffs '+traceback.format_exc())

    log.info('===============Setoff calculation ends========')
    return result


def get_CYLA_setoff(HP_Loss_Setoff,E6,E8,E10,E12,E13,E15):
    log.info('===============CYLA Setoff calculation starts========')
    global taxable_income
    result={}
    CYLA_setoff_a,CYLA_setoff_b,CYLA_setoff_c,CYLA_setoff_d,CYLA_setoff_e,CYLA_setoff_f= (0,)*6
    CYLA_setoff_g,CYLA_setoff_h,CYLA_setoff_i,CYLA_setoff_j,CYLA_setoff_k,CYLA_setoff_l= (0,)*6
    try:
        log.info('HP_Loss_Setoff '+str(HP_Loss_Setoff))
        log.info('E6 '+str(E6))
        log.info('E8 '+str(E8))
        log.info('E10 '+str(E10))
        log.info('E12 '+str(E12))
        log.info('E13 '+str(E13))
        log.info('E15 '+str(E15))
        # HP_Loss to be set off (HP_Loss_Setoff) = min (2 lacs, H653)
        # HP_Loss_Setoff=min(200000,0)
        # taxable Income
        AR=taxable_income

        log.info('AR '+str(AR))

        if int(AR) >= 100000:
            log.info('AR greater than or equal 100000')
            # finding CYLA_setoff_a
            # If (HP_Loss_Setoff > E6 & E6 > 0) then
            # ( a = E6
            #   HP_Loss_Setoff = HP_Loss_Setoff - E6
            # )

            # If (HP_Loss_Setoff < E6 & HP_Loss_Setoff > 0) then
            # ( a = HP_Loss_Setoff
            #   E6 = E6 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E6 and E6 >0:
                CYLA_setoff_a=E6
                HP_Loss_Setoff=HP_Loss_Setoff-E6

            log.info('HP_Loss_Setoff after checking CYLA_a first condition '+str(HP_Loss_Setoff))

            if HP_Loss_Setoff < E6 and HP_Loss_Setoff >0:
                CYLA_setoff_a=HP_Loss_Setoff
                E6=E6-HP_Loss_Setoff
                HP_Loss_Setoff=HP_Loss_Setoff-CYLA_setoff_a


            log.info('HP_Loss_Setoff after finding CYLA_a second condition '+str(HP_Loss_Setoff))


            # finding CYLA_setoff_c
            # If (HP_Loss_Setoff > E10 & E10 > 0) then
            # ( c = E10
            #   HP_Loss_Setoff = HP_Loss_Setoff - E10
            # )

            # If (HP_Loss_Setoff < E10 & HP_Loss_Setoff > 0) then
            # ( c = HP_Loss_Setoff
            #   E10 = 10 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E10 and E10 >0:
                CYLA_setoff_c=E10
                HP_Loss_Setoff=HP_Loss_Setoff-E10

            if HP_Loss_Setoff < E10 and HP_Loss_Setoff >0:
                CYLA_setoff_c=HP_Loss_Setoff
                E10=E10-HP_Loss_Setoff

            log.info('HP_Loss_Setoff after finding CYLA_c '+str(HP_Loss_Setoff))

            # finding CYLA_setoff_f
            # If (HP_Loss_Setoff > E15 & E15 > 0) then
            # ( f = E15
            #   HP_Loss_Setoff = HP_Loss_Setoff - E15
            # )

            # If (HP_Loss_Setoff < E15 & HP_Loss_Setoff > 0) then
            # ( f = HP_Loss_Setoff
            #   E15 = E15 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E15 and E15 >0:
                CYLA_setoff_f=E15
                HP_Loss_Setoff=HP_Loss_Setoff-E15

            if HP_Loss_Setoff < E15 and HP_Loss_Setoff >0:
                CYLA_setoff_f=HP_Loss_Setoff
                E15=E15-HP_Loss_Setoff

            log.info('HP_Loss_Setoff after finding CYLA_f '+str(HP_Loss_Setoff))


            # finding CYLA_setoff_e
            # If (HP_Loss_Setoff > E13 & E13 > 0) then
            # ( e = E13
            #   HP_Loss_Setoff = HP_Loss_Setoff - E13
            # )

            # If (HP_Loss_Setoff < E13 & HP_Loss_Setoff > 0) then
            # ( e = HP_Loss_Setoff
            #   E13 = E13 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E13 and E13 >0:
                CYLA_setoff_e=E13
                HP_Loss_Setoff=HP_Loss_Setoff-E13

            if HP_Loss_Setoff < E13 and HP_Loss_Setoff >0:
                CYLA_setoff_e=HP_Loss_Setoff
                E13=E13-HP_Loss_Setoff

            log.info('HP_Loss_Setoff after finding CYLA_e '+str(HP_Loss_Setoff))

            # finding CYLA_setoff_b
            # If (HP_Loss_Setoff > E8 & E8 > 0) then
            # ( b = E8
            #   HP_Loss_Setoff = HP_Loss_Setoff - E8
            # )

            # If (HP_Loss_Setoff < E8 & HP_Loss_Setoff > 0) then
            # ( b = HP_Loss_Setoff
            #   E8 = E8 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E8 and E8 >0:
                CYLA_setoff_b=E8
                HP_Loss_Setoff=HP_Loss_Setoff-E8

            if HP_Loss_Setoff < E8 and HP_Loss_Setoff >0:
                CYLA_setoff_b=HP_Loss_Setoff
                E8=E8-HP_Loss_Setoff


            log.info('HP_Loss_Setoff after finding CYLA_b '+str(HP_Loss_Setoff))
            # finding CYLA_setoff_d
            # If (HP_Loss_Setoff > E12 & E12 > 0) then
            # ( d = E12
            #   HP_Loss_Setoff = HP_Loss_Setoff - E12
            # )

            # If (HP_Loss_Setoff < E12 & HP_Loss_Setoff > 0) then
            # ( d = HP_Loss_Setoff
            #   E12 = E12 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E12 and E12 >0:
                CYLA_setoff_d=E12
                HP_Loss_Setoff=HP_Loss_Setoff-E12

            if HP_Loss_Setoff < E12 and HP_Loss_Setoff >0:
                CYLA_setoff_d=HP_Loss_Setoff
                E12=E12-HP_Loss_Setoff


        elif AR < 100000:
            log.info('AR is less than 100000')
            # finding CYLA_setoff_e
            # If (HP_Loss_Setoff > E13 & E13 > 0) then
            # ( e = E13
            #   HP_Loss_Setoff = HP_Loss_Setoff - E13
            # )

            # If (HP_Loss_Setoff < E13 & HP_Loss_Setoff > 0) then
            # ( e = HP_Loss_Setoff
            #   E13 = E13 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E13 and E13 >0:
                CYLA_setoff_e=E13
                HP_Loss_Setoff=HP_Loss_Setoff-E13

            if HP_Loss_Setoff < E13 and HP_Loss_Setoff >0:
                CYLA_setoff_e=HP_Loss_Setoff
                E13=E13-HP_Loss_Setoff

            # finding CYLA_setoff_b
            # If (HP_Loss_Setoff > E8 & E8 > 0) then
            # ( b = E8
            #   HP_Loss_Setoff = HP_Loss_Setoff - E8
            # )

            # If (HP_Loss_Setoff < E8 & HP_Loss_Setoff > 0) then
            # ( b = HP_Loss_Setoff
            #   E8 = E8 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E8 and E8 >0:
                CYLA_setoff_b=E8
                HP_Loss_Setoff=HP_Loss_Setoff-E8

            if HP_Loss_Setoff < E8 and HP_Loss_Setoff >0:
                CYLA_setoff_b=HP_Loss_Setoff
                E8=E8-HP_Loss_Setoff

            # finding CYLA_setoff_d
            # If (HP_Loss_Setoff > E12 & E12 > 0) then
            # ( d = E12
            #   HP_Loss_Setoff = HP_Loss_Setoff - E12
            # )

            # If (HP_Loss_Setoff < E12 & HP_Loss_Setoff > 0) then
            # ( d = HP_Loss_Setoff
            #   E12 = E12 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E12 and E12 >0:
                CYLA_setoff_d=E12
                HP_Loss_Setoff=HP_Loss_Setoff-E12

            if HP_Loss_Setoff < E12 and HP_Loss_Setoff >0:
                CYLA_setoff_d=HP_Loss_Setoff
                E12=E12-HP_Loss_Setoff

            # finding CYLA_setoff_a
            # If (HP_Loss_Setoff > E6 & E6 > 0) then
            # ( a = E6
            #   HP_Loss_Setoff = HP_Loss_Setoff - E6
            # )

            # If (HP_Loss_Setoff < E6 & HP_Loss_Setoff > 0) then
            # ( a = HP_Loss_Setoff
            #   E6 = E6 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E6 and E6 >0:
                CYLA_setoff_a=E6
                HP_Loss_Setoff=HP_Loss_Setoff-E6

            if HP_Loss_Setoff < E6 and HP_Loss_Setoff >0:
                CYLA_setoff_a=HP_Loss_Setoff
                E6=E6-HP_Loss_Setoff


            # finding CYLA_setoff_c
            # If (HP_Loss_Setoff > E10 & E10 > 0) then
            # ( c = E10
            #   HP_Loss_Setoff = HP_Loss_Setoff - E10
            # )

            # If (HP_Loss_Setoff < E10 & HP_Loss_Setoff > 0) then
            # ( c = HP_Loss_Setoff
            #   E10 = 10 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E10 and E10 >0:
                CYLA_setoff_c=E10
                HP_Loss_Setoff=HP_Loss_Setoff-E10

            if HP_Loss_Setoff < E10 and HP_Loss_Setoff >0:
                CYLA_setoff_c=HP_Loss_Setoff
                E10=E10-HP_Loss_Setoff

            # finding CYLA_setoff_f
            # If (HP_Loss_Setoff > E15 & E15 > 0) then
            # ( f = E15
            #   HP_Loss_Setoff = HP_Loss_Setoff - E15
            # )

            # If (HP_Loss_Setoff < E15 & HP_Loss_Setoff > 0) then
            # ( f = HP_Loss_Setoff
            #   E15 = E15 - HP_Loss_Setoff
            # )
            if HP_Loss_Setoff > E15 and E15 >0:
                CYLA_setoff_f=E15
                HP_Loss_Setoff=HP_Loss_Setoff-E15

            if HP_Loss_Setoff < E15 and HP_Loss_Setoff >0:
                CYLA_setoff_f=HP_Loss_Setoff
                E15=E15-HP_Loss_Setoff

        result['CYLA_setoff_a']=CYLA_setoff_a
        result['CYLA_setoff_b']=CYLA_setoff_b
        result['CYLA_setoff_c']=CYLA_setoff_c
        result['CYLA_setoff_d']=CYLA_setoff_d
        result['CYLA_setoff_e']=CYLA_setoff_e
        result['CYLA_setoff_f']=CYLA_setoff_f
        result['CYLA_setoff_g']=CYLA_setoff_g
        result['CYLA_setoff_h']=CYLA_setoff_h
        result['CYLA_setoff_i']=CYLA_setoff_i
        result['CYLA_setoff_j']=CYLA_setoff_j
        result['CYLA_setoff_k']=CYLA_setoff_k
        result['CYLA_setoff_l']=CYLA_setoff_l
    except Exception as ex:
        log.info('Error in getting get_CYLA_setoff '+traceback.format_exc())
    log.info('===============CYLA Setoff calculation ends========')
    return result


def check_null(value):
    if value=='null' or value==None:
        return str(0)
    else:
        return str(value)


