from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
# 
import logging
import json
import traceback

from .models import SalatTaxUser
from .models import Variables_80d,State_Code,VI_Deductions,Client_fin_info1,Personal_Details,Return_Details,Donation_Details
#for url request 

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

@csrf_exempt
def save_common_deductions(request):
	if request.method=='POST':
		result={}
		try:
			data_pass=request.POST.get('data_pass')
			req_data = json.loads(data_pass)
			log.info(req_data)

			client_id=req_data['client_id']
			S_80C=req_data['S_80C']
			S_80CCD=req_data['S_80CCD']
			S_80CCD1=req_data['S_80CCD1']
			S_80CCD2=req_data['S_80CCD2']
			S_80D=req_data['S_80D']
			S_80G=req_data['S_80G']
			S_80DD=req_data['S_80DD']
			# S_80TTA=req_data['S_80TTA']
			S_80TTB=req_data['S_80TTB']

			R_ID = get_rid(client_id)

			if S_80C=='':
				S_80C=0
			if S_80CCD=='':
				S_80CCD=0

			if S_80CCD1=='':
				S_80CCD1=0
			if S_80CCD2=='':
				S_80CCD2=0

			if S_80D=='':
				S_80D=0
			if S_80G=='':
				S_80G=0
			if S_80DD=='':
				S_80DD=0
			if S_80TTB=='':
				S_80TTB=0

			# if S_80TTB!=0:
			# 	S_80TTA=0
			# else:
			# 	S_80TTA=None

			parent_dependent=req_data['parent_dependent']
			parent_senier_citizen=req_data['parent_senier_citizen']

			if parent_senier_citizen=='yes':
				parent_citizen_type='SC'
			else:
				parent_citizen_type='OC'

			self_citizen_type=req_data['self_citizen_type']
			self_health_checkup=req_data['self_health_checkup']
			self_insurance_premium=req_data['self_insurance_premium']
			self_medical_expenditure=req_data['self_medical_expenditure']

			log.info('self_health_checkup '+str(self_health_checkup))
			log.info('self_insurance_premium '+str(self_insurance_premium))
			log.info('self_medical_expenditure '+str(self_medical_expenditure))

			parent_health_checkup=req_data['parent_health_checkup']
			parent_insurance_premium=req_data['parent_insurance_premium']
			parent_medical_expenditure=req_data['parent_medical_expenditure']

			log.info('parent_health_checkup '+str(parent_health_checkup))
			log.info('parent_insurance_premium '+str(parent_insurance_premium))
			log.info('parent_medical_expenditure '+str(parent_medical_expenditure))

			if parent_dependent=='no':
				parent_health_checkup=0
				parent_insurance_premium=0
				parent_medical_expenditure=0

			if parent_citizen_type=='no':
				parent_medical_expenditure=0

			if self_citizen_type=='OC':
				self_medical_expenditure=0

			var_80D_Self=0
			var_80D_parents=0
			var_80D_Self_Limit=0
			var_80D_parents_Limit=0
			var_80D_Deduction=0

			log.info('parent_dependent '+str(parent_dependent))
			log.info('parent_citizen_type '+str(parent_citizen_type))
			log.info('self_citizen_type '+str(self_citizen_type))

			"""
			If (Self = Ordinary Citizen) Then
				PHC = min(5000, PHC(user input))
				MI = MI(user input)
				ME = 0 
				80D_Self = PHC + MI + ME
				80D_Self_Limit  = 25000
			"""
			log.info('self_citizen_type '+str(self_citizen_type))

			if self_citizen_type=='OC':
				log.info('self_citizen_type condition '+self_citizen_type)
				PHC=min(5000,check_int(self_health_checkup))
				MI=check_int(self_insurance_premium)
				ME=0
				var_80D_Self = PHC + MI + ME
				var_80D_Self_Limit  = 25000

			"""
			If (Self =  Senior Citizen or Super Senior Citizen) Then
				PHC = min(5000, PHC(user input)
				MI = MI (user input)
				ME = ME (user input) 
				80D_Self = PHC + MI + ME
				80D_Self_Limit  = 50000 
			""" 

			if self_citizen_type=='SC' or self_citizen_type=='SSC':
				log.info('self_citizen_type condition '+self_citizen_type)
				PHC=min(5000,check_int(self_health_checkup))
				MI=check_int(self_insurance_premium)
				ME=check_int(self_medical_expenditure)
				var_80D_Self = PHC + MI + ME
				var_80D_Self_Limit  = 50000

			"""
			If (Parents = Ordinary   Citizen  & Dependent) Then
				PHC = min(5000, PHC(user input))
				MI = MI(user input)
				ME = 0
				80D_parents = PHC + MI + ME
				80D_parents_Limit  = 25000 
			""" 

			log.info('parent_citizen_type '+parent_citizen_type)
			log.info('parent_dependent '+parent_dependent)

			if parent_citizen_type=='OC' and parent_dependent=='yes':
				log.info('parent_citizen_type condition '+parent_citizen_type)
				PHC1=min(5000,check_int(parent_health_checkup))
				MI1=check_int(parent_insurance_premium)
				ME1=0
				var_80D_parents = PHC1 + MI1 + ME1
				var_80D_parents_Limit  = 25000
			"""
			If (Parents =  Senior Citizen or Super Senior Citizen) Then
				PHC = min(5000, PHC(user input))
				MI = MI (user input)
				ME = ME (user input)
				80D_parents = PHC + MI + ME
				80D_parents_Limit  = 50000 
			"""
			if parent_citizen_type=='SC' or parent_citizen_type=='SSC':
				log.info('parent_citizen_type condition'+parent_citizen_type)
				PHC1=min(5000,check_int(parent_health_checkup))
				MI1=check_int(parent_insurance_premium)
				ME1=check_int(parent_medical_expenditure)
				var_80D_parents = PHC1 + MI1 + ME1
				var_80D_parents_Limit  = 50000

			""" 
			
			80D_User = 80D_Self + 80D_parents
			80D_Limit =  80D_Self_Limit  + 80D_parents_Limit  
			80D Deduction = min (80D_Limit, 80D_User)

			"""

			log.info('var_80D_Self '+str(var_80D_Self))
			log.info('var_80D_parents '+str(var_80D_parents))


			var_80d_user=var_80D_Self + var_80D_parents
			var_80D_Limit=var_80D_Self_Limit  + var_80D_parents_Limit
			var_80D_Deduction = min (var_80D_Limit, var_80d_user)

			log.info('var_80d_user '+str(var_80d_user))
			log.info('var_80D_Deduction '+str(var_80D_Deduction))
			S_80D=var_80d_user

			log.info('S_80C '+str(S_80C))
			log.info('S_80CCD '+str(S_80CCD))
			log.info('S_80D '+str(S_80D))
			log.info('S_80G '+str(S_80G))
			log.info('S_80DD '+str(S_80DD))
			log.info('S_80CCD1 '+str(S_80CCD1))
			log.info('S_80CCD2 '+str(S_80CCD2))
			# log.info('S_80TTA '+str(S_80TTA))
			log.info('S_80TTB '+str(S_80TTB))

			if self_citizen_type=='OC':
				S_80TTA=S_80TTB
				S_80TTB=0
			elif self_citizen_type=='SC' or self_citizen_type=='SSC':
				S_80TTB=S_80TTB
				S_80TTA=0
			else:
				S_80TTB=0
				S_80TTA=0

			log.info('S_80TTA '+str(S_80TTA))
			log.info('S_80TTB '+str(S_80TTB))

			result['status']= 'start'
			if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
				VI_Deductions.objects.create(
					R_Id = R_ID,
					VI_80_C = S_80C,
					VI_80_CCD_1B= S_80CCD,
					VI_80_D = S_80D,
					VI_80_G = S_80G,
					VI_80_DD = S_80DD,
					VI_80_CCD_1 = S_80CCD1,
					VI_80_CCD_2 = S_80CCD2,
					VI_80_TTA = S_80TTA,
					VI_80_TTB = S_80TTB,
				).save()
			else:
				Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)
				Deductions_instance.VI_80_C = S_80C
				Deductions_instance.VI_80_CCD_1B = S_80CCD
				Deductions_instance.VI_80_D = S_80D
				Deductions_instance.VI_80_G = S_80G
				Deductions_instance.VI_80_DD = S_80DD
				Deductions_instance.VI_80_TTA = S_80TTA
				Deductions_instance.VI_80_TTB = S_80TTB
				Deductions_instance.VI_80_CCD_1 = S_80CCD1
				Deductions_instance.VI_80_CCD_2 = S_80CCD2
				Deductions_instance.save()

			# ========================80D calculation================


			if Personal_Details.objects.filter(P_Id=client_id).exists():
				personal_instance=Personal_Details.objects.get(P_Id=client_id)
				personal_instance.dependent_parent_age=parent_citizen_type
				personal_instance.save()

				log.info('Parent citizen type Updated')

			if not Variables_80d.objects.filter(R_Id=R_ID).exists():
				Variables_80d.objects.create(
					R_Id=R_ID,
					self_health_checkup=check_int(self_health_checkup),
					parent_health_checkup=check_int(parent_health_checkup),
					self_insurance_premium=check_int(self_insurance_premium),
					parent_insurance_premium=check_int(parent_insurance_premium),
					self_medical_expenditure=check_int(self_medical_expenditure),
					parent_medical_expenditure=check_int(parent_medical_expenditure)

				).save()
				log.info('Entry Created in Variables_80d table')
			else:
				Variables_80d_instance=Variables_80d.objects.get(R_Id=R_ID)
				Variables_80d_instance.self_health_checkup=check_int(self_health_checkup)
				Variables_80d_instance.parent_health_checkup=check_int(parent_health_checkup)
				Variables_80d_instance.self_insurance_premium=check_int(self_insurance_premium)
				Variables_80d_instance.parent_insurance_premium=check_int(parent_insurance_premium)
				Variables_80d_instance.self_medical_expenditure=check_int(self_medical_expenditure)
				Variables_80d_instance.parent_medical_expenditure=check_int(parent_medical_expenditure)
				Variables_80d_instance.save()
				log.info('Entry Updated in Variables_80d table')

			# ========================80D calulation ends============
			result['status']= 'success'
			result['req_data']= req_data
		except Exception as ex:
			log.info('Error in deduction api : ' + traceback.format_exc())
			result['status']= 'Error in deduction api : ' + traceback.format_exc()

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


def check_int(val):
	if val=='':
		return 0
	else:
		return int(val)

@csrf_exempt
def save_other_deductions(request):
	if request.method=='POST':
		result={}
		try:
			data_pass=request.POST.get('data_pass')
			req_data = json.loads(data_pass)

			client_id =req_data['client_id']
			S_80CCG = req_data['S_80CCG']
			S_80E = req_data['S_80E']
			S_80EE = req_data['S_80EE']
			S_80GG = req_data['S_80GG']
			S_80GGA = req_data['S_80GGA']
			# S_80DD=req_data['S_80DD']
			S_80U = req_data['S_80U']
			S_80DDB=0
			# S_80DDB=req_data['S_80DDB']
			S_80GGC=req_data['S_80GGC']
			R_ID = get_rid(client_id)

			if S_80CCG=='':
				S_80CCG=0
			if S_80E=='':
				S_80E=0
			if S_80EE =='':
				S_80EE =0
			if S_80GG =='':
				S_80GG =0
			if S_80GGA =='':
				S_80GGA =0
			# if S_80DD=='':
			# 	S_80DD=0
			if S_80U == '':
				S_80U= 0
			if S_80DDB == '':
				S_80DDB = 0
			if S_80GGC == '':
				S_80GGC = 0

			result['status']= 'start'
			if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
				VI_Deductions.objects.create(
					R_Id = R_ID,
					VI_80_E= S_80E,
					VI_80_U = S_80U,
					VI_80_CCG = S_80CCG,
					VI_80_EE = S_80EE,
					VI_80_GG = S_80GG,
					VI_80_GGA = S_80GGA,
					VI_80_DDB = S_80DDB,
					VI_80_GGC = S_80GGC,
				).save()
			else:
				Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)
				Deductions_instance.VI_80_E = S_80E
				Deductions_instance.VI_80_U = S_80U
				Deductions_instance.VI_80_CCG = S_80CCG
				Deductions_instance.VI_80_EE = S_80EE
				Deductions_instance.VI_80_GG = S_80GG
				Deductions_instance.VI_80_GGA = S_80GGA
				Deductions_instance.VI_80_DDB = S_80DDB
				Deductions_instance.VI_80_GGC = S_80GGC
				Deductions_instance.save()

			result['status']= 'success'
			result['req_data']= req_data

		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


def calculate_age(client_id):
	age=''
	try:
		for client in Personal_Details.objects.filter(P_Id = client_id):
			if client.dob is None:
				dob = ''
			else:
				dob = client.dob.strftime('%d-%m-%Y')

			if dob == '':
				age = 50
			else:
				age1 = dob.split('-')[2]
				age = 2019 - int(age1)

		return age
	except Exception as ex:
		log.info('Error in caculating age '+ex)

@csrf_exempt
def get_deductions(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			R_ID = get_rid(client_id)
			log.info(R_ID)

			d_80c = 0
			d_80ccd = 0
			d_80ccd1 = 0
			d_80ccd2 = 0
			d_80d = 0
			d_80dd = 0
			d_80g = 0
			d_80tta = 0
			d_80ttb = 0

			d_80e = 0
			d_80u = 0
			d_80ccg = 0
			d_80ee = 0
			d_80gg = 0
			d_80gga = 0
			d_80ddb = 0
			d_80ggc = 0
			citizen_type=''

			# for client in Client_fin_info1.objects.filter(client_id = client_id):
			# 	d_80ccd = float( client.number_80ccd or 0)
			# 	d_80e = float( client.number_80e or 0)
			# 	d_80g = float( client.number_80g or 0)
			# 	d_80tta = float( client.number_80tta or 0)
			if VI_Deductions.objects.filter(R_Id=R_ID).exists():
				for d in VI_Deductions.objects.filter(R_Id = R_ID):
					d_80c = float( d.VI_80_C or 0)
					d_80ccd = float( d.VI_80_CCD_1B or 0)
					d_80ccd1 = float( d.VI_80_CCD_1 or 0)
					d_80ccd2 = float( d.VI_80_CCD_2 or 0)
					d_80d = float( d.VI_80_D or 0)
					d_80dd = float( d.VI_80_DD or 0)
					d_80g = float( d.VI_80_G or 0)
					d_80tta = float( d.VI_80_TTA or 0)
					d_80ttb = float( d.VI_80_TTB or 0)
					d_80e = float( d.VI_80_E or 0)
					d_80u = float( d.VI_80_U or 0)
					d_80ccg = float( d.VI_80_CCG or 0)
					d_80ee = float( d.VI_80_EE or 0)
					d_80gg = float( d.VI_80_GG or 0)
					d_80gga = float( d.VI_80_GGA or 0)
					d_80ddb = float( d.VI_80_DDB or 0)
					d_80ggc = float( d.VI_80_GGC or 0)

			self_citizen_type=0
			self_health_checkup=0
			self_insurance_premium=0
			self_medical_expenditure=0

			parent_health_checkup=0
			parent_insurance_premium=0
			parent_medical_expenditure=0

			if Variables_80d.objects.filter(R_Id=R_ID).exists():
				for data in Variables_80d.objects.filter(R_Id = R_ID):
					self_health_checkup=data.self_health_checkup
					self_insurance_premium=data.self_insurance_premium
					self_medical_expenditure=data.self_medical_expenditure
					parent_health_checkup=data.parent_health_checkup
					parent_insurance_premium=data.parent_insurance_premium
					parent_medical_expenditure=data.parent_medical_expenditure

			if calculate_age(client_id)<60:
				self_citizen_type='OC'
			elif calculate_age(client_id)>=60 and calculate_age(client_id)<90:
				self_citizen_type='SC'
			elif calculate_age(client_id)>=90:
				self_citizen_type='SSC'

			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if personal_instance.dependent_parent_age!=None or personal_instance.dependent_parent_age!='NA':
				parent_dependent='Y'
				parent_citizen_type=personal_instance.dependent_parent_age
			else:
				parent_dependent='N'
				parent_citizen_type='NA'

			result['status']= 'success'
			result['d_80c']= d_80c
			result['d_80ccd']= d_80ccd
			result['d_80ccd1']= d_80ccd1
			result['d_80ccd2']= d_80ccd2
			result['d_80d']= d_80d
			result['d_80dd']= d_80dd
			result['d_80g']= d_80g
			result['d_80tta']= d_80tta
			result['d_80ttb']= d_80ttb
			result['d_80e']= d_80e
			result['d_80u']= d_80u
			result['d_80ccg']= d_80ccg
			result['d_80ee']= d_80ee
			result['d_80gg']= d_80gg
			result['d_80gga']= d_80gga
			result['d_80ddb']= d_80ddb
			result['d_80ggc']= d_80ggc
			result['citizen_type']=self_citizen_type
			result['parent_citizen_type']=parent_citizen_type
			result['parent_dependent']=parent_dependent
			result['self_health_checkup']=self_health_checkup
			result['self_insurance_premium']=self_insurance_premium
			result['self_medical_expenditure']=self_medical_expenditure
			result['parent_health_checkup']=parent_health_checkup
			result['parent_insurance_premium']=parent_insurance_premium
			result['parent_medical_expenditure']=parent_medical_expenditure
		except Exception as ex:
			result['status']= 'Error in getting deduction data:' + traceback.format_exc()

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def get_id(request,id):
	if SalatTaxUser.objects.filter(id=id).exists():
		salattaxid = SalatTaxUser.objects.filter(id=id).first()
	else:
		salattaxid = None

	state_list = State_Code.objects.all()
	state_code_json=[]
	for data in state_list:
		code={}
		code['code']=data.code
		code['state']=data.state
		state_code_json.append(code)

	return render(request,'deductions.html',
		{
		'id':salattaxid,
		'client_id':id,
		'state_list':json.dumps(state_code_json),
		})

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance,FY='2018-2019').exists():
		Return_Details_instance=Return_Details.objects.get(P_id=personal_instance,FY='2018-2019')
		R_ID = Return_Details_instance.R_id
	else:
		log.info('R_ID Entry not exists')
		# for rd in Return_Details.objects.filter(P_id=personal_instance):
		# 	log.info(rd.R_id)

	return R_ID

@csrf_exempt
def save_donee_details(request):
	if request.method=='POST':
		result={}
		try:
			data_pass=request.POST.get('data')
			req_data = json.loads(data_pass)

			client_id=req_data['client_id']
			doneeID = req_data['doneeID']
			name=req_data['name']
			address=req_data['address']
			city=req_data['city']
			state=req_data['state']
			pin=req_data['pin']
			pan=req_data['pan']
			amount=req_data['amount']
			percent=req_data['percent']
			e_amount=req_data['e_amount']
			
			result['status']= 'start'
			R_ID = get_rid(client_id)

			if not Donation_Details.objects.filter(R_Id=R_ID,donation_id=doneeID).exists():
				Donation_Details.objects.create(
					R_Id = R_ID,
					donation_id = doneeID,
					donee_name= name,
					address = address,
					city_town = city,
					state_code = state,
					pin_code = pin,
					donee_pan = pan,
					amt_of_donation = e_amount,
					donation_percent=percent
				).save()
			else:
				Donation_instance = Donation_Details.objects.get(R_Id=R_ID,donation_id=doneeID)
				Donation_instance.donation_id = doneeID
				Donation_instance.donee_name = name
				Donation_instance.address = address
				Donation_instance.city_town = city
				Donation_instance.state_code = state
				Donation_instance.pin_code = pin
				Donation_instance.donee_pan = pan
				Donation_instance.amt_of_donation = e_amount
				Donation_instance.donation_percent=percent
				Donation_instance.save()


			result['status']= 'success'
			result['req_data']= req_data
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_donation(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			R_ID = get_rid(client_id)

			name = []
			address = []
			city = []
			state = []
			pin = []
			pan = []
			amount = []
			percent=[]
			donee_count = 0

			if Donation_Details.objects.filter(R_Id=R_ID).exists():
				for d in Donation_Details.objects.filter(R_Id = R_ID):
					donee_count = donee_count + 1
					name.append( d.donee_name or 0)
					address.append( d.address or 0)
					city.append( d.city_town or 0)
					state.append( d.state_code or 0)
					pin.append( d.pin_code or 0)
					pan.append( d.donee_pan or 0)
					amount.append( int(d.amt_of_donation) or 0)
					percent.append( d.donation_percent or 0)

			result['status']= 'success'
			result['donee_count']= donee_count
			result['name']= name
			result['address']= address
			result['city']= city
			result['state']= state
			result['pin']= pin
			result['pan']= pan
			result['amount']= amount
			result['percent']= percent
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')




