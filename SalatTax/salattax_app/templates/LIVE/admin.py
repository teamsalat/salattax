# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from import_export import resources
from .models import demo,Personal_Details,Address_Details,Return_Details,Original_return_Details,Employer_Details
from .models import Employer_Details,Bank_Details,Listed_Debentures_ST,Agriculture_Income
from .models import Form16,Income,Other_Income_Common,Other_Income_Rare,Exempt_Income,VI_Deductions
from .models import Shares_LT,Shares_ST,Equity_LT,Equity_ST,Debt_MF_LT,Debt_MF_ST,Listed_Debentures_LT
from .models import Other_Exempt_Income,House_Property_Details,Property_Owner_Details
from .models import salatclient,tds_tcs,tax_paid,refunds, Business_Partner,Business, Mail_Send_Log
from .models import SalatTaxRole,SalatTaxUser,ResetPassword,Client_fin_info1,Client_fin_info
from .models import State_Details,client_info,sign_up_otp,Donation_Details,computation,Bank_List,State_Code
from .models import temp_ITR_value,Investment_Details,Modules,Invoice_Details,Module_Subscription
from .models import accommodation_details,child_info,details_80c,home_loan,details_80d,allowances,ERI_user, Partner_Deposit_Details, Partner_Deposit_Balance
from .models import Variables_80d,Partner_Settlement,Revenue_Sharing,RFPaymentDetails,UserReferralCode,Offer,tax_variables,RFChargesDetails,salat_registration,user_tracking,Mail_Trigger,SalaryBreakUp,PerquisiteBreakUp,ProfitinLieuBreakUp,TenantDetails,CGTransactions,CGCalculation,mfSchemeAssetClass,currentCostInflationIndex,HighestPrice31012018,CGSummary
# from .models import salat_registration,Chapter,SubChapter,SubSection
from import_export.admin import ImportExportModelAdmin

class SalatTaxRoleAdmin(admin.ModelAdmin):
	list_display = ('rolename','permission','created','updated')
	
class SalatTaxUserAdmin(admin.ModelAdmin):
	list_display = ('id','user','phone','email','firstname','lastname','user_id','role','referral_code','referred_by_code','created','updated')

	row_id_fields=('role',)
	
	def role(self, obj):
		return obj.role.id

class ResetPasswordAdmin(admin.ModelAdmin):
	list_display = ('salattaxuser','email','username','url','status','timestamp')

class demoAdmin(admin.ModelAdmin):
	list_display = ('name',)

class Personal_DetailsAdmin(ImportExportModelAdmin):
	list_display = ('P_Id','business_partner','pan','pan_type','dob','pan_image','aadhar_no',
		'aadhar_image','name','email','mobile','father_name','gender','dependent_parent_age',
		'no_of_child','state_of_employment','disability','financial_goal','created_time','updated_time',
		'updated_by')
	search_fields = ('P_Id','business_partner_id__id')
	# list_editable = ( 'P_Id', )
	row_id_fields=('business_partner_id',)
	
	def business_partner(self, obj):
		return obj.business_partner_id


class Personal_DetailsAdmin_Import(resources.ModelResource):
	class Meta:
		model = Personal_Details
		fields = ('P_Id','business_partner','pan','pan_type','dob','pan_image','aadhar_no',
		'aadhar_image','name','email','mobile','father_name','gender','dependent_parent_age',
		'no_of_child','state_of_employment','disability','financial_goal','created_time','updated_time',
		'updated_by')

class Address_DetailsAdmin(admin.ModelAdmin):
	list_display = ('p_id','flat_door_block_no','name_of_premises_building','road_street_postoffice',
		'area_locality','pincode','town_city','state','country','current_place',
		'created_time','updated_time')

class Return_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_id','P_ID','business_partner','FY','AY','return_filing_category','return_filing_type','xml_generated','xml_uploaded','return_e_verified','created_time','updated_time')
	search_fields = ('R_id','P_Id','business_partner_id__id')
	row_id_fields=('P_id','business_partner_id',)
	
	def P_ID(self, obj):
		if obj.P_id!=None:
			return obj.P_id.pan
		else:
			return ''
	def business_partner(self, obj):
		if obj.business_partner_id!=None:
			return obj.business_partner_id.id
		else:
			return ''

class Original_return_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_id','FY','Receipt_no_original_return','Date_of_original_return_filing',
		'notice_no','notice_date','created_time','updated_time')

class Employer_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_ID','FY','F16_no','Name_of_employer','TAN_of_employer','PAN_of_employer',
		'Address_of_employer','Employer_city','Employer_state','Employer_pin','Employer_category',
		'residential_status','start_date','end_date','monthly_basic_salary','monthly_hra',
		'created_time','updated_time')

	row_id_fields=('P_id')

	def P_ID(self, obj):
		return obj.P_id.id

class Bank_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_id','Bank_id','Default_bank','IFSC_code','Account_no','Account_type',
		'Bank_proof','created_time','updated_time')

class Form16Admin(admin.ModelAdmin):
	list_display = ('Salary_as_per_provision','Value_of_Perquisites','Profits_in_lieu_of_Salary',
		'Total_Gross_Salary','Gross_Salary_netof_Allowance','NSC','HRA_Allowed','Medical_Allowance',
		'Conveyance_Allowance_Allowed','LTA','LSE','HRA_Claimed','Conveyance_Allowance_Claimed',
		'Income_House_Property','Home_Loan_Interest','Income_Capital_Gains','Income_Other_Sources',
		'Gratuity','ELSS','EPF','PPF','VPF','LIC','NSC_Interest','Superannuation',
		'Home_Loan_Principal','FD','ULIP','Child_Edu_Fee','Sukanya_Samriddhi_Scheme','Stamp_Duty',
		'Form_80CCC','NPS_Employee','NPS_Employer','Form_80CCD','Form_80CCF','Form_80D_Self_Family',
		'Form_80D_Parents','Medical_Insurance','Form_80E','Form_80G','Form_80TTA',
		'Entertainment_Allowance','Profession_Tax','VIA_Deductions','Basic_Salary',
		'Dearness_Allowance','Personal_Allowance','Bonus','Performance_Pay','Special_Allowance',
		'Food_Allowance','Extra_Salary','RGESS','Exemption_Less_Allowance','Total_Less_Allowance',
		'Attire','Professional_Pursuit','Telephone_allowance','Other_Allowance',
		'Other_Any_Other_Income','cid','emp_type','Email','form16_path','password','P_D','S_C','C',
		'R','self_house','HRA_given','basic','DA','rent','HRA_given_value','stay','home_loan',
		'interest_paid','rent_earn','disability','disability_type','time_stamp','salat_tax',
		'tax_leakage','flag','created_time','updated_time')

class IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','company_name','Salary','Allowances_not_exempt','Value_of_perquisites',
		'Profits_in_lieu_of_salary','Entertainment_Allowance','LTA','Tax_paid_on_non_monetary_perquisite',
		'Form16_HRA','Other_allowances','Profession_Tax','created_time','updated_time')

class Other_Income_CommonAdmin(admin.ModelAdmin):
	list_display = ('R_Id','FY','Interest_Deposits','Interest_Savings','Commission','Other_Income',
		'Other_Interest','FD','post_office_interest','created_time','updated_time')

class Other_Income_RareAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Taxable_Dividend','Rental_Income_other_than_house_property','Winnings',
		'Amount_borrowed_or_repaid_on_hundi','Dividend_over_10_lacs','Investment_income_NRI',
		'created_time','updated_time')

class Exempt_IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Tax_Free_Interest','LTCG_Equity','Tax_Free_Dividend',
		'Agriculture_Income_Id','Other_exempt_income_Id','created_time','updated_time')

class VI_DeductionsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','VI_80_C','VI_80_CCC','VI_80_CCD_1','VI_80_CCD_1B','VI_80_CCD_2',
		'VI_80_CCG','VI_80_D','VI_80_DD','VI_80_DDB','VI_80_E','VI_80_EE','VI_80_G','VI_80_GG',
		'VI_80_GGA','VI_80_GGC','VI_80_QQB','VI_80_RRB','VI_80_TTA','VI_80_U','VI_80_TTB','created_time',
		'updated_time')

class Agriculture_IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Agriculture_Income_Id','Gross_Agriculture_Receipts',
		'Expenditure_Incurred_on_Agriculture','Unabsorbed_agriculture_loss','created_time','updated_time')

class Other_Exempt_IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','P_Id','FY','Other_Exempt_Income_Id','Other_Exempt_Income_Nature',
		'Other_Exempt_Income_Amount')

class Shares_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure','totalFMV')

class Shares_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Equity_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure','totalFMV')

class Equity_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Debt_MF_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Indexed_Cost_of_Acquisition','Expenditure')

class Debt_MF_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')
	
class Listed_Debentures_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Listed_Debentures_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class House_Property_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Property_Id','Name_of_the_Premises_Building_Village','Road_Street_Post_Office',
		'Area_Locality','Town_City','State','Country','PIN_Code','Your_percentage_of_share_in_Property',
		'Type_of_Hosue_Property','Rent_Received','Rent_Cannot_be_Realised','Property_Tax','Name_of_Tenant',
		'PAN_of_Tenant','Interest_on_Home_loan')

class Property_Owner_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Property_Id','co_owner_no','Name_of_Co_Owner','PAN_of_Co_owner','Percentage_Share_in_Property')

class salatclientAdmin(ImportExportModelAdmin):
	list_display = ('PAN','Is_salat_client','Is_ERI_reg','ERI_id','Reg_date')


class salatclientAdmin_Import(resources.ModelResource):
	class Meta:
		model = Personal_Details
		fields = ('PAN','Is_salat_client','Is_ERI_reg','ERI_id','Reg_date')


class tds_tcsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','part','Deductor_Collector_Name','TAN_PAN','section','no_of_transaction',
		'amount_paid','tax_deducted','tds_tcs_deposited','year')

class tax_paidAdmin(admin.ModelAdmin):
	list_display = ('R_Id','major_head','minor_head','total_tax','BSR_code','deposit_date','challan_serial_no')

class refundsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Assessment_Year','mode','amount_of_refund','interest','payment_date')
	
class Client_fin_info1Admin(admin.ModelAdmin):
	list_display = ('company_name','company_address','employee_name','employee_address','employee_pan','company_pan','company_tan','ay',
		'period_from','period_to','amount_paid_credited','amount_of_tax_deducted','salary_as_per_provision',
		'value_of_perquisites','profits_in_lieu_of_salary','total_gross_salary','gross_salary_netof_allowance',
		'nsc','hra_allowed','medical_allowance','conveyance_allowance_allowed','lta','lse','hra_claimed',
		'conveyance_allowance_claimed','income_house_property','home_loan_interest','income_capital_gains',
		'income_other_sources','gratuity','elss','epf','ppf','vpf','lic','nsc_interest','superannuation',
		'home_loan_principal','fd','ulip','child_edu_fee','sukanya_samriddhi_scheme','stamp_duty',
		'number_80ccc','nps_employee','nps_employer','number_80ccd','number_80ccf','number_80d_self_family',
		'number_80d_parents','medical_insurance','number_80e','number_80g','number_80tta',
		'entertainment_allowance','profession_tax','via_deductions','basic_salary','dearness_allowance',
		'personal_allowance','bonus','performance_pay','special_allowance','food_allowance','extra_salary',
		'rgess','exemption_less_allowance','total_less_allowance','attire','professional_pursuit',
		'telephone_allowance','other_allowance','other_any_other_income','cid','emp_type','email',
		'form16_path','password','p_d','s_c','c','r','self_house','hra_given','basic','da','rent',
		'hra_given_value','stay','home_loan','interest_paid','rent_earn','disability','disability_type',
		'time_stamp','salat_tax','tax_leakage','flag','client_id')

class Client_fin_infoAdmin(admin.ModelAdmin):
	list_display = ('company_name','company_address','employee_name','employee_address','employee_pan','company_pan','company_tan',
		'ay','period_from','period_to','amount_paid_credited','amount_of_tax_deducted','salary_as_per_provision',
		'value_of_perquisites','profits_in_lieu_of_salary','total_gross_salary','gross_salary_netof_allowance',
		'nsc','hra_allowed','medical_allowance','conveyance_allowance_allowed','lta','lse','hra_claimed',
		'conveyance_allowance_claimed','income_house_property','home_loan_interest','income_capital_gains',
		'income_other_sources','gratuity','elss','epf','ppf','vpf','lic','nsc_interest','superannuation',
		'home_loan_principal','fd','ulip','child_edu_fee','sukanya_samriddhi_scheme','stamp_duty','number_80ccc',
		'nps_employee','nps_employer','number_80ccd','number_80ccf','number_80d_self_family','number_80d_parents',
		'medical_insurance','number_80e','number_80g','number_80tta','entertainment_allowance','profession_tax',
		'via_deductions','basic_salary','dearness_allowance','personal_allowance','bonus','performance_pay',
		'special_allowance','food_allowance','extra_salary','rgess','exemption_less_allowance',
		'total_less_allowance','attire','professional_pursuit','telephone_allowance','other_allowance',
		'other_any_other_income','cid','emp_type','email','form16_path','password','p_d','s_c','c','r',
		'self_house','hra_given','basic','da','rent','hra_given_value','stay','home_loan','interest_paid',
		'rent_earn','disability','disability_type','time_stamp','salat_tax','tax_leakage','flag')

class client_infoAdmin(admin.ModelAdmin):
	list_display = ('username','email','password','created_time')

class sign_up_otpAdmin(admin.ModelAdmin):
	list_display = ('username','mailid','otp','verified','created_time')

class Donation_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','donation_id','donee_name','address','city_town','state_code','pin_code',
		'donee_pan','amt_of_donation','donation_percent')

class computationAdmin(admin.ModelAdmin):
	list_display = ('R_Id','salary_income','property_income','capital_gains','gross_total_income',
		'total_deduction','taxable_income','IT_normal_rates','IT_special_rates','rebate','surcharge',
		'add_education_cess','total_tax','interest_234A','interest_234B','interest_234C','interest_234F','interest_on_tax','taxpayable_refund')

class temp_ITR_valueAdmin(ImportExportModelAdmin):
	list_display = ('PAN','year','ITR','income_from_salary','no_of_hp','income_from_hp',
		'interest_payable_borrowed_capital','capital_gain','schedule_si_income','income_from_other_src',
		'business_or_profession','interest_gross','gross_total_income','deduction','c_80','d_80','tta_80',
		'ccd_1b_80','ccg_80','e_80','ee_80','g_80','gg_80','ccd_2_80','total_income','schedule_si_tax',
		'total_tax_interest','tax_paid','adv_tax_self_ass_tax','total_tcs','total_tds_ITR','total_tds_26AS')
	# list_editable = ( 'P_Id', )

class temp_ITR_valueAdmin_Import(resources.ModelResource):
	class Meta:
		model = temp_ITR_value
		fields = ('PAN','year','ITR','income_from_salary','no_of_hp','income_from_hp',
		'interest_payable_borrowed_capital','capital_gain','schedule_si_income','income_from_other_src',
		'business_or_profession','interest_gross','gross_total_income','deduction','c_80','d_80','tta_80',
		'ccd_1b_80','ccg_80','e_80','ee_80','g_80','gg_80','ccd_2_80','total_income','schedule_si_tax',
		'total_tax_interest','tax_paid','adv_tax_self_ass_tax','total_tcs','total_tds_ITR','total_tds_26AS')

class temp_ITR_valueAdmin_Import(resources.ModelResource):
	class Meta:
		model = temp_ITR_value
		fields = ('PAN','year','ITR','income_from_salary','no_of_hp','income_from_hp',
		'interest_payable_borrowed_capital','capital_gain','schedule_si_income','income_from_other_src',
		'business_or_profession','interest_gross','gross_total_income','deduction','c_80','d_80','tta_80',
		'ccd_1b_80','ccg_80','e_80','ee_80','g_80','gg_80','ccd_2_80','total_income','schedule_si_tax',
		'total_tax_interest','tax_paid','adv_tax_self_ass_tax','total_tcs','total_tds_ITR','total_tds_26AS')

class Investment_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_Id','year_of_first_share_sold')

class ModulesAdmin(ImportExportModelAdmin):
	list_display = ('module_Id','module_name')

class Invoice_DetailsAdmin(admin.ModelAdmin):
	list_display = ('id','client_id','invoice_no','invoice_date','invoice_amount','CGST','SGST','IGST','discount',
		'offer_code','transaction_id','payment_status','mode_of_payment')

class Module_SubscriptionAdmin(admin.ModelAdmin):
	list_display = ('client_id','business_partner','module_Id','invoice_no','year','module_cost','module_detail1')
	row_id_fields=('business_partner_id',)
	
	def business_partner(self, obj):
		return obj.business_partner_id.id

class child_infoAdmin(admin.ModelAdmin):
	list_display = ('p_id','child_no','child_type','Child_dob','Child_age')

class accommodation_detailsAdmin(admin.ModelAdmin):
	list_display = ('p_id','FY','type_of_accommodation','current_place','annual_rent_paid')

class details_80cAdmin(admin.ModelAdmin):
	list_display = ('R_id','elss','epf','bank_fd','school_fee','ppf','life_insurance','ulip','nsc',
		'sukanya_samriddhi_scheme','post_office_deposit','repayment_principal_home_loan','stamp_duty','home_loan')

class home_loanAdmin(admin.ModelAdmin):
	list_display = ('R_id','principal_amount','interest_amount','rate')

class details_80dAdmin(admin.ModelAdmin):
	list_display = ('R_id','mi_self','mi_parents','mc_self','mc_parents')

class allowancesAdmin(admin.ModelAdmin):
	list_display = ('R_id','company_name','entertainment_allowance','lta','hra','other_allowance','travel_expense_incurred','gratuity','earned_leave_encashment')

class ERI_userAdmin(ImportExportModelAdmin):
	list_display = ('eri_name','eri_pan','eri_dob','password','user_id','status','user_pan','date_time')

class ERI_userAdmin_Import(resources.ModelResource):
	class Meta:
		model = ERI_user
		fields = ('eri_name','eri_pan','eri_dob','password','user_id','status','user_pan','date_time')

class salat_registrationAdmin(ImportExportModelAdmin):
	list_display = ('pan','date_time','login','is_salat_client','otp_trigger','otp_submitted','itr_count',
		'downloaded_itr_count','processed_itr_count','downloaded_26as_count','processed_26as_count')

class salat_registrationAdmin_Import(resources.ModelResource):
	class Meta:
		model = salat_registration
		fields = ('pan','date_time','login','is_salat_client','otp_trigger','otp_submitted','itr_count',
		'downloaded_itr_count','processed_itr_count','downloaded_26as_count','processed_26as_count')


class Mail_Send_LogAdmin(admin.ModelAdmin):
	list_display = ('id','business_partner','sender_mail','receiver_mail','status','mail_type','created')
	search_fields = ('business_partner_id__id','p_id__id','sender_mail','receiver_mail','status','mail_type')
	row_id_fields=('business_partner_id','p_id')
	
	def business_partner(self, obj):
		return obj.business_partner_id.name
	def clent_name(self, obj):
		return obj.p_id.name

# class ChapterAdmin(admin.ModelAdmin):
#   list_display = ('title','slug','date_completed','completed')

# class SubChapterAdmin(admin.ModelAdmin):
#   list_display = ('chapter','title','slug','completed')

# class SubSectionAdmin(admin.ModelAdmin):
#   list_display = ('sub_chapter','title','slug','completed')



admin.site.register(SalatTaxRole, SalatTaxRoleAdmin)
admin.site.register(SalatTaxUser, SalatTaxUserAdmin)
admin.site.register(ResetPassword, ResetPasswordAdmin)
admin.site.register(demo, demoAdmin)
admin.site.register(Personal_Details, Personal_DetailsAdmin)
admin.site.register(Address_Details, Address_DetailsAdmin)
admin.site.register(Return_Details, Return_DetailsAdmin)
admin.site.register(Original_return_Details, Original_return_DetailsAdmin)
admin.site.register(Employer_Details, Employer_DetailsAdmin)
admin.site.register(Bank_Details, Bank_DetailsAdmin)
admin.site.register(Form16, Form16Admin)
admin.site.register(Income, IncomeAdmin)
admin.site.register(Other_Income_Common, Other_Income_CommonAdmin)
admin.site.register(Other_Income_Rare, Other_Income_RareAdmin)
admin.site.register(Exempt_Income, Exempt_IncomeAdmin)
admin.site.register(VI_Deductions, VI_DeductionsAdmin)
admin.site.register(Agriculture_Income, Agriculture_IncomeAdmin)
admin.site.register(Other_Exempt_Income, Other_Exempt_IncomeAdmin)
admin.site.register(Shares_LT, Shares_LTAdmin)
admin.site.register(Shares_ST, Shares_STAdmin)
admin.site.register(Equity_LT, Equity_LTAdmin)
admin.site.register(Equity_ST, Equity_STAdmin)
admin.site.register(Debt_MF_LT, Debt_MF_LTAdmin)
admin.site.register(Debt_MF_ST, Debt_MF_STAdmin)
admin.site.register(Listed_Debentures_LT, Listed_Debentures_LTAdmin)
admin.site.register(Listed_Debentures_ST, Listed_Debentures_STAdmin)
admin.site.register(House_Property_Details, House_Property_DetailsAdmin)
admin.site.register(Property_Owner_Details, Property_Owner_DetailsAdmin)
admin.site.register(salatclient, salatclientAdmin)
admin.site.register(tds_tcs, tds_tcsAdmin)
admin.site.register(tax_paid, tax_paidAdmin)
admin.site.register(refunds, refundsAdmin)
admin.site.register(Client_fin_info1, Client_fin_info1Admin)
admin.site.register(Client_fin_info, Client_fin_infoAdmin)
admin.site.register(client_info, client_infoAdmin)
admin.site.register(sign_up_otp, sign_up_otpAdmin)
admin.site.register(Donation_Details, Donation_DetailsAdmin)
admin.site.register(computation, computationAdmin)
admin.site.register(temp_ITR_value, temp_ITR_valueAdmin)
admin.site.register(Investment_Details, Investment_DetailsAdmin)
admin.site.register(Modules, ModulesAdmin)
admin.site.register(Invoice_Details, Invoice_DetailsAdmin)
admin.site.register(Module_Subscription, Module_SubscriptionAdmin)
admin.site.register(accommodation_details, accommodation_detailsAdmin)
admin.site.register(child_info, child_infoAdmin)
admin.site.register(details_80c, details_80cAdmin)
admin.site.register(home_loan, home_loanAdmin)
admin.site.register(details_80d, details_80dAdmin)
admin.site.register(allowances, allowancesAdmin)
admin.site.register(ERI_user, ERI_userAdmin)
admin.site.register(salat_registration, salat_registrationAdmin)
admin.site.register(Mail_Send_Log, Mail_Send_LogAdmin)
# admin.site.register(Chapter, ChapterAdmin)
# admin.site.register(SubChapter, SubChapterAdmin)
# admin.site.register(SubSection, SubSectionAdmin)


class State_Details_Import(resources.ModelResource):
	class Meta:
		model = State_Details
		fields = ('id','nse_state', 'bse_state','cvl_state', 'ckyc_state', 'state')

class State_DetailsAdmin(ImportExportModelAdmin):
	list_display = ('id','nse_state', 'bse_state', 'cvl_state', 'ckyc_state', 'state')
	search_fields = ('nse_state', 'bse_state', 'cvl_state', 'ckyc_state', 'state')
	resource_class = State_Details_Import

class State_Code_Import(resources.ModelResource):
	class Meta:
		model = State_Code
		fields = ('id','code', 'state')

class State_CodeAdmin(ImportExportModelAdmin):
	list_display = ('id','code', 'state')
	search_fields = ('code', 'state')
	resource_class = State_Code_Import

class Bank_List_Import(resources.ModelResource):
	class Meta:
		model = Bank_List
		fields = ('id','nse_bank_code', 'bank_name')

class Bank_ListAdmin(ImportExportModelAdmin):
	list_display = ('id','nse_bank_code', 'bank_name')
	search_fields = ('nse_bank_code', 'bank_name')
	resource_class = Bank_List_Import

class SalaryBreakUp_Import(resources.ModelResource):
	class Meta:
		model = SalaryBreakUp
		fields = ('id','R_Id','company_name','basic_salary','dearness_allowance','conveyance_allowance','annuity_or_pension','commuted_pension','gratuity','fees_commission','advance_of_salary','Other_allowances','leave_encashment')
  
class SalaryBreakUpAdmin(ImportExportModelAdmin):
	list_display = ('id','R_Id','company_name','basic_salary','dearness_allowance','conveyance_allowance','annuity_or_pension','commuted_pension','gratuity','fees_commission','advance_of_salary','Other_allowances','leave_encashment')
	search_fields = ('id', 'R_Id','company_name','basic_salary','dearness_allowance','conveyance_allowance','annuity_or_pension','commuted_pension','gratuity','fees_commission','advance_of_salary','Other_allowances','leave_encashment')
	resource_class = SalaryBreakUp_Import

class PerquisiteBreakUp_Import(resources.ModelResource):
	class Meta:
		model = PerquisiteBreakUp
		fields = ('id','R_Id','company_name','accommodation','cars_or_other_automotive','domestic_help','utilitiy_bills','concessional_loans','holiday_expenses','concessional_travel','free_meals','free_education','gifts_vouchers','credit_card_expenses','club_expenses','other_benefits')
   
class PerquisiteBreakUpAdmin(ImportExportModelAdmin):
	list_display = ('id','R_Id','company_name','accommodation','cars_or_other_automotive','domestic_help','utilitiy_bills','concessional_loans','holiday_expenses','concessional_travel','free_meals','free_education','gifts_vouchers','credit_card_expenses','club_expenses','other_benefits')
	search_fields = ('id','R_Id','company_name','accommodation','cars_or_other_automotive','domestic_help','utilitiy_bills','concessional_loans','holiday_expenses','concessional_travel','free_meals','free_education','gifts_vouchers','credit_card_expenses','club_expenses','other_benefits')
	resource_class = PerquisiteBreakUp_Import


class ProfitinLieuBreakUp_Import(resources.ModelResource):
	class Meta:
		model = ProfitinLieuBreakUp
		fields = ('id','R_Id','company_name','termination_compensation','keyman_insurance','other_profit_in_lieu_salary','other')
 

class ProfitinLieuBreakUpAdmin(ImportExportModelAdmin):
	list_display = ('id','R_Id','company_name','termination_compensation','keyman_insurance','other_profit_in_lieu_salary','other')
	search_fields =('id','R_Id','company_name','termination_compensation','keyman_insurance','other_profit_in_lieu_salary','other')
	resource_class = ProfitinLieuBreakUp_Import

class TenantDetails_Import(resources.ModelResource):
	class Meta:
		model = TenantDetails
		fields = ('id','R_Id','Property_Id','tenant_no','tenant_name','tenant_pan')

class TenantDetailsAdmin(ImportExportModelAdmin):
	list_display = ('id','R_Id','Property_Id','tenant_no','tenant_name','tenant_pan')
	search_fields =('id','R_Id','Property_Id','tenant_no','tenant_name','tenant_pan')
	resource_class = TenantDetails_Import


admin.site.register(State_Details, State_DetailsAdmin)
admin.site.register(Bank_List, Bank_ListAdmin)
admin.site.register(State_Code, State_CodeAdmin)
admin.site.register(SalaryBreakUp, SalaryBreakUpAdmin)
admin.site.register(PerquisiteBreakUp, PerquisiteBreakUpAdmin)
admin.site.register(ProfitinLieuBreakUp, ProfitinLieuBreakUpAdmin)
admin.site.register(TenantDetails, TenantDetailsAdmin)

# from .models import Visitor, Pageview
# from .settings import TRACK_PAGEVIEWS
# from datetime import timedelta

# class VisitorAdmin(admin.ModelAdmin):
#     date_hierarchy = 'start_time'

#     list_display = ('session_key', 'user', 'start_time', 'session_over',
#         'pretty_time_on_site', 'ip_address', 'user_agent','expiry_time','end_time')
#     list_filter = ('user', 'ip_address')

#     def session_over(self, obj):
#         return obj.session_ended() or obj.session_expired()
#     session_over.boolean = True

#     def pretty_time_on_site(self, obj):
#         if obj.time_on_site is not None:
#             return timedelta(seconds=obj.time_on_site)
#     pretty_time_on_site.short_description = 'Time on site'


# admin.site.register(Visitor, VisitorAdmin)


# class PageviewAdmin(admin.ModelAdmin):
#     date_hierarchy = 'view_time'

#     list_display = ('visitor','url','referer','query_string','method', 'view_time')


# if TRACK_PAGEVIEWS:
#     admin.site.register(Pageview, PageviewAdmin)


class user_trackingAdmin(ImportExportModelAdmin):
	list_display = ('session_key','session_count','client_id','user_id','event','event_name','time_on_site',
		'device_platform','device_type','browser','created_time')

	def time_on_site(self, obj):
		if obj.time_on_site is not None:
			return timedelta(seconds=obj.time_on_site)
	time_on_site.short_description = 'Time on site'

	
class user_trackingAdmin_Import(resources.ModelResource):
	class Meta:
		model = user_tracking
		fields =('session_key','session_count','client_id','user_id','event','event_name','time_on_site',
		'device_platform','device_type','browser','created_time')


class Mail_TriggerAdmin(ImportExportModelAdmin):
	list_display = ('P_Id','mail_id','mail_content','sent','created_time')

class Mail_TriggerAdmin_Import(resources.ModelResource):
	class Meta:
		model = Mail_Trigger
		fields = ('P_Id','mail_id','mail_content')

class BusinessAdmin(admin.ModelAdmin):
	list_display = ('id','main_business_name')
	search_fields = ('id','main_business_name', )

class Business_PartnerAdmin(admin.ModelAdmin):
	list_display = ('id','User','mainBusiness','partner_business_name','partner_id','status','name','pan','email','mobile','primary_business','address','city','state','pincode','ifsc','account_no','account_type','bank_holder_name','bank_proof','created_date')
	search_fields = ('id','user_id__id','main_business__id','partner_business_name','partner_id','status','name','pan','email','mobile','primary_business','address','city','state','pincode','ifsc','account_no','account_type','bank_holder_name','bank_proof','created_date')
	row_id_fields=('user_id','main_business')
	
	def User(self, obj):
		return obj.user_id.user
	def mainBusiness(self, obj):
		return obj.main_business.main_business_name  


class Partner_Deposit_DetailsAdmin(admin.ModelAdmin):
	list_display = ('id','business_partner','deposite_amount','deposit_date','transaction_id','show_in_balance','payment_status','mode_of_payment')
	search_fields = ('id','business_partner_id__id','deposite_amount','deposit_date','transaction_id','show_in_balance','payment_status','mode_of_payment')
	row_id_fields=('business_partner_id',)

	def business_partner(self, obj):
		return obj.business_partner_id.id  

class Partner_Deposit_BalanceAdmin(admin.ModelAdmin):
	list_display = ('id','business_partner','deposit_balance')
	search_fields = ('id','business_partner_id__id','deposit_balance')
	row_id_fields=('business_partner_id',)

	def business_partner(self, obj):
		return obj.business_partner_id.id  

class Partner_SettlementAdmin(admin.ModelAdmin):
	list_display = ('id','module_subscription_id','partner_share','settlement_status')
	search_fields = ('id','module_subscription_id__id','partner_share','settlement_status')
	row_id_fields=('module_subscription_id',)

	def module_subscription_id(self, obj):
		return obj.module_subscription_id.id  

class Revenue_SharingAdmin(admin.ModelAdmin):
	list_display = ('id','business_partner','module','partner_share')
	search_fields = ('id','business_partner_id__id','module_id','partner_share')
	row_id_fields=('business_partner','module')

	def business_partner(self, obj):
		return obj.business_partner_id.partner_business_name 

	def module(self, obj):
		return obj.module_id.module_name

class Partner_Deposit_BalanceAdmin(admin.ModelAdmin):
	list_display = ('id','business_partner','deposit_balance')
	search_fields = ('id','business_partner_id__id','deposit_balance')
	row_id_fields=('business_partner_id',)

	def business_partner(self, obj):
		return obj.business_partner_id.id  

class RFChargesDetailsAdmin_Import(resources.ModelResource):
	class Meta:
		model = RFChargesDetails
		fields = ('id','return_complexity','charges')

class RFChargesDetailsAdmin(ImportExportModelAdmin):
	list_display = ('id','return_complexity','charges')
	resource_class = RFChargesDetailsAdmin_Import

class tax_variablesAdmin(admin.ModelAdmin):
	list_display = ('R_Id','salary_income','house_property','business_or_profession','capital_gain','foreign_assets','taxable_income_exceed_50l')

class OfferAdmin_Import(resources.ModelResource):
	class Meta:
		model = Offer
		fields = ('offer_code','offer_amount','expiry_date')

class OfferAdmin(ImportExportModelAdmin):
	list_display = ('offer_code','offer_amount','expiry_date')
	resource_class = OfferAdmin_Import

class RFPaymentDetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','business_partner_id','discount_code','basic_charge','form16_charge','HP_charge','CG_shares_charge','CG_MF_charge','expert_review_charge','phone_support_charge','express_filing_charge')

class UserReferralCodeAdmin(admin.ModelAdmin):
	list_display=('salatax_user_id','referral_code')

class Variables_80dAdmin(admin.ModelAdmin):
	list_display = ('R_Id','self_health_checkup','parent_health_checkup','self_insurance_premium','parent_insurance_premium','self_medical_expenditure','parent_medical_expenditure')

class CGTransactionsAdmin(admin.ModelAdmin):
	list_display = ('businessPartnerId','intermediaryId','clientId','assetClass','scripSchemeCode','transactionType','transactionDate','qtyUnits','priceNAV','amount','transactionCost','folioNo')

class CGCalculationAdmin(admin.ModelAdmin):
	list_display = ('businessPartnerId','intermediaryId','clientId','financialYear','cgTransactionId','assetClass','scripSchemeCode','sellDate','sellQty','sellPriceNAV','sellAmount','purchaseDate','purchasePriceNAV','FMV','costofAcquisition','CGType','capitalGain')
class mfSchemeAssetClassAdmin(admin.ModelAdmin):
	list_display = ('amc','schemeCode','productCode','schemeName','schemeType','taxType')
class currentCostInflationIndexAdmin(admin.ModelAdmin):
	list_display = ('financialYear','costInflationIndex')
class HighestPrice31012018Admin(admin.ModelAdmin):
	list_display = ('productCode','highestPrice')
class CGSummaryAdmin(admin.ModelAdmin):
	list_display = ('businessPartnerId','clientId','financialYear','assetClass','CGType','totalSellAmount','totalCostofAcquisition','indexedCostofAcquisition','expenditure')

admin.site.register(user_tracking, user_trackingAdmin)
admin.site.register(Mail_Trigger, Mail_TriggerAdmin)
admin.site.register(RFChargesDetails, RFChargesDetailsAdmin)
admin.site.register(tax_variables, tax_variablesAdmin)
admin.site.register(Offer, OfferAdmin)
admin.site.register(RFPaymentDetails, RFPaymentDetailsAdmin)
admin.site.register(UserReferralCode, UserReferralCodeAdmin)
admin.site.register(Business, BusinessAdmin)
admin.site.register(Business_Partner, Business_PartnerAdmin)
admin.site.register(Partner_Deposit_Details, Partner_Deposit_DetailsAdmin)
admin.site.register(Partner_Deposit_Balance, Partner_Deposit_BalanceAdmin)
admin.site.register(Partner_Settlement, Partner_SettlementAdmin)
admin.site.register(Revenue_Sharing, Revenue_SharingAdmin)
admin.site.register(Variables_80d, Variables_80dAdmin)
admin.site.register(CGTransactions, CGTransactionsAdmin)
admin.site.register(CGCalculation, CGCalculationAdmin)
admin.site.register(mfSchemeAssetClass, mfSchemeAssetClassAdmin)
admin.site.register(currentCostInflationIndex, currentCostInflationIndexAdmin)
admin.site.register(HighestPrice31012018, HighestPrice31012018Admin)
admin.site.register(CGSummary, CGSummaryAdmin)