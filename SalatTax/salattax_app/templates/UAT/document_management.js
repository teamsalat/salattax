
app.controller('document_management_controller',function($rootScope,$scope,$http,$parse,CONFIG,$timeout,$sce) {

var FILE_PATH=JSON.stringify(CONFIG['S3_FILE_PATH']).replace(/\"/g, "");
var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
var S3_BUCKET=JSON.stringify(CONFIG['S3_BUCKET']).replace(/\"/g, "");
	// alert('document_management_controller');
$(window).load(function(){

  $scope.user.module_expiry='Y'
  $scope.freeze_module($scope.user.module_expiry);

  $('.loader').hide();
  $('#small_image').hide();
	$scope.kyc_forms=$scope.array_to_json_conversion('kyc_forms','KYC')
	$scope.mfu_forms=$scope.array_to_json_conversion('mfu_forms','MFU')
	$scope.nse_forms=$scope.array_to_json_conversion('nse_forms','NSE')
	$scope.bse_forms=$scope.array_to_json_conversion('bse_forms','BSE')
  $scope.nps_forms=$scope.array_to_json_conversion('nps_forms','NPS')
  $scope.$apply();
    
})

$scope.freeze_module=function(flag){

  if(flag=='N'){

      swal({

                title: "Plan Expired",
                text: "Kindly Resubscribe.",
                type: "info",
                closeOnConfirm: false,
                html: false,
                confirmButtonText: "Resubscribe",
               
               
      }, function(isConfirm){

        if(isConfirm){
         
          window.open($scope.user.resubscription_url)
        
        }

      })

  }
}

$scope.restrict_file_upload=function(){

      swal({

                title: "Memory is Full",
                text: "Kindly Resubscribe.",
                type: "info",
                closeOnConfirm: false,
                html: false,
                showCancelButton: true,
                confirmButtonText: "Resubscribe",
               
               
      }, function(isConfirm){

        if(isConfirm){
         
          window.open($scope.user.resubscription_url)
        
        }

      })

  
}

$scope.restrict_on_search=function(){

      swal({

                title: "",
                text: "Oops! You have exceeded your Free space limit for Document Management and hence have limited searches.You can easily remove this restriction by subscribing to next higher tier in Document Management by clicking here",
                type: "warning",
                closeOnConfirm: false,
                html: false,
                showCancelButton: true,
                confirmButtonText: "Upgrade Plan",
               
               
      }, function(isConfirm){

        if(isConfirm){
         
          window.open($scope.user.resubscription_url)
        
        }

      })

  
}

$scope.search_count_zero=function(){

      swal({

                title: "Your Free Tier Plan is Expired",
                text: "Kindly Resubscribe",
                type: "info",
                closeOnConfirm: false,
                html: false,
                showCancelButton: true,
                confirmButtonText: "Resubscribe",
               
               
      }, function(isConfirm){

        if(isConfirm){
         
          window.open($scope.user.resubscription_url)
        
        }

      })
  
}

$scope.array_to_json_conversion=function(variable,folder){

	var forms=[];
    var forms_array=$scope.fetch_dynamic_variable(variable)
      for(var i=0;i<forms_array.length;i++){
      	var name=forms_array[i].replace(/\.pdf/g, "");
        forms.push({ 
              path: FILE_PATH+S3_BUCKET+'/common/blank_forms/'+folder+'/'+forms_array[i],
              file: forms_array[i],
              filetype: 'pdf',
              name:name
        });
     }

   return forms
}

$scope.fetch_dynamic_variable=function(variable) {
 
    return $scope.user[variable];
}


$scope.view_pdf=function(name,link,filetype){
  
  if(filetype==''){
    filetype=name.split('.')
    filetype=filetype[1]
  }
 
  if(filetype=='pdf' || filetype=='PDF' || filetype=='tiff' || filetype=='TIFF'){

      $('embed').attr('src',link);
      // $('.modal-title1').html(name);
      $('#modal-fullscreen').modal('show');

  }else if(filetype=='png' || filetype=='PNG'){

      $rootScope.iviewer('image_preview',link)
      $('#modal-fullscreen-png').modal('show');

  }

        
}

$scope.delete_file=function(filename) {


        swal({

                title: "Are you sure?",
                text: "It will permanently deleted !",
                type: "warning",
                closeOnConfirm: false,
                  html: false,
                  showCancelButton: true,
                  confirmButtonText: "Yes, delete it!",
               
               
              }, function(){

                        $.ajax({

                              url:'/delete_file/',
                              type:'POST',
                              dataType: 'json',
                              async:false,
                              data:{'filename':filename,'b_id':$scope.user.id},
                              success: function(response){

                                $scope.elastic_search($scope.user.search);
                                $scope.get_file_info();
                                $scope.$apply();
                                swal.close()
                                

                              }

                        })

      });
  

}

$scope.elastic_search_on_enter=function(e,input){

  if(e.keyCode==13)
  {
    $scope.elastic_search(input);
  }else{
    $scope.autocomplete('search',e,$scope.user.search)
  }
}


$scope.update_search_count=function(){


        $.ajax({

                url:'/update_search_count/',
                type:'POST',
                dataType: 'json',
                async:false,
                data:{'b_id':$scope.user.id},
                success: function(response){

                  $scope.user.search_count=response.count

                }

        });


}

$scope.elastic_search=function(input){

    // alert(input)
    if($scope.user.document_space=='Y' || $scope.user.search_count!='0'){

          if(input!=undefined && input!=''){


            $.ajax({

                    url:'/search/',
                    type:'POST',
                    dataType: 'json',
                    async:false,
                    data:{'input':input,'document_type':$scope.user.document_type,'b_id':$scope.user.id},
                    success: function(response){

                   
                      var forms_array=response.result
                      var forms=[];
                      var random=Math.floor((Math.random()*10000)+1)  
                      if(forms_array.length!=0)
                      {
                          for(var i=0;i<forms_array.length;i++)
                          {
                           
                              // if(forms_array[i].filename.indexOf('.pdf')==-1 && forms_array[i].filename.indexOf('.tiff')==-1){

                                forms.push({
                                      path: FILE_PATH+S3_BUCKET+'/B'+forms_array[i].business_id+'/'+forms_array[i].filename+'?'+random,
                                      name: forms_array[i].name,
                                      filename:forms_array[i].filename,
                                      document_type:forms_array[i].document_type
                                });

                              // }
                               
                          }

                          console.log(JSON.stringify(forms))

                          $scope.client_documents=forms
                          $scope.response_error=''

                          if($scope.user.document_space=='N' && $scope.user.plan=='Free')
                            $scope.update_search_count();
                          // $scope.$apply();

                      }else if(forms_array.length==0){
                        $scope.response_error="Ooops! No Data Found"
                        $scope.client_documents=[]
                      }
                      

                    }

            });

          }else{

            $('#search').focus();
          }

    }else if($scope.user.document_space=='N' && $scope.user.plan=='Free' && $scope.user.search_count=='0'){

        $scope.search_count_zero();
    }
   
}



$scope.get_file_info=function()
{
    var b_id=$scope.user.id;

          $.ajax({

                              url:'/get_file_info/',
                              type:'POST',
                              dataType: 'json',
                              async:false,
                              data:{'b_id':$scope.user.id},
                              success: function(response){

                                if(response.result=='Success'){

                                  console.log(JSON.stringify(response))
                                  $scope.count=response.count
                                  $scope.filesize=response.filesize
                                  $scope.allocated_space=response.space_allocated
                                  $scope.subscription_end_date=$scope.format_date_for_compare(response.end_date)
                                  $scope.$apply();
                                }
                             

                              }

          })


}

$scope.format_date_for_compare=function(date) {

  if(date!='' && date!=undefined && date.indexOf('-')!=-1){
    date=date.split('-')
    return date[2]+'/'+date[1]+'/'+date[0]
  }else{
    return date
  }
 
  // body...
}

$("#li_scanned_documents").on('click', function(event){

    $scope.get_file_info();

    if($scope.user.document_space=='N' && $scope.user.plan=='Free' && $scope.user.search_count=='10'){
      $scope.restrict_on_search();
    }
    
})


$scope.autocomplete=function(type,e,keyword){

  if(e.keyCode!=13){

          var b_id=$scope.user.id;

          // var Exp = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;

          // alphanumeric_keyword=Exp.test(keyword)

          // console.log('alphanumeric_keyword '+alphanumeric_keyword)

          var numbers = /^[0-9]+$/;

          var alphanumeric = /^[a-z0-9]+$/i;

          number_keyword=numbers.test(keyword)

          alphanumeric_keyword=alphanumeric.test(keyword)

          var character = /[a-z]/i;

          character_keyword=character.test(keyword)

          if(character_keyword){
            validation_length=4
          }else{
            validation_length=8
          }

          // console.log('number_keyword '+number_keyword)
          // console.log('alphanumeric_keyword '+alphanumeric_keyword)
          // console.log('validation_length '+validation_length)

          if(keyword.length>=validation_length){

              if(keyword!='' && keyword!=undefined)
              {
                   $.ajax({

                                  url:'/autocomplete/',
                                  type:'POST',
                                  dataType: 'json',
                                  async:false,
                                  data:{'b_id':b_id,'input':keyword},
                                  success: function(response){

                                    console.log(JSON.stringify(response.result));
                                    if(type=='search'){
                                      $scope.searchResult=response.result
                                 
                                    }else if(type=='new_upload'){
                                      $scope.searchResult1=response.result
                                    }
                                 

                                  }

                  })
                   
              }else{

                $scope.searchResult1={};
                $scope.searchResult={};
              }


          }
 
              

  }
 



}

$scope.split_platform_id=function(value){

  value=value.split(' ');
  return value[0]

}

 $scope.setValue = function(type,index,$event){
    
      if(type=='search'){

        $scope.client_documents={};

        if($scope.searchResult[index].platform_id){
          $scope.user.search = $scope.split_platform_id($scope.searchResult[index].platform_id);
          $scope.search_client_name=$scope.searchResult[index]

        }else{
          $scope.user.search = $scope.searchResult[index].pan;
          $scope.search_client_name=$scope.searchResult[index]

        }

        $scope.searchResult = {};

      }else if(type=='new_upload'){
        if($scope.searchResult1[index].platform_id)
          $scope.user.client = $scope.split_platform_id($scope.searchResult1[index].platform_id);
        else
          $scope.user.client = $scope.searchResult1[index].pan;

        $scope.user.client_data=$scope.searchResult1[index].name+'|'+$scope.searchResult1[index].pan+'|'+$scope.user.client

        $scope.searchResult1 = {};

        $scope.get_bank_details($scope.user.new_document_type,$scope.user.client);
      }
       
        $event.stopPropagation();
      
}


$scope.searchboxClicked = function($event){

      $scope.searchResult = {};    
      $event.stopPropagation();
  
}

$scope.focusedIndex = 0;
$scope.focusedIndex1 = 0;



 $scope.handleKeyDown = function($event) 
 {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $event.preventDefault();
                if ($scope.focusedIndex !== $scope.searchResult.length - 1) {
                    $scope.focusedIndex++;
                }
            }
            else if (keyCode === 38) {
                // Up
                $event.preventDefault();
                if ($scope.focusedIndex !== 0) {
                    $scope.focusedIndex--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex >= 0) {
                // Enter
                $scope.setValue('search',$scope.focusedIndex,$event);
            }
};

 $scope.handleKeyDown1 = function($event) 
 {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $event.preventDefault();
                if ($scope.focusedIndex1 !== $scope.searchResult1.length - 1) {
                    $scope.focusedIndex1++;
                }
            }
            else if (keyCode === 38) {
                // Up
                $event.preventDefault();
                if ($scope.focusedIndex1 !== 0) {
                    $scope.focusedIndex1--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex1 >= 0) {
                // Enter
                $scope.setValue('new_upload',$scope.focusedIndex1,$event);
            }
};


$scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj)
{

  $('.loader').hide();
  if(fileObj.filesize>5000000){
    $scope.upload_file_error='Files must not exceed 5 MB'
    $scope.upload_file='';
    $scope.upload_file_msg='';

  }else{
    $scope.upload_file_msg="File Uploaded Successfully"
    $scope.upload_file_error=''
  }


}


$scope.loader=function(event, reader, file, fileList, fileObjs, object){

        $('.loader').show();
}


$scope.upload=function(status,document_type,file){

                $scope.extra_flag=$scope.user.bank;
    
                 var data=JSON.stringify(file);

                  $.ajax({
                                  url:'/new_file_upload/',
                                  type:'POST',
                                  async:false,
                                  dataType: 'json',
                                  data:{'data':data,'category':status,'document_type':document_type,'bid':$scope.user.id,'client': $scope.user.client_data,'keyword':$scope.find_keyword(document_type),'extra_flag':$scope.extra_flag},
                                  success: function(response){

                                      $('.loader').hide();
                                      if(response.status==true){
                                        swal('Document Uploaded Successfully')
                                        $("#modal-add-new-file").modal('hide');
                                        $scope.get_file_info();
                                        
                                      }else{
                                        swal(response.status)
                                      }
                                   
                                    ////Next Tab Show function call
                                    

                                  },error:function(response) {
                      
                                      $('.loader').hide();
                                      swal(response.status)
                                     
                                  }

                  });
}

$('#modal-add-new-file').on('shown.bs.modal', function() {

   if($scope.user.document_space=='No'){
      $('#modal-add-new-file').modal('hide');
      $scope.restrict_file_upload();
  }

})

$scope.upload_new_file=function(client_documents,business_documents,document_type,client,file){

  if($scope.user.document_space=='Y' || $scope.user.document_space=='N'){


      var status=$scope.category_validation(client_documents,business_documents);

      if(status!=false){

        if(status=='Clients')
        {

            if(file!='')
            {

              if(file.filesize<3000000)
              {
                  if(client!='')
                  {

                    if(document_type=='CHEQUE' || document_type=='STATEMENT' || document_type=='PASSBK' || document_type=='NSE_M' || document_type=='MFU_M' || document_type=='BSE_M'){
                      if($scope.user.bank!='' && $scope.user.bank!=undefined)
                      {    
                            $scope.upload(status,document_type,file);

                      }else{
                          swal('Please Select Bank Account No');
                      }
                    }else if(document_type!='CHEQUE' && document_type!='STATEMENT' && document_type!='PASSBK' || document_type!='NSE_M' || document_type!='MFU_M' || document_type!='BSE_M'){
                         $scope.upload(status,document_type,file);

                    }
                    

                  }else if(client==''){

                    swal('Please mention client name');
                  }
              }else if(file.filesize>3000000){

                swal('Oops! File too large!');
              }

            }else if(file==''){

              swal('Please Upload File');
            }

        }else if(status=='Business'){

           swal('Want to upload business document? Coming Soon...');

        }

        
        
      }else if(status==false){

          swal('Please Select Document Category');

      }

  }
  // else if($scope.user.document_space=='N' || $scope.user.document_space=='Y'){

  //     $('#modal-add-new-file').modal('hide');
  //     $scope.restrict_file_upload();
  // }

}

$scope.category_validation=function(client_documents,business_documents)
{

  if($('#'+client_documents).hasClass('btn-info')){
    return 'Clients'
  }else if($('#'+business_documents).hasClass('btn-info')) {
    return 'Business'
  }else{
    return false
  }
}

$scope.highlight = function(text, search) {
    if (!search) {
        return $sce.trustAsHtml(text);
    }
    return $sce.trustAsHtml(text.replace(new RegExp(search, 'gi'), '<span class="highlighted">$&</span>'));
};

$scope.find_keyword=function(document_type){

  if(document_type=='PAN' || document_type=='P' || document_type=='D' || document_type=='U' || document_type=='V' || document_type=='B' || document_type=='E' || document_type=='T' || document_type=='M' || document_type=='G' || document_type=='W' || document_type=='PC'){
    return 'P'
  }else{
    return 'A'
  }
}

$scope.reset_model=function(){
  $scope.user.client='';
  $scope.user.new_document_type='PAN';
  $scope.upload_file='';
  $scope.searchResult1={};
  $scope.account_details={};
  $scope.bank_details=1;
  $scope.upload_file_msg='';
}

$scope.get_bank_details=function(document_type,client)
{
 
  if(document_type=='CHEQUE' || document_type=='STATEMENT' || document_type=='PASSBK' || document_type=='NSE_M' || document_type=='MFU_M' || document_type=='BSE_M'){
    if(client!='' && client!=undefined)
    {
          $.ajax({
                                url:'/get_bank_details/',
                                type:'POST',
                                async:false,
                                dataType: 'json',
                                data:{'client': $scope.user.client,'bid':$scope.user.id},
                                success: function(response){

                                  // alert(response.status)
                                  if(response.status==true){
                                    $scope.get_account_no(response.data);
                                  }else{
                                    swal(response.status)
                                  }
                                 
                                  ////Next Tab Show function call
                                  

                            }

                });        
    }
  }
}


$scope.bank_details=1;
$scope.get_account_no=function(data){
  var elements=data.length;
  $scope.bank_details=elements;
  if(elements==1){
    $scope.user.bank='BANK1'
  }else if(elements>1){
      forms=[]
      for ( var i = 0; i < elements; i++)
      {
          forms.push({
                keyword: 'BANK'+data[i].bank_no,
                account_no: data[i].account_no,
                                
          });

          $scope.account_details=forms

      }

  }
}



$rootScope.iviewer=function(id,image){

  $('#'+id).css('border','1px solid #dae0e8');
  var id = $("#"+id).iviewer(
  {
          src: image,
          // onClick: function () {
          //     window.open(image)
          // }
          // onFinishLoad: function () {
          //     id.iviewer('zoom_by', 2);
          // }
  });

  id.iviewer('loadImage', image);
  id.iviewer('fit');

  $("#in").click(function(){ id.iviewer('zoom_by', 1); });
  $("#out").click(function(){ id.iviewer('zoom_by', -1); });
  $("#fit").click(function(){ id.iviewer('fit'); });
  $("#orig").click(function(){ id.iviewer('set_zoom', 100); });
  $("#update").click(function(){ id.iviewer('update_container_info'); });


}




})


