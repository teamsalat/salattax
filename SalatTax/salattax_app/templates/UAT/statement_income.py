# coding=utf-8
from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

import os, sys
# import webdriver
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time
from io import BytesIO
from StringIO import StringIO
import subprocess
from subprocess import Popen, PIPE, STDOUT
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
#from pyvirtualdisplay import Display

#for url request
import requests

# from dateutil.relativedelta import relativedelta
import pdfkit
import xml.etree.cElementTree as ET
from .path import *
import codecs
# import openpyxl
# import pandas as pd
import random
import string
from django.db.models import Q

import zipfile
from zipfile import ZipFile

from .ITR2_xml_generation import *
from .serializer import RFChargesSerializer
from .serializer import tax_variablesSerializer,RFPaymentDetailsSerializer
from django.db.models.functions import Now

@csrf_exempt
def get_personal_info(request):
	if request.method=='POST':
		result={}

		try:
			client_id=request.POST.get('client_id')
			name, PAN, dob, address = ('',)*4

			for client in Personal_Details.objects.filter(P_Id = client_id):
				name = client.name.replace('|',' ').title()
				PAN = client.pan
				# temp_date = json_serial(client.dob.strftime("%d-%m-%Y"))
				dob = client.dob.strftime("%d-%m-%Y")

			personal_instance=Personal_Details.objects.get(P_Id=client_id)

			for a in Address_Details.objects.filter(p_id=personal_instance):
				address = (a.flat_door_block_no or '')+' '+(a.name_of_premises_building or '')+' '+(a.road_street_postoffice or '')
				address = address+' '+(a.area_locality or '')+', '+(a.town_city or '')+', '+(a.state or '')

			result['status']= 'success'
			result['name']= name
			result['PAN']= PAN
			result['dob']= dob
			result['address']= address.title()
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_statement_income(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			R_ID = get_rid(client_id)

			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			# no_of_company = Client_fin_info1.objects.filter(client_id=client_id).exclude(flag=2).count()
			no_of_company = 0

			name = []
			salary = []
			less_tax = []
			sal, ICU_head_sal, salary_income = (0,)*3
			ICU_head_sal_str = ''

			for ed in Employer_Details.objects.filter(P_id=personal_instance):
				no_of_company += 1
				# name.append(ed.Name_of_employer.title())
				name.append(ed.Name_of_employer)
				# income_arr = get_income_varibles(R_ID,'income',ed.Name_of_employer)
				if Income.objects.filter(R_Id=R_ID).exists():
					for i in Income.objects.filter(R_Id=R_ID,company_name=ed.Name_of_employer):
						calculated_salary = float(i.Salary or 0)+abs(float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0))-abs(float(i.Other_allowances or 0))
						salary.append(calculated_salary)
						salary_income += float(i.Salary or 0) 
						less_tax.append( float(i.Profession_Tax or 0) )
						ICU_head_sal += calculated_salary - float(i.Profession_Tax or 0)
						ICU_head_sal_str += ' ' + str(calculated_salary) + ' - ' + str(i.Profession_Tax)
				else:
					salary.append( 0 )
					less_tax.append(0)

			if not computation.objects.filter(R_Id = R_ID).exists():
				computation.objects.create(
					R_Id = R_ID,
					salary_income = salary_income,
					).save()
			else:
				computation_instance=computation.objects.get(R_Id=R_ID)
				computation_instance.salary_income=salary_income
				computation_instance.save()

			result['status']= 'success'
			result['no_of_company']= no_of_company
			result['name']= name
			result['salary']= salary
			result['less_tax']= less_tax
			result['ICU_head_sal']= ICU_head_sal
			result['ICU_head_sal_str']= ICU_head_sal_str
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_house_income(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			R_ID = get_rid(client_id)

			no_of_property = House_Property_Details.objects.filter(R_Id=R_ID).count()

			# address = []
			# rent_r = []
			# municipal_tax = []
			# interest_payble = []
			# standard_deduction = []
			# Type_of_House_Property = []
			# net_income = []
			r, m, sd, i, ICU_house_p = (0,)*5

			hp_arr = get_itr_varibles(client_id,'hp_detail')
			Type_of_House_Property = hp_arr['HP_type_of_hp']
			address = hp_arr['address']
			rent_r = hp_arr['HP_Rent_Received']
			municipal_tax = hp_arr['HP_municipal_tax']
			interest_payble = hp_arr['HP_Interest']
			standard_deduction = hp_arr['standard_deduction']
			net_income = hp_arr['HP_ICU_house_p']
			ICU_house_p = hp_arr['ICU_house_p_display']

			proportionate_annual_value_arr = hp_arr['proportionate_annual_value']
			less_standard_deduction_arr = hp_arr['less_standard_deduction']
			ICU_house_p_latest = hp_arr['ICU_house_p_latest']
			assessee_share = hp_arr['assessee_share']

			# for a in House_Property_Details.objects.filter(R_Id = R_ID):
				# Type_of_House_Property.append(a.Type_of_Hosue_Property)
				# add=(a.Name_of_the_Premises_Building_Village or '')+' '+(a.Road_Street_Post_Office or '')
				# add = add+' '+(a.Area_Locality or '')+' '+(a.Town_City or '')+', '+(a.State or '')
				# address.append(add)
				# r = float(a.Rent_Received or 0)
				# rent_r.append( r )
				# m = float(a.Property_Tax or 0)
				# municipal_tax.append( m )
				# i = float(a.Interest_on_Home_loan or 0)
				# interest_payble.append( i )
				# sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
				# standard_deduction.append( sd )
				# net_income.append( float(a.Property_Tax or 0) )
				# ICU_house_p += abs(r)-abs(m)-abs(sd)-abs(i)

			if not computation.objects.filter(R_Id = R_ID).exists():
				computation.objects.create(
					R_Id = R_ID,
					property_income = ICU_house_p,
					).save()
			else:
				computation_instance=computation.objects.get(R_Id=R_ID)
				computation_instance.property_income=ICU_house_p
				computation_instance.save()

			result['status']= 'success'
			result['no_of_property']= no_of_property
			result['Type_of_House_Property']= Type_of_House_Property
			result['address']= address
			result['rent_r']= rent_r
			result['municipal_tax']= municipal_tax
			result['interest_payble']= interest_payble
			result['standard_deduction']= standard_deduction
			result['net_income']= net_income
			result['ICU_house_p']= sum(ICU_house_p_latest)

			result['proportionate_annual_value'] = proportionate_annual_value_arr
			result['less_standard_deduction'] = less_standard_deduction_arr
			result['ICU_house_p_latest'] = ICU_house_p_latest
			result['assessee_share'] = assessee_share
			# result['ICU_house_p_latest_display'] = sum(ICU_house_p_latest)
		except Exception as ex:
			log.error('Error in get_house_income : '+traceback.format_exc())
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_statement_income_other_source(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			R_ID = get_rid(client_id)

			Interest_Income, Interest_Savings, Other_Income, dividend, ICU_other_s = (0,)*5

			other_i_arr = get_itr_varibles(client_id,'other_i_common')
			log.info(other_i_arr)
			Interest_Savings = other_i_arr['Interest_Savings']
			Other_Income = other_i_arr['Other_Income']
			Interest_Income = other_i_arr['Interest_Income']
			FD = other_i_arr['FD']
			exempt_i_arr = get_itr_varibles(client_id,'exempt_i')
			dividend = exempt_i_arr['dividend']

			# for a in Other_Income_Common.objects.filter(R_Id = R_ID):
			# 	Interest_Savings = float(a.Interest_Savings or 0)
			# 	Other_Income = float(a.Other_Income or 0)
			# 	Interest_Income = float(a.Other_Interest or 0)
			# for e in Exempt_Income.objects.filter(R_Id = R_ID):
			# 	dividend = float(e.Tax_Free_Dividend or 0)

			result['status']= 'success'
			result['Interest_Savings']= Interest_Savings
			result['Other_Income']= Other_Income
			result['Interest_Income']= Interest_Income
			result['dividend']= dividend
			result['FD']= FD
			result['ICU_other_s']= Interest_Income + Interest_Savings + Other_Income + FD
		except Exception as ex:
			result['status']= 'Error get_statement_income_other_source: ' + traceback.format_exc()
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_deduction(request):
	if request.method=='POST':
		result={}
		try:
			citizen_type='OC'
			client_id=request.POST.get('client_id')

			for client in Personal_Details.objects.filter(P_Id = client_id):
				if client.dob is None:
					dob = ''
				else:
					dob = client.dob.strftime('%d-%m-%Y')

			if dob == '':
				age = 50
			else:
				age1 = dob.split('-')[2]
				age = 2019 - int(age1)

			log.info(age)

			R_ID = get_rid(client_id)

			d_80c, d_80ccd, d_80g, d_80dd, d_80u, d_80d, d_80e, d_80tta, total_deduction = (0,)*9
			d_80ttb = (0,)*1

			deduction_arr = get_itr_varibles(client_id,'deduction')
			d_80c = deduction_arr['d_80c']
			d_80ccc = deduction_arr['d_80ccc']
			d_80ccd = deduction_arr['d_80ccd']
			d_80ccd1 = deduction_arr['d_80ccd1']
			d_80g = deduction_arr['d_80g']
			d_80dd = deduction_arr['d_80dd']
			d_80u = deduction_arr['d_80u']
			d_80d = deduction_arr['d_80d']
			d_80e = deduction_arr['d_80e']
			d_80tta = deduction_arr['d_80tta']
			d_80ttb = deduction_arr['d_80ttb']
			d_80ddb = deduction_arr['d_80ddb']
			d_80ee = deduction_arr['d_80ee']
			d_80gg = deduction_arr['d_80gg']
			d_80gga = deduction_arr['d_80gga']
			d_80ggc = deduction_arr['d_80ggc']
			d_80rrb = deduction_arr['d_80rrb']
			d_80qqb = deduction_arr['d_80qqb']
			d_80ccg = deduction_arr['d_80ccg']

			# for a in VI_Deductions.objects.filter(R_Id = R_ID):
				# d_80c = float(a.VI_80_C or 0)
				# d_80ccd = float(a.VI_80_CCD_1 or 0)
				# d_80g = float(a.VI_80_G or 0)
				# d_80dd = float(a.VI_80_DD or 0)
				# d_80u = float(a.VI_80_U or 0)
				# d_80d = float(a.VI_80_D or 0)
				# d_80e = float(a.VI_80_E or 0)
				# d_80tta = float(a.VI_80_TTA or 0)

				# d_80ccc = float(a.VI_80_CCC or 0)
				# d_80ccd1 = float(a.VI_80_CCD_1 or 0)
				# d_80ddb = float(a.VI_80_DDB or 0)
				# d_80ee = float(a.VI_80_EE or 0)
				# d_80gg = float(a.VI_80_GG or 0)
				# d_80gga = float(a.VI_80_GGA or 0)
				# d_80ggc = float(a.VI_80_GGC or 0)
				# d_80rrb = float(a.VI_80_RRB or 0)
				# d_80qqb = float(a.VI_80_QQB or 0)
				# d_80ccg = float(a.VI_80_CCG or 0)
			if d_80c>150000:
				d_80c = 150000
			if d_80ccd>50000:
				d_80ccd=50000
			if d_80dd > 75000:
				d_80dd = 75000
			if d_80u > 125000:
				d_80u = 125000
			if d_80tta > 10000:
				d_80tta = 10000


			log.info('d_80c' +str(d_80c))
			log.info( 'd_80ccd ' +str(d_80ccd))
			log.info(' d_80g ' +str(d_80g))
			log.info(' d_80dd '+str(d_80dd))
			log.info(' d_80u '+ str(d_80u))
			log.info('d_80d ' +str(d_80d))
			log.info('d_80e '+str(d_80e))
			log.info(' d_80ttb ' + str(d_80ttb))
			log.info('d_80tta '+ str(d_80tta))

			# total_deduction = d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta
			# total_deduction += d_80ccc + d_80ccd1 + d_80ddb + d_80ee + d_80gg + d_80gga + d_80ggc
			# total_deduction += d_80rrb + d_80qqb + d_80ccg
			return_year = 0
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]


			if age<60:
				var_80D_Self_Limit=25000
			elif age>60:
				var_80D_Self_Limit=50000

			parent_citizen_type=get_parent_age(client_id)
			if parent_citizen_type=='OC':
				var_80D_parents_Limit=25000
			elif parent_citizen_type=='SC' or parent_citizen_type=='SSC':
				var_80D_parents_Limit=50000
			else:
				var_80D_parents_Limit=0


			var_80D_Limit=var_80D_Self_Limit  + var_80D_parents_Limit
			log.info('var_80D_Limit '+str(var_80D_Limit))

			d_80d=min(d_80d,var_80D_Limit)

			if(return_year == '2019'):
				d_80ttb = min(d_80ttb,50000)
				if age<60:
					log.info('d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta')
					log.info(d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta)
					total_deduction = d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta
				
				else:
					citizen_type='SC'
					log.info('d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80ttb')
					log.info(d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80ttb)
					total_deduction = d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80ttb
			else:
				log.info('d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta')
				log.info(d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta)
				total_deduction = d_80c + d_80ccd + d_80g + d_80dd + d_80u + d_80d + d_80e + d_80tta

			log.info(total_deduction)

			if not computation.objects.filter(R_Id = R_ID).exists():
				computation.objects.create(
					R_Id = R_ID,
					total_deduction = round(total_deduction,2),
					).save()
			else:
				computation_instance=computation.objects.get(R_Id=R_ID)
				computation_instance.total_deduction=round(total_deduction,2)
				computation_instance.save()

			result['status']= 'success'
			result['d_80c']= d_80c
			result['d_80ccd']= d_80ccd
			result['d_80g']= d_80g
			result['d_80dd']= d_80dd
			result['d_80u']= d_80u
			result['d_80d']= d_80d
			result['d_80e']= d_80e
			result['d_80tta']= d_80tta
			result['d_80ttb']= d_80ttb
			result['var_80D_Limit']= var_80D_Limit
			result['citizen_type']=citizen_type
			result['total_deduction']= total_deduction
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def months_between(start,end):
	months = []
	cursor = start
	while cursor <= end:
		if cursor.month not in months:
			months.append(cursor.month)
		cursor += timedelta(weeks=1)

	return months

def count_month(start,end):
	months = []
	while start < end:
		months.append(str(start) )
		start += relativedelta(months=1)

	return len(months)

@csrf_exempt
def cal_income_chargeable(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			pan=request.POST.get('pan')

			R_ID = get_rid(client_id)

			return_year = 0
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]

			if(return_year == '2019'):
				value_of_computation = cal_income_chargeable_2019(R_ID,client_id)
			else:
				value_of_computation = cal_income_chargeable_2018(R_ID,client_id)
			# age, dob = ('',)*2
			
			# sal, ICU_head_sal, r, m, sd, i, ICU_house_p, Interest_Income, Interest_Savings = (0,)*9
			# Other_Income, dividend, ICU_other_s, d_80c, d_80d, d_80e, d_80tta, GTI, TI, Tx = (0,)*10
			# Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
			# total_adv_tax_paid, balance_tax, refund = (0,)*3
			# interest_total, interest_234A, interest_234B, interest_234C = (0,)*4

			# for client in Personal_Details.objects.filter(P_Id = client_id):
			# 	if client.dob is None:
			# 		dob = ''
			# 	else:
			# 		dob = client.dob.strftime('%d-%m-%Y')

			# log.info(return_year)
			# if dob == '':
			# 	age = 50
			# else:
			# 	age1 = dob.split('-')[2]
			# 	age = 2019 - int(age1)

			# log.info(age)
			# edu_cess_per = 0.03
			# # Income_chargeable under the head Salaries (S)
			# # for c in Client_fin_info1.objects.filter(client_id = client_id):
			# # 	sal = float(c.salary_as_per_provision or 0) + float(c.value_of_perquisites or 0)
			# # 	sal += float(c.profits_in_lieu_of_salary or 0) -float(c.entertainment_allowance or 0)
			# # 	ICU_head_sal += sal - float(c.profession_tax or 0)
			# calculated_salary = 0
			
			# for i in Income.objects.filter(R_Id=R_ID):
			# 	# abs () change - to + for correction only (05apr2019) 
			# 	calculated_salary = float(i.Salary or 0)+abs(float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0))-abs(float(i.Form16_HRA or 0)+float(i.LTA or 0) + float(i.Other_allowances or 0) )
			# 	# sal += i.Salary
			# 	sal += calculated_salary
			# 	ICU_head_sal += calculated_salary - float(i.Profession_Tax or 0) - float(i.Entertainment_Allowance or 0)

			# # Income_chargeable under House Property (HP)
			# for a in House_Property_Details.objects.filter(R_Id = R_ID):
			# 	r = float(a.Rent_Received or 0)
			# 	m = float(a.Property_Tax or 0)
			# 	i = float(a.Interest_on_Home_loan or 0)
			# 	sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
			# 	ICU_house_p += abs(r)-abs(m)-abs(sd)-abs(i)

			# # Income_from Other Sources (OS)
			# fd, commission = (0,)*2
			# for a in Other_Income_Common.objects.filter(R_Id = R_ID):
			# 	Interest_Income = float(a.Other_Interest or 0)
			# 	Interest_Savings = float(a.Interest_Savings or 0)
			# 	Other_Income = float(a.Other_Income or 0)
			# 	fd = float(a.FD or 0)
			# 	commission = float(a.Commission or 0)
			# # ICU_other_s = Interest_Income + Interest_Savings + Other_Income
			# ICU_other_s = Interest_Income + Interest_Savings + Other_Income + fd + commission

			# # Deduction
			# for a in VI_Deductions.objects.filter(R_Id = R_ID):
			# 	d_80c = float(a.VI_80_C or 0)
			# 	d_80d = float(a.VI_80_D or 0)
			# 	d_80e = float(a.VI_80_E or 0)
			# 	d_80tta = float(a.VI_80_TTA or 0)

			# if return_year == '2019':
			# 	edu_cess_per = 0.04
			# 	if d_80c>150000:
			# 		d_80c = 150000
			# 	if d_80d>30000:
			# 		d_80d = 30000
			# 	if d_80tta > 10000:
			# 		d_80tta = 10000
			# else:
			# 	edu_cess_per = 0.03
			# 	if d_80c>150000:
			# 		d_80c = 150000
			# 	if d_80d>30000:
			# 		d_80d = 30000
			# 	if d_80tta > 10000:
			# 		d_80tta = 10000

			# # Capital Gain
			# st_shares_tpv, st_shares_tsv, st_shares_gain, lt_shares_gain = (0,)*4
			# st_equity_tpv, st_equity_tsv, st_equity_gain, lt_equity_gain = (0,)*4
			# lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
			# lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3
			# st_debt_MF_gain = 0
			# if Shares_ST.objects.filter(R_Id=R_ID).exists():
			# 	Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
			# 	st_shares_gain= Shares_ST_instance.Expenditure
			# if Shares_LT.objects.filter(R_Id=R_ID).exists():
			# 	Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
			# 	lt_shares_gain= Shares_LT_instance.Expenditure
			# if Equity_ST.objects.filter(R_Id=R_ID).exists():
			# 	Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
			# 	st_equity_gain= Equity_ST_instance.Expenditure
			# if Equity_LT.objects.filter(R_Id=R_ID).exists():
			# 	Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
			# 	lt_equity_gain= Equity_ST_instance.Expenditure
			# if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
			# 	Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
			# 	lt_debt_MF_gain= Debt_MF_LT_instance.Expenditure
			# if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
			# 	Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
			# 	st_debt_MF_gain= Debt_MF_ST_instance.Expenditure

			# if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
			# 	Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
			# 	lt_listed_d_gain= Listed_Debentures_LT_instance.Expenditure

			# # ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain
			# ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain + lt_shares_gain + lt_equity_gain + lt_debt_MF_gain

			# # Gross Total Income (GTI)
			# # GTI = ICU_head_sal + ICU_house_p + ICU_other_s
			# GTI = ICU_head_sal + ICU_house_p + ICU_other_s + ICU_CG
			# # Also add Income chargable under business and profession

			# # ----Taxable income (TI) = GTI -80c - 80D - 80E - 80TTA
			# # TI = float(GTI - d_80c - d_80d - d_80e - d_80tta)
			# total_deduction = 0
			# if computation.objects.filter(R_Id = R_ID).exists():
			# 	computation_instance=computation.objects.get(R_Id=R_ID)
			# 	total_deduction = computation_instance.total_deduction or 0
			# TI = GTI - float(total_deduction)

			# if age < 60:
			# 	log.info('Not Senior Citizen')
			# 	if ( TI>1000000):
			# 		Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
			# 	if ( TI>500000 ):
			# 		if TI<1000000:
			# 			Tx = ( (TI-500000)*0.2 ) + 12500
			# 	if ( TI>250000 ):
			# 		if TI<500000:
			# 			Tx = ( (TI-250000)*0.05 )
			# 	if ( TI<250000 ):
			# 		Tx = 0
			# else:
			# 	log.info('Senior Citizen')
			# 	if ( TI>1000000):
			# 		Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
			# 	if ( TI>500000 ):
			# 		if TI<1000000:
			# 			Tx = ( (TI-500000)*0.2 ) + 12500
			# 	if ( TI>300000 ):
			# 		if TI<500000:
			# 			Tx = ( (TI-300000)*0.05 )
			# 	if ( TI<300000 ):
			# 		Tx = 0

			# # Tax at Special rates (TX1) = (Shares ST1,ST2)*15%
			# # spcl_rate15 = (st_shares_tpv + st_equity_tpv) * 0.15
			# # spcl_rate20 = (lt_debt_MF_tpv) * 0.20
			# # spcl_rate10 = (lt_listed_d_tpv) * 0.10
			# spcl_rate15 = (st_shares_gain + st_equity_gain) * 0.15
			# spcl_rate20 = (lt_debt_MF_gain) * 0.20
			# spcl_rate10 = (lt_listed_d_gain) * 0.10
			# Tx1 = spcl_rate15 + spcl_rate20 + spcl_rate10
			# # Rebate (R) = if TI<3,50,000 then R= 2500 if TI<=3,00,000 then R=Tx
			# if TI < 350000 :
			# 	rebate = 2500
			# if TI <= 300000:
			# 	rebate = Tx

			# # Surcharge(SH)
			# if TI>5000000 and TI<10000000:
			# 	surcharge = (Tx + Tx1)*0.1
			# if TI>10000000:
			# 	surcharge = (Tx + Tx1)*0.15

			# education_cess = (Tx + Tx1 - rebate)*edu_cess_per
			# tax_with_cess = (Tx + Tx1 - rebate + education_cess)

			# PAN = ''
			# if Personal_Details.objects.filter(P_Id = client_id).exists():
			# 	for client in Personal_Details.objects.filter(P_Id = client_id):
			# 		PAN = client.pan

			# for tds in tds_tcs.objects.filter(R_Id = R_ID):
			# 	total_tds_paid += float(tds.tds_tcs_deposited or 0)

			# for tax in tax_paid.objects.filter(R_Id = R_ID):
			# 	total_adv_tax_paid += float(tax.total_tax or 0)

			# if (Tx == rebate):
			# 	education_cess = 0
			# 	tax_with_cess = 0

			# balance_tax = tax_with_cess - total_tds_paid - total_adv_tax_paid
			# if balance_tax<0:
			# 	refund = balance_tax

			# startDate = '2018-7-31'
			# startDate234B = '2018-4-1'
			# # change start date to 31 Aug
			# now = datetime.now()
			# endDate = now.strftime("%Y-%m-%d")
			# cur_date = datetime.strptime(startDate, '%Y-%m-%d').date()
			# end = datetime.strptime(endDate, '%Y-%m-%d').date()
			# post_due_date = []

			# cur_date234B = datetime.strptime(startDate234B, '%Y-%m-%d').date()
			# end234B = datetime.strptime(endDate, '%Y-%m-%d').date()
			# post_due_date234B = []
			
			# while cur_date < end:
			# 	post_due_date.append(str(cur_date) )
			# 	cur_date += relativedelta(months=1)

			# while cur_date234B < end234B:
			# 	post_due_date234B.append(str(cur_date234B) )
			# 	cur_date234B += relativedelta(months=1)

			# per_int_rate = len(post_due_date)
			# per_int_rate234B = len(post_due_date234B)
			# interest_234A = (tax_with_cess-total_tds_paid)*(per_int_rate/100)
			# if tax_with_cess - total_tds_paid >10000 and total_adv_tax_paid==0:
			# 	interest_234B = round( (tax_with_cess-total_tds_paid)*0.01*per_int_rate234B ,2)
			# elif total_adv_tax_paid < (tax_with_cess - total_tds_paid)*0.9:
			# 	interest_234B = round( balance_tax*0.01*per_int_rate234B ,2)

			# start_cond1 = '2018-6-15'
			# startDate_cond1 = datetime.strptime(start_cond1, '%Y-%m-%d').date()
			# start_cond2 = '2018-9-15'
			# startDate_cond2 = datetime.strptime(start_cond2, '%Y-%m-%d').date()
			# start_cond3 = '2018-12-15'
			# startDate_cond3 = datetime.strptime(start_cond3, '%Y-%m-%d').date()
			# start_cond4 = '2019-3-15'
			# startDate_cond4 = datetime.strptime(start_cond4, '%Y-%m-%d').date()

			# # if total_adv_tax_paid<0:
			# # 	interest_234C = 0
			# # else:
			# # 	for tax in tax_paid.objects.all():
			# # 		if tax.deposit_date:
			# # 			pass
			# interest_234C_text = ''
			# for tax in tax_paid.objects.all():
			# 	interest_234C_text = count_month(startDate_cond1,end)

			# interest_total = interest_234A + interest_234B + interest_234C
			log.info(value_of_computation)
			sal, GTI, ICU_CG, TI, rebate, surcharge, Tx, Tx1, education_cess  = (0,)*9
			total_adv_tax_paid, interest_total, balance_tax, interest_234A, interest_234B = (0,)*5
			interest_234C, interest_234C_text, tax_with_cess, refund, total_tds_paid, ICU_head_sal = (0,)*6
			ICU_house_p, ICU_other_s = (0,)*2

			# log.info(interest_234A)
			# log.info(interest_234B)
			# log.info(interest_234C)

			# interest_total = interest_234A + interest_234B + interest_234C
			sal = value_of_computation['sal']
			GTI = value_of_computation['GTI']
			ICU_CG = value_of_computation['ICU_CG']
			TI = value_of_computation['TI']
			rebate = value_of_computation['rebate']
			surcharge = value_of_computation['surcharge']
			Tx = value_of_computation['Tx']
			Tx1 = value_of_computation['Tx1']
			education_cess = value_of_computation['education_cess']
			total_adv_tax_paid = value_of_computation['total_adv_tax_paid']
			interest_total = value_of_computation['interest_total']
			balance_tax = value_of_computation['balance_tax']
			interest_234A = value_of_computation['interest_234A']
			interest_234B = value_of_computation['interest_234B']
			interest_234C = value_of_computation['interest_234C']
			interest_234F = value_of_computation['interest_234F']
			tax_with_cess = value_of_computation['tax_with_cess']
			refund = value_of_computation['refund']
			total_tds_paid = value_of_computation['total_tds_paid']
			ICU_head_sal = value_of_computation['ICU_head_sal']
			ICU_house_p = value_of_computation['ICU_house_p']
			ICU_other_s = value_of_computation['ICU_other_s']

			balance_tax_FE = value_of_computation['balance_tax_FE']
			Interest_on_FE = value_of_computation['Interest_on_FE']
			Tax_payable_FE = value_of_computation['Tax_payable_FE']

			if not computation.objects.filter(R_Id = R_ID).exists():
				computation.objects.create(
					R_Id = R_ID,
					salary_income = sal,
					gross_total_income = round(GTI),
					capital_gains = round(ICU_CG),
					taxable_income = round(TI),
					rebate = round(rebate),
					surcharge = round(surcharge),
					IT_normal_rates = round(Tx),
					IT_special_rates = round(Tx1),
					add_education_cess = round(education_cess),
					total_tax = round(total_adv_tax_paid),
					interest_234A=round(interest_234A),
					interest_234B=round(interest_234B),
					interest_234C=round(interest_234C),
					interest_234F=round(interest_234F),
					interest_on_tax = round(interest_total),
					taxpayable_refund = round(balance_tax+interest_total),
				).save()
			else:
				computation_instance=computation.objects.get(R_Id=R_ID)
				computation_instance.salary_income=round(sal)
				computation_instance.gross_total_income=round(GTI)
				computation_instance.capital_gains=round(ICU_CG)
				computation_instance.taxable_income=round(TI)
				computation_instance.rebate=round(rebate)
				computation_instance.surcharge=round(surcharge)
				computation_instance.IT_normal_rates=round(Tx)
				computation_instance.IT_special_rates=round(Tx1)
				computation_instance.add_education_cess=round(education_cess)
				computation_instance.total_tax=round(total_adv_tax_paid)
				computation_instance.interest_234A=round(interest_234A)
				computation_instance.interest_234B=round(interest_234B)
				computation_instance.interest_234C=round(interest_234C)
				computation_instance.interest_234F=round(interest_234F)
				computation_instance.interest_on_tax=round(interest_total)
				computation_instance.taxpayable_refund=round(balance_tax+interest_total)
				computation_instance.save()

			result['status']= 'success'
			result['interest_234A']= interest_234A
			result['interest_234B']= interest_234B
			result['interest_234C']= interest_234C
			result['interest_234F']= interest_234F
			result['interest_total']=interest_total
			result['GTI']= round(GTI)
			result['ICU_CG']= round(ICU_CG)
			result['TI']= round(TI)
			result['Tx']= round(Tx)
			result['Tx1']= round(Tx1)
			result['rebate']= round(rebate)
			result['surcharge']= round(surcharge)
			result['education_cess']= round(education_cess)
			result['tax_with_cess']= round(tax_with_cess)
			result['balance_tax']= round(balance_tax)
			result['refund']= round(refund)
			result['total_adv_tax_paid']= round(total_adv_tax_paid)
			result['total_tds_paid']= round(total_tds_paid)
			result['ICU_head_sal']= round(ICU_head_sal)
			result['ICU_house_p']= round(ICU_house_p)
			result['ICU_other_s']= round(ICU_other_s)

			result['balance_tax_FE']= round(balance_tax_FE)
			result['Interest_on_FE']= round(Interest_on_FE)
			result['Tax_payable_FE']= round(Tax_payable_FE)
		except Exception as ex:
			log.error('Error in cal_income_chargeable : '+traceback.format_exc())
			result['status']= 'Error in cal_income_chargeable : '+traceback.format_exc()
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def cal_income_chargeable_original(R_ID,client_id):
	result={}
	result['status'] = 'pending'
	age, dob = ('',)*2
	
	sal, ICU_head_sal, r, m, sd, i, ICU_house_p, Interest_Income, Interest_Savings = (0,)*9
	Other_Income, dividend, ICU_other_s, d_80c, d_80d, d_80e, d_80tta, GTI, TI, Tx = (0,)*10
	Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
	total_adv_tax_paid, balance_tax, refund = (0,)*3
	interest_total, interest_234A, interest_234B, interest_234C = (0,)*4

	for client in Personal_Details.objects.filter(P_Id = client_id):
		if client.dob is None:
			dob = ''
		else:
			dob = client.dob.strftime('%d-%m-%Y')

	log.info(return_year)
	if dob == '':
		age = 50
	else:
		age1 = dob.split('-')[2]
		age = 2019 - int(age1)

	log.info(age)
	edu_cess_per = 0.03
	# Income_chargeable under the head Salaries (S)
	# for c in Client_fin_info1.objects.filter(client_id = client_id):
	# 	sal = float(c.salary_as_per_provision or 0) + float(c.value_of_perquisites or 0)
	# 	sal += float(c.profits_in_lieu_of_salary or 0) -float(c.entertainment_allowance or 0)
	# 	ICU_head_sal += sal - float(c.profession_tax or 0)
	calculated_salary = 0
	
	for i in Income.objects.filter(R_Id=R_ID):
		# abs () change - to + for correction only (05apr2019) 
		calculated_salary = float(i.Salary or 0)+abs(float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0))-abs(float(i.Form16_HRA or 0)+float(i.LTA or 0) + float(i.Other_allowances or 0) )
		# sal += i.Salary
		sal += calculated_salary
		ICU_head_sal += calculated_salary - float(i.Profession_Tax or 0) - float(i.Entertainment_Allowance or 0)

	# Income_chargeable under House Property (HP)
	for a in House_Property_Details.objects.filter(R_Id = R_ID):
		r = float(a.Rent_Received or 0)
		m = float(a.Property_Tax or 0)
		i = float(a.Interest_on_Home_loan or 0)
		sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
		ICU_house_p += abs(r)-abs(m)-abs(sd)-abs(i)

	# Income_from Other Sources (OS)
	fd, commission = (0,)*2
	for a in Other_Income_Common.objects.filter(R_Id = R_ID):
		Interest_Income = float(a.Other_Interest or 0)
		Interest_Savings = float(a.Interest_Savings or 0)
		Other_Income = float(a.Other_Income or 0)
		fd = float(a.FD or 0)
		commission = float(a.Commission or 0)
	# ICU_other_s = Interest_Income + Interest_Savings + Other_Income
	ICU_other_s = Interest_Income + Interest_Savings + Other_Income + fd + commission

	# Deduction
	for a in VI_Deductions.objects.filter(R_Id = R_ID):
		d_80c = float(a.VI_80_C or 0)
		d_80d = float(a.VI_80_D or 0)
		d_80e = float(a.VI_80_E or 0)
		d_80tta = float(a.VI_80_TTA or 0)

	if return_year == '2019':
		edu_cess_per = 0.04
		if d_80c>150000:
			d_80c = 150000
		if d_80d>30000:
			d_80d = 30000
		if d_80tta > 10000:
			d_80tta = 10000
	else:
		edu_cess_per = 0.03
		if d_80c>150000:
			d_80c = 150000
		if d_80d>30000:
			d_80d = 30000
		if d_80tta > 10000:
			d_80tta = 10000

	# Capital Gain
	st_shares_tpv, st_shares_tsv, st_shares_gain, lt_shares_gain = (0,)*4
	st_equity_tpv, st_equity_tsv, st_equity_gain, lt_equity_gain = (0,)*4
	lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
	lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3
	st_debt_MF_gain = 0
	if Shares_ST.objects.filter(R_Id=R_ID).exists():
		Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
		st_shares_gain= Shares_ST_instance.Expenditure
	if Shares_LT.objects.filter(R_Id=R_ID).exists():
		Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
		lt_shares_gain= Shares_LT_instance.Expenditure
	if Equity_ST.objects.filter(R_Id=R_ID).exists():
		Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
		st_equity_gain= Equity_ST_instance.Expenditure
	if Equity_LT.objects.filter(R_Id=R_ID).exists():
		Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
		lt_equity_gain= Equity_ST_instance.Expenditure
	if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
		Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
		lt_debt_MF_gain= Debt_MF_LT_instance.Expenditure
	if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
		Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
		st_debt_MF_gain= Debt_MF_ST_instance.Expenditure

	if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
		Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
		lt_listed_d_gain= Listed_Debentures_LT_instance.Expenditure

	# ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain
	ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain + lt_shares_gain + lt_equity_gain + lt_debt_MF_gain

	# Gross Total Income (GTI)
	# GTI = ICU_head_sal + ICU_house_p + ICU_other_s
	GTI = ICU_head_sal + ICU_house_p + ICU_other_s + ICU_CG
	# Also add Income chargable under business and profession

	# ----Taxable income (TI) = GTI -80c - 80D - 80E - 80TTA
	# TI = float(GTI - d_80c - d_80d - d_80e - d_80tta)
	total_deduction = 0
	if computation.objects.filter(R_Id = R_ID).exists():
		computation_instance=computation.objects.get(R_Id=R_ID)
		total_deduction = computation_instance.total_deduction or 0
	TI = GTI - float(total_deduction)

	if age < 60:
		log.info('Not Senior Citizen')
		if ( TI>1000000):
			Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
		if ( TI>500000 ):
			if TI<1000000:
				Tx = ( (TI-500000)*0.2 ) + 12500
		if ( TI>250000 ):
			if TI<500000:
				Tx = ( (TI-250000)*0.05 )
		if ( TI<250000 ):
			Tx = 0
	else:
		log.info('Senior Citizen')
		if ( TI>1000000):
			Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
		if ( TI>500000 ):
			if TI<1000000:
				Tx = ( (TI-500000)*0.2 ) + 12500
		if ( TI>300000 ):
			if TI<500000:
				Tx = ( (TI-300000)*0.05 )
		if ( TI<300000 ):
			Tx = 0

	# Tax at Special rates (TX1) = (Shares ST1,ST2)*15%
	# spcl_rate15 = (st_shares_tpv + st_equity_tpv) * 0.15
	# spcl_rate20 = (lt_debt_MF_tpv) * 0.20
	# spcl_rate10 = (lt_listed_d_tpv) * 0.10
	spcl_rate15 = (st_shares_gain + st_equity_gain) * 0.15
	spcl_rate20 = (lt_debt_MF_gain) * 0.20
	spcl_rate10 = (lt_listed_d_gain) * 0.10
	Tx1 = spcl_rate15 + spcl_rate20 + spcl_rate10
	# Rebate (R) = if TI<3,50,000 then R= 2500 if TI<=3,00,000 then R=Tx
	if TI < 350000 :
		rebate = 2500
	if TI <= 300000:
		rebate = Tx

	# Surcharge(SH)
	if TI>5000000 and TI<10000000:
		surcharge = (Tx + Tx1)*0.1
	if TI>10000000:
		surcharge = (Tx + Tx1)*0.15

	education_cess = (Tx + Tx1 - rebate)*edu_cess_per
	tax_with_cess = (round(Tx) + round(Tx1) - round(rebate) + round(education_cess))

	PAN = ''
	if Personal_Details.objects.filter(P_Id = client_id).exists():
		for client in Personal_Details.objects.filter(P_Id = client_id):
			PAN = client.pan

	for tds in tds_tcs.objects.filter(R_Id = R_ID):
		total_tds_paid += float(tds.tds_tcs_deposited or 0)

	for tax in tax_paid.objects.filter(R_Id = R_ID):
		total_adv_tax_paid += float(tax.total_tax or 0)

	if (Tx == rebate):
		education_cess = 0
		tax_with_cess = 0

	balance_tax = tax_with_cess - total_tds_paid - total_adv_tax_paid
	if balance_tax<0:
		refund = balance_tax

	startDate = '2018-7-31'
	startDate234B = '2018-4-1'
	# change start date to 31 Aug
	now = datetime.now()
	endDate = now.strftime("%Y-%m-%d")
	cur_date = datetime.strptime(startDate, '%Y-%m-%d').date()
	end = datetime.strptime(endDate, '%Y-%m-%d').date()
	post_due_date = []

	cur_date234B = datetime.strptime(startDate234B, '%Y-%m-%d').date()
	end234B = datetime.strptime(endDate, '%Y-%m-%d').date()
	post_due_date234B = []
	
	while cur_date < end:
		post_due_date.append(str(cur_date) )
		cur_date += relativedelta(months=1)

	while cur_date234B < end234B:
		post_due_date234B.append(str(cur_date234B) )
		cur_date234B += relativedelta(months=1)

	per_int_rate = len(post_due_date)
	per_int_rate234B = len(post_due_date234B)
	interest_234A = (tax_with_cess-total_tds_paid)*(per_int_rate/100)
	if tax_with_cess - total_tds_paid >10000 and total_adv_tax_paid==0:
		interest_234B = round( (tax_with_cess-total_tds_paid)*0.01*per_int_rate234B ,2)
	elif total_adv_tax_paid < (tax_with_cess - total_tds_paid)*0.9:
		interest_234B = round( balance_tax*0.01*per_int_rate234B ,2)

	start_cond1 = '2018-6-15'
	startDate_cond1 = datetime.strptime(start_cond1, '%Y-%m-%d').date()
	start_cond2 = '2018-9-15'
	startDate_cond2 = datetime.strptime(start_cond2, '%Y-%m-%d').date()
	start_cond3 = '2018-12-15'
	startDate_cond3 = datetime.strptime(start_cond3, '%Y-%m-%d').date()
	start_cond4 = '2019-3-15'
	startDate_cond4 = datetime.strptime(start_cond4, '%Y-%m-%d').date()

	interest_234C_text = ''
	for tax in tax_paid.objects.all():
		interest_234C_text = count_month(startDate_cond1,end)

	interest_total = interest_234A + interest_234B + interest_234C
	result['status'] = 'success'
	return result

def cal_income_chargeable_2018(R_ID,client_id):
	result={}
	result['status'] = 'pending'
	age, dob = ('',)*2
	
	sal, ICU_head_sal, r, m, sd, i, ICU_house_p, Interest_Income, Interest_Savings = (0,)*9
	Other_Income, dividend, ICU_other_s, d_80c, d_80d, d_80e, d_80tta, GTI, TI, Tx = (0,)*10
	Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
	total_adv_tax_paid, balance_tax, refund = (0,)*3
	interest_total, interest_234A, interest_234B, interest_234C,interest_234F = (0,)*5

	for client in Personal_Details.objects.filter(P_Id = client_id):
		if client.dob is None:
			dob = ''
		else:
			dob = client.dob.strftime('%d-%m-%Y')

	if dob == '':
		age = 50
	else:
		age1 = dob.split('-')[2]
		age = 2019 - int(age1)

	log.info(age)
	edu_cess_per = 0.03
	# Income_chargeable under the head Salaries (S)
	# for c in Client_fin_info1.objects.filter(client_id = client_id):
	# 	sal = float(c.salary_as_per_provision or 0) + float(c.value_of_perquisites or 0)
	# 	sal += float(c.profits_in_lieu_of_salary or 0) -float(c.entertainment_allowance or 0)
	# 	ICU_head_sal += sal - float(c.profession_tax or 0)
	calculated_salary = 0
	
	for i in Income.objects.filter(R_Id=R_ID):
		# abs () change - to + for correction only (05apr2019) 
		calculated_salary = float(i.Salary or 0)+abs(float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0))-abs(float(i.Form16_HRA or 0)+float(i.LTA or 0) + float(i.Other_allowances or 0) )
		# sal += i.Salary
		sal += calculated_salary
		ICU_head_sal += calculated_salary - float(i.Profession_Tax or 0) - float(i.Entertainment_Allowance or 0)

	# Income_chargeable under House Property (HP)
	for a in House_Property_Details.objects.filter(R_Id = R_ID):
		r = float(a.Rent_Received or 0)
		m = float(a.Property_Tax or 0)
		i = float(a.Interest_on_Home_loan or 0)
		sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
		less_standard_deduction= (float(30)/100)*sd
		ICU_house_p += sd-float(a.Interest_on_Home_loan)-less_standard_deduction
	
	# Income_from Other Sources (OS)
	fd, commission = (0,)*2
	for a in Other_Income_Common.objects.filter(R_Id = R_ID):
		Interest_Income = float(a.Other_Interest or 0)
		Interest_Savings = float(a.Interest_Savings or 0)
		Other_Income = float(a.Other_Income or 0)
		fd = float(a.FD or 0)
		commission = float(a.Commission or 0)
	# ICU_other_s = Interest_Income + Interest_Savings + Other_Income
	ICU_other_s = Interest_Income + Interest_Savings + Other_Income + fd + commission

	# Deduction
	for a in VI_Deductions.objects.filter(R_Id = R_ID):
		d_80c = float(a.VI_80_C or 0)
		d_80d = float(a.VI_80_D or 0)
		d_80e = float(a.VI_80_E or 0)
		d_80tta = float(a.VI_80_TTA or 0)

	edu_cess_per = 0.03
	if d_80c>150000:
		d_80c = 150000
	if d_80d>30000:
		d_80d = 30000
	if d_80tta > 10000:
		d_80tta = 10000

	# Capital Gain
	st_shares_tpv, st_shares_tsv, st_shares_gain, lt_shares_gain = (0,)*4
	st_equity_tpv, st_equity_tsv, st_equity_gain, lt_equity_gain = (0,)*4
	lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
	lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3
	st_debt_MF_gain = 0
	if Shares_ST.objects.filter(R_Id=R_ID).exists():
		Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
		st_shares_gain= Shares_ST_instance.Expenditure
	if Shares_LT.objects.filter(R_Id=R_ID).exists():
		Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
		lt_shares_gain= Shares_LT_instance.Expenditure
	if Equity_ST.objects.filter(R_Id=R_ID).exists():
		Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
		st_equity_gain= Equity_ST_instance.Expenditure
	if Equity_LT.objects.filter(R_Id=R_ID).exists():
		Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
		lt_equity_gain= Equity_ST_instance.Expenditure
	if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
		Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
		lt_debt_MF_gain= Debt_MF_LT_instance.Expenditure
	if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
		Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
		st_debt_MF_gain= Debt_MF_ST_instance.Expenditure

	if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
		Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
		lt_listed_d_gain= Listed_Debentures_LT_instance.Expenditure

	# ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain
	ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain + lt_shares_gain + lt_equity_gain + lt_debt_MF_gain

	# Gross Total Income (GTI)
	# GTI = ICU_head_sal + ICU_house_p + ICU_other_s
	GTI = ICU_head_sal + ICU_house_p + ICU_other_s + ICU_CG
	# Also add Income chargable under business and profession

	# ----Taxable income (TI) = GTI -80c - 80D - 80E - 80TTA
	# TI = float(GTI - d_80c - d_80d - d_80e - d_80tta)
	total_deduction = 0
	if computation.objects.filter(R_Id = R_ID).exists():
		computation_instance=computation.objects.get(R_Id=R_ID)
		total_deduction = computation_instance.total_deduction or 0
	TI = GTI - float(total_deduction)

	if age < 60:
		log.info('Not Senior Citizen')
		if ( TI>1000000):
			Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
		if ( TI>500000 ):
			if TI<1000000:
				Tx = ( (TI-500000)*0.2 ) + 12500
		if ( TI>250000 ):
			if TI<500000:
				Tx = ( (TI-250000)*0.05 )
		if ( TI<250000 ):
			Tx = 0
	else:
		log.info('Senior Citizen')
		if ( TI>1000000):
			Tx = ( (TI-1000000)*0.3 ) + 100000 + 10000
		if ( TI>500000 ):
			if TI<1000000:
				Tx = ( (TI-500000)*0.2 ) + 10000
		if ( TI>300000 ):
			if TI<500000:
				Tx = ( (TI-300000)*0.05 )
		if ( TI<300000 ):
			Tx = 0

	# Tax at Special rates (TX1) = (Shares ST1,ST2)*15%
	# spcl_rate15 = (st_shares_tpv + st_equity_tpv) * 0.15
	# spcl_rate20 = (lt_debt_MF_tpv) * 0.20
	# spcl_rate10 = (lt_listed_d_tpv) * 0.10
	spcl_rate15 = (st_shares_gain + st_equity_gain) * 0.15
	spcl_rate20 = (lt_debt_MF_gain) * 0.20
	spcl_rate10 = (lt_listed_d_gain) * 0.10
	Tx1 = spcl_rate15 + spcl_rate20 + spcl_rate10
	# Rebate (R) = if TI<3,50,000 then R= 2500 if TI<=3,00,000 then R=Tx
	if TI < 350000 :
		rebate = 2500
	if TI <= 300000:
		rebate = Tx

	# Surcharge(SH)
	if TI>5000000 and TI<10000000:
		surcharge = (Tx + Tx1)*0.1
	if TI>10000000:
		surcharge = (Tx + Tx1)*0.15

	education_cess = (Tx + Tx1 - rebate)*edu_cess_per
	tax_with_cess = (round(Tx) + round(Tx1) - round(rebate) + round(education_cess))

	PAN = ''
	if Personal_Details.objects.filter(P_Id = client_id).exists():
		for client in Personal_Details.objects.filter(P_Id = client_id):
			PAN = client.pan

	for tds in tds_tcs.objects.filter(R_Id = R_ID,year='2018'):
		total_tds_paid += float(tds.tds_tcs_deposited or 0)

	for tax in tax_paid.objects.filter(R_Id = R_ID):
		total_adv_tax_paid += float(tax.total_tax or 0)

	if (Tx == rebate):
		education_cess = 0
		tax_with_cess = 0

	balance_tax = tax_with_cess - total_tds_paid - total_adv_tax_paid
	if balance_tax<0:
		refund = balance_tax

	log.info(' ====================Interest Calculation starts=================')

	# =========================234A calculation starts===============
	# 234A
	# If retun_filing_date <= return_filing_due_date then exit
	# Else
	# Interest_234A = 1% * Tax_Payable * [month( retun_filing_date ) - month( return_filing_due_date )]
	# log.info(get_financial_year('06-15'))
	# tax_payable_amount=tax_with_cess - total_tds_paid
	tax_payable_amount=tax_with_cess
	log.info('tax_payable_amount '+str(tax_payable_amount))
	if(tax_payable_amount>=0):

		now = datetime.now()
		current_date = now.strftime("%Y-%m-%d")

		return_filing_date = datetime.strptime(current_date, '%Y-%m-%d').date()
		return_filing_year=now.strftime("%Y")
		return_filing_month=now.strftime("%m")
		log.info('current_date '+str(current_date))

		return_filing_due_date1 = datetime.strptime(return_filing_due_date, '%Y-%m-%d').date()
		return_filing_due_year =return_filing_due_date1.strftime("%Y")
		return_filing_due_month =return_filing_due_date1.strftime("%m")
		log.info('return_filing_due_date '+str(return_filing_due_date1))

		interest_234A_rate=0.01

		if return_filing_date<=return_filing_due_date1:
			pass
		else:
			log.info('return_filing_month '+return_filing_month)
			log.info('return_filing_due_month '+return_filing_due_month)
			month_diff=diff_month(return_filing_year,return_filing_due_year,return_filing_month,return_filing_due_month)
			log.info('month_diff '+str(month_diff))
			interest_234A = round((tax_payable_amount*interest_234A_rate)*month_diff,2)


		log.info('interest_234A '+str(interest_234A))

		# ========================234A calculation ends=================

		# ========================234B calculation starts===============
		# 234B
		# If ((Tax_Payable - Total_TDS_paid) > 10000 & Advance_Tax_Paid = 0)
		# Then
		# Interest_234B = 1% * (Tax_Payable -Total_TDS_paid ) * (month(retun_filing_date) - 4)

		# Else If (Advance_Tax_Paid < 90% * (Tax_Payable - Total_TDS_paid))
		# Then
		# Interest_234B = 1% * (Tax_Payable -Total_TDS_paid - Advance_Tax_Paid) *  (month(retun_filing_date) - 4)
		if age <60 and (tax_payable_amount - total_tds_paid) > 10000:
			interest_234B_rate=0.01
			log.info('total_tds_paid '+str(total_tds_paid))
			log.info('tax_payable_amount - total_tds_paid '+str(tax_payable_amount - total_tds_paid))
			log.info('total_adv_tax_paid '+str(total_adv_tax_paid))


			if (tax_payable_amount - total_tds_paid) >10000 and total_adv_tax_paid==0:
				log.info('interest_234B first condition')
				log.info(((tax_payable_amount-total_tds_paid)*interest_234B_rate))
				log.info(int(return_filing_month)-3)
				interest_234B = round(((tax_payable_amount-total_tds_paid)*interest_234B_rate)*float(int(return_filing_month)-3) ,2)
			
			elif total_adv_tax_paid < (tax_payable_amount - total_tds_paid)*0.9:
				log.info('interest_234B second condition')
				interest_234B = round(((tax_payable_amount-total_tds_paid-total_adv_tax_paid)*interest_234B_rate)*float(int(return_filing_month)-3) ,2)
			
		log.info('interest_234B '+str(interest_234B))

		# ========================234B calculation ends===============

		# ========================234C calculation starts===============
		# 	234C
		# Calculate
		# Advance_Tax_paid_b4_15Jun
		# Advance_Tax_paid_b4_15Sep
		# Advance_Tax_paid_b4_15Dec
		# Advance_Tax_paid_b4_15Mar
		# Amount = Tax_Payable - Total_TDS_paid

		# If (Advance_Tax_paid_b4_15Jun < 15% * Amount) 
		# Then
		# Interest_234C_1 = 1% * 3 * (15% * Amount - Advance_Tax_paid_b4_15Jun)

		# If (Advance_Tax_paid_b4_15Sep < 45% * Amount) 
		# Then
		# Interest_234C_2 = 1% * 3 * (45% * Amount - Advance_Tax_paid_b4_15Sep)

		# If (Advance_Tax_paid_b4_15Dec < 75% * Amount) 
		# Then
		# Interest_234C_3 = 1% * 3 * (75% * Amount - Advance_Tax_paid_b4_15Dec)

		# If (Advance_Tax_paid_b4_15Mar < 75% * Amount) 
		# Then
		# Interest_234C_4 = 1% * 1 * (100% * Amount - Advance_Tax_paid_b4_15Mar)

		# Interest_234C = Interest_234C_1 + Interest_234C_2 + Interest_234C_3 + Interest_234C_4
		if age <60:
			start_cond1 = '2018-6-15'
			startDate_cond1 = datetime.strptime(start_cond1, '%Y-%m-%d').date()
			start_cond2 = '2018-9-15'
			startDate_cond2 = datetime.strptime(start_cond2, '%Y-%m-%d').date()
			start_cond3 = '2018-12-15'
			startDate_cond3 = datetime.strptime(start_cond3, '%Y-%m-%d').date()
			start_cond4 = '2019-3-15'
			startDate_cond4 = datetime.strptime(start_cond4, '%Y-%m-%d').date()

			Amount = tax_payable_amount - total_tds_paid

			log.info('Amount '+str(Amount))

			financial_start_date='2018-04-01'
			financial_start_date = datetime.strptime(financial_start_date, '%Y-%m-%d').date()

			financial_end_date='2019-03-31'
			financial_end_date = datetime.strptime(financial_end_date, '%Y-%m-%d').date()

			Advance_Tax_paid_b4_15Jun,Advance_Tax_paid_b4_15Sep,Advance_Tax_paid_b4_15Dec,Advance_Tax_paid_b4_15Mar=(0,)*4
			Interest_234C_1,Interest_234C_2,Interest_234C_3,Interest_234C_4=(0,)*4

			Tax_paid_b4_15Jun_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond1,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Jun_data.exists():
				log.info
				for data in Tax_paid_b4_15Jun_data:
					Advance_Tax_paid_b4_15Jun=Advance_Tax_paid_b4_15Jun+data.total_tax

			log.info('Advance_Tax_paid_b4_15Jun '+str(Advance_Tax_paid_b4_15Jun))

			Tax_paid_b4_15Sep_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond2,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Sep_data.exists():
				for data in Tax_paid_b4_15Sep_data:
					Advance_Tax_paid_b4_15Sep=Advance_Tax_paid_b4_15Sep+data.total_tax

			log.info('Advance_Tax_paid_b4_15Sep '+str(Advance_Tax_paid_b4_15Sep))

			Tax_paid_b4_15Dec_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond3,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Dec_data.exists():
				for data in Tax_paid_b4_15Dec_data:
					Advance_Tax_paid_b4_15Dec=Advance_Tax_paid_b4_15Dec+data.total_tax

			log.info('Advance_Tax_paid_b4_15Dec '+str(Advance_Tax_paid_b4_15Dec))

			Tax_paid_b4_15Mar_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond4,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Mar_data.exists():
				for data in Tax_paid_b4_15Mar_data:
					Advance_Tax_paid_b4_15Mar=Advance_Tax_paid_b4_15Mar+data.total_tax

			log.info('Advance_Tax_paid_b4_15Mar '+str(Advance_Tax_paid_b4_15Mar))

			log.info('15 percent of amount '+str(0.15*Amount))
			if(Advance_Tax_paid_b4_15Jun<(0.15*Amount)):
				Interest_234C_1 = round(0.01 * 3 * (0.15 * Amount - Advance_Tax_paid_b4_15Jun),2)

			log.info('Interest_234C_1 '+str(Interest_234C_1))

			log.info('45 percent of amount '+str(0.45*Amount))
			if(Advance_Tax_paid_b4_15Sep<(0.45*Amount)):
				Interest_234C_2 = round(0.01 * 3 * (0.45 * Amount - Advance_Tax_paid_b4_15Sep),2)
			
			log.info('Interest_234C_2 '+str(Interest_234C_2))

			log.info('75 percent of amount '+str(0.75*Amount))
			if(Advance_Tax_paid_b4_15Dec<(0.75*Amount)):
				Interest_234C_3 = round(0.01 * 3 * (0.75 * Amount - Advance_Tax_paid_b4_15Dec),2)

			log.info('Interest_234C_3 '+str(Interest_234C_3))

			if(Advance_Tax_paid_b4_15Mar<(0.75*Amount)):
				Interest_234C_4 = round(0.01 *1 * (1 * Amount - Advance_Tax_paid_b4_15Mar),2)
			
			log.info('Interest_234C_4 '+str(Interest_234C_4))

			interest_234C=round((Interest_234C_1+Interest_234C_2+Interest_234C_3+Interest_234C_4),2)
		
		log.info('Interest_234C '+str(interest_234C))
		# interest_234C_text = ''
		# for tax in tax_paid.objects.all():
		# 	interest_234C_text = count_month(startDate_cond1,end)

		# ========================234C calculation ends===============

		# ========================234F calculation starts=============
		# 234F
		# Calculate month( retun_filing_date )
		# If (Total_Taxable_Income < 500000 & (return_filing_due_date < retun_filing_date < 01-04-2020))
		# Then 
		# Interest_234F = 1000

		# If (Total_Taxable_Income > 500000 & (return_filing_due_date < retun_filing_date < 01-01-2020))
		# Then 
		# Interest_234F = 5000

		# If (Total_Taxable_Income > 500000 & (31-12-2019 < retun_filing_date < 01-04-2020))
		# Then 
		# Interest_234F = 10000

		# ========================234F calculation ends===============
		compare_date1='2020-04-01'
		compare_date1 = datetime.strptime(compare_date1, '%Y-%m-%d').date()

		compare_date2='2020-01-01'
		compare_date2 = datetime.strptime(compare_date2, '%Y-%m-%d').date()

		compare_date3='2019-12-31'
		compare_date3 = datetime.strptime(compare_date3, '%Y-%m-%d').date()


		log.info('Total_Taxable_Income '+str(TI))

		if (TI<500000) and (return_filing_due_date1 < return_filing_date < compare_date1):
			interest_234F = round(1000,2)

		if (TI>500000) and (return_filing_due_date1 < return_filing_date < compare_date2):
			interest_234F = round(5000,2)


		if (TI<500000) and (compare_date3 < return_filing_date < compare_date1):
			interest_234F = round(10000,2)

		log.info('Interest_234F '+str(interest_234F))


		interest_total = interest_234A + interest_234B + interest_234C + interest_234F

		log.info('interest_total '+str(interest_total))

	log.info(' ====================Interest Calculation ends=================')
	log.info(' ====================Balance Tax Calculation starts=================')
	balance_tax_FE=0

	# calling already created ajax through function
	response = requests.post(
	    'http://13.126.16.56/get_26AS_taxes_self_a/',
	    data={'r_id': client_id},
	)

	self_assessment=response.json()
	self_assessment=sum(self_assessment['tax_p'])


	log.info('self_assessment '+str(self_assessment))
	
	# If (SA > Interest_on_BT_Interest_Cal, BT_Interest_Cal - SA + Interest_on_BT_Interest_Cal, BT_Interest_Cal)
	if self_assessment>interest_total:
		balance_tax_FE=balance_tax-self_assessment+interest_total
	else:
		balance_tax_FE=balance_tax

	log.info('balance_tax_FE '+str(balance_tax_FE))
	

	Interest_on_FE=0
	# max(Interest_on_BT_Interest_Cal - SA, 0)
	Interest_on_FE=max((interest_total-self_assessment),0)

	log.info('Interest_on_FE '+str(Interest_on_FE))

	# Balance_Tax_FE + Interest_on_FE
	Tax_payable_FE=0
	Tax_payable_FE=balance_tax_FE+Interest_on_FE

	log.info(' ====================Balance Tax Calculation ends=================')
	result['sal'] = sal
	result['GTI'] = GTI
	result['ICU_CG'] = ICU_CG
	result['TI'] = TI
	result['rebate'] = rebate
	result['surcharge'] = surcharge
	result['Tx'] = Tx
	result['Tx1'] = Tx1
	result['education_cess'] = education_cess
	result['total_adv_tax_paid'] = total_adv_tax_paid
	result['balance_tax'] = balance_tax
	result['balance_tax_FE'] = balance_tax_FE
	result['Interest_on_FE'] = Interest_on_FE
	result['Tax_payable_FE'] = Tax_payable_FE
	result['interest_234A'] = interest_234A
	result['interest_234B'] = interest_234B
	result['interest_234C'] = interest_234C
	result['interest_234F'] = interest_234F
	result['interest_total']=interest_total
	# result['interest_234C_text'] = interest_234C_text
	result['tax_with_cess'] = tax_with_cess
	result['refund'] = refund
	result['total_tds_paid'] = total_tds_paid
	result['ICU_head_sal'] = ICU_head_sal
	result['ICU_house_p'] = ICU_house_p
	result['ICU_other_s'] = ICU_other_s
	# interest_total = value_of_computation['interest_total']
	result['status'] = 'success'
	return result

def cal_income_chargeable_2019(R_ID,client_id):
	result={}
	result['status'] = 'pending'
	age, dob = ('',)*2

	standard_deduction_sal = 40000
	
	sal, ICU_head_sal, r, m, sd, i, ICU_house_p, Interest_Income, Interest_Savings = (0,)*9
	Other_Income, dividend, ICU_other_s, d_80c, d_80d, d_80e, d_80tta, d_80ttb, GTI, TI, Tx = (0,)*11
	Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
	total_adv_tax_paid, balance_tax, refund = (0,)*3
	interest_total, interest_234A, interest_234B, interest_234C,interest_234F = (0,)*5

	for client in Personal_Details.objects.filter(P_Id = client_id):
		if client.dob is None:
			dob = ''
		else:
			dob = client.dob.strftime('%d-%m-%Y')

	if dob == '':
		age = 50
	else:
		age1 = dob.split('-')[2]
		age = 2019 - int(age1)

	log.info(age)
	edu_cess_per = 0.04
	# Income_chargeable under the head Salaries (S)
	calculated_salary = 0
	
	for i in Income.objects.filter(R_Id=R_ID):
		# abs () change - to + for correction only (05apr2019) 
		calculated_salary = float(i.Salary or 0)+float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0) - float(i.Other_allowances or 0)
		# sal += i.Salary
		sal += calculated_salary
		ICU_head_sal += calculated_salary - float(i.Profession_Tax or 0) - float(i.Entertainment_Allowance or 0)

	if ICU_head_sal > 0:
		ICU_head_sal = ICU_head_sal - standard_deduction_sal
	# Income_chargeable under House Property (HP)
	for a in House_Property_Details.objects.filter(R_Id = R_ID):
		r = float(a.Rent_Received or 0)
		m = float(a.Property_Tax or 0)
		i = float(a.Interest_on_Home_loan or 0)
		sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
		less_standard_deduction= (float(30)/100)*sd
		ICU_house_p += sd-float(a.Interest_on_Home_loan)-less_standard_deduction

	# Income_from Other Sources (OS)
	fd, commission = (0,)*2
	for a in Other_Income_Common.objects.filter(R_Id = R_ID):
		Interest_Income = float(a.Other_Interest or 0)
		Interest_Savings = float(a.Interest_Savings or 0)
		Other_Income = float(a.Other_Income or 0)
		fd = float(a.FD or 0)
		commission = float(a.Commission or 0)
	# ICU_other_s = Interest_Income + Interest_Savings + Other_Income
	ICU_other_s = Interest_Income + Interest_Savings + Other_Income + fd + commission

	# agri_income=0
	# for a in Exempt_Income.objects.filter(R_Id = R_ID):
	# 	agri_income = float(a.Gross_Agriculture_Receipts or 0)

	# Deduction
	for a in VI_Deductions.objects.filter(R_Id = R_ID):
		d_80c = float(a.VI_80_C or 0)
		d_80d = float(a.VI_80_D or 0)
		d_80e = float(a.VI_80_E or 0)
		d_80tta = float(a.VI_80_TTA or 0)
		d_80ttb = float(a.VI_80_TTB or 0)

	if d_80c>150000:
		d_80c = 150000
	if d_80d>30000:
		d_80d = 30000
	if d_80tta > 10000:
		d_80tta = 10000
	if d_80ttb > 50000:
		d_80ttb = 50000

	# Capital Gain
	st_shares_tpv, st_shares_tsv, st_shares_gain, lt_shares_gain = (0,)*4
	st_equity_tpv, st_equity_tsv, st_equity_gain, lt_equity_gain = (0,)*4
	lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
	lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3
	st_listed_d_tpv, st_listed_d_tsv, st_listed_d_gain = (0,)*3

	st_debt_MF_gain = 0
	if Shares_ST.objects.filter(R_Id=R_ID).exists():
		Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
		st_shares_gain= Shares_ST_instance.Expenditure
	if Shares_LT.objects.filter(R_Id=R_ID).exists():
		Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
		lt_shares_gain= Shares_LT_instance.Expenditure
	if Equity_ST.objects.filter(R_Id=R_ID).exists():
		Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
		st_equity_gain= Equity_ST_instance.Expenditure
	if Equity_LT.objects.filter(R_Id=R_ID).exists():
		Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
		lt_equity_gain= Equity_LT_instance.Expenditure
	if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
		Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
		lt_debt_MF_gain= Debt_MF_LT_instance.Expenditure
	if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
		Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
		st_debt_MF_gain= Debt_MF_ST_instance.Expenditure

	if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
		Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
		lt_listed_d_gain= Listed_Debentures_LT_instance.Expenditure

	if Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
		Listed_Debentures_ST_instance = Listed_Debentures_ST.objects.get(R_Id=R_ID)
		st_listed_d_gain= Listed_Debentures_ST_instance.Expenditure

	log.info('======capital gains start=====')
	log.info('st_shares_gain '+str(st_shares_gain))
	log.info('lt_shares_gain '+str(lt_shares_gain))

	log.info('st_equity_gain '+str(st_equity_gain))
	log.info('lt_equity_gain '+str(lt_equity_gain))

	log.info('st_debt_MF_gain '+str(st_debt_MF_gain))
	log.info('lt_debt_MF_gain '+str(lt_debt_MF_gain))

	log.info('st_listed_d_gain '+str(st_listed_d_gain))
	log.info('lt_listed_d_gain '+str(lt_listed_d_gain))
	# ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain
	ICU_CG = st_shares_gain + st_equity_gain + st_debt_MF_gain + lt_shares_gain + lt_equity_gain + lt_debt_MF_gain + lt_listed_d_gain

	log.info('Total capital gain '+str(ICU_CG))

	log.info('======capital gains end=====')
	# Gross Total Income (GTI)
	# GTI = ICU_head_sal + ICU_house_p + ICU_other_s
	ITR=determine_ITR(R_ID)
	log.info('ITR '+str(ITR))

	# ----Taxable income (TI) = GTI -80c - 80D - 80E - 80TTA
	# TI = float(GTI - d_80c - d_80d - d_80e - d_80tta)
	total_deduction = 0
	if computation.objects.filter(R_Id = R_ID).exists():
		computation_instance=computation.objects.get(R_Id=R_ID)
		total_deduction = computation_instance.total_deduction or 0

	if ITR==1:
		GTI = ICU_head_sal + ICU_house_p + ICU_other_s + ICU_CG
		TI = GTI - float(total_deduction)

	elif  ITR==2:
		Total_TI=ICU_head_sal + ICU_house_p + ICU_other_s + ICU_CG

		log.info('Income from Salary '+str(ICU_head_sal))
		log.info('Income from HP '+str(ICU_house_p))
		log.info('Income from CG '+str(ICU_CG))
		log.info('Income from Other '+str(ICU_other_s))

		log.info('Total_TI '+str(Total_TI))
		# /////////////////// CG setoff starts////////////////////
		# short term shares + Equity Gain
		ST_CapgainonAssets_Share_Equity=st_shares_gain + st_equity_gain
		D5 = max (0,ST_CapgainonAssets_Share_Equity)

		# short term debt MF + debanture
		ST_CapgainonAssets_Debentures_Debt=st_debt_MF_gain+st_listed_d_gain
		D7 =max(0, ST_CapgainonAssets_Debentures_Debt)

		# H457 max(0, Long Term CapgainonAssets Equity Shares + Debentures + Equity MF's)
		LT_CapgainonAssets_Equity_MF=lt_equity_gain
		LT_CapgainonAssets_Debenture=lt_listed_d_gain
		D9 =max(0,(LT_CapgainonAssets_Equity_MF+LT_CapgainonAssets_Debenture))
		log.info('D9 '+str(D9))

		# H466 max(0, Long Term CapgainonAssets Debt MF's)
		LT_CapgainonAssets_Debt_MF=lt_debt_MF_gain
		D10=max(0, LT_CapgainonAssets_Debt_MF)

		Shares_ST_CG=int(st_shares_gain)
		EMF_ST_CG=int(st_equity_gain)
		E4 = StclSetoff15Per=abs(min(0, Shares_ST_CG) + min(0, EMF_ST_CG))

		DebtMF_ST_CG=int(st_debt_MF_gain)
		Debenture_ST_CG=int(st_listed_d_gain)
		G4 = StclSetoffAppRate=abs(min(0, DebtMF_ST_CG) + min(0, Debenture_ST_CG))
		
		Shares_LT_CG=int(lt_shares_gain)
		EMF_LT_CG=int(lt_equity_gain)
		I4 = LtclSetOff10Per=abs(min(0, Shares_LT_CG) + min(0, EMF_LT_CG) + min(0, Debenture_ST_CG))

		DebtMF_LT_CG=int(lt_debt_MF_gain)
		J4 = LtclSetOff20Per=abs(min(0, DebtMF_LT_CG))
		
		CG_setoffs=get_setoffs(D5,D7,D9,D10,E4,G4,I4,J4)

		log.info('CG_setoffs '+str(CG_setoffs))

		# /////////////////// CG setoff ends////////////////////

		# ///////////// CYLA setoff starts //////////////////
		# max(0, equal to TotIncUnderHeadSalaries)
		E6=max(0, ICU_house_p)
		
		# D5-d
		D5=E4
		d=CG_setoffs['setoff_d']
		InStcg15Per_CurrYrCapGain=D5-d
		E8=InStcg15Per_CurrYrCapGain

		# D7-a
		D7=G4
		a=CG_setoffs['setoff_a']
		InStcgAppRate_CurrYrCapGain=D7-a
		E10=InStcgAppRate_CurrYrCapGain

		# min(0, D9 - (b+e+h))
		D9=I4
		b=CG_setoffs['setoff_b']
		e=CG_setoffs['setoff_e']
		h=CG_setoffs['setoff_h']
		InLtcg10Per_CurrYrCapGain=min(0,D9-(b+e+h))
		E12=InLtcg10Per_CurrYrCapGain
			

		# min(0, D10- (c+f+g))
		D10=J4
		c=CG_setoffs['setoff_c']
		f=CG_setoffs['setoff_f']
		g=CG_setoffs['setoff_g']
		InLtcg20Per_CurrYrCapGain=min(0, D10- (c+f+g))
		E13=InLtcg20Per_CurrYrCapGain

		TotOthSrcNoRaceHorse=0
		E15=max(0, TotOthSrcNoRaceHorse)

		TotHPlossCurYr=abs(min(0,ICU_house_p))

		CYLA_setoffs=get_CYLA_setoff(TotHPlossCurYr,E6,E8,E10,E12,E13,E15)

		currentyrloss=0
		for CYLA_setoff in CYLA_setoffs:
			currentyrloss += CYLA_setoffs[CYLA_setoff]

		log.info('CYLA_setoffs '+str(CYLA_setoffs))

		log.info('Total_TI '+str(Total_TI))

		log.info('currentyrloss '+str(currentyrloss))
		# ///////////// CYLA setoff ends //////////////////

		GTI=Total_TI-currentyrloss

		log.info('Total_TI-currentyrloss '+str(GTI))

		TI = GTI - float(total_deduction)

		log.info('Taxable Income after deducted deductions'+str(TI))
		# 2A
		log.info('spcl_rate15 amount '+str(st_shares_gain + st_equity_gain))

		spcl_rate15 = (st_shares_gain + st_equity_gain) * 0.15
		log.info('spcl_rate15 '+str(spcl_rate15))

		log.info('spcl_rate20 amount '+str(lt_debt_MF_gain))

		spcl_rate20 = (lt_debt_MF_gain) * 0.20
		log.info('spcl_rate20 '+str(spcl_rate20))

		log.info('lt spcl_rate10 amount '+str(lt_equity_gain+lt_shares_gain))

		if (lt_equity_gain+lt_shares_gain) < 100000:
			lt_spcl_rate10=0
		elif (lt_equity_gain+lt_shares_gain) > 100000:
			lt_spcl_rate10 = ((lt_equity_gain+lt_shares_gain)-100000) * 0.10

		log.info('lt_spcl_rate10 '+str(lt_spcl_rate10))

		log.info('spcl_rate10 amount '+str(lt_listed_d_gain))

		spcl_rate10 = (lt_listed_d_gain) * 0.10

		log.info('spcl_rate10 '+str(spcl_rate10))

		Tx1 = spcl_rate15 + spcl_rate10 + spcl_rate20 + lt_spcl_rate10

		log.info('Tx1 '+str(Tx1))
		# TotalIncome - IncChargeableTaxSplRates + NetAgricultureIncomeOrOtherIncomeForRate
		IncChargeableTaxSplRates=st_shares_gain + st_equity_gain + lt_shares_gain + lt_equity_gain + lt_debt_MF_gain + lt_listed_d_gain
		NetAgricultureIncomeOrOtherIncomeForRate=0
		TI=TI-IncChargeableTaxSplRates+NetAgricultureIncomeOrOtherIncomeForRate
		log.info('Total Taxable Income after adding CG special rates '+str(TI))
	# Also add Income chargable under business and profession

	if age < 60:
		log.info('Not Senior Citizen')
		if ( TI>1000000):
			Tx = ( (TI-1000000)*0.3 ) + 100000 + 12500
		if ( TI>500000 ):
			if TI<1000000:
				Tx = ( (TI-500000)*0.2 ) + 12500
		if ( TI>250000 ):
			if TI<500000:
				Tx = ( (TI-250000)*0.05 )
		if ( TI<250000 ):
			Tx = 0
	else:
		log.info('Senior Citizen')
		if ( TI>1000000):
			Tx = ( (TI-1000000)*0.3 ) + 100000 + 10000
		if ( TI>500000 ):
			if TI<1000000:
				Tx = ( (TI-500000)*0.2 ) + 10000
		if ( TI>300000 ):
			if TI<500000:
				Tx = ( (TI-300000)*0.05 )
		if ( TI<300000 ):
			Tx = 0

	
	if TI < 350000 :
		rebate = 2500
	if TI <= 300000:
		rebate = Tx

	# Surcharge(SH)
	if TI>5000000 and TI<10000000:
		surcharge = (Tx + Tx1)*0.1
	if TI>10000000:
		surcharge = (Tx + Tx1)*0.15

	education_cess = (Tx + Tx1 - rebate)*edu_cess_per
	tax_with_cess = (round(Tx) + round(Tx1) - round(rebate) + round(education_cess))

	PAN = ''
	if Personal_Details.objects.filter(P_Id = client_id).exists():
		for client in Personal_Details.objects.filter(P_Id = client_id):
			PAN = client.pan

	for tds in tds_tcs.objects.filter(R_Id = R_ID,year='2019'):
		total_tds_paid += float(tds.tds_tcs_deposited or 0)

	for tax in tax_paid.objects.filter(R_Id = R_ID):
		total_adv_tax_paid += float(tax.total_tax or 0)

	if (Tx == rebate):
		education_cess = 0
		tax_with_cess = 0

	# calling already created ajax through function
	response = requests.post(
	    'http://13.126.16.56/get_26AS_taxes_self_a/',
	    data={'r_id': client_id},
	)

	self_assessment=response.json()
	self_assessment=sum(self_assessment['tax_p'])

	total_adv_tax_paid=total_adv_tax_paid-float(self_assessment)
	balance_tax = tax_with_cess - total_tds_paid - total_adv_tax_paid
	if balance_tax<0:
		refund = balance_tax

	log.info(' ====================Interest Calculation starts=================')
	# =========================234A calculation starts===============
	# 234A
	# If retun_filing_date <= return_filing_due_date then exit
	# Else
	# Interest_234A = 1% * Tax_Payable * [month( retun_filing_date ) - month( return_filing_due_date )]
	# log.info(get_financial_year('06-15'))
	# tax_payable_amount=tax_with_cess - total_tds_paid
	tax_payable_amount=tax_with_cess
	log.info('tax_payable_amount '+str(tax_payable_amount))
	if(tax_payable_amount>=0):

		now = datetime.now()
		current_date = now.strftime("%Y-%m-%d")

		return_filing_date = datetime.strptime(current_date, '%Y-%m-%d').date()
		return_filing_year=now.strftime("%Y")
		return_filing_month=now.strftime("%m")
		log.info('current_date '+str(current_date))

		# return_filing_due_date = '2019-7-31'
		return_filing_due_date1 = datetime.strptime(return_filing_due_date, '%Y-%m-%d').date()
		return_filing_due_year =return_filing_due_date1.strftime("%Y")
		return_filing_due_month =return_filing_due_date1.strftime("%m")
		log.info('return_filing_due_date '+str(return_filing_due_date1))

		interest_234A_rate=0.01

		if return_filing_date<=return_filing_due_date1:
			pass
		else:
			log.info('return_filing_month '+return_filing_month)
			log.info('return_filing_due_month '+return_filing_due_month)
			month_diff=diff_month(return_filing_year,return_filing_due_year,return_filing_month,return_filing_due_month)
			log.info('month_diff '+str(month_diff))
			interest_234A = round((tax_payable_amount*interest_234A_rate)*month_diff,2)


		log.info('interest_234A '+str(interest_234A))

		# ========================234A calculation ends=================

		# ========================234B calculation starts===============
		# 234B
		# If ((Tax_Payable - Total_TDS_paid) > 10000 & Advance_Tax_Paid = 0)
		# Then
		# Interest_234B = 1% * (Tax_Payable -Total_TDS_paid ) * (month(retun_filing_date) - 4)

		# Else If (Advance_Tax_Paid < 90% * (Tax_Payable - Total_TDS_paid))
		# Then
		# Interest_234B = 1% * (Tax_Payable -Total_TDS_paid - Advance_Tax_Paid) *  (month(retun_filing_date) - 4)
		if age <60 and (tax_payable_amount - total_tds_paid) > 10000:
			interest_234B_rate=0.01
			log.info('total_tds_paid '+str(total_tds_paid))
			log.info('tax_payable_amount - total_tds_paid '+str(tax_payable_amount - total_tds_paid))
			log.info('total_adv_tax_paid '+str(total_adv_tax_paid))


			if (tax_payable_amount - total_tds_paid) >10000 and total_adv_tax_paid==0:
				log.info('interest_234B first condition')
				log.info(((tax_payable_amount-total_tds_paid)*interest_234B_rate))
				log.info(int(return_filing_month)-3)
				interest_234B = round(((tax_payable_amount-total_tds_paid)*interest_234B_rate)*float(int(return_filing_month)-3) ,2)
			
			elif total_adv_tax_paid < (tax_payable_amount - total_tds_paid)*0.9:
				log.info('interest_234B second condition')
				interest_234B = round(((tax_payable_amount-total_tds_paid-total_adv_tax_paid)*interest_234B_rate)*float(int(return_filing_month)-3) ,2)
			
		log.info('interest_234B '+str(interest_234B))

		# ========================234B calculation ends===============

		# ========================234C calculation starts===============
		# 	234C
		# Calculate
		# Advance_Tax_paid_b4_15Jun
		# Advance_Tax_paid_b4_15Sep
		# Advance_Tax_paid_b4_15Dec
		# Advance_Tax_paid_b4_15Mar
		# Amount = Tax_Payable - Total_TDS_paid

		# If (Advance_Tax_paid_b4_15Jun < 15% * Amount) 
		# Then
		# Interest_234C_1 = 1% * 3 * (15% * Amount - Advance_Tax_paid_b4_15Jun)

		# If (Advance_Tax_paid_b4_15Sep < 45% * Amount) 
		# Then
		# Interest_234C_2 = 1% * 3 * (45% * Amount - Advance_Tax_paid_b4_15Sep)

		# If (Advance_Tax_paid_b4_15Dec < 75% * Amount) 
		# Then
		# Interest_234C_3 = 1% * 3 * (75% * Amount - Advance_Tax_paid_b4_15Dec)

		# If (Advance_Tax_paid_b4_15Mar < 75% * Amount) 
		# Then
		# Interest_234C_4 = 1% * 1 * (100% * Amount - Advance_Tax_paid_b4_15Mar)

		# Interest_234C = Interest_234C_1 + Interest_234C_2 + Interest_234C_3 + Interest_234C_4
		Amount = tax_payable_amount - total_tds_paid
		if age <60 and Amount > 10000:
			start_cond1 = '2018-6-15'
			startDate_cond1 = datetime.strptime(start_cond1, '%Y-%m-%d').date()
			start_cond2 = '2018-9-15'
			startDate_cond2 = datetime.strptime(start_cond2, '%Y-%m-%d').date()
			start_cond3 = '2018-12-15'
			startDate_cond3 = datetime.strptime(start_cond3, '%Y-%m-%d').date()
			start_cond4 = '2019-3-15'
			startDate_cond4 = datetime.strptime(start_cond4, '%Y-%m-%d').date()

			log.info('Amount '+str(Amount))

			financial_start_date='2018-04-01'
			financial_start_date = datetime.strptime(financial_start_date, '%Y-%m-%d').date()

			financial_end_date='2019-03-31'
			financial_end_date = datetime.strptime(financial_end_date, '%Y-%m-%d').date()

			Advance_Tax_paid_b4_15Jun,Advance_Tax_paid_b4_15Sep,Advance_Tax_paid_b4_15Dec,Advance_Tax_paid_b4_15Mar=(0,)*4
			Interest_234C_1,Interest_234C_2,Interest_234C_3,Interest_234C_4=(0,)*4

			Tax_paid_b4_15Jun_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond1,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Jun_data.exists():
				log.info
				for data in Tax_paid_b4_15Jun_data:
					Advance_Tax_paid_b4_15Jun=Advance_Tax_paid_b4_15Jun+data.total_tax

			log.info('Advance_Tax_paid_b4_15Jun '+str(Advance_Tax_paid_b4_15Jun))

			Tax_paid_b4_15Sep_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond2,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Sep_data.exists():
				for data in Tax_paid_b4_15Sep_data:
					Advance_Tax_paid_b4_15Sep=Advance_Tax_paid_b4_15Sep+data.total_tax

			log.info('Advance_Tax_paid_b4_15Sep '+str(Advance_Tax_paid_b4_15Sep))

			Tax_paid_b4_15Dec_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond3,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Dec_data.exists():
				for data in Tax_paid_b4_15Dec_data:
					Advance_Tax_paid_b4_15Dec=Advance_Tax_paid_b4_15Dec+data.total_tax

			log.info('Advance_Tax_paid_b4_15Dec '+str(Advance_Tax_paid_b4_15Dec))

			Tax_paid_b4_15Mar_data=tax_paid.objects.filter(R_Id = R_ID,deposit_date__lte=startDate_cond4,deposit_date__range=(financial_start_date, financial_end_date))
			if Tax_paid_b4_15Mar_data.exists():
				for data in Tax_paid_b4_15Mar_data:
					Advance_Tax_paid_b4_15Mar=Advance_Tax_paid_b4_15Mar+data.total_tax

			log.info('Advance_Tax_paid_b4_15Mar '+str(Advance_Tax_paid_b4_15Mar))

			log.info('15 percent of amount '+str(0.15*Amount))
			if(Advance_Tax_paid_b4_15Jun<(0.15*Amount)):
				Interest_234C_1 = round(0.01 * 3 * (0.15 * Amount - Advance_Tax_paid_b4_15Jun),2)

			log.info('Interest_234C_1 '+str(Interest_234C_1))

			log.info('45 percent of amount '+str(0.45*Amount))
			if(Advance_Tax_paid_b4_15Sep<(0.45*Amount)):
				Interest_234C_2 = round(0.01 * 3 * (0.45 * Amount - Advance_Tax_paid_b4_15Sep),2)
			
			log.info('Interest_234C_2 '+str(Interest_234C_2))

			log.info('75 percent of amount '+str(0.75*Amount))
			if(Advance_Tax_paid_b4_15Dec<(0.75*Amount)):
				Interest_234C_3 = round(0.01 * 3 * (0.75 * Amount - Advance_Tax_paid_b4_15Dec),2)

			log.info('Interest_234C_3 '+str(Interest_234C_3))

			if(Advance_Tax_paid_b4_15Mar<(0.75*Amount)):
				Interest_234C_4 = round(0.01 *1 * (1 * Amount - Advance_Tax_paid_b4_15Mar),2)
			
			log.info('Interest_234C_4 '+str(Interest_234C_4))

			interest_234C=round((Interest_234C_1+Interest_234C_2+Interest_234C_3+Interest_234C_4),2)
		
		log.info('Interest_234C '+str(interest_234C))
		# interest_234C_text = ''
		# for tax in tax_paid.objects.all():
		# 	interest_234C_text = count_month(startDate_cond1,end)

		# ========================234C calculation ends===============

		# ========================234F calculation starts=============
		# 234F
		# Calculate month( retun_filing_date )
		# If (Total_Taxable_Income < 500000 & (return_filing_due_date < retun_filing_date < 01-04-2020))
		# Then 
		# Interest_234F = 1000

		# If (Total_Taxable_Income > 500000 & (return_filing_due_date < retun_filing_date < 01-01-2020))
		# Then 
		# Interest_234F = 5000

		# If (Total_Taxable_Income > 500000 & (31-12-2019 < retun_filing_date < 01-04-2020))
		# Then 
		# Interest_234F = 10000

		# ========================234F calculation ends===============
		compare_date1='2020-04-01'
		compare_date1 = datetime.strptime(compare_date1, '%Y-%m-%d').date()

		compare_date2='2020-01-01'
		compare_date2 = datetime.strptime(compare_date2, '%Y-%m-%d').date()

		compare_date3='2019-12-31'
		compare_date3 = datetime.strptime(compare_date3, '%Y-%m-%d').date()


		log.info('Total_Taxable_Income '+str(TI))

		if (TI<500000) and (return_filing_due_date1 < return_filing_date < compare_date1):
			interest_234F = round(1000,2)

		if (TI>500000) and (return_filing_due_date1 < return_filing_date < compare_date2):
			interest_234F = round(5000,2)


		if (TI<500000) and (compare_date3 < return_filing_date < compare_date1):
			interest_234F = round(10000,2)

		log.info('Interest_234F '+str(interest_234F))


		interest_total = interest_234A + interest_234B + interest_234C + interest_234F

		log.info('interest_total '+str(interest_total))

	log.info(' ====================Interest Calculation ends=================')

	log.info(' ====================Balance Tax Calculation starts=================')
	balance_tax_FE=0

	log.info('self_assessment '+str(self_assessment))

	log.info('BT_Interest_Cal '+str(balance_tax))

	log.info('interest_total '+str(interest_total))
	
	# If (SA > Interest_on_BT_Interest_Cal, BT_Interest_Cal - SA + Interest_on_BT_Interest_Cal, BT_Interest_Cal)
	if self_assessment>interest_total:
		balance_tax_FE=balance_tax-self_assessment+interest_total
	else:
		balance_tax_FE=balance_tax

	log.info('balance_tax_FE '+str(balance_tax_FE))
	

	Interest_on_FE=0
	# max(Interest_on_BT_Interest_Cal - SA, 0)
	Interest_on_FE=max((interest_total-self_assessment),0)

	log.info('Interest_on_FE '+str(Interest_on_FE))

	# Balance_Tax_FE + Interest_on_FE
	Tax_payable_FE=0
	Tax_payable_FE=balance_tax_FE+Interest_on_FE

	log.info('Tax_payable_FE '+str(Tax_payable_FE))

	log.info(' ====================Balance Tax Calculation ends=================')
	
	result['sal'] = sal
	result['GTI'] = GTI
	result['ICU_CG'] = ICU_CG
	result['TI'] = TI
	result['rebate'] = rebate
	result['surcharge'] = surcharge
	result['Tx'] = Tx
	result['Tx1'] = Tx1
	result['education_cess'] = education_cess
	result['total_adv_tax_paid'] = total_adv_tax_paid
	result['balance_tax'] = balance_tax
	result['balance_tax_FE'] = balance_tax_FE
	result['Interest_on_FE'] = Interest_on_FE
	result['Tax_payable_FE'] = Tax_payable_FE
	result['interest_234A'] = interest_234A
	result['interest_234B'] = interest_234B
	result['interest_234C'] = interest_234C
	result['interest_234F'] = interest_234F
	result['interest_total']=interest_total
	# result['interest_234C_text'] = interest_234C_text
	result['tax_with_cess'] = tax_with_cess
	result['refund'] = refund
	result['total_tds_paid'] = total_tds_paid
	result['ICU_head_sal'] = ICU_head_sal
	result['ICU_house_p'] = ICU_house_p
	result['ICU_other_s'] = ICU_other_s
	# interest_total = value_of_computation['interest_total']
	result['status'] = 'success'
	return result

#function take input of the datestring like 2017-05-01
def get_financial_year(datestring):
	try:
		log.info(datestring)
		date = datetime.strptime(datestring, "%m-%d").date()
		#initialize the current year
		year_of_date=date.year
		#initialize the current financial year start date
		financial_year_start_date = datetime.strptime("04-01","%m-%d").date()
		if date<financial_year_start_date:
			log.info('if')
			return str(current_year()-1)
		else:
			log.info('else')
			return str(current_year())
	except Exception as ex:
		log.info('Error in getting finacial year '+traceback.format_exc())


def diff_month(d1_year, d2_year,d1_month,d2_month):
	return (int(d1_year) - int(d2_year)) * 12 + int(d1_month) - int(d2_month)

def pay_direct(client_id):
	url = 'https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp'

	# from pyvirtualdisplay import Display 
	# display = Display(visible=0, size=(1024, 768)) 
	# display.start() 

	# chrome_options = webdriver.ChromeOptions()
	# chrome_options.add_argument('--headless')
	# chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors. 
	# driver = webdriver.Chrome(executable_path=chrome_executable_path, 
		# service_args=['--verbose', '--log-path=/tmp/chromedriver.log'])

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--no-sandbox')
	# chrome_options.add_argument('--window-size=1420,1080')
	# chrome_options.add_argument('--headless')
	# chrome_options.add_argument('--disable-gpu')
	driver1 = webdriver.Chrome(chrome_options=chrome_options)
	driver1.get(url)
	time.sleep(2)
	challan_280 = driver1.find_elements_by_xpath('//*[@id="selectform"]/div[1]/div[2]/section/div/div[1]/div/div/a')[0]
	driver1.execute_script("arguments[0].click();", challan_280)
	time.sleep(2)

	# driver1.find_elements_by_id("input[type='radio'][value='0021']")[0].click()
	tax_applicable = driver1.find_elements_by_xpath('//*[@id="0021"]')[0]
	driver1.execute_script("arguments[0].click();", tax_applicable)
	type_of_payment = driver1.find_elements_by_xpath('//*[@id="300"]')[0]
	driver1.execute_script("arguments[0].click();", type_of_payment)
	# //*[@id="0021"]
	# //*[@id="div_pan_error"]/div/input
	# //*[@id="div_assessment_error"]/div/select

	pi_arr = get_itr_varibles(client_id,'personal_information')
	PAN = pi_arr['PAN']
	mobile = pi_arr['mobile']

	pan_input = driver1.find_elements_by_xpath('//*[@id="div_pan_error"]/div/input')[0]
	pan_input.send_keys(PAN)

	address_arr = get_itr_varibles(client_id,'address_detail')
	ResidenceNo = address_arr['ResidenceNo']
	ResidenceName = address_arr['ResidenceName']
	RoadOrStreet = address_arr['RoadOrStreet']
	LocalityOrArea = address_arr['LocalityOrArea']
	city = address_arr['city']
	state = address_arr['state']
	PinCode = address_arr['PinCode']

	return_detail_arr = get_itr_varibles(client_id,'return_detail')
	AY = return_detail_arr['AY']
	AY = AY.replace("-20", "-")

	select = Select(driver1.find_elements_by_xpath('//*[@id="div_assessment_error"]/div/select')[0])
	select.select_by_visible_text(AY)

	flat_door_block = driver1.find_elements_by_xpath('//*[@id="div_add_line1_error"]/div/input')[0]
	flat_door_block.send_keys(ResidenceNo)
	premises = driver1.find_elements_by_xpath('//*[@id="div_add_line2_error"]/div/input')[0]
	premises.send_keys(ResidenceName)
	road_street = driver1.find_elements_by_xpath('//*[@id="div_add_line3_error"]/div/input')[0]
	road_street.send_keys(RoadOrStreet)
	area_locality = driver1.find_elements_by_xpath('//*[@id="div_add_line4_error"]/div/input')[0]
	area_locality.send_keys(LocalityOrArea)
	city_input = driver1.find_elements_by_xpath('//*[@id="div_add_line5_error"]/div/input')[0]
	city_input.send_keys(city)

	select = Select(driver1.find_elements_by_xpath('//*[@id="div_state_error"]/div/select')[0])
	select.select_by_value(state)

	pincode = driver1.find_elements_by_xpath('//*[@id="Add_PIN"]')[0]
	pincode.send_keys(PinCode)

	email = ''
	for a in SalatTaxUser.objects.filter(id=client_id):
		email = a.email

	email_input = driver1.find_elements_by_xpath('//*[@id="div_email_error"]/div/input')[0]
	email_input.send_keys(email)
	mobile_input = driver1.find_elements_by_xpath('//*[@id="div_mobile_error"]/div/input')[0]
	mobile_input.send_keys(mobile)

	time.sleep(1)

	return 'success'
	#  *************************     previous implementation     *************************
	# driver.get("https://www.incometaxindiaefiling.gov.in/home")

	# driver.find_element_by_class_name('ui-icon-closethick').click()
	# e_pay = driver.find_element_by_xpath('//*[@id="ltcol_services_R"]/ul/app-links[12]/li/a[1]')
	# driver.execute_script("arguments[0].click();", e_pay)

	# epay_button = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div/button[1]')
	# driver.execute_script("arguments[0].click();", epay_button)

	# time.sleep(1)
	# driver.switch_to_window(driver.window_handles[1])
	# time.sleep(1)

	# challan_281 = driver.find_element_by_xpath('//*[@id="281"]/td[1]/a')
	# driver.execute_script("arguments[0].click();", challan_281)

@csrf_exempt
def pay_to_IT(request):
	result={}
	if request.method=='POST':
		try:
			client_id=request.POST.get('client_id')

			# chromedriver = "/usr/local/bin/chromedriver"
			# os.environ["webdriver.chrome.driver"] = chromedriver
			# driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")

			# "Error : Message: unknown error: Chrome failed to start: exited abnormally
			# (Driver info: chromedriver=2.35.528139 (47ead77cb35ad2a9a83248b292151462a66cd881),platform=Linux 4.4.0-1081-aws x86_64)

			pay_IT = pay_direct(client_id)

			result['status']= 'success'
			result['pay_IT'] = pay_IT
		except Exception as ex:
			result['status']= 'Error : %s' % ex
			log.error('Error in pay_to_IT : '+traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		try:
			for client in Personal_Details.objects.exclude(P_Id = 1):
				log.info(client.P_Id)
				log.info(excel_db(client.pan,client.P_Id))
			result['status'] = excel_db('BRHPB8482J',19)
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')

def xml_content(client_id,request,software_id):
	R_ID = get_rid(client_id)
	log.info(R_ID)

	AssessmentYear = 0
	if Return_Details.objects.filter(R_id=R_ID).exists():
		Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
		AssessmentYear = Return_Details_instance.AY
		AssessmentYear = AssessmentYear.split('-')[0]
	# AssessmentYear = now.strftime("%Y")
	# content = ''
	c = '<?xml version="1.0" encoding="ISO-8859-1"?>\n'
	c += '<ITRETURN:ITR xmlns:ITRETURN="http://incometaxindiaefiling.gov.in/main" xmlns:ITR1FORM="http://incometaxindiaefiling.gov.in/ITR1" xmlns:ITRForm="http://incometaxindiaefiling.gov.in/master" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'
	c += '<ITR1FORM:ITR1>\n'

	now = datetime.now()
	CreationDate = now.strftime("%Y-%m-%d")

	c += '<ITRForm:CreationInfo>\n'
	c += '<ITRForm:SWVersionNo>R1</ITRForm:SWVersionNo>\n'
	c += '<ITRForm:SWCreatedBy>'+software_id+'</ITRForm:SWCreatedBy>\n'
	c += '<ITRForm:XMLCreatedBy>'+software_id+'</ITRForm:XMLCreatedBy>\n'
	c += '<ITRForm:XMLCreationDate>'+CreationDate+'</ITRForm:XMLCreationDate>\n'
	c += '<ITRForm:IntermediaryCity>Delhi</ITRForm:IntermediaryCity>\n'
	c += '<ITRForm:Digest>-</ITRForm:Digest>\n'
	c += '</ITRForm:CreationInfo>\n'

	# AssessmentYear = now.strftime("%Y")
	c += '<ITRForm:Form_ITR1>\n'
	c += '<ITRForm:FormName>ITR-1</ITRForm:FormName>\n'
	# Description ??
	c += '<ITRForm:Description>For Indls having Income from Salary, Pension, family pension and Interest</ITRForm:Description>\n'
	c += '<ITRForm:AssessmentYear>'+AssessmentYear+'</ITRForm:AssessmentYear>\n'
	c += '<ITRForm:SchemaVer>Ver1.1</ITRForm:SchemaVer>\n'
	c += '<ITRForm:FormVer>Ver1.0</ITRForm:FormVer>\n'
	c += '</ITRForm:Form_ITR1>\n'

	fname, mname, lname, PAN, dob, address, mobile, aadhar_no = ('',)*8

	pi_arr = get_itr_varibles(client_id,'personal_information')
	fname = pi_arr['fname']
	mname = pi_arr['mname']
	lname = pi_arr['lname']
	PAN = pi_arr['PAN']
	dob = pi_arr['dob']
	mobile = pi_arr['mobile']
	aadhar_no = pi_arr['aadhar_no']

	# for client in Personal_Details.objects.filter(P_Id = client_id):
		# fname = client.name.split('|')[0]
		# mname = client.name.split('|')[1]
		# lname = client.name.split('|')[2]
		# PAN = client.pan
		# temp_date = json_serial(client.dob)
		# dob = temp_date.split('T')[0]
		# mobile = client.mobile
		# aadhar_no = client.aadhar_no or ''

	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	ResidenceNo, ResidenceName, RoadOrStreet, LocalityOrArea, city = ('',)*5
	state, PinCode, Employer_category, residential_status, email, Add_statecode = ('',)*6

	bank_arr = get_itr_varibles(client_id,'bank_detail')
	no_of_bank = bank_arr['no_of_bank']
	bank_name = bank_arr['bank_name']
	IFSC = bank_arr['IFSC']
	acc_no = bank_arr['acc_no']
	acc_type = bank_arr['acc_type']
	# no_of_bank = Bank_Details.objects.filter(P_id=personal_instance).count()
	state_list = State_Code.objects.all()
	
	address_arr = get_itr_varibles(client_id,'address_detail')
	ResidenceNo = address_arr['ResidenceNo']
	ResidenceName = address_arr['ResidenceName']
	RoadOrStreet = address_arr['RoadOrStreet']
	LocalityOrArea = address_arr['LocalityOrArea']
	city = address_arr['city']
	state = address_arr['state']
	PinCode = address_arr['PinCode']
	Add_statecode = address_arr['Add_statecode']
	Employer_category = address_arr['Employer_category']
	residential_status = address_arr['residential_status']

	HP_i, Rent_Received, municipal_tax, ICU_house_p = (0,)*4
	TypeOfHP = ''
	for a in House_Property_Details.objects.filter(R_Id = R_ID):
		HP_i += float(a.Interest_on_Home_loan or 0)
		Rent_Received += a.Rent_Received
		municipal_tax += a.Property_Tax
		if a.Type_of_Hosue_Property!='self_occupied':
			TypeOfHP = 'L'
		else:
			TypeOfHP = 'S'
		sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
		ICU_house_p += abs(a.Rent_Received)-abs(a.Property_Tax)-abs(sd)-abs(a.Interest_on_Home_loan or 0)
	if HP_i>200000:
		HP_i = 200000
	for a in SalatTaxUser.objects.filter(id=client_id):
		email = a.email

	c += '<ITRForm:PersonalInfo>\n'
	c += '<ITRForm:AssesseeName>\n'
	c += '<ITRForm:FirstName>'+fname+'</ITRForm:FirstName>\n'
	if mname != '':
		c += '<ITRForm:MiddleName>'+mname+'</ITRForm:MiddleName>\n'
	c += '<ITRForm:SurNameOrOrgName>'+lname+'</ITRForm:SurNameOrOrgName>\n'
	c += '</ITRForm:AssesseeName>\n'
	c += '<ITRForm:PAN>'+PAN+'</ITRForm:PAN>\n'
	c += '<ITRForm:Address>\n'
	c += '<ITRForm:ResidenceNo>'+ResidenceNo+'</ITRForm:ResidenceNo>\n'
	c += '<ITRForm:ResidenceName>'+ResidenceName+'</ITRForm:ResidenceName>\n'
	if RoadOrStreet!='':
		c += '<ITRForm:RoadOrStreet>'+RoadOrStreet+'</ITRForm:RoadOrStreet>\n'
	c += '<ITRForm:LocalityOrArea>'+LocalityOrArea+'</ITRForm:LocalityOrArea>\n'
	c += '<ITRForm:CityOrTownOrDistrict>'+city+'</ITRForm:CityOrTownOrDistrict>\n'
	c += '<ITRForm:StateCode>'+str(Add_statecode)+'</ITRForm:StateCode>\n'
	c += '<ITRForm:PinCode>'+PinCode+'</ITRForm:PinCode>\n'
	c += '<ITRForm:CountryCodeMobile>91</ITRForm:CountryCodeMobile>\n'
	c += '<ITRForm:MobileNo>'+mobile+'</ITRForm:MobileNo>\n'
	# c += '<ITRForm:EmailAddress>'+email+'</ITRForm:EmailAddress>\n'
	c += '<ITRForm:EmailAddress>swapnil.bagmar@gmail.com</ITRForm:EmailAddress>\n'
	c += '</ITRForm:Address>\n'
	c += '<ITRForm:DOB>'+dob+'</ITRForm:DOB>\n'
	if Employer_category=='Other' or  Employer_category=='' :
		c += '<ITRForm:EmployerCategory>OTH</ITRForm:EmployerCategory>\n'
	else:
		c += '<ITRForm:EmployerCategory>'+Employer_category+'</ITRForm:EmployerCategory>\n'
	if aadhar_no != '':
		c += '<ITRForm:AadhaarCardNo>'+str(aadhar_no)+'</ITRForm:AadhaarCardNo>\n'
	c += '</ITRForm:PersonalInfo>\n'

	pan = PAN
	
	sal, ICU_head_sal, r, m, sd, i, Interest_Income, Interest_Savings = (0,)*8
	Other_Income, dividend, ICU_other_s, d_80c, d_80d, d_80e, d_80tta, GTI, TI, Tx = (0,)*10
	Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
	total_adv_tax_paid, balance_tax = (0,)*2
	salary, PerquisitesValue, ProfitsInSalary, DeductionUs16 = (0,)*4
	total_deduction,taxable_income = (0,)*2
	
	for a in Other_Income_Common.objects.filter(R_Id = R_ID):
		ICU_other_s = float(a.Other_Interest or 0) + float(a.Interest_Savings or 0) + float(a.Other_Income or 0)
	
	# Income chargeable under the head Salaries (S)
	calculated_salary = 0
	income_arr = get_itr_varibles(client_id,'income_detail')
	calculated_salary = income_arr['calculated_salary']
	PerquisitesValue = income_arr['PerquisitesValue']
	ProfitsInSalary = income_arr['ProfitsInSalary']
	DeductionUs16 = income_arr['DeductionUs16']
	sal = income_arr['sal']
	ICU_head_sal = income_arr['ICU_head_sal']
	# for i in Income.objects.filter(R_Id=R_ID):
		# calculated_salary = float(i.Salary or 0)+abs(float(i.Value_of_perquisites or 0)+float(i.Profits_in_lieu_of_salary or 0))-abs(float(i.Form16_HRA or 0)+float(i.LTA or 0) + float(i.Other_allowances or 0) + float(i.Profession_Tax or 0) + float(i.Entertainment_Allowance or 0) )
		# PerquisitesValue += float(i.Value_of_perquisites or 0)
		# ProfitsInSalary += float(i.Profits_in_lieu_of_salary or 0)
		# DeductionUs16 += float(i.Profession_Tax or 0)+float(i.Entertainment_Allowance or 0)
		# # sal += float(i.Salary or 0)
		# sal += float(i.Salary or 0)-abs(float(i.Form16_HRA or 0)-float(i.LTA or 0) - float(i.Other_allowances or 0) )
		# ICU_head_sal += calculated_salary

	ttp ,rebate ,educess ,gtl, IT_normal_rates  = (0,)*5
	computation_arr = get_itr_varibles(client_id,'computation')
	# for c in computation.objects.filter(R_Id = R_ID):
		# salary = float(c.salary_income or 0)
		# GTI = float(c.gross_total_income or 0)
		# total_deduction = float(c.total_deduction or 0)
		# IT_normal_rates = float(c.IT_normal_rates or 0)

		# ttp = float(c.taxable_income or 0)
		# rebate = float(c.rebate or 0)
		# educess = float(c.add_education_cess or 0)
	ttp = computation_arr['ttp']
	rebate = computation_arr['rebate']
	educess = computation_arr['educess']
	gtl = computation_arr['gtl']
	salary = computation_arr['salary']
	GTI = computation_arr['GTI']
	total_deduction = computation_arr['total_deduction']
	taxable_income = computation_arr['taxable_income']
	IT_normal_rates = computation_arr['Tx']
	Tx1 = computation_arr['Tx1']
	surcharge = computation_arr['surcharge']

	interest_234A = computation_arr['interest_234A']
	interest_234B = computation_arr['interest_234B']
	interest_234C = computation_arr['interest_234C']
	interest_234F = computation_arr['interest_234F']
	total_interest = computation_arr['total_interest']
	
	log.info('ITR-1 : '+pan)
	return_detail_arr = get_itr_varibles(client_id,'return_detail')
	ReturnFileSec1 = return_detail_arr['ReturnFileSec1']
	ReturnType1 = return_detail_arr['ReturnType1']
	ReceiptNo1 = return_detail_arr['ReceiptNo1']
	OrigRetFiledDate1 = return_detail_arr['OrigRetFiledDate1']

	c += '<ITRForm:FilingStatus>\n'
	c += '<ITRForm:ReturnFileSec>'+str(ReturnFileSec1)+'</ITRForm:ReturnFileSec>\n'
	c += '<ITRForm:ReturnType>'+str(ReturnType1)+'</ITRForm:ReturnType>\n'
	c += '<ITRForm:ReceiptNo>'+ReceiptNo1+'</ITRForm:ReceiptNo>\n'
	c += '<ITRForm:OrigRetFiledDate>'+OrigRetFiledDate1+'</ITRForm:OrigRetFiledDate>\n'
	
	c += '<ITRForm:PortugeseCC5A>N</ITRForm:PortugeseCC5A>\n'
	c += '</ITRForm:FilingStatus>\n'

	c += '<ITRForm:ITR1_IncomeDeductions>\n'
	c += '<ITRForm:Salary>'+str(int(sal) )+'</ITRForm:Salary>\n'
	c += '<ITRForm:AlwnsNotExempt>0</ITRForm:AlwnsNotExempt>\n'
	c += '<ITRForm:PerquisitesValue>'+str(int(PerquisitesValue) )+'</ITRForm:PerquisitesValue>\n'
	c += '<ITRForm:ProfitsInSalary>'+str(int(ProfitsInSalary) )+'</ITRForm:ProfitsInSalary>\n'
	c += '<ITRForm:DeductionUs16>'+str(int(DeductionUs16) )+'</ITRForm:DeductionUs16>\n'
	c += '<ITRForm:IncomeFromSal>'+str(int(ICU_head_sal) )+'</ITRForm:IncomeFromSal>\n'
	if TypeOfHP!='':
		c += '<ITRForm:TypeOfHP>'+TypeOfHP+'</ITRForm:TypeOfHP>\n'
		c += '<ITRForm:GrossRentReceived>'+str(int(Rent_Received))+'</ITRForm:GrossRentReceived>\n'
	# c += '<ITRForm:TaxPaidlocalAuth>'+str(int(municipal_tax))+'</ITRForm:TaxPaidlocalAuth>\n'
	c += '<ITRForm:AnnualValue>'+str(int(Rent_Received-municipal_tax))+'</ITRForm:AnnualValue>\n'
	c += '<ITRForm:StandardDeduction>'+str(int( 0.3 * (Rent_Received-municipal_tax) ))+'</ITRForm:StandardDeduction>\n'
	# c += '<ITRForm:InterestPayable>'+str(int(abs(HP_i) ))+'</ITRForm:InterestPayable>\n'
	c += '<ITRForm:TotalIncomeOfHP>'+str(int(ICU_house_p) )+'</ITRForm:TotalIncomeOfHP>\n'
	c += '<ITRForm:IncomeOthSrc>'+str(int(ICU_other_s) )+'</ITRForm:IncomeOthSrc>\n'
	c += '<ITRForm:GrossTotIncome>'+str(int(GTI) )+'</ITRForm:GrossTotIncome>\n'

	d_80c ,d_80ccc ,d_80ccd ,d_80g ,d_80dd ,d_80u ,d_80d ,d_80e ,d_80tta ,d_80ddb ,d_80ee = (0,)*11
	d_80gg ,d_80gga ,d_80ggc ,d_80rrb ,d_80qqb ,d_80ccg, d_80ccd1 = (0,)*7

	deduction_arr = get_itr_varibles(client_id,'deduction')
	d_80c = deduction_arr['d_80c']
	d_80ccc = deduction_arr['d_80ccc']
	d_80ccd = deduction_arr['d_80ccd']
	d_80ccd1 = deduction_arr['d_80ccd1']
	d_80g = deduction_arr['d_80g']
	d_80dd = deduction_arr['d_80dd']
	d_80u = deduction_arr['d_80u']
	d_80d = deduction_arr['d_80d']
	d_80e = deduction_arr['d_80e']
	d_80tta = deduction_arr['d_80tta']
	d_80ddb = deduction_arr['d_80ddb']
	d_80ee = deduction_arr['d_80ee']
	d_80gg = deduction_arr['d_80gg']
	d_80gga = deduction_arr['d_80gga']
	d_80ggc = deduction_arr['d_80ggc']
	d_80rrb = deduction_arr['d_80rrb']
	d_80qqb = deduction_arr['d_80qqb']
	d_80ccg = deduction_arr['d_80ccg']


	c += '<ITRForm:UsrDeductUndChapVIA>\n'
	c += '<ITRForm:Section80C>'+str(int(d_80c) )+'</ITRForm:Section80C>\n'
	c += '<ITRForm:Section80CCC>'+str(int(d_80ccc) )+'</ITRForm:Section80CCC>\n'
	c += '<ITRForm:Section80CCDEmployeeOrSE>0</ITRForm:Section80CCDEmployeeOrSE>\n'
	c += '<ITRForm:Section80CCD1B>'+str(int(d_80ccd) )+'</ITRForm:Section80CCD1B>\n'
	c += '<ITRForm:Section80CCDEmployer>'+str(int(d_80ccd1) )+'</ITRForm:Section80CCDEmployer>\n'
	# c += '<ITRForm:Section80D>'+str(d_80d)+'</ITRForm:Section80D>\n'
	# c += create_xml_health_premium_block(client_id)
	c += '<ITRForm:Section80DHealthInsPremium>\n'
	c += '<ITRForm:HealthInsurancePremium>0</ITRForm:HealthInsurancePremium>\n'
	c += '<ITRForm:Sec80DHealthInsurancePremiumUsr>'+str(int(d_80d) )+'</ITRForm:Sec80DHealthInsurancePremiumUsr>\n'
	c += '<ITRForm:Sec80DMedicalExpenditureUsr>0</ITRForm:Sec80DMedicalExpenditureUsr>\n'
	c += '<ITRForm:Sec80DPreventiveHealthCheckUpUsr>0</ITRForm:Sec80DPreventiveHealthCheckUpUsr>\n'
	c += '</ITRForm:Section80DHealthInsPremium>\n'
	c += '<ITRForm:Section80DD>'+str(int(d_80dd) )+'</ITRForm:Section80DD>\n'
	c += '<ITRForm:Section80DDB>'+str(int(d_80ddb) )+'</ITRForm:Section80DDB>\n'
	c += '<ITRForm:Section80E>'+str(int(d_80e) )+'</ITRForm:Section80E>\n'
	c += '<ITRForm:Section80EE>'+str(int(d_80ee) )+'</ITRForm:Section80EE>\n'
	c += '<ITRForm:Section80G>'+str(int(d_80g) )+'</ITRForm:Section80G>\n'
	c += '<ITRForm:Section80GG>'+str(int(d_80gg) )+'</ITRForm:Section80GG>\n'
	c += '<ITRForm:Section80GGA>'+str(int(d_80gga) )+'</ITRForm:Section80GGA>\n'
	c += '<ITRForm:Section80GGC>'+str(int(d_80ggc) )+'</ITRForm:Section80GGC>\n'
	c += '<ITRForm:Section80U>'+str(int(d_80u) )+'</ITRForm:Section80U>\n'
	c += '<ITRForm:Section80RRB>'+str(int(d_80rrb) )+'</ITRForm:Section80RRB>\n'
	c += '<ITRForm:Section80QQB>'+str(int(d_80qqb) )+'</ITRForm:Section80QQB>\n'
	c += '<ITRForm:Section80CCG>'+str(int(d_80ccg) )+'</ITRForm:Section80CCG>\n'
	c += '<ITRForm:Section80TTA>'+str(int(d_80tta) )+'</ITRForm:Section80TTA>\n'
	c += '<ITRForm:TotalChapVIADeductions>'+str(int(total_deduction) )+'</ITRForm:TotalChapVIADeductions>\n'
	c += '</ITRForm:UsrDeductUndChapVIA>\n'

	c += '<ITRForm:DeductUndChapVIA>\n'
	limited_deduction = 0
	if d_80c>150000:
		limited_deduction += 150000
		c += '<ITRForm:Section80C>150000</ITRForm:Section80C>\n'
	else:
		limited_deduction += d_80c
		c += '<ITRForm:Section80C>'+str(int(d_80c) )+'</ITRForm:Section80C>\n'
	c += '<ITRForm:Section80CCC>'+str(int(d_80ccc) )+'</ITRForm:Section80CCC>\n'
	c += '<ITRForm:Section80CCDEmployeeOrSE>0</ITRForm:Section80CCDEmployeeOrSE>\n'
	limited_deduction += d_80ccc
	if d_80ccd>50000:
		c += '<ITRForm:Section80CCD1B>50000</ITRForm:Section80CCD1B>\n'
		limited_deduction += 50000
	else:
		c += '<ITRForm:Section80CCD1B>'+str(int(d_80ccd) )+'</ITRForm:Section80CCD1B>\n'
		limited_deduction += d_80ccd
	c += '<ITRForm:Section80CCDEmployer>'+str(int(d_80ccd1) )+'</ITRForm:Section80CCDEmployer>\n'
	if d_80d>25000:
		c += '<ITRForm:Section80D>25000</ITRForm:Section80D>\n'
		limited_deduction += 30000
	else:
		c += '<ITRForm:Section80D>'+str(int(d_80d) )+'</ITRForm:Section80D>\n'
		limited_deduction += d_80d
	c += '<ITRForm:Section80DD>'+str(int(d_80dd) )+'</ITRForm:Section80DD>\n'
	limited_deduction += d_80dd
	c += '<ITRForm:Section80DDB>'+str(int(d_80ddb) )+'</ITRForm:Section80DDB>\n'
	c += '<ITRForm:Section80E>'+str(int(d_80e) )+'</ITRForm:Section80E>\n'
	c += '<ITRForm:Section80EE>'+str(int(d_80ee) )+'</ITRForm:Section80EE>\n'
	c += '<ITRForm:Section80G>'+str(int(d_80g) )+'</ITRForm:Section80G>\n'
	c += '<ITRForm:Section80GG>'+str(int(d_80gg) )+'</ITRForm:Section80GG>\n'
	c += '<ITRForm:Section80GGA>'+str(int(d_80gga) )+'</ITRForm:Section80GGA>\n'
	c += '<ITRForm:Section80GGC>'+str(int(d_80ggc) )+'</ITRForm:Section80GGC>\n'
	limited_deduction += d_80ddb + d_80e + d_80ee + d_80g + d_80gg + d_80gga + d_80ggc
	
	c += '<ITRForm:Section80U>'+str(int(d_80u) )+'</ITRForm:Section80U>\n'
	limited_deduction += d_80u
	c += '<ITRForm:Section80RRB>'+str(int( d_80rrb) )+'</ITRForm:Section80RRB>\n'
	c += '<ITRForm:Section80QQB>'+str(int( d_80qqb) )+'</ITRForm:Section80QQB>\n'
	c += '<ITRForm:Section80CCG>'+str(int( d_80ccg) )+'</ITRForm:Section80CCG>\n'
	limited_deduction += d_80rrb + d_80qqb + d_80ccg
	if d_80tta > 10000:
		c += '<ITRForm:Section80TTA>10000</ITRForm:Section80TTA>\n'
		limited_deduction += 10000
	else:
		c += '<ITRForm:Section80TTA>'+str(int( d_80tta ) )+'</ITRForm:Section80TTA>\n'
		limited_deduction += d_80tta
	c += '<ITRForm:TotalChapVIADeductions>'+str(int( limited_deduction ) )+'</ITRForm:TotalChapVIADeductions>\n'
	c += '</ITRForm:DeductUndChapVIA>\n'
	c += '<ITRForm:TotalIncome>'+str(int(GTI-total_deduction) )+'</ITRForm:TotalIncome>\n'
	c += '</ITRForm:ITR1_IncomeDeductions>\n'

	c += '<ITRForm:ITR1_TaxComputation>\n'
	c += '<ITRForm:TotalTaxPayable>'+str(int(IT_normal_rates) )+'</ITRForm:TotalTaxPayable>\n'
	c += '<ITRForm:Rebate87A>'+str(int(rebate) )+'</ITRForm:Rebate87A>\n'
	# content += '<ITRForm:TaxPayableOnRebate>'+str(int( (Tx+Tx1-0) - rebate ))+'</ITRForm:TaxPayableOnRebate>\n'

	c += '<ITRForm:TaxPayableOnRebate>'+str(int( (IT_normal_rates+Tx1-0) - rebate ))+'</ITRForm:TaxPayableOnRebate>\n'
	c += '<ITRForm:EducationCess>'+str(int(educess) )+'</ITRForm:EducationCess>\n'
	c += '<ITRForm:GrossTaxLiability>'+str(int(IT_normal_rates-rebate+educess) )+'</ITRForm:GrossTaxLiability>\n'
	c += '<ITRForm:Section89>0</ITRForm:Section89>\n'
	c += '<ITRForm:NetTaxLiability>'+str(int((IT_normal_rates-rebate+educess)-0) )+'</ITRForm:NetTaxLiability>\n'
	# 0 is Section89

	log.info('interest_234A '+str(interest_234A))
	log.info('interest_234B '+str(interest_234B))
	log.info('interest_234C '+str(interest_234C))
	log.info('interest_234F '+str(interest_234F))
	log.info('total_interest '+str(total_interest))

	c += '<ITRForm:TotalIntrstPay>'+str(int(total_interest))+'</ITRForm:TotalIntrstPay>\n'
	c += '<ITRForm:IntrstPay>\n'
	c += '<ITRForm:IntrstPayUs234A>'+str(int(interest_234A))+'</ITRForm:IntrstPayUs234A>\n'
	c += '<ITRForm:IntrstPayUs234B>'+str(int(interest_234B))+'</ITRForm:IntrstPayUs234B>\n'
	c += '<ITRForm:IntrstPayUs234C>'+str(int(interest_234C))+'</ITRForm:IntrstPayUs234C>\n'
	c += '<ITRForm:LateFilingFee234F>'+str(int(interest_234F))+'</ITRForm:LateFilingFee234F>\n'
	c += '</ITRForm:IntrstPay>\n'
	c += '<ITRForm:TotTaxPlusIntrstPay>'+str(int(IT_normal_rates-rebate+educess+total_interest) )+'</ITRForm:TotTaxPlusIntrstPay>\n'
	# TotTaxPlusIntrstPay = NetTaxLiability + Add Above Interest A,B,C
	c += '</ITRForm:ITR1_TaxComputation>\n'

	adv_t, sa_t, tds_value, tcs_value, tax_paid_count, taxes_paid_total_tax = (0,)*6
	tp_bsr_code = []
	tp_deposit_date = []
	tp_challan_no = []
	tp_total_tax = []

	date = datetime(2018,03,31)
	date = date.replace(tzinfo=None)
	tax_paid_arr = get_itr_varibles(client_id,'tax_paid')
	adv_t = tax_paid_arr['adv_t']
	sa_t = tax_paid_arr['sa_t']
	tax_paid_count = tax_paid_arr['tax_paid_count']
	tp_bsr_code = tax_paid_arr['tp_bsr_code']
	tp_deposit_date = tax_paid_arr['tp_deposit_date']
	tp_challan_no = tax_paid_arr['tp_challan_no']
	tp_total_tax = tax_paid_arr['tp_total_tax']
	taxes_paid_total_tax = tax_paid_arr['taxes_paid_total_tax']

	# if tax_paid.objects.filter(R_Id=R_ID).exists():
		# for tax in tax_paid.objects.filter(R_Id = R_ID):
		# 	if date >tax.deposit_date.replace(tzinfo=None) :
		# 		adv_t += tax.total_tax or 0
		# 	else:
		# 		sa_t += tax.total_tax or 0
		# 	tax_paid_count += 1
		# 	tp_bsr_code.append(tax.BSR_code or 0)
		# 	tp_deposit_date.append(tax.deposit_date.strftime("%Y-%m-%d") or 0)
		# 	tp_challan_no.append(tax.challan_serial_no or 0)
		# 	tp_total_tax.append(tax.total_tax or 0)
		# 	taxes_paid_total_tax += tax.total_tax or 0

	tds_count, total_tds1, tcs_count, total_tcs, tds2_count, total_tds2 = (0,)*6
	tds_dc_name = []
	tds_amt_paid = []
	tds_deducted = []
	tds_TAN_PAN = []
	tcs_dc_name = []
	tcs_amt_paid = []
	tcs_deducted = []
	tcs_TAN_PAN = []
	tds2_dc_name = []
	tds2_amt_paid = []
	tds2_tds_deducted = []
	tds2_TAN_PAN = []

	tds_tcs_arr = get_itr_varibles(client_id,'tds_tcs')
	tds_count = tds_tcs_arr['tds_count']
	total_tds1 = tds_tcs_arr['total_tds1']
	tcs_count = tds_tcs_arr['tcs_count']
	total_tcs = tds_tcs_arr['total_tcs']
	tds2_count = tds_tcs_arr['tds2_count']
	total_tds2 = tds_tcs_arr['total_tds2']
	tcs_value = tds_tcs_arr['tcs_value']
	tds_value = tds_tcs_arr['tds_value']
	tds_dc_name = tds_tcs_arr['tds_dc_name']
	tds_amt_paid = tds_tcs_arr['tds_amt_paid']
	tds_deducted = tds_tcs_arr['tds_deducted']
	tds_TAN_PAN = tds_tcs_arr['tds_TAN_PAN']
	tcs_dc_name = tds_tcs_arr['tcs_dc_name']
	tcs_amt_paid = tds_tcs_arr['tcs_amt_paid']
	tcs_deducted = tds_tcs_arr['tcs_deducted']
	tcs_TAN_PAN = tds_tcs_arr['tcs_TAN_PAN']
	tds2_dc_name = tds_tcs_arr['tds2_dc_name']
	tds2_amt_paid = tds_tcs_arr['tds2_amt_paid']
	tds2_tds_deducted = tds_tcs_arr['tds2_tds_deducted']
	tds2_TAN_PAN = tds_tcs_arr['tds2_TAN_PAN']

	# if tds_tcs.objects.filter(R_Id=PAN).exists():
		# for tds in tds_tcs.objects.filter(R_Id = PAN):
		# 	if tds.part == 'B':
		# 		tcs_value += tds.tds_tcs_deposited
		# 		tcs_count += 1
		# 		tcs_TAN_PAN.append(tds.TAN_PAN)
		# 		tcs_dc_name.append(tds.Deductor_Collector_Name)
		# 		tcs_amt_paid.append(tds.amount_paid)
		# 		tcs_deducted.append(tds.tax_deducted)
		# 		total_tcs += tds.tax_deducted
		# 	else:
		# 		tds_value += tds.tds_tcs_deposited
		# for tds in tds_tcs.objects.filter(R_Id = PAN,section='192'):
		# 	tds_count += 1
		# 	total_tds1 += tds.tax_deducted
		# 	tds_TAN_PAN.append(tds.TAN_PAN)
		# 	tds_dc_name.append(tds.Deductor_Collector_Name)
		# 	tds_amt_paid.append(tds.amount_paid)
		# 	tds_deducted.append(tds.tax_deducted)
		# for tds2 in tds_tcs.objects.filter(R_Id = PAN).exclude(section='192'):
		# 	tds2_count += 1
		# 	total_tds2 += tds2.tax_deducted
		# 	tds2_TAN_PAN.append(tds2.TAN_PAN)
		# 	tds2_dc_name.append(tds2.Deductor_Collector_Name)
		# 	tds2_amt_paid.append(tds2.amount_paid)
		# 	tds2_tds_deducted.append(tds2.tax_deducted)

	c += '<ITRForm:TaxPaid>\n'
	c += '<ITRForm:TaxesPaid>\n'
	c += '<ITRForm:AdvanceTax>'+str(adv_t)+'</ITRForm:AdvanceTax>\n'
	c += '<ITRForm:TDS>'+str(tds_value)+'</ITRForm:TDS>\n'
	c += '<ITRForm:TCS>'+str(tcs_value)+'</ITRForm:TCS>\n'
	c += '<ITRForm:SelfAssessmentTax>'+str(sa_t)+'</ITRForm:SelfAssessmentTax>\n'
	c += '<ITRForm:TotalTaxesPaid>'+str(adv_t+tds_value+tcs_value+sa_t)+'</ITRForm:TotalTaxesPaid>\n'

	c += '</ITRForm:TaxesPaid>\n'

	if ( IT_normal_rates+Tx1 + surcharge + educess ) - (tds_value+tcs_value+adv_t+sa_t) >0:
	# if (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t)>0:
		c += '<ITRForm:BalTaxPayable>'+str(int( (IT_normal_rates-rebate+educess)-(adv_t+tds_value+tcs_value+sa_t) ))+'</ITRForm:BalTaxPayable>\n'
	else:
		c += '<ITRForm:BalTaxPayable>0</ITRForm:BalTaxPayable>\n'
	# if(TotTaxPlusIntrstPay - TotalTaxesPaid >0, TotTaxPlusIntrstPay - TotalTaxesPaid, 0)
	c += '</ITRForm:TaxPaid>\n'

	c += '<ITRForm:Refund>\n'
	# if (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t)<0:
	if ( IT_normal_rates+Tx1 + surcharge + educess ) - (tds_value+tcs_value+adv_t) <0:
		c += '<ITRForm:RefundDue>'+str(int(abs( (IT_normal_rates-rebate+educess)-(adv_t+tds_value+tcs_value) ) ))+'</ITRForm:RefundDue>\n'
	else:
		c += '<ITRForm:RefundDue>0</ITRForm:RefundDue>\n'
	c += '<ITRForm:BankAccountDtls>\n'
	if no_of_bank >= 1:
		c += '<ITRForm:PriBankDetails>\n'
		# for x in xrange(0,no_of_bank):
		c += '<ITRForm:IFSCCode>'+str(IFSC[0])+'</ITRForm:IFSCCode>\n'
		c += '<ITRForm:BankName>'+str(bank_name[0])+'</ITRForm:BankName>\n'
		c += '<ITRForm:BankAccountNo>'+str(acc_no[0])+'</ITRForm:BankAccountNo>\n'
		c += '</ITRForm:PriBankDetails>\n'
		if no_of_bank > 1:
			for x in xrange(1,no_of_bank):
				c += '<ITRForm:AddtnlBankDetails>\n'
				c += '<ITRForm:IFSCCode>'+str(IFSC[x])+'</ITRForm:IFSCCode>\n'
				c += '<ITRForm:BankName>'+str(bank_name[x])+'</ITRForm:BankName>\n'
				c += '<ITRForm:BankAccountNo>'+str(acc_no[x])+'</ITRForm:BankAccountNo>\n'
				c += '</ITRForm:AddtnlBankDetails>\n'
	else:
		c += '<ITRForm:PriBankDetails>\n'
		c += '<ITRForm:IFSCCode></ITRForm:IFSCCode>\n'
		c += '<ITRForm:BankName></ITRForm:BankName>\n'
		c += '<ITRForm:BankAccountNo></ITRForm:BankAccountNo>\n'
		c += '</ITRForm:PriBankDetails>\n'

	c += '</ITRForm:BankAccountDtls>\n'
	c += '</ITRForm:Refund>\n'

	log.info('ITR-1 : '+pan)
	if tds_count!=0:
		c += '<ITRForm:TDSonSalaries>\n'
		for x in xrange(0,tds_count):
			c += '<ITRForm:TDSonSalary>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:TAN>'+tds_TAN_PAN[x]+'</ITRForm:TAN>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollecterName>'+str(tds_dc_name[x])+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
			c += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:IncChrgSal>'+str(tds_amt_paid[x])+'</ITRForm:IncChrgSal>\n'
			c += '<ITRForm:TotalTDSSal>'+str(tds_deducted[x])+'</ITRForm:TotalTDSSal>\n'
			c += '</ITRForm:TDSonSalary>\n'
		c += '<ITRForm:TotalTDSonSalaries>'+str(total_tds1)+'</ITRForm:TotalTDSonSalaries>\n'
		c += '</ITRForm:TDSonSalaries>\n'
	if tds2_count!=0:
		c += '<ITRForm:TDSonOthThanSals>\n'
		for x in xrange(0,tds2_count):
			c += '<ITRForm:TDSonOthThanSal>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:TAN>'+tds2_TAN_PAN[x]+'</ITRForm:TAN>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollecterName>'+tds2_dc_name[x]+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
			c += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:AmtForTaxDeduct>'+str(tds2_amt_paid[x])+'</ITRForm:AmtForTaxDeduct>\n'
			c += '<ITRForm:DeductedYr>2017</ITRForm:DeductedYr>\n'
			c += '<ITRForm:TotTDSOnAmtPaid>'+str(tds2_tds_deducted[x])+'</ITRForm:TotTDSOnAmtPaid>\n'
			c += '<ITRForm:ClaimOutOfTotTDSOnAmtPaid>'+str(tds2_tds_deducted[x])+'</ITRForm:ClaimOutOfTotTDSOnAmtPaid>\n'
			c += '</ITRForm:TDSonOthThanSal>\n'
		c += '<ITRForm:TotalTDSonOthThanSals>'+str(total_tds2)+'</ITRForm:TotalTDSonOthThanSals>\n'
		c += '</ITRForm:TDSonOthThanSals>\n'

	# c += '<ITRForm:TDSDtls26QC>\n'
	# c += '<ITRForm:TDSDetails26QC>\n'
	# c += '<ITRForm:PANofTenant>ANRPB0496J</ITRForm:PANofTenant>\n'
	# c += '<ITRForm:NameOfTenant>TENANT KA NAAM</ITRForm:NameOfTenant>\n'
	# c += '<ITRForm:AmtForTaxDeduct>30000</ITRForm:AmtForTaxDeduct>\n'
	# c += '<ITRForm:DeductedYr>2017</ITRForm:DeductedYr>\n'
	# c += '<ITRForm:TaxDeducted>3000</ITRForm:TaxDeducted>\n'
	# c += '<ITRForm:ClaimOutOfTotTDSOnAmtPaid>3000</ITRForm:ClaimOutOfTotTDSOnAmtPaid>\n'
	# c += '</ITRForm:TDSDetails26QC>\n'
	# c += '<ITRForm:TotalTDSDetails26QC>3000</ITRForm:TotalTDSDetails26QC>\n'
	# c += '</ITRForm:TDSDtls26QC>\n'
	if tcs_count>0:
		c += '<ITRForm:ScheduleTCS>\n'
		for x in xrange(0,tcs_count):
			c += '<ITRForm:TCS>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:TAN>'+tcs_TAN_PAN[x]+'</ITRForm:TAN>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollecterName>'+tcs_dc_name[x]+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
			c += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:AmtTaxCollected>'+str(tcs_amt_paid[x])+'</ITRForm:AmtTaxCollected>\n'
			c += '<ITRForm:CollectedYr>2017</ITRForm:CollectedYr>\n'
			c += '<ITRForm:TotalTCS>'+str(tcs_amt_paid[x])+'</ITRForm:TotalTCS>\n'
			c += '<ITRForm:AmtTCSClaimedThisYear>'+str(tcs_amt_paid[x])+'</ITRForm:AmtTCSClaimedThisYear>\n'
			c += '</ITRForm:TCS>\n'
		c += '<ITRForm:TotalSchTCS>'+str(total_tcs)+'</ITRForm:TotalSchTCS>\n'
		c += '</ITRForm:ScheduleTCS>\n'

	if tax_paid_count != 0:
		try:
			c += '<ITRForm:TaxPayments>\n'
			log.info(tp_bsr_code)
			for x in range(len(tp_bsr_code)):
				c += '<ITRForm:TaxPayment>\n'
				if len(str(tp_bsr_code[x]))==6:
					BSRCode='0'+str(tp_bsr_code[x])
				else:
					BSRCode=tp_bsr_code[x]
				c += '<ITRForm:BSRCode>'+str(BSRCode)+'</ITRForm:BSRCode>\n'
				c += '<ITRForm:DateDep>'+str(tp_deposit_date[x])+'</ITRForm:DateDep>\n'
				c += '<ITRForm:SrlNoOfChaln>'+str(tp_challan_no[x])+'</ITRForm:SrlNoOfChaln>\n'
				c += '<ITRForm:Amt>'+str(tp_total_tax[x])+'</ITRForm:Amt>\n'
				c += '</ITRForm:TaxPayment>\n'
		except Exception as ex:
			log.info('Error in tax payment block '+str(traceback.format_exc()))

		c += '<ITRForm:TotalTaxPayments>'+str(taxes_paid_total_tax)+'</ITRForm:TotalTaxPayments>\n'
		c += '</ITRForm:TaxPayments>\n'

	c += '<ITR1FORM:TaxExmpIntInc>0</ITR1FORM:TaxExmpIntInc>\n'

	c += '<ITRForm:Verification>\n'
	c += '<ITRForm:Declaration>\n'
	# c += '<ITRForm:AssesseeVerName>'+fname+' '+lname+'</ITRForm:AssesseeVerName>\n'
	if mname == '':
		c += '<ITRForm:AssesseeVerName>'+fname+' '+lname+'</ITRForm:AssesseeVerName>\n'
		c += '<ITRForm:FatherName>'+lname+'</ITRForm:FatherName>\n'
	else:
		c += '<ITRForm:AssesseeVerName>'+fname+' '+mname+' '+lname+'</ITRForm:AssesseeVerName>\n'
		c += '<ITRForm:FatherName>'+mname+'</ITRForm:FatherName>\n'
	c += '<ITRForm:AssesseeVerPAN>'+PAN+'</ITRForm:AssesseeVerPAN>\n'
	c += '</ITRForm:Declaration>\n'
	c += '<ITRForm:Capacity>S</ITRForm:Capacity>\n'
	c += '<ITRForm:Place>'+city+'</ITRForm:Place>\n'
	c += '<ITRForm:Date>'+CreationDate+'</ITRForm:Date>\n'
	c += '</ITRForm:Verification>\n'
	
	c += '</ITR1FORM:ITR1>\n'
	c += '</ITRETURN:ITR>\n'
	return c


def xml_content_2019(client_id,request,software_id):
	R_ID = get_rid(client_id)
	log.info(R_ID)

	AssessmentYear = 0
	if Return_Details.objects.filter(R_id=R_ID).exists():
		Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
		AssessmentYear = Return_Details_instance.AY
		AssessmentYear = AssessmentYear.split('-')[0]
	# AssessmentYear = now.strftime("%Y")
	c = '<?xml version="1.0" encoding="ISO-8859-1"?>\n'
	c += '<ITRETURN:ITR xmlns:ITRETURN="http://incometaxindiaefiling.gov.in/main" xmlns:ITR1FORM="http://incometaxindiaefiling.gov.in/ITR1" xmlns:ITRForm="http://incometaxindiaefiling.gov.in/master" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'
	c += '<ITR1FORM:ITR1>\n'

	now = datetime.now()
	CreationDate = now.strftime("%Y-%m-%d")

	c += '<ITRForm:CreationInfo>\n'
	c += '<ITRForm:SWVersionNo>R1</ITRForm:SWVersionNo>\n'
	c += '<ITRForm:SWCreatedBy>'+software_id+'</ITRForm:SWCreatedBy>\n'
	c += '<ITRForm:XMLCreatedBy>'+software_id+'</ITRForm:XMLCreatedBy>\n'
	c += '<ITRForm:XMLCreationDate>'+CreationDate+'</ITRForm:XMLCreationDate>\n'
	c += '<ITRForm:IntermediaryCity>Delhi</ITRForm:IntermediaryCity>\n'
	c += '<ITRForm:Digest>-</ITRForm:Digest>\n'
	c += '</ITRForm:CreationInfo>\n'

	# AssessmentYear = now.strftime("%Y")
	c += '<ITRForm:Form_ITR1>\n'
	c += '<ITRForm:FormName>ITR-1</ITRForm:FormName>\n'
	# Description ??
	c += '<ITRForm:Description>For Indls having Income from Salary, Pension, family pension and Interest</ITRForm:Description>\n'
	c += '<ITRForm:AssessmentYear>'+AssessmentYear+'</ITRForm:AssessmentYear>\n'
	c += '<ITRForm:SchemaVer>Ver1.0</ITRForm:SchemaVer>\n'
	c += '<ITRForm:FormVer>Ver1.0</ITRForm:FormVer>\n'
	c += '</ITRForm:Form_ITR1>\n'

	fname, fathername, mname, lname, PAN, dob, address, mobile, aadhar_no = ('',)*9

	pi_arr = get_itr_varibles(client_id,'personal_information')
	fathername = pi_arr['fathername']
	fname = pi_arr['fname']
	mname = pi_arr['mname']
	lname = pi_arr['lname']
	PAN = pi_arr['PAN']
	dob = pi_arr['dob']
	mobile = pi_arr['mobile']
	aadhar_no = pi_arr['aadhar_no']

	age1 = dob.split('-')[2]
	age = 2019 - int(age1)

	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	ResidenceNo, ResidenceName, RoadOrStreet, LocalityOrArea, city = ('',)*5
	state, PinCode, Employer_category, residential_status, email, Add_statecode = ('',)*6

	bank_arr = get_itr_varibles(client_id,'bank_detail')
	no_of_bank = bank_arr['no_of_bank']
	bank_name = bank_arr['bank_name']
	IFSC = bank_arr['IFSC']
	acc_no = bank_arr['acc_no']
	acc_type = bank_arr['acc_type']
	# no_of_bank = Bank_Details.objects.filter(P_id=personal_instance).count()
	state_list = State_Code.objects.all()
	
	address_arr = get_itr_varibles(client_id,'address_detail')
	ResidenceNo = address_arr['ResidenceNo']
	ResidenceName = address_arr['ResidenceName']
	RoadOrStreet = address_arr['RoadOrStreet']
	LocalityOrArea = address_arr['LocalityOrArea']
	city = address_arr['city']
	state = address_arr['state']
	PinCode = address_arr['PinCode']
	Add_statecode = address_arr['Add_statecode']
	Employer_category = address_arr['Employer_category']
	residential_status = address_arr['residential_status']

	HP_i, Rent_Received, municipal_tax, ICU_house_p = (0,)*4
	TypeOfHP = ''
	for a in House_Property_Details.objects.filter(R_Id = R_ID):
		HP_i += float(a.Interest_on_Home_loan or 0)
		Rent_Received += a.Rent_Received
		municipal_tax += a.Property_Tax
		if a.Type_of_Hosue_Property=='self_occupied':
			TypeOfHP = 'S'
		elif a.Type_of_Hosue_Property=='let_out':
			TypeOfHP = 'L'
		else:
			TypeOfHP = 'D'
		sd = float( ( (a.Rent_Received or 0)-(a.Property_Tax or 0) )*0.3 )
		ICU_house_p += abs(a.Rent_Received)-abs(a.Property_Tax)-abs(sd)-abs(a.Interest_on_Home_loan or 0)
	if HP_i>200000:
		HP_i = 200000
	for a in SalatTaxUser.objects.filter(id=client_id):
		email = a.email

	c += '<ITRForm:PersonalInfo>\n'
	c += '<ITRForm:AssesseeName>\n'
	c += '<ITRForm:FirstName>'+fname+'</ITRForm:FirstName>\n'
	if mname != '':
		c += '<ITRForm:MiddleName>'+mname+'</ITRForm:MiddleName>\n'
	c += '<ITRForm:SurNameOrOrgName>'+lname+'</ITRForm:SurNameOrOrgName>\n'
	c += '</ITRForm:AssesseeName>\n'
	c += '<ITRForm:PAN>'+PAN+'</ITRForm:PAN>\n'
	c += '<ITRForm:Address>\n'
	c += '<ITRForm:ResidenceNo>'+ResidenceNo+'</ITRForm:ResidenceNo>\n'
	c += '<ITRForm:ResidenceName>'+ResidenceName+'</ITRForm:ResidenceName>\n'
	if RoadOrStreet!='':
		c += '<ITRForm:RoadOrStreet>'+RoadOrStreet+'</ITRForm:RoadOrStreet>\n'
	c += '<ITRForm:LocalityOrArea>'+LocalityOrArea+'</ITRForm:LocalityOrArea>\n'
	c += '<ITRForm:CityOrTownOrDistrict>'+city+'</ITRForm:CityOrTownOrDistrict>\n'
	c += '<ITRForm:StateCode>'+str(Add_statecode)+'</ITRForm:StateCode>\n'
	c += '<ITRForm:CountryCode>91</ITRForm:CountryCode>\n'
	c += '<ITRForm:PinCode>'+PinCode+'</ITRForm:PinCode>\n'
	c += '<ITRForm:CountryCodeMobile>91</ITRForm:CountryCodeMobile>\n'
	c += '<ITRForm:MobileNo>'+mobile+'</ITRForm:MobileNo>\n'
	c += '<ITRForm:EmailAddress>'+email+'</ITRForm:EmailAddress>\n'
	# c += '<ITRForm:EmailAddress>swapnil.bagmar@gmail.com</ITRForm:EmailAddress>\n'
	c += '</ITRForm:Address>\n'
	c += '<ITRForm:DOB>'+dob+'</ITRForm:DOB>\n'
	if Employer_category=='Other' or  Employer_category=='' :
		c += '<ITRForm:EmployerCategory>OTH</ITRForm:EmployerCategory>\n'
	else:
		c += '<ITRForm:EmployerCategory>'+Employer_category+'</ITRForm:EmployerCategory>\n'
	if aadhar_no != '':
		c += '<ITRForm:AadhaarCardNo>'+str(aadhar_no)+'</ITRForm:AadhaarCardNo>\n'
	c += '</ITRForm:PersonalInfo>\n'

	pan = PAN
	
	sal, ICU_head_sal, r, m, sd, i, Interest_Income, Interest_Savings, FD = (0,)*9
	Other_Income, dividend, ICU_other_s, d_80c, d_80d, d_80e, d_80tta, GTI, TI, Tx = (0,)*10
	Tx1, rebate, surcharge, education_cess, tax_with_cess, total_tds_paid = (0,)*6
	salary, PerquisitesValue, ProfitsInSalary, DeductionUs16 = (0,)*4
	entertainment_a, profession_t, ProfitsInSalary, DeductionUs16 = (0,)*4
	total_deduction, taxable_income, total_adv_tax_paid, balance_tax = (0,)*4
	
	other_i_arr = get_itr_varibles(client_id,'other_i_common')
	Interest_Savings = other_i_arr['Interest_Savings']
	Other_Income = other_i_arr['Other_Income']
	Interest_Income = other_i_arr['Interest_Income']
	FD = other_i_arr['FD']
	family_pension = other_i_arr['family_pension']

	exempt_i_arr = get_itr_varibles(client_id,'exempt_i')
	dividend = exempt_i_arr['dividend']
	agri_income = exempt_i_arr['agri_income']
	other_exempt_income = exempt_i_arr['other_exempt_income']
	Tax_Free_Interest = exempt_i_arr['Tax_Free_Interest']

	lt_shares_gain, lt_equity_gain = (0,)*2
	if Shares_LT.objects.filter(R_Id=R_ID).exists():
		Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
		lt_shares_gain= Shares_LT_instance.Expenditure
	if Equity_LT.objects.filter(R_Id=R_ID).exists():
		Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
		lt_equity_gain= Equity_LT_instance.Expenditure

	for a in Other_Income_Common.objects.filter(R_Id = R_ID):
		ICU_other_s = float(a.Other_Interest or 0) + float(a.Interest_Savings or 0) + float(a.Other_Income or 0) + float(a.FD or 0) + float(a.family_pension or 0)
	
	# Income chargeable under the head Salaries (S)
	calculated_salary = 0
	income_arr = get_itr_varibles(client_id,'income_detail')
	calculated_salary = income_arr['calculated_salary']
	PerquisitesValue = income_arr['PerquisitesValue']
	form16_hra = income_arr['form16_hra']
	lta = income_arr['lta']
	other_allowances = income_arr['other_allowances']
	ProfitsInSalary = income_arr['ProfitsInSalary']
	DeductionUs16 = income_arr['DeductionUs16']
	entertainment_a = income_arr['entertainment_a']
	profession_t = income_arr['profession_t']
	sal = income_arr['sal']
	ICU_head_sal = income_arr['ICU_head_sal']

	ttp ,rebate ,educess ,gtl, IT_normal_rates  = (0,)*5
	computation_arr = get_itr_varibles(client_id,'computation')

	ttp = computation_arr['ttp']
	rebate = computation_arr['rebate']
	educess = computation_arr['educess']
	gtl = computation_arr['gtl']
	salary = computation_arr['salary']
	GTI = computation_arr['GTI']
	total_deduction = computation_arr['total_deduction']
	taxable_income = computation_arr['taxable_income']
	IT_normal_rates = computation_arr['Tx']
	Tx1 = computation_arr['Tx1']
	surcharge = computation_arr['surcharge']
	interest_234A= computation_arr['interest_234A']
	interest_234B= computation_arr['interest_234B']
	interest_234C= computation_arr['interest_234C']
	interest_234F= computation_arr['interest_234F']
	total_interest= computation_arr['total_interest']

	
	log.info('ITR-1 : '+pan)
	return_detail_arr = get_itr_varibles(client_id,'return_detail')
	ReturnFileSec1 = return_detail_arr['ReturnFileSec1']
	ReturnType1 = return_detail_arr['ReturnType1']
	ReceiptNo1 = return_detail_arr['ReceiptNo1']
	OrigRetFiledDate1 = return_detail_arr['OrigRetFiledDate1']

	c += '<ITRForm:FilingStatus>\n'
	c += '<ITRForm:ReturnFileSec>'+str(ReturnFileSec1)+'</ITRForm:ReturnFileSec>\n'
	if ReturnFileSec1 == '17':
		c += '<ITRForm:ReturnType>'+str(ReturnType1)+'</ITRForm:ReturnType>\n'
		c += '<ITRForm:ReceiptNo>'+ReceiptNo1+'</ITRForm:ReceiptNo>\n'
		c += '<ITRForm:OrigRetFiledDate>'+OrigRetFiledDate1+'</ITRForm:OrigRetFiledDate>\n'
		c += '<ITRForm:PortugeseCC5A>N</ITRForm:PortugeseCC5A>\n'
	c += '</ITRForm:FilingStatus>\n'

	Salary  = int(sal) - int(PerquisitesValue) - int(ProfitsInSalary)
	if(salary!=0):
		DeductionUs16ia = 40000
	else:
		DeductionUs16ia=0
	c += '<ITRForm:ITR1_IncomeDeductions>\n'
	c += '<ITRForm:GrossSalary>'+str(int(sal) )+'</ITRForm:GrossSalary>\n'
	c += '<ITRForm:Salary>'+str(int(Salary) )+'</ITRForm:Salary>\n'
	# c += '<ITRForm:AlwnsNotExempt>0</ITRForm:AlwnsNotExempt>\n'
	c += '<ITRForm:PerquisitesValue>'+str(int(PerquisitesValue) )+'</ITRForm:PerquisitesValue>\n'
	c += '<ITRForm:ProfitsInSalary>'+str(int(ProfitsInSalary) )+'</ITRForm:ProfitsInSalary>\n'

	TotalAllwncExemptUs10 = int(form16_hra)+int(lta)+int(other_allowances)
	if int(form16_hra)+int(lta)+int(other_allowances) > 0:
		c += '<ITRForm:AllwncExemptUs10>\n'
		if int(form16_hra)>0:
			c += '<ITRForm:AllwncExemptUs10Dtls>\n'
			c += '<ITRForm:SalNatureDesc>10(13A)</ITRForm:SalNatureDesc>\n'
			c += '<ITRForm:SalOthAmount>'+str(int(form16_hra) )+'</ITRForm:SalOthAmount>\n'
			c += '</ITRForm:AllwncExemptUs10Dtls>\n'
		if int(lta)>0:
			c += '<ITRForm:AllwncExemptUs10Dtls>\n'
			c += '<ITRForm:SalNatureDesc>10(5)</ITRForm:SalNatureDesc>\n'
			c += '<ITRForm:SalOthAmount>'+str(int(lta) )+'</ITRForm:SalOthAmount>\n'
			c += '</ITRForm:AllwncExemptUs10Dtls>\n'
		if int(other_allowances)>0:
			c += '<ITRForm:AllwncExemptUs10Dtls>\n'
			c += '<ITRForm:SalNatureDesc>OTH</ITRForm:SalNatureDesc>\n'
			c += '<ITRForm:SalOthNatOfInc>Any Other</ITRForm:SalOthNatOfInc>\n'
			c += '<ITRForm:SalOthAmount>'+str(int(other_allowances) )+'</ITRForm:SalOthAmount>\n'
			c += '</ITRForm:AllwncExemptUs10Dtls>\n'
		c += '<ITRForm:TotalAllwncExemptUs10>'+str(TotalAllwncExemptUs10)+'</ITRForm:TotalAllwncExemptUs10>\n'
		c += '</ITRForm:AllwncExemptUs10>\n'

	c += '<ITRForm:NetSalary>'+str(int(sal)-int(TotalAllwncExemptUs10) )+'</ITRForm:NetSalary>\n'
	DeductionUs16 = int(DeductionUs16ia)+ int(profession_t)+ int(entertainment_a)
	c += '<ITRForm:DeductionUs16>'+str(DeductionUs16)+'</ITRForm:DeductionUs16>\n'

	c += '<ITRForm:DeductionUs16ia>'+str(int(DeductionUs16ia) )+'</ITRForm:DeductionUs16ia>\n'
	c += '<ITRForm:EntertainmentAlw16ii>'+str(int(entertainment_a) )+'</ITRForm:EntertainmentAlw16ii>\n'
	c += '<ITRForm:ProfessionalTaxUs16iii>'+str(int(profession_t) )+'</ITRForm:ProfessionalTaxUs16iii>\n'

	# c += '<ITRForm:IncomeFromSal>'+str(int(ICU_head_sal) )+'</ITRForm:IncomeFromSal>\n'
	c += '<ITRForm:IncomeFromSal>'+str(int(sal)-int(TotalAllwncExemptUs10) - DeductionUs16 )+'</ITRForm:IncomeFromSal>\n'
	
	if TypeOfHP!='':
		c += '<ITRForm:TypeOfHP>'+TypeOfHP+'</ITRForm:TypeOfHP>\n'
		c += '<ITRForm:GrossRentReceived>'+str(int(Rent_Received))+'</ITRForm:GrossRentReceived>\n'
	if int(municipal_tax) > 0:
		c += '<ITRForm:TaxPaidlocalAuth>'+str(int(municipal_tax))+'</ITRForm:TaxPaidlocalAuth>\n'
	c += '<ITRForm:AnnualValue>'+str(int(Rent_Received-municipal_tax))+'</ITRForm:AnnualValue>\n'
	c += '<ITRForm:StandardDeduction>'+str(int( 0.3 * (Rent_Received-municipal_tax) ))+'</ITRForm:StandardDeduction>\n'
	
	if int(abs(HP_i) )>0:
		c += '<ITRForm:InterestPayable>'+str(int(abs(HP_i) ))+'</ITRForm:InterestPayable>\n'
	c += '<ITRForm:ArrearsUnrealizedRentRcvd>0</ITRForm:ArrearsUnrealizedRentRcvd>\n'
	
	c += '<ITRForm:TotalIncomeOfHP>'+str(int(ICU_house_p) )+'</ITRForm:TotalIncomeOfHP>\n'
	c += '<ITRForm:IncomeOthSrc>'+str(int(ICU_other_s) )+'</ITRForm:IncomeOthSrc>\n'

	# Continue
	if int(ICU_other_s) >0:
		c += '<ITRForm:OthersInc>\n'
		if int(FD) > 0:
			c += '<ITRForm:OthersIncDtlsOthSrc>\n'
			c += '<ITRForm:OthSrcNatureDesc>IFD</ITRForm:OthSrcNatureDesc>\n'
			# c += '<ITRForm:OthSrcOthNatOfInc>INTEREST</ITRForm:OthSrcOthNatOfInc>\n'
			c += '<ITRForm:OthSrcOthAmount>'+str(int(FD) )+'</ITRForm:OthSrcOthAmount>\n'
			c += '</ITRForm:OthersIncDtlsOthSrc>\n'
		if int(Interest_Savings) > 0:
			c += '<ITRForm:OthersIncDtlsOthSrc>\n'
			c += '<ITRForm:OthSrcNatureDesc>SAV</ITRForm:OthSrcNatureDesc>\n'
			# c += '<ITRForm:OthSrcOthNatOfInc>INTEREST</ITRForm:OthSrcOthNatOfInc>\n'
			c += '<ITRForm:OthSrcOthAmount>'+str(int(Interest_Savings) )+'</ITRForm:OthSrcOthAmount>\n'
			c += '</ITRForm:OthersIncDtlsOthSrc>\n'
		if int(family_pension) > 0:
			c += '<ITRForm:OthersIncDtlsOthSrc>\n'
			c += '<ITRForm:OthSrcNatureDesc>FAP</ITRForm:OthSrcNatureDesc>\n'
			# c += '<ITRForm:OthSrcOthNatOfInc>INTEREST</ITRForm:OthSrcOthNatOfInc>\n'
			c += '<ITRForm:OthSrcOthAmount>'+str(int(family_pension) )+'</ITRForm:OthSrcOthAmount>\n'
			c += '</ITRForm:OthersIncDtlsOthSrc>\n'
		if int(Interest_Income + Other_Income) > 0:
			c += '<ITRForm:OthersIncDtlsOthSrc>\n'
			c += '<ITRForm:OthSrcNatureDesc>OTH</ITRForm:OthSrcNatureDesc>\n'
			c += '<ITRForm:OthSrcOthNatOfInc>Other Interest</ITRForm:OthSrcOthNatOfInc>\n'
			c += '<ITRForm:OthSrcOthAmount>'+str(int(Interest_Income + Other_Income) )+'</ITRForm:OthSrcOthAmount>\n'
			c += '</ITRForm:OthersIncDtlsOthSrc>\n'
		c += '</ITRForm:OthersInc>\n'

	# family pension
	DeductionUs57iia = 0
	if int(family_pension) > 0:
		if (int(family_pension)/3)>15000 > 0:
			DeductionUs57iia = 15000
		else:
			DeductionUs57iia = int(family_pension)
		c += '<ITRForm:DeductionUs57iia>'+str(DeductionUs57iia)+'</ITRForm:DeductionUs57iia>\n'

	GrossTotIncome = int(sal)-int(TotalAllwncExemptUs10) - DeductionUs16 + int(ICU_house_p) + int(ICU_other_s)
	c += '<ITRForm:GrossTotIncome>'+str(int(GrossTotIncome) )+'</ITRForm:GrossTotIncome>\n'

	d_80c ,d_80ccc ,d_80ccd ,d_80g ,d_80dd ,d_80u ,d_80d ,d_80e ,d_80tta ,d_80ddb ,d_80ee = (0,)*11
	d_80gg ,d_80gga ,d_80ggc ,d_80rrb ,d_80qqb ,d_80ccg, d_80ccd1 = (0,)*7

	deduction_arr = get_itr_varibles(client_id,'deduction')
	d_80c = deduction_arr['d_80c']
	d_80ccc = deduction_arr['d_80ccc']
	d_80ccd = deduction_arr['d_80ccd']
	d_80ccd1 = deduction_arr['d_80ccd1']
	d_80ccd2 = deduction_arr['d_80ccd2']
	d_80g = deduction_arr['d_80g']
	d_80dd = deduction_arr['d_80dd']
	d_80u = deduction_arr['d_80u']
	d_80d = deduction_arr['d_80d']
	d_80e = deduction_arr['d_80e']
	d_80tta = deduction_arr['d_80tta']
	d_80ttb = deduction_arr['d_80ttb']
	d_80ddb = deduction_arr['d_80ddb']
	d_80ee = deduction_arr['d_80ee']
	d_80gg = deduction_arr['d_80gg']
	d_80gga = deduction_arr['d_80gga']
	d_80ggc = deduction_arr['d_80ggc']
	d_80rrb = deduction_arr['d_80rrb']
	d_80qqb = deduction_arr['d_80qqb']
	d_80ccg = deduction_arr['d_80ccg']

	c += '<ITRForm:UsrDeductUndChapVIA>\n'
	c += '<ITRForm:Section80C>'+str(int(d_80c) )+'</ITRForm:Section80C>\n'
	c += '<ITRForm:Section80CCC>'+str(int(d_80ccc) )+'</ITRForm:Section80CCC>\n'
	c += '<ITRForm:Section80CCDEmployeeOrSE>'+str(int(d_80ccd1) )+'</ITRForm:Section80CCDEmployeeOrSE>\n'
	c += '<ITRForm:Section80CCD1B>'+str(int(d_80ccd) )+'</ITRForm:Section80CCD1B>\n'
	c += '<ITRForm:Section80CCDEmployer>'+str(int(d_80ccd2) )+'</ITRForm:Section80CCDEmployer>\n'
	# c += '<ITRForm:Section80D>'+str(d_80d)+'</ITRForm:Section80D>\n'
	c += create_xml_health_premium_block(client_id)
	# c += '<ITRForm:Section80DHealthInsPremium>\n'
	# # list
	# # c += '<ITRForm:HealthInsurancePremium>0</ITRForm:HealthInsurancePremium>\n'
	# c += '<ITRForm:Sec80DHealthInsurancePremiumUsr>0</ITRForm:Sec80DHealthInsurancePremiumUsr>\n'
	# # list
	# # c += '<ITRForm:MedicalExpenditure>0</ITRForm:MedicalExpenditure>\n'
	# # c += '<ITRForm:Sec80DMedicalExpenditureUsr>0</ITRForm:Sec80DMedicalExpenditureUsr>\n'
	# c += '<ITRForm:Sec80DMedicalExpenditureUsr>'+str(int(d_80d) )+'</ITRForm:Sec80DMedicalExpenditureUsr>\n'
	# # list
	# c += '<ITRForm:PreventiveHealthCheckUp>1</ITRForm:PreventiveHealthCheckUp>\n'
	# # c += '<ITRForm:Sec80DPreventiveHealthCheckUpUsr>'+str(int(d_80d) )+'</ITRForm:Sec80DPreventiveHealthCheckUpUsr>\n'
	# c += '<ITRForm:Sec80DPreventiveHealthCheckUpUsr>'+str(int(0) )+'</ITRForm:Sec80DPreventiveHealthCheckUpUsr>\n'
	# c += '</ITRForm:Section80DHealthInsPremium>\n'
	total_deduction=int(d_80c)+int(d_80ccc)+int(d_80ccd1)+int(d_80ccd)+int(d_80ccd2)+int(d_80dd)+int(d_80ddb)+int(d_80e)+int(d_80ee)+int(d_80g)+int(d_80gg)+int(d_80gga)+int(d_80ggc)+int(d_80u)+int(d_80tta)+int(d_80ttb)+int(d_80d)
	if int(d_80dd) > 0:
		c += '<ITRForm:Section80DDUsrType>0</ITRForm:Section80DDUsrType>\n'
	c += '<ITRForm:Section80DD>'+str(int(d_80dd) )+'</ITRForm:Section80DD>\n'
	if int(d_80ddb) > 0:
		c += '<ITRForm:Section80DDBUsrType>0</ITRForm:Section80DDBUsrType>\n'
	c += '<ITRForm:Section80DDB>'+str(int(d_80ddb) )+'</ITRForm:Section80DDB>\n'
	c += '<ITRForm:Section80E>'+str(int(d_80e) )+'</ITRForm:Section80E>\n'
	c += '<ITRForm:Section80EE>'+str(int(d_80ee) )+'</ITRForm:Section80EE>\n'
	c += '<ITRForm:Section80G>'+str(int(d_80g) )+'</ITRForm:Section80G>\n'
	c += '<ITRForm:Section80GG>'+str(int(d_80gg) )+'</ITRForm:Section80GG>\n'
	c += '<ITRForm:Section80GGA>'+str(int(d_80gga) )+'</ITRForm:Section80GGA>\n'
	c += '<ITRForm:Section80GGC>'+str(int(d_80ggc) )+'</ITRForm:Section80GGC>\n'
	if int(d_80u) > 0:
		c += '<ITRForm:Section80UUsrType>'+str(int(d_80u) )+'</ITRForm:Section80UUsrType>\n'
	c += '<ITRForm:Section80U>'+str(int(d_80u) )+'</ITRForm:Section80U>\n'
	# c += '<ITRForm:Section80RRB>'+str(int(d_80rrb) )+'</ITRForm:Section80RRB>\n'
	# c += '<ITRForm:Section80QQB>'+str(int(d_80qqb) )+'</ITRForm:Section80QQB>\n'
	c += '<ITRForm:Section80CCG>'+str(int(d_80ccg) )+'</ITRForm:Section80CCG>\n'
	c += '<ITRForm:Section80TTA>'+str(int(d_80tta) )+'</ITRForm:Section80TTA>\n'
	c += '<ITRForm:Section80TTB>'+str(int(d_80ttb) )+'</ITRForm:Section80TTB>\n'
	c += '<ITRForm:TotalChapVIADeductions>'+str(int(total_deduction) )+'</ITRForm:TotalChapVIADeductions>\n'
	c += '</ITRForm:UsrDeductUndChapVIA>\n'

	c += '<ITRForm:DeductUndChapVIA>\n'
	limited_deduction = 0
	if d_80c>150000:
		limited_deduction += 150000
		c += '<ITRForm:Section80C>150000</ITRForm:Section80C>\n'
	else:
		limited_deduction += d_80c
		c += '<ITRForm:Section80C>'+str(int(d_80c) )+'</ITRForm:Section80C>\n'
	c += '<ITRForm:Section80CCC>'+str(int(d_80ccc) )+'</ITRForm:Section80CCC>\n'
	c += '<ITRForm:Section80CCDEmployeeOrSE>'+str(int(d_80ccd1) )+'</ITRForm:Section80CCDEmployeeOrSE>\n'
	limited_deduction += d_80ccc
	if d_80ccd>50000:
		c += '<ITRForm:Section80CCD1B>50000</ITRForm:Section80CCD1B>\n'
		limited_deduction += 50000
	else:
		c += '<ITRForm:Section80CCD1B>'+str(int(d_80ccd) )+'</ITRForm:Section80CCD1B>\n'
		limited_deduction += d_80ccd
	c += '<ITRForm:Section80CCDEmployer>'+str(int(d_80ccd2) )+'</ITRForm:Section80CCDEmployer>\n'
	
	if age<60:
		if d_80d>25000:
			c += '<ITRForm:Section80D>25000</ITRForm:Section80D>\n'
			limited_deduction += 30000
		else:
			c += '<ITRForm:Section80D>'+str(int(d_80d) )+'</ITRForm:Section80D>\n'
			limited_deduction += d_80d
	else:
		if d_80d>50000:
			c += '<ITRForm:Section80D>50000</ITRForm:Section80D>\n'
			limited_deduction += 50000
		else:
			c += '<ITRForm:Section80D>'+str(int(d_80d) )+'</ITRForm:Section80D>\n'
			limited_deduction += d_80d

	c += '<ITRForm:Section80DD>'+str(int(d_80dd) )+'</ITRForm:Section80DD>\n'
	limited_deduction += d_80dd
	c += '<ITRForm:Section80DDB>'+str(int(d_80ddb) )+'</ITRForm:Section80DDB>\n'
	c += '<ITRForm:Section80E>'+str(int(d_80e) )+'</ITRForm:Section80E>\n'
	c += '<ITRForm:Section80EE>'+str(int(d_80ee) )+'</ITRForm:Section80EE>\n'
	c += '<ITRForm:Section80G>'+str(int(d_80g) )+'</ITRForm:Section80G>\n'
	c += '<ITRForm:Section80GG>'+str(int(d_80gg) )+'</ITRForm:Section80GG>\n'
	c += '<ITRForm:Section80GGA>'+str(int(d_80gga) )+'</ITRForm:Section80GGA>\n'
	c += '<ITRForm:Section80GGC>'+str(int(d_80ggc) )+'</ITRForm:Section80GGC>\n'
	limited_deduction += d_80ddb + d_80e + d_80ee + d_80g + d_80gg + d_80gga + d_80ggc
	
	c += '<ITRForm:Section80U>'+str(int(d_80u) )+'</ITRForm:Section80U>\n'
	limited_deduction += d_80u
	# c += '<ITRForm:Section80RRB>'+str(int( d_80rrb) )+'</ITRForm:Section80RRB>\n'
	# c += '<ITRForm:Section80QQB>'+str(int( d_80qqb) )+'</ITRForm:Section80QQB>\n'
	c += '<ITRForm:Section80CCG>'+str(int( d_80ccg) )+'</ITRForm:Section80CCG>\n'
	limited_deduction += d_80rrb + d_80qqb + d_80ccg
	if d_80tta > 10000:
		c += '<ITRForm:Section80TTA>10000</ITRForm:Section80TTA>\n'
		limited_deduction += 10000
	else:
		c += '<ITRForm:Section80TTA>'+str(int( d_80tta ) )+'</ITRForm:Section80TTA>\n'
		limited_deduction += d_80tta
	if d_80ttb > 50000:
		c += '<ITRForm:Section80TTB>50000</ITRForm:Section80TTB>\n'
		limited_deduction += 50000
	else:
		c += '<ITRForm:Section80TTB>'+str(int( d_80ttb ) )+'</ITRForm:Section80TTB>\n'
		limited_deduction += d_80ttb
	# c += '<ITRForm:Section80TTB>0</ITRForm:Section80TTB>\n'
	c += '<ITRForm:TotalChapVIADeductions>'+str(int( limited_deduction ) )+'</ITRForm:TotalChapVIADeductions>\n'
	c += '</ITRForm:DeductUndChapVIA>\n'
	c += '<ITRForm:TotalIncome>'+str(int(GrossTotIncome-limited_deduction))+'</ITRForm:TotalIncome>\n'

	ExemptIncAgriOthUs10Total = int(agri_income) + int(dividend) + int(other_exempt_income) + int(Tax_Free_Interest) + int(lt_shares_gain) + int(lt_equity_gain)
	if ExemptIncAgriOthUs10Total > 0:
		c += '<ITRForm:ExemptIncAgriOthUs10>\n'
		if int(agri_income) > 0:
			c += '<ITRForm:ExemptIncAgriOthUs10Dtls>\n'
			c += '<ITRForm:NatureDesc>AGRI</ITRForm:NatureDesc>\n'
			c += '<ITRForm:OthAmount>'+str(int(agri_income) )+'</ITRForm:OthAmount>\n'
			c += '</ITRForm:ExemptIncAgriOthUs10Dtls>\n'
		# if int(insurance) > 0:
		# 	c += '<ITRForm:ExemptIncAgriOthUs10Dtls>\n'
		# 	c += '<ITRForm:NatureDesc>10(10D)</ITRForm:NatureDesc>\n'
		# 	c += '<ITRForm:OthAmount>'+str(int(insurance) )+'</ITRForm:OthAmount>\n'
		# 	c += '</ITRForm:ExemptIncAgriOthUs10Dtls>\n'
		if int(dividend) + int(other_exempt_income) + int(Tax_Free_Interest) + int(lt_shares_gain) + int(lt_equity_gain) > 0:
			c += '<ITRForm:ExemptIncAgriOthUs10Dtls>\n'
			c += '<ITRForm:NatureDesc>10(34)</ITRForm:NatureDesc>\n'
			c += '<ITRForm:OthNatOfInc>Any Other</ITRForm:OthNatOfInc>\n'
			c += '<ITRForm:OthAmount>'+str(int(insurance) )+'</ITRForm:OthAmount>\n'
			c += '</ITRForm:ExemptIncAgriOthUs10Dtls>\n'

		c += '<ITRForm:ExemptIncAgriOthUs10Total>'+str(int(ExemptIncAgriOthUs10Total) )+'</ITRForm:ExemptIncAgriOthUs10Total>\n'
		c += '</ITRForm:ExemptIncAgriOthUs10>\n'

	c += '</ITRForm:ITR1_IncomeDeductions>\n'

	c += '<ITRForm:ITR1_TaxComputation>\n'
	c += '<ITRForm:TotalTaxPayable>'+str(int(IT_normal_rates) )+'</ITRForm:TotalTaxPayable>\n'
	c += '<ITRForm:Rebate87A>'+str(int(rebate) )+'</ITRForm:Rebate87A>\n'
	# c += '<ITRForm:TaxPayableOnRebate>'+str(int( (Tx+Tx1-0) - rebate ))+'</ITRForm:TaxPayableOnRebate>\n'

	c += '<ITRForm:TaxPayableOnRebate>'+str(int( (IT_normal_rates+Tx1-0) - rebate ))+'</ITRForm:TaxPayableOnRebate>\n'
	c += '<ITRForm:EducationCess>'+str(int(educess) )+'</ITRForm:EducationCess>\n'
	c += '<ITRForm:GrossTaxLiability>'+str(int(IT_normal_rates-rebate+educess) )+'</ITRForm:GrossTaxLiability>\n'
	c += '<ITRForm:Section89>0</ITRForm:Section89>\n'
	c += '<ITRForm:NetTaxLiability>'+str(int((IT_normal_rates-rebate+educess)-0) )+'</ITRForm:NetTaxLiability>\n'
	# 0 is Section89

	log.info('interest_234A '+str(interest_234A))
	log.info('interest_234B '+str(interest_234B))
	log.info('interest_234C '+str(interest_234C))
	log.info('interest_234F '+str(interest_234F))
	log.info('total_interest '+str(total_interest))

	c += '<ITRForm:TotalIntrstPay>'+str(int(total_interest))+'</ITRForm:TotalIntrstPay>\n'
	c += '<ITRForm:IntrstPay>\n'
	c += '<ITRForm:IntrstPayUs234A>'+str(int(interest_234A))+'</ITRForm:IntrstPayUs234A>\n'
	c += '<ITRForm:IntrstPayUs234B>'+str(int(interest_234B))+'</ITRForm:IntrstPayUs234B>\n'
	c += '<ITRForm:IntrstPayUs234C>'+str(int(interest_234C))+'</ITRForm:IntrstPayUs234C>\n'
	c += '<ITRForm:LateFilingFee234F>'+str(int(interest_234F))+'</ITRForm:LateFilingFee234F>\n'
	c += '</ITRForm:IntrstPay>\n'
	c += '<ITRForm:TotTaxPlusIntrstPay>'+str(int(IT_normal_rates-rebate+educess) )+'</ITRForm:TotTaxPlusIntrstPay>\n'
	# TotTaxPlusIntrstPay = NetTaxLiability + Add Above Interest A,B,C
	c += '</ITRForm:ITR1_TaxComputation>\n'

	adv_t, sa_t, tds_value, tcs_value, tax_paid_count, taxes_paid_total_tax = (0,)*6
	tp_bsr_code = []
	tp_deposit_date = []
	tp_challan_no = []
	tp_total_tax = []

	date = datetime(2018,03,31)
	date = date.replace(tzinfo=None)
	tax_paid_arr = get_itr_varibles(client_id,'tax_paid')
	adv_t = tax_paid_arr['adv_t']
	sa_t = tax_paid_arr['sa_t']
	tax_paid_count = tax_paid_arr['tax_paid_count']
	tp_bsr_code = tax_paid_arr['tp_bsr_code']
	tp_deposit_date = tax_paid_arr['tp_deposit_date']
	tp_challan_no = tax_paid_arr['tp_challan_no']
	tp_total_tax = tax_paid_arr['tp_total_tax']
	taxes_paid_total_tax = tax_paid_arr['taxes_paid_total_tax']

	tds_count, total_tds1, tcs_count, total_tcs, tds2_count, total_tds2, tds3_count, total_tds3 = (0,)*8
	tds_dc_name = []
	tds_amt_paid = []
	tds_deducted = []
	tds_TAN_PAN = []
	tcs_dc_name = []
	tcs_amt_paid = []
	tcs_deducted = []
	tcs_TAN_PAN = []
	tds2_dc_name = []
	tds2_amt_paid = []
	tds2_tds_deducted = []
	tds2_TAN_PAN = []
	tds3_dc_name = []
	tds3_amt_paid = []
	tds3_tds_deducted = []
	tds3_TAN_PAN = []

	tds_tcs_arr = get_itr_varibles(client_id,'tds_tcs')
	tds_count = tds_tcs_arr['tds_count']
	total_tds1 = tds_tcs_arr['total_tds1']
	tcs_count = tds_tcs_arr['tcs_count']
	total_tcs = tds_tcs_arr['total_tcs']
	tds2_count = tds_tcs_arr['tds2_count']
	total_tds2 = tds_tcs_arr['total_tds2']
	tds3_count = tds_tcs_arr['tds3_count']
	total_tds3 = tds_tcs_arr['total_tds3']
	tcs_value = tds_tcs_arr['tcs_value']
	tds_value = tds_tcs_arr['tds_value']
	tds_dc_name = tds_tcs_arr['tds_dc_name']
	tds_amt_paid = tds_tcs_arr['tds_amt_paid']
	tds_deducted = tds_tcs_arr['tds_deducted']
	tds_TAN_PAN = tds_tcs_arr['tds_TAN_PAN']
	tcs_dc_name = tds_tcs_arr['tcs_dc_name']
	tcs_amt_paid = tds_tcs_arr['tcs_amt_paid']
	tcs_deducted = tds_tcs_arr['tcs_deducted']
	tcs_TAN_PAN = tds_tcs_arr['tcs_TAN_PAN']
	tds2_dc_name = tds_tcs_arr['tds2_dc_name']
	tds2_amt_paid = tds_tcs_arr['tds2_amt_paid']
	tds2_tds_deducted = tds_tcs_arr['tds2_tds_deducted']
	tds2_TAN_PAN = tds_tcs_arr['tds2_TAN_PAN']
	tds3_dc_name = tds_tcs_arr['tds3_dc_name']
	tds3_amt_paid = tds_tcs_arr['tds3_amt_paid']
	tds3_tds_deducted = tds_tcs_arr['tds3_tds_deducted']
	tds3_TAN_PAN = tds_tcs_arr['tds3_TAN_PAN']

	c += '<ITRForm:TaxPaid>\n'
	c += '<ITRForm:TaxesPaid>\n'
	c += '<ITRForm:AdvanceTax>'+str(adv_t)+'</ITRForm:AdvanceTax>\n'
	c += '<ITRForm:TDS>'+str(tds_value)+'</ITRForm:TDS>\n'
	c += '<ITRForm:TCS>'+str(tcs_value)+'</ITRForm:TCS>\n'
	c += '<ITRForm:SelfAssessmentTax>'+str(sa_t)+'</ITRForm:SelfAssessmentTax>\n'
	c += '<ITRForm:TotalTaxesPaid>'+str(adv_t+tds_value+tcs_value+sa_t)+'</ITRForm:TotalTaxesPaid>\n'

	c += '</ITRForm:TaxesPaid>\n'

	if ( IT_normal_rates+Tx1 + surcharge + educess ) - (tds_value+tcs_value+adv_t+sa_t) >0:
	# if (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t)>0:
		c += '<ITRForm:BalTaxPayable>'+str(int( (IT_normal_rates-rebate+educess)-(adv_t+tds_value+tcs_value+sa_t) ))+'</ITRForm:BalTaxPayable>\n'
	else:
		c += '<ITRForm:BalTaxPayable>0</ITRForm:BalTaxPayable>\n'
	# if(TotTaxPlusIntrstPay - TotalTaxesPaid >0, TotTaxPlusIntrstPay - TotalTaxesPaid, 0)
	c += '</ITRForm:TaxPaid>\n'

	c += '<ITRForm:Refund>\n'
	# if (ttp+rebate+educess)-(adv_t+tds_value+tcs_value+sa_t)<0:
	if ( IT_normal_rates+Tx1 + surcharge + educess ) - (tds_value+tcs_value+adv_t) <0:
		c += '<ITRForm:RefundDue>'+str(int(abs( (IT_normal_rates-rebate+educess)-(adv_t+tds_value+tcs_value) ) ))+'</ITRForm:RefundDue>\n'
	else:
		c += '<ITRForm:RefundDue>0</ITRForm:RefundDue>\n'
	c += '<ITRForm:BankAccountDtls>\n'
	if no_of_bank >= 0:
		# c += '<ITRForm:PriBankDetails>\n'
		# # for x in xrange(0,no_of_bank):
		# c += '<ITRForm:IFSCCode>'+str(IFSC[0])+'</ITRForm:IFSCCode>\n'
		# c += '<ITRForm:BankName>'+str(bank_name[0])+'</ITRForm:BankName>\n'
		# c += '<ITRForm:BankAccountNo>'+str(acc_no[0])+'</ITRForm:BankAccountNo>\n'
		# c += '</ITRForm:PriBankDetails>\n'
		# if no_of_bank > 1:
			for x in xrange(0,no_of_bank):
				c += '<ITRForm:AddtnlBankDetails>\n'
				c += '<ITRForm:IFSCCode>'+str(IFSC[x])+'</ITRForm:IFSCCode>\n'
				c += '<ITRForm:BankName>'+str(bank_name[x])+'</ITRForm:BankName>\n'
				c += '<ITRForm:BankAccountNo>'+str(acc_no[x])+'</ITRForm:BankAccountNo>\n'
				if x==0:
					c += '<ITRForm:UseForRefund>true</ITRForm:UseForRefund>\n'
				else:
					c += '<ITRForm:UseForRefund>false</ITRForm:UseForRefund>\n'

				c += '</ITRForm:AddtnlBankDetails>\n'
	# else:
	# 	c += '<ITRForm:PriBankDetails>\n'
	# 	c += '<ITRForm:IFSCCode></ITRForm:IFSCCode>\n'
	# 	c += '<ITRForm:BankName></ITRForm:BankName>\n'
	# 	c += '<ITRForm:BankAccountNo></ITRForm:BankAccountNo>\n'
	# 	c += '</ITRForm:PriBankDetails>\n'

	c += '</ITRForm:BankAccountDtls>\n'
	c += '</ITRForm:Refund>\n'

	# c += '<ITRForm:Schedule80G>\n'
	# c += '<ITRForm:TotalDonationsUs80GCash>0</ITRForm:TotalDonationsUs80GCash>\n'
	# c += '<ITRForm:TotalDonationsUs80GOtherMode>0</ITRForm:TotalDonationsUs80GOtherMode>\n'
	# c += '<ITRForm:TotalDonationsUs80G>0</ITRForm:TotalDonationsUs80G>\n'
	# c += '<ITRForm:TotalEligibleDonationsUs80G>0</ITRForm:TotalEligibleDonationsUs80G>\n'
	# c += '</ITRForm:Schedule80G>\n'
	d_name = []
	d_address = []
	d_city = []
	d_state_code = []
	d_pin_code = []
	d_pan = []
	d_amt = []
	d_percent = []
	total_donation, donation_count = (0,)*2
	donation_arr = get_itr_varibles(client_id,'donation')
	d_name = donation_arr['d_name']
	d_address = donation_arr['d_address']
	d_city = donation_arr['d_city']
	d_state_code = donation_arr['d_state_code']
	d_pin_code = donation_arr['d_pin_code']
	d_pan = donation_arr['d_pan']
	d_amt = donation_arr['d_amt']
	d_percent = donation_arr['d_percent']
	total_donation = donation_arr['total_donation']
	donation_count = donation_arr['donation_count']
	EligibleDonationAmt=0
	DonationAmt=0
	TotalEligibleDonationAmt=0
	total_donation_amount=0
	total_eligible_amount=0

	log.info(d_percent)
	c += '<ITRForm:Schedule80G>\n'
	if int(total_donation) > 0 and 100.0 in d_percent:
		c += '<ITRForm:Don100Percent>\n'
		for x in xrange(0,donation_count):
			if d_percent[x]==100.0:
				c += '<ITRForm:DoneeWithPan>\n'
				c += '<ITRForm:DoneeWithPanName>'+str(d_name[x])+'</ITRForm:DoneeWithPanName>\n'
				c += '<ITRForm:DoneePAN>'+str(d_pan[x])+'</ITRForm:DoneePAN>\n'
				c += '<ITRForm:AddressDetail>\n'
				c += '<ITRForm:AddrDetail>'+str(d_address[x])+'</ITRForm:AddrDetail>\n'
				c += '<ITRForm:CityOrTownOrDistrict>'+str(d_city[x])+'</ITRForm:CityOrTownOrDistrict>\n'
				c += '<ITRForm:StateCode>'+str(d_state_code[x])+'</ITRForm:StateCode>\n'
				c += '<ITRForm:PinCode>'+str(d_pin_code[x])+'</ITRForm:PinCode>\n'
				c += '</ITRForm:AddressDetail>\n'
				c += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
				c += '<ITRForm:DonationAmtOtherMode>'+str(int(d_amt[x]))+'</ITRForm:DonationAmtOtherMode>\n'
				c += '<ITRForm:DonationAmt>'+str(int(d_amt[x]))+'</ITRForm:DonationAmt>\n'
				
				DonationAmt += int(d_amt[x])
				TotalEligibleDonationAmt += int(d_amt[x])
				c += '<ITRForm:EligibleDonationAmt>'+str(int(d_amt[x]))+'</ITRForm:EligibleDonationAmt>\n'
				c += '</ITRForm:DoneeWithPan>\n'
		
		total_donation_amount += DonationAmt
		total_eligible_amount += TotalEligibleDonationAmt
		c += '<ITRForm:TotDon100PercentCash>0</ITRForm:TotDon100PercentCash>\n'
		c += '<ITRForm:TotDon100PercentOtherMode>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotDon100PercentOtherMode>\n'
		c += '<ITRForm:TotDon100Percent>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotDon100Percent>\n'
		c += '<ITRForm:TotEligibleDon100Percent>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotEligibleDon100Percent>\n'
		c += '</ITRForm:Don100Percent>\n'

	if int(total_donation) > 0 and 50.0 in d_percent:
		c += '<ITRForm:Don50PercentNoApprReqd>\n'
		for x in xrange(0,donation_count):
			if d_percent[x]==50.0:
				c += '<ITRForm:DoneeWithPan>\n'
				c += '<ITRForm:DoneeWithPanName>'+str(d_name[x])+'</ITRForm:DoneeWithPanName>\n'
				c += '<ITRForm:DoneePAN>'+str(d_pan[x])+'</ITRForm:DoneePAN>\n'
				c += '<ITRForm:AddressDetail>\n'
				c += '<ITRForm:AddrDetail>'+str(d_address[x])+'</ITRForm:AddrDetail>\n'
				c += '<ITRForm:CityOrTownOrDistrict>'+str(d_city[x])+'</ITRForm:CityOrTownOrDistrict>\n'
				c += '<ITRForm:StateCode>'+str(d_state_code[x])+'</ITRForm:StateCode>\n'
				c += '<ITRForm:PinCode>'+str(d_pin_code[x])+'</ITRForm:PinCode>\n'
				c += '</ITRForm:AddressDetail>\n'
				c += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
				c += '<ITRForm:DonationAmtOtherMode>'+str(2*int(d_amt[x]))+'</ITRForm:DonationAmtOtherMode>\n'
				c += '<ITRForm:DonationAmt>'+str(2*int(d_amt[x]))+'</ITRForm:DonationAmt>\n'
				
				DonationAmt += 2*int(d_amt[x])
				TotalEligibleDonationAmt += int(d_amt[x])
				c += '<ITRForm:EligibleDonationAmt>'+str(int(d_amt[x]))+'</ITRForm:EligibleDonationAmt>\n'
				c += '</ITRForm:DoneeWithPan>\n'
		
		total_donation_amount += DonationAmt
		total_eligible_amount += TotalEligibleDonationAmt
		c += '<ITRForm:TotDon50PercentNoApprReqdCash>0</ITRForm:TotDon50PercentNoApprReqdCash>\n'
		c += '<ITRForm:TotDon50PercentNoApprReqdOtherMode>'+str(2*int(TotalEligibleDonationAmt))+'</ITRForm:TotDon50PercentNoApprReqdOtherMode>\n'
		c += '<ITRForm:TotDon50PercentNoApprReqd>'+str(2*int(TotalEligibleDonationAmt))+'</ITRForm:TotDon50PercentNoApprReqd>\n'
		c += '<ITRForm:TotEligibleDon50Percent>'+str(int(TotalEligibleDonationAmt))+'</ITRForm:TotEligibleDon50Percent>\n'
		c += '</ITRForm:Don50PercentNoApprReqd>\n'
		
	# c += '<ITRForm:Don100PercentApprReqd>\n'
	# c += '<ITRForm:DoneeWithPan>\n'
	# c += '<ITRForm:DoneeWithPanName>0</ITRForm:DoneeWithPanName>\n'
	# c += '<ITRForm:DoneePAN>0</ITRForm:DoneePAN>\n'
	# c += '<ITRForm:AddressDetail>\n'
	# c += '<ITRForm:AddrDetail>0</ITRForm:AddrDetail>\n'
	# c += '<ITRForm:CityOrTownOrDistrict>0</ITRForm:CityOrTownOrDistrict>\n'
	# c += '<ITRForm:StateCode>0</ITRForm:StateCode>\n'
	# c += '<ITRForm:PinCode>0</ITRForm:PinCode>\n'
	# c += '</ITRForm:AddressDetail>\n'
	# c += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
	# c += '<ITRForm:DonationAmtOtherMode>0</ITRForm:DonationAmtOtherMode>\n'
	# c += '<ITRForm:DonationAmt>0</ITRForm:DonationAmt>\n'
	# c += '<ITRForm:EligibleDonationAmt>0</ITRForm:EligibleDonationAmt>\n'
	# c += '</ITRForm:DoneeWithPan>\n'
	# c += '<ITRForm:TotDon100PercentApprReqdCash>0</ITRForm:TotDon100PercentApprReqdCash>\n'
	# c += '<ITRForm:TotDon100PercentApprReqdOtherMode>0</ITRForm:TotDon100PercentApprReqdOtherMode>\n'
	# c += '<ITRForm:TotDon100PercentApprReqd>0</ITRForm:TotDon100PercentApprReqd>\n'
	# c += '<ITRForm:TotEligibleDon100PercentApprReqd>0</ITRForm:TotEligibleDon100PercentApprReqd>\n'
	# c += '</ITRForm:Don100PercentApprReqd>\n'
		
	# c += '<ITRForm:Don50PercentApprReqd>\n'
	# c += '<ITRForm:DoneeWithPan>\n'
	# c += '<ITRForm:DoneeWithPanName>0</ITRForm:DoneeWithPanName>\n'
	# c += '<ITRForm:DoneePAN>0</ITRForm:DoneePAN>\n'
	# c += '<ITRForm:AddressDetail>\n'
	# c += '<ITRForm:AddrDetail>0</ITRForm:AddrDetail>\n'
	# c += '<ITRForm:CityOrTownOrDistrict>0</ITRForm:CityOrTownOrDistrict>\n'
	# c += '<ITRForm:StateCode>0</ITRForm:StateCode>\n'
	# c += '<ITRForm:PinCode>0</ITRForm:PinCode>\n'
	# c += '</ITRForm:AddressDetail>\n'
	# c += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
	# c += '<ITRForm:DonationAmtOtherMode>0</ITRForm:DonationAmtOtherMode>\n'
	# c += '<ITRForm:DonationAmt>0</ITRForm:DonationAmt>\n'
	# c += '<ITRForm:EligibleDonationAmt>0</ITRForm:EligibleDonationAmt>\n'
	# c += '</ITRForm:DoneeWithPan>\n'
	# c += '<ITRForm:TotDon50PercentApprReqdCash>0</ITRForm:TotDon50PercentApprReqdCash>\n'
	# c += '<ITRForm:TotDon50PercentApprReqdOtherMode>0</ITRForm:TotDon50PercentApprReqdOtherMode>\n'
	# c += '<ITRForm:TotDon50PercentApprReqd>0</ITRForm:TotDon50PercentApprReqd>\n'
	# c += '<ITRForm:TotEligibleDon50PercentApprReqd>0</ITRForm:TotEligibleDon50PercentApprReqd>\n'
	# c += '</ITRForm:Don50PercentApprReqd>\n'

	c += '<ITRForm:TotalDonationsUs80GCash>0</ITRForm:TotalDonationsUs80GCash>\n'
	c += '<ITRForm:TotalDonationsUs80GOtherMode>'+str(total_donation_amount)+'</ITRForm:TotalDonationsUs80GOtherMode>\n'
	c += '<ITRForm:TotalDonationsUs80G>'+str(total_donation_amount)+'</ITRForm:TotalDonationsUs80G>\n'
	c += '<ITRForm:TotalEligibleDonationsUs80G>'+str(total_eligible_amount)+'</ITRForm:TotalEligibleDonationsUs80G>\n'
	c += '</ITRForm:Schedule80G>\n'

	if int(d_80gga) > 0:
		c += '<ITRForm:Schedule80GGA>\n'
		c += '<ITRForm:DonationDtlsSciRsrchRuralDev>\n'
		c += '<ITRForm:RelevantClauseUndrDedClaimed>0</ITRForm:RelevantClauseUndrDedClaimed>\n'
		c += '<ITRForm:NameOfDonee>0</ITRForm:NameOfDonee>\n'
		c += '<ITRForm:AddressDetail>\n'
		c += '<ITRForm:AddrDetail>0</ITRForm:AddrDetail>\n'
		c += '<ITRForm:CityOrTownOrDistrict>0</ITRForm:CityOrTownOrDistrict>\n'
		c += '<ITRForm:StateCode>0</ITRForm:StateCode>\n'
		c += '<ITRForm:PinCode>0</ITRForm:PinCode>\n'
		c += '</ITRForm:AddressDetail>\n'
		c += '<ITRForm:DoneePAN>0</ITRForm:DoneePAN>\n'
		c += '<ITRForm:DonationAmtCash>0</ITRForm:DonationAmtCash>\n'
		c += '<ITRForm:DonationAmtOtherMode>0</ITRForm:DonationAmtOtherMode>\n'
		c += '<ITRForm:DonationAmt>0</ITRForm:DonationAmt>\n'
		c += '<ITRForm:EligibleDonationAmt>0</ITRForm:EligibleDonationAmt>\n'
		c += '</ITRForm:DonationDtlsSciRsrchRuralDev>\n'
		c += '<ITRForm:TotalDonationAmtCash80GGA>0</ITRForm:TotalDonationAmtCash80GGA>\n'
		c += '<ITRForm:TotalDonationAmtOtherMode80GGA>0</ITRForm:TotalDonationAmtOtherMode80GGA>\n'
		c += '<ITRForm:TotalDonationsUs80GGA>0</ITRForm:TotalDonationsUs80GGA>\n'
		c += '<ITRForm:TotalEligibleDonationAmt80GGA>0</ITRForm:TotalEligibleDonationAmt80GGA>\n'
		c += '</ITRForm:Schedule80GGA>\n'

	log.info('ITR-1 : '+pan)
	if tds_count!=0:
		c += '<ITRForm:TDSonSalaries>\n'
		for x in xrange(0,tds_count):
			c += '<ITRForm:TDSonSalary>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:TAN>'+tds_TAN_PAN[x]+'</ITRForm:TAN>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollecterName>'+str(tds_dc_name[x])+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
			c += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:IncChrgSal>'+str(tds_amt_paid[x])+'</ITRForm:IncChrgSal>\n'
			c += '<ITRForm:TotalTDSSal>'+str(tds_deducted[x])+'</ITRForm:TotalTDSSal>\n'
			c += '</ITRForm:TDSonSalary>\n'
		c += '<ITRForm:TotalTDSonSalaries>'+str(total_tds1)+'</ITRForm:TotalTDSonSalaries>\n'
		c += '</ITRForm:TDSonSalaries>\n'
	
	if tds2_count!=0:
		c += '<ITRForm:TDSonOthThanSals>\n'
		for x in xrange(0,tds2_count):
			c += '<ITRForm:TDSonOthThanSal>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:TAN>'+tds2_TAN_PAN[x]+'</ITRForm:TAN>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollecterName>'+tds2_dc_name[x]+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
			c += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:AmtForTaxDeduct>'+str(tds2_amt_paid[x])+'</ITRForm:AmtForTaxDeduct>\n'
			c += '<ITRForm:DeductedYr>2018</ITRForm:DeductedYr>\n'
			c += '<ITRForm:TotTDSOnAmtPaid>'+str(tds2_tds_deducted[x])+'</ITRForm:TotTDSOnAmtPaid>\n'
			c += '<ITRForm:ClaimOutOfTotTDSOnAmtPaid>'+str(tds2_tds_deducted[x])+'</ITRForm:ClaimOutOfTotTDSOnAmtPaid>\n'
			c += '</ITRForm:TDSonOthThanSal>\n'
		c += '<ITRForm:TotalTDSonOthThanSals>'+str(total_tds2)+'</ITRForm:TotalTDSonOthThanSals>\n'
		c += '</ITRForm:TDSonOthThanSals>\n'
	
	if tds3_count > 0:
		c += '<ITRForm:ScheduleTDS3Dtls>\n'
		for x in xrange(0,tds3_count):
			c += '<ITRForm:TDS3Details>\n'
			c += '<ITRForm:PANofTenant>'+str(tds3_TAN_PAN[x])+'</ITRForm:PANofTenant>\n'
			c += '<ITRForm:NameOfTenant>'+str(tds3_dc_name[x])+'</ITRForm:NameOfTenant>\n'
			c += '<ITRForm:GrsRcptToTaxDeduct>'+str(tds3_amt_paid[x])+'</ITRForm:GrsRcptToTaxDeduct>\n'
			c += '<ITRForm:DeductedYr>2018</ITRForm:DeductedYr>\n'
			c += '<ITRForm:TDSDeducted>'+str(tds3_tds_deducted[x])+'</ITRForm:TDSDeducted>\n'
			c += '<ITRForm:TDSClaimed>'+str(tds3_tds_deducted[x])+'</ITRForm:TDSClaimed>\n'
			c += '</ITRForm:TDS3Details>\n'
		c += '<ITRForm:TotalTDS3Details>'+str(total_tds3)+'</ITRForm:TotalTDS3Details>\n'
		c += '</ITRForm:ScheduleTDS3Dtls>\n'

	if tcs_count>0:
		c += '<ITRForm:ScheduleTCS>\n'
		for x in xrange(0,tcs_count):
			c += '<ITRForm:TCS>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:TAN>'+tcs_TAN_PAN[x]+'</ITRForm:TAN>\n'
			c += '<ITRForm:EmployerOrDeductorOrCollecterName>'+tcs_dc_name[x]+'</ITRForm:EmployerOrDeductorOrCollecterName>\n'
			c += '</ITRForm:EmployerOrDeductorOrCollectDetl>\n'
			c += '<ITRForm:AmtTaxCollected>'+str(tcs_amt_paid[x])+'</ITRForm:AmtTaxCollected>\n'
			c += '<ITRForm:CollectedYr>2017</ITRForm:CollectedYr>\n'
			c += '<ITRForm:TotalTCS>'+str(tcs_amt_paid[x])+'</ITRForm:TotalTCS>\n'
			c += '<ITRForm:AmtTCSClaimedThisYear>'+str(tcs_amt_paid[x])+'</ITRForm:AmtTCSClaimedThisYear>\n'
			c += '</ITRForm:TCS>\n'
		c += '<ITRForm:TotalSchTCS>'+str(total_tcs)+'</ITRForm:TotalSchTCS>\n'
		c += '</ITRForm:ScheduleTCS>\n'

	if tax_paid_count != 0:
		try:
			c += '<ITRForm:TaxPayments>\n'
			log.info(tp_bsr_code)
			for x in range(len(tp_bsr_code)):
				c += '<ITRForm:TaxPayment>\n'
				if len(str(tp_bsr_code[x]))==6:
					BSRCode='0'+str(tp_bsr_code[x])
				else:
					BSRCode=tp_bsr_code[x]
				c += '<ITRForm:BSRCode>'+str(BSRCode)+'</ITRForm:BSRCode>\n'
				c += '<ITRForm:DateDep>'+str(tp_deposit_date[x])+'</ITRForm:DateDep>\n'
				c += '<ITRForm:SrlNoOfChaln>'+str(tp_challan_no[x])+'</ITRForm:SrlNoOfChaln>\n'
				c += '<ITRForm:Amt>'+str(tp_total_tax[x])+'</ITRForm:Amt>\n'
				c += '</ITRForm:TaxPayment>\n'
		except Exception as ex:
			log.info('Error in tax payment block '+str(traceback.format_exc()))

		c += '<ITRForm:TotalTaxPayments>'+str(taxes_paid_total_tax)+'</ITRForm:TotalTaxPayments>\n'
		c += '</ITRForm:TaxPayments>\n'

	# c += '<ITR1FORM:TaxExmpIntInc>0</ITR1FORM:TaxExmpIntInc>\n'

	c += '<ITRForm:Verification>\n'
	c += '<ITRForm:Declaration>\n'
	# c += '<ITRForm:AssesseeVerName>'+fname+' '+lname+'</ITRForm:AssesseeVerName>\n'
	if mname == '':
		c += '<ITRForm:AssesseeVerName>'+fname+' '+lname+'</ITRForm:AssesseeVerName>\n'
	else:
		c += '<ITRForm:AssesseeVerName>'+fname+' '+mname+' '+lname+'</ITRForm:AssesseeVerName>\n'

	if fathername == '':
		c += '<ITRForm:FatherName>'+lname+'</ITRForm:FatherName>\n'
	else:
		c += '<ITRForm:FatherName>'+fathername+'</ITRForm:FatherName>\n'

	c += '<ITRForm:AssesseeVerPAN>'+PAN+'</ITRForm:AssesseeVerPAN>\n'
	c += '</ITRForm:Declaration>\n'
	c += '<ITRForm:Capacity>S</ITRForm:Capacity>\n'
	c += '<ITRForm:Place>'+city+'</ITRForm:Place>\n'
	# c += '<ITRForm:Date>'+CreationDate+'</ITRForm:Date>\n'
	c += '</ITRForm:Verification>\n'
	
	c += '</ITR1FORM:ITR1>\n'
	c += '</ITRETURN:ITR>\n'
	return c


@csrf_exempt
def generate_xml(request):
	if request.method=='POST':
		result={}
		try:
			software_id = "SW10001898"
			client_id=request.POST.get('client_id')
			PAN = 'PAN'
			for client in Personal_Details.objects.filter(P_Id = client_id):
				PAN = client.pan

			R_ID = get_rid(client_id)
			digest_code = ''

			ITR = 2
			no_HP, dividend, other_exempt_income, taxable_income, capital_gain = (0,)*5
			no_HP = House_Property_Details.objects.filter(R_Id=R_ID).count()

			if Exempt_Income.objects.filter(R_Id = R_ID).exists():
				for client in Exempt_Income.objects.filter(R_Id = R_ID):
					dividend = client.Tax_Free_Dividend or 0
					other_exempt_income = client.Other_exempt_income_Id or 0

			if computation.objects.filter(R_Id = R_ID).exists():
				for client in computation.objects.filter(R_Id = R_ID):
					taxable_income = client.taxable_income or 0

			if Shares_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Shares_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Equity_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Equity_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
				capital_gain = 1

			# House Property < 2 OR (Dividend Income + Any other Exempt Income) < 5000 
			# OR Taxable Income < Rs. 50,00,000/-OR No Capital Gain then ITR - 1 else ITR 2
			condition_itr2 = 0

			if no_HP < 2:
				ITR = 1
			else:
				condition_itr2 = 1
			if (dividend + other_exempt_income) < 5000:
				ITR = 1
			else:
				condition_itr2 = 1
			if taxable_income < 5000000:
				ITR = 1
			else:
				condition_itr2 = 1
			if capital_gain == 0:
				ITR = 1
			else:
				condition_itr2 = 1
			if condition_itr2 == 1:
				ITR = 2

			MYDIR = os.path.dirname(__file__)
			# file = open(MYDIR+'/xml/'+PAN+'.xml','w')
			return_year = '2019'
			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]

			# log.info('return_year'+return_year)
			filename = ''
			filepath = ''
			zipname = ''
			if ITR == 1:
				filename = PAN+'_ITR-1_'+return_year+'_N_1234.xml'
				filepath = selenium_path+'/xml/'+filename
				file = open(filepath,'w')
				if return_year == '2019':
					content = xml_content_2019(client_id,request,software_id)
				else:
					content = xml_content(client_id,request,software_id)
				# content = content.encode(encoding="utf-8",errors="strict")
				content=content.replace('&','AND')
				file.write(content)
				zipname = PAN+'_ITR-1_'+return_year+'_N_1234.zip'
				log.info(filepath)
			else:
				log.info('ITR2')
				filename = PAN+'_ITR-2_'+return_year+'_N_1234.xml'
				filepath = selenium_path+'/xml/'+filename
				file = open(filepath,'w')
				content2 = xml_content2(client_id,request,software_id)
				# content2 = content2.encode(encoding="utf-8",errors="strict")
				content2=content2.replace('&','AND')
				file.write(content2)
				zipname = PAN+'_ITR-2_'+return_year+'_N_1234.zip'
				log.info(filepath)
			
			file.close()
			# file_paths = get_all_file_paths(selenium_path+'/xml') 
			digest_code = generating_hash(filename)
			log.info(digest_code)
			# log.info(digest_code.split('\n'))
			# digest_code = digest_code.replace(unicode('↵',"utf-8") ,'' )
			digest_code = digest_code.replace('\n' ,'' )
			if "Exception" not in digest_code :
				if "Error" not in digest_code:
					log.info('start replacing Digest code')
					text_to_search = '<ITRForm:Digest>-</ITRForm:Digest>'
					text_to_replace = '<ITRForm:Digest>'+digest_code+'</ITRForm:Digest>'
					filedata = ''
					# Read in the file
					with open(filepath, 'r') as file :
						filedata = file.read()

					# Replace the target string
					filedata = filedata.replace(text_to_search ,text_to_replace )

					# Write the file out again
					with open(filepath, 'w') as file:
						file.write(filedata)
					file.close()
			else:
				log.info('Exception in digest code')
			
			os.chdir(selenium_path+'/xml')
			ZipFile(zipname, 'w').write(filename)

			try:
				encodings = ['utf-8','utf-7','utf-16','ascii','ISO-8859-1','ISO-8859-2','ISO-8859-15','UTF8','UTF7','UTF16','us-ascii']
				# for e in encodings:
				# 	try:
				# 		f = codecs.open(filepath, encoding=e , errors='strict')
				# 		for line in f:
				# 			pass
				# 		# log.info("Valid "+e)
				# 	except Exception as ex:
				# 		log.info("invalid "+e)
				# # f = codecs.open(filename, encoding='utf-8', errors='strict')
				# # for line in f:
				# # 	pass
				# # log.info("Valid utf-8")
			except UnicodeDecodeError:
				log.info("invalid XML")
			
			# for file_name in file_paths:
			# 	log.info(file_name)

			# writing files to a zipfile 
			# with ZipFile(selenium_path+'/xml/'+PAN+'_ITR-2_'+return_year+'_N_1234.zip','w') as zip: 
				# writing each file one by one 
				# for file in file_paths:
					# zip.write(file) 

			# zipfile.ZipFile(selenium_path+'/xml/'+PAN+'_ITR-2_'+return_year+'_N_1234.zip', mode='w').write(PAN+'_ITR-2_'+return_year+'_N_1234.xml')
			# ZipFile.write(selenium_path+'/xml/'+PAN+'_ITR-2_'+return_year+'_N_1234.xml')
			Return_Details_instance.xml_generated=Now()
			Return_Details_instance.save()
			result['ITR']=ITR
			result['status']= 'success'
			result['client_id']= client_id
			result['no_HP']= no_HP
			result['dividend']= dividend
			result['other_exempt_income']= other_exempt_income
			result['taxable_income']= str(taxable_income)
			result['capital_gain']= capital_gain
			result['ITR']= ITR
			result['filename']= filename
			result['digest_code'] = digest_code
			# result['zip_file'] = zipname
			result['zip_file'] = filename
		except Exception as ex:
			log.error('Error in : '+traceback.format_exc())
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def get_all_file_paths(directory): 
	# initializing empty file paths list 
	file_paths = [] 

	# crawling through directory and subdirectories
	for root, directories, files in os.walk(directory): 
		for filename in files:
			# join the two strings in order to form the full filepath. 
			filepath = os.path.join(root, filename) 
			file_paths.append(filepath) 

	# returning all file paths 
	return file_paths


@csrf_exempt
def html_to_pdf(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')

			# pdfkit.from_url('http://127.0.0.1:8000/NewReview', client_id+'out.pdf')
			pdfkit.from_url('https://salattax.com/NewReview/'+client_id, client_id+'out.pdf')

			result['status']= 'success'
		except Exception as ex:
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_id(request,id):
	if SalatTaxUser.objects.filter(id=id).exists():
		salattaxid = SalatTaxUser.objects.filter(id=id).first()
	else:
		salattaxid = None

	RFCharges_Serializer=RFChargesSerializer(RFChargesDetails.objects.all(),many='true')

	return render(request,'NewReview.html',
	{
		'id':salattaxid,
		'RF_charges':json.dumps(RFCharges_Serializer.data),
		'client_id':id
	})


# /html/body/div[2]/div[3]/div/button[1]
# //*[@id="281"]/td[1]/a
# sudo apt-get install xvfb libfontconfig wkhtmltopdf 2367
# 3001

def unique(list1):
	# intilize a null list
	unique_list = []
	# traverse for all elements
	for x in list1:
		# check if exists in unique_list or not
		if x not in unique_list:
			if x.startswith( '1' ):
				unique_list.append(x)
			if x.startswith( '2' ):
				unique_list.append(x)
	
	return unique_list


def get_partA(worksheet,start,end,pan,part,year,R_ID):
	section_list = []
	for curr_row_tan1 in xrange(start+2,end):
		if worksheet.cell(curr_row_tan1,2).value != '':
			section_list.append(worksheet.cell(curr_row_tan1,2).value)
	section_list = unique(section_list)

	for section in section_list:
		tan1_list = []
		Deductor_Collector_Name = worksheet.cell(start,1).value
		TAN_PAN = worksheet.cell(start,2).value
		no_of_transaction = 0
		amount_paid = 0.0
		tax_deducted = 0.0
		tds_tcs_deposited = 0.0
		DB_entry = 0
		for curr_row_tan1 in xrange(start+2,end):
			if worksheet.cell(curr_row_tan1,2).value == section:
				DB_entry = 1
				# log.info( len(worksheet.cell(curr_row_tan1,7).value) )
				if(len(worksheet.cell(curr_row_tan1,7).value) > 4):
					no_of_transaction += 1
					# log.info(worksheet.cell(curr_row_tan1,9).value)
					amount_paid += float( worksheet.cell(curr_row_tan1,7).value )
					tax_deducted += float( worksheet.cell(curr_row_tan1,8).value )
					tds_tcs_deposited += float( worksheet.cell(curr_row_tan1,9).value )

		if DB_entry == 1:
			if not tds_tcs.objects.filter(R_Id=R_ID,part=part,TAN_PAN=TAN_PAN,section=section,year=year).exists():
				log.info('part '+part+' saved in DB for '+pan)
				tds_tcs.objects.create(
					R_Id=R_ID,
					part=part,
					Deductor_Collector_Name=Deductor_Collector_Name,
					TAN_PAN=TAN_PAN,
					section=section,
					no_of_transaction=no_of_transaction,
					amount_paid=amount_paid,
					tax_deducted=tax_deducted,
					tds_tcs_deposited=tds_tcs_deposited,
					year=year,
				).save()
	return 1

def excel_db(pan,client_id):
	import xlrd
	from dateutil.parser import parse
	from django.db.models import Avg, Max, Min, Sum
	log.info('start excel_db '+pan)
	
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	excel_file = selenium_path+"/zip_26AS/"
	process_26asfile = 0
	# log.info('start excel_to_db')
	if pan != '':
		try:
			for file in os.listdir(excel_file):
				if(file.find('.xls') != -1 and file.find(pan+"-") != -1):
					log.info(file)
					filename = excel_file+file
					year = file.replace(pan+"-","")
					year = year.replace('.xls',"")
					log.info('year-> '+year+', File Name-> '+file)
					workbook = xlrd.open_workbook(filename)
					worksheet = workbook.sheet_by_name('26AS')

					num_rows = worksheet.nrows - 1
					curr_row = -1

					partA_status, partA1_status, partA2_status, partB_status, partC_status, partD_status = (0,)*6
					partE_status, partF_status, partG_status, part_A_count = (0,)*4

					no_of_TAN, no_of_TAN_partA1, no_of_TAN_partA2, no_of_TAN_partB, partA_start, partA_end = (0,)*6

					partA1_start, partA1_end, partA2_start, partA2_end, partB_start, partB_end , partC_start = (0,)*7
					partC_end, partD_start, partD_end, isunicode, A_no_trans, A1_no_trans, A2_no_trans , B_no_trans = (0,)*8
					while curr_row < num_rows:
						curr_row += 1
						row = worksheet.row(curr_row) # get each row, list type
						if( isinstance(row[1].value, unicode) ):
							isunicode = 1
							row_value = row[1].value.encode('utf-8')
						else:
							isunicode = 0
							row_value = row[1].value

						if ( row[1].value == 'PART A - Details of Tax Deducted at Source'):
							partA_status = 1
							partA_start = curr_row
							row2 = worksheet.row(curr_row+2) # get each row, list type
							if (row2[3].value == '*********** No Transactions Present **********'):
								A_no_trans = 1
						if row[1].value == 'PART A1 - Details of Tax Deducted at Source for 15G / 15H':
							partA_status = 0
							partA1_status = 1
							partA1_start = curr_row
							partA_end = curr_row
							row2 = worksheet.row(curr_row+2) # get each row, list type
							if (row2[3].value == '*********** No Transactions Present **********'):
								A1_no_trans = 1
						if row[1].value == 'PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)' :
							partA1_status = 0
							partA2_status = 1
							partA2_start = curr_row
							partA1_end = curr_row
							row2 = worksheet.row(curr_row+2) # get each row, list type
							if (row2[3].value == '*********** No Transactions Present **********'):
								A2_no_trans = 1
						if row[1].value == 'PART B - Details of Tax Collected at Source' :
							partA2_status = 0
							partB_status = 1
							partB_start = curr_row
							partA2_end = curr_row
							row2 = worksheet.row(curr_row+2) # get each row, list type
							if (row2[3].value == '*********** No Transactions Present **********'):
								B_no_trans = 1
						if row[1].value == 'PART C - Details of Tax Paid (other than TDS or TCS)' :
							partB_status = 0
							partC_status = 1
							partC_start = curr_row
							partB_end = curr_row
						if row[1].value == 'PART D - Details of Paid Refund':
							partC_status = 0
							partD_status = 1
							partD_start = curr_row
							partC_end = curr_row
						if row[1].value == 'PART E - Details of AIR Transaction':
							partD_status = 0
							partD_end = curr_row

						if partA_status == 1:
							part_A_count += 1
							if (row[0].value!='' ):
								if row[0].value!='\n':
									no_of_TAN += 1

						if partA1_status == 1:
							if (row[0].value!=''):
								if row[0].value!='\n':
									no_of_TAN_partA1 += 1

						if partA2_status == 1:
							if (row[0].value!=''):
								if row[0].value!='\n':
									no_of_TAN_partA2 += 1
						
						if partB_status == 1:
							if (row[0].value!=''):
								if row[0].value!='\n':
									no_of_TAN_partB += 1

						if partC_status == 1:
							if (row[0].value==1):
								if row[0].value!='\n':
									partC_start = curr_row

						if partD_status == 1:
							# part_D_count += 1
							if (row[0].value==1):
								if row[0].value!='\n':
									partD_start = curr_row

					R_ID = pan
					try:
						ay = str(year)+'-'+str(int(year)+1)
						fy = str(int(year)-1)+'-'+str(int(year))
						log.info('ay : '+ay+' , fy : '+fy)
						max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
						if not Return_Details.objects.filter(P_id=personal_instance,FY = fy).exists():
							Return_Details.objects.create(
								R_id = max_r_id['R_id__max']+1,
								P_id = personal_instance,
								FY = fy,
								AY = ay,
							).save()
							log.info('Return_Details created '+str(max_r_id['R_id__max']+1))

						# log.info('start excel_to_db')
						if Return_Details.objects.filter(P_id=personal_instance,FY = fy).exists():
							for client in Return_Details.objects.filter(P_id=personal_instance,FY = fy):
								R_ID = client.R_id
					except Exception as ex:
						log.error('Error in : '+traceback.format_exc())
					
					log.info('no_of_TAN : '+str(no_of_TAN))
					if A_no_trans != 1:
						if no_of_TAN-1!= 0:
							for i in xrange(1,no_of_TAN):
								start = 0
								end =0
								for x in xrange(partA_start,partA_end):
									nestr = worksheet.cell(x,0).value
									nestr = re.findall(r'\d+', nestr)
									for n in nestr:
										if (str(n) == str(i)):
											start = x
										if (str(n) == str(i+1)):
											end = x
									if (i == no_of_TAN-1):
										end = partA_end
								if end!=0:
									get_partA(worksheet,start,end,pan,'A',year,R_ID)

					# log.info('no_of_TAN_partA1 : '+str(no_of_TAN_partA1))
					if A1_no_trans != 1:
						if no_of_TAN_partA1-1!= 0:
							for i in xrange(1,no_of_TAN_partA1):
								start = 0
								end =0
								for x in xrange(partA1_start,partA1_end):
									nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
									for n in nestr:
										if (str(n) == str(i)):
											start = x
										if (str(n) == str(i+1)):
											end = x
									if (i == no_of_TAN_partA1-1):
										end = partA1_end
								if end!=0:
									get_partA(worksheet,start,end,pan,'A1',year,R_ID)

					log.info('no_of_TAN_partA2 : '+str(no_of_TAN_partA2))
					if A2_no_trans != 1:
						if no_of_TAN_partA2-1!= 0:
							for i in xrange(1,no_of_TAN_partA2):
								start = 0
								end =0
								for x in xrange(partA2_start,partA2_end):
									nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
									for n in nestr:
										if (str(n) == str(i)):
											start = x
										if (str(n) == str(i+1)):
											end = x
									if (i == no_of_TAN_partA2-1):
										end = partA2_end
								get_partA(worksheet,start,end,pan,'A2',year,R_ID)
					
					# log.info('no_of_TAN_partB : '+str(B_no_trans) )
					if B_no_trans != 1:
						if no_of_TAN_partB-1!= 0:
							for i in xrange(1,no_of_TAN_partB):
								start = 0
								end =0
								for x in xrange(partB_start,partB_end):
									nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
									for n in nestr:
										if (str(n) == str(i)):
											start = x
										if (str(n) == str(i+1)):
											end = x
									if (i == no_of_TAN_partB-1):
										end = partB_end
								get_partA(worksheet,start,end,pan,'B',year,R_ID)
					
					log.info('partC_start ->'+str(partC_start)+' '+str(partC_end) )
					if partC_start+2 < partC_end-2:
						for curr_row_partC in xrange(partC_start+2,partC_end-2):
							if worksheet.cell(curr_row_partC,1).value != '':
								major_head = worksheet.cell_value(curr_row_partC,1)
								minor_head = worksheet.cell_value(curr_row_partC,2)
								total_tax = worksheet.cell_value(curr_row_partC,7)
								BSR_code = worksheet.cell_value(curr_row_partC,8)

								try:
									total_tax = int(float(total_tax) )
								except ValueError:
									total_tax = 0
									log.info(total_tax+" Not a int")

								cell = worksheet.cell(curr_row_partC, 9)
								if cell.ctype == xlrd.XL_CELL_DATE:
									date = datetime.datetime(1899, 12, 30)
									get_ = datetime.timedelta(int(cell.value))
									get_col2 = str(date + get_)[:10]
									d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
									get_col = d.strftime('%Y-%m-%d %H:%M')
								else:
									get_col = parse(cell.value)

								deposit_date = get_col
								challan_serial_no = worksheet.cell_value(curr_row_partC,10)
								if not tax_paid.objects.filter(R_Id=R_ID,deposit_date=deposit_date).exists():
									tax_paid.objects.create(
										R_Id=R_ID,
										major_head=major_head,
										minor_head=minor_head,
										total_tax=total_tax,
										BSR_code=BSR_code,
										deposit_date=deposit_date,
										challan_serial_no=challan_serial_no
									).save()
									log.info('tax_paid created for R_ID :'+str(R_ID))

					log.info('partD_start ->'+str(partD_start)+' '+str(partD_end) )
					if partD_start+2 < partD_end-2:
						date_container = 0
						amt_container = 0
						int_container = 0
						for curr_row_partD in xrange(partD_start+2,partD_end-2):
							# partD_list = []
							if worksheet.cell(curr_row_partD,1).value != '':
								if(curr_row_partD == partD_start+2):
									for x in xrange(1,num_rows):
										cell_value = worksheet.cell(curr_row_partD-1, x).value.encode('ascii','ignore')
										if(cell_value == ''):
											break
										if(cell_value.find('Date') != -1 ):
											date_container = x
										if(cell_value.find('Amount') != -1 ):
											amt_container = x
										if(cell_value.find('Interest') != -1 ):
											int_container = x
								Assessment_Year = worksheet.cell(curr_row_partD,1).value
								mode = worksheet.cell(curr_row_partD,2).value
								# 3, 4 = amt, int
								amount_of_refund = worksheet.cell(curr_row_partD,amt_container).value
								interest = worksheet.cell(curr_row_partD,int_container).value
								# log.info(Assessment_Year)
							
								cell = worksheet.cell(curr_row_partD, date_container)
								if cell.ctype == xlrd.XL_CELL_DATE:
									date = datetime.datetime(1899, 12, 30)
									get_ = datetime.timedelta(int(cell.value))
									get_col2 = str(date + get_)[:10]
									d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
									get_col = d.strftime('%Y-%m-%d %H:%M')
								else:
									get_col = parse(cell.value)

								payment_date = get_col
								if not refunds.objects.filter(R_Id=pan,payment_date=payment_date).exists():
									refunds.objects.create(
										R_Id=pan,
										Assessment_Year=Assessment_Year,
										mode=mode,
										amount_of_refund=amount_of_refund,
										interest=interest,
										payment_date=payment_date,
									).save()
					process_26asfile = process_26asfile + 1
		except Exception as ex:
			# user_error_tracking(client_id,'excel_to_db : %s' % ex,request)
			log.error('Error in excel_to_db: '+traceback.format_exc())
			return 'Error excel_to_db : %s' % ex 
	else:
		return 'PAN is Empty'

def generating_hash(xml_filename):
	try:
		log.info('generating_hash')
		MYDIR = os.path.dirname(__file__)
		jar_path = MYDIR+'/form16/generate_digest.jar'
		hash_code = ''

		p = Popen(['java', '-jar',jar_path,xml_filename], stdout=PIPE, stderr=STDOUT)
		for line in p.stdout:
			hash_code = hash_code + line
		# return subprocess.call(['java', '-jar', '/media/salat/DATA D/Ubuntu_related_file/captcha_arg.jar','/home/salat/Downloads/images_captcha/screenshot.png'])
		return hash_code
	except Exception as ex:
		log.info('Error in generating_hash '+traceback.format_exc())


@csrf_exempt
def get_details_for_pricing(request):
	result={}
	try:
		client_id=request.POST.get('client_id')
		log.info(client_id)
		R_Id=get_rid(client_id)

		referred_by_code=SalatTaxUser.objects.get(id=client_id).referred_by_code
		tax_variable_data=tax_variables.objects.filter(R_Id=R_Id)
		tax_variables_serializer=tax_variablesSerializer(tax_variable_data,many='true')
		result['data']=tax_variables_serializer.data

		personal_instance=Personal_Details.objects.get(P_Id=client_id)
		result['no_of_companies']=Employer_Details.objects.filter(P_id=personal_instance).count()

		result['no_of_house_properties']=House_Property_Details.objects.filter(R_Id=R_Id).count()
		
		result['shares']=Shares_LT.objects.filter(R_Id=R_Id).exists() or Shares_ST.objects.filter(R_Id=R_Id).exists()
	
		result['MF']=Equity_LT.objects.filter(R_Id=R_Id).exists() or Equity_ST.objects.filter(R_Id=R_Id).exists() or Debt_MF_LT.objects.filter(R_Id=R_Id).exists() or Debt_MF_ST.objects.filter(R_Id=R_Id).exists()
		result['referred_by_code']=referred_by_code
	except Exception as ex:
		result['data']=traceback.format_exc()
		log.info('Error in getting details for pricing '+traceback.format_exc())

	return HttpResponse(json.dumps(result),content_type='application/json')


@csrf_exempt
def calculate_discount(request):
	result={}
	salattax_user_referral_code=''
	res=0
	try:
		discount_code=request.POST.get('discount_code')
		total_amount=request.POST.get('total_amount')

		username = request.session.get('login')
		if username is None:
			pass
		else:
			if username!='':
				atuser = User.objects.get(username=username)
				salattax_user_referral_code=SalatTaxUser.objects.get(user=atuser).referral_code

		log.info(salattax_user_referral_code)

		# check code is not yourself
		if discount_code==salattax_user_referral_code:
			res=1
			result['data']='Oops! you can\'t use this referral code'
		else:
			referral_code=SalatTaxUser.objects.filter(referral_code=discount_code)

			offer_code=Offer.objects.filter(offer_code=discount_code)

			username = request.session.get('login')
			
			if referral_code.exists():
				first_character_of_discount_code=discount_code[0]
				offer_code1=Offer.objects.filter(offer_code=first_character_of_discount_code)

				if not offer_code.exists() and offer_code1.exists():
					db_offer_code=first_character_of_discount_code
				else:
					res=1
					result['data']='Code is invalid'

			elif not referral_code.exists() and offer_code.exists():
				db_offer_code=discount_code

			else:
				db_offer_code=''
				res=1
				result['data']='Code is invalid'

			if db_offer_code:
				check_expiry_date=Offer.objects.filter(offer_code=db_offer_code,expiry_date__gte=current_date())
					
				if check_expiry_date.exists():
					
					offer_amount_formula=check_expiry_date[0].offer_amount

					formula=offer_amount_formula.replace('amount',total_amount)
					
					# formula='=round('+offer_amount_formula.replace('amount','A2')+')'
					res=0
					# result['data']=writing_excel_file(formula,total_amount)
					result['data']=eval(formula)
				else:
					res=1
					result['data']='Code is expired'

	except Exception as ex:
		res=1
		result['data']=traceback.format_exc()
		log.info('Error in calculating discount for pricing '+traceback.format_exc())

	result['status']=res

	return HttpResponse(json.dumps(result),content_type='application/json')

def current_date():
	return datetime.today().strftime('%Y-%m-%d')


def writing_excel_file(formula,total_amount):
	try:
		# Call a Workbook() function of openpyxl  
		# to create a new blank Workbook object 
		wb = openpyxl.Workbook() 
		# Get workbook active sheet   
		# from the active attribute. 
		sheet = wb.active 
		# writing to the cell of an excel sheet 
		sheet['A1'] = 'values'
		sheet['A2'] = total_amount
		sheet['A3'] = formula
		# sheet['A2'] = '= SUM(A1:A5)'
		# save the file
		excel_file=selenium_path+"/offer_calculation.xlsx"

		wb.save(excel_file) 

		# # workbook object is created 
		wb_obj = openpyxl.load_workbook(excel_file,data_only=True) 

		sheet_obj = wb_obj.active 

		value =sheet_obj['A3'].value

		log.info(value)
		
		return value

	except Exception as ex:
		log.info('Error in writing Excel for offer calculation '+traceback.format_exc())
		return 0

@csrf_exempt
def RF_payment_details_api(request):
	log.info('==================================================================')
	log.info('RF Payment Details API start')
	result={}
	res=0
	try:
		client_id=request.POST.get('client_id')
		R_Id=get_rid(client_id)
		b_id=request.POST.get('b_id')
		role=request.POST.get('role')
		data=request.POST.get('data')
		req_data=json.loads(data)
		# log.info(req_data)

		business_partner_instance=Business_Partner.objects.get(id=b_id)
		if RFPaymentDetails.objects.filter(R_Id=R_Id).exists():

			RFpaymentdetails_instance=RFPaymentDetails.objects.get(R_Id=R_Id)
			RFpaymentdetails_instance.business_partner_id=business_partner_instance
			RFpaymentdetails_instance.discount_code=req_data['referral_code']
			RFpaymentdetails_instance.basic_charge=req_data['basic_return_filing_charge']
			RFpaymentdetails_instance.form16_charge=req_data['no_of_companies_charge']
			RFpaymentdetails_instance.HP_charge=req_data['no_of_house_properties_charge']
			RFpaymentdetails_instance.CG_shares_charge=req_data['CG_shares_charge']
			RFpaymentdetails_instance.CG_MF_charge=req_data['CG_MF_charge']
			RFpaymentdetails_instance.phone_support_charge=req_data['phone_support_charge']
			RFpaymentdetails_instance.express_filing_charge=req_data['priority_RF_charge']
			RFpaymentdetails_instance.save()

			log.info('RF Payment Entry Updated')

		elif not RFPaymentDetails.objects.filter(R_Id=R_Id).exists():

			RFPaymentDetails.objects.create(

				R_Id=R_Id,
				business_partner_id=business_partner_instance,
				discount_code=req_data['referral_code'],
				basic_charge=req_data['basic_return_filing_charge'],
				form16_charge=req_data['no_of_companies_charge'],
				HP_charge=req_data['no_of_house_properties_charge'],
				CG_shares_charge=req_data['CG_shares_charge'],
				CG_MF_charge=req_data['CG_MF_charge'],
				phone_support_charge=req_data['phone_support_charge'],
				express_filing_charge=req_data['priority_RF_charge'],

			).save()
			log.info('RF Payment Entry Created')

		# 
		RF_payment_details_id=RFPaymentDetails.objects.get(R_Id=R_Id).id
		return_filing_module_instance=Modules.objects.get(module_name='Return Filing')
		tax_consultaion_instance=Modules.objects.get(module_name='Tax Consultation')
		module_data_id=return_filing_module_instance.id
		module_subscription_data=Module_Subscription.objects.filter(module_Id=module_data_id,client_id=client_id,module_detail1=RF_payment_details_id,invoice_no='',business_partner_id=business_partner_instance)

		# return filing module subscription
		if module_subscription_data.exists():
			module_subscription_instance=Module_Subscription.objects.get(module_Id=int(module_data_id),client_id=int(client_id),module_detail1=RF_payment_details_id,invoice_no='',business_partner_id=business_partner_instance)
			module_subscription_instance.year=int(current_year())
			module_subscription_instance.module_cost=req_data['total_return_filing_charges']
			module_subscription_instance.save()

			log.info('Module Subscription Return Filing Entry Updated')

		elif not module_subscription_data.exists():
			Module_Subscription.objects.create(

				module_Id=module_data_id,
				invoice_no='',
				client_id=client_id,
				business_partner_id=business_partner_instance,
				module_detail1=RF_payment_details_id,
				year=int(current_year()),
				module_cost=req_data['total_return_filing_charges']

			).save()

			log.info('Module Subscription Return Filing Entry Created')


		# expert consultation module subscription
		# log.info(req_data['expert_consultation_charge'])
		# log.info(role)
		if req_data['expert_consultation_charge']!=0:
			consultaion_module_data_id=Modules.objects.get(module_name='Tax Consultation').id
			log.info(consultaion_module_data_id)
			consultaion_module_subscription_data=Module_Subscription.objects.filter(module_Id=consultaion_module_data_id,client_id=client_id,module_detail1=b_id,invoice_no='',business_partner_id=business_partner_instance)

			if consultaion_module_subscription_data.exists():
				module_subscription_instance=Module_Subscription.objects.get(module_Id=consultaion_module_data_id,client_id=client_id,module_detail1=b_id,invoice_no='',business_partner_id=business_partner_instance)
				module_subscription_instance.year=int(current_year())
				module_subscription_instance.module_cost=req_data['expert_consultation_charge']
				module_subscription_instance.save()

				log.info('Module Subscription Tax Consultation Entry Updated')

			elif not consultaion_module_subscription_data.exists():
				Module_Subscription.objects.create(

					module_Id=consultaion_module_data_id,
					client_id=client_id,
					invoice_no='',
					business_partner_id=business_partner_instance,
					module_detail1=b_id,
					year=int(current_year()),
					module_cost=req_data['expert_consultation_charge']

				).save()

				log.info('Module Subscription Return Filing Tax Consultation Entry Created')

		partner_share=0
		salat_share=0
		partner_share_flag=0
		partner_share_status=''

		if (role=='Partner' or role=='Admin') and req_data['mode_of_payment']=='Cash':

			return_filing_partner_share_condition={ 'business_partner_id':business_partner_instance,
													 'module_id':return_filing_module_instance}
			
			if Revenue_Sharing.objects.filter(**return_filing_partner_share_condition).exists():
				return_filing_partner_share=Revenue_Sharing.objects.get(**return_filing_partner_share_condition).partner_share
			else:
				return_filing_partner_share=0
				log.info('Return Filing Revenue Sharing Entry not exists')

			return_filing_partner_share_amount=(int(req_data['total_return_filing_charges'])*int(return_filing_partner_share))/100
			return_filing_salat_share_amount=int(req_data['total_return_filing_charges'])-return_filing_partner_share_amount
			log.info('Return Filing Total Amount '+str(req_data['total_return_filing_charges']))
			log.info('return_filing_partner_share '+str(return_filing_partner_share))
			log.info('return_filing_partner_share_amount '+str(return_filing_partner_share_amount))
			log.info('return_filing_salat_share_amount '+str(return_filing_salat_share_amount))

			# Insert Update Partner Settlment for return filing module 
			if module_subscription_data.exists():
				module_subscription_instance=Module_Subscription.objects.get(module_Id=int(module_data_id),client_id=int(client_id),module_detail1=RF_payment_details_id,invoice_no='',business_partner_id=business_partner_instance)
				if Partner_Settlement.objects.filter(module_subscription_id=module_subscription_instance).exists():
					partner_settlement_instance=Partner_Settlement.objects.get(module_subscription_id=module_subscription_instance)
					partner_settlement_instance.partner_share=return_filing_partner_share_amount
					partner_settlement_instance.settlement_status='N'
					partner_settlement_instance.save()

					log.info('Return Filing Partner Settlement Entry Updated')
				else:
					Partner_Settlement.objects.create(
						module_subscription_id=module_subscription_instance,
						partner_share=return_filing_partner_share_amount,
						settlement_status='N'
					).save()

					log.info('Return Filing Partner Settlement Entry Created')

			if req_data['expert_consultation_charge']!=0:
				tax_consultaion_partner_share_condition={ 'business_partner_id':business_partner_instance,
													 'module_id':tax_consultaion_instance}

				if Revenue_Sharing.objects.filter(**tax_consultaion_partner_share_condition):
					tax_consultation_partner_share=Revenue_Sharing.objects.get(**tax_consultaion_partner_share_condition).partner_share
				else:
					tax_consultation_partner_share=0
					log.info('Tax Consultation Revenue Sharing Entry not exists')

				tax_consultation_partner_share_amount=(int(req_data['expert_consultation_charge'])*int(tax_consultation_partner_share))/100
				tax_consultation_salat_share_amount=int(req_data['expert_consultation_charge'])-tax_consultation_partner_share_amount
				log.info('Tax Consultation Total Amount '+str(req_data['expert_consultation_charge']))
				log.info('tax_consultation_partner_share '+str(tax_consultation_partner_share))
				log.info('tax_consultation_partner_share_amount '+str(tax_consultation_partner_share_amount))
				log.info('tax_consultation_salat_share_amount '+str(tax_consultation_salat_share_amount))
	
					# Insert Update Partner Settlment for tax consultation module 
				if consultaion_module_subscription_data.exists():
					module_subscription_instance=Module_Subscription.objects.get(module_Id=consultaion_module_data_id,client_id=client_id,module_detail1=b_id,invoice_no='',business_partner_id=business_partner_instance)
					if Partner_Settlement.objects.filter(module_subscription_id=module_subscription_instance).exists():
						partner_settlement_instance=Partner_Settlement.objects.get(module_subscription_id=module_subscription_instance)
						partner_settlement_instance.partner_share=tax_consultation_partner_share_amount
						partner_settlement_instance.settlement_status='N'
						partner_settlement_instance.save()

						log.info('Partner Settlement Entry Updated')
					else:
						Partner_Settlement.objects.create(
							module_subscription_id=module_subscription_instance,
							partner_share=tax_consultation_partner_share_amount,
							settlement_status='N'
						).save()

					log.info('Partner Settlement Entry Created')
			else:
				tax_consultation_salat_share_amount=0
				tax_consultation_partner_share=0


			if Partner_Deposit_Balance.objects.filter(business_partner_id=business_partner_instance).exists():
				partner_deposite_balance_instance=Partner_Deposit_Balance.objects.get(business_partner_id=business_partner_instance)
				partner_deposite_balance=partner_deposite_balance_instance.deposit_balance
				salat_share=return_filing_salat_share_amount+tax_consultation_salat_share_amount	

				log.info('Addition Of Salat Share '+str(salat_share))
				log.info('Available Partner Balance '+str(partner_deposite_balance))
				
				if int(salat_share)<=int(partner_deposite_balance):

					partner_share_status='Good. Salat Share debited from your deposite'

					updated_deposite_balance=int(partner_deposite_balance)-int(salat_share)

					log.info('Updated Deposite Balance '+str(updated_deposite_balance))
					partner_deposite_balance_instance.deposit_balance=updated_deposite_balance
					partner_deposite_balance_instance.save()

					log.info('Partner Deposite Balance Updated')

					if module_subscription_data.exists():
						module_subscription_instance=Module_Subscription.objects.get(module_Id=int(module_data_id),client_id=int(client_id),module_detail1=RF_payment_details_id,invoice_no='',business_partner_id=business_partner_instance)
						if Partner_Settlement.objects.filter(module_subscription_id=module_subscription_instance).exists():
							partner_settlement_instance=Partner_Settlement.objects.get(module_subscription_id=module_subscription_instance)
							partner_settlement_instance.settlement_status='Y'
							partner_settlement_instance.save()

							log.info('partner_settlement Updated for return filing module')

					if req_data['expert_consultation_charge']!=0:
						if consultaion_module_subscription_data.exists():
							module_subscription_instance=Module_Subscription.objects.get(module_Id=consultaion_module_data_id,client_id=client_id,module_detail1=b_id,invoice_no='',business_partner_id=business_partner_instance)
							if Partner_Settlement.objects.filter(module_subscription_id=module_subscription_instance).exists():
								partner_settlement_instance=Partner_Settlement.objects.get(module_subscription_id=module_subscription_instance)
								partner_settlement_instance.settlement_status='Y'
								partner_settlement_instance.save()

								log.info('partner_settlement Updated for tax consultation module')


					invoice_count=Invoice_Details.objects.filter(~Q(invoice_no='')).count()
					invoice_no=generate_invoice_no(str(invoice_count+1))
					invoice_details_data=Invoice_Details.objects.filter(client_id=client_id,invoice_no='')
					if invoice_details_data.exists():

						invoice_details_instance=Invoice_Details.objects.get(client_id=client_id,invoice_no='')
						invoice_details_instance.invoice_no=invoice_no
						invoice_details_instance.invoice_amount=req_data['total_amount_payable']
						invoice_details_instance.discount=req_data['discount']
						invoice_details_instance.offer_code=req_data['referral_code']
						invoice_details_instance.mode_of_payment=req_data['mode_of_payment']
						invoice_details_instance.payment_status='success'
						invoice_details_instance.save()

					elif not invoice_details_data.exists():

						Invoice_Details.objects.create(

							client_id=client_id,
							invoice_no=invoice_no,
							invoice_amount=req_data['total_amount_payable'],
							discount=req_data['discount'],
							offer_code=req_data['referral_code'],
							mode_of_payment=req_data['mode_of_payment'],
							payment_status='success'

						).save()

					if module_subscription_data.exists():
						module_subscription_instance=Module_Subscription.objects.get(module_Id=int(module_data_id),client_id=int(client_id),module_detail1=RF_payment_details_id,invoice_no='',business_partner_id=business_partner_instance)
						module_subscription_instance.invoice_no=invoice_no
						module_subscription_instance.save()

					if req_data['expert_consultation_charge']!=0:
						consultaion_module_data_id=Modules.objects.get(module_name='Tax Consultation').id
						consultaion_module_subscription_data=Module_Subscription.objects.filter(module_Id=consultaion_module_data_id,client_id=client_id,module_detail1=b_id,invoice_no='',business_partner_id=business_partner_instance)

						if consultaion_module_subscription_data.exists():
							module_subscription_instance=Module_Subscription.objects.get(module_Id=consultaion_module_data_id,client_id=client_id,module_detail1=b_id,invoice_no='',business_partner_id=business_partner_instance)
							module_subscription_instance.invoice_no=invoice_no
							module_subscription_instance.save()

							log.info('Module Subscription Tax Consultation Entry Updated')

				elif int(salat_share)>int(partner_deposite_balance):
					partner_share_flag=1
					partner_share_status='Your deposite Balance low.Kindly top-up balance'
				
				else:
					partner_share_flag=1
					partner_share_status='Invalid condition'
					log.info('Invalid condition')

			else:
				partner_share_flag=1
				partner_share_status='Please make payment to proceed'
		else:
			partner_share_flag=1
			log.info('Skip partner share logic')


			invoice_details_data=Invoice_Details.objects.filter(client_id=client_id,invoice_no='')
			if invoice_details_data.exists():

				invoice_details_instance=Invoice_Details.objects.get(client_id=client_id,invoice_no='')
				invoice_details_instance.invoice_amount=req_data['total_amount_payable']
				invoice_details_instance.discount=req_data['discount']
				invoice_details_instance.offer_code=req_data['referral_code']
				invoice_details_instance.mode_of_payment=req_data['mode_of_payment']
				invoice_details_instance.payment_status='Pending'
				invoice_details_instance.save()

			elif not invoice_details_data.exists():

				Invoice_Details.objects.create(

					client_id=client_id,
					invoice_no='',
					invoice_amount=req_data['total_amount_payable'],
					discount=req_data['discount'],
					offer_code=req_data['referral_code'],
					mode_of_payment=req_data['mode_of_payment'],
					payment_status='Pending'

				).save()

		# update reffered by code in salattax user table
		salattax_user_instance=SalatTaxUser.objects.get(id=client_id)
		salattax_user_instance.referred_by_code=req_data['referral_code']
		salattax_user_instance.save()

		log.info('Salattax user referred by code updated')
		
		log.info('RF Payment Details API end')
		log.info('==================================================================')
		# log.info(generate_invoice_no('1'))
		# log.info(generate_referral_code('P'))
		result['data']='success'
		result['partner_share_flag']=partner_share_flag
		result['partner_share_status']=partner_share_status
	except Exception as ex:
		result['data']='Error in RF payment details api '+traceback.format_exc()
		log.info('Error in RF payment details api '+traceback.format_exc())
		res=1

	result['status']=res
	return HttpResponse(json.dumps(result),content_type="application/json")


def generate_invoice_no(number):
	try:
		now=datetime.now()
		current_year=now.year
		next_year=now.year+1
		line=str(next_year)
		n=2
		list_chunks_by_2=[line[i:i+n] for i in range(0, len(line), n)]
		year_combination=str(current_year)+list_chunks_by_2[1]

		invoice_no=number.zfill(4)
		return year_combination+'-'+invoice_no
	except Exception as ex:
		log.info('generate invoice no '+traceback.format_exc())

def current_year():
	now=datetime.now()
	return now.year

@csrf_exempt
def pay_to_IT_api(request):
	result={}
	res=0
	try:
		client_id=request.POST.get('client_id')
		R_Id=get_rid(client_id)
		data=request.POST.get('data')
		req_data=json.loads(data)

		if req_data['date_of_deposit']!='':
			date=req_data['date_of_deposit'].split('/')
			date=date[2]+'-'+date[1]+'-'+date[0]
		else:
			date=None

		tax_paid_data=tax_paid.objects.filter(R_Id=R_Id,challan_serial_no=req_data['challan_no'])

		if tax_paid_data.exists():
			tax_paid_instance=tax_paid.objects.get(R_Id=R_Id,challan_serial_no=req_data['challan_no'])
			tax_paid_instance.total_tax=req_data['amount_paid']
			tax_paid_instance.BSR_code=req_data['BSR_code']
			tax_paid_instance.deposit_date=date
			tax_paid_instance.save()

			log.info('Tax paid entry updated')
		elif not tax_paid_data.exists():

			tax_paid.objects.create(

				R_Id=R_Id,
				challan_serial_no=req_data['challan_no'],
				total_tax=req_data['amount_paid'],
				BSR_code=req_data['BSR_code'],
				deposit_date=date,
				major_head=req_data['major_head'],
				minor_head=req_data['minor_head']


			).save()

			log.info('Tax paid entry created')

		result['data']='success'
	except Exception as ex:
		result['data']='Error in pay to IT API '+traceback.format_exc()
		log.info('Error in pay to IT API '+traceback.format_exc())
		res=1

	result['status']=res
	return HttpResponse(json.dumps(result),content_type='application/json')


@csrf_exempt
def get_invoice_details(request):
	result={}
	payment_pending_flag=0
	status=''
	flag=0
	paid_module_code=0
	net_amount_payable=0
	net_amount_payable_total=0
	total_amount_paid=0
	try:
		client_id=request.POST.get('client_id')
		total_amount_payable=request.POST.get('total_amount_payable')
		expert_consultation=request.POST.get('expert_consultation')
		R_ID=get_rid(client_id)
		log.info(R_ID)

		log.info('==================checking pricing flaw starts============')
		if total_amount_payable=='':
			total_amount_payable=0
		if expert_consultation=='':
			expert_consultation=0

		log.info('total_amount_payable '+str(total_amount_payable))

		log.info('expert_consultation '+str(expert_consultation))

		total_RF_cost=int(total_amount_payable)-int(expert_consultation)

		log.info('total_RF_cost '+str(total_RF_cost))

		RFPaymentDetails_data=RFPaymentDetails.objects.filter(R_Id=R_ID)
		if RFPaymentDetails_data.exists():
			RFPaymentDetails_serializer=RFPaymentDetailsSerializer(RFPaymentDetails_data,many='true')
			rf_data=RFPaymentDetails_serializer.data
			log.info(rf_data)
			for data1 in rf_data:
				if data1['RF_Module_subscription']:
					RF_paid_module_code=data1['RF_Module_subscription']['module_cost']
					total_amount_paid+=int(RF_paid_module_code)
					log.info('RF_paid_module_code '+str(RF_paid_module_code))
					if int(total_RF_cost)>int(RF_paid_module_code):
						net_amount_payable+=int(total_RF_cost)-int(RF_paid_module_code)
						paid_module_code=1
				else:
					net_amount_payable+=int(total_RF_cost)
					log.info('RF Entry not exists')
				
				log.info('net_amount_payable '+str(net_amount_payable))

				if data1['EC_Module_subscription']:
					EC_paid_module_code=data1['EC_Module_subscription']['module_cost']
					total_amount_paid+=int(EC_paid_module_code)
					log.info('EC_paid_module_code '+str(EC_paid_module_code))
					if int(expert_consultation)>int(EC_paid_module_code):
						net_amount_payable+=int(expert_consultation)-int(EC_paid_module_code)
						paid_module_code=1
				else:
					net_amount_payable+=int(expert_consultation)
					log.info('EC Entry not exists')

				log.info('net_amount_payable '+str(net_amount_payable))

		else:
			status='Invoice Entry not exists'
			log.info(status)
			flag=1

	except Exception as ex:
		log.info('Error in getting invoice details '+traceback.format_exc())
		status='Error in getting invoice details '+traceback.format_exc()
		flag=1

	log.info('==================checking pricing flaw ends============')

	result['net_amount_payable']=net_amount_payable
	result['total_amount_payable']=int(total_amount_payable)
	result['total_amount_paid']=total_amount_paid
	result['status']=paid_module_code
	return HttpResponse(json.dumps(result),content_type='application/json')