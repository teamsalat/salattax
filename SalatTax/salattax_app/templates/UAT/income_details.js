
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('income_details_controller', ['$scope','$compile','$timeout','$parse',function($scope,$compile,$timeout,$parse,CONFIG) {
	redirect_url = $scope.url;
	var client_id = 1;
	var pan = '';
	$scope.status1= 'F';
	$scope.add_16_1 = { 'display': ''};
	$scope.delete_16_1 = { 'display': 'none'};
	$scope.add_16_2 = { 'display': ''};
	$scope.delete_16_2 = { 'display': 'none'};
	$scope.add_16_3 = { 'display': ''};
	$scope.delete_16_3 = { 'display': 'none'};
	// $scope.return_detail_fy = '2018-2019';
	var selected_fy = '';

	$scope.form16_1_fileCount=0;
	$scope.form16_2_fileCount=0;
	$scope.form16_3_fileCount=0;
	$scope.form16_1_fileObj1='';
	$scope.form16_1_fileObj2='';
	$scope.form16_2_fileObj1='';
	$scope.form16_2_fileObj2='';
	$scope.form16_3_fileObj1='';
	$scope.form16_3_fileObj2='';

	$('.loader').hide();

	$scope.company1_entertainment_allowance_hide='Y'
	$scope.company2_entertainment_allowance_hide='Y'
	$scope.company3_entertainment_allowance_hide='Y'

	$scope.entertainment_allowance_hide='N'

	$scope.gotopersonaldetails=function(){
    	window.location.href = redirect_url+'/personal_information/'+client_id;
  	}

  	$scope.gotosalarytab=function(){
  		$('#HousePropertyTab').removeClass('active');
      	$('#salaryTab').tab('show');
      	$("html, body").animate({ scrollTop: 0 }, "slow");
  	}

  	$scope.gotohousepropertytab=function(){
  		$('#businessTab').removeClass('active');
      	$('#HousePropertyTab').tab('show');
      	$("html, body").animate({ scrollTop: 0 }, "slow");
  	}

  	$scope.gotobusinesstab=function(){
  		$('#otherSourcesTab').removeClass('active');
      	$('#businessTab').tab('show');
      	$("html, body").animate({ scrollTop: 0 }, "slow");
  	}

  	$scope.gotoothersourcestab=function(){
  		$('#exemptIncomeTab').removeClass('active');
      	$('#otherSourcesTab').tab('show');
      	$("html, body").animate({ scrollTop: 0 }, "slow");
  	}

  	$scope.gotoexemptincometab=function(){
  		$('#capitalGainsTab').removeClass('active');
      	$('#exemptIncomeTab').tab('show');
      	$("html, body").animate({ scrollTop: 0 }, "slow");
  	}


	$scope.validation = function(type,value,$event){
		//no_of_companies/Residential Status/ No of banks
		if((type == "no_of_companies")&& value!==undefined){
			if(value!='' && value!=null){
				$scope.no_of_companies_error= "";
				return true;
			}else{
				$scope.no_of_companies_error= "Please Select No. of Companies in Financial Year "+selected_fy;
				return false;
			}
		}else if((type == "no_of_companies")&& value==undefined){
			$scope.no_of_companies_error= "No. of Companies is required";
			return false;
		}

		//Employer Name
		if((type == "company1_emp_name" || type == "company2_emp_name" || type == "company3_emp_name") && value!==undefined){
			// var pan_length=value.length;
			if(type == "company1_emp_name")
				$scope.company1_emp_name_error= "";
			else if(type == "company2_emp_name")
				$scope.company2_emp_name_error= "";
			else if(type == "company3_emp_name")
				$scope.company3_emp_name_error= "";
			return true;
		}else if((type == "company1_emp_name" || type == "company2_emp_name" || type == "company3_emp_name") && value==undefined){
			if(type == "company1_emp_name")
				$scope.company1_emp_name_error= "Employer Name is required";
			else if(type == "company2_emp_name")
				$scope.company2_emp_name_error= "Employer Name is required";
			else if(type == "company3_emp_name")
				$scope.company3_emp_name_error= "Employer Name is required";
			return false;
		}

		//PAN
		if((type == "company1_pan" || type == "company2_pan" || type == "company3_pan") && value!==undefined){
			var pan_length=value.length;
			if( ( (/[A-Z]{5}\d{4}[A-Z]{1}/.test(value)) && pan_length==10) || value == '' ){
				if(type == "company1_pan")
					$scope.company1_pan_error= "";
				else if(type == "company2_pan")
					$scope.company2_pan_error= "";
				else if(type == "company3_pan")
					$scope.company3_pan_error= "";
				return true;
			}else{
				if(type == "company1_pan")
					$scope.company1_pan_error= "Please Enter Valid PAN";
				else if(type == "company2_pan")
					$scope.company2_pan_error= "Please Enter Valid PAN";
				else if(type == "company3_pan")
					$scope.company3_pan_error= "Please Enter Valid PAN";
				return false;
			}
		}else if((type == "company1_pan" || type == "company2_pan" || type == "company3_pan") && value==undefined){
			if(type == "company1_pan")
				$scope.company1_pan_error= "PAN is required";
			else if(type == "company2_pan")
				$scope.company2_pan_error= "PAN is required";
			else if(type == "company2_pan")
				$scope.company2_pan_error= "PAN is required";
			return false;
		}

		//TAN
		if((type == "company1_tan" || type == "company2_tan" || type == "company3_tan") && value!==undefined){
			var pan_length=value.length;
			if( ( (/[A-Z]{4}\d{5}[A-Z]{1}/.test(value)) && pan_length==10) || value == '' ){
				if(type == "company1_tan")
					$scope.company1_tan_error= "";
				else if(type == "company2_tan")
					$scope.company2_tan_error= "";
				else if(type == "company3_tan")
					$scope.company3_tan_error= "";
				return true;
			}else{
				if(type == "company1_tan")
					$scope.company1_tan_error= "Please Enter Valid TAN";
				else if(type == "company2_tan")
					$scope.company2_tan_error= "Please Enter Valid TAN";
				
				else if(type == "company3_tan")
					$scope.company3_tan_error= "Please Enter Valid TAN";
				return false;
			}
		}else if((type == "company1_tan" || type == "company2_tan" || type == "company3_tan") && value==undefined){
			if(type == "company1_tan")
				$scope.company1_tan_error= "TAN is required";
			else if(type == "company2_tan")
				$scope.company2_tan_error= "TAN is required";
			else if(type == "company3_tan")
				$scope.company3_tan_error= "TAN is required";
			return false;
		}

		//address
		if((type == "company1_add" || type=="company2_add" || type=="company3_add") && value!==undefined){
			if(type == "company1_add")
				$scope.company1_add_error= "";
			else if(type == "company2_add")
				$scope.company2_add_error= "";
			else if(type == "company3_add")
				$scope.company3_add_error= "";
			return true;
		}else if((type == "company1_add" || type=="company2_add" || type=="company3_add") && value==undefined){
			if(type == "company1_add")
				$scope.company1_add_error= "Employer Address is required";
			else if(type == "company2_add")
				$scope.company2_add_error= "Employer Address is required";
			else if(type == "company3_add")
				$scope.company3_add_error= "Employer Address is required";
			return false;
		}

		//Town/city
		if((type == "company1_city" || type=="company2_city" || type=="company3_city") && value!==undefined){
			if(type == "company1_city")
				$scope.company1_city_error= "";
			else if(type == "company2_city")
				$scope.company2_city_error= "";
			else if(type == "company3_city")
				$scope.company3_city_error= "";
			return true;
		}else if((type == "company1_city" || type=="company2_city" || type=="company3_city") && value==undefined){
			if(type == "company1_city")
				$scope.company1_add_error= "Employer Address is required";
			else if(type == "company2_city")
				$scope.company2_add_error= "Employer Address is required";
			else if(type == "company3_city")
				$scope.company3_add_error= "Employer Address is required";
			return false;
		}

		//Pin
		if((type == "company1_pin" || type == "company2_pin" || type == "company3_pin") && value!==undefined){
			var pin_length=value.length;
			if( ( (/\d{6}/.test(value)) && pin_length==6) || value=='' ){ 
				if(type == "company1_pin"){
					$scope.company1_pin_error= "";
					if (value!='')
						var pin_city_state = $scope.get_cityState_pin("c1",value);
				}else if(type == "company2_pin"){
					$scope.company2_pin_error= "";
					if (value!='')
						var pin_city_state = $scope.get_cityState_pin("c2",value);
				}else if(type == "company3_pin"){
					$scope.company3_pin_error= "";
					if (value!='')
						var pin_city_state = $scope.get_cityState_pin("c3",value);
				}
				return true;
			}else{
				if(type == "company1_pin")
					$scope.company1_pin_error= "Please Enter Valid PIN";
				else if(type == "company2_pin")
					$scope.company2_pin_error= "Please Enter Valid PIN";
				else if(type == "company3_pin")
					$scope.company3_pin_error= "Please Enter Valid PIN";
				return false;
			}
		}else if((type == "company1_pin" || type == "company2_pin" || type == "company3_pin") && value==undefined){
			if(type == "company1_pin")
				$scope.company1_pin_error= "PIN is required";
			else if(type == "company2_pin")
				$scope.company2_pin_error= "PIN is required";
			else if(type == "company3_pin")
				$scope.company3_pin_error= "PIN is required";
			return false;
		}

		// State/country
		if((type == "company1_state" || type=="company2_state" || type=="company3_state") && value!==undefined){
			if(type == "company1_state")
				$scope.company1_state_error= "";
			else if(type == "company2_state")
				$scope.company2_state_error= "";
			else if(type == "company3_state")
				$scope.company3_state_error= "";
			return true;
		}else if((type == "company1_state" || type=="company2_state" || type=="company3_state") && value==undefined){
			if(type == "company1_state")
				$scope.company1_state_error= "State is required";
			else if(type == "company2_state")
				$scope.company2_state_error= "State is required";
			else if(type == "company3_state")
				$scope.company3_state_error= "State is required";
			return false;
		}

		// salary as per provision
		if((type == "company1_salary_provision" || type=="company2_salary_provision" || type=="company3_salary_provision") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value)) || value=='' ){
				if(type == "company1_salary_provision"){
					$scope.company1_salary_provision_error= "";
					$scope.calculated_salary('1');
				}
				else if(type == "company2_salary_provision"){
					$scope.calculated_salary('2');
					$scope.company2_salary_provision_error= "";
				}
				else if(type == "company3_salary_provision"){
					$scope.calculated_salary('3');
					$scope.company3_salary_provision_error= "";
				}
				return true;
			}else{
				if(type == "company1_salary_provision")
					$scope.company1_salary_provision_error= "Please Enter Valid Salary";
				else if(type == "company2_salary_provision")
					$scope.company2_salary_provision_error= "Please Enter Valid Salary";
				else if(type == "company3_salary_provision")
					$scope.company3_salary_provision_error= "Please Enter Valid Salary";
				return false;
			}
		}

		//Value of perquisites and  Profits in lieu of Salary
		if((type == "company1_perquisites" || type=="company2_perquisites" || type=="company3_perquisites" || type == "company1_profits_lieu_salary" || 
			type=="company2_profits_lieu_salary" || type=="company3_profits_lieu_salary") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value)) || value=='' ){
				if(type == "company1_perquisites"){
					$scope.calculated_salary('1');
					$scope.company1_perquisites_error= "";
				}
				else if(type == "company2_perquisites"){
					$scope.calculated_salary('2');
					$scope.company2_perquisites_error= "";
				}
				else if(type == "company3_perquisites"){
					$scope.calculated_salary('3');
					$scope.company3_perquisites_error= "";
				}
				else if(type == "company1_profits_lieu_salary"){
					$scope.calculated_salary('1');
					$scope.company1_profits_lieu_salary_error= "";
				}
				else if(type == "company2_profits_lieu_salary"){
					$scope.calculated_salary('2');
					$scope.company2_profits_lieu_salary_error= "";
				}
				else if(type == "company3_profits_lieu_salary"){
					$scope.calculated_salary('3');
					$scope.company3_profits_lieu_salary_error= "";
				}
				return true;
			}else{
				if(type == "company1_perquisites")
					$scope.company1_perquisites_error= "Please Enter Valid Value of perquisites";
				else if(type == "company2_perquisites")
					$scope.company2_perquisites_error= "Please Enter Valid Value of perquisites";
				else if(type == "company3_perquisites")
					$scope.company3_perquisites_error= "Please Enter Valid Value of perquisites";
				else if(type == "company1_profits_lieu_salary")
					$scope.company1_profits_lieu_salary_error= "Please Enter Valid value";
				else if(type == "company2_profits_lieu_salary")
					$scope.company2_profits_lieu_salary_error= "Please Enter Valid value";
				else if(type == "company3_profits_lieu_salary")
					$scope.company3_profits_lieu_salary_error= "Please Enter Valid value";
				return false;
			}
		}
		// Profession Tax and Entertainment Allowance
		if((type == "company1_profession_tax" || type=="company2_profession_tax" || type=="company3_profession_tax" || 
			type == "company1_entertainment_allowance" || type=="company2_entertainment_allowance" || type=="company3_entertainment_allowance") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value)) || value=='' ){
				if(type == "company1_profession_tax")
					$scope.company1_profession_tax_error= "";
				else if(type == "company2_profession_tax")
					$scope.company2_profession_tax_error= "";
				else if(type == "company3_profession_tax")
					$scope.company3_profession_tax_error= "";
				else if(type == "company1_entertainment_allowance")
					$scope.company1_entertainment_allowance_error= "";
				else if(type == "company2_entertainment_allowance")
					$scope.company2_entertainment_allowance_error= "";
				else if(type == "company3_entertainment_allowance")
					$scope.company3_entertainment_allowance_error= "";
				return true;
			}else{
				if(type == "company1_profession_tax")
					$scope.company1_profession_tax_error= "Please Enter Valid Profession Tax";
				else if(type == "company2_profession_tax")
					$scope.company2_profession_tax_error= "Please Enter Valid Profession Tax";
				else if(type == "company3_profession_tax")
					$scope.company3_profession_tax_error= "Please Enter Valid Profession Tax";
				else if(type == "company1_entertainment_allowance")
					$scope.company1_entertainment_allowance_error= "Please Enter Valid value";
				else if(type == "company2_entertainment_allowance")
					$scope.company2_entertainment_allowance_error= "Please Enter Valid value";
				else if(type == "company3_entertainment_allowance")
					$scope.company3_entertainment_allowance_error= "Please Enter Valid value";
				return false;
			}
		}
		// LTA and HRA
		if((type == "company1_LTA" || type=="company2_LTA" || type=="company3_LTA" || type == "company1_HRA" || type=="company2_HRA" || type=="company3_HRA") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value)) || value=='' ){
				if(type == "company1_LTA"){
					$scope.calculated_salary('1');
					$scope.company1_LTA_error= "";
				}
				else if(type == "company2_LTA"){
					$scope.calculated_salary('2');
					$scope.company2_LTA_error= "";
				}
				else if(type == "company3_LTA"){
					$scope.calculated_salary('3');
					$scope.company3_LTA_error= "";
				}
				else if(type == "company1_HRA"){
					$scope.calculated_salary('1');
					$scope.company1_HRA_error= "";
				}
				else if(type == "company2_HRA"){
					$scope.calculated_salary('2');
					$scope.company2_HRA_error= "";
				}
				else if(type == "company3_HRA"){
					$scope.calculated_salary('3');
					$scope.company3_HRA_error= "";
				}
				return true;
			}else{
				if(type == "company1_LTA")
					$scope.company1_LTA_error= "Please Enter Valid LTA";
				else if(type == "company2_LTA")
					$scope.company2_LTA_error= "Please Enter Valid LTA";
				else if(type == "company3_LTA")
					$scope.company3_LTA_error= "Please Enter Valid LTA";
				else if(type == "company1_HRA")
					$scope.company1_HRA_error= "Please Enter Valid HRA";
				else if(type == "company2_HRA")
					$scope.company2_HRA_error= "Please Enter Valid HRA";
				else if(type == "company3_HRA")
					$scope.company3_HRA_error= "Please Enter Valid HRA";
				return false;
			}
		}
		// Other Allowances 
		if((type == "company1_total_allowances" || type=="company2_total_allowances" || type=="company3_total_allowances" ) && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value)) || value=='' ){
				if(type == "company1_total_allowances"){
					$scope.calculated_salary('1');
					$scope.company1_other_allowance_error= "";
				}
				else if(type == "company2_total_allowances"){
					$scope.calculated_salary('2');
					$scope.company2_other_allowance_error= "";
				}
				else if(type == "company3_total_allowances"){
					$scope.calculated_salary('3');
					$scope.company3_other_allowance_error= "";
				}
				return true;
			}else{
				if(type == "company1_total_allowances")
					$scope.company1_other_allowance_error= "Please Enter Valid Other Allowances";
				else if(type == "company2_total_allowances")
					$scope.company2_other_allowance_error= "Please Enter Valid Other Allowances";
				else if(type == "company3_total_allowances")
					$scope.company3_other_allowance_error= "Please Enter Valid Other Allowances";
				return false;
			}
		}

		////////////////////////////// House Property /////////////////////////////
		//House Property / House Property number
		if((type == "house_property" || type== "house_property_type1" || type== "house_property_type2" || type== "house_property_type3") && value!==undefined){
			if(value==''){
				if(type == "house_property")
					$scope.house_property_error= "Do you have House Property?";
				else if(type == "house_property_type1")
					$scope.house_property_type1_error= "Select Property Type";
				else if(type == "house_property_type2")
					$scope.house_property_type2_error= "Select Property2 Type";
				else if(type == "house_property_type3")
					$scope.house_property_type3_error= "Select Property2 Type";
				return false;
			}else{
				if(type == "house_property")
					$scope.house_property_error= "";
				else if(type == "house_property_type1")
					$scope.house_property_type1_error= "";
				else if(type == "house_property_type2")
					$scope.house_property_type2_error= "";
				else if(type == "house_property_type3")
					$scope.house_property_type3_error= "";
				return true;
			}
		}else if((type == "house_property" || type== "house_property_type1" || type== "house_property_type2" || type== "house_property_type3") && value==undefined){
			if(type == "house_property")
				$scope.house_property_error= "Do you have House Property?";
			else if(type == "house_property_type1")
				$scope.house_property_type1_error= "Property Type Selection required";
			else if(type == "house_property_type2")
				$scope.house_property_type2_error= "Property Type Selection required";
			else if(type == "house_property_type3")
				$scope.house_property_type3_error= "Property Type Selection required";
			return false;
		}

		// no_of_property
		if((type == "no_of_property") && value!==undefined){
			if(value!=''){
				$scope.no_of_property_error= "";
				return true;
			}else{
				$scope.no_of_property_error= "Please Select No of Property";
				return false;
			}
		}else if((type == "no_of_property" ) && value==undefined){
			$scope.no_of_property_error= "No of Property is required";
			return false;
		}

		// property address
		if((type == "property1_add" || type=="property2_add" || type=="property3_add") && value!==undefined){
			if(value!=''){ 
				if(type == "property1_add")
					$scope.property1_add_error= "";
				else if(type == "property2_add")
					$scope.property2_add_error= "";
				else if(type == "property3_add")
					$scope.property3_add_error= "";
				return true;
			}else{
				if(type == "property1_add")
					$scope.property1_add_error= "Please Enter Address";
				else if(type == "property2_add")
					$scope.property2_add_error= "Please Enter Address";
				else if(type == "property3_add")
					$scope.property3_add_error= "Please Enter Address";
				return false;
			}
		}else if((type == "property1_add" || type=="property2_add" || type=="property3_add") && value==undefined){
			if(type == "property1_add")
				$scope.property1_add_error= "Property Address is required";
			else if(type == "property2_add")
				$scope.property2_add_error= "Property Address is required";
			else if(type == "property3_add")
				$scope.property3_add_error= "Property Address is required";
			return false;
		}

		// Property Pin
		if((type == "property1_pin" || type == "property2_pin" || type == "property3_pin") && value!==undefined){
			var pin_length=value.length;
			if((/\d{6}/.test(value)) && pin_length==6){ 
				if(type == "property1_pin"){
					$scope.property1_pin_error= "";
					var pin_city_state = $scope.get_cityState_pin("1",value);
				}else if(type == "property2_pin"){
					$scope.property2_pin_error= "";
					var pin_city_state = $scope.get_cityState_pin("2",value);
				}else if(type == "property3_pin"){
					var pin_city_state = $scope.get_cityState_pin("3",value);
					$scope.property3_pin_error= "";
				}
				return true;
			}else{
				if(type == "property1_pin")
					$scope.property1_pin_error= "Please Enter Valid PIN";
				else if(type == "property2_pin")
					$scope.property2_pin_error= "Please Enter Valid PIN";
				else if(type == "property3_pin")
					$scope.property3_pin_error= "Please Enter Valid PIN";
				return false;
			}
		}else if((type == "property1_pin" || type == "property2_pin" || type == "property3_pin") && value==undefined){
			if(type == "property1_pin")
				$scope.property1_pin_error= "PIN is required";
			else if(type == "property2_pin")
				$scope.property2_pin_error= "PIN is required";
			else if(type == "property3_pin")
				$scope.property3_pin_error= "PIN is required";
			return false;
		}

		//Town/city
		if((type == "property1_city" || type=="property2_city" || type=="property3_city") && value!==undefined){
			if(value!=''){ 
				if(type == "property1_city")
					$scope.property1_city_error= "";
				else if(type == "property2_city")
					$scope.property2_city_error= "";
				else if(type == "property3_city")
					$scope.property3_city_error= "";
				return true;
			}else{
				if(type == "property1_city")
					$scope.property1_city_error= "Please Enter City";
				else if(type == "property2_city")
					$scope.property2_city_error= "Please Enter City";
				else if(type == "property3_city")
					$scope.property3_city_error= "Please Enter City";
				return false;
			}
		}else if((type == "property1_city" || type=="property2_city" || type=="property3_city") && value==undefined){
			if(type == "property1_city")
				$scope.property1_city_error= "City is required";
			else if(type == "property2_city")
				$scope.property2_city_error= "City is required";
			else if(type == "property3_city")
				$scope.property3_city_error= "City is required";
			return false;
		}

		// State/country
		if((type == "property1_state" || type=="property2_state" || type=="property3_state") && value!==undefined){
			if(value!=''){
				if(type == "property1_state")
					$scope.property1_state_error= "";
				else if(type == "property2_state")
					$scope.property2_state_error= "";
				else if(type == "property3_state")
					$scope.property3_state_error= "";
				return true;
			}else{
				if(type == "property1_state")
					$scope.property1_state_error= "Please Select State";
				else if(type == "property2_state")
					$scope.property2_state_error= "Please Select State";
				else if(type == "property3_state")
					$scope.property3_state_error= "Please Select State";
				return false;
			}
		}else if((type == "property1_state" || type=="property2_state" || type=="property3_state") && value==undefined){
			if(type == "property1_state")
				$scope.property1_state_error= "State is required";
			else if(type == "property2_state")
				$scope.property2_state_error= "State is required";
			else if(type == "property3_state")
				$scope.property3_state_error= "State is required";
			return false;
		}

		// co=owned?
		if((type == "co_owned1" || type == "co_owned2" || type == "co_owned3") && value!==undefined){
			if(value!=''){
				if(type == "co_owned1")
					$scope.co_owned1_error= "";
				else if(type == "co_owned2")
					$scope.co_owned2_error= "";
				else if(type == "co_owned3")
					$scope.co_owned3_error= "";
				return true;
			}else{
				if(type == "co_owned1")
					$scope.co_owned1_error= "Is this Property co-owned";
				else if(type == "co_owned2")
					$scope.co_owned2_error= "Is this Property co-owned";
				else if(type == "co_owned3")
					$scope.co_owned3_error= "Is this Property co-owned";
				return false;
			}
		}else if((type == "co_owned1" || type == "co_owned2" || type == "co_owned3") && value==undefined){
			if(type == "co_owned1")
				$scope.co_owned1_error= "Is this co-owned";
			else if(type == "co_owned2")
				$scope.co_owned2_error= "Is this co-owned";
			else if(type == "co_owned3")
				$scope.co_owned3_error= "Is this co-owned";
			return false;
		}

		//property1_co_owner_name
		if(( type=="property1_co_owner_name" || type=="property2_co_owner_name" || type=="property3_co_owner_name") && value!==undefined){
			if(value!=''){ 
				if(type == "property1_co_owner_name")
					$scope.property1_co_owner_name_error= "";
				else if(type == "property2_co_owner_name")
					$scope.property2_co_owner_name_error= "";
				else if(type == "property3_co_owner_name")
					$scope.property3_co_owner_name_error= "";
				return true;
			}else{
				if(type == "property1_co_owner_name")
					$scope.property1_co_owner_name_error= "Please Enter Co-owner Name";
				else if(type == "property2_co_owner_name")
					$scope.property2_co_owner_name_error= "Please Enter Co-owner Name";
				else if(type == "property3_co_owner_name")
					$scope.property3_co_owner_name_error= "Please Enter Co-owner Name";
				return false;
			}
		}else if((type=="property1_co_owner_name" || type=="property2_co_owner_name" || type=="property3_co_owner_name") && value==undefined){
			if(type == "property1_co_owner_name")
				$scope.property1_co_owner_name_error= "Co-owner Name is required";
			else if(type == "property2_co_owner_name")
				$scope.property2_co_owner_name_error= "Co-owner Name is required";
			else if(type == "property3_co_owner_name")
				$scope.property3_co_owner_name_error= "Co-owner Name is required";
			return false;
		}

		// Municipal_tax/share_in_property
		if((type=="share_in_property1" || type=="share_in_property2" || type=="share_in_property3" || type=="property1_percent_share" || 
			type=="property2_percent_share" || type=="property3_percent_share" || type=="property1_municipal_tax" || 
			type=="property2_municipal_tax" || type=="property3_municipal_tax") && value!==undefined){
			value = $scope.number_format(value);
			if(/^[0-9]*$/.test(value) && value!=''){
				if(type == "share_in_property1")
					$scope.share_in_property1_error= "";
				else if(type == "share_in_property2")
					$scope.share_in_property2_error= "";
				else if(type == "share_in_property3")
					$scope.share_in_property3_error= "";
				else if(type == "property1_percent_share")
					$scope.property1_percent_share_error= "";
				else if(type == "property2_percent_share")
					$scope.property1_percent_share_error= "";
				else if(type == "property3_percent_share")
					$scope.property1_percent_share_error= "";
				else if(type == "property1_municipal_tax")
					$scope.property1_municipal_tax_error= "";
				else if(type == "property2_municipal_tax")
					$scope.property2_municipal_tax_error= "";
				else if(type == "property3_municipal_tax")
					$scope.property3_municipal_tax_error= "";
				return true;
			}else{
				if(type == "share_in_property1")
					$scope.share_in_property1_error= "Please Enter valid Shares in property";
				else if(type == "share_in_property2")
					$scope.share_in_property2_error= "Please Enter valid Shares in property";
				else if(type == "share_in_property3")
					$scope.share_in_property3_error= "Please Enter valid Shares in property";
				else if(type == "property1_percent_share")
					$scope.property1_percent_share_error= "Please Enter Valid Persent Share";
				else if(type == "property2_percent_share")
					$scope.property1_percent_share_error= "Please Enter Valid Persent Share";
				else if(type == "property3_percent_share")
					$scope.property1_percent_share_error= "Please Enter Valid Persent Share";
				else if(type == "property1_municipal_tax")
					$scope.property1_municipal_tax_error= "Please Enter Valid Municipal Tax";
				else if(type == "property2_municipal_tax")
					$scope.property2_municipal_tax_error= "Please Enter Valid Municipal Tax";
				else if(type == "property3_municipal_tax")
					$scope.property3_municipal_tax_error= "Please Enter Valid Municipal Tax";
				return false;
			}
		}

		//percent_share/share_in_property1
		if((type=="property1_rent_received" || type=="property2_rent_received" || type=="property3_rent_received") && value!==undefined){
			value = $scope.number_format(value);
			if(/^[0-9]*$/.test(value) && value!='' ){
				if(type == "property1_rent_received")
					$scope.property1_rent_received_error= "";
				else if(type == "property2_rent_received")
					$scope.property2_rent_received_error= "";
				else if(type == "property3_rent_received")
					$scope.property3_rent_received_error= "";
				return true;
			}else{
				if(type == "property1_rent_received")
					$scope.property1_rent_received_error= "Please Enter valid Rent Received";
				else if(type == "property2_rent_received")
					$scope.property2_rent_received_error= "Please Enter valid Rent Received";
				else if(type == "property3_rent_received")
					$scope.property3_rent_received_error= "Please Enter valid Rent Received";
				return false;
			}
		}

		//co-owner PAN
		if((type == "property1_co_owner_pan" || type == "property2_co_owner_pan" || type == "property3_co_owner_pan") && value!==undefined){
			var pan_length=value.length;
			if((/[A-Z]{5}\d{4}[A-Z]{1}/.test(value)) && pan_length==10 ){
				if(type == "property1_co_owner_pan")
					$scope.property1_co_owner_pan_error= "";
				else if(type == "property2_co_owner_pan")
					$scope.property2_co_owner_pan_error= "";
				else if(type == "property3_co_owner_pan")
					$scope.property3_co_owner_pan_error= "";
				return true;
			}else{
				if(type == "property1_co_owner_pan")
					$scope.property1_co_owner_pan_error= "Please Enter Valid PAN";
				else if(type == "property2_co_owner_pan")
					$scope.property2_co_owner_pan_error= "Please Enter Valid PAN";
				else if(type == "property3_co_owner_pan")
					$scope.property3_co_owner_pan_error= "Please Enter Valid PAN";
				return false;
			}
		}else if((type == "property1_co_owner_pan" || type == "property2_co_owner_pan" || type == "property3_co_owner_pan") && value==undefined)
			return true;

		// Other Source
		if((type == "saving_bank_int" || type == "fd_interest" || type == "other_interest" || type == "commission" || type == "other_income" || type == "family_pension") && value!==undefined){
			value = $scope.number_format(value);
			var length=value.length;
			if( /^\d+$/.test(value) || length==0 ){
				if(type == "saving_bank_int")
					$scope.saving_bank_int_error= "";
				else if(type == "fd_interest")
					$scope.fd_interest_error= "";
				else if(type == "other_interest")
					$scope.other_interest_error= "";
				else if(type == "commission")
					$scope.commission_error= "";
				else if(type == "other_income")
					$scope.other_income_error= "";
				else if(type == "family_pension")
					$scope.family_pension_error= "";
				return true;
			}else{
				if(type == "saving_bank_int")
					$scope.saving_bank_int_error= "Enter Valid Saving Bank Interest";
				else if(type == "fd_interest")
					$scope.fd_interest_error= "Enter Valid FD Interest";
				else if(type == "other_interest")
					$scope.other_interest_error= "Enter Valid Other Interest";
				else if(type == "commission")
					$scope.commission_error= "Enter Valid Commission";
				else if(type == "other_income")
					$scope.other_income_error= "Enter Valid Other Income";
				else if(type == "family_pension")
					$scope.family_pension_error= "Enter Valid Family Pension";
				return false;
			}
			$scope.$apply();
		}else if((type == "saving_bank_int" || type == "fd_interest" || type == "other_interest" || type == "commission" || type == "other_income") && value==undefined)
			return true;
		//////////832
	}
		$scope.validation_CG = function(type,value,$event){
		//Shares
		value=$scope.number_format(value)
		var re=/^[-]?\d*$/
		if((type == "st_shares_tpv" || type == "st_shares_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "st_shares_tpv" )
					$scope.shares_tpv_error = "";
				if( type == "st_shares_tsv" )
					$scope.shares_tsv_error = "";
				$scope.calculate_gain_loss('ST_shares');
				return true;
			}else{
				if( type == "st_shares_tpv" )
					$scope.shares_tpv_error = "Please Enter Valid Short Term Purchase Value .";
				if( type == "st_shares_tsv" )
					$scope.shares_tsv_error = "Please Enter Valid Short Term Sell Value.";
				
				return false;
			}

		}else if((type == "st_shares_tpv" || type == "st_shares_tsv" || type == "st_shares_gain") && value==undefined){
			return false;
		}

		if(re.test(value)){
			if( type == "st_shares_gain" )
				$scope.shares_gain_error = "";
			return true;
		}else{
			if( type == "st_shares_gain" )
				$scope.shares_gain_error = "Please Enter Valid Short Term Gain / Loss.";
			return false;
		}


		if((type == "lt_shares_tpv" || type == "lt_shares_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "lt_shares_tpv" )
					$scope.lt_shares_tpv_error = "";
				if( type == "lt_shares_tsv" )
					$scope.lt_shares_tsv_error = "";
				$scope.calculate_gain_loss('LT_shares');
				return true;
			}else{
				if( type == "lt_shares_tpv" )
					$scope.lt_shares_tpv_error = "Please Enter Valid Long Term Purchase Value .";
				if( type == "lt_shares_tsv" )
					$scope.lt_shares_tsv_error = "Please Enter Valid Long Term Sell Value.";
		
				return false;
			}
		}else if((type == "lt_shares_tpv" || type == "lt_shares_tsv" || type == "lt_shares_gain" ) && value==undefined){
			return false;
		}

		if(re.test(value)){
			if( type == "lt_shares_gain" )
				$scope.lt_shares_gain_error = "";
			return true;
		}else{
			if( type == "lt_shares_gain" )
				$scope.lt_shares_gain_error = "Please Enter Valid Long Term Gain / Loss.";
			return false;
		}


		//Equity Mutual Fund
		if((type == "st_equity_tpv" || type == "st_equity_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "st_equity_tpv" )
					$scope.st_equity_tpv_error = "";
				if( type == "st_equity_tsv" )
					$scope.st_equity_tsv_error = "";
				$scope.calculate_gain_loss('ST_equity');
				return true;
			}else{
				if( type == "st_equity_tpv" )
					$scope.st_equity_tpv_error = "Please Enter Valid Short Term Purchase Value .";
				if( type == "st_equity_tsv" )
					$scope.st_equity_tsv_error = "Please Enter Valid Short Term Sell Value.";
		
				return false;
			}
		}else if((type == "st_equity_tpv" || type == "st_equity_tsv" || type == "st_equity_gain" ) && value==undefined){
			return false;
		}



		if(re.test(value)){
			if( type == "st_equity_gain" )
				$scope.st_equity_gain_error = "";
			return true;
		}else{
			if( type == "st_equity_gain" )
				$scope.st_equity_gain_error = "Please Enter Valid Short Term Gain / Loss.";
			return false;
		}


		if((type == "lt_equity_tpv" || type == "lt_equity_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "lt_equity_tpv" )
					$scope.lt_equity_tpv_error = "";
				if( type == "lt_equity_tsv" )
					$scope.lt_equity_tsv_error = "";

				$scope.calculate_gain_loss('LT_equity');
				return true;
			}else{
				if( type == "lt_equity_tpv" )
					$scope.lt_equity_tpv_error = "Please Enter Valid Long Term Purchase Value .";
				if( type == "lt_equity_tsv" )
					$scope.lt_equity_tsv_error = "Please Enter Valid Long Term Sell Value.";

				return false;
			}
		}else if((type == "lt_equity_tpv" || type == "lt_equity_tsv" || type == "lt_equity_gain" ) && value==undefined){
			return false;
		}


		if(re.test(value)){
			if( type == "lt_equity_gain" )
				$scope.lt_equity_gain_error = "";
			return true;
		}else{
			if( type == "lt_equity_gain" )
				$scope.lt_equity_gain_error = "Please Enter Valid Long Term Gain / Loss.";
			return false;
		}


		//Debt Mutual Fund/ Liquid Fund
		if((type == "st_debt_MF_tpv" || type == "st_debt_MF_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "st_debt_MF_tpv" )
					$scope.st_debt_MF_tpv_error = "";
				if( type == "st_debt_MF_tsv" )
					$scope.st_debt_MF_tsv_error = "";
			
				$scope.calculate_gain_loss('ST_debt_MF');
				return true;
			}else{
				if( type == "st_debt_MF_tpv" )
					$scope.st_debt_MF_tpv_error = "Please Enter Valid Short Term Purchase Value .";
				if( type == "st_debt_MF_tsv" )
					$scope.st_debt_MF_tsv_error = "Please Enter Valid Short Term Sell Value.";
			
				return false;
			}
		}else if((type == "st_debt_MF_tpv" || type == "st_debt_MF_tsv" || type == "st_debt_MF_gain" ) && value==undefined){
			return false;
		}

		if(re.test(value)){
			if( type == "st_debt_MF_gain" )
				$scope.st_debt_MF_gain_error = "";
			return true;
		}else{
			if( type == "st_debt_MF_gain" )
				$scope.st_debt_MF_gain_error = "Please Enter Valid Short Term Gain / Loss.";
			return false;
		}


		if((type == "lt_debt_MF_tpv" || type == "lt_debt_MF_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "lt_debt_MF_tpv" )
					$scope.lt_debt_MF_tpv_error = "";
				if( type == "lt_debt_MF_tsv" )
					$scope.lt_debt_MF_tsv_error = "";
				if( type == "lt_debt_MF_gain" )
					$scope.lt_debt_MF_gain_error = "";
				$scope.calculate_gain_loss('LT_debt_MF');
				return true;
			}else{
				if( type == "lt_debt_MF_tpv" )
					$scope.lt_debt_MF_tpv_error = "Please Enter Valid Long Term Purchase Value .";
				if( type == "lt_debt_MF_tsv" )
					$scope.lt_debt_MF_tsv_error = "Please Enter Valid Long Term Sell Value.";
				if( type == "lt_debt_MF_gain" )
					$scope.lt_debt_MF_gain_error = "Please Enter Valid Long Term Gain / Loss.";
				return false;
			}
		}else if((type == "lt_debt_MF_tpv" || type == "lt_debt_MF_tsv" || type == "lt_debt_MF_gain" ) && value==undefined){
			return false;
		}

		if(re.test(value)){
			if( type == "lt_debt_MF_gain" )
				$scope.lt_debt_MF_gain_error = "";
			return true;
		}else{
			if( type == "lt_debt_MF_gain" )
				$scope.lt_debt_MF_gain_error = "Please Enter Valid Lomg Term Gain / Loss.";
			return false;
		}


		//Listed Debentures
		if((type == "st_listed_d_tpv" || type == "st_listed_d_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "st_listed_d_tpv" )
					$scope.st_listed_d_tpv_error = "";
				if( type == "st_listed_d_tsv" )
					$scope.st_listed_d_tsv_error = "";
				if( type == "st_listed_d_gain" )
					$scope.st_listed_d_gain_error = "";
				$scope.calculate_gain_loss('ST_listed_d');
				return true;
			}else{
				if( type == "st_listed_d_tpv" )
					$scope.st_listed_d_tpv_error = "Please Enter Valid Short Term Purchase Value .";
				if( type == "st_listed_d_tsv" )
					$scope.st_listed_d_tsv_error = "Please Enter Valid Short Term Sell Value.";
				if( type == "st_listed_d_gain" )
					$scope.st_listed_d_gain_error = "Please Enter Valid Short Term Gain / Loss.";
				return false;
			}
		}else if((type == "st_listed_d_tpv" || type == "st_listed_d_tsv" || type == "st_listed_d_gain" ) && value==undefined){
			return false;
		}

		if(re.test(value)){
			if( type == "st_listed_d_gain" )
				$scope.st_listed_d_gain_error = "";
			return true;
		}else{
			if( type == "st_listed_d_gain" )
				$scope.st_listed_d_gain_error = "Please Enter Valid Lomg Term Gain / Loss.";
			return false;
		}



		if((type == "lt_listed_d_tpv" || type == "lt_listed_d_tsv") && value!==undefined){
			value = $scope.number_format(value);
			if((/^[0-9]*$/.test(value))){
				if( type == "lt_listed_d_tpv" )
					$scope.lt_listed_d_tpv_error = "";
				if( type == "lt_listed_d_tsv" )
					$scope.lt_listed_d_tsv_error = "";
				if( type == "lt_listed_d_gain" )
					$scope.lt_listed_d_gain_error = "";
				$scope.calculate_gain_loss('LT_listed_d');
				return true;
			}else{
				if( type == "lt_listed_d_tpv" )
					$scope.lt_listed_d_tpv_error = "Please Enter Valid Long Term Purchase Value .";
				if( type == "lt_listed_d_tsv" )
					$scope.lt_listed_d_tsv_error = "Please Enter Valid Long Term Sell Value.";
				if( type == "lt_listed_d_gain" )
					$scope.lt_listed_d_gain_error = "Please Enter Valid Long Term Gain / Loss.";
				return false;
			}
		}else if((type == "lt_listed_d_tpv" || type == "lt_listed_d_tsv" || type == "lt_listed_d_gain" ) && value==undefined){
			return false;
		}


		if(re.test(value)){
			if( type == "lt_listed_d_gain" )
				$scope.lt_listed_d_gain_error = "";
			return true;
		}else{
			if( type == "lt_listed_d_gain" )
				$scope.lt_listed_d_gain_error = "Please Enter Valid Lomg Term Gain / Loss.";
			return false;
		}
	}


	$('.loader').show();
	$(window).load(function() {

		console.log('ID : '+$scope.user.id);
		console.log('Client ID : '+$scope.user.client_id);
    	client_id=$scope.user.client_id

		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				if(response.client_id!=0 && $scope.user.id == ''){
					client_id = response.client_id;
					$scope.get_income_details();
					$scope.get_breakup_details();
					$scope.get_co_owner_details();
				}
				else if($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				if($scope.user.id != ''){
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						async:false,
						data:{'username':$scope.user.id},
						success: function(response){
							// console.log(response);
							// client_id = response.client_id;
							// $rootScope.username=response.username

							$.ajax({
								url:'/get_return_details/',
								type:'POST',
								dataType: 'json',
								data:{'client_id':client_id},
								success: function(response){
									console.log(response);
									$scope.return_detail_fy = response.return_year;
									selected_fy = response.return_year;
								}
							});
							email = response.email;
							$scope.get_income_details();
							$scope.get_breakup_details();
							$scope.get_co_owner_details();

							// $('.loader').hide();

						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}

				$('.loader').hide();
			}
		});


	});

	// $scope.$watch("demoInput", function(){
	// 	console.log('ID : '+$scope.user.id);

	// 	$.ajax({
	// 		url:'/get_client/',
	// 		type:'POST',
	// 		dataType: 'json',
	// 		data:{'mail':'mail'},
	// 		success: function(response){
	// 			// console.log(response);
	// 			if(response.client_id!=0 && $scope.user.id == ''){
	// 				client_id = response.client_id;
	// 				$scope.get_income_details();
	// 				$scope.get_breakup_details();
	// 				$scope.get_co_owner_details();

	// 			}
	// 			else if($scope.user.id == ''){
	// 				$scope.display_logout = {"display" : "block"};
	// 				$scope.display_login = {"display" : "none"};
	// 			}
	// 			if($scope.user.id != ''){
	// 				$.ajax({
	// 					url:'/get_client_byID/',
	// 					type:'POST',
	// 					dataType: 'json',
	// 					data:{'username':$scope.user.id},
	// 					success: function(response){
	// 						// console.log(response);
	// 						client_id = response.client_id;
	// 						$.ajax({
	// 							url:'/get_return_details/',
	// 							type:'POST',
	// 							dataType: 'json',
	// 							data:{'client_id':client_id},
	// 							success: function(response){
	// 								console.log(response);
	// 								$scope.return_detail_fy = response.return_year;
	// 								selected_fy = response.return_year;
	// 							}
	// 						});
	// 						email = response.email;
	// 						$scope.get_income_details();
	// 						$scope.get_breakup_details();
	// 						$scope.get_co_owner_details();
	// 					}
	// 				});
	// 			}
	// 			else{
	// 				$scope.display_logout = {"display" : "block"};
	// 				$scope.display_login = {"display" : "none"};
	// 			}
	// 		}
	// 	});
	// },100);

	$scope.business_profession_income = function(value){
	    if(value == 'yes')
		    $scope.business_pi_msg = 'If you have earned income from Business or Profession you are required to provide details of the same. One of our tax experts will call you to better understand your business financials.';
		else
			$scope.business_pi_msg = '';
	}

	$scope.OtherDetails_save1 = function(type,value,$event){
		var status=$scope.validation_check1();
		console.log(status)
		$scope.status1= 'F';

		if(status==true)
			$scope.status1= 'T';
		else
			$scope.status1= 'F';
	}
	$scope.OtherDetails_save2 = function(type,value,$event){
		var status=$scope.validation_check2();
	    $scope.status2= 'F';

		if(status==true){
			// $scope.user_tracking('submitted','Income details - House Property');
	    	$scope.status2= 'T';
		}
		else
	    	$scope.status2= 'F';
	}
	$scope.OtherDetails_save3 = function(type,value,$event){
		var status=$scope.validation_check3();
	    $scope.status3= 'F';

		if(status==true){
			$scope.save_business_profession_income();
			// $scope.user_tracking('submitted','Income details - Business & Profession');
	    	$scope.status3= 'T';
		}
		else
	    	$scope.status3= 'F';
	}
	$scope.OtherDetails_save4 = function(type,value,$event){
		var status = $scope.validation_check4();
	    $scope.status4 = 'F';
		if(status==true){
			// $scope.user_tracking('submitted','Income details - Capital Gain');
	    	$scope.status4= 'T';
	    	window.location.href = redirect_url+'/deductions/'+client_id;
		}
		else
	    	$scope.status4= 'F';
	}
	$scope.gotoSaveIncomeDetails = function(type,value,$event){
		var status=$scope.validation_check5();
		console.log(status)
		if(status==true){

			console.log('if '+status)
			var pass_obj = { "saving_bank_int":$scope.number_format($scope.user.saving_bank_int)};
			pass_obj["client_id"] = client_id;
			pass_obj["FY"] = 2018;
			pass_obj["fd_interest"] = $scope.number_format($scope.user.fd_interest);
			pass_obj["other_interest"] = $scope.number_format($scope.user.other_interest);
			pass_obj["commission"] = $scope.number_format($scope.user.commission);
			pass_obj["other_income"] = $scope.number_format($scope.user.other_income);
			pass_obj["family_pension"] = $scope.number_format($scope.user.family_pension);
			pass_obj["post_office_interest"]= $scope.number_format($scope.user.post_office_interest);
			var data_pass = JSON.stringify(pass_obj);

			console.log(data_pass)

			$.ajax({
				url:'/save_other_sources/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					console.log(status)
					if(response.status=='success'){
						// $scope.user_tracking('submitted','Income details - Other Sources');
						$scope.status5= 'T';
						$scope.show_exempt_income();
						// window.location.href = redirect_url+'/deductions/'+client_id;
					}else{
						// $scope.user_tracking('error in data',response.status);
						$scope.status5= 'F';
					}
				},error:function(response){
					console.log(response)
				}
			});
		}else{
			console.log('validations fail')
      		// $scope.user_tracking('error in data','other sources validation');
      		$scope.status5= 'F';
		}

		// $scope.$apply();
	}

	$scope.show_exempt_income=function(){
			console.log('show exempt income')
		    $('#otherSourcesTab').removeClass('active');
            $('#exemptIncomeTab').tab('show');
	}

	$scope.validation_check1 = function(){

		var no_of_companies_status = $scope.validation("no_of_companies",$scope.user.no_of_companies);
		no_of_company = $scope.user.no_of_companies;

		/*$.ajax({
				url:'/edit_Employer_Details/',
				type:'POST',
				dataType: 'json',
				async:false,
				data:{'option':'delete_all','client_id':client_id},
				success: function(response){
					// console.log(response);
				}
		});*/

		$scope.EnableDisable_company1_name();
		$scope.EnableDisable_company2_name();
		$scope.EnableDisable_company3_name();

		if($scope.user.earn_salary=='yes'){


			if(no_of_company==0){
				no_of_companies_status = false;
				// $scope.user_tracking('error in data','no of company is 0');
			}
			
			$scope.save_Employer_Details();

			if(no_of_company==1 || no_of_company==2 || no_of_company==3 ){

				$scope.load_all_breakup_modal(1)

				var company1validation = $scope.validation_company1();
				console.log('company1validation '+company1validation)
				if (company1validation==true) {
					var pass_obj = { "emp_name":$scope.user.company1_emp_name };
					pass_obj["client_id"] = client_id;
					pass_obj["c_tan"] = $scope.user.company1_tan;
					pass_obj["c_pan"] = $scope.user.company1_pan;
					pass_obj["c_address"] = $scope.user.company1_add;
					pass_obj["c_pin"] = $scope.user.company1_pin;
					pass_obj["c_city"] = $scope.user.company1_city;
					pass_obj["c_state"] = $scope.user.company1_state;
					if($scope.user.company1_salary_provision == undefined || $scope.user.company1_salary_provision == 0)
						pass_obj["c_salary"] = 0;
					else
						pass_obj["c_salary"] = $scope.number_format($scope.user.company1_salary_provision);
					if($scope.user.company1_perquisites == undefined || $scope.user.company1_perquisites == 0)
						pass_obj["c_perq"] = 0;
					else
						pass_obj["c_perq"] = $scope.number_format($scope.user.company1_perquisites);
					if($scope.user.company1_profits_lieu_salary == undefined || $scope.user.company1_profits_lieu_salary == 0)
						pass_obj["c_profits_lieu"] = 0;
					else
						pass_obj["c_profits_lieu"] = $scope.number_format($scope.user.company1_profits_lieu_salary);
					if($scope.user.company1_profession_tax == undefined || $scope.user.company1_profession_tax == 0)
						pass_obj["c_Ptax"] = 0;
					else
						pass_obj["c_Ptax"] = $scope.number_format($scope.user.company1_profession_tax);
					if($scope.user.company1_entertainment_allowance == undefined || $scope.user.company1_entertainment_allowance == 0)
						pass_obj["c_Eallowance"] = 0;
					else
						pass_obj["c_Eallowance"] = $scope.number_format($scope.user.company1_entertainment_allowance);
					if($scope.user.company1_LTA == undefined || $scope.user.company1_LTA == 0)
						pass_obj["c_lta"] = 0;
					else
						pass_obj["c_lta"] = $scope.number_format($scope.user.company1_LTA);
					if($scope.user.company1_HRA == undefined || $scope.user.company1_HRA == 0)
						pass_obj["c_hra"] = 0;
					else
						pass_obj["c_hra"] = $scope.number_format($scope.user.company1_HRA);
					if($scope.user.company1_total_allowances == undefined || $scope.user.company1_total_allowances == 0)
						pass_obj["c_OtherAllowance"] = 0;
					else
						pass_obj["c_OtherAllowance"] = $scope.number_format($scope.user.company1_total_allowances);
					
					pass_obj["c_employer_category"]=$scope.user.company1_employer_category
					pass_obj["earn_salary"]=$scope.user.earn_salary
					var data_pass = JSON.stringify(pass_obj);

					$.ajax({
						url:'/save_Employer_Details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							console.log(response);
							// if(response.status != 'success')
								// $scope.user_tracking('error in data',response.status);
						}
					});
				}
				else{
					no_of_companies_status = false;
	      			// $scope.user_tracking('error in data','company 1 validation');
				}
			}
			if( no_of_company==2 || no_of_company==3 ){

				$scope.load_all_breakup_modal(2)

				var company2validation = $scope.validation_company2();
				if (company2validation==true) {
					var pass_obj = { "emp_name":$scope.user.company2_emp_name};
					pass_obj["client_id"] = client_id;
					pass_obj["c_tan"] = $scope.user.company2_tan;
					pass_obj["c_pan"] = $scope.user.company2_pan;
					pass_obj["c_address"] = $scope.user.company2_add;
					pass_obj["c_pin"] = $scope.user.company2_pin;
					pass_obj["c_city"] = $scope.user.company2_city;
					pass_obj["c_state"] = $scope.user.company2_state;
					if($scope.user.company2_salary_provision == undefined || $scope.user.company2_salary_provision == 0)
						pass_obj["c_salary"] = 0;
					else
						pass_obj["c_salary"] = $scope.number_format($scope.user.company2_salary_provision);
					if($scope.user.company2_perquisites == undefined || $scope.user.company2_perquisites == 0)
						pass_obj["c_perq"] = 0;
					else
						pass_obj["c_perq"] = $scope.number_format($scope.user.company2_perquisites);
					if($scope.user.company2_profits_lieu_salary == undefined || $scope.user.company2_profits_lieu_salary == 0)
						pass_obj["c_profits_lieu"] = 0;
					else
						pass_obj["c_profits_lieu"] = $scope.number_format($scope.user.company2_profits_lieu_salary);
					if($scope.user.company2_profession_tax == undefined || $scope.user.company2_profession_tax == 0)
						pass_obj["c_Ptax"] = 0;
					else
						pass_obj["c_Ptax"] = $scope.number_format($scope.user.company2_profession_tax);
					if($scope.user.company2_entertainment_allowance == undefined || $scope.user.company2_entertainment_allowance == 0)
						pass_obj["c_Eallowance"] = 0;
					else
						pass_obj["c_Eallowance"] = $scope.number_format($scope.user.company2_entertainment_allowance);
					if($scope.user.company2_LTA == undefined || $scope.user.company2_LTA == 0)
						pass_obj["c_lta"] = 0;
					else
						pass_obj["c_lta"] = $scope.number_format($scope.user.company2_LTA);
					if($scope.user.company2_HRA == undefined || $scope.user.company2_HRA == 0)
						pass_obj["c_hra"] = 0;
					else
						pass_obj["c_hra"] = $scope.number_format($scope.user.company2_HRA);
					if($scope.user.company2_total_allowances == undefined || $scope.user.company2_total_allowances == 0)
						pass_obj["c_OtherAllowance"] = 0;
					else
						pass_obj["c_OtherAllowance"] = $scope.number_format($scope.user.company2_total_allowances);
					
					pass_obj["c_employer_category"]=$scope.user.company2_employer_category
					pass_obj["earn_salary"]=$scope.user.earn_salary
					var data_pass = JSON.stringify(pass_obj);

					$.ajax({
						url:'/save_Employer_Details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							// if(response.status != 'success')
								// $scope.user_tracking('error in data',response.status);
						}
					});
				}
				else{
					no_of_companies_status = false;
	      			// $scope.user_tracking('error in data','company 2 validation');
				}
			}
			if( no_of_company==3 ){

				$scope.load_all_breakup_modal(3)

				var company3validation = $scope.validation_company3();
				if (company3validation==true) {
					var pass_obj = { "emp_name":$scope.user.company3_emp_name};
					pass_obj["client_id"] = client_id;
					pass_obj["c_tan"] = $scope.user.company3_tan;
					pass_obj["c_pan"] = $scope.user.company3_pan;
					pass_obj["c_address"] = $scope.user.company3_add;
					pass_obj["c_pin"] = $scope.user.company3_pin;
					pass_obj["c_city"] = $scope.user.company3_city;
					pass_obj["c_state"] = $scope.user.company3_state;
					if($scope.user.company3_salary_provision ==undefined || $scope.user.company3_salary_provision == 0)
						pass_obj["c_salary"] = 0;
					else
						pass_obj["c_salary"] = $scope.number_format($scope.user.company3_salary_provision);
					if($scope.user.company3_perquisites == undefined || $scope.user.company3_perquisites == 0)
						pass_obj["c_perq"] = 0;
					else
						pass_obj["c_perq"] = $scope.number_format($scope.user.company3_perquisites);
					if($scope.user.company3_profits_lieu_salary == undefined || $scope.user.company3_profits_lieu_salary == 0)
						pass_obj["c_profits_lieu"] = 0;
					else
						pass_obj["c_profits_lieu"] = $scope.number_format($scope.user.company3_profits_lieu_salary);
					if($scope.user.company3_profession_tax == undefined || $scope.user.company3_profession_tax == 0)
						pass_obj["c_Ptax"] = 0;
					else
						pass_obj["c_Ptax"] = $scope.number_format($scope.user.company3_profession_tax);
					if($scope.user.company3_entertainment_allowance == undefined || $scope.user.company3_entertainment_allowance == 0)
						pass_obj["c_Eallowance"] = 0;
					else
						pass_obj["c_Eallowance"] = $scope.number_format($scope.user.company3_entertainment_allowance);
					if($scope.user.company3_LTA == undefined || $scope.user.company3_LTA == 0)
						pass_obj["c_lta"] = 0;
					else
						pass_obj["c_lta"] = $scope.number_format($scope.user.company3_LTA);
					if($scope.user.company3_HRA == undefined || $scope.user.company3_HRA == 0)
						pass_obj["c_hra"] = 0;
					else
						pass_obj["c_hra"] = $scope.number_format($scope.user.company3_HRA);
					if($scope.user.company3_total_allowances == undefined || $scope.user.company3_total_allowances == 0)
						pass_obj["c_OtherAllowance"] = 0;
					else
						pass_obj["c_OtherAllowance"] = $scope.number_format($scope.user.company3_total_allowances);
					
					pass_obj["c_employer_category"]=$scope.user.company3_employer_category
					pass_obj["earn_salary"]=$scope.user.earn_salary

					var data_pass = JSON.stringify(pass_obj);

					$.ajax({
						url:'/save_Employer_Details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							// if(response.status != 'success')
								// $scope.user_tracking('error in data',response.status);
						}
					});
				}
				else{
					no_of_companies_status = false;
	      			// $scope.user_tracking('error in data','company 3 validation');
				}
			}

			if( no_of_companies_status==true ){
				// $scope.user_tracking('submitted','Income details - Salary');
				return true;
			}
			else
				return false;


		}else if($scope.user.earn_salary=='no'){

			$.ajax({
				url:'/edit_Employer_Details/',
				type:'POST',
				dataType: 'json',
				async:false,
				data:{'option':'delete_all','client_id':client_id},
				success: function(response){
					console.log(response);
				}
			});

			$scope.user.no_of_companies=''
			$scope.reset_company_data();
			return true;

		}
	}

	$scope.reset_company_data=function(){

		$scope.user.company1_pan= '';
		$scope.user.company1_tan= ''
		$scope.user.company1_emp_name='';
		$scope.user.company1_add= '';
		$scope.user.company1_pin='';
		$scope.user.company1_city= '';
		$scope.user.company1_state= '';
		$scope.user.company1_employer_category=''
	
		$scope.user.company1_calculated_sal= ''
		$scope.user.company1_salary_provision = ''
		$scope.user.company1_perquisites=''
		$scope.user.company1_profits_lieu_salary=''
		$scope.user.company1_profession_tax=''
		$scope.user.company1_entertainment_allowance=0
		$scope.user.company1_LTA=''
		$scope.user.company1_HRA=''
		$scope.user.company1_total_allowances=''

	}

	$scope.validation_check2 = function(){
		var house_property_status = $scope.validation("house_property",$scope.user.house_property);
		var no_of_property_status = $scope.validation("no_of_property",$scope.user.no_of_property);

		if( house_property_status==true ){
			if($scope.user.house_property=='yes'){
				if(no_of_property_status==true){
					var status=$scope.checkPropertyDetails();
					if(status==true){
						var no_of_property = $scope.user.no_of_property;
						if(no_of_property==1){
							$scope.delete_house_property_detail('2');
							$scope.delete_house_property_detail('3');
						}
						if(no_of_property==2)
							$scope.delete_house_property_detail('3');
					}else{
						// $scope.user_tracking('error in data','Saving HP');
					}
					return status;
				}
				else{
					// $scope.user_tracking('error in data','Select No of properties');
					return false;
				}
			}
			else{
				$scope.delete_house_property_detail('all');

				$scope.reset_house_property_data();
				return true;
			}
		}else{
			// $scope.user_tracking('error in data','Select Do you have HP');
			return false;
		}
	}

	$scope.reset_house_property_data=function(){


			$scope.user.no_of_property=''
			$scope.user.house_property_type1=''
			$scope.user.property1_add=''
			$scope.user.property1_pin=''
			$scope.user.property1_city=''
			$scope.user.property1_state = ''
			$scope.user.share_in_property1=''
			$scope.user.co_owned1 =''

			$scope.user.property1_municipal_tax= 0
			$scope.user.property1_interest_payable= 0
			$scope.user.property1_rent_received=0

	}

	$scope.validation_check3 = function(){
		return true;
	}
	$scope.validation_check4 = function(){
		var final_status = true;
		var shares_status = false;
		var equity_status = false;
		var debt_MF_status = false;
		var listed_d_status = false;
		if($scope.user.shares == true){
			var temp_shares_status = true;
			var st_shares_tpv_status = $scope.validation_CG("st_shares_tpv",$scope.user.st_shares_tpv);
			var st_shares_tsv_status = $scope.validation_CG("st_shares_tsv",$scope.user.st_shares_tsv);
			var st_shares_gain_status = $scope.validation_CG("st_shares_gain",$scope.user.st_shares_gain);
			var lt_shares_tpv_status = $scope.validation_CG("lt_shares_tpv",$scope.user.lt_shares_tpv);
			var lt_shares_tsv_status = $scope.validation_CG("lt_shares_tsv",$scope.user.lt_shares_tsv);
			var lt_shares_gain_status = $scope.validation_CG("lt_shares_gain",$scope.user.lt_shares_gain);
			if(st_shares_tpv_status == true && st_shares_tsv_status == true && st_shares_gain_status == true)
				temp_shares_status = true;
			else
				temp_shares_status = false;

			if(temp_shares_status == true && lt_shares_tpv_status == true && lt_shares_tsv_status == true && lt_shares_gain_status == true){
				shares_status = true;
			}
			else{
				// $scope.user_tracking('error in data','CG - Shares validation');
				final_status = false;
			}
		}
		if($scope.user.equity == true){
			var temp_equity_status = true;
			var st_equity_tpv_status = $scope.validation_CG("st_equity_tpv",$scope.user.st_equity_tpv);
			var st_equity_tsv_status = $scope.validation_CG("st_equity_tsv",$scope.user.st_equity_tsv);
			var st_equity_gain_status = $scope.validation_CG("st_equity_gain",$scope.user.st_equity_gain);
			var lt_equity_tpv_status = $scope.validation_CG("lt_equity_tpv",$scope.user.lt_equity_tpv);
			var lt_equity_tsv_status = $scope.validation_CG("lt_equity_tsv",$scope.user.lt_equity_tsv);
			var lt_equity_gain_status = $scope.validation_CG("lt_equity_gain",$scope.user.lt_equity_gain);
			if(st_equity_tpv_status == true && st_equity_tsv_status == true && st_equity_gain_status == true)
				temp_equity_status = true;
			else
				temp_equity_status = false;

			if(temp_equity_status == true && lt_equity_tpv_status == true && lt_equity_tsv_status == true && lt_equity_gain_status == true){
				equity_status = true;
			}
			else{
				// $scope.user_tracking('error in data','CG - Equity validation');
				final_status = false;
			}
		}
		if($scope.user.debt_MF == true){
			var temp_debt_status = true;
			var st_debt_MF_tpv_status = $scope.validation_CG("st_debt_MF_tpv",$scope.user.st_debt_MF_tpv);
			var st_debt_MF_tsv_status = $scope.validation_CG("st_debt_MF_tsv",$scope.user.st_debt_MF_tsv);
			var st_debt_MF_gain_status = $scope.validation_CG("st_debt_MF_gain",$scope.user.st_debt_MF_gain);
			var lt_debt_MF_tpv_status = $scope.validation_CG("lt_debt_MF_tpv",$scope.user.lt_debt_MF_tpv);
			var lt_debt_MF_tsv_status = $scope.validation_CG("lt_debt_MF_tsv",$scope.user.lt_debt_MF_tsv);
			var lt_debt_MF_gain_status = $scope.validation_CG("lt_debt_MF_gain",$scope.user.lt_debt_MF_gain);
			if(st_debt_MF_tpv_status == true && st_debt_MF_tsv_status == true && st_debt_MF_gain_status == true)
				temp_debt_status = true;
			else
				temp_debt_status = false;

			if(temp_debt_status == true && lt_debt_MF_tpv_status == true && lt_debt_MF_tsv_status == true && lt_debt_MF_gain_status == true){
				debt_MF_status = true;
			}
			else{
				// $scope.user_tracking('error in data','CG - Debt MF validation');
				final_status = false;
			}
		}
		if($scope.user.listed_d == true){
			var temp_listed_status = true;
			var st_listed_d_tpv_status = $scope.validation_CG("st_listed_d_tpv",$scope.user.st_listed_d_tpv);
			var st_listed_d_tsv_status = $scope.validation_CG("st_listed_d_tsv",$scope.user.st_listed_d_tsv);
			var st_listed_d_gain_status = $scope.validation_CG("st_listed_d_gain",$scope.user.st_listed_d_gain);
			var lt_listed_d_tpv_status = $scope.validation_CG("lt_listed_d_tpv",$scope.user.lt_listed_d_tpv);
			var lt_listed_d_tsv_status = $scope.validation_CG("lt_listed_d_tsv",$scope.user.lt_listed_d_tsv);
			var lt_listed_d_gain_status = $scope.validation_CG("lt_listed_d_gain",$scope.user.lt_listed_d_gain);
			if(st_listed_d_tpv_status == true && st_listed_d_tsv_status == true && st_listed_d_gain_status == true)
				temp_listed_status = true;
			else
				temp_listed_status = false;

			if(temp_listed_status == true && lt_listed_d_tpv_status == true && lt_listed_d_tsv_status == true && lt_listed_d_gain_status == true){
				listed_d_status = true;
			}
			else{
				// $scope.user_tracking('error in data','CG - Listed Debentures validation');
				final_status = false;
			}
		}
		// alert('share : '+$scope.user.shares);
		// alert('equity : '+$scope.user.equity);
		// alert('debt_MF : '+$scope.user.debt_MF);
		// alert('listed_d : '+$scope.user.listed_d);

		if(shares_status == true){
			var pass_obj = { "variable":'shares',"client_id":client_id };
			pass_obj["st_shares_tpv"] = $scope.number_format($scope.user.st_shares_tpv);
			pass_obj["st_shares_tsv"] = $scope.number_format($scope.user.st_shares_tsv);
			pass_obj["st_shares_gain"] = $scope.number_format($scope.user.st_shares_gain);
			pass_obj["lt_shares_tpv"] = $scope.number_format($scope.user.lt_shares_tpv);
			pass_obj["lt_shares_tsv"] = $scope.number_format($scope.user.lt_shares_tsv);
			pass_obj["lt_shares_gain"] = $scope.number_format($scope.user.lt_shares_gain);
  			var data_pass = JSON.stringify(pass_obj);
			$.ajax({
				url:'/save_shares/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){ console.log(response); }
			});
		}else{
			$.ajax({
				url:'/delete_capital_gain/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'type':'shares'},
				success: function(response){ }
			});
		}
		if(equity_status == true){
			var pass_obj = { "variable":'equity',"client_id":client_id };
			pass_obj["st_equity_tpv"] = $scope.number_format($scope.user.st_equity_tpv);
			pass_obj["st_equity_tsv"] = $scope.number_format($scope.user.st_equity_tsv);
			pass_obj["st_equity_gain"] = $scope.number_format($scope.user.st_equity_gain);
			pass_obj["lt_equity_tpv"] = $scope.number_format($scope.user.lt_equity_tpv);
			pass_obj["lt_equity_tsv"] = $scope.number_format($scope.user.lt_equity_tsv);
			pass_obj["lt_equity_gain"] = $scope.number_format($scope.user.lt_equity_gain);
  			var data_pass = JSON.stringify(pass_obj);
			$.ajax({
				url:'/save_equity/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){
					console.log(response);
				}
			});
		}else{
			$.ajax({
				url:'/delete_capital_gain/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'type':'equity'},
				success: function(response){ }
			});
		}
		if(debt_MF_status == true){
			var pass_obj = { "variable":'Debt Mutual Fund/ Liquid Fund',"client_id":client_id };
			pass_obj["st_debt_MF_tpv"] = $scope.number_format($scope.user.st_debt_MF_tpv);
			pass_obj["st_debt_MF_tsv"] = $scope.number_format($scope.user.st_debt_MF_tsv);
			pass_obj["st_debt_MF_gain"] = $scope.number_format($scope.user.st_debt_MF_gain);
			pass_obj["lt_debt_MF_tpv"] = $scope.number_format($scope.user.lt_debt_MF_tpv);
			pass_obj["lt_debt_MF_tsv"] = $scope.number_format($scope.user.lt_debt_MF_tsv);
			pass_obj["lt_debt_MF_gain"] = $scope.number_format($scope.user.lt_debt_MF_gain);
  			var data_pass = JSON.stringify(pass_obj);
			$.ajax({
				url:'/save_debt_MF/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){ }
			});
		}else{
			$.ajax({
				url:'/delete_capital_gain/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'type':'debt_MF'},
				success: function(response){ }
			});
		}
		if(listed_d_status == true){
			var pass_obj = { "variable":'Listed Debentures',"client_id":client_id };
			pass_obj["st_listed_d_tpv"] = $scope.number_format($scope.user.st_listed_d_tpv);
			pass_obj["st_listed_d_tsv"] = $scope.number_format($scope.user.st_listed_d_tsv);
			pass_obj["st_listed_d_gain"] = $scope.number_format($scope.user.st_listed_d_gain);
			pass_obj["lt_listed_d_tpv"] = $scope.number_format($scope.user.lt_listed_d_tpv);
			pass_obj["lt_listed_d_tsv"] = $scope.number_format($scope.user.lt_listed_d_tsv);
			pass_obj["lt_listed_d_gain"] = $scope.number_format($scope.user.lt_listed_d_gain);
  			var data_pass = JSON.stringify(pass_obj);
			$.ajax({
				url:'/save_listed_d/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){ }
			});
		}else{
			$.ajax({
				url:'/delete_capital_gain/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'type':'listed_d'},
				success: function(response){ }
			});
		}
		return final_status;
	}
	$scope.validation_check5 = function(){
		var saving_bank_int_status = $scope.validation("saving_bank_int",$scope.user.saving_bank_int);
		var fd_interest_status = $scope.validation("fd_interest",$scope.user.fd_interest);
		var other_interest_status = $scope.validation("other_interest",$scope.user.other_interest);
		var commission_status = $scope.validation("commission",$scope.user.commission);
		var other_income_status = $scope.validation("other_income",$scope.user.other_income);
		var family_pension_status = $scope.validation("family_pension",$scope.user.family_pension);

		if( saving_bank_int_status==true && fd_interest_status==true && other_interest_status==true && 
			commission_status==true && other_income_status==true && family_pension_status==true){
			return true;
		}else
			return false;
	}

	$scope.validation_company1 = function(){
		var final_status = false;
		var c_name_status = $scope.validation("company1_emp_name",$scope.user.company1_emp_name);
		var pan_status = $scope.validation("company1_pan",$scope.user.company1_pan);
		var tan_status = $scope.validation("company1_tan",$scope.user.company1_tan);
		var c_add_status = $scope.validation("company1_add",$scope.user.company1_add);
		var pin_status = $scope.validation("company1_pin",$scope.user.company1_pin);
		var city_status = $scope.validation("company1_city",$scope.user.company1_city);
		var state_status = $scope.validation("company1_state",$scope.user.company1_state);
		var salary_status = $scope.validation("company1_salary_provision",$scope.user.company1_salary_provision);
		var vop_status = $scope.validation("company1_perquisites",$scope.user.company1_perquisites);
		var pLieuS_status = $scope.validation("company1_profits_lieu_salary",$scope.user.company1_profits_lieu_salary);
		var pTax_status = $scope.validation("company1_profession_tax",$scope.user.company1_profession_tax);
		var ea_status = $scope.validation("company1_entertainment_allowance",$scope.user.company1_entertainment_allowance);
		
		var breakup_status=$scope.final_breakup_validation('company1')
		console.log(breakup_status)
		// var lta_status = $scope.validation("company1_LTA",$scope.user.company1_LTA);
		// var hra_status = $scope.validation("company1_HRA",$scope.user.company1_HRA);
		// var oa_status = $scope.validation("company1_other_allowance",$scope.user.company1_other_allowance);

		var lta_status =true
		var hra_status =true
		var oa_status = true

		// console.log('c_name_status'+c_name_status)
		// console.log('pan_status'+pan_status)
		// console.log('c_name_status'+c_name_status)
		// console.log('tan_status'+tan_status)
		// console.log('c_add_status'+c_add_status)
		// console.log('pin_status'+pin_status)
		// console.log('city_status'+city_status)
		// console.log('state_status'+state_status)
		// console.log('salary_status'+salary_status)
		// console.log('vop_status'+vop_status)
		// console.log('pLieuS_status'+pLieuS_status)
		// console.log('pTax_status'+pTax_status)
		// console.log('ea_status'+ea_status)
		// console.log('lta_status'+lta_status)
		// console.log('hra_status'+hra_status)
		// console.log('oa_status'+oa_status)

		if( c_name_status==true && pan_status==true && tan_status==true && c_add_status==true && pin_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && state_status==true && salary_status==true && vop_status==true && pLieuS_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && pTax_status==true && ea_status==true && lta_status==true && hra_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && oa_status==true && breakup_status==true){
			final_status = true;
		}else
			final_status = false;

		// alert(c_name_status);
		// alert(pan_status);
		// alert(tan_status);
		// alert(c_add_status);
		// alert(pin_status);
		// alert(state_status);
		// alert(salary_status);
		// alert(vop_status);
		// alert(pLieuS_status);
		// alert(pTax_status);
		// alert(ea_status);
		// alert(lta_status);
		// alert(hra_status);
		// alert(oa_status);

		// alert('final_status');
		// alert(final_status);
		return final_status;
	}
		$scope.validation_company2 = function(){
		var final_status = false;
		var c_name_status = $scope.validation("company2_emp_name",$scope.user.company2_emp_name);
		var pan_status = $scope.validation("company2_pan",$scope.user.company2_pan);
		var tan_status = $scope.validation("company2_tan",$scope.user.company2_tan);
		var c_add_status = $scope.validation("company2_add",$scope.user.company2_add);
		var pin_status = $scope.validation("company2_pin",$scope.user.company2_pin);
		var city_status = $scope.validation("company2_city",$scope.user.company2_city);
		var state_status = $scope.validation("company2_state",$scope.user.company2_state);
		var salary_status = $scope.validation("company2_salary_provision",$scope.user.company2_salary_provision);
		var vop_status = $scope.validation("company2_perquisites",$scope.user.company2_perquisites);
		var pLieuS_status = $scope.validation("company2_profits_lieu_salary",$scope.user.company2_profits_lieu_salary);
		var pTax_status = $scope.validation("company2_profession_tax",$scope.user.company2_profession_tax);
		var ea_status = $scope.validation("company2_entertainment_allowance",$scope.user.entertainment_allowance);
		// var lta_status = $scope.validation("company2_LTA",$scope.user.company2_LTA);
		// var hra_status = $scope.validation("company2_HRA",$scope.user.company2_HRA);
		// var oa_status = $scope.validation("company2_other_allowance",$scope.user.company2_other_allowance);
		var lta_status =true
		var hra_status =true
		var oa_status = true
		var breakup_status=$scope.final_breakup_validation('company2')

		// console.log('company2')
		// console.log('c_name_status'+c_name_status)
		// console.log('pan_status'+pan_status)
		// console.log('c_name_status'+c_name_status)
		// console.log('tan_status'+tan_status)
		// console.log('c_add_status'+c_add_status)
		// console.log('pin_status'+pin_status)
		// console.log('city_status'+city_status)
		// console.log('state_status'+state_status)
		// console.log('salary_status'+salary_status)
		// console.log('vop_status'+vop_status)
		// console.log('pLieuS_status'+pLieuS_status)
		// console.log('pTax_status'+pTax_status)
		// console.log('ea_status'+ea_status)
		// console.log('lta_status'+lta_status)
		// console.log('hra_status'+hra_status)
		// console.log('oa_status'+oa_status)
		// console.log('breakup '+breakup_status)


		if( c_name_status==true && pan_status==true && tan_status==true && c_add_status==true && pin_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && state_status==true && salary_status==true && vop_status==true && pLieuS_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && pTax_status==true && ea_status==true && lta_status==true && hra_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && oa_status==true && breakup_status==true){
			final_status = true;
		}else
			final_status = false;

		return final_status;
	}
	$scope.validation_company3 = function(){
		var final_status = false;
		var c_name_status = $scope.validation("company3_emp_name",$scope.user.company3_emp_name);
		var pan_status = $scope.validation("company3_pan",$scope.user.company3_pan);
		var tan_status = $scope.validation("company3_tan",$scope.user.company3_tan);
		var c_add_status = $scope.validation("company3_add",$scope.user.company3_add);
		var pin_status = $scope.validation("company3_pin",$scope.user.company3_pin);
		var city_status = $scope.validation("company3_city",$scope.user.company3_city);
		var state_status = $scope.validation("company3_state",$scope.user.company3_state);
		var salary_status = $scope.validation("company3_salary_provision",$scope.user.company3_salary_provision);
		var vop_status = $scope.validation("company3_perquisites",$scope.user.company3_perquisites);
		var pLieuS_status = $scope.validation("company3_profits_lieu_salary",$scope.user.company3_profits_lieu_salary);
		var pTax_status = $scope.validation("company3_profession_tax",$scope.user.company3_profession_tax);
		var ea_status = $scope.validation("company3_entertainment_allowance",$scope.user.company3_entertainment_allowance);
		// var lta_status = $scope.validation("company3_LTA",$scope.user.company3_LTA);
		// var hra_status = $scope.validation("company3_HRA",$scope.user.company3_HRA);
		// var oa_status = $scope.validation("company3_other_allowance",$scope.user.company3_other_allowance);
		var breakup_status=$scope.final_breakup_validation('company3')

		var lta_status =true
		var hra_status =true
		var oa_status = true

		// console.log('company3')
		// console.log('c_name_status'+c_name_status)
		// console.log('pan_status'+pan_status)
		// console.log('c_name_status'+c_name_status)
		// console.log('tan_status'+tan_status)
		// console.log('c_add_status'+c_add_status)
		// console.log('pin_status'+pin_status)
		// console.log('city_status'+city_status)
		// console.log('state_status'+state_status)
		// console.log('salary_status'+salary_status)
		// console.log('vop_status'+vop_status)
		// console.log('pLieuS_status'+pLieuS_status)
		// console.log('pTax_status'+pTax_status)
		// console.log('ea_status'+ea_status)
		// console.log('lta_status'+lta_status)
		// console.log('hra_status'+hra_status)
		// console.log('oa_status'+oa_status)
		// console.log('breakup '+breakup_status)

		if( c_name_status==true && pan_status==true && tan_status==true && c_add_status==true && pin_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && state_status==true && salary_status==true && vop_status==true && pLieuS_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && pTax_status==true && ea_status==true && lta_status==true && hra_status==true){
			final_status = true;
		}else
			final_status = false;
		if(final_status==true && oa_status==true && breakup_status==true){
			final_status = true;
		}else
			final_status = false;

		return final_status;
	}

	$scope.checkPropertyDetails = function(){
		var no_of_property = $scope.user.no_of_property;
		var P1 = true;
		var P2 = true;
		var P3 = true;
		if(no_of_property == 1 || no_of_property == 2 || no_of_property == 3){
			var P1_propertyTypeStatus = $scope.validation("house_property_type1",$scope.user.house_property_type1);
			var P1_addStatus = $scope.validation("property1_add",$scope.user.property1_add);
			var P1_pinStatus = $scope.validation("property1_pin",$scope.user.property1_pin);
			var P1_cityStatus = $scope.validation("property1_city",$scope.user.property1_city);
			var P1_co_ownedStatus = $scope.validation("co_owned1",$scope.user.co_owned1);
			if ($scope.user.co_owned1=='no'){
				var P1_co_owner_status=true
			}
			else{
				var P1_co_owner_status=$scope.co_owner_validation('property1','co_owner')
			}

			if ($scope.user.house_property_type1=='let_out'){
				var P1_tenant_status=$scope.co_owner_validation('property1','tenant')
			}
			else{
				var P1_tenant_status=true
			}
			
			var P1_municipal_tax = $scope.validation("property1_municipal_tax",$scope.user.property1_municipal_tax);
			var P1_rent_received = $scope.validation("property1_rent_received",$scope.user.property1_rent_received);
			if(P1_propertyTypeStatus==true && P1_addStatus==true && P1_pinStatus==true && P1_cityStatus==true && P1_co_ownedStatus==true && P1_municipal_tax==true &&
				P1_rent_received == true && P1_co_owner_status==true && P1_tenant_status==true){
				P1 = true;
				var pass_obj = { "propertyID":1,"propertyType":$scope.user.house_property_type1, "address":$scope.user.property1_add};
				pass_obj["client_id"] = client_id;
				pass_obj["city"] = $scope.user.property1_city;
				pass_obj["pin"] = $scope.user.property1_pin;
				pass_obj["state"] = $scope.user.property1_state;
				pass_obj["co_owned"] = $scope.user.co_owned1;
				if ($scope.user.co_owned1=='no') {
					pass_obj["share_in_property"] = 100;
					pass_obj["co_owned_name"] = 0;
					pass_obj["co_owned_pan"] = 0;
					pass_obj["percent_share"] = 0;
				}
				else{
					pass_obj["share_in_property"] = $scope.user.share_in_property1;
					pass_obj["co_owned_name"] = $scope.user.property1_co_owner_name;
					pass_obj["co_owned_pan"] = $scope.user.property1_co_owner_pan;
					pass_obj["percent_share"] = $scope.number_format($scope.user.property1_percent_share);
				}
				pass_obj["municipal_tax"] = $scope.number_format($scope.user.property1_municipal_tax);
				pass_obj["interest_payable"] = $scope.number_format($scope.user.property1_interest_payable);
				pass_obj["rent_received"] = $scope.number_format($scope.user.property1_rent_received);
      			var data_pass = JSON.stringify(pass_obj);

				$.ajax({
					url:'/save_house_property/',
					type:'POST',
					dataType: 'json',
					data:{'data':data_pass},
					success: function(response){
						console.log(response);
						// if(response.status != 'success')
							// $scope.user_tracking('error in data',response.status);
					}
				});

				$scope.co_owner_api('1')
				$scope.tenant_api('1')
			}
			else{
      			// $scope.user_tracking('error in data','HP 1 validation');
				P1 = false;
			}
			// alert('P1 '+P1);
		}
		if(no_of_property == 2 || no_of_property == 3){
			var P2_propertyTypeStatus = $scope.validation("house_property_type2",$scope.user.house_property_type2);
			var P2_addStatus = $scope.validation("property2_add",$scope.user.property2_add);
			var P2_pinStatus = $scope.validation("property2_pin",$scope.user.property2_pin);
			var P2_cityStatus = $scope.validation("property2_city",$scope.user.property2_city);
			var P2_co_ownedStatus = $scope.validation("co_owned2",$scope.user.co_owned2);
			if ($scope.user.co_owned2=='no'){
				var P2_co_owner_status=true
			}
			else{

				var P2_co_owner_status=$scope.co_owner_validation('property2','co_owner')
			}

			if ($scope.user.house_property_type2=='let_out'){
				var P2_tenant_status=$scope.co_owner_validation('property1','tenant')
			}
			else{
				var P2_tenant_status=true
			}
			var P2_municipal_tax = $scope.validation("property2_municipal_tax",$scope.user.property2_municipal_tax);
			var P2_rent_received = $scope.validation("property2_rent_received",$scope.user.property2_rent_received);

			if(P2_propertyTypeStatus==true && P2_addStatus==true && P2_pinStatus==true && P2_cityStatus==true && 
				 P2_co_owner_status==true && P2_municipal_tax==true &&
				P2_rent_received == true && P2_tenant_status==true){
				P2 = true;
				var pass_obj = { "propertyID":2,"propertyType":$scope.user.house_property_type2, "address":$scope.user.property2_add};
				pass_obj["client_id"] = client_id;
				pass_obj["city"] = $scope.user.property2_city;
				pass_obj["pin"] = $scope.user.property2_pin;
				pass_obj["state"] = $scope.user.property2_state;
				pass_obj["co_owned"] = $scope.user.co_owned2;
				if ($scope.user.co_owned2=='no') {
					pass_obj["share_in_property"] = 100;
					pass_obj["co_owned_name"] = 0;
					pass_obj["co_owned_pan"] = 0;
					pass_obj["percent_share"] = 0;
				}
				else{
					pass_obj["share_in_property"] = $scope.user.share_in_property2;
					pass_obj["co_owned_name"] = $scope.user.property2_co_owner_name;
					pass_obj["co_owned_pan"] = $scope.user.property2_co_owner_pan;
					pass_obj["percent_share"] = $scope.number_format($scope.user.property2_percent_share);
				}
				pass_obj["municipal_tax"] = $scope.number_format($scope.user.property2_municipal_tax);
				pass_obj["interest_payable"] = $scope.number_format($scope.user.property2_interest_payable);
				pass_obj["rent_received"] = $scope.number_format($scope.user.property2_rent_received);
				var data_pass = JSON.stringify(pass_obj);

				$.ajax({
					url:'/save_house_property/',
					type:'POST',
					dataType: 'json',
					data:{'data':data_pass},
					success: function(response){
						// console.log(response);
						// if(response.status != 'success')
							// $scope.user_tracking('error in data',response.status);
					}
				});

				$scope.co_owner_api('2')
				$scope.tenant_api('2')
			}
			else{
      			// $scope.user_tracking('error in data','HP 2 validation');
				P2 = false;
			}
			// alert('P2 '+P2);
		}
		if(no_of_property == 3){
			var P3_propertyTypeStatus = $scope.validation("house_property_type3",$scope.user.house_property_type3);
			var P3_addStatus = $scope.validation("property3_add",$scope.user.property3_add);
			var P3_pinStatus = $scope.validation("property3_pin",$scope.user.property3_pin);
			var P3_cityStatus = $scope.validation("property3_city",$scope.user.property3_city);
			var P3_co_ownedStatus = $scope.validation("co_owned3",$scope.user.co_owned3);
			if ($scope.user.co_owned3=='no'){
				var P3_co_owner_status=true
			}
			else{
				var P3_co_owner_status=$scope.co_owner_validation('property3','co_owner')
			}

			if ($scope.user.house_property_type2=='let_out'){
				var P3_tenant_status=$scope.co_owner_validation('property1','tenant')
			}
			else{
				var P3_tenant_status=true
			}
			var P3_municipal_tax = $scope.validation("property3_municipal_tax",$scope.user.property3_municipal_tax);
			var P3_rent_received = $scope.validation("property3_rent_received",$scope.user.property3_rent_received);
			//
			if(P3_propertyTypeStatus==true && P3_addStatus==true && P3_pinStatus==true && P3_cityStatus==true && 
				P3_co_owner_status==true && P3_municipal_tax==true &&
				P3_rent_received == true && P3_tenant_status==true){
				P3 = true;

				var pass_obj = { "propertyID":3,"propertyType":$scope.user.house_property_type3, "address":$scope.user.property3_add};
				pass_obj["client_id"] = client_id;
				pass_obj["city"] = $scope.user.property3_city;
				pass_obj["pin"] = $scope.user.property3_pin;
				pass_obj["state"] = $scope.user.property3_state;
				pass_obj["co_owned"] = $scope.user.co_owned3;
				if ($scope.user.co_owned3=='no') {
					pass_obj["share_in_property"] = 100;
					pass_obj["co_owned_name"] = 0;
					pass_obj["co_owned_pan"] = 0;
					pass_obj["percent_share"] = 0;
				}
				else{
					pass_obj["share_in_property"] = $scope.user.share_in_property3;
					pass_obj["co_owned_name"] = $scope.user.property3_co_owner_name;
					pass_obj["co_owned_pan"] = $scope.user.property3_co_owner_pan;
					pass_obj["percent_share"] = $scope.number_format($scope.user.property3_percent_share);
				}
				pass_obj["municipal_tax"] = $scope.number_format($scope.user.property3_municipal_tax);
				pass_obj["interest_payable"] = $scope.number_format($scope.user.property3_interest_payable);
				pass_obj["rent_received"] = $scope.number_format($scope.user.property3_rent_received);
      			var data_pass = JSON.stringify(pass_obj);

				$.ajax({
					url:'/save_house_property/',
					type:'POST',
					dataType: 'json',
					data:{'data':data_pass},
					success: function(response){
						// console.log(response);
						// if(response.status != 'success')
							// $scope.user_tracking('error in data',response.status);
					}
				});

				$scope.co_owner_api('3')
				$scope.tenant_api('3')
			}
			else{
      			// $scope.user_tracking('error in data','HP 3 validation');
				P3 = false;
			}
			// alert('P3 '+P3);
		}
		return P1 && P2 && P3;
	}

	$scope.get_cityState_pin = function(property,value){
		$.ajax({
			url:'/get_cityState_pin/',
			type:'POST',
			dataType: 'json',
			data:{'pin':value},
			success: function(response){
				// console.log(response);
				city_record = response.city_state.records;
				if(city_record.length==0)
					$scope.property1_pin_error= "Please Enter Valid PIN";
				else{
					city = response.city_state.records['0']['taluk'];
					statename = response.city_state.records['0']['statename'];
					if(property=='1'){
						$scope.user.property1_city = city;
						$scope.property1_pin_error= "";
						$scope.user.property1_state = statename;
					}
					if(property=='2'){
						$scope.user.property2_city = city;
						$scope.property2_pin_error= "";
						$scope.user.property2_state = statename;
					}
					if(property=='3'){
						$scope.user.property3_city = city;
						$scope.property3_pin_error= "";
						$scope.user.property3_state = statename;
					}
					if(property=='c1'){
						$scope.user.company1_city = city;
						$scope.user.company1_state = statename;
						label_float('city1_div');
					}
					if(property=='c2'){
						$scope.user.company2_city = city;
						$scope.user.company2_state = statename;
						label_float('city2_div');
					}
					if(property=='c3'){
						$scope.user.company3_city = city;
						$scope.user.company3_state = statename;
						label_float('city3_div');
					}
					$scope.$apply();
				}
			}
		});
	}

	$scope.delete_form16 = function(no){
		if(no==1){
			pan = $scope.user.company1_pan;
			company_name = $scope.user.company1_emp_name;
		}
		if(no==2){
			pan = $scope.user.company2_pan;
			company_name = $scope.user.company2_emp_name;
		}
		if(no==3){
			pan = $scope.user.company3_pan;
			company_name = $scope.user.company3_emp_name;
		}
		$.ajax({
			url:'/delete_form16/',
			type:'POST',
			dataType: 'json',
			data:{'pan':pan,'client_id':client_id,'company_name':company_name},
			success: function(response){
				console.log(response);
				$scope.delete_Employer_Details(company_name);
			}
		});
	}

	$scope.reset_filename = function(){
		form16_1_no = 1;
		count_run1 = 0;
	}
	$scope.reset_filename2 = function(){
		form16_2_no = 1;
		count_run2 = 0;
	}
	$scope.reset_filename3 = function(){
		form16_3_no = 1;
		count_run3 = 0;
	}

	form16_1_count=0
	$scope.onLoad_form16_1 = function (e, reader, file, fileList, fileOjects, fileObj){

		$scope.form16_1_fileCount = file_count = fileList.length;

		if(form16_1_count==0){
			$scope.form16_1_fileObj1='';
			$scope.form16_1_fileObj2='';
		}
	
		console.log(file['name']);
		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		console.log(filetype);
		var expr = /.pdf/;
		var check_pdf = filetype.match(expr);
		console.log(check_pdf);

		form16_1_count++;

		if(check_pdf != null){
			if(form16_1_count==1){
				$scope.form16_1_fileObj1=fileObj;
			}else if(form16_1_count==2){
				$scope.form16_1_fileObj2=fileObj;
			}
			
			$scope.form16_1_error= "";
			return true;
		}else{
			$scope.form16_1_error= "Upload Only PDF file.";
			return false;
		}
	}

	$scope.upload_form16_1 = function (){
		form16_1_count=0

		var form16_1_no = 1;
		var switch_form16_1 = 1;
		var cur1_file_no = 0;

		if($scope.form16_1_fileCount!=0 &&  $scope.form16_1_fileObj1!=''){
				
			for(i=1;i<=$scope.form16_1_fileCount;i++){
				switch_form16_1 = 0;
				var file_name =client_id+'_form16_Part'+form16_1_no;
				
				passwordA = $scope.user.form1_passwordA
				passwordB = $scope.user.form1_passwordB

				file_count = $scope.form16_1_fileCount;

				if(form16_1_no==1){
					var fileObj=$scope.form16_1_fileObj1;
				}else if (form16_1_no==2){
					var fileObj=$scope.form16_1_fileObj2;
				}

				var data=JSON.stringify(fileObj);

				form16_1_no++;

				$('.loader').show();
				$.ajax({
					url:'/file_upload/',
					type:'POST',
					dataType: 'json',
					data:{'data':data,'pan':file_name ,'type':'1'},
					success: function(response){
						console.log(response);
						status = JSON.stringify(response['status']).replace(/\"/g, "");
						pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
						cur1_file_no++;
						console.log(file_count)
						console.log(cur1_file_no)
						////if(response['status'] == 'success' && file_count==form16_1_no-1 && count_run1==0){
						if(response['status'] == 'success' && file_count==cur1_file_no && count_run1==0){
							count_run1 = 1;
							$.ajax({
								url:'/form16_run_jar/',
								type:'POST',
								dataType: 'json',
								data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'1','client_id':client_id },
								success: function(response){
									console.log(response);
									if(response['status']=='success'){
										output = JSON.stringify(response['output']).replace(/\"/g, "");
										full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
										switch_form16_1 = 1;
										if(output==0){
											//alert('Saved in DB');
											$.ajax({
												url:'/get_DB_data/',
												type:'POST',
												dataType: 'json',
												data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
												success: function(response){
													console.log(response);
													$('.loader').hide(); 
<<<<<<< HEAD
													//$scope.Form16_save_Employer_Details();
													if(response['status']=='success'){
														if(response['year_match']=='Y'){
															$scope.Form16_save_Employer_Details();
=======
													//$scope.Form16_save_Employer_Details(1);
													if(response['status']=='success'){
														if(response['year_match']=='Y'){
															$scope.Form16_save_Employer_Details(1);
>>>>>>> 94e2a906ca3684471de452c2e85a8b96fcdaefaa
														}else{
															swal_message='Mismatch in Assessment Year';
															$scope.Form16sweetalert('Ooops !',swal_message,'info') 
														}		
													}else{
											            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
														$scope.Form16sweetalert('Ooops !',swal_message,'info') 
													}
													//setTimeout(function(){ window.location.reload(); }, 500);
													
												},error:function(response) {
										            $('.loader').hide(); 
										            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
										        }
											});
										}
										else if(output == 1){
											$('.loader').hide();
											//alert('Part A missing');
											swal_message='Part A missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == 2){
											$('.loader').hide();
											//alert('Part B missing');
											swal_message='Part B missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == -1){
											$('.loader').hide();
											//alert('Something Went Wrong.');
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_1 = '';
												$scope.form1_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
											
										}else{
											$('.loader').hide();
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_1 = '';
												$scope.form1_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
									}else{
										$('.loader').hide();
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								},error:function(response) {
						            $('.loader').hide(); 
						            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
						        }
							});
						}else{
							if(file_count==cur1_file_no){
								$('.loader').hide();
								swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
							}
						}
					},error:function(response) {
			            $('.loader').hide();
			            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info') 
			        }
				});
				$scope.form16_1_upload_error= "";
			}
			return true;
		}else{
			$scope.form16_1_upload_error= "Please upload correct file";
			return false;
		}
	};

	form16_2_count=0
	$scope.onLoad_form16_2 = function (e, reader, file, fileList, fileOjects, fileObj){

		$scope.form16_2_fileCount = file_count = fileList.length;

		if(form16_2_count==0){
			$scope.form16_2_fileObj1='';
			$scope.form16_2_fileObj2='';
		}
	
		console.log(file['name']);
		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		console.log(filetype);
		var expr = /.pdf/;
		var check_pdf = filetype.match(expr);
		console.log(check_pdf);

		form16_2_count++;

		if(check_pdf != null){
			if(form16_2_count==1){
				$scope.form16_2_fileObj1=fileObj;
			}else if(form16_2_count==2){
				$scope.form16_2_fileObj2=fileObj;
			}

			$scope.form16_2_error= "";
			return true;
		}else{
			$scope.form16_2_error= "Upload Only PDF file.";
			return false;
		}
	}

	$scope.upload_form16_2 = function (){
		form16_2_count=0

		var form16_2_no = 1;
		var switch_form16_2 = 1;
		var cur2_file_no = 0;
			
		if($scope.form16_2_fileCount!=0 &&  $scope.form16_2_fileObj1!=''){

			for(i=1;i<=$scope.form16_2_fileCount;i++){	

				switch_form16_2 = 0;
				var file_name =client_id+'_form16_Part'+form16_2_no;
				
				passwordA = $scope.user.form2_passwordA
				passwordB = $scope.user.form2_passwordB

				file_count = $scope.form16_2_fileCount;

				if(form16_2_no==1){
					var fileObj=$scope.form16_2_fileObj1;
				}else if (form16_2_no==2){
					var fileObj=$scope.form16_2_fileObj2;
				}

				var data=JSON.stringify(fileObj);

				form16_2_no++;
				
				$('.loader').show();
				$.ajax({
					url:'/file_upload/',
					type:'POST',
					dataType: 'json',
					data:{'data':data,'pan':file_name ,'type':'2'},
					success: function(response){
						console.log(response);
						status = JSON.stringify(response['status']).replace(/\"/g, "");
						pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
						cur2_file_no++;
						console.log(file_count)
						console.log(cur2_file_no)
						////if(response['status'] == 'success' && file_count==form16_2_no-1 && count_run2==0){
						if(response['status'] == 'success' && file_count==cur2_file_no && count_run2==0){
							count_run2 = 1;
							$.ajax({
								url:'/form16_run_jar/',
								type:'POST',
								dataType: 'json',
								data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'2','client_id':client_id },
								success: function(response){
									console.log(response);
									if(response['status']=='success'){
										output = JSON.stringify(response['output']).replace(/\"/g, "");
										full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
										switch_form16_2 = 1;
										if(output==0){
											//alert('Saved in DB');
											$.ajax({
												url:'/get_DB_data/',
												type:'POST',
												dataType: 'json',
												data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
												success: function(response){
													console.log(response);
													$('.loader').hide(); 
<<<<<<< HEAD
													//$scope.Form16_save_Employer_Details();
													if(response['status']=='success'){
														if(response['year_match']=='Y'){	
															$scope.Form16_save_Employer_Details();
=======
													//$scope.Form16_save_Employer_Details(2);
													if(response['status']=='success'){
														if(response['year_match']=='Y'){	
															$scope.Form16_save_Employer_Details(2);
>>>>>>> 94e2a906ca3684471de452c2e85a8b96fcdaefaa
														}else{
															swal_message='Mismatch in Assessment Year';
															$scope.Form16sweetalert('Ooops !',swal_message,'info') 
														}		
													}else{
											            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
														$scope.Form16sweetalert('Ooops !',swal_message,'info') 
													}
													//setTimeout(function(){ window.location.reload(); }, 500);
												},error:function(response) {
										            $('.loader').hide();
										            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
										        }
											});
										}
										else if(output == 1){
											$('.loader').hide(); 
											//alert('Part A missing');
											swal_message='Part A missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == 2){
											$('.loader').hide(); 
											//alert('Part B missing');
											swal_message='Part B missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == -1){
											$('.loader').hide(); 
											//alert('Something Went Wrong.');
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_2 = '';
												$scope.form2_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
										else{
											$('.loader').hide();
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_2 = '';
												$scope.form2_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
									}else{
										$('.loader').hide();
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								},error:function(response) {
						            $('.loader').hide(); 
						            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
						        }
							});
						}else{
							if(file_count==cur2_file_no){
								$('.loader').hide();
								swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
							}
						}
					},error:function(response) {
			            $('.loader').hide(); 
			            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
			        }
				});
				$scope.form16_2_upload_error= "";
				
			}
			return true;
		}else{
			$scope.form16_2_upload_error= "Please upload correct file";
			return false;
		}
	};

	form16_3_count=0
	$scope.onLoad_form16_3 = function (e, reader, file, fileList, fileOjects, fileObj){

		if(form16_3_count==0){
			$scope.form16_3_fileObj1='';
			$scope.form16_3_fileObj2='';
		}

		$scope.form16_3_fileCount = file_count = fileList.length;
	
		console.log(file['name']);
		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		console.log(filetype);
		var expr = /.pdf/;
		var check_pdf = filetype.match(expr);
		console.log(check_pdf);

		form16_3_count++;

		if(check_pdf != null){
			if(form16_3_count==1){
				$scope.form16_3_fileObj1=fileObj;
			}else if(form16_3_count==2){
				$scope.form16_3_fileObj2=fileObj;
			}

			$scope.form16_3_error= "";
			return true;
		}else{
			$scope.form16_3_error= "Upload Only PDF file.";
			return false;
		}
	}

	$scope.upload_form16_3 = function (e, reader, file, fileList, fileOjects, fileObj){
		form16_3_count=0
<<<<<<< HEAD

=======
			
>>>>>>> 94e2a906ca3684471de452c2e85a8b96fcdaefaa
		var form16_3_no = 1;
		var switch_form16_3 = 1;
		var cur3_file_no = 0;

		if($scope.form16_3_fileCount!=0 &&  $scope.form16_3_fileObj1!=''){

			for(i=1;i<=$scope.form16_3_fileCount;i++){	

				switch_form16_3 = 0;
				var file_name =client_id+'_form16_Part'+form16_3_no;
				
				passwordA = $scope.user.form3_passwordA
				passwordB = $scope.user.form3_passwordB

				file_count = $scope.form16_3_fileCount;

				if(form16_3_no==1){
					var fileObj=$scope.form16_3_fileObj1;
				}else if(form16_3_no==2){
					var fileObj=$scope.form16_3_fileObj2;
				}

				var data=JSON.stringify(fileObj);

				form16_3_no++;
				
				$('.loader').show();
				$.ajax({
					url:'/file_upload/',
					type:'POST',
					dataType: 'json',
					data:{'data':data,'pan':file_name ,'type':'3'},
					success: function(response){
						console.log(response);
						status = JSON.stringify(response['status']).replace(/\"/g, "");
						pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
						cur3_file_no++;
						console.log(file_count)
						console.log(cur3_file_no)
						////if(response['status'] == 'success' && file_count==form16_3_no-1 && count_run3==0){
						if(response['status'] == 'success' && file_count==cur3_file_no && count_run3==0){
							count_run3 = 1;
							$.ajax({
								url:'/form16_run_jar/',
								type:'POST',
								dataType: 'json',
								data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'3','client_id':client_id },
								success: function(response){
									console.log(response);
									if(response['status']=='success'){
										output = JSON.stringify(response['output']).replace(/\"/g, "");
										full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
										switch_form16_3 = 1;
										if(output==0){
											//alert('Saved in DB');
											$.ajax({
												url:'/get_DB_data/',
												type:'POST',
												dataType: 'json',
												data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
												success: function(response){
													console.log(response);
													$('.loader').hide(); 
<<<<<<< HEAD
													//$scope.Form16_save_Employer_Details();
													if(response['status']=='success'){
														if(response['year_match']=='Y'){	
															$scope.Form16_save_Employer_Details();
=======
													//$scope.Form16_save_Employer_Details(3);
													if(response['status']=='success'){
														if(response['year_match']=='Y'){	
															$scope.Form16_save_Employer_Details(3);
>>>>>>> 94e2a906ca3684471de452c2e85a8b96fcdaefaa
														}else{
															swal_message='Mismatch in Assessment Year';
															$scope.Form16sweetalert('Ooops !',swal_message,'info') 
														}		
													}else{
											            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
														$scope.Form16sweetalert('Ooops !',swal_message,'info') 
													}
													//setTimeout(function(){ window.location.reload(); }, 500);
												},error:function(response) {
										            $('.loader').hide(); 
										            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
													$scope.Form16sweetalert('Ooops !',swal_message,'info')
										        }
											});
										}
										else if(output == 1){
											$('.loader').hide(); 
											//alert('Part A missing');
											swal_message='Part A missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == 2){
											$('.loader').hide(); 
											//alert('Part B missing');
											swal_message='Part B missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == -1 || output == 'Exiting Now....'){
											$('.loader').hide(); 
											//alert('Something Went Wrong.');
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_3 = '';
												$scope.form3_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
										else{
											$('.loader').hide();
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_3 = '';
												$scope.form3_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
									}else{
										$('.loader').hide();
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								},error:function(response) {
						            $('.loader').hide();
						            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
									$scope.Form16sweetalert('Ooops !',swal_message,'info') 
						        }
							});
						}else{
							if(file_count==cur3_file_no){
								$('.loader').hide();
								swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
							}
						}
					},error:function(response) {
			            $('.loader').hide(); 
			            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
			        }
				});
				$scope.form16_3_upload_error= "";
				
			}
			return true;
		}else{
			$scope.form16_3_upload_error= "Please upload correct file";
			return false;
		}
	
	};
<<<<<<< HEAD
	$scope.Form16_save_Employer_Details = function(){
=======

	$scope.Form16_save_Employer_Details = function(form16_no){
>>>>>>> 94e2a906ca3684471de452c2e85a8b96fcdaefaa
		$.ajax({
			url:'/edit_Employer_Details/',
			type:'POST',
			dataType: 'json',
			data:{'option':'add','client_id':client_id,'form16_no':form16_no},
			success: function(response){
				console.log(response);
				if(response.status != undefined){
					if(response.status == 'success'){
						//alert('Awesome! Your Form 16 was read successfully and the data has been updated for relevant fields. This easily saved 10 minutes of your valuable time.')
						swal_message='Awesome! Your Form 16 was read successfully and the data has been updated for relevant fields. This easily saved 10 minutes of your valuable time.';
						$scope.Form16sweetalert('Success',swal_message,'success');
					}else{
						//alert('Oops! there was some problem in reading your Form 16. This typically happens if ')
						swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
					}
				}else{
					swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
					$scope.Form16sweetalert('Ooops !',swal_message,'info')
				}
				
			}
		});
	}


	$scope.Form16sweetalert=function(title,message,status){

		console.log(title)
		swal({
		      title: title,
		      text: message,
		      type: status,
		      showCancelButton: false,
		      closeOnConfirm: false,
		      html: true,
		      showConfirmButton:true,
		      confirmButtonText: "OK",
		}, function(isConfirm) {
		    if(isConfirm){
		    	if(status=='success'){
		    		window.location.reload();
		    	}else{
		    		window.location.reload();
		    		//swal.close();
		    	}
	        }
	
		})

	  // alert(msg)

	}

	$scope.EnableDisable_company1_name= function(){
		if($scope.user.company1_emp_name!=null && $scope.user.company1_emp_name.trim()!=''){
			$("#company1_emp_name").prop("disabled",true);
		}else{
			$("#company1_emp_name").prop("disabled",false);
		}
	}

	$scope.EnableDisable_company2_name = function(){
		if($scope.user.company2_emp_name!=null && $scope.user.company2_emp_name.trim()!=''){
			$("#company2_emp_name").prop("disabled",true);
		}else{
			$("#company2_emp_name").prop("disabled",false);
		}
	}

	$scope.EnableDisable_company3_name = function(){
		if($scope.user.company3_emp_name!=null && $scope.user.company3_emp_name.trim()!=''){
			$("#company3_emp_name").prop("disabled",true);
		}else{
			$("#company3_emp_name").prop("disabled",false);
		}
	}

	$scope.get_income_details = function(){
		// $scope.save_Employer_Details();
		$.ajax({
			url:'/get_form16_data/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'data':'data','client_id':client_id},
			success: function(response){
				console.log(response);

				no_of_company = JSON.stringify(response['no_of_company']).replace(/\"/g, "");
				
				if(no_of_company==0 && no_of_company!=''){
					$("#no_of_companies").hide();
					$scope.user.earn_salary='no'
					$scope.user.no_of_companies=''
				}else{
					$("#no_of_companies").show();
					$scope.user.earn_salary='yes'
					console.log(no_of_company)
					$scope.user.no_of_companies = no_of_company.toString();
					$scope.$apply();
				}

				console.log($scope.user.no_of_companies)
				// $scope.$apply();

			
				if(no_of_company>0){
					$scope.no_of_companies_error= "";
				}
				if(no_of_company == 1){
					$scope.delete_16_1 = { 'display': ''};
					$scope.add_16_1 = { 'display': 'none'};
					$scope.add_16_2 = { 'display': ''};
					$scope.add_16_3 = { 'display': ''};
				}
				if(no_of_company == 2){
					$scope.delete_16_1 = { 'display': ''};
					$scope.delete_16_2 = { 'display': ''};
					$scope.add_16_1 = { 'display': 'none'};
					$scope.add_16_2 = { 'display': 'none'};
					$scope.add_16_3 = { 'display': ''};
				}
				if(no_of_company == 3){
					$scope.delete_16_1 = { 'display': ''};
					$scope.delete_16_2 = { 'display': ''};
					$scope.delete_16_3 = { 'display': ''};
					$scope.add_16_1 = { 'display': 'none'};
					$scope.add_16_2 = { 'display': 'none'};
					$scope.add_16_3 = { 'display': 'none'};
				}
				if(no_of_company==1 || no_of_company==2 || no_of_company==3 ){
					$scope.user.company1_pan= response.PAN[0];
					$scope.user.company1_tan= response.TAN[0];

					if(response.company_name[0]!=null && response.company_name[0].trim()!=''){
						$("#company1_emp_name").prop("disabled",true);
					}else{
						$("#company1_emp_name").prop("disabled",false);
					}

					$scope.user.company1_emp_name= response.company_name[0].trim();
					$scope.user.company1_add= response.address[0];
					$scope.user.company1_pin= response.pin[0];
					$scope.user.company1_city= response.city[0];
					$scope.user.company1_state= response.state[0];
					$scope.user.company1_employer_category=response.employer_category[0]
					// console.log($scope.user.company1_entertainment_allowance)

					if(response.calculated_salary[0] != '')
						$scope.user.company1_calculated_sal= $scope.money_format(response.calculated_salary[0]);
					// if(response.salary[0] != '')
						$scope.user.company1_salary_provision = $scope.money_format(response.salary[0]);
					// if(response.vop[0] != '')
						$scope.user.company1_perquisites= $scope.money_format(response.vop[0]);
					// if(response.profit_lieu[0] != '')
						$scope.user.company1_profits_lieu_salary= $scope.money_format(response.profit_lieu[0]);
					if(response.p_tax[0] != 0)
						$scope.user.company1_profession_tax= $scope.money_format(response.p_tax[0]);
					if(response.entertainment_a[0] != 0)
						$scope.user.company1_entertainment_allowance= $scope.money_format(response.entertainment_a[0]);
					else
						$scope.user.company1_entertainment_allowance=0

					if(response.LTA[0] != 0)
						$scope.user.company1_LTA= $scope.money_format(response.LTA[0]);
					if(response.HRA[0] != 0)
						$scope.user.company1_HRA= $scope.money_format(response.HRA[0]);
					// if(response.other[0] != 0)
						$scope.user.company1_total_allowances= $scope.money_format(response.other[0]);

					// console.log($scope.user.company1_salary_provision)
					// if($scope.user.company1_emp_name != '')
					// 	label_float('ename1_div');
					// if($scope.user.company1_pan != '')
	    //     			label_float('pan1_div');
					// if($scope.user.company1_tan != '')
	    //     			label_float('tan1_div');
					// if($scope.user.company1_add != '')
	    //     			label_float('add1_div');
					// if($scope.user.company1_pin != '')
					// 	label_float('pin1_div');
					// if($scope.user.company1_city != '')
					// 	label_float('city1_div');
					// if($scope.user.company1_salary_provision != '')
	    //     			label_float('sp1_div');
					// if($scope.user.company1_perquisites != '')
	    //     			label_float('pq1_div');
					// if($scope.user.company1_profits_lieu_salary != '')
	    //     			label_float('lieu1_div');
					// if($scope.user.company1_profession_tax != '')
	    //     			label_float('pt1_div');
					// if($scope.user.company1_entertainment_allowance != '')
	    //     			label_float('ea1_div');
					// if($scope.user.company1_LTA != '')
	    //     			label_float('lta1_div');
					// if($scope.user.company1_HRA != '')
	    //     			label_float('hra1_div');
					// if($scope.user.company1_other_allowance != '')
	    //     			label_float('oa1_div');
					// if($scope.user.company1_calculated_sal != '')
					// 	label_float('c_sal1_div');
				}
				if(no_of_company==2 || no_of_company==3 ){
					$scope.user.company2_pan= response.PAN[1];
					$scope.user.company2_tan= response.TAN[1];

					if(response.company_name[1]!=null && response.company_name[1].trim()!=''){
						$("#company2_emp_name").prop("disabled",true);
					}else{
						$("#company2_emp_name").prop("disabled",false);
					}
					$scope.user.company2_emp_name= response.company_name[1].trim();
					$scope.user.company2_add= response.address[1];
					$scope.user.company2_pin= response.pin[1];
					$scope.user.company2_city= response.city[1];
					$scope.user.company2_state= response.state[1];
					$scope.user.company2_employer_category=response.employer_category[1]
					if(response.calculated_salary[1] != '')
						$scope.user.company2_calculated_sal = $scope.money_format(response.calculated_salary[1]);
					if(response.salary[1] != '')
						$scope.user.company2_salary_provision = $scope.money_format(response.salary[1]);
					// if(response.vop[1] != ''){
					// 	log.info('if')
						$scope.user.company2_perquisites = $scope.money_format(response.vop[1]);
					// }
					// console.log(response.vop[1])
					// console.log($scope.money_format(response.vop[1]))
					// $scope.$apply();
					// console.log($scope.user.company2_perquisites)
					// if(response.profit_lieu[1] != '')
						$scope.user.company2_profits_lieu_salary = $scope.money_format(response.profit_lieu[1]);
					// if(response.p_tax[1] != 0)
						$scope.user.company2_profession_tax = $scope.money_format(response.p_tax[1]);
					
					if(response.entertainment_a[1] != 0)
						$scope.user.company2_entertainment_allowance = $scope.money_format(response.entertainment_a[1]);
					else
						$scope.user.company2_entertainment_allowance=0

					if(response.LTA[1] != 0)
						$scope.user.company2_LTA = $scope.money_format(response.LTA[1]);
					if(response.HRA[1] != 0)
						$scope.user.company2_HRA = $scope.money_format(response.HRA[1]);
					// if(response.other[1] != 0)
						$scope.user.company2_total_allowances = $scope.money_format(response.other[1]);
					
					// if($scope.user.company2_emp_name != '')
					// 	label_float('ename2_div');
					// if($scope.user.company2_pan != '')
					// 	label_float('pan2_div');
					// if($scope.user.company2_tan != '')
	    //     			label_float('tan2_div');
					// if($scope.user.company2_add != '')
					// 	label_float('add2_div');
					// if($scope.user.company2_pin != '')
					// 	label_float('pin2_div');
					// if($scope.user.company2_city != '')
					// 	label_float('city2_div');
					// if($scope.user.company2_salary_provision != '')
					// 	label_float('sp2_div');
					// if($scope.user.company2_perquisites != '')
					// 	label_float('pq2_div');
					// if($scope.user.company2_profits_lieu_salary != '')
					// 	label_float('lieu2_div');
					// if($scope.user.company2_profession_tax != '')
					// 	label_float('pt2_div');
					// // if($scope.user.company2_entertainment_allowance != '')
					// // 	label_float('ea2_div');
					// if($scope.user.company2_LTA != '')
					// 	label_float('lta2_div');
					// if($scope.user.company2_HRA != '')
					// 	label_float('hra2_div');
					// if($scope.user.company2_other_allowance != '')
	    //     			label_float('oa2_div');
					// if($scope.user.company2_calculated_sal != '')
					// 	label_float('c_sal2_div');

					$scope.$apply();
				}
				if( no_of_company==3 ){
					$scope.user.company3_pan= response.PAN[2];
					$scope.user.company3_tan= response.TAN[2];

					if(response.company_name[2]!=null && response.company_name[2].trim()!=''){
						$("#company3_emp_name").prop("disabled",true);
					}else{
						$("#company3_emp_name").prop("disabled",false);
					}
					$scope.user.company3_emp_name= response.company_name[2].trim();
					$scope.user.company3_add= response.address[2];
					$scope.user.company3_pin= response.pin[2];
					$scope.user.company3_city= response.city[2];
					$scope.user.company3_state= response.state[2];
					$scope.user.company3_employer_category=response.employment_category[2]
					if(response.calculated_salary[2] != '')
						$scope.user.company3_calculated_sal= $scope.money_format(response.calculated_salary[2]);
					// $scope.user.company3_salary_provision= response.salary[2];
					if(response.salary[2] != '')
						$scope.user.company3_salary_provision= $scope.money_format(response.salary[2]);
					// if(response.vop[2] != '')
						$scope.user.company3_perquisites= $scope.money_format(response.vop[2]);
					// if(response.profit_lieu[2] != '')
						$scope.user.company3_profits_lieu_salary= $scope.money_format(response.profit_lieu[2]);
					if(response.p_tax[2] != 0)
						$scope.user.company3_profession_tax= $scope.money_format(response.p_tax[2]);
					if(response.entertainment_a[2] != 0)
						$scope.user.company3_entertainment_allowance= $scope.money_format(response.entertainment_a[2]);
					else
						$scope.user.company3_entertainment_allowance=0

					if(response.LTA[2] != 0)
						$scope.user.company3_LTA= $scope.money_format(response.LTA[2]);
					if(response.HRA[2] != 0)
						$scope.user.company3_HRA= $scope.money_format(response.HRA[2]);
					// if(response.other[2] != 0)
						$scope.user.company3_total_allowances= $scope.money_format(response.other[2]);

					// if($scope.user.company3_emp_name != '')
					// 	label_float('ename3_div');
					// if($scope.user.company3_pan != '')
					// 	label_float('pan3_div');
					// if($scope.user.company3_tan != '')
	    //     			label_float('tan3_div');
					// if($scope.user.company3_add != '')
					// 	label_float('add3_div');
					// if($scope.user.company3_pin != '')
					// 	label_float('pin3_div');
					// if($scope.user.company3_city != '')
					// 	label_float('city3_div');
					// if($scope.user.company3_salary_provision != '')
	    //     			label_float('sp3_div');
					// if($scope.user.company3_perquisites != '')
	    //     			label_float('pq3_div');
					// if($scope.user.company3_profits_lieu_salary != '')
	    //     			label_float('lieu3_div');
					// if($scope.user.company3_profession_tax != '')
	    //     			label_float('pt3_div');
					// // if($scope.user.company3_entertainment_allowance != '')
	    // //     			label_float('ea3_div');
					// if($scope.user.company3_LTA != '')
	    //     			label_float('lta3_div');
					// if($scope.user.company3_HRA != '')
	    //     			label_float('hra3_div');
					// if($scope.user.company3_other_allowance != '')
	    //     			label_float('oa3_div');
					// if($scope.user.company3_calculated_sal != '')
					// 	label_float('c_sal3_div')
				}


				$scope.final_entertainment_allowance($scope.user.company1_employer_category,$scope.user.company2_employer_category,$scope.user.company3_employer_category)
				noc_a_js(no_of_company);
				$scope.$apply();
				
			}
		});
		$.ajax({
			url:'/get_house_property/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
	        	no_of_hp = JSON.stringify(response.no_of_hp).replace(/\"/g, "");

				$scope.user.business_profession_income = 'no';
				if(no_of_hp>0){
					$scope.user.house_property = 'yes';
					showHousePropertyElement(true);
					$scope.user.no_of_property = no_of_hp;
					nohouseproperty_js(no_of_hp);
				}
				else
					$scope.user.house_property = 'no';

				if(no_of_hp ==1 || no_of_hp ==2 || no_of_hp ==3 ){
					$scope.user.house_property_type1= response.property_type[0];
					$scope.user.property1_add= response.Premises[0];
					$scope.user.property1_pin= response.PIN[0];
					$scope.user.property1_city= response.Town_City[0];
					$scope.user.property1_state = response.State[0];
					$scope.user.share_in_property1= response.per_s_in_p[0];
					$scope.user.co_owned1 = response.Is_coOwned[0];

					if($scope.user.co_owned1 == 'no'){
						$scope.$apply();
						isThisCoOwned('1');
					}
					// $scope.user.property1_co_owner_name= response.coOwned_name[0];
					// $scope.user.property1_co_owner_pan= response.coOwned_pan[0];
					// $scope.user.property1_percent_share= $scope.money_format(response.coOwned_per_s[0]);
					$scope.user.property1_municipal_tax= $scope.money_format(response.Property_Tax[0]);
					$scope.user.property1_interest_payable= $scope.money_format(response.Interest[0]);
					$scope.user.property1_rent_received= $scope.money_format(response.rent[0]);
					// if($scope.user.property1_add != '')
					// 	label_float('p_add1_div');
					// if($scope.user.property1_pin != '')
					// 	label_float('p_pin1_div');
					// if($scope.user.property1_city != '')
					// 	label_float('p_city1_div');
					// if($scope.user.share_in_property1 != '')
					// 	label_float('share1_div');
					// if($scope.user.property1_co_owner_name != '')
					// 	label_float('co_name1_div');
					// if($scope.user.property1_co_owner_pan != '')
					// 	label_float('co_pan1_div');
					// if($scope.user.property1_percent_share != '')
					// 	label_float('per_share1_div');
					// if($scope.user.property1_municipal_tax != '')
					// 	label_float('mtax1_div');
					// if($scope.user.property1_interest_payable != '')
					// 	label_float('ipay1_div');
					// if($scope.user.property1_rent_received != '')
					// 	label_float('rent1_div');
				}
				if( no_of_hp ==2 || no_of_hp ==3 ){
					$scope.user.house_property_type2= response.property_type[1];
					$scope.user.property2_add= response.Premises[1];
					$scope.user.property2_pin= response.PIN[1];
					$scope.user.property2_city= response.Town_City[1];
					$scope.user.property2_state= response.State[1];
					$scope.user.share_in_property2= response.per_s_in_p[1];
					$scope.user.co_owned2 = response.Is_coOwned[1];
					if($scope.user.co_owned2 == 'no'){
						$scope.$apply();
						isThisCoOwned('2');
					}
					// $scope.user.property2_co_owner_name= response.coOwned_name[1];
					// $scope.user.property2_co_owner_pan= response.coOwned_pan[1];
					// $scope.user.property2_percent_share= $scope.money_format(response.coOwned_per_s[1]);
					$scope.user.property2_municipal_tax= $scope.money_format(response.Property_Tax[1]);
					$scope.user.property2_interest_payable= $scope.money_format(response.Interest[1]);
					$scope.user.property2_rent_received= $scope.money_format(response.rent[1]);

					if($scope.user.property2_add != '')
						label_float('p_add2_div');
					if($scope.user.property2_pin != '')
						label_float('p_pin2_div');
					if($scope.user.property2_city != '')
						label_float('p_city2_div');
					if($scope.user.share_in_property2 != '')
						label_float('share2_div');
					// if($scope.user.property2_co_owner_name != '')
					// 	label_float('co_name2_div');
					// if($scope.user.property2_co_owner_pan != '')
					// 	label_float('co_pan2_div');
					// if($scope.user.property2_percent_share != '')
					// 	label_float('per_share2_div');
					if($scope.user.property2_municipal_tax != '')
						label_float('mtax2_div');
					if($scope.user.property2_interest_payable != '')
						label_float('ipay2_div');
					if($scope.user.property2_rent_received != '')
						label_float('rent2_div');
				}
				if( no_of_hp ==3 ){
					$scope.user.house_property_type3= response.property_type[2];
					$scope.user.property3_add= response.Premises[2];
					$scope.user.property3_pin= response.PIN[2];
					$scope.user.property3_city= response.Town_City[2];
					$scope.user.property3_state= response.State[2];
					$scope.user.share_in_property3= response.per_s_in_p[2];
					$scope.user.co_owned3 = response.Is_coOwned[1];
					if($scope.user.co_owned3 == 'no'){
						$scope.$apply();
						isThisCoOwned('3');
					}
					// $scope.user.property3_co_owner_name= response.coOwned_name[2];
					// $scope.user.property3_co_owner_pan= response.coOwned_pan[2];
					// $scope.user.property3_percent_share= $scope.money_format(response.coOwned_per_s[2]);
					$scope.user.property3_municipal_tax= $scope.money_format(response.Property_Tax[2]);
					$scope.user.property3_interest_payable= $scope.money_format(response.Interest[2]);
					$scope.user.property3_rent_received= $scope.money_format(response.rent[2]);

					if($scope.user.property3_add != '')
						label_float('p_add3_div');
					if($scope.user.property3_pin != '')
						label_float('p_pin3_div');
					if($scope.user.property3_city != '')
						label_float('p_city3_div');
					if($scope.user.share_in_property3 != '')
						label_float('share1_div');
					// if($scope.user.property3_co_owner_name != '')
					// 	label_float('co_name3_div');
					// if($scope.user.property3_co_owner_pan != '')
					// 	label_float('co_pan3_div');
					// if($scope.user.property3_percent_share != '')
					// 	label_float('per_share3_div');
					if($scope.user.property3_municipal_tax != '')
						label_float('mtax3_div');
					if($scope.user.property3_interest_payable != '')
						label_float('ipay3_div');
					if($scope.user.property3_rent_received != '')
						label_float('rent3_div');
				}
				$scope.$apply();
			}
		});

		$.ajax({
			url:'/get_other_sources/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				if(response.Interest_Savings != 0)
					$scope.user.saving_bank_int = $scope.money_format(response.Interest_Savings);
				if(response.FD != 0)
					$scope.user.fd_interest = $scope.money_format(response.FD);
				if(response.other_interest != 0)
					$scope.user.other_interest = $scope.money_format(response.other_interest);
				if(response.Commission != 0)
					$scope.user.commission = $scope.money_format(response.Commission);
				if(response.Other_Income != 0)
					$scope.user.other_income = $scope.money_format(response.Other_Income);
				if(response.family_pension != 0)
					$scope.user.family_pension = $scope.money_format(response.family_pension);
				if(response.post_office_interest != 0)
					$scope.user.post_office_interest = $scope.money_format(response.post_office_interest);
				$scope.$apply();
				if($scope.user.saving_bank_int != '')
					label_float('s_bank_int_div');
				if($scope.user.fd_interest != '')
					label_float('fd_div');
				if($scope.user.other_interest != '')
					label_float('other_interest_div');
				if($scope.user.commission != '')
					label_float('commission_div');
				if($scope.user.other_income != '')
					label_float('other_income_div');
				if($scope.user.family_pension != '')
					label_float('family_pension_div');
			}
		});
		$.ajax({
			url:'/get_capital_gain/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				// $scope.user.other_interest = response.other_interest;
				if(response.shares == 1){
					$scope.user.shares = true;
					hide_show_CG('shares');
					$scope.user.st_shares_tpv = $scope.money_format(response.st_shares_tpv);
					$scope.user.st_shares_tsv = $scope.money_format(response.st_shares_tsv);
					$scope.user.st_shares_gain = $scope.money_format(response.st_shares_gain);
					$scope.user.lt_shares_tpv = $scope.money_format(response.lt_shares_tpv);
					$scope.user.lt_shares_tsv = $scope.money_format(response.lt_shares_tsv);
					$scope.user.lt_shares_gain = $scope.money_format(response.lt_shares_gain);
				}
				if(response.equity == 1){
					$scope.user.equity = true;
					hide_show_CG('equity');
					$scope.user.st_equity_tpv = $scope.money_format(response.st_equity_tpv);
					$scope.user.st_equity_tsv = $scope.money_format(response.st_equity_tsv);
					$scope.user.st_equity_gain = $scope.money_format(response.st_equity_gain);
					$scope.user.lt_equity_tpv = $scope.money_format(response.lt_equity_tpv);
					$scope.user.lt_equity_tsv = $scope.money_format(response.lt_equity_tsv);
					$scope.user.lt_equity_gain = $scope.money_format(response.lt_equity_gain);
				}
				if(response.equity == 1){
					$scope.user.equity = true;
					hide_show_CG('equity');
					$scope.user.st_equity_tpv = $scope.money_format(response.st_equity_tpv);
					$scope.user.st_equity_tsv = $scope.money_format(response.st_equity_tsv);
					$scope.user.st_equity_gain = $scope.money_format(response.st_equity_gain);
					$scope.user.lt_equity_tpv = $scope.money_format(response.lt_equity_tpv);
					$scope.user.lt_equity_tsv = $scope.money_format(response.lt_equity_tsv);
					$scope.user.lt_equity_gain = $scope.money_format(response.lt_equity_gain);
				}
				if(response.debt_MF == 1){
					$scope.user.debt_MF = true;
					hide_show_CG('debt_MF');
					$scope.user.st_debt_MF_tpv = $scope.money_format(response.st_debt_MF_tpv);
					$scope.user.st_debt_MF_tsv = $scope.money_format(response.st_debt_MF_tsv);
					$scope.user.st_debt_MF_gain = $scope.money_format(response.st_debt_MF_gain);
					$scope.user.lt_debt_MF_tpv = $scope.money_format(response.lt_debt_MF_tpv);
					$scope.user.lt_debt_MF_tsv = $scope.money_format(response.lt_debt_MF_tsv);
					$scope.user.lt_debt_MF_gain = $scope.money_format(response.lt_debt_MF_gain);
				}
				if(response.listed_d == 1){
					$scope.user.listed_d = true;
					hide_show_CG('listed_d');
					$scope.user.st_listed_d_tpv = $scope.money_format(response.st_listed_d_tpv);
					$scope.user.st_listed_d_tsv = $scope.money_format(response.st_listed_d_tsv);
					$scope.user.st_listed_d_gain = $scope.money_format(response.st_listed_d_gain);
					$scope.user.lt_listed_d_tpv = $scope.money_format(response.lt_listed_d_tpv);
					$scope.user.lt_listed_d_tsv = $scope.money_format(response.lt_listed_d_tsv);
					$scope.user.lt_listed_d_gain = $scope.money_format(response.lt_listed_d_gain);
				}
				$scope.$apply();
				/*if($scope.user.other_interest != '')
					label_float('other_interest_div');*/
			}
		});
	}

	$scope.save_Employer_Details = function(){
		$.ajax({
			url:'/edit_Employer_Details/',
			type:'POST',
			dataType: 'json',
			data:{'option':'add','client_id':client_id},
			success: function(response){
				console.log(response);
				/*if(response.status == 'success'){
					if(response.form16_exist != undefined){
						if(response.form16_exist == 1){
							alert('Awesome! Your Form 16 was read successfully and the data has been updated for relevant fields. This easily saved 10 minutes of your valuable time.')
						}
					}
				}*/
				// if(response.status != 'success')
					// $scope.user_tracking('error in data',response.status);
			}
		});
	}

	$scope.delete_Employer_Details = function(company_name){
		$.ajax({
			url:'/edit_Employer_Details/',
			type:'POST',
			dataType: 'json',
			data:{'option':'delete','client_id':client_id,'company_name':company_name},
			success: function(response){
				console.log(response);
				window.location.reload();
			}
		});
	}

	$scope.delete_house_property_detail = function(house_property){
		$.ajax({
			url:'/delete_house_property/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id,'option':house_property},
			success: function(response){
				// console.log(response);
				// if(response.status != 'success')
					// $scope.user_tracking('error in data','Deleting HP : '+house_property+response.status);
			}
		});
	}

	$scope.calculated_salary = function(company_no){
		if (company_no == '1') {
			// alert(company_no);
			var sal = $scope.number_format($scope.user.company1_salary_provision);
			if(sal == '')
				sal = 0;
			else
				sal = parseInt(sal);
			var perq = $scope.number_format($scope.user.company1_perquisites);
			if(perq == '')
				perq = 0;
			else
				perq = parseInt(perq);
			var pls = $scope.number_format($scope.user.company1_profits_lieu_salary);
			if(pls == '')
				pls = 0;
			else
				pls = parseInt(pls);
			var lta = $scope.number_format($scope.user.company1_LTA);
			if(lta == '')
				lta = 0;
			else
				lta = parseInt(lta);
			var hra = $scope.number_format($scope.user.company1_HRA);
			if(hra == '')
				hra = 0;
			else
				hra = parseInt(hra);
			var otherA = $scope.number_format($scope.user.company1_total_allowances);
			if(otherA == '')
				otherA = 0;
			else
				otherA = parseInt(otherA);
			$scope.user.company1_calculated_sal= $scope.money_format(sal+perq+pls -otherA);
		}
		if (company_no == '2') {
			var sal = $scope.number_format($scope.user.company2_salary_provision);
			if(sal == '')
				sal = 0;
			else
				sal = parseInt(sal);
			var perq = $scope.number_format($scope.user.company2_perquisites);
			if(perq == '')
				perq = 0;
			else
				perq = parseInt(perq);
			var pls = $scope.number_format($scope.user.company2_profits_lieu_salary);
			if(pls == '')
				pls = 0;
			else
				pls = parseInt(pls);
			var lta = $scope.number_format($scope.user.company2_LTA);
			if(lta == '')
				lta = 0;
			else
				lta = parseInt(lta);
			var hra = $scope.number_format($scope.user.company2_HRA);
			if(hra == '')
				hra = 0;
			else
				hra = parseInt(hra);
			var otherA = $scope.number_format($scope.user.company2_total_allowances);
			if(otherA == '')
				otherA = 0;
			else
				otherA = parseInt(otherA);
			$scope.user.company2_calculated_sal= $scope.money_format(sal+perq+pls-otherA);
		}
		if (company_no == '3') {
			var sal = $scope.number_format($scope.user.company2_salary_provision);
			if(sal == '')
				sal = 0;
			else
				sal = parseInt(sal);
			var perq = $scope.number_format($scope.user.company2_perquisites);
			if(perq == '')
				perq = 0;
			else
				perq = parseInt(perq);
			var pls = $scope.number_format($scope.user.company2_profits_lieu_salary);
			if(pls == '')
				pls = 0;
			else
				pls = parseInt(pls);
			var lta = $scope.number_format($scope.user.company2_LTA);
			if(lta == '')
				lta = 0;
			else
				lta = parseInt(lta);
			var hra = $scope.number_format($scope.user.company2_HRA);
			if(hra == '')
				hra = 0;
			else
				hra = parseInt(hra);
			var otherA = $scope.number_format($scope.user.company3_total_allowances);
			if(otherA == '')
				otherA = 0;
			else
				otherA = parseInt(otherA);
			$scope.user.company1_calculated_sal= $scope.money_format(sal+perq+pls -otherA);
		}
	}

	$scope.calculate_gain_loss = function(variable){
		if(variable == 'ST_shares'){
			var p_value = $scope.number_format($scope.user.st_shares_tpv);
			var s_value = $scope.number_format($scope.user.st_shares_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined){
				$scope.user.st_shares_gain = parseInt(s_value) - parseInt(p_value);
			}
		}
		if(variable == 'LT_shares'){
			var p_value = $scope.number_format($scope.user.lt_shares_tpv);
			var s_value = $scope.number_format($scope.user.lt_shares_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined){
				$scope.user.lt_shares_gain = parseInt(s_value) - parseInt(p_value);
			}
		}
		if(variable == 'ST_equity'){
			var p_value = $scope.number_format($scope.user.st_equity_tpv);
			var s_value = $scope.number_format($scope.user.st_equity_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined){
				$scope.user.st_equity_gain = parseInt(s_value) - parseInt(p_value);
			}
		}
		if(variable == 'LT_equity'){
			var p_value = $scope.number_format($scope.user.lt_equity_tpv);
			var s_value = $scope.number_format($scope.user.lt_equity_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined)
				$scope.user.lt_equity_gain = parseInt(s_value) - parseInt(p_value);
		}
		if(variable == 'ST_debt_MF'){
			var p_value = $scope.number_format($scope.user.st_debt_MF_tpv);
			var s_value = $scope.number_format($scope.user.st_debt_MF_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined)
				$scope.user.st_debt_MF_gain = parseInt(s_value) - parseInt(p_value);
		}
		if(variable == 'LT_debt_MF'){
			var p_value = $scope.number_format($scope.user.lt_debt_MF_tpv);
			var s_value = $scope.number_format($scope.user.lt_debt_MF_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined)
				$scope.user.lt_debt_MF_gain = parseInt(s_value) - parseInt(p_value);
		}
		if(variable == 'ST_listed_d'){
			var p_value = $scope.number_format($scope.user.st_listed_d_tpv);
			var s_value = $scope.number_format($scope.user.st_listed_d_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined)
				$scope.user.st_listed_d_gain = parseInt(s_value) - parseInt(p_value);
		}
		if(variable == 'LT_listed_d'){
			var p_value = $scope.number_format($scope.user.lt_listed_d_tpv);
			var s_value = $scope.number_format($scope.user.lt_listed_d_tsv);
			if(p_value != '' && s_value != '' && p_value != undefined && s_value != undefined)
				$scope.user.lt_listed_d_gain = parseInt(s_value) - parseInt(p_value);
		}
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return 0

		if(!isNaN(x)){
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.number_format = function(val){

		if(val!=undefined){
			val = val.toString();
			val = val.replace(/[,]/g, "");
		
			return val;
		}else{
			return 0
		}
		
	}

	$scope.save_business_profession_income = function(){
		// alert($scope.user.business_profession_income);
		// save_business_profession_income

		var pass_obj = { "business_profession_income":$scope.user.business_profession_income };
		pass_obj["client_id"] = client_id;
		var data_pass = JSON.stringify(pass_obj);
		$.ajax({
			url:'/save_business_profession_income/',
			type:'POST',
			dataType: 'json',
			data:{'option':'add','client_id':client_id,'data':data_pass},
			success: function(response){
				console.log(response);
				// if(response.status != 'success')
					// $scope.user_tracking('error in data',response.status);
			}
		});

	}


	$scope.hide_all_companies_data=function(value){

		console.log(value)
		if(value=='no'){
			$("#no_of_companies").hide();
			document.getElementById("employer1").style.display = "none";
            document.getElementById("employer2").style.display = "none";
            document.getElementById("employer3").style.display = "none";
		}else{
			$("#no_of_companies").show();
			noc($scope.user.no_of_companies)
		}

	}

	salary_breakup_variable=['basic_salary','conveyance_allowance','HRA','LTA','leave_encashment','others']
	salary_breakup_label=['Basic Salary','Conveyance Allowance','HRA','LTA','Leave Encashment','Others']
	salary_breakup_tooltip=[
		'Basic Salary',
		'Conveyance Allowance',
		'If you have not claimed HRA in Form 16, the same can be claimed here. Just keep rent receipts with you safely. Deduction - Least of the following <br/>1. Actual HRA received. <br/>2. 50%/40% of (basic salary + Dearness allowance). <br/>3. Actual rent paid less 10% of basic salary + dearness allowance.',
		'If you have not claimed in Form 16, the same can be claimed here. Just keep travel tickets safely as proof. The exemption can be claimed twice in the block of four years. Exemption is available only on the actual travel costs i.e., air, rail or bus fare incurred by the employee. No expenses such as local conveyance, sightseeing, hotel accommodation, food etc are eligible for exemption.',
		'Leave Encashment',
		' Others'
	]


	perquisites_breakup_variable=['accommodation','cars_or_other_automotive','utilitiy_bills','concessional_travel','free_meals','free_education','other_benefits']
	perquisites_breakup_label=['Accommodation','Cars / Other Automotive','Gas, electricity, water','Free or concessional travel','Free meals','Free education','Other benefits or amenities']
	perquisites_breakup_tooltip=[
		'Accommodation','Cars / Other Automotive',
		'Gas, electricity, water',
		'Free or concessional travel',
		'Free meals',
		'Free education',
		'Other benefits or amenities'
	]


	profits_in_lieu_salary_breakup_variable=['termination_compensation','keyman_insurance','other_profit_in_lieu_salary','other']
	profits_in_lieu_salary_breakup_label=['Compensation received by an assessee from his employer in connection with the termination of his employment or modification','Any payment received by an assessee from his employer or from a provident or other fund, sum received under Keyman Insurance Policy including Bonus thereto','Any amount received by assessee from any person before joining or after cessation of employment with that person','Any Other']
	profits_in_lieu_salary_breakup_tooltip=[
		'Compensation received by an assessee from his employer in connection with the termination of his employment or modification',
		'Any payment received by an assessee from his employer or from a provident or other fund, sum received under Keyman Insurance Policy including Bonus thereto',
		'Any amount received by assessee from any person before joining or after cessation of employment with that person',
		'Any Other'
		
	]


	allowances_breakup_variable=['LTA_allowance','earned_leave_encashment','HRA_allowance','other_allowance']
	allowances_breakup_label=['Leave Travel allowance','Earned leave encashment',' House Rent Allowance','Any Other']
	allowances_breakup_tooltip=[
		'Compensation received by an assessee from his employer in connection with the termination of his employment or modification',
		'Any payment received by an assessee from his employer or from a provident or other fund, sum received under Keyman Insurance Policy including Bonus thereto',
		'Any amount received by assessee from any person before joining or after cessation of employment with that person',
		'Any Other'
		
	]

	co_owner_variable=['co_owner_no','Name_of_Co_Owner','PAN_of_Co_owner','Percentage_Share_in_Property']
	co_owner_label=['','Name of Co–owner','PAN of Co–owner',' Percentage share in Property']
	co_owner_tooltip=[
		'',
		'',
		'',
		''
		
	]


	tenant_variable=['tenant_no','tenant_name','tenant_pan']
	tenant_label=['','Name of Tenant','PAN of Tenant']
	tenant_tooltip=[
		'',
		'',
		'',
		''
		
	]

	breakup_keyword_variable=['salary_breakup','perquisites_breakup','profits_in_lieu_salary_breakup','allowances_breakup']


	$scope.make_variable_json=function(element,variable_array_name,fixed_value){

		console.log(element)
		console.log(variable_array_name)
		console.log(fixed_value)

		variable={}
		
		iterate_length=eval(variable_array_name+'_variable').length
		for(var value=0; value<iterate_length ;value++){


	        var dynamic_array_variable=variable_array_name+'_variable'
	        var dynamic_array_label=variable_array_name+'_label'
	        var dynamic_array_tooltip=variable_array_name+'_tooltip'

	        var dynamic_array_value=[]
	        dynamic_array_value=eval(dynamic_array_variable)

	        var dynamic_array_label_value=[]
	        dynamic_array_label_value=eval(dynamic_array_label)

	        var ddynamic_array_tooltip_value=[]
	        dynamic_array_tooltip_value=eval(dynamic_array_tooltip)

	      	this[dynamic_array_value[value]]={}

	      	// console.log('user.'+comapany+'_'+dynamic_array_value[value])
	        this[dynamic_array_value[value]].name='user.'+element+'_'+dynamic_array_value[value]
	        this[dynamic_array_value[value]].id=element+'_'+dynamic_array_value[value]
	        this[dynamic_array_value[value]].error=element+'_'+dynamic_array_value[value]+'_error'
	        this[dynamic_array_value[value]].tooltip=dynamic_array_tooltip_value[value]
	        this[dynamic_array_value[value]].label=dynamic_array_label_value[value]

	        if(breakup_keyword_variable.includes(variable_array_name)){

	        	var parameter={}
		        parameter['element']=element
		        parameter['keyword']=variable_array_name
		        array_of_variable=[]
		        for(var i=0; i<iterate_length-1 ;i++){

		        	array_of_variable.push(element+'_'+dynamic_array_value[i])
		        }

		        parameter['variables_for_sum']=array_of_variable
		     
		        parameter['other_variable']='user.'+element+'_'+dynamic_array_value[iterate_length-1]
		        
		        this[dynamic_array_value[value]].param=parameter
		      
	        }
	        
	        this[dynamic_array_value[value]].value=''

	     
	        var prefilled_dynamic_variable_value=$scope.fetch_breakup_variable(element+'_'+dynamic_array_value[value])
	        // console.log(prefilled_dynamic_variable_value)
	        if(prefilled_dynamic_variable_value!=undefined)
	          this[dynamic_array_value[value]].value=prefilled_dynamic_variable_value
	        else
	          this[dynamic_array_value[value]].value=fixed_value



	        variable[dynamic_array_value[value]]=this[dynamic_array_value[value]]


      
    	}

    	console.log(variable)
    	return variable

	}

	$scope.disable_other_breakup_variable=function(company){

	
			$('#'+company+'_other_allowance').prop('disabled',true)
			$('#'+company+'_other').prop('disabled',true)
			$('#'+company+'_other_benefits').prop('disabled',true)
			// console.log('#'+company+'_others')
			$('#'+company+'_others').prop('disabled',true)
	
		
	}

	$scope.dynamicCaller=function(function_name,parameter,function_name1,parameter1){
   		$scope[function_name](parameter);//others calculation
   		$scope[function_name1](parameter1)
	}

	$scope.apply_money_format_on_all_breakup_variable=function(variable){
		// console.log(variable)
		var value=$scope.fetch_breakup_variable(variable)
		// console.log(value)
		var value_with_money_format=$scope.money_format($scope.convert_to_int(value))
		// console.log(value_with_money_format)
		$scope.initialise('user.'+variable,value_with_money_format)
	}

	$scope.breakups_variable_others_calculation=function(object){
		console.log(JSON.stringify(object))
		// {"element":"company1",
		// "keyword":"salary_breakup",
		// "variables_for_sum":[
		// 						"user.company1_basic_salary",
		// 						"user.company1_conveyance_allowance",
		// 						"user.company1_HRA","user.company1_LTA",
		// 						"user.company1_leave_encashment"
		// 					],
		// "other_variable":"user.company1_others"
		// }

		variables_for_sum=object.variables_for_sum
		sum_of_non_others=0
		for(iterate=0;iterate<variables_for_sum.length;iterate++){

			var value=$scope.fetch_breakup_variable(variables_for_sum[iterate])
			sum_of_non_others=sum_of_non_others+$scope.convert_to_int(value)

		}

		// console.log(sum_of_non_others)
		var total_value=parseInt($scope.number_format($scope.fetch_breakup_variable(object.element+'_'+$scope.get_salary_variable(object.keyword))))
		// console.log(total_value)

		if(total_value==0){
			$scope.initialise(object.other_variable,0)
		}else{

			var division=$scope.convert_to_int(total_value)-sum_of_non_others
			console.log(division)
			console.log(Math.max(0, division))
			$scope.initialise(object.other_variable,$scope.money_format(Math.max(0, division)))
		}

		
		// alert(object.keyword);
	}


	$scope.dynamic_break_up_modal_html=function(company,keyword){

    	// console.log(variable)

    	keyword.split('_')
    	$scope.breakup_modal_heading=$scope.toTitleCase(company+' '+keyword.split('_').join(' '))

    	$scope.breakup_modal_keyword=keyword
    	$scope.breakup_modal_company=company

    	// $scope.breakup_modal_td=$scope.make_variable_json(company,keyword,0)
    	$scope.initialise(company+'_breakup_modal_td',$scope.make_variable_json(company,keyword,0))
    	// $scope.$apply();

    	$scope.breakup_error=''
		$("#modalBreakup").modal('show');

		$timeout(function() {
			$scope.disable_other_breakup_variable(company);

		}, 200);



		// $scope.$apply();

		

		// alert();


	}


	$scope.toTitleCase=function(str) {
	        return str.replace(
	            /\w\S*/g,
	            function(txt) {
	                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	            }
	        );
	}

	$scope.final_breakup_validation=function(company){

		salary_breakup_status=$scope.match_breakup_values_validation('salary_breakup',company)
		perquisites_breakup_status=$scope.match_breakup_values_validation('perquisites_breakup',company)
		profits_in_lieu_salary_breakup_status=$scope.match_breakup_values_validation('profits_in_lieu_salary_breakup',company)
		allowances_breakup_status=$scope.match_breakup_values_validation('allowances_breakup',company)

		// console.log(salary_breakup_status)
		if(salary_breakup_status==true && perquisites_breakup_status==true && profits_in_lieu_salary_breakup_status==true && allowances_breakup_status==true){
			return true
		}else{
			return false
		}
	}

	$scope.breakup_api=function(company,keyword,user){

		console.log('breakup_api')
		console.log(company)
		console.log(keyword)

		var data=$scope.prepare_breakup_input(company,keyword,'int')
		console.log(data)
		var salary_breakup_status=$scope.match_breakup_values_validation(keyword,company)
		console.log(salary_breakup_status)
		if(salary_breakup_status==true){

			$scope.breakup_error=''
			$.ajax({
						url:'/save_breakups/',
						type:'POST',
						dataType: 'json',
						data:{'data':data,'keyword':keyword,'client_id':client_id,'company_name':$scope.fetch_breakup_variable(company+'_emp_name'),'company':company},
						success: function(response){
							// alert(JSON.stringify(response))
							
							console.log(response.status);
							if(response.status==0){
								$scope.breakup_error=''
								$('#modalBreakup').modal('hide');
							}else{
								$scope.breakup_error=response.status
							}
							$scope.$apply();
						},error:function(response){
							$scope.breakup_error=response.status
							console.log(response.status);
						}
		    });

		}else{
			$scope.breakup_error='Addition Not Match'
		}

		

	}

	$scope.make_input_object=function(company,variable_array_name){

		iterate_length=eval(variable_array_name+'_variable').length

		var dynamic_array_variable=variable_array_name+'_variable'
	    var dynamic_array_value=[]
	    dynamic_array_value=eval(dynamic_array_variable)

		array_of_variable=[]
		for(var i=0; i<iterate_length-1 ;i++){

		    array_of_variable.push(company+'_'+dynamic_array_value[i])
		}

		 
		other_variable='user.'+company+'_'+dynamic_array_value[iterate_length-1]

		var data= {
					"element":company,
					"keyword":variable_array_name,
					"variables_for_sum":array_of_variable,
					"other_variable":other_variable
				}

		return data
	}

	$scope.get_breakup_details=function(){


		$.ajax({
				url:'/get_breakup_details/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id},
				success: function(response){
					// console.log(JSON.stringify(response))
					console.log(response.data);
					var data=response.data

					var elements=data.length

					salary_breakup_param=[]
					perquisites_breakup_param=[]
					profit_in_liue_breakup_param=[]
					allowance_breakup_param=[]

					for(iterate=0;iterate<elements;iterate++){
						no=iterate
						no=no+1

						var salary_breakup=data[iterate].SalaryBreakup_Details
						// console.log(salary_breakup)
						$scope.fetching_all_breakups('company'+no,salary_breakup,0)

						console.log(data[iterate])
						var perquisite_breakup=data[iterate].PerquisiteBreakUp_Details
						// console.log(perquisite_breakup)
						$scope.fetching_all_breakups('company'+no,perquisite_breakup,0)

					
						var profitinlieu_breakup=data[iterate].ProfitinLieuBreakUp_Details
						// console.log(profitinlieu_breakup)
						$scope.fetching_all_breakups('company'+no,profitinlieu_breakup,0)
						

						var allowances_breakup=data[iterate].allowances_Details

						$scope.fetching_allowances_breakup('company'+no,allowances_breakup,0)

						salary_breakup_param[iterate]=$scope.make_input_object('company'+no,'salary_breakup')

						perquisites_breakup_param[iterate]=$scope.make_input_object('company'+no,'perquisites_breakup')

						profit_in_liue_breakup_param[iterate]=$scope.make_input_object('company'+no,'profits_in_lieu_salary_breakup')

						allowance_breakup_param[iterate]=$scope.make_input_object('company'+no,'allowances_breakup')
					
						// $scope.breakups_variable_others_calculation(parameter[iterate])

						// $scope.breakups_variable_others_calculation($scope.make_input_object('company'+no,'perquisites_breakup'))

						// $scope.breakups_variable_others_calculation($scope.make_input_object('company'+no,'profits_in_lieu_salary_breakup'))

						// $scope.breakups_variable_others_calculation($scope.make_input_object('company'+no,'allowances_breakup'))
						// $scope.disable_other_breakup_variable('company'+no);
						$scope.$apply()


					}

					for(var i=0;i<salary_breakup_param;i++){
						$scope.breakups_variable_others_calculation(salary_breakup_param[i])
					}

					for(var i=0;i<perquisites_breakup_param;i++){
						$scope.breakups_variable_others_calculation(perquisites_breakup_param[i])
					}

					for(var i=0;i<profit_in_liue_breakup_param;i++){
						$scope.breakups_variable_others_calculation(profit_in_liue_breakup_param[i])
					}

					for(var i=0;i<allowance_breakup_param;i++){
						$scope.breakups_variable_others_calculation(allowance_breakup_param[i])
					}

			},error:function(response){
				console.log(response.status);
			}
		});

	}

	$scope.get_co_owner_details=function(){


		$.ajax({
				url:'/get_co_owner_details/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id},
				success: function(response){
					// console.log(JSON.stringify(response))
					// alert(response.data);
					var data=response.data

					var elements=data.length
					// console.log(elements)

					for(element=0;element<elements;element++){

						property_no=data[element].Property_Id
						// console.log(property_no)

						var co_owner_details=data[element].co_owner_details

						var tenant_details=data[element].tenant_details

						// console.log(co_owner_details)

						// console.log('property'+property_no+'_no_of_co_owner')
						// console.log(co_owner_details.length.toString())
						if(co_owner_details!=null){

							$scope.initialise('user.property'+property_no+'_no_of_co_owner',co_owner_details.length.toString())
						
						// $timeout(function() {
							$scope.create_dynamic_variable('property'+property_no,$scope.fetch_breakup_variable('property'+property_no+'_no_of_co_owner'),'co_owner')
						// }, 0);

							for(iterate=0;iterate<co_owner_details.length;iterate++){

								co_owner_no=iterate+1
								keyword='property'+property_no+'_co_owner'+co_owner_no
								$scope.fetching_all_breakups(keyword,co_owner_details[iterate],'')

							}

						}else{

							$scope.initialise('user.property'+property_no+'_no_of_co_owner','1')
							$scope.create_dynamic_variable('property'+property_no,$scope.fetch_breakup_variable('property'+property_no+'_no_of_co_owner'),'co_owner')
						}
						
						// console.log(property_no)

						if(tenant_details!=null){

							if(tenant_details.length.toString()!='0'){
								$scope.initialise('user.property'+property_no+'_no_of_tenant',tenant_details.length.toString())
							}else{
								$scope.initialise('user.property'+property_no+'_no_of_tenant','1')
							}

							// $timeout(function() {
								$scope.create_dynamic_variable('property'+property_no,$scope.fetch_breakup_variable('property'+property_no+'_no_of_tenant'),'tenant')
							// }, 0);


							// console.log(property_no)
							for(iterate=0;iterate<tenant_details.length;iterate++){

								tenant_no=iterate+1
								keyword='property'+property_no+'_tenant'+tenant_no
								$scope.fetching_all_breakups(keyword,tenant_details[iterate],'')

							}
						}else{
							$scope.initialise('user.property'+property_no+'_no_of_tenant','1')

							$scope.create_dynamic_variable('property'+property_no,$scope.fetch_breakup_variable('property'+property_no+'_no_of_tenant'),'tenant')

						}
						
						$scope.$apply()


					}

			},error:function(response){
				console.log(response.status);
			}
		});

	}


	$scope.fetching_allowances_breakup=function(keyword,data,default_value){

		// console.log(data)
		if(data!=null){
			
			for(key in data){
				
				if(key=='lta')
					var variable='user.'+keyword+'_LTA_allowance'
				else if(key=='hra')
					var variable='user.'+keyword+'_HRA_allowance'
				else
					var variable='user.'+keyword+'_'+key


				// console.log(variable)
				if(data[key]!=null){

					// console.log(typeof data[key])
					if(typeof data[key]=='number'){
						// console.log('if')
						$scope.initialise(variable,$scope.money_format(data[key]))
						$scope.$apply();

					}else if(typeof data[key]=='string'){
						// console.log('else')
						$scope.initialise(variable,data[key])
						$scope.$apply();
					}

				}else if(data[key]==null){
					$scope.initialise(variable,default_value)
					 $scope.$apply();
				}
				
			}
		}

		$scope.$apply();		

	}

	$scope.fetching_all_breakups=function(keyword,data,default_value){

		// console.log(data)
		if(data!=null){
			
			for(key in data){
				
				var variable='user.'+keyword+'_'+key
				// console.log(variable)
				if(data[key]!=null){

					// console.log(typeof data[key])
					if(typeof data[key]=='number'){
						// console.log('if')
						$scope.initialise(variable,$scope.money_format(data[key]))
						$scope.$apply();

					}else if(typeof data[key]=='string'){
						// console.log('else')
						$scope.initialise(variable,data[key])
						$scope.$apply();
					}

				}else if(data[key]==null){
					$scope.initialise(variable,default_value)
					 $scope.$apply();
				}
				
			}
		}

		$scope.$apply();		

	}

	$scope.calculate_other_allowances=function(keyword,data,default_value){

		sum=0
		value=0
		console.log(data)
		if(data!=null){
			
			for(key in data){

				var variable='user.'+keyword+'_'+key

				console.log(data[key])
				if(data[key]!=null && key!='R_id'){

					if(typeof data[key]=='number'){

						value=data[key]
					}
					
				}else if(data[key]==null){
					value=0
				}

				sum=sum+value
				
			}
		}

	
		HRA=$scope.fetch_breakup_variable(keyword+'_HRA_allowance')
		LTA=$scope.fetch_breakup_variable(keyword+'_LTA_allowance')

		// console.log(HRA)
		// console.log(LTA)
		// console.log(sum)

		total_allowances=$scope.convert_to_int(HRA)+$scope.convert_to_int(LTA)+sum
		// console.log(total_allowances)
		$scope.initialise('user.'+keyword+'_total_allowances',total_allowances)
		$scope.$apply();


	
	}

	$scope.get_salary_variable=function(keyword){

		if(keyword=='salary_breakup'){
			return 'salary_provision'
		}else if(keyword=='perquisites_breakup'){
			return 'perquisites'
		}else if(keyword=='perquisites_breakup'){
			return 'perquisites'
		}else if(keyword=='profits_in_lieu_salary_breakup'){
			return 'profits_lieu_salary'
		}else if(keyword=='allowances_breakup'){
			return 'total_allowances'
		}

	}

	$scope.match_breakup_values_validation=function(keyword,company){

		// var company='company'+no
		var dynamic_array_variable=keyword+'_variable'
	    var dynamic_array_value=[]
	    dynamic_array_value=eval(dynamic_array_variable)
	    addition=0

	    for(i=0;i<dynamic_array_value.length;i++){
	    	// console.log(company+'_'+dynamic_array_value[i])
	    	value=$scope.fetch_breakup_variable(company+'_'+dynamic_array_value[i])
	    	// console.log(value)
	    	// console.log($scope.convert_to_int(value))
	    	addition=addition+$scope.convert_to_int(value)	
	    }

	    // console.log(company+'_'+$scope.get_salary_variable(keyword))
	    var salary_variable=$scope.fetch_breakup_variable(company+'_'+$scope.get_salary_variable(keyword))
	    var salary_variable=$scope.convert_to_int(salary_variable)
	    console.log('salary_variable '+salary_variable)
	    console.log('addition '+addition)

	    var match_status=salary_variable==addition
	    console.log('match_status '+match_status)

	    if(match_status==true){
	    	$scope.initialise(company+'_'+keyword+'_error','')
	    	$('#'+company+'_'+keyword+'_error').hide()
	    	return true
	    }else{
	    	$scope.initialise(company+'_'+keyword+'_error','Additon Not Match')
	    	$('#'+company+'_'+keyword+'_error').show()
	    	return false
	    }

	}

	

	$scope.convert_to_int=function(value){

		if(value!=undefined && value!=0){
			return parseInt(value.toString().replace(/,/g,''))
		}else if(value==undefined){
			return 0
		}else{
			return parseInt(value)
		}

	}

	$scope.prepare_breakup_input=function(company,variable_array_name,value_type){

		var data={}

		iterate_length=eval(variable_array_name+'_variable').length
		var dynamic_array_variable=variable_array_name+'_variable'
	    var dynamic_array_value=[]
	    dynamic_array_value=eval(dynamic_array_variable)

		for(var value=0; value<iterate_length ;value++){

			// console.log(dynamic_array_value[value])
			var temp_var=company+'_'+dynamic_array_value[value]
			comma_separated_value=$scope.fetch_breakup_variable(temp_var)

			if(value_type=='int'){
				if(comma_separated_value==undefined)
					comma_separated_value=0
				data[temp_var]= $scope.remove_comma(comma_separated_value)
			}else if(value_type=='str'){
				if(comma_separated_value==undefined)
					comma_separated_value=''
				data[temp_var]= comma_separated_value
			}
		}
		
		return JSON.stringify(data)

	}

	$scope.remove_comma=function(value){
		if(value!=undefined)
			return value.toString().replace(/,/g, '')
		else 
			return value
	}

	$scope.initialise=function(variable,prefilled_value){

	  // console.log(variable)
	  // console.log(prefilled_value)
	  if(prefilled_value!=undefined || variable!=undefined){
	    var model=$parse(variable)
	    model.assign($scope,prefilled_value)
	  }



	}

	$scope.fetch_breakup_variable=function(a) {
 
 		// console.log(a)
 		// console.log($scope.user[a])
      	return $scope.user[a];
	}

	$scope.create_dynamic_variable=function(element,no,variable_array_name){

		// element=''property1,property2'
		//no=no_of_co_owner for particular property
		// variable_array_name='static_variable_array_name'
		// console.log(element)
		// console.log(no)
		// console.log(variable_array_name)

		variable_list_JSON=[]
 

		for(var i=1;i<=no;i++){

		      var arguement=$scope.make_variable_json(element+'_'+variable_array_name+parseInt(i),variable_array_name,'')
		      // console.log(arguement)
		      variable_list_JSON.push(arguement);

		}

		// console.log(variable_list_JSON)
		// console.log(element+'_'+variable_array_name+'_variable_list')
		$scope.initialise(element+'_'+variable_array_name+'_variable_list',variable_list_JSON)
		// $scope.co_owner_variable_list=variable_list_JSON
		// $scope.$apply()

	}

	$scope.make_upper=function(value){

	  // value = ubo1_name
	  //$scope.$eval(value)='Rukhsar'

	  var model=$parse(value)
	  model.assign($scope,$scope.$eval(value).toUpperCase())

	}

	$scope.getting_JSON_data=function(keyword_no,variable_array_name,no){
		var data={}

		// console.log(variable_array_name)
		// console.log(no)
		for(element=1;element<=parseInt(no);element++){

			keyword='property'+keyword_no+'_'+variable_array_name+element
			Object.assign(data,JSON.parse($scope.prepare_breakup_input(keyword,variable_array_name,'str')))
				
		}

		return JSON.stringify(data)
	}


	$scope.co_owner_api=function(no_of_propery){

		// console.log(no_of_propery)
		// no_of_propery=propery_no 1 or 2 or 3
		// console.log('property'+no_of_propery+'_no_of_co_owner')
		no_of_co_owner=$scope.fetch_breakup_variable('property'+no_of_propery+'_no_of_co_owner')
		// console.log(no_of_co_owner)
		
		data=$scope.getting_JSON_data(no_of_propery,'co_owner',no_of_co_owner)

		// console.log(data)

		co_owner_flag=$scope.fetch_breakup_variable('co_owned'+no_of_propery)

		if(data){

			$.ajax({
						url:'/co_owner_api/',
						type:'POST',
						dataType: 'json',
						// async:false,
						data:{'data':data,'no_of_propery':no_of_propery,'client_id':client_id,'no_of_co_owner':no_of_co_owner,'co_owner_flag':co_owner_flag},
						success: function(response){
						
							console.log(response.status);
							
						},error:function(response){
							
							console.log(response.status);
						}
		    });

		}
	
		
	}


	$scope.tenant_api=function(no_of_propery){

		no_of_tenant=$scope.fetch_breakup_variable('property'+no_of_propery+'_no_of_tenant')
		data=$scope.getting_JSON_data(no_of_propery,'tenant',no_of_tenant)

		// console.log(data)

		tenant_flag=$scope.fetch_breakup_variable('house_property_type'+no_of_propery)

		if(data){

				$.ajax({
						url:'/tenant_api/',
						type:'POST',
						// async:false,
						dataType: 'json',
						data:{
								'data':data,
								'no_of_propery':no_of_propery,
								'client_id':client_id,
								'no_of_tenant':no_of_tenant,
								'tenant_flag':tenant_flag
						},
						success: function(response){
							
							console.log(response.status);
							
						},error:function(response){
							
							console.log(response.status);
						}
			});

		}

	
	}

	$scope.Name_of_Co_Owner_mandatory='M'
	$scope.PAN_of_Co_owner_mandatory='NM'
	$scope.Percentage_Share_in_Property_mandatory='NM'

	$scope.tenant_name_mandatory='M'
	$scope.tenant_pan_mandatory='NM'


	$scope.co_owner_validation=function(element,keyword){

		// element=property1,property2,property3
		// keyword=co_owner,tenant
		no=$scope.fetch_breakup_variable(element+'_no_of_'+keyword)
		var validation_status=$scope.iteartive_block_variable_validation(no,keyword,element+'_'+keyword)
		return validation_status
	}

	

	$scope.iteartive_specific_variable_validation=function(variable,actual_value,mandatory_variable){

		  // console.log(variable)
		  // console.log(actual_value)
		  // console.log(mandatory_variable)

		  var value=$scope.check_appropriate_value(actual_value)
		  
		 
		  if(typeof value=='number')
		    value=value.toString()


		  if(value==undefined || value==''){

		      // console.log($scope[mandatory_variable])
		      if($scope[mandatory_variable+'_mandatory']=='M'){

		        // console.log('error show')
		        $scope.apply_error_variable(variable,$scope.make_error_message(mandatory_variable))
		        $('#'+variable+'_error').show();
		        return false
		      }else{
		       
		        // console.log('error hide')
		        $scope.apply_error_variable(variable,'')
		        $('#'+variable+'_error').hide();
		        return true
		      }
		      
		  }else{

		      $scope.apply_error_variable(variable,'')
		      $('#'+variable+'_error').hide();
		      return true
		  }

	}


	$scope.iteartive_pan_variable_validation=function(variable,actual_value,mandatory_variable){

	  // console.log(variable)
	  // // console.log(value)
	  // console.log(mandatory_variable)

	  var value=$scope.$eval(actual_value)
	  if(value==undefined)
	    var value=actual_value

	  // console.log(value)

	  // console.log($scope.ubo_validation(variable,value,mandatory_variable))
	  if($scope.iteartive_specific_variable_validation(variable,value,mandatory_variable)){

	    if(value!='' && value!=undefined){

	      if(/[A-Za-z]{5}\d{4}[A-Za-z]{1}/.test(value)){

	        $scope.apply_error_variable(variable,'')
	        $('#'+variable+'_error').hide();
	        return true

	      }else{

	        $scope.apply_error_variable(variable,'Please Enter Valid PAN')
	        $('#'+variable+'_error').show();
	        return false

	      }

	    }else{
	    	return true
	    }
	      
	  }else{
	  	return false
	  }
	

	}

	$scope.check_appropriate_value=function(actual_value){

	    // console.log(actual_value)
	    try {
		  var value=$scope.$eval(actual_value)
		}
		catch(err) {

		  // console.log(err)
		  var value=actual_value
		}
	    // var value=$scope.$eval(actual_value)
	    if(value==undefined)
	      var value=actual_value

	    return value
	}


	$scope.make_error_message=function(variable){

      // console.log(variable)

      	if(variable.indexOf('_')!=-1)
        	variable=variable.replace(/\_/g, " ")
      	return 'Please Enter '+$scope.toTitleCase(variable)
      
    } 


	$scope.apply_error_variable=function(variable,value) {

	      // console.log(variable+'_error')
	      // console.log(value)
	      var model = $parse(variable+'_error');
	      model.assign($scope, value);
	}


	$scope.iteartive_block_variable_validation=function(iterate_length,variable_array_name,keyword){

		variable_array_length=eval(variable_array_name+'_variable').length
		var dynamic_array_variable=variable_array_name+'_variable'
	    var variable_list=[]
	    variable_list=eval(dynamic_array_variable)


	    validation=[]
	    final_validation=[]
	    // console.log(keyword)
	    // iterate no. of cowner
	    for(var iterate=0;iterate<iterate_length;iterate++){

	    	// iterate each and every variable in array
		    for(var element=0;element<variable_array_length;element++){

		    	var no=iterate+1
		        var concat=keyword+no+'_'+variable_list[element]

		        if(variable_list[element]=='PAN_of_Co_owner'){
          			this[variable_list[element]]=$scope.iteartive_pan_variable_validation(concat,$scope.fetch_breakup_variable(concat),variable_list[element])
        		}else{
        			this[variable_list[element]]=$scope.iteartive_specific_variable_validation(concat,$scope.fetch_breakup_variable(concat),variable_list[element])
        		}
		       
		        
		        validation.push(this[variable_list[element]])
		    }

		    // console.log(validation)


		    if(validation.includes(false))
		       final_validation.push(false)
		    else
		       final_validation.push(true)
		}

		// console.log(final_validation)

	    if(final_validation.includes(false))
		    return false
		else
		    return true
  
	}

	$scope.entertainment_allowance_hide_show=function(company,employment_category){

		console.log(employment_category)
		if(employment_category=='GOV'){
			// $scope.initialise(company+'_entertainment_allowance_hide','Y')
			$scope.initialise('user.'+company+'_entertainment_allowance','5,000')

		}else{
			// $scope.initialise(company+'_entertainment_allowance_hide','N')
			$scope.initialise('user.'+company+'_entertainment_allowance','0')
		}

	}

	$scope.final_entertainment_allowance=function(company1,company2,company3){

		// alert();
		if(company1=='GOV' || company2=='GOV' || company3=='GOV'){
			// $scope.initialise(company+'_entertainment_allowance_hide','Y')
			$scope.initialise('user.entertainment_allowance','5,000')

		}else{
			// $scope.initialise(company+'_entertainment_allowance_hide','N')
			$scope.initialise('user.entertainment_allowance','0')
		}

	}

	$scope.load_all_breakup_modal=function(no_of_company){

		company='company'+no_of_company
		$scope.initialise(company+'_breakup_modal_td',$scope.make_variable_json(company,'salary_breakup',0))
		$scope.initialise(company+'_breakup_modal_td',$scope.make_variable_json(company,'perquisites_breakup',0))
		$scope.initialise(company+'_breakup_modal_td',$scope.make_variable_json(company,'profits_in_lieu_salary_breakup',0))
		$scope.initialise(company+'_breakup_modal_td',$scope.make_variable_json(company,'allowances_breakup',0))

		$scope.breakups_variable_others_calculation($scope.make_input_object(company,'salary_breakup'))
		$scope.breakups_variable_others_calculation($scope.make_input_object(company,'perquisites_breakup'))
		$scope.breakups_variable_others_calculation($scope.make_input_object(company,'profits_in_lieu_salary_breakup'))
		$scope.breakups_variable_others_calculation($scope.make_input_object(company,'allowances_breakup'))
		// $scope.dynamic_break_up_modal_html('company'+no_of_company,'salary_breakup')
		// $scope.dynamic_break_up_modal_html('company'+no_of_company,'perquisites_breakup')
		// $scope.dynamic_break_up_modal_html('company'+no_of_company,'profits_in_lieu_salary_breakup')
		// $scope.dynamic_break_up_modal_html('company'+no_of_company,'allowances_breakup')
		$scope.breakup_api(company,'salary_breakup',$scope.user)
		$scope.breakup_api(company,'perquisites_breakup',$scope.user)
		$scope.breakup_api(company,'profits_in_lieu_salary_breakup',$scope.user)
		$scope.breakup_api(company,'allowances_breakup',$scope.user)


	}


	// $scope.calculate_others_breakup_value=function(){

	// }

}]);
