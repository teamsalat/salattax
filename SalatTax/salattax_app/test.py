# from django.http import HttpResponse
# # Python logging package
# from django.db.models import Q
# import logging
# import json
# import time
# from path import *
# import requests
# import pdftotext
# import re
# import traceback
# import pandas as pd
# import numpy as np
# from datetime import datetime
# from models import CGTransactions,CGCalculation,mfSchemeAssetClass,currentCostInflationIndex,HighestPrice31012018
# stdlogger = logging.getLogger(__name__)
# log = logging.getLogger(__name__)

# lumpsumPurchaseList=['Purchase','Purchase(NAV','Purchase - via Internet','Purchase - via Online']
# lumpsumPurchaseRejectionList=['Reversed']
# sipPurchaseList=['Systematic Investment Purchase','SIP Purchase -','Purchase Appln','Sys. Investment']
# sipPurchaseRejectionList=['SIP PurchaseCheque dishonoured','SIP Purchase (Reversal - Code I - Cheque Dishonoured']
# stpInList=['ABCDEFG']
# stpRejectionList=['ABCDEFG']
# stpOutList=['ABCDEFG']
# switchInList=['Switch In','Exchange From','Lateral Shift In']
# switchInRejectionList=['ABCDEFG']
# switchOutList=['Switch Out','Switch-out','Exchange To','Lateral Shift Out']
# switchOutRejectionList=['ABCDEFG']
# redemptionList=['Redemption - via Online','Redemption less TDS','Redemption']
# dividendReinvestList=['Dividend Reinvest']
# dividendReinvestRejectionList=['ABCDEFG']
# dividendPayoutRejectionList=['ABCDEFG']
# dividendPayoutList=['Dividend Payout']
# sttPaidList=['STT Paid']

# mappingDict={'Lumpsum Purchase':lumpsumPurchaseList,'Lumpsum Purchase Rejection':lumpsumPurchaseRejectionList,'SIP Purchase':sipPurchaseList,'SIP Purchase Rejection':sipPurchaseRejectionList,'STP In':stpInList,'STP Rejection':stpRejectionList,'STP Out':stpOutList,'Switch In':switchInList,'Switch In Rejection':switchInRejectionList,'Switch Out':switchOutList,'Switch Out Rejection':switchOutRejectionList,'Redemption':redemptionList,'#Dividend Payout':dividendPayoutList,'Dividend Reinvest':dividendReinvestList,'Dividend Reinvest Rejection':dividendReinvestRejectionList,'Dividend Payout Rejection':dividendPayoutRejectionList,'#STT Paid':sttPaidList}

# def getFinancialYearList(fd1,fd2):
# 	temp= str(fd1).split("-")
# 	start=int(temp[1])

# 	temp= str(fd2).split("-")
# 	endd=int(temp[1])

# 	diff=endd-start
# 	fyars=[]
# 	i=1
# 	fyars.append(str(fd1))
# 	while i <diff:
# 		tt=start+i
# 		if i==1:
# 			fyars.append(str(start)+'-'+str(tt))
# 		else:
# 			fyars.append(str(tt-1)+'-'+str(tt))
# 		i+=1
# 		fyars.append(str(fd2))
# 	return fyars

# #function take input of the datestring like 2017-05-01
# def get_financial_year(datestring):
#     date = datetime.strptime(datestring, "%Y-%m-%d").date()
#     #initialize the current year
#     year_of_date=date.year
#     #initialize the current financial year start date
#     financial_year_start_date = datetime.strptime(str(year_of_date)+"-04-01","%Y-%m-%d").date()
#     if date<financial_year_start_date:
#     	# return 'April, '+ str(financial_year_start_date.year-1)+' to March, '+ str(financial_year_start_date.year)
#         return str(financial_year_start_date.year-1)+'-'+str(financial_year_start_date.year)
#     else:
#     	# return 'April, '+ str(financial_year_start_date.year)+' to March, '+ str(financial_year_start_date.year+1)
#         return str(financial_year_start_date.year)+'-'+str(financial_year_start_date.year+1)

# def matchDict(arnNo,panNo,schemeCode,transactionDate,folioNo,line,sellTypeTransactionList):
# 	if any(c in line for c in sellTypeTransactionList[1]):
# 		return sellTypeTransactionList[0]
# 	return ''	

# def dateFormatStandardisation(df,column_name,data_source):
#     ### dateFormatStandardisation
#     log.info(data_source+' Date Format Standardisation')
#     try:

#         #log.info(df[column_name])
#         if (df[column_name].dtype=='float64'):
#             df[column_name]=pd.to_datetime('1899-12-30') + pd.to_timedelta(df[column_name],'D')

#         df[column_name] = df[column_name].fillna('')
#         df[column_name] = df[column_name].astype(str)
#         df[column_name] = df[column_name].str.lower()
#         df[column_name] = df[column_name].str.strip()
#         #df[column_name] = df[column_name].astype('datetime64')

#         ##df[column_name] = df[column_name].apply(lambda x: convert8LenDate(x) if len(x)=='8' else x )

#         #log.info(df[column_name])
#         #### Change date format to yyyy-mm-dd
#         df[column_name] = pd.to_datetime(df[column_name], errors='coerce').dt.strftime('%Y-%m-%d').replace('NaT', '')
        
#         #log.info(df[column_name])

#         return df

#     except Exception as ex:
#         log.error('Error in '+data_source+' '+column_name+' dateFormatStandardisation: '+traceback.format_exc())
#         df = []
#         return df

# def testFunction(request):
# 	result={}
# 	i=0
# 	try:
# 		if CGTransactions.objects.filter(businessId = 0,partnerId = 0).exists():
# 			CGTransactions_dataaa = CGTransactions.objects.values_list('folioNo',flat=True).distinct().filter(businessId = 0,partnerId = 0)
# 			log.info(CGTransactions_dataaa)
# 			for CGTranss in CGTransactions_dataaa:
# 				log.info(CGTranss)

# 				if CGTransactions.objects.filter(businessId = 0,partnerId = 0,folioNo = CGTranss).exists():
# 					CGTransactions_dataa = CGTransactions.objects.values_list('scripSchemeCode',flat=True).distinct().filter(businessId = 0,partnerId = 0,folioNo = CGTranss)
# 					log.info(CGTransactions_dataa)
# 					for CGTrans in CGTransactions_dataa:
# 						log.info(CGTrans)
# 						cgAmount=0
# 						if CGTransactions.objects.filter(businessId = 0,partnerId = 0,scripSchemeCode = CGTrans,folioNo = CGTranss).exists():
# 							CGTransactions_data = CGTransactions.objects.filter(businessId = 0,partnerId = 0,scripSchemeCode = CGTrans,folioNo = CGTranss).order_by('id').values()
# 							log.info('Folio'+str(CGTrans))
# 							CGDF = pd.DataFrame(list(CGTransactions_data))
# 							CGDF.insert(1, 'redeemed_unit', '')
# 							CGDF.insert(1, 'redeemed_unit_flg', '')
# 							CGDF.insert(1, 'cgAmount', '')
# 							CGDF['qtyUnits']=CGDF['qtyUnits'].apply(lambda x: x.replace("(", "").replace(")", "") if '(' in x else x)
# 							CGDF['qtyUnits']=CGDF['qtyUnits'].apply(lambda x: x.replace(",", "") if ',' in x else x)
# 							CGDF['qtyUnits'].astype(float)
# 							CGDF['priceNAV']=CGDF['priceNAV'].apply(lambda x: x.replace("(", "").replace(")", "") if '(' in x else x)
# 							CGDF['priceNAV']=CGDF['priceNAV'].apply(lambda x: x.replace(",", "") if ',' in x else x)
# 							CGDF['priceNAV'].astype(float)
# 							CGDF['amount']=CGDF['amount'].apply(lambda x: x.replace("(", "").replace(")", "") if '(' in x else x)
# 							CGDF['amount']=CGDF['amount'].apply(lambda x: x.replace(",", "") if ',' in x else x)
# 							CGDF['amount'].astype(float)
# 							CGDF=dateFormatStandardisation(CGDF,'transactionDate','TTT')
# 							#CGDFF = pd.DataFrame(CGDF.loc[(CGDF['transactionType']=='Redemption') & ((CGDF['transactionDate'] > '2018-04-01') & (CGDF['transactionDate'] <= '2019-03-31'))])
# 							CGDFF = pd.DataFrame(CGDF.loc[((CGDF['transactionType']=='Redemption') | (CGDF['transactionType']=='Switch Out'))])
# 							# log.info(CGDFF.index.values[0])
							
# 							for j in range(len(CGDFF.index.values)):
# 								reedeam_index=CGDFF.index[j]
# 								reedeam_unit=CGDFF.loc[reedeam_index , : ].qtyUnits.replace("(", "").replace(")", "")
# 								reedeam_nav=CGDFF.loc[reedeam_index , : ].priceNAV.replace("(", "").replace(")", "")
# 								reedeam_date=CGDFF.loc[reedeam_index , : ].transactionDate
# 								reedeam_id=int(CGDFF.loc[reedeam_index , : ].id)

# 								log.info('**********')
# 								log.info(reedeam_index)
# 								log.info(reedeam_unit)
# 								log.info(reedeam_nav)
# 								log.info('**********')
								
# 								CGDFFF = pd.DataFrame(CGDF.loc[((CGDF['transactionType']=='Lumpsum Purchase') | (CGDF['transactionType']=='SIP Purchase') | (CGDF['transactionType']=='Dividend Reinvest') | (CGDF['transactionType']=='Switch In')) & (CGDF.index <reedeam_index)])

# 								oldAmount=0
# 								newAmount=0
# 								log.info(CGDFFF['qtyUnits'])
# 								for i in range(len(CGDFFF.index)):
# 									log.info('Iterations')
# 									if float(CGDFFF.qtyUnits[CGDFFF.index[i]])<=float(reedeam_unit) and CGDFFF['redeemed_unit_flg'][CGDFFF.index[i]]=='':
# 										log.info(float(CGDFFF.qtyUnits[CGDFFF.index[i]]))
# 										CGDFFF['redeemed_unit'][CGDFFF.index[i]]=CGDFFF.qtyUnits[CGDFFF.index[i]]
# 										CGDFFF['redeemed_unit_flg'][CGDFFF.index[i]]='Y'

# 										CGDF['redeemed_unit'][CGDFFF.index[i]]=CGDFFF.qtyUnits[CGDFFF.index[i]]
# 										CGDF['redeemed_unit_flg'][CGDFFF.index[i]]='Y'

# 										oldAmount=float(CGDFFF.qtyUnits[CGDFFF.index[i]])*float(CGDFFF.priceNAV[CGDFFF.index[i]])
# 										newAmount=float(CGDFFF.qtyUnits[CGDFFF.index[i]])*float(reedeam_nav)
# 										log.info(oldAmount)
# 										log.info(newAmount)
# 										log.info(newAmount-oldAmount)
# 										cgAmount=cgAmount+(newAmount-oldAmount)
# 										reedeam_unit=float(reedeam_unit)-float(CGDFFF.qtyUnits[CGDFFF.index[i]])
# 										log.info(reedeam_unit)

# 										#Calculate sellDate-buyDate
# 										buyDate=datetime.strptime(str(CGDFFF.transactionDate[CGDFFF.index[i]]),'%Y-%m-%d')
# 										sellDate=datetime.strptime(str(reedeam_date),'%Y-%m-%d')
# 										diffrance=sellDate-buyDate
# 										log.info(diffrance.days)

# 										CGType='0'
# 										costOfAquisition=0.0
# 										FMV=0.0
# 										if CGDFFF.assetClass[CGDFFF.index[i]]=='Equity' or CGDFFF.assetClass[CGDFFF.index[i]]=='Shares':
# 											if HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).exists():
# 												highestPrice31012018Data=HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).latest('id')
# 												FMV=highestPrice31012018Data.highestPrice
# 											if diffrance.days < 365 and sellDate != buyDate:
# 												CGType='ST'
# 												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
# 											else:
# 												CGType='LT'
# 												costOfAquisition=max(float(CGDFFF.priceNAV[CGDFFF.index[i]]),min(FMV,reedeam_nav))
# 										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debt':
# 											if diffrance.days < 1095:
# 												CGType='ST'
# 												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
# 											else:
# 												CGType='LT'
# 												log.info(get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]]))
# 												if currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).exists():
# 													sellCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).latest('id')
# 													reedeamCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(reedeam_date)).latest('id')
# 													costOfAquisition=(reedeamCurrentCostInflationIndexData.costInflationIndex*float(CGDFFF.amount[CGDFFF.index[i]]))/sellCurrentCostInflationIndexData.costInflationIndex
# 													log.info(costOfAquisition)
# 										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debentures':
# 											if diffrance.days < 365:
# 												CGType='ST'
# 											else:
# 												CGType='LT'					

# 										capitalGain  = float(newAmount) - (costOfAquisition * float(CGDFFF.redeemed_unit[CGDFFF.index[i]]))		

# 										if CGTransactions.objects.filter(id=reedeam_id).exists():
# 											CGTransactionsData=CGTransactions.objects.filter(id=reedeam_id).latest('id')
# 											CGCalculation_data = CGCalculation(businessPartnerId=0,intermediaryId=CGDFFF.intermediaryId[CGDFFF.index[i]],pId=CGDFFF.pId[CGDFFF.index[i]],cgTransactionId=CGTransactionsData,assetClass=CGDFFF.assetClass[CGDFFF.index[i]],scripSchemeCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]],sellDate=reedeam_date,sellQty=CGDFFF.redeemed_unit[CGDFFF.index[i]],sellPriceNAV=reedeam_nav,sellAmount=newAmount,purchaseDate=CGDFFF.transactionDate[CGDFFF.index[i]],purchasePriceNAV=float(CGDFFF.priceNAV[CGDFFF.index[i]]),FMV=FMV,costofAcquisition=costOfAquisition,CGType=CGType,capitalGain=capitalGain)
# 											CGCalculation_data.save()
# 									elif float(CGDFFF.qtyUnits[CGDFFF.index[i]])>float(reedeam_unit) and CGDFFF['redeemed_unit_flg'][CGDFFF.index[i]]=='':
# 										log.info(reedeam_unit)
# 										log.info(float(CGDFFF.qtyUnits[CGDFFF.index[i]]))
# 										aa=float(CGDFFF.qtyUnits[CGDFFF.index[i]])-float(reedeam_unit)
# 										CGDFFF['redeemed_unit'][CGDFFF.index[i]]=reedeam_unit
# 										CGDFFF['qtyUnits'][CGDFFF.index[i]]=aa

# 										CGDF['redeemed_unit'][CGDFFF.index[i]]=reedeam_unit
# 										CGDF['qtyUnits'][CGDFFF.index[i]]=aa

# 										oldAmount=float(reedeam_unit)*float(float(CGDFFF.priceNAV[CGDFFF.index[i]]))
# 										newAmount=float(reedeam_unit)*float(float(reedeam_nav))
# 										log.info(oldAmount)
# 										log.info(newAmount)
# 										log.info(newAmount-oldAmount)
# 										cgAmount=cgAmount+(newAmount-oldAmount)
# 										reedeam_unit=float(reedeam_unit)-float(CGDFFF.qtyUnits[CGDFFF.index[i]])
# 										log.info(reedeam_unit)
# 										log.info('##############')
# 										log.info(cgAmount)
# 										CGDFFF['cgAmount'][CGDFFF.index[i]]=cgAmount
# 										CGDF['cgAmount'][CGDFFF.index[i]]=cgAmount

# 										#Calculate sellDate-buyDate
# 										buyDate=datetime.strptime(str(CGDFFF.transactionDate[CGDFFF.index[i]]),'%Y-%m-%d')
# 										sellDate=datetime.strptime(str(reedeam_date),'%Y-%m-%d')
# 										diffrance=sellDate-buyDate
# 										log.info(diffrance.days)

# 										CGType='0'
# 										costOfAquisition=0.0
# 										FMV=0.0
# 										if CGDFFF.assetClass[CGDFFF.index[i]]=='Equity' or CGDFFF.assetClass[CGDFFF.index[i]]=='Shares':
# 											if buyDate < datetime.strptime('2018-01-31','%Y-%m-%d'):
# 												if HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).exists():
# 													highestPrice31012018Data=HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).latest('id')
# 													FMV=highestPrice31012018Data.highestPrice
# 											if diffrance.days < 365 and sellDate != buyDate:
# 												CGType='ST'
# 												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
# 											else:
# 												CGType='LT'
# 												costOfAquisition=max(float(CGDFFF.priceNAV[CGDFFF.index[i]]),min(FMV,reedeam_nav))
# 										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debt':
# 											if diffrance.days < 1095:
# 												CGType='ST'
# 												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
# 											else:
# 												CGType='LT'
# 												log.info(get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]]))
# 												if currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).exists():
# 													sellCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).latest('id')
# 													reedeamCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(reedeam_date)).latest('id')
# 													costOfAquisition=(reedeamCurrentCostInflationIndexData.costInflationIndex*float(CGDFFF.amount[CGDFFF.index[i]]))/sellCurrentCostInflationIndexData.costInflationIndex
# 													log.info(costOfAquisition)
# 										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debentures':
# 											if diffrance.days < 365:
# 												CGType='ST'
# 											else:
# 												CGType='LT'
												
# 										capitalGain  = float(newAmount) - (costOfAquisition * float(CGDFFF.redeemed_unit[CGDFFF.index[i]]))

# 										if CGTransactions.objects.filter(id=reedeam_id).exists():
# 											CGTransactionsData=CGTransactions.objects.filter(id=reedeam_id).latest('id')
# 											CGCalculation_data = CGCalculation(businessPartnerId=0,intermediaryId=CGDFFF.intermediaryId[CGDFFF.index[i]],pId=CGDFFF.pId[CGDFFF.index[i]],cgTransactionId=CGTransactionsData,assetClass=CGDFFF.assetClass[CGDFFF.index[i]],scripSchemeCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]],sellDate=reedeam_date,sellQty=CGDFFF.redeemed_unit[CGDFFF.index[i]],sellPriceNAV=reedeam_nav,sellAmount=newAmount,purchaseDate=CGDFFF.transactionDate[CGDFFF.index[i]],purchasePriceNAV=float(CGDFFF.priceNAV[CGDFFF.index[i]]),FMV=FMV,costofAcquisition=costOfAquisition,CGType=CGType,capitalGain=capitalGain)
# 											CGCalculation_data.save()
# 										break

# 							# log.info(CGDFFF['redeemed_unit'])
# 							# log.info(CGDFFF['redeemed_unit_flg'])
# 							#log.info(CGDFFF.index.values[0])
# 							log.info(cgAmount)
# 							log.info(CGDF['cgAmount'])
					
# 		return HttpResponse(json.dumps(result), content_type='application/json')
# 	except Exception as ex:
# 		log.error('Error in Business report View: %s' % traceback.format_exc())
# 		return HttpResponse('fail', content_type='application/json')

# def testFunction111(request):
# 	result={}
# 	wordList={}
# 	panRegex = r'^[A-Z]{5}[0-9]{4}[A-Z]$'
# 	dateRegex = r'\d\d-(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{4}'
# 	folioCount=0
# 	email=''
# 	mobile=''
# 	folioNo=''
# 	panNo=''
# 	arnNo=''
# 	schemeCode=''
# 	transactionDate=''
# 	last_pos=''
# 	transactionType=''
# 	qtyUnits =''
# 	priceNAV=''
# 	amount =''
# 	globalPanNo=''
# 	try:
# 		# Load your PDF
# 		with open("/home/salatTax/SalatTax/danny.pdf", "rb") as f:
# 			pdf = pdftotext.PDF(f,"bharat123")
# 		f.close()
# 		# Save all text to a txt file.
# 		with open('/home/salatTax/SalatTax/danny.txt', 'w') as f:
# 			f.write(re.sub(' +', ' ',"\n\n\r".join(pdf).encode('utf-8').strip()))
# 		f.close()

# 		f=open("/home/salatTax/SalatTax/danny.txt", "r")	
# 		#lines = list(f)
# 		#log.info(lines)
# 		line = f.readline()
# 		while line:
# 			tt=line.split(" ")
# 			if re.match(dateRegex, tt[1]) and tt[2]=='To' and re.match(dateRegex, tt[3]):
# 				log.info('Report From Date: '+tt[1])
# 				log.info('Report To Date: '+tt[3])
# 			elif 'Email Id:' in line:
# 				temp= line.split(" ")
# 				email='Email: '.join(s for s in temp if '@' in s)

# 			elif 'Mobile:' in line:
# 				temp= line.split(" ")
# 				mobile='Mobile: '.join(s for s in temp if '+' in s)

# 			elif 'Folio No:' in line:
# 				folioCount=folioCount+1
# 				temp= line.split(" ")
# 				if temp[3]=='/':
# 					folioNo=temp[2]+temp[3]+temp[4]
# 				else:
# 					folioNo=temp[2]
# 				#log.info(str(folioCount)+' Folio No: '+folioNo)	
# 				panNo=''.join(s for s in temp if re.match(panRegex, s))
# 				log.info('panNo: '+panNo)
# 				if panNo!='':
# 					globalPanNo=panNo
# 				ttt=f.readline()
# 				temp2=ttt.split(" ")	
# 				if 'CAMSCASWS' in ttt:
# 					temp2=f.readline().split(" ")
# 					log.info(temp2)
# 					temp3=temp2[0].split("-")
# 					schemeCode=temp3[0]
# 					log.info('Scheme Code: '+schemeCode)
# 				else:
# 					log.info(temp2)
# 					temp3=temp2[0].split("-")
# 					schemeCode=temp3[0]
# 					log.info('Scheme Code: '+schemeCode)
# 				if ')' in "".join(s for s in temp2 if 'ARN-' in s) :
# 					log.info(temp2)
# 					ss="".join(s for s in temp2 if 'ARN-' in s)
# 					arnNo=ss[:-1]
# 				else:
# 					temp2=f.readline().split(" ")
# 					log.info(temp2)
# 					if ')' in "".join(s for s in temp2 if 'ARN-' in s) :
# 						log.info(temp2)
# 						ss="".join(s for s in temp2 if 'ARN-' in s)
# 						arnNo=ss[:-2]
# 					elif ')' in "".join(s for s in temp2 if ')' in s) :
# 						log.info(temp2)
# 						ss="".join(s for s in temp2 if ')' in s)
# 						arnNo='ARN-'+ss[:-2]
# 					else:
# 						arnNo='DIRECT'
# 					log.info('ARN No: '+arnNo)	

# 				line = f.readline()
# 				while line:	
# 					if ('.' or 'Folio No:') and not '***Invalid Redemption' in line:
# 						if 'Folio No:' in line:
# 							log.info(line)
# 							f.seek(last_pos)
# 							break
# 						temp= line.split(" ")
# 						#log.info(temp)
# 						if re.match(dateRegex, temp[0]):
# 							transactionDate=temp[0]
# 							# log.info(line)
# 							# log.info(temp)
# 							transactionType=''

# 							matchArray=filter(lambda n : n if n != None else '',map(lambda kv : matchDict(arnNo,panNo,schemeCode,transactionDate,folioNo,line,kv), mappingDict.iteritems()))
# 							if len(matchArray)>=1:
# 								matchValue=matchArray[0]
# 								if '/' in "".join(s for s in temp if 'ARN-' in s) :
# 									ss="".join(s for s in temp if 'ARN-' in s)
# 									ss= ss.split("/")
# 									arnNo=ss[0]
# 								elif 'ARN-' in "".join(s for s in temp if 'ARN-' in s) :
# 									ss="".join(s for s in temp if 'ARN-' in s)
# 									arnNo=ss				

# 								if '#'in matchValue:
# 									qtyUnits = '0'
# 									priceNAV= '0'
# 									amount = temp[len(temp)-1]
# 									transactionType=matchValue.replace('#', '')
# 								else:	
# 									qtyUnits = temp[len(temp)-3]
# 									priceNAV= temp[len(temp)-2]
# 									amount = temp[len(temp)-4]
# 									transactionType=matchValue

# 								if panNo=='':
# 									panNo=globalPanNo

# 								assetClasss=''
# 								if mfSchemeAssetClass.objects.filter(productCode=schemeCode).exists():
# 									mfSchemeAssetClassData=mfSchemeAssetClass.objects.filter(productCode=schemeCode).latest('id')
# 									log.info('assetClass')
# 									assetClasss=mfSchemeAssetClassData.taxType
# 								else:
# 									assetClasss=''

# 								if not CGTransactions.objects.filter(businessId = 0,partnerId = 0,intermediaryId= arnNo,pId = panNo,assetClass = assetClasss,scripSchemeCode = schemeCode,transactionType = transactionType,transactionDate = transactionDate,qtyUnits = qtyUnits,priceNAV= priceNAV,amount = amount,folioNo = folioNo).exists():	
# 									CGTransactions_data = CGTransactions(businessId = 0,partnerId = 0,intermediaryId= arnNo,pId = panNo,assetClass = assetClasss,scripSchemeCode = schemeCode,transactionType = transactionType,transactionDate = transactionDate,qtyUnits = qtyUnits,priceNAV= priceNAV,amount = amount,transactionCost = '',folioNo = folioNo)
# 									CGTransactions_data.save()	

# 					last_pos=f.tell()		
# 					line = f.readline()
# 			line = f.readline()
# 		f.close()
# 		return HttpResponse('success', content_type='application/json')
# 	except Exception as ex:
# 		log.error('Error in Business report View: %s' % traceback.format_exc())
# 		return HttpResponse('fail', content_type='application/json')		