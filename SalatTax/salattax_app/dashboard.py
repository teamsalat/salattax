from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone

import os, sys
from io import BytesIO
from StringIO import StringIO
import logging
import json

from .models import sign_up_otp,SalatTaxUser,SalatTaxRole,ResetPassword,Personal_Details
from .models import Client_fin_info1, tds_tcs,tax_paid, salatclient, computation,Employer_Details
from .models import salat_registration,Return_Details, Business_Partner
#for url request
import requests
from datetime import date, datetime

import random
# for Email
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import hashlib
import os.path
from os import path
import re

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

import traceback
from .path import *

@csrf_exempt
def check_business(request):
	if request.method=='POST':
		result={}

		try:
			client_id = request.POST.get('client_id')
			is_business = 0

			for client in SalatTaxUser.objects.filter(id=client_id):
				log.info(client.id)
				salattaxUser = client


			# role_instance = SalatTaxRole.objects.get(rolename='Admin')

			# salattaxUser = SalatTaxUser.objects.get(id=client_id)

			# if role_instance == salattaxUser.role:
			# 	is_business = 1
			partnerId=0
			if salattaxUser.role is None:
				pass
			else:
				is_business = salattaxUser.role.rolename

				if Business_Partner.objects.filter(user_id=salattaxUser).exists():
					business_partner_inst=Business_Partner.objects.filter(user_id=salattaxUser).latest("id")
					partnerId=business_partner_inst.id

				businessId = salattaxUser.role.rolename

			result['status'] = 'success'
			result['client_id'] = client_id
			result['is_business'] = is_business
			result['partnerId'] = partnerId
		except Exception as ex:
			result['status']= 'Error : %s' % ex
			log.info('Error in checking business '+traceback.format_exc())

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_business_client(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			client_count = 0
			CID = []
			name = []
			PAN = []
			form16 = []
			client_reg = []
			AS26 = []
			taxpayble = []
			payment = []
			xml = []
			itr_verify = []
			username = ''

			# for client in User.objects.all():
			# 	log.info(str(client.id) +' '+ client.username)
			# log.info('SalatTaxUser')
			# for stu in SalatTaxUser.objects.all():
			# 	log.info(str(stu.id) +' '+ str(stu.user_id) +' '+ str(stu.user) )

			MYDIR = os.path.dirname(__file__).rsplit('/', 1)[0]

			if client_id != '19' and client_id != '14':
				for c in Personal_Details.objects.filter(Business_Id=client_id):
					client_count += 1
					if SalatTaxUser.objects.filter(id=c.P_Id).exists():
						for su in User.objects.filter(username='salattax'):
							CID.append(c.P_Id) #for testing change here
						# for su in User.objects.filter(id=c.P_Id):
						for stu in SalatTaxUser.objects.filter(id=c.P_Id):
							username = str(stu.user)
					else:
						CID.append(c.P_Id)

					if c.name is not None:
						name.append(c.name.replace('|',' '))
						PAN.append(c.pan)
					else:
						if Client_fin_info1.objects.filter(client_id=c.P_Id).exclude(flag=2).exists():
							for client in Client_fin_info1.objects.filter(client_id = c.P_Id).exclude(flag=2):
								name.append(client.employee_name.replace('|',' '))
								PAN.append(client.employee_pan)
						else:
							name.append('')
							PAN.append(username)
					
					R_ID = get_rid(c.P_Id)
					if computation.objects.filter(R_Id=R_ID).exists():
						for comp in computation.objects.filter(R_Id = R_ID):
							if comp.taxpayable_refund is None:
								taxpayble.append('0')
							else:
								taxpayble.append(str(comp.taxpayable_refund) )
					else:
						taxpayble.append('-')

					payment.append('Pending')
					itr_verify.append('Unknown')

					file_path = MYDIR+'/static/xml/'+c.pan+'.xml'
					if path.exists(file_path):
						xml.append('Generated')
					else:
						xml.append('Pending')

					personal_instance=Personal_Details.objects.get(P_Id=c.P_Id)
					if Client_fin_info1.objects.filter(client_id=c.P_Id).exclude(flag=2).exists():
						form16.append('Processed')
					else:
						if Employer_Details.objects.filter(P_id=personal_instance).exists():
							form16.append('Manual')
						else:
							form16.append('Pending')

					if salatclient.objects.filter(PAN=c.pan).exists():
						if salatclient.objects.filter(PAN=c.pan,Is_salat_client=1,Is_ERI_reg=1).exists():
							client_reg.append('Processed')
						else:
							client_reg.append('Pending')
					else:
						client_reg.append('Error')

					if tds_tcs.objects.filter(R_Id=R_ID).exists():
						AS26.append('Processed')
					else:
						if tax_paid.objects.filter(R_Id=R_ID).exists():
							AS26.append('Processed')
						else:
							AS26.append('Pending')

			if client_id == '19' or client_id == '14':
				for c in Personal_Details.objects.exclude(P_Id=1):
					client_count += 1
					if SalatTaxUser.objects.filter(id=c.P_Id).exists():
						for su in User.objects.filter(username='salattax'):
							CID.append(c.P_Id) #for testing change here
						# for su in User.objects.filter(id=c.P_Id):
						for stu in SalatTaxUser.objects.filter(id=c.P_Id):
							username = str(stu.user)
					else:
						CID.append(c.P_Id)

					if c.name is not None:
						name.append(c.name.replace('|',' '))
						PAN.append(c.pan)
					else:
						if Client_fin_info1.objects.filter(client_id=c.P_Id).exclude(flag=2).exists():
							for client in Client_fin_info1.objects.filter(client_id = c.P_Id).exclude(flag=2):
								name.append(client.employee_name.replace('|',' '))
								PAN.append(client.employee_pan)
						else:
							name.append('')
							PAN.append(username)
					
					R_ID = get_rid(c.P_Id)
					taxpayable_amt = ''
					if computation.objects.filter(R_Id=R_ID).exists():
						for comp in computation.objects.filter(R_Id = R_ID):
							if comp.taxpayable_refund is None:
								taxpayable_amt = '0'
							else:
								taxpayable_amt = str(comp.taxpayable_refund)
					else:
						taxpayable_amt = '-'
					taxpayble.append(taxpayable_amt)
					log.info(str(c.P_Id)+' '+str(R_ID)+' '+c.name)

					payment.append('Pending')
					itr_verify.append('Unknown')

					# file_path = MYDIR+'/static/xml/'+c.pan+'.xml'
					# if path.exists(file_path):
					# 	xml.append('Generated')
					# else:
					# 	xml.append('Pending')
					xml_gen = 'Pending'
					for filepath in os.listdir(selenium_path+'/xml/'):
						if filepath.startswith( c.pan+'_ITR' ):
							xml_gen = 'Generated'
					xml.append(xml_gen)

					# log.info(str(c.P_Id)+' '+c.name)
					personal_instance=Personal_Details.objects.get(P_Id=c.P_Id)
					if Client_fin_info1.objects.filter(client_id=c.P_Id).exclude(flag=2).exists():
						form16.append('Processed')
					else:
						if Employer_Details.objects.filter(P_id=personal_instance).exists():
							form16.append('Manual')
						else:
							form16.append('Pending')

					if salatclient.objects.filter(PAN=c.pan).exists():
						if salatclient.objects.filter(PAN=c.pan,Is_salat_client=1,Is_ERI_reg=1).exists():
							client_reg.append('Processed')
						else:
							client_reg.append('Pending')
					else:
						client_reg.append('Error')

					# if tds_tcs.objects.filter(R_Id=c.pan).exists():
					# 	AS26.append('Processed')
					# else:
					# 	if tax_paid.objects.filter(R_Id=c.P_Id).exists():
					# 		AS26.append('Processed')
					# 	else:
					# 		AS26.append('Pending')
					if salat_registration.objects.filter(pan=c.pan).exists():
						as_26 = 'Processed'
						salat_registration_instance = salat_registration.objects.filter(pan=c.pan).order_by('-date_time')[0]
						
						if(salat_registration_instance.processed_26as_count is None or salat_registration_instance.processed_26as_count==''):
							as_26 = 'Pending'
						else:
							if(int(salat_registration_instance.processed_26as_count)<5):
								if(salat_registration_instance.processed_26as_count!=salat_registration_instance.downloaded_26as_count):
									as_26 = 'Pending'
					else:
						as_26 = 'Pending'
					AS26.append(as_26)


			result['status'] = 'success'
			result['client_id'] = client_id
			result['CID'] = CID
			result['name'] = name
			result['PAN'] = PAN
			result['form16'] = form16
			result['client_reg'] = client_reg
			result['AS26'] = AS26
			result['taxpayble'] = taxpayble
			result['payment'] = payment
			result['xml'] = xml
			result['itr_verify'] = itr_verify
			result['client_count'] = client_count
		except Exception as ex:
			log.error('Error in : '+traceback.format_exc())
			result['status']= 'Error : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance).exists():
		Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
		R_ID = Return_Details_instance.R_id
		# for rd in Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019'):
		# 	R_ID = rd.R_id

	return R_ID


