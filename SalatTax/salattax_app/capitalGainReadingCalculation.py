from django.http import HttpResponse
# Python logging package
from django.db.models import Q
import logging
import json
import time
from path import *
import requests
#import pdftotext
import re
import traceback
import pandas as pd
import numpy as np
import random
from datetime import datetime
from django.contrib.auth.models import User
from models import CGTransactions,CGCalculation,mfSchemeAssetClass,currentCostInflationIndex,HighestPrice31012018,CGSummary,Business_Partner,SalatTaxUser,SalatTaxRole,Personal_Details,Shares_LT,Shares_ST,Equity_LT,Equity_ST,Debt_MF_LT,Debt_MF_ST,Listed_Debentures_LT,Listed_Debentures_ST,Return_Details
from django.views.decorators.csrf import csrf_exempt
stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

lumpsumPurchaseList=['Purchase','Purchase(NAV','Purchase - via Internet','Purchase - via Online']
lumpsumPurchaseRejectionList=['Reversed']
sipPurchaseList=['Systematic Investment Purchase','SIP Purchase -','Purchase Appln','Sys. Investment']
sipPurchaseRejectionList=['SIP PurchaseCheque dishonoured','SIP Purchase (Reversal - Code I - Cheque Dishonoured']
stpInList=['ABCDEFG']
stpRejectionList=['ABCDEFG']
stpOutList=['ABCDEFG']
switchInList=['Switch In','Exchange From','Lateral Shift In']
switchInRejectionList=['ABCDEFG']
switchOutList=['Switch Out','Switch-out','Exchange To','Lateral Shift Out','Systematic Transfer']
switchOutRejectionList=['ABCDEFG']
redemptionList=['Redemption - via Online','Redemption less TDS','Redemption']
dividendReinvestList=['Dividend Reinvest']
dividendReinvestRejectionList=['ABCDEFG']
dividendPayoutRejectionList=['ABCDEFG']
dividendPayoutList=['Dividend Payout']
sttPaidList=['STT Paid']

mappingDict={'Lumpsum Purchase':lumpsumPurchaseList,'Lumpsum Purchase Rejection':lumpsumPurchaseRejectionList,'SIP Purchase':sipPurchaseList,'SIP Purchase Rejection':sipPurchaseRejectionList,'STP In':stpInList,'STP Rejection':stpRejectionList,'STP Out':stpOutList,'Switch In':switchInList,'Switch In Rejection':switchInRejectionList,'Switch Out':switchOutList,'Switch Out Rejection':switchOutRejectionList,'Redemption':redemptionList,'#Dividend Payout':dividendPayoutList,'Dividend Reinvest':dividendReinvestList,'Dividend Reinvest Rejection':dividendReinvestRejectionList,'Dividend Payout Rejection':dividendPayoutRejectionList,'#STT Paid':sttPaidList}

def getFinancialYearList(fd1,fd2):
	temp= str(fd1).split("-")
	start=int(temp[1])

	temp= str(fd2).split("-")
	endd=int(temp[1])

	diff=endd-start
	fyars=[]
	i=1
	fyars.append(str(fd1))
	while i <diff:
		tt=start+i
		if i==1:
			fyars.append(str(start)+'-'+str(tt))
		else:
			fyars.append(str(tt-1)+'-'+str(tt))
		i+=1
		fyars.append(str(fd2))
	return fyars

#function take input of the datestring like 2017-05-01
def get_financial_year(datestring):
	date = datetime.strptime(datestring, "%Y-%m-%d").date()
	#initialize the current year
	year_of_date=date.year
	#initialize the current financial year start date
	financial_year_start_date = datetime.strptime(str(year_of_date)+"-04-01","%Y-%m-%d").date()
	if date<financial_year_start_date:
		# return 'April, '+ str(financial_year_start_date.year-1)+' to March, '+ str(financial_year_start_date.year)
		return str(financial_year_start_date.year-1)+'-'+str(financial_year_start_date.year)
	else:
		# return 'April, '+ str(financial_year_start_date.year)+' to March, '+ str(financial_year_start_date.year+1)
		return str(financial_year_start_date.year)+'-'+str(financial_year_start_date.year+1)

def matchDict(arnNo,panNo,schemeCode,transactionDate,folioNo,line,sellTypeTransactionList):
	if any(c in line for c in sellTypeTransactionList[1]):
		return sellTypeTransactionList[0]
	return ''   

def dateFormatStandardisation(df,column_name,data_source):
	### dateFormatStandardisation
	log.info(data_source+' Date Format Standardisation')
	try:

		#log.info(df[column_name])
		if (df[column_name].dtype=='float64'):
			df[column_name]=pd.to_datetime('1899-12-30') + pd.to_timedelta(df[column_name],'D')

		df[column_name] = df[column_name].fillna('')
		df[column_name] = df[column_name].astype(str)
		df[column_name] = df[column_name].str.lower()
		df[column_name] = df[column_name].str.strip()
		#df[column_name] = df[column_name].astype('datetime64')

		##df[column_name] = df[column_name].apply(lambda x: convert8LenDate(x) if len(x)=='8' else x )

		#log.info(df[column_name])
		#### Change date format to yyyy-mm-dd
		df[column_name] = pd.to_datetime(df[column_name], errors='coerce').dt.strftime('%Y-%m-%d').replace('NaT', '')
		
		#log.info(df[column_name])

		return df

	except Exception as ex:
		log.error('Error in '+data_source+' '+column_name+' dateFormatStandardisation: '+traceback.format_exc())
		df = []
		return df

def generate_referral_code (keyword,size=4, chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
	random_character=''.join(random.choice(chars) for _ in range(size))
	return keyword+random_character

def get_rid(client_id,financialYear):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance,FY=financialYear).exists():
		Return_Details_instance=Return_Details.objects.get(P_id=personal_instance,FY=financialYear)
		R_ID = Return_Details_instance.R_id
	else:
		log.info('R_ID Entry not exists')
	return R_ID

def insert8table(R_ID,Total_Sell_Value,Cost_of_Acquisition,capitalGain,totalFMV,financialYear,assetClass,CGType):
	if R_ID>0:
		if assetClass=='Share' and CGType=='LT':
			if not Shares_LT.objects.filter(R_Id=R_ID).exists():
				Shares_LT.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
					totalFMV=totalFMV
				).save()
			else:
				Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
				Shares_LT_instance.Total_Sell_Value=Total_Sell_Value
				Shares_LT_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Shares_LT_instance.Expenditure=capitalGain
				Shares_LT_instance.totalFMV=totalFMV
				Shares_LT_instance.save()

		elif assetClass=='Share' and CGType=='ST':
			if not Shares_ST.objects.filter(R_Id=R_ID).exists():
				Shares_ST.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
				).save()
			else:
				Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
				Shares_ST_instance.Total_Sell_Value=Total_Sell_Value
				Shares_ST_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Shares_ST_instance.Expenditure=capitalGain
				Shares_ST_instance.save()		

		elif assetClass=='Equity' and CGType=='LT':
			if not Equity_LT.objects.filter(R_Id=R_ID).exists():
				Equity_LT.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
					totalFMV=totalFMV
				).save()
			else:
				Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
				Equity_LT_instance.Total_Sell_Value=Total_Sell_Value
				Equity_LT_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Equity_LT_instance.Expenditure=capitalGain
				Equity_LT_instance.totalFMV=totalFMV
				Equity_LT_instance.save()

		elif assetClass=='Equity' and CGType=='ST':
			if not Equity_ST.objects.filter(R_Id=R_ID).exists():
				Equity_ST.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
				).save()
			else:
				Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
				Equity_ST_instance.Total_Sell_Value=Total_Sell_Value
				Equity_ST_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Equity_ST_instance.Expenditure=capitalGain
				Equity_ST_instance.save()

		elif assetClass=='Debt' and CGType=='LT':
			if not Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
				Debt_MF_LT.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
				).save()
			else:
				Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
				Debt_MF_LT_instance.Total_Sell_Value=Total_Sell_Value
				Debt_MF_LT_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Debt_MF_LT_instance.Expenditure=capitalGain
				Debt_MF_LT_instance.save()

		elif assetClass=='Debt' and CGType=='ST':
			if not Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
				Debt_MF_ST.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
				).save()
			else:
				Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
				Debt_MF_ST_instance.Total_Sell_Value=Total_Sell_Value
				Debt_MF_ST_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Debt_MF_ST_instance.Expenditure=capitalGain
				Debt_MF_ST_instance.save()

		elif assetClass=='Debenture' and CGType=='LT':
			if not Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
				Listed_Debentures_LT.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
				).save()
			else:
				Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
				Listed_Debentures_LT_instance.Total_Sell_Value=Total_Sell_Value
				Listed_Debentures_LT_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Listed_Debentures_LT_instance.Expenditure=capitalGain
				Listed_Debentures_LT_instance.save()

		elif assetClass=='Debenture' and CGType=='ST':
			if not Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
				Listed_Debentures_ST.objects.create(
					R_Id = R_ID,
					Total_Sell_Value = Total_Sell_Value,
					Cost_of_Acquisition= Cost_of_Acquisition,
					Expenditure = capitalGain,
				).save()
			else:
				Listed_Debentures_ST_instance = Listed_Debentures_ST.objects.get(R_Id=R_ID)
				Listed_Debentures_ST_instance.Total_Sell_Value=Total_Sell_Value
				Listed_Debentures_ST_instance.Cost_of_Acquisition=Cost_of_Acquisition
				Listed_Debentures_ST_instance.Expenditure=capitalGain
				Listed_Debentures_ST_instance.save()						

def capitalGainCalculation(businessPartnerId,clientId):
	log.info(clientId)
	log.info(businessPartnerId)
	businessPartnerId=Business_Partner.objects.filter(id=businessPartnerId).latest('id')
	#clientId=SalatTaxUser.objects.filter(id=158).latest('id')
	clientId=SalatTaxUser.objects.filter(id=int(clientId)).latest('id')
	result={}
	i=0
	try:
		if CGTransactions.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId).exists():
			CGTransactions_dataaa = CGTransactions.objects.values_list('folioNo',flat=True).distinct().filter(businessPartnerId=businessPartnerId,clientId=clientId)
			log.info(CGTransactions_dataaa)
			for CGTranss in CGTransactions_dataaa:
				log.info(CGTranss)

				if CGTransactions.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,folioNo = CGTranss).exists():
					CGTransactions_dataa = CGTransactions.objects.values_list('scripSchemeCode',flat=True).distinct().filter(businessPartnerId=businessPartnerId,clientId=clientId,folioNo = CGTranss)
					log.info(CGTransactions_dataa)
					for CGTrans in CGTransactions_dataa:
						log.info(CGTrans)
						cgAmount=0
						if CGTransactions.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,scripSchemeCode = CGTrans,folioNo = CGTranss).exists():
							CGTransactions_data = CGTransactions.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,scripSchemeCode = CGTrans,folioNo = CGTranss).order_by('id').values()
							log.info('Folio'+str(CGTrans))
							CGDF = pd.DataFrame(list(CGTransactions_data))
							CGDF.insert(1, 'redeemed_unit', '')
							CGDF.insert(1, 'redeemed_unit_flg', '')
							CGDF.insert(1, 'cgAmount', '')
							CGDF['qtyUnits']=CGDF['qtyUnits'].apply(lambda x: x.replace("(", "").replace(")", "") if '(' in x else x)
							CGDF['qtyUnits']=CGDF['qtyUnits'].apply(lambda x: x.replace(",", "") if ',' in x else x)
							CGDF['qtyUnits'].astype(float)
							CGDF['priceNAV']=CGDF['priceNAV'].apply(lambda x: x.replace("(", "").replace(")", "") if '(' in x else x)
							CGDF['priceNAV']=CGDF['priceNAV'].apply(lambda x: x.replace(",", "") if ',' in x else x)
							CGDF['priceNAV'].astype(float)
							CGDF['amount']=CGDF['amount'].apply(lambda x: x.replace("(", "").replace(")", "") if '(' in x else x)
							CGDF['amount']=CGDF['amount'].apply(lambda x: x.replace(",", "") if ',' in x else x)
							CGDF['amount'].astype(float)
							CGDF=dateFormatStandardisation(CGDF,'transactionDate','TTT')
							#CGDFF = pd.DataFrame(CGDF.loc[(CGDF['transactionType']=='Redemption') & ((CGDF['transactionDate'] > '2018-04-01') & (CGDF['transactionDate'] <= '2019-03-31'))])
							CGDFF = pd.DataFrame(CGDF.loc[((CGDF['transactionType']=='Redemption') | (CGDF['transactionType']=='Switch Out'))])
							# log.info(CGDFF.index.values[0])
							for j in range(len(CGDFF.index.values)):
								reedeam_index=CGDFF.index[j]
								reedeam_unit=CGDFF.loc[reedeam_index , : ].qtyUnits.replace("(", "").replace(")", "")
								reedeam_nav=CGDFF.loc[reedeam_index , : ].priceNAV.replace("(", "").replace(")", "")
								reedeam_date=CGDFF.loc[reedeam_index , : ].transactionDate
								reedeam_id=int(CGDFF.loc[reedeam_index , : ].id)

								log.info('**********')
								log.info(reedeam_index)
								log.info(reedeam_unit)
								log.info(reedeam_nav)
								log.info('**********')
								
								CGDFFF = pd.DataFrame(CGDF.loc[((CGDF['transactionType']=='Lumpsum Purchase') | (CGDF['transactionType']=='SIP Purchase') | (CGDF['transactionType']=='Dividend Reinvest') | (CGDF['transactionType']=='Switch In')) & (CGDF.index <reedeam_index)])

								oldAmount=0
								newAmount=0
								log.info(CGDFFF['qtyUnits'])
								for i in range(len(CGDFFF.index)):
									log.info('Iterations')
									if float(CGDFFF.qtyUnits[CGDFFF.index[i]])<=float(reedeam_unit) and CGDFFF['redeemed_unit_flg'][CGDFFF.index[i]]=='':
										log.info('iiiiiiiii')
										log.info(CGDFFF['transactionDate'])
										log.info(CGDFF['transactionDate'])
										log.info(float(CGDFFF.qtyUnits[CGDFFF.index[i]]))
										CGDFFF['redeemed_unit'][CGDFFF.index[i]]=CGDFFF.qtyUnits[CGDFFF.index[i]]
										CGDFFF['redeemed_unit_flg'][CGDFFF.index[i]]='Y'

										CGDF['redeemed_unit'][CGDFFF.index[i]]=CGDFFF.qtyUnits[CGDFFF.index[i]]
										CGDF['redeemed_unit_flg'][CGDFFF.index[i]]='Y'

										oldAmount=float(CGDFFF.qtyUnits[CGDFFF.index[i]])*float(CGDFFF.priceNAV[CGDFFF.index[i]])
										newAmount=float(CGDFFF.qtyUnits[CGDFFF.index[i]])*float(reedeam_nav)
										log.info(oldAmount)
										log.info(newAmount)
										log.info(newAmount-oldAmount)
										cgAmount=cgAmount+(newAmount-oldAmount)
										reedeam_unit=float(reedeam_unit)-float(CGDFFF.qtyUnits[CGDFFF.index[i]])
										log.info(reedeam_unit)

										#Calculate sellDate-buyDate
										buyDate=datetime.strptime(str(CGDFFF.transactionDate[CGDFFF.index[i]]),'%Y-%m-%d')
										sellDate=datetime.strptime(str(reedeam_date),'%Y-%m-%d')
										diffrance=sellDate-buyDate
										log.info(diffrance.days)

										CGType='0'
										costOfAquisition=0.0
										FMV=0.0
										if CGDFFF.assetClass[CGDFFF.index[i]]=='Equity' or CGDFFF.assetClass[CGDFFF.index[i]]=='Shares':
											if HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).exists():
												highestPrice31012018Data=HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).latest('id')
												FMV=highestPrice31012018Data.highestPrice
											if diffrance.days < 365 and sellDate != buyDate:
												CGType='ST'
												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
											else:
												CGType='LT'
												costOfAquisition=max(float(CGDFFF.priceNAV[CGDFFF.index[i]]),min(FMV,reedeam_nav))
										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debt':
											if diffrance.days < 1095:
												CGType='ST'
												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
											else:
												CGType='LT'
												log.info(get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]]))
												if currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).exists():
													sellCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).latest('id')
													reedeamCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(reedeam_date)).latest('id')
													costOfAquisition=(reedeamCurrentCostInflationIndexData.costInflationIndex*float(CGDFFF.amount[CGDFFF.index[i]]))/sellCurrentCostInflationIndexData.costInflationIndex
													log.info(costOfAquisition)
										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debentures':
											if diffrance.days < 365:
												CGType='ST'
											else:
												CGType='LT'                 

										capitalGain  = float(newAmount) - (costOfAquisition * float(CGDFFF.redeemed_unit[CGDFFF.index[i]]))     

										if CGTransactions.objects.filter(id=reedeam_id).exists():
											CGTransactionsData=CGTransactions.objects.filter(id=reedeam_id).latest('id')
											CGCalculation_data = CGCalculation(businessPartnerId=businessPartnerId,intermediaryId=CGDFFF.intermediaryId[CGDFFF.index[i]],clientId=clientId,cgTransactionId=CGTransactionsData,assetClass=CGDFFF.assetClass[CGDFFF.index[i]],scripSchemeCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]],sellDate=reedeam_date,sellQty=CGDFFF.redeemed_unit[CGDFFF.index[i]],sellPriceNAV=reedeam_nav,sellAmount=newAmount,purchaseAmount=oldAmount,purchaseDate=CGDFFF.transactionDate[CGDFFF.index[i]],purchasePriceNAV=float(CGDFFF.priceNAV[CGDFFF.index[i]]),FMV=FMV,costofAcquisition=costOfAquisition,CGType=CGType,capitalGain=capitalGain,financialYear=get_financial_year(reedeam_date))
											CGCalculation_data.save()
									elif float(CGDFFF.qtyUnits[CGDFFF.index[i]])>float(reedeam_unit) and CGDFFF['redeemed_unit_flg'][CGDFFF.index[i]]=='':
										log.info('eeeeeeeee')
										log.info(CGDFFF['transactionDate'])
										log.info(CGDFF['transactionDate'])
										log.info(reedeam_unit)
										log.info(float(CGDFFF.qtyUnits[CGDFFF.index[i]]))
										aa=float(CGDFFF.qtyUnits[CGDFFF.index[i]])-float(reedeam_unit)
										CGDFFF['redeemed_unit'][CGDFFF.index[i]]=reedeam_unit
										CGDFFF['qtyUnits'][CGDFFF.index[i]]=aa

										CGDF['redeemed_unit'][CGDFFF.index[i]]=reedeam_unit
										CGDF['qtyUnits'][CGDFFF.index[i]]=aa

										oldAmount=float(reedeam_unit)*float(float(CGDFFF.priceNAV[CGDFFF.index[i]]))
										newAmount=float(reedeam_unit)*float(float(reedeam_nav))
										log.info(oldAmount)
										log.info(newAmount)
										log.info(newAmount-oldAmount)
										cgAmount=cgAmount+(newAmount-oldAmount)
										reedeam_unit=float(reedeam_unit)-float(CGDFFF.qtyUnits[CGDFFF.index[i]])
										log.info(reedeam_unit)
										log.info('##############')
										log.info(cgAmount)
										CGDFFF['cgAmount'][CGDFFF.index[i]]=cgAmount
										CGDF['cgAmount'][CGDFFF.index[i]]=cgAmount

										#Calculate sellDate-buyDate
										buyDate=datetime.strptime(str(CGDFFF.transactionDate[CGDFFF.index[i]]),'%Y-%m-%d')
										sellDate=datetime.strptime(str(reedeam_date),'%Y-%m-%d')
										diffrance=sellDate-buyDate
										log.info(diffrance.days)

										CGType='0'
										costOfAquisition=0.0
										FMV=0.0
										if CGDFFF.assetClass[CGDFFF.index[i]]=='Equity' or CGDFFF.assetClass[CGDFFF.index[i]]=='Shares':
											if buyDate < datetime.strptime('2018-01-31','%Y-%m-%d'):
												if HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).exists():
													highestPrice31012018Data=HighestPrice31012018.objects.filter(productCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]]).latest('id')
													FMV=highestPrice31012018Data.highestPrice
											if diffrance.days < 365 and sellDate != buyDate:
												CGType='ST'
												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
											else:
												CGType='LT'
												costOfAquisition=max(float(CGDFFF.priceNAV[CGDFFF.index[i]]),min(FMV,reedeam_nav))
										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debt':
											if diffrance.days < 1095:
												CGType='ST'
												costOfAquisition=float(CGDFFF.priceNAV[CGDFFF.index[i]])
											else:
												CGType='LT'
												log.info(get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]]))
												if currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).exists():
													sellCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(CGDFFF.transactionDate[CGDFFF.index[i]])).latest('id')
													reedeamCurrentCostInflationIndexData=currentCostInflationIndex.objects.filter(financialYear=get_financial_year(reedeam_date)).latest('id')
													costOfAquisition=(reedeamCurrentCostInflationIndexData.costInflationIndex*float(CGDFFF.amount[CGDFFF.index[i]]))/sellCurrentCostInflationIndexData.costInflationIndex
													log.info(costOfAquisition)
										elif CGDFFF.assetClass[CGDFFF.index[i]]=='Debentures':
											if diffrance.days < 365:
												CGType='ST'
											else:
												CGType='LT'
												
										capitalGain  = float(newAmount) - (costOfAquisition * float(CGDFFF.redeemed_unit[CGDFFF.index[i]]))

										if CGTransactions.objects.filter(id=reedeam_id).exists():
											CGTransactionsData=CGTransactions.objects.filter(id=reedeam_id).latest('id')
											CGCalculation_data = CGCalculation(businessPartnerId=businessPartnerId,intermediaryId=CGDFFF.intermediaryId[CGDFFF.index[i]],clientId=clientId,cgTransactionId=CGTransactionsData,assetClass=CGDFFF.assetClass[CGDFFF.index[i]],scripSchemeCode=CGDFFF.scripSchemeCode[CGDFFF.index[i]],sellDate=reedeam_date,sellQty=CGDFFF.redeemed_unit[CGDFFF.index[i]],sellPriceNAV=reedeam_nav,sellAmount=newAmount,purchaseAmount=oldAmount,purchaseDate=CGDFFF.transactionDate[CGDFFF.index[i]],purchasePriceNAV=float(CGDFFF.priceNAV[CGDFFF.index[i]]),FMV=FMV,costofAcquisition=costOfAquisition,CGType=CGType,capitalGain=capitalGain,financialYear=get_financial_year(reedeam_date))
											CGCalculation_data.save()
										break

							# log.info(CGDFFF['redeemed_unit'])
							# log.info(CGDFFF['redeemed_unit_flg'])
							#log.info(CGDFFF.index.values[0])
							log.info(cgAmount)
							log.info(CGDF['cgAmount'])

			#start CG Summary Table Calculation Block				
			if CGCalculation.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId).exists():
				CGCalculation_dataaa = CGCalculation.objects.values_list('financialYear',flat=True).distinct().filter(businessPartnerId=businessPartnerId,clientId=clientId)
				log.info(CGCalculation_dataaa)
				for CGTranss in CGCalculation_dataaa:
					log.info(CGTranss)

					if CGCalculation.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss).exists():
						CGCalculation_dataa = CGCalculation.objects.values_list('assetClass',flat=True).distinct().filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss)
						log.info(CGCalculation_dataa)
						for CGTrans in CGCalculation_dataa:
							log.info(CGTrans)

							if CGCalculation.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans).exists():
								CGCalculation_dataaaa = CGCalculation.objects.values_list('CGType',flat=True).distinct().filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans)
								log.info(CGCalculation_dataaaa)
								for CGTransss in CGCalculation_dataaaa:
									log.info(CGTransss)
									if CGCalculation.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans,CGType=CGTransss).exists():
										CGCalculation_dataaaaa = CGCalculation.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans,CGType=CGTransss).order_by('id').values()
										totalSellAmount=0
										totalCostofAcquisition=0
										totalCapitalGain=0
										totalFMV=0
										totalPurchaseAmount=0
										for valueArray in CGCalculation_dataaaaa:
											totalSellAmount=totalSellAmount+valueArray['sellAmount']
											totalPurchaseAmount=totalPurchaseAmount+valueArray['purchaseAmount']
											totalCostofAcquisition=totalCostofAcquisition+valueArray['costofAcquisition']
											totalCapitalGain=totalCapitalGain+valueArray['capitalGain']
											totalFMV=totalFMV+valueArray['FMV']

										if CGSummary.objects.filter(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans,CGType=CGTransss).exists():
											CGSummary_data = CGSummary.objects.get(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans,CGType=CGTransss)
											CGSummary_data.totalSellAmount=totalSellAmount
											CGSummary_data.totalCostofAcquisition=totalCostofAcquisition
											CGSummary_data.totalCapitalGain=totalCapitalGain
											CGSummary_data.totalFMV=totalFMV
											CGSummary_data.save()
										else:
											CGSummary_data = CGSummary(businessPartnerId=businessPartnerId,clientId=clientId,financialYear = CGTranss,assetClass=CGTrans,CGType=CGTransss,totalSellAmount=totalSellAmount,totalCostofAcquisition=totalCostofAcquisition,indexedCostofAcquisition=0,totalCapitalGain=totalCapitalGain,totalFMV=totalFMV)
											CGSummary_data.save()
										#totalCostofAcquisition
										insert8table(get_rid(clientId.id,CGTranss),int(totalSellAmount),int(totalPurchaseAmount),int(totalCapitalGain),int(totalFMV),CGTranss,CGTrans,CGTransss)
			#stop CG Summary Table Calculation Block								
			
		else:
			log.info('fffffffff')
		log.info('success')
		return result
	except Exception as ex:
		log.error('Error in Business report View: %s' % traceback.format_exc())
		log.info('fail')
		return 'fail'

@csrf_exempt
def capitalGainReading(request):
	result={}
	wordList={}
	panRegex = r'^[A-Z]{5}[0-9]{4}[A-Z]$'
	dateRegex = r'\d\d-(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{4}'
	folioCount=0
	email=''
	mobile=''
	folioNo=''
	panNo=''
	arnNo=''
	schemeCode=''
	transactionDate=''
	last_pos=''
	transactionType=''
	qtyUnits =''
	priceNAV=''
	amount =''
	globalPanNo=''
	try:
		# Load your PDF
		with open(selenium_path+"/ConsolidatedAccountStatement/"+request.POST.get('file_name')+".pdf", "rb") as f:
			pdf = pdftotext.PDF(f,request.POST.get('password'))
		f.close()
		# Save all text to a txt file.
		with open(selenium_path+"/ConsolidatedAccountStatement/"+request.POST.get('file_name')+".txt", 'w') as f:
			f.write(re.sub(' +', ' ',"\n\n\r".join(pdf).encode('utf-8').strip()))
		f.close()

		f=open(selenium_path+"/ConsolidatedAccountStatement/"+request.POST.get('file_name')+".txt", "r")    
		#lines = list(f)
		#log.info(lines)
		line = f.readline()
		while line:
			tt=line.split(" ")
			if re.match(dateRegex, tt[1]) and tt[2]=='To' and re.match(dateRegex, tt[3]):
				log.info('Report From Date: '+tt[1])
				log.info('Report To Date: '+tt[3])
			elif 'Email Id:' in line:
				temp= line.split(" ")
				email='Email: '.join(s for s in temp if '@' in s)

			elif 'Mobile:' in line:
				temp= line.split(" ")
				mobile='Mobile: '.join(s for s in temp if '+' in s)

			elif 'Folio No:' in line:
				folioCount=folioCount+1
				temp= line.split(" ")
				if '/' in line:
					if temp[3]=='/':
						folioNo=temp[2]+temp[3]+temp[4]
					else:
						folioNo=temp[2]
				else:
					folioNo=temp[1]		
				#log.info(str(folioCount)+' Folio No: '+folioNo)    
				panNo=''.join(s for s in temp if re.match(panRegex, s))
				log.info('panNo: '+panNo)
				if panNo!='':
					globalPanNo=panNo
				ttt=f.readline()
				temp2=ttt.split(" ")    
				if 'CAMSCASWS' in ttt:
					temp2=f.readline().split(" ")
					log.info(temp2)
					temp3=temp2[0].split("-")
					schemeCode=temp3[0]
					log.info('Scheme Code: '+schemeCode)
				else:
					log.info(temp2)
					temp3=temp2[0].split("-")
					schemeCode=temp3[0]
					log.info('Scheme Code: '+schemeCode)
				if ')' in "".join(s for s in temp2 if 'ARN-' in s) :
					log.info(temp2)
					ss="".join(s for s in temp2 if 'ARN-' in s)
					arnNo=ss[:-1]
				else:
					temp2=f.readline().split(" ")
					log.info(temp2)
					if ')' in "".join(s for s in temp2 if 'ARN-' in s) :
						log.info(temp2)
						ss="".join(s for s in temp2 if 'ARN-' in s)
						arnNo=ss[:-2]
					elif ')' in "".join(s for s in temp2 if ')' in s) :
						log.info(temp2)
						ss="".join(s for s in temp2 if ')' in s)
						arnNo='ARN-'+ss[:-2]
					else:
						arnNo='DIRECT'
					log.info('ARN No: '+arnNo)  

				line = f.readline()
				while line: 
					if ('.' or 'Folio No:') and not '***Invalid Redemption' in line:
						if 'Folio No:' in line:
							log.info(line)
							f.seek(last_pos)
							break
						temp= line.split(" ")
						#log.info(temp)
						if re.match(dateRegex, temp[0]):
							transactionDate=temp[0]
							# log.info(line)
							# log.info(temp)
							transactionType=''

							matchArray=filter(lambda n : n if n != None else '',map(lambda kv : matchDict(arnNo,panNo,schemeCode,transactionDate,folioNo,line,kv), mappingDict.iteritems()))
							if len(matchArray)>=1:
								matchValue=matchArray[0]
								if '/' in "".join(s for s in temp if 'ARN-' in s) :
									ss="".join(s for s in temp if 'ARN-' in s)
									ss= ss.split("/")
									arnNo=ss[0]
								elif 'ARN-' in "".join(s for s in temp if 'ARN-' in s) :
									ss="".join(s for s in temp if 'ARN-' in s)
									arnNo=ss                

								if '#'in matchValue:
									qtyUnits = '0'
									priceNAV= '0'
									amount = temp[len(temp)-1]
									transactionType=matchValue.replace('#', '')
								else:   
									qtyUnits = temp[len(temp)-3]
									priceNAV= temp[len(temp)-2]
									amount = temp[len(temp)-4]
									transactionType=matchValue

								if panNo=='':
									panNo=globalPanNo

								assetClasss=''
								if mfSchemeAssetClass.objects.filter(productCode=schemeCode).exists():
									mfSchemeAssetClassData=mfSchemeAssetClass.objects.filter(productCode=schemeCode).latest('id')
									log.info('assetClass')
									assetClasss=mfSchemeAssetClassData.taxType
								else:
									assetClasss=''

								log.info(request.POST.get('bpid'));
								log.info(panNo);
								log.info(panNo);    

								if not User.objects.filter(username=panNo).exists():
									User.objects.create_user(panNo, email, panNo)

								if not SalatTaxUser.objects.filter(user=User.objects.filter(username=panNo).latest('id')).exists():
									while(True):
										referral_code=generate_referral_code('C')
										log.info(referral_code)
										if not SalatTaxUser.objects.filter(referral_code=referral_code).exists():
											break

									atuser = User.objects.get(username=panNo)
									if not SalatTaxUser.objects.filter(user=atuser).exists():
										SalatTaxUser.objects.create(
											user = atuser,
											email = email,
											referral_code=referral_code
										).save()

									salattaxUser = SalatTaxUser.objects.get(user=atuser)
									role_instance = SalatTaxRole.objects.get(rolename='Client')
									salattaxUser.role = role_instance
									salattaxUser.save()

								SalatTaxUserObj= SalatTaxUser.objects.filter(user=User.objects.get(username=panNo)).latest('id')
								client_id = SalatTaxUserObj.id
								if not ( Personal_Details.objects.filter(P_Id=client_id).exists() ):
									Personal_Details.objects.create(
										P_Id=client_id,
										business_partner_id=Business_Partner.objects.filter(id=request.POST.get('bpid')).latest('id'),
										pan=panNo,
									).save()    

								clientId=SalatTaxUser.objects.filter(user=User.objects.filter(username=panNo).latest('id')).latest('id')
								if not CGTransactions.objects.filter(businessPartnerId=Business_Partner.objects.filter(id=request.POST.get('bpid')).latest('id'),intermediaryId= arnNo,clientId = clientId,assetClass = assetClasss,scripSchemeCode = schemeCode,transactionType = transactionType,transactionDate = transactionDate,qtyUnits = qtyUnits,priceNAV= priceNAV,amount = amount,folioNo = folioNo).exists():    
									CGTransactions_data = CGTransactions(businessPartnerId=Business_Partner.objects.filter(id=request.POST.get('bpid')).latest('id'),intermediaryId= arnNo,clientId = clientId,assetClass = assetClasss,scripSchemeCode = schemeCode,transactionType = transactionType,transactionDate = transactionDate,qtyUnits = qtyUnits,priceNAV= priceNAV,amount = amount,transactionCost = '',folioNo = folioNo)
									CGTransactions_data.save()      
												

					last_pos=f.tell()       
					line = f.readline()
			line = f.readline()
		f.close()
		capitalGainCalculation(request.POST.get('bpid'),request.POST.get('cid'))
		return HttpResponse('success', content_type='application/json')
	except Exception as ex:
		log.error('Error in Business report View: %s' % traceback.format_exc())
		return HttpResponse('fail', content_type='application/json')        
