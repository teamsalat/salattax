from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone

import os, sys
from PIL import Image
# Python logging package
import logging
import json
import webdriver
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time
from io import BytesIO
from StringIO import StringIO
import subprocess
from subprocess import Popen, PIPE, STDOUT
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
#from pyvirtualdisplay import Display
import requests
from django.core.urlresolvers import reverse
import os.path
from zipfile import ZipFile
import zipfile
import re
import xlwt
import xlrd
import datetime
from datetime import datetime
from .models import SalatTaxUser,tds_tcs,refunds,tax_paid,salatclient,temp_ITR_value,Personal_Details
from .models import User,Business_Partner,Investment_Details,child_info,Return_Details,ERI_user, salat_registration
import socket
import MySQLdb
import traceback
from django.db.models import Avg, Max, Min, Sum
from dateutil.parser import parse
stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

from .path import *

from .index import create_user_login

def driver_call():
	# caps = DesiredCapabilities.CHROME
	# caps['loggingPrefs'] = {'performance': 'ALL'}
	# driver = webdriver.Chrome(desired_capabilities=caps)
	# sftp://ubuntu@ec2-13-232-156-104.ap-south-1.compute.amazonaws.com/usr/lib/chromium-browser/chromedriver
	# browser.implicitly_wait(30)
	chrome_options = webdriver.ChromeOptions()
	downloadPath = selenium_path+'/zip_26AS'
	chrome_options.add_argument('--headless')
	# chrome_options.add_argument('no-sandbox') # required when running as root user. otherwise you would get no sandbox errors. 
	# chrome_options.add_argument('--disable-gpu')  # Last I checked this was necessary for windows
	# chrome_options.add_argument("--start-maximized")
	prefs = {'download.default_directory' : downloadPath,
	"download.prompt_for_download": False,
	"download.directory_upgrade": True,
	"safebrowsing.enabled": True}
	chrome_options.add_experimental_option('prefs', prefs)
	# log.info('headless maximized')
	driver = webdriver.Chrome(executable_path=chrome_executable_path, chrome_options=chrome_options,
	# driver = webdriver.Chrome(executable_path='/usr/lib/chromium-browser/chromedriver', chrome_options=chrome_options,
	  service_args=['--verbose', '--log-path=/tmp/chromedriver1.log'])

	driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
	params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
	command_result = driver.execute("send_command", params)

	# chromedriver=2.35.528139
	# platform=Linux 4.4.0-1062-aws x86_64

	return driver

def geckodriver_call():
	os.environ['MOZ_HEADLESS'] = '1'
	firefox_capabilities = DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True
	firefox_capabilities['binary'] = '/usr/bin/geckodriver'
	log.info('start geckodriver')
	browser = webdriver.Firefox(executable_path='/usr/bin/geckodriver',capabilities=firefox_capabilities)
	log.info('stop geckodriver')

	return browser

@csrf_exempt
def goto_client_registration(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			# request.session['_old_post'] = request.POST.get('data')
			request.session['_old_post'] = data
			# old_post = request.session.get('_old_post')
			result['status']='success'
			# return HttpResponseRedirect(reverse('client_registration'))
		except Exception as ex:
			result['status']= 'Error in json data: %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_client_registration(request):
	old_post = request.session.get('_old_post')
	return HttpResponse(json.dumps(old_post), content_type='application/json')

@csrf_exempt
def check_registration(request):
	if request.method=='POST':
		result={}
		result['pan_status']= ''
		result['dob_status']= ''
		result['valid_last_name']= ''
		try:
			pan=request.POST.get('pan')
			dob=request.POST.get('dob')
			last_name=request.POST.get('last_name')
			result['pan']=pan
			result['dob']=dob
			result['last_name']=last_name
			# display = Display(visible=0, size=(800, 600))
			# display.start()
			# chromedriver = "/usr/local/bin/chromedriver"
			# os.environ["webdriver.chrome.driver"] = chromedriver
			# driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
			driver = driver_call()
			driver.get("https://www.incometaxindiaefiling.gov.in/home")
			# url = driver.title

			# display.stop()
			check_register = register(driver,pan,dob,last_name)
			result['pan_status']=check_register['pan_registered']
			result['dob_status']= check_register['valid_dob']
			result['valid_last_name']= check_register['valid_last_name']
			# result['status']="Selenium webdriver Version: %s" % (webdriver.__version__)
			result['status']="success"
			# result['url']=url
		except Exception as ex:
			result['status']="Exception %s" % ex
			log.error('check_registration Error : %s' % ex)
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def selenium_add_client(request):
	if request.method=='POST':
		result={}
		result['pan_status']= ''
		result['dob_status']= ''
		try:
			data=request.POST.get('data')
			req_data = json.loads(data)

			pan=req_data['pan']
			dob=req_data['dob']
			result['pan']=pan
			result['dob']=dob

			# chromedriver = "/usr/local/bin/chromedriver"
			# os.environ["webdriver.chrome.driver"] = chromedriver
			# driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
			driver = driver_call()
			add_client_status = add_client(driver,pan,dob)
			result['status']= add_client_status
		except Exception as ex:
			result['status']='Error : %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_otp_salat_client(request):
	if request.method=='POST':
		result={}
		try:
			salat_mail_otp = request.POST.get('salat_mail_otp')
			salat_mobile_otp = request.POST.get('salat_mobile_otp')
			pan = request.POST.get('pan')
			MYDIR = os.path.dirname(__file__)
			filename_mail = MYDIR+'/'+pan+'_salat_mail_otp';
			filename_mobile = MYDIR+'/'+pan+'_salat_mobile_otp';

			file = open(filename_mail+'.txt','w')
			file.write(salat_mail_otp) 
			file.close() 

			file1 = open(filename_mobile+'.txt','w')
			file1.write(salat_mobile_otp) 
			file1.close()

			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def forgot_password(request):
	if request.method=='POST':
		result={}
		try:
			pan = request.POST.get('pan')
			text = forgot_password_get_text(pan)
			
			result['pan']= pan
			result['text']= text
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_otp_efiling_client(request):
	if request.method=='POST':
		result={}
		try:
			efiling_mail_otp = request.POST.get('efiling_mail_otp')
			efiling_mobile_otp = request.POST.get('efiling_mobile_otp')
			pan = request.POST.get('pan')
			MYDIR = os.path.dirname(__file__)
			filename_mail = MYDIR+'/'+pan+'_efiling_mail_otp';
			filename_mobile = MYDIR+'/'+pan+'_efiling_mobile_otp';

			file = open(filename_mail+'.txt','w')
			file.write(efiling_mail_otp) 
			file.close() 

			file1 = open(filename_mobile+'.txt','w')
			file1.write(efiling_mobile_otp) 
			file1.close()

			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def selenium_register_user(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			mail=request.POST.get('mail')
			req_data = json.loads(data)

			# chromedriver = "/usr/local/bin/chromedriver"
			# os.environ["webdriver.chrome.driver"] = chromedriver
			# driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
			driver = driver_call()
			check_register = register_user(driver,req_data,mail)

			result['tab_completed']= check_register['tab_completed']
			result['exceptions']=check_register['exceptions']
			result['pan_status']=check_register['pan_registered']
			result['dob_status']= check_register['valid_dob']
			result['last_name_status']= check_register['valid_last_name']
			result['Residential_status']= check_register['valid_Residential']
			result['mobile_status']= check_register['valid_mobile']
			result['mail_status']= check_register['valid_mail']
			result['city_status']= check_register['valid_city']
			result['pin_status']= check_register['valid_pin']
			result['city_status']= check_register['valid_city']
			result['state']= check_register['state']

			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def register_user_otp(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			mail=request.POST.get('mail')
			req_data = json.loads(data)

			driver = driver_call()
			register_client_status = register_userotp(driver,req_data,mail)
			# result['tab_completed']= register_client_status['tab_completed']
			# result['state']= register_client_status['state']

			result['status']= register_client_status
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_form_26AS(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			req_data = json.loads(data)
			pan = req_data['pan']

			count = 0
			get_user_form26_AS = 'success'

			if tds_tcs.objects.filter(R_Id=pan).exists():
				for ed in tds_tcs.objects.filter(R_Id=pan):
					if(ed.year!=''):
						count = count + 1

			if(count<=0):
				driver = driver_call()
				log.info('START')
				# get_user_form26_AS = get_user_form26AS(driver,req_data)
				driver.close()
			result['status']= get_user_form26_AS
			result['count']= count
		except Exception as ex:
			result['status']='Error get_form_26AS : %s' % ex
			log.error('Error in get_form_26AS: '+traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_captcha_26AS(request):
	if request.method=='POST':
		result={}

		try:
			pan=request.POST.get('pan')
			captcha_26AS=request.POST.get('captcha_26AS')

			MYDIR = os.path.dirname(__file__)
			filename = MYDIR+'/'+pan+'_captcha_26AS'

			file1 = open(filename+'.txt','w')
			file1.write(captcha_26AS) 
			file1.close()

			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def read_captcha_status(request):
	if request.method=='POST':
		result={}

		try:
			pan=request.POST.get('pan')

			MYDIR = os.path.dirname(__file__)
			filename = MYDIR+'/'+pan+'_captcha_status.txt'

			content = ''
			if os.path.exists(filename):
				file = open(filename,'r')
				content = file.read()

			result['content']= content
			result['status']= 'success'
		except Exception as ex:
			result['content']= ''
			result['status']='Error in json data: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['content']= ''
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def refresh_status(request):
	if request.method=='POST':
		result={}

		try:
			pan=request.POST.get('pan')

			MYDIR = os.path.dirname(__file__)
			filename = MYDIR+'/'+pan+'_refresh_status'

			file = open(filename+'.txt','w')
			file.write('YES') 
			file.close()

			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in json data: %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def unzip_txt(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			req_data = json.loads(data)
			pan = req_data['pan']
			dob = req_data['dob']
			password = re.sub('[^A-Za-z0-9]+', '', dob)
			result['status']= 'start'
			result['unzip']= ''
			result['toexcel']= ''

			# unzip_file = "/home/salattax/SalatTax/salattax_app/static/file_26AS/"
			unzip_file = selenium_path+"/zip_26AS/"
			log.info('Zip file list :')
			for file in os.listdir(unzip_file):
				if(file.find('.zip') != -1 and file.find(pan) != -1):
					log.info(file)
					with zipfile.ZipFile(selenium_path+"/zip_26AS/"+file,"r") as zip_ref:
						zip_ref.extractall(path=unzip_file,pwd=password)
			result['unzip']= 'success'

			for file in os.listdir(unzip_file):
				if(file.find('.txt') != -1 and file.find(pan) != -1):
					# Create workbook and worksheet 
					# txt_file = unzip_file+pan+"-2018.txt"
					txt_file = unzip_file+file
					wbk = xlwt.Workbook() 
					sheet = wbk.add_sheet('26AS')
					row = 0  # row counter
					f = open(txt_file)
					for line in f: 
						colCount = 0
						for column in line.split("^"): 
							sheet.write(row, colCount, column)
							colCount += 1
						row += 1

					wbk.save(unzip_file+file.split(".")[0]+'.xls')

			result['toexcel']= 'success'
			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def excelToDB(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			client_id=request.POST.get('client_id')
			req_data = json.loads(data)
			pan = req_data['pan']
			dob = req_data['dob']
			result['status']= 'start'
			result['excel_to_db']= excel_to_db(pan,client_id)
			
			result['status']= 'success'
			# result['client_id']= client_id
		except Exception as ex:
			result['status']='Error : %s' % ex
			log.error('Error in excelToDB: '+traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def check_isSalat(request):
	log.info('check_isSalat')
	if request.method=='POST':
		user_exist=''
		client_id=''
		entry_exist = ''
		# pan1_flag=0;
		# pan1_update_flag=0;
		result={}
		try:
			pan=request.POST.get('pan')
			role=request.POST.get('role')
			email=request.POST.get('email')
			b_id=request.POST.get('b_id')

			log.info(b_id)
			status=0
			# log.info(email)
			entry_exist = 0
			flag = ''
		  
			personal_details_condition={'pan':pan}

			if salatclient.objects.filter(PAN=pan,Is_salat_client=1).exists():
				if Personal_Details.objects.filter(**personal_details_condition).exists():
					business_partner_instance=Personal_Details.objects.get(**personal_details_condition)
					if business_partner_instance.business_partner_id!=None:
						business_partner_id_db=business_partner_instance.business_partner_id.id
					else:
						business_partner_id_db=''

					if int(business_partner_id_db)!=int(b_id):
						status=1
						log.info('PAN already register with other partner')
					else:
						status=0
						log.info('PAN already registered')

				entry_exist = 1

			elif not salatclient.objects.filter(PAN=pan,Is_salat_client=1).exists():
				entry_exist = 1
				user_exist=create_user_login(request,pan,email,pan,'Client','')
				user = User.objects.get(username=pan,is_superuser=0)
				user.password = pan
				user.save()
				log.info('Login Account Created')
				

			atuser = User.objects.get(username=pan,email=email)
			client_id = SalatTaxUser.objects.get(user=atuser,email=email).id
		
			if salatclient.objects.filter(PAN=pan).exclude(ERI_id = None).exists():
				for client in salatclient.objects.filter(PAN=pan).exclude(ERI_id = None):
					flag = client.Is_salat_client

		   
			result['status'] = status
			result['entry_exist'] = entry_exist
			result['flag'] = flag
			result['user_exist']=user_exist
			result['client_id']=client_id
		except Exception as ex:
			result['status']="Error : %s" % traceback.format_exc()
			log.error('Error in check_isSalat : %s' % traceback.format_exc())
		
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		return False

@csrf_exempt
def update_isSalat(request):
	if request.method=='POST':
		# pan1_flag=0;
		# pan1_update_flag=0;
		result={}
		try:
			pan=request.POST.get('pan')
			entry_exist = 0
			## Update
			salatclient_instance=salatclient.objects.get(PAN=pan)
			if not salatclient.objects.filter(PAN=pan).exists():
				salatclient.objects.create(
					PAN=pan,
					Is_salat_client=1
				).save()
			else:
				salatclient_instance.Is_salat_client=1
				salatclient_instance.save();
		   
			result['status'] = "success"
			result['entry_exist'] = entry_exist
		except Exception as ex:
			result['status']="Error : %s" % ex
			log.error('Error in json data: %s' % ex)
		
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		return False

def get_clear_browsing_button(driver):
	"""Find the "CLEAR BROWSING BUTTON" on the Chrome settings page."""
	return driver.find_element_by_css_selector('* /deep/ #clearBrowsingDataConfirm')

def clear_cache(driver, timeout=60):
	"""Clear the cookies and cache for the ChromeDriver instance."""
	# navigate to the settings page
	driver.get('chrome://settings/clearBrowserData')

	screenshot(driver,'cache')
	# wait for the button to appear
	wait = WebDriverWait(driver, timeout)
	wait.until(get_clear_browsing_button)

	# click the button to clear the cache
	get_clear_browsing_button(driver).click()

	# wait for the button to be gone before returning
	wait.until_not(get_clear_browsing_button)

def captcha_text_read(captch_img_path):
	# where jar is running copy tessdata folder and set TESSDATA_PREFIX
	# export TESSDATA_PREFIX=/home/salattax/SalatTax/tessdata
	# echo $TESSDATA_PREFIX
	return_captcha = ''
	p = Popen(['java', '-jar','captcha_arg.jar',captch_img_path], stdout=PIPE, stderr=STDOUT)
	log.info(p.stdout)
	return_captcha = ''
	for line in p.stdout:
		return_captcha = line
	# return subprocess.call(['java', '-jar', '/media/salat/DATA D/Ubuntu_related_file/captcha_arg.jar','/home/salat/Downloads/images_captcha/screenshot.png'])
	return return_captcha

def captch_ref(driver,captch_img_path):
	try:
		captcha_id = driver.find_element_by_id("captchaImg")
		location = captcha_id.location
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width']
		bottom = location['y'] + size['height']
		img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
		img.save(captch_img_path)

		password = driver.find_element_by_id("Login_password")
		password.send_keys("Derivatives4$")

		captcha_text_1 = captcha_text_read(captch_img_path)
		driver.find_element_by_id("Login_captchaCode").send_keys(captcha_text_1)
		
		time.sleep(2)
		screenshot(driver,'captcha')
		try:
			driver.find_element_by_class_name('error')
			error = 1
			return error
		except Exception as ex:
			error = 0
			return error
	except Exception as ex:
		error = 0
		return error

def captch_add_client(driver,captch_img_path):
	driver.find_element_by_class_name('refreshImg').click()
	try:
		driver.find_element_by_id("AddClientDetails_captchaCode").clear()
		captcha_id = driver.find_element_by_id("captchaImg")
		location = captcha_id.location
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width']
		bottom = location['y'] + size['height']
		img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
		img.save(captch_img_path)

		captcha_text_2 = captcha_text_read(captch_img_path)
		driver.find_element_by_id("AddClientDetails_captchaCode").send_keys(captcha_text_2)
		try:
			element = driver.find_elements_by_class_name('error')
			for line in element:
				check_element = line.text
				if (check_element.find('Invalid Code') == 0):
					error = 1
				else:
					error = 0
				if (check_element.find(' PAN is not registered ') >= 0):
					error = 'PAN not registered'
			return error
		except Exception:
			error = 0
			return error
	except Exception:
		error = 0
		return error

def captch_view_form26AS(driver,captch_img_path):
	try:
		driver.find_element_by_id("View26AS_captchaCode").clear()
		captcha_id = driver.find_element_by_id("captchaImg")
		location = captcha_id.location
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width']
		bottom = location['y'] + size['height']
		img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
		img.save(captch_img_path)
		time.sleep(1)

		captcha_text_2 = captcha_text_read(captch_img_path)
		driver.find_element_by_id("View26AS_captchaCode").send_keys(captcha_text_2)
		try:
			element = driver.find_elements_by_class_name('error')
			for line in element:
				check_element = line.text
				if (check_element.find('Invalid Code') == 0):
					error = 1
				else:
					error = 0
				if (check_element.find(' PAN is not registered ') >= 0):
					error = 'PAN not registered'
			return error
		except Exception:
			error = 0
			return error
	except Exception:
		error = 0
		return error

def captch_save_form26AS(driver,captch_img_path):
	try:
		captcha_id = driver.find_element_by_id("captchaImg")
		location = captcha_id.location
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width']
		bottom = location['y'] + size['height']
		img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
		img.save(captch_img_path)
		return 0
	except Exception as e:
		# return 'Error : %s' % e
		error = 1
		return error

def captch_ITR_download(driver,captch_img_path,id):
	try:
		driver.find_element_by_id(id).clear()
		captcha_id = driver.find_element_by_id("captchaImg")
		location = captcha_id.location
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width']
		bottom = location['y'] + size['height']
		img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
		img.save(captch_img_path)

		captcha_text_2 = captcha_text_read(captch_img_path)
		driver.find_element_by_id(id).send_keys(captcha_text_2)
		time.sleep(1)
		try:
			element = driver.find_elements_by_class_name('error')
			for line in element:
				check_element = line.text
				if (check_element.find('Invalid Code') == 0):
					error = 1
				else:
					error = 0
			return error
		except Exception:
			error = 0
			return error
	except Exception:
		error = 0
		return error

def register(driver,pan,user_dob,last_name):
	driver.get("https://www.incometaxindiaefiling.gov.in/home")
	time.sleep(2)
	driver.find_element_by_class_name('ui-icon-closethick').click()
	register_yourself = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section/div/app-register-options/ul/app-register[1]/li/h1/input')[0]
	driver.execute_script("arguments[0].click();", register_yourself)

	select = Select(driver.find_element_by_id('userTypeSel'))
	select.select_by_value('11')

	continue_register = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
	driver.execute_script("arguments[0].click();", continue_register)

	PAN = driver.find_element_by_id("RegistrationInvidualValidation_userProfile_userId")
	surname = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_surName')
	middlename = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_middleName')
	lastname = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_firstName')
	dob = driver.find_element_by_id('dateField')
	
	PAN.send_keys(pan)
	surname.send_keys(last_name)
	middlename.send_keys("shivdas")
	lastname.send_keys("swapnil")
	dob.send_keys(user_dob)
	residential_status = driver.find_elements_by_xpath('//*[@id="contact_nriFlagN"]')[0]
	driver.execute_script("arguments[0].click();", residential_status)

	continue_basic = driver.find_elements_by_xpath('//*[@id="continue"]')[0]
	driver.execute_script("arguments[0].click();", continue_basic)
	result={}

	pan_registered = 'NO'
	valid_dob = 'Invalid'
	valid_last_name = 'check'
	error = 0
	try:
		time.sleep(1)
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
			if (check_element.find('Date of Birth') >= 0):
				valid_dob = 'Invalid'
			else:
				valid_dob = 'valid'
			if (check_element.find('PAN') >= 0):
				pan_registered = check_element
			else:
				pan_registered = 'NO'
			if (check_element.find('Status') >= 0):
				pan_registered = check_element
				valid_dob = check_element
			if (check_element.find('Surname') >= 0):
				valid_last_name = 'Invalid'
		error = 1
	except Exception:
		error = 0

	result['pan_registered'] = pan_registered
	result['valid_dob'] = valid_dob
	result['valid_last_name'] = valid_last_name
	driver.close()
	log.info(pan_registered)
	return result

def screenshot(driver,f_name):
	captch_img_path1 = selenium_path+'/images_captcha/'
	id = driver.find_element_by_tag_name("body")
	location = id.location
	size = id.size
	img = driver.get_screenshot_as_png()
	img = Image.open(StringIO(img))
	left = location['x']
	top = location['y']
	right = location['x'] + size['width']
	bottom = location['y'] + size['height']
	img = img.crop((int(left), int(top), int(right), int(bottom)))
	img.save(captch_img_path1+f_name+'.png')

def login(driver):
	# captch_img_path = '/home/salat/Downloads/images_captcha/screenshot.png'
	captch_img_path = selenium_path+'/images_captcha/screenshot.png'
	driver.get("https://www.incometaxindiaefiling.gov.in/home")

	# close popup
	try:
		time.sleep(1)
		driver.find_element_by_class_name('ui-icon-closethick').click()
	except Exception as e:
		error = 0
	
	# Click login Here
	time.sleep(1)
	login_here = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section/div/app-register-options/ul/app-register[2]/li/h1/input')[0]
	driver.execute_script("arguments[0].click();", login_here)
	
	# Enter Username And Password
	# log.info(driver.title)
	time.sleep(2)
	username = driver.find_element_by_id("Login_userName")
	password = driver.find_element_by_id("Login_password")

	username.send_keys("ERIA101868")
	password.send_keys("Derivatives4$")
	
	# screenshot(driver,'l1')
	# captcha screenshot
	captcha_id = driver.find_element_by_id("captchaImg")
	location = captcha_id.location
	size = captcha_id.size
	img = driver.get_screenshot_as_png()
	img = Image.open(StringIO(img))
	left = location['x']
	top = location['y']
	right = location['x'] + size['width']
	bottom = location['y'] + size['height']
	img = img.crop((int(left)-1, int(top)-1, int(right)-1, int(bottom)-1))
	img.save(captch_img_path)
	time.sleep(1)

	# captcha check and Login button
	error = 0
	already_login = 0

	captcha_code = driver.find_elements_by_xpath('//*[@id="button1"]')[0]
	# captcha_text = driver.find_element_by_id("Login_captchaCode")
	captcha_text_1 = captcha_text_read(captch_img_path)
	driver.find_element_by_id("Login_captchaCode").send_keys(captcha_text_1)
	# driver.execute_script("arguments[0].click();", captcha_code)
	log.info('while loop for captcha')
	try:
		driver.find_element_by_class_name('error')
		error = 1
	except Exception:
		error = 0

	error_captcha = 1
	error_captcha_count = 1
	while error_captcha == 1:
		screenshot(driver,'captcha'+str(error_captcha_count))
		# error_captcha_count += 1
		# time.sleep(1)
		error_captcha = captch_ref(driver,captch_img_path)
		# if error_captcha!=1:
		# 	captcha_code = driver.find_elements_by_xpath('//*[@id="button1"]')[0]
		# 	driver.execute_script("arguments[0].click();", captcha_code)
		# log.info(error_captcha)

	time.sleep(2)
	log.info('login successfully')
	screenshot(driver,'l3')
	try:
		continue_login = driver.find_elements_by_xpath('//*[@id="ForcedLogin"]/table[2]/tbody/tr/td[1]/input')[0]
		driver.execute_script("arguments[0].click();", continue_login)
		already_login = 1
	except Exception:
		already_login = 0
	try:
		continue_login1 = driver.find_elements_by_xpath('//*[@id="formContainerDialog1"]/table/tbody/tr[2]/td/div/input')[0]
		driver.execute_script("arguments[0].click();", continue_login1)
		already_login = 1
	except Exception:
		already_login = 0

	try:
		skip = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_2"]')[0]
		driver.execute_script("arguments[0].click();", skip)
		# log.info('skip')
	except Exception:
		already_login = 0

def login_ITR(driver):
	captch_img_path = selenium_path+'/images_captcha/screenshot.png'
	driver.get("https://www.incometaxindiaefiling.gov.in/home")

	# close popup
	try:
		driver.find_element_by_class_name('ui-icon-closethick').click()
	except Exception:
		error = 0
	# screenshot(driver,'f_name1')
	# login_here = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section/div/app-register-options/ul/app-register[2]/li/h1/input')[0]
	# driver.execute_script("arguments[0].click();", login_here)

	login_here = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section[1]/div/app-register-options/ul/app-register[1]/li/h1/input')[0]
	driver.execute_script("arguments[0].click();", login_here)
	
	login_here = driver.find_elements_by_xpath('//*[@id="header"]/div[1]/div[2]/div/div/a[1]')[0]
	login_here.click()

	time.sleep(1)
	# screenshot(driver,'f_name')
	# log.info('test error')
	# Enter Username And Password
	username = driver.find_element_by_id("Login_userName")
	password = driver.find_element_by_id("Login_password")

	username.send_keys("ERIA101868")
	password.send_keys("Derivatives4$")

	# captcha screenshot
	captcha_id = driver.find_element_by_id("captchaImg")
	location = captcha_id.location
	size = captcha_id.size
	img = driver.get_screenshot_as_png()
	img = Image.open(StringIO(img))
	left = location['x']
	top = location['y']
	right = location['x'] + size['width']
	bottom = location['y'] + size['height']
	img = img.crop((int(left)-1, int(top)-1, int(right)-1, int(bottom)-1))
	img.save(captch_img_path)

	error = 0
	already_login = 0

	captcha_code = driver.find_elements_by_xpath('//*[@id="button1"]')[0]
	captcha_text_1 = captcha_text_read(captch_img_path)
	driver.find_element_by_id("Login_captchaCode").send_keys(captcha_text_1)
	# log.info('test error')
	try:
		driver.find_element_by_xpath('/html/body/div/div/table/tbody/tr[1]/td[2]/strong')
		return 2
	except Exception:
		error = 0

	# log.info('test error')
	try:
		driver.find_element_by_class_name('error')
		error = 1
	except Exception:
		error = 0

	while error == 1:
		error = captch_ref(driver,captch_img_path)

	if error == 1:
		driver.find_element_by_id("Login_captchaCode").clear()
		captch_ref(driver,captch_img_path)
	else:
		error = 0

	try:
		time.sleep(1)
		continue_login = driver.find_elements_by_xpath('//*[@id="ForcedLogin"]/table[2]/tbody/tr/td[1]/input')[0]
		driver.execute_script("arguments[0].click();", continue_login)
		already_login = 1
	except Exception:
		already_login = 0
	try:
		continue_login1 = driver.find_elements_by_xpath('//*[@id="formContainerDialog1"]/table/tbody/tr[2]/td/div/input')[0]
		driver.execute_script("arguments[0].click();", continue_login1)
		already_login = 1
	except Exception:
		already_login = 0

	try:
		skip = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_2"]')[0]
		driver.execute_script("arguments[0].click();", skip)
	except Exception:
		already_login = 0

	time.sleep(1)
	log.info('Login Done')
	return 0

def get_timeout(driver):
	# s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	# s.settimeout(120)
	# socket.settimeout(60)

	# driver = webdriver.Chrome(chromedriver)
	driver.get("https://www.incometaxindiaefiling.gov.in/home")
	# log.info(driver.title)

	time.sleep(1)
	driver.find_element_by_class_name('ui-icon-closethick').click()

	for x in xrange(0,100):
		try:
			# driver.implicitly_wait(5)
			time.sleep(1)
			log.info(x)
			log.info(datetime.datetime.now())
		except Exception as ex:
			log.info("Exception :" + str(ex))

def check_download(driver):
	driver.get("https://github.com/mozilla/geckodriver/")
	# log.info(driver.title)

	time.sleep(1)
	clone = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/div[4]/details/summary')[0]
	driver.execute_script("arguments[0].click();", clone)

	time.sleep(5)
	download = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]/div[4]/details/div/div/div[1]/div[2]/a')[0]
	driver.execute_script("arguments[0].click();", download)
	# /html/body/div[4]/div/div/div[2]/div[1]

	captcha_id = driver.find_elements_by_xpath('/html/body/div[4]/div/div/div[2]/div[1]')[0]
	# captcha_id = driver.find_element_by_id("js-repo-pjax-container")
	location = captcha_id.location
	size = captcha_id.size
	img = driver.get_screenshot_as_png()
	img = Image.open(StringIO(img))
	left = location['x']
	top = location['y']
	right = location['x'] + size['width']
	bottom = location['y'] + size['height']
	img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
	img.save(selenium_path+'/images_captcha/d1.png')
	#
	time.sleep(15)
	return 'success1'

def add_client(driver,pan,user_dob):
	log.info('start add_client')
	captch_img_path = selenium_path+'/images_captcha/add_client_img.png'
	login_status = 2
	while login_status == 2:
		login_status = login_ITR(driver)

	manage_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[5]/p/a')[0]
	hover = ActionChains(driver).move_to_element(manage_client)
	hover.perform()

	time.sleep(2)
	# screenshot(driver,'ac1')
	add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[5]/div/dl/dt/a')[0]
	driver.execute_script("arguments[0].click();", add_client)
	# Enter PAN And DOB
	PAN = driver.find_element_by_id("AddClientDetails_eriClient_id_panNo")
	DOB = driver.find_element_by_id("dateField")

	# Assuming PAN AND DOB are valid
	# PAN.send_keys("DAVPP6576C")
	# DOB.send_keys("23-06-1992")
	PAN.send_keys(pan)
	DOB.send_keys(user_dob)

	try:
		time.sleep(1)
		# driver.find_element_by_class_name('error')
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
			if (check_element.find(' PAN is already ') >= 0):
				driver.close()
				return 'PAN already added'
			if (check_element.find(' PAN is not registered ') >= 0):
				return 'PAN not registered'
			if (check_element.find(' PAN does not exist.') >= 0):
				return 'PAN does not exist'
	except Exception:
		error1 = 0

	# screenshot(driver,'ac2')
	error = 1
	while error == 1:
		time.sleep(1)
		error = captch_add_client(driver,captch_img_path)

	if (error == 'PAN not registered'):
		return 'PAN not registered'

	try:
		time.sleep(1)
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
			if (check_element.find(' PAN is already ') >= 0):
				driver.close()
				return 'PAN already added'
			if (check_element.find(' PAN is not registered ') >= 0):
				return 'PAN not registered'
			if (check_element.find(' PAN does not exist.') >= 0):
				return 'PAN does not exist'
	except Exception:
		error1 = 0

	status = 'fail'
	time.sleep(1)
	# screenshot(driver,'ac3')
	try:
		driver.find_element_by_id('VerifyAddClientDetails_mobilePin')
		status = 'success'
	except Exception:
		status = 'fail'

	MYDIR = os.path.dirname(__file__)
	filename_mail = MYDIR+'/'+pan+'_salat_mail_otp.txt';
	filename_mobile = MYDIR+'/'+pan+'_salat_mobile_otp.txt';

	error_otp = 1
	timeout = time.time() + 60*10   # 5 minutes from now
	while error_otp == 1:
		if time.time() > timeout:
			return 'Enter OTP'
			break
		try:
			file = open(filename_mail,'r') 
			mail_otp = file.read() 
			file2 = open(filename_mobile, 'r') 
			mobile_otp = file2.read()
			driver.find_element_by_id("VerifyAddClientDetails_emailPin").send_keys(mail_otp)
			driver.find_element_by_id("VerifyAddClientDetails_mobilePin").send_keys(mobile_otp)
			error_otp = 0
		except Exception:
			error_otp = 1

	# screenshot(driver,'ac4')
	if (error_otp == 0):
		try:
			# WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="VerifyAddClientDetails_mobilePin"] and text() != ""]')))
			# WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="VerifyAddClientDetails_emailPin"] and text() != ""]')))
			submit_otp = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
			driver.execute_script("arguments[0].click();", submit_otp)
			try:
				time.sleep(5)
				otp_error = driver.find_elements_by_xpath('//*[@id="VerifyAddClientDetails"]/table/tbody/tr[3]/td/div[1]')[0]
				# driver.close()
				return 'Invalid Mail OTP'
			except Exception:
				return 'success'
			try:
				time.sleep(5)
				otp_error = driver.find_elements_by_xpath('//*[@id="VerifyAddClientDetails"]/table/tbody/tr[2]/td/div[2]')[0]
				# driver.close()
				return 'Invalid Mobile OTP'
			except Exception:
				return 'success'
		except TimeoutException:
			# driver.close()
			return 'fail'
			# raise Exception('Unable to find text in this element after waiting 10 seconds')
		try:
			transaction_id = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div')[0]
			return 'Client Added'
		except TimeoutException:
			# driver.close()
			return 'fail'
	else:
		driver.close()
		return 'fail'

	# screenshot(driver,'ac5')
	time.sleep(1)
	driver.close()
	return 0

def forgot_password_get_text(pan):
	log.info('forgot_password_get_text')
	mobile_txt = ''
	mail_txt = ''
	driver_t = driver_call()
	captch_img_path = selenium_path+'/images_captcha/download_ITR.png'

	driver_t.get("https://www.incometaxindiaefiling.gov.in/home")

	try:
		driver_t.find_element_by_class_name('ui-icon-closethick').click()
	except Exception:
		error = 0

	# click login
	# click forgot password - Reset (Updated)
	time.sleep(1)
	# screenshot(driver_t,'get_text1')
	try:
		login_here = driver_t.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section[1]/div/app-register-options/ul/app-register[3]/li/h1/input')[0]
		login_here.click()
	except Exception as ex:
		log.info('Error in Reset password : %s' %ex)

	log.info('forgot_password ResetPassword')
	time.sleep(1)
	# screenshot(driver_t,'get_text2')

	try:
		user_id = driver_t.find_element_by_id("ResetPassword_accountNumber")
		user_id.send_keys(pan)

		error = 1
		log.info('Forgot Password WHILE LOOP ...')
		while error == 1:
			error = captch_ITR_download(driver_t,captch_img_path,'ResetPassword_captchaCode')

		time.sleep(1)
		select_op = Select(driver_t.find_element_by_id("ResetPwdOption"))
		select_op.select_by_value('OTP')
		
		driver_t.find_element_by_id('UpdateContactDtls_0').click()
		driver_t.find_element_by_id('allUser0').click()

		mail_txt = driver_t.find_element_by_xpath('//*[@id="otpDetails"]/tbody/tr[1]/td[2]').text
		mobile_txt = driver_t.find_element_by_xpath('//*[@id="otpDetails"]/tbody/tr[2]/td[2]').text
	except Exception as ex:
		log.info( 'Error forgot_password_get_text : %s' % ex )
	log.info('Output Text : '+mail_txt+' '+mobile_txt)
	driver_t.close()

	return mail_txt+' '+mobile_txt

def register_user(driver,req_data,mail):
	tab_completed = 0
	pan = req_data['pan1']
	last_name = req_data['pan1_last_name']
	middle_name = req_data['pan1_middle_name']
	first_name = req_data['pan1_first_name']
	user_dob = req_data['pan1_dob']
	mobile = req_data['mobile']
	Residential_status = req_data['Residential_status']
	flat_door = req_data['pan1_flatno']
	locality = req_data['pan1_area']
	pinCode = req_data['pan1_pin']
	city = req_data['pan1_city']

	driver.get("https://www.incometaxindiaefiling.gov.in/home")

	driver.find_element_by_class_name('ui-icon-closethick').click()
	register_yourself = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section/div/app-register-options/ul/app-register[1]/li/h1/input')[0]
	driver.execute_script("arguments[0].click();", register_yourself)

	select = Select(driver.find_element_by_id('userTypeSel'))
	select.select_by_value('11')

	continue_register = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
	driver.execute_script("arguments[0].click();", continue_register)

	PAN = driver.find_element_by_id("RegistrationInvidualValidation_userProfile_userId")
	surname = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_surName')
	middlename = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_middleName')
	lastname = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_firstName')
	dob_element = driver.find_element_by_id('dateField')
	
	PAN.send_keys(pan)
	surname.send_keys(last_name)
	dob_element.send_keys(user_dob)
	if(Residential_status == 'Resident') :
		residential_status = driver.find_elements_by_xpath('//*[@id="contact_nriFlagN"]')[0]
		driver.execute_script("arguments[0].click();", residential_status)
	else:
		residential_status = driver.find_elements_by_xpath('//*[@id="contact_nriFlagY"]')[0]
		driver.execute_script("arguments[0].click();", residential_status)

	time.sleep(1)
	continue_basic = driver.find_elements_by_xpath('//*[@id="continue"]')[0]
	driver.execute_script("arguments[0].click();", continue_basic)
	result={}
	result['tab_completed'] = tab_completed

	pan_registered = 'NO'
	valid_dob = 'check'
	valid_last_name = 'check'
	valid_Residential = 'check'
	error = 1
	result['exceptions'] = error
	try:
		time.sleep(1)
		# driver.find_element_by_class_name('error')
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
			if (check_element.find('Date of Birth') >= 0):
				valid_dob = 'Invalid'
			else:
				valid_dob = 'valid'
			if (check_element.find('PAN') >= 0):
				pan_registered = check_element
			else:
				pan_registered = 'NO'
			if (check_element.find('Status') >= 0):
				pan_registered = check_element
				valid_dob = check_element
			if (check_element.find('Surname') >= 0):
				valid_last_name = 'Invalid'
			else:
				valid_last_name = 'valid'
			if (check_element.find('Residential Status') >= 0):
				valid_Residential = 'Invalid'
			else:
				valid_Residential = 'valid'
		error = 1
	except Exception:
		error = 0

	tab_completed = 1
	result['pan_registered'] = pan_registered
	result['valid_dob'] = valid_dob
	result['valid_last_name'] = valid_last_name
	result['valid_Residential'] = valid_Residential
	result['tab_completed'] = tab_completed

	time.sleep(1)
	valid_pin = 'check'
	valid_city = 'check'
	state = ''
	try:
		driver.find_element_by_id("password").send_keys("pass@123")
		driver.find_element_by_id("passwordConf").send_keys("pass@123")
		select_pri = Select(driver.find_element_by_id('primaryQuestionSec'))
		select_pri.select_by_value('1')
		select_sec = Select(driver.find_element_by_id('secretQuestionSec'))
		select_sec.select_by_value('2')
		driver.find_element_by_id("SecInput1").send_keys("1")
		driver.find_element_by_id("SecInput").send_keys("1")

		driver.find_element_by_id("contactMobNo").send_keys(mobile)
		select_contact = Select(driver.find_element_by_id('RegistrationIndividual_userProfile_userContactDetails_mobNumBlgsTo'))
		select_contact.select_by_value('1')

		driver.find_element_by_id("emailId").send_keys(mail)
		select_mail = Select(driver.find_element_by_id('RegistrationIndividual_userProfile_userContactDetails_emailIdBlgsTo'))
		select_mail.select_by_value('1')

		driver.find_element_by_id("pinCode").send_keys(pinCode)
		time.sleep(2)
		select_mail = Select(driver.find_element_by_id('cur_country'))
		select_mail.select_by_value('91')

		driver.find_element_by_id("doorNumber").send_keys(flat_door)
		driver.find_element_by_id("locality").send_keys(locality)

		select_pin = Select(driver.find_element_by_id("city"))
		city_count = len(select_pin.options)
		try:
			if (city_count==1):
				valid_city = 'Invalid'
			if (city_count > 1):
				valid_pin = 'valid'
				select_pin.select_by_value(city)
			else:
				valid_pin = 'Invalid'
				valid_city = 'check pin'
				# Select(driver.find_element_by_id("city")).select_by_index(2)
		except Exception:
			valid_city = 'Invalid'

		state = driver.find_element_by_id("stateDesc").get_attribute('value')

		error = 1
	except Exception as ex:
		result['exceptions'] = 'ex'
		error = 0

	time.sleep(1)
	continue_2 = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
	driver.execute_script("arguments[0].click();", continue_2)

	valid_mobile = 'check'
	valid_mail = 'check'

	try:
		driver.find_element_by_class_name('error')
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
			if (check_element.find('mail ID') >= 0):
				valid_mail = 'Invalid'
			if (check_element.find('Mobile') >= 0):
				valid_mobile = 'Invalid'
		error = 1
	except Exception:
		error = 0

	tab_completed = 2
	result['valid_mobile'] = valid_mobile
	result['valid_mail'] = valid_mail
	result['valid_city'] = valid_city
	result['valid_pin'] = valid_pin
	result['valid_city'] = valid_city
	result['state'] = state
	result['tab_completed'] = tab_completed

	try:
		time.sleep(1)
		continue_otp = driver.find_elements_by_xpath('//*[@id="confirm"]')[0]
		driver.execute_script("arguments[0].click();", continue_otp)
	except Exception:
		error = 0

	time.sleep(5)
	return result

def register_userotp(driver,req_data,mail):
	tab_completed = 0
	pan = req_data['pan']
	last_name = req_data['last_name']
	middle_name = req_data['middle_name']
	first_name = req_data['first_name']
	user_dob = req_data['dob']
	mobile = req_data['mobile']
	Residential_status = req_data['Residential_status']
	flat_door = req_data['flatno']
	locality = req_data['area']
	pinCode = req_data['pin']
	city = req_data['city']

	driver.get("https://www.incometaxindiaefiling.gov.in/home")

	driver.find_element_by_class_name('ui-icon-closethick').click()
	register_yourself = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section/div/app-register-options/ul/app-register[1]/li/h1/input')[0]
	driver.execute_script("arguments[0].click();", register_yourself)

	select = Select(driver.find_element_by_id('userTypeSel'))
	select.select_by_value('11')

	continue_register = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
	driver.execute_script("arguments[0].click();", continue_register)

	PAN = driver.find_element_by_id("RegistrationInvidualValidation_userProfile_userId")
	surname = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_surName')
	middlename = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_middleName')
	lastname = driver.find_element_by_id('RegistrationInvidualValidation_userProfile_userPersonalDetails_firstName')
	dob_element = driver.find_element_by_id('dateField')
	
	PAN.send_keys(pan)
	surname.send_keys(last_name)
	dob_element.send_keys(user_dob)
	if(Residential_status == 'Resident') :
		residential_status = driver.find_elements_by_xpath('//*[@id="contact_nriFlagN"]')[0]
		driver.execute_script("arguments[0].click();", residential_status)
	else:
		residential_status = driver.find_elements_by_xpath('//*[@id="contact_nriFlagY"]')[0]
		driver.execute_script("arguments[0].click();", residential_status)

	time.sleep(1)
	continue_basic = driver.find_elements_by_xpath('//*[@id="continue"]')[0]
	driver.execute_script("arguments[0].click();", continue_basic)
	result={}
	result['tab_completed'] = tab_completed

	pan_registered = 'NO'
	tab_completed = 1
	result['pan_registered'] = pan_registered
	state = ''
	try:
		driver.find_element_by_id("password").send_keys("pass@123")
		driver.find_element_by_id("passwordConf").send_keys("pass@123")
		select_pri = Select(driver.find_element_by_id('primaryQuestionSec'))
		select_pri.select_by_value('1')
		select_sec = Select(driver.find_element_by_id('secretQuestionSec'))
		select_sec.select_by_value('2')
		driver.find_element_by_id("SecInput1").send_keys("1")
		driver.find_element_by_id("SecInput").send_keys("1")

		driver.find_element_by_id("contactMobNo").send_keys(mobile)
		select_contact = Select(driver.find_element_by_id('RegistrationIndividual_userProfile_userContactDetails_mobNumBlgsTo'))
		select_contact.select_by_value('1')

		driver.find_element_by_id("emailId").send_keys(mail)
		select_mail = Select(driver.find_element_by_id('RegistrationIndividual_userProfile_userContactDetails_emailIdBlgsTo'))
		select_mail.select_by_value('1')

		driver.find_element_by_id("pinCode").send_keys(pinCode)
		time.sleep(2)
		select_mail = Select(driver.find_element_by_id('cur_country'))
		select_mail.select_by_value('91')

		driver.find_element_by_id("doorNumber").send_keys(flat_door)
		driver.find_element_by_id("locality").send_keys(locality)

		select_pin = Select(driver.find_element_by_id("city"))
		city_count = len(select_pin.options)
		try:
			if (city_count > 1):
				select_pin.select_by_value(city)
		except Exception:
			valid_city = 'Invalid'

		state = driver.find_element_by_id("stateDesc").get_attribute('value')

		error = 1
	except Exception as ex:
		result['exceptions'] = 'ex'

	time.sleep(1)
	continue_2 = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
	driver.execute_script("arguments[0].click();", continue_2)

	try:
		driver.find_element_by_class_name('error')
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
		error = 1
	except Exception:
		error = 0

	tab_completed = 2
	# result['state'] = state
	# result['tab_completed'] = tab_completed

	try:
		time.sleep(1)
		continue_otp = driver.find_elements_by_xpath('//*[@id="confirm"]')[0]
		driver.execute_script("arguments[0].click();", continue_otp)
	except Exception:
		error = 0

	MYDIR = os.path.dirname(__file__)
	time.sleep(1)
	filename_mail = MYDIR+'/'+pan+'_efiling_mail_otp.txt';
	filename_mobile = MYDIR+'/'+pan+'_efiling_mobile_otp.txt';

	error_otp = 1
	timeout = time.time() + 60   # 5 minutes from now
	mail_otp = ''
	mobile_otp = ''
	while error_otp == 1:
		if time.time() > timeout:
			return 'Enter OTP'
			break
		try:
			file2 = open(filename_mobile, 'r') 
			mobile_otp = file2.read()
			driver.find_element_by_id("VerifyIndividualRegistration_mobilePin").send_keys(mobile_otp)
			file = open(filename_mail,'r') 
			mail_otp = file.read() 
			driver.find_element_by_id("VerifyIndividualRegistration_emailPin").send_keys(mail_otp)
			error_otp = 0
		except Exception as ex:
			# return 'Error in json data: %s' % ex
			error_otp = 1

	if (error_otp == 0):
		try:
			submit_otp = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
			driver.execute_script("arguments[0].click();", submit_otp)
			try:
				time.sleep(2)
				otp_error = driver.find_elements_by_xpath('//*[@id="VerifyIndividualRegistration"]/table/tbody/tr[5]/td/div[1]')[0]
				# driver.close()
				return 'Invalid Mail OTP'
			except Exception:
				error_otp = 'success'
			try:
				time.sleep(2)
				otp_error = driver.find_elements_by_xpath('//*[@id="VerifyIndividualRegistration"]/table/tbody/tr[6]/td/div[1]')[0]
				return 'Invalid Mobile OTP'
			except Exception:
				error_otp = 'success'
		except TimeoutException:
			return 'fail'
			# raise Exception('Unable to find text in this element after waiting 10 seconds')
		try:
			transaction_id = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/div[3]/div/table/tbody/tr/td/p[1]')[0]
			return 'Client Added'
		except TimeoutException:
			# driver.close()
			return 'fail'
	else:
		# driver.close()
		return 'fail'

	time.sleep(5)
	# return result

def get_user_form26AS(driver,req_data):
	pan = req_data['pan']
	log.info('start 26AS '+pan)
	# captch_img_path = '/home/salat/Downloads/images_captcha/view_form26AS_img.png'
	captch_img_path = selenium_path+'/images_captcha/view_form26AS.png'
	login_status = 2
	while login_status == 2:
		login_status = login_ITR(driver)
	result={}
	time.sleep(1)
	my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
	hover = ActionChains(driver).move_to_element(my_account)
	hover.perform()

	viewform26as = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl/dt/a')[0]
	driver.execute_script("arguments[0].click();", viewform26as)

	PAN = driver.find_element_by_id("View26AS_panNumber")
	# Assuming PAN AND DOB are valid
	PAN.send_keys(pan)
	error = 1
	error_captcha_count = 1
	while error == 1:
		error_captcha_count += 1
		error = captch_view_form26AS(driver,captch_img_path)
		log.info(error)

	time.sleep(1)
	# driver.set_page_load_timeout(10)
	# log.info(driver.title)
	
	try:
		time.sleep(2)
		driver.implicitly_wait(2)
		try:
			# log.info('continue_view')
			continue_view = driver.find_elements_by_xpath('//*[@id="view26AS"]/table/tbody/tr[4]/td/input')[0]
			driver.execute_script("arguments[0].click();", continue_view)
		except Exception as e:
			log.info('continue_view Error : %s' % e)

		time.sleep(2)
		# screenshot(driver,'ac0')
		driver.switch_to_window(driver.window_handles[1])
		time.sleep(1)
		# log.info(driver.title)
	except Exception as e:
		log.info('Error : %s' % e)
		return 'Error : %s' % e

	try:
		time.sleep(2)
		# screenshot(driver,'ac1')
		i_agree = driver.find_element_by_xpath('//*[@id="Details"]')
		i_agree.click()
		log.info('start redirect ... ')
		time.sleep(2)
		# time.sleep(45)
		driver.implicitly_wait(5)
		driver.set_page_load_timeout(5)
		driver.get("https://services.tdscpc.gov.in/serv/tapn/view26AS.xhtml")
		# screenshot(driver,'ac2')
		time.sleep(2)
		# log.info('view_TC ... ')
	except Exception as e:
		log.info('Error : %s' % e)
		return 'Error : %s' % e

	captcha_save = 1
	try:
		time.sleep(2)
		# driver.implicitly_wait(2)
		driver.set_page_load_timeout(2)
		# screenshot(driver,'ac4')
		for year in xrange(2014,2019):
			select_AssessmentYear = Select(driver.find_element_by_id('AssessmentYearDropDown'))
			select_AssessmentYear.select_by_value(str(year))
			driver.implicitly_wait(1)
			select_viewType = Select(driver.find_element_by_id('viewType'))
			select_viewType.select_by_value('Text')
			# log.info(driver.page_source)

			downloadPath = selenium_path+'/zip_26AS'
			driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
			params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
			command_result = driver.execute("send_command", params)
			time.sleep(1)
			driver.implicitly_wait(2)
			# screenshot(driver,'ac7')
			view_download = driver.find_elements_by_xpath('//*[@id="btnSubmit"]')[0]
			driver.execute_script("arguments[0].click();", view_download)
			log.info(str(year)+' 26AS Downloaded. ')
			time.sleep(2)
			driver.implicitly_wait(2)
		log.info('Exit ... ')
		# screenshot(driver,'ac8')
	except Exception as e:
		log.info('Error : %s' % e)
		return 'Error : %s' % e
	
	return 'success'

def check_captcha(filename,driver,status_filename):
	try:
		file = open(filename,'r')
		captcha_txt = file.read()
		driver.find_element_by_id("captcha").send_keys(captcha_txt)
		# time.sleep(1)
		view_download = driver.find_elements_by_xpath('//*[@id="btnSubmit"]')[0]
		driver.execute_script("arguments[0].click();", view_download)
		time.sleep(2)
		error_message = ''
		error_message = driver.find_element_by_id('message').text

		file = open(status_filename+'.txt','w')
		if (error_message == ''):
			file.write('correct')
			return 1
		else:
			file.write('wrong')
		file.close()
		return 0
	except Exception as e:
		file = open(status_filename+'.txt','w')
		file.write('Error : %s' % e)
		file.close()
		return 1

def unique(list1):
	# intilize a null list
	unique_list = []
	# traverse for all elements
	for x in list1:
		# check if exists in unique_list or not
		if x not in unique_list:
			if x.startswith( '1' ):
				unique_list.append(x)
			if x.startswith( '2' ):
				unique_list.append(x)
	
	return unique_list

def get_partA(worksheet,start,end,pan,part,year):
	section_list = []
	for curr_row_tan1 in xrange(start+2,end):
		if worksheet.cell(curr_row_tan1,2).value != '':
			section_list.append(worksheet.cell(curr_row_tan1,2).value)
	section_list = unique(section_list)

	for section in section_list:
		tan1_list = []
		Deductor_Collector_Name = worksheet.cell(start,1).value
		TAN_PAN = worksheet.cell(start,2).value
		no_of_transaction = 0
		amount_paid = 0.0
		tax_deducted = 0.0
		tds_tcs_deposited = 0.0
		DB_entry = 0
		for curr_row_tan1 in xrange(start+2,end):
			if worksheet.cell(curr_row_tan1,2).value == section:
				DB_entry = 1
				no_of_transaction += 1
				amount_paid += float( worksheet.cell(curr_row_tan1,7).value )
				tax_deducted += float( worksheet.cell(curr_row_tan1,8).value )
				tds_tcs_deposited += float( worksheet.cell(curr_row_tan1,9).value )

		if DB_entry == 1:
			# log.info(Deductor_Collector_Name)
			# log.info('part '+part+' saving in DB for '+pan)
			if not tds_tcs.objects.filter(R_Id=pan,part=part,TAN_PAN=TAN_PAN,section=section,year=year).exists():
				log.info('part '+part+' saved in DB for '+pan)
				tds_tcs.objects.create(
					R_Id=pan,
					part=part,
					Deductor_Collector_Name=Deductor_Collector_Name,
					TAN_PAN=TAN_PAN,
					section=section,
					no_of_transaction=no_of_transaction,
					amount_paid=amount_paid,
					tax_deducted=tax_deducted,
					tds_tcs_deposited=tds_tcs_deposited,
					year=year,
				).save()
	return 1

def excel_to_db(pan,r_id):
	excel_file = selenium_path+"/zip_26AS/"
	# for file in os.listdir(excel_file):
	# 	if(file.find('.xls') != -1 and file.find(pan+"-") != -1):
	# 		log.info(file)
	for file in os.listdir(excel_file):
		if(file.find('.xls') != -1 and file.find(pan+"-") != -1):
			log.info(file)
			filename = excel_file+file
			year = file.replace(pan+"-","")
			year = year.replace('.xls',"")
			# filename = selenium_path+"/zip_26AS/AOEPV7343H.xls"
			log.info('year-> '+year+', File Name-> '+file)
			workbook = xlrd.open_workbook(filename)
			worksheet = workbook.sheet_by_name('26AS')

			num_rows = worksheet.nrows - 1
			curr_row = -1

			partA_status, partA1_status, partA2_status, partB_status, partC_status, partD_status = (0,)*6
			partE_status, partF_status, partG_status, part_A_count = (0,)*4

			no_of_TAN, no_of_TAN_partA1, no_of_TAN_partA2, no_of_TAN_partB, partA_start, partA_end = (0,)*6

			partA1_start, partA1_end, partA2_start, partA2_end, partB_start, partB_end , partC_start = (0,)*7
			partC_end, partD_start, partD_end, isunicode, A_no_trans, A1_no_trans, A2_no_trans , B_no_trans = (0,)*8
			while curr_row < num_rows:
				curr_row += 1
				row = worksheet.row(curr_row) # get each row, list type
				if( isinstance(row[1].value, unicode) ):
					isunicode = 1
					row_value = row[1].value.encode('utf-8')
				else:
					isunicode = 0
					row_value = row[1].value

				if ( row[1].value == 'PART A - Details of Tax Deducted at Source'):
					partA_status = 1
					partA_start = curr_row
					row2 = worksheet.row(curr_row+2) # get each row, list type
					if (row2[3].value == '*********** No Transactions Present **********'):
						A_no_trans = 1
				if row[1].value == 'PART A1 - Details of Tax Deducted at Source for 15G / 15H':
					partA_status = 0
					partA1_status = 1
					partA1_start = curr_row
					partA_end = curr_row
					row2 = worksheet.row(curr_row+2) # get each row, list type
					if (row2[3].value == '*********** No Transactions Present **********'):
						A1_no_trans = 1
				if row[1].value == 'PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)' :
					partA1_status = 0
					partA2_status = 1
					partA2_start = curr_row
					partA1_end = curr_row
					row2 = worksheet.row(curr_row+2) # get each row, list type
					if (row2[3].value == '*********** No Transactions Present **********'):
						A2_no_trans = 1
				if row[1].value == 'PART B - Details of Tax Collected at Source' :
					partA2_status = 0
					partB_status = 1
					partB_start = curr_row
					partA2_end = curr_row
					row2 = worksheet.row(curr_row+2) # get each row, list type
					if (row2[3].value == '*********** No Transactions Present **********'):
						B_no_trans = 1
				if row[1].value == 'PART C - Details of Tax Paid (other than TDS or TCS)' :
					partB_status = 0
					partC_status = 1
					partC_start = curr_row
					partB_end = curr_row
				if row[1].value == 'PART D - Details of Paid Refund':
					partC_status = 0
					partD_status = 1
					partD_start = curr_row
					partC_end = curr_row
				if row[1].value == 'PART E - Details of AIR Transaction':
					partD_status = 0
					partD_end = curr_row

				if partA_status == 1:
					part_A_count += 1
					if (row[0].value!='' ):
						if row[0].value!='\n':
							no_of_TAN += 1

				if partA1_status == 1:
					if (row[0].value!=''):
						if row[0].value!='\n':
							no_of_TAN_partA1 += 1

				if partA2_status == 1:
					if (row[0].value!=''):
						if row[0].value!='\n':
							no_of_TAN_partA2 += 1
				
				if partB_status == 1:
					if (row[0].value!=''):
						if row[0].value!='\n':
							no_of_TAN_partB += 1

				if partC_status == 1:
					if (row[0].value==1):
						if row[0].value!='\n':
							partC_start = curr_row

				if partD_status == 1:
					# part_D_count += 1
					if (row[0].value==1):
						if row[0].value!='\n':
							partD_start = curr_row

			log.info('no_of_TAN : '+str(no_of_TAN))
			if A_no_trans != 1:
				if no_of_TAN-1!= 0:
					for i in xrange(1,no_of_TAN):
						start = 0
						end =0
						for x in xrange(partA_start,partA_end):
							nestr = worksheet.cell(x,0).value
							nestr = re.findall(r'\d+', nestr)
							for n in nestr:
								if (str(n) == str(i)):
									start = x
								if (str(n) == str(i+1)):
									end = x
							if (i == no_of_TAN-1):
								end = partA_end
						if end!=0:
							get_partA(worksheet,start,end,pan,'A',year)

			# log.info('no_of_TAN_partA1 : '+str(no_of_TAN_partA1))
			if A1_no_trans != 1:
				if no_of_TAN_partA1-1!= 0:
					for i in xrange(1,no_of_TAN_partA1):
						start = 0
						end =0
						for x in xrange(partA1_start,partA1_end):
							nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
							for n in nestr:
								if (str(n) == str(i)):
									start = x
								if (str(n) == str(i+1)):
									end = x
							if (i == no_of_TAN_partA1-1):
								end = partA1_end
						if end!=0:
							get_partA(worksheet,start,end,pan,'A1',year)

			# log.info('no_of_TAN_partA2 : '+str(no_of_TAN_partA2))
			if A2_no_trans != 1:
				if no_of_TAN_partA2-1!= 0:
					for i in xrange(1,no_of_TAN_partA2):
						start = 0
						end =0
						for x in xrange(partA2_start,partA2_end):
							nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
							for n in nestr:
								if (str(n) == str(i)):
									start = x
								if (str(n) == str(i+1)):
									end = x
							if (i == no_of_TAN_partA2-1):
								end = partA2_end
						get_partA(worksheet,start,end,pan,'A2',year)
			
			# log.info('no_of_TAN_partB : '+str(B_no_trans) )
			if B_no_trans != 1:
				if no_of_TAN_partB-1!= 0:
					for i in xrange(1,no_of_TAN_partB):
						start = 0
						end =0
						for x in xrange(partB_start,partB_end):
							nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
							for n in nestr:
								if (str(n) == str(i)):
									start = x
								if (str(n) == str(i+1)):
									end = x
							if (i == no_of_TAN_partB-1):
								end = partB_end
						get_partA(worksheet,start,end,pan,'B',year)
			
			# log.info('partC_start ->'+str(partC_start)+' '+str(partC_end) )
			if partC_start+2 < partC_end-2:
				for curr_row_partC in xrange(partC_start+2,partC_end-2):
					if worksheet.cell(curr_row_partC,1).value != '':
						major_head = worksheet.cell_value(curr_row_partC,1)
						minor_head = worksheet.cell_value(curr_row_partC,2)
						total_tax = worksheet.cell_value(curr_row_partC,7)
						BSR_code = worksheet.cell_value(curr_row_partC,8)

						try:
							total_tax = int(float(total_tax) )
						except ValueError:
							total_tax = 0
							log.info(total_tax+" Not a int")

						cell = worksheet.cell(curr_row_partC, 9)
						if cell.ctype == xlrd.XL_CELL_DATE:
							date = datetime.datetime(1899, 12, 30)
							get_ = datetime.timedelta(int(cell.value))
							get_col2 = str(date + get_)[:10]
							d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
							get_col = d.strftime('%Y-%m-%d %H:%M')
						else:
							get_col = parse(cell.value)

						deposit_date = get_col
						challan_serial_no = worksheet.cell_value(curr_row_partC,10)
						if not tax_paid.objects.filter(R_Id=r_id,deposit_date=deposit_date).exists():
							tax_paid.objects.create(
								R_Id=r_id,
								major_head=major_head,
								minor_head=minor_head,
								total_tax=total_tax,
								BSR_code=BSR_code,
								deposit_date=deposit_date,
								challan_serial_no=challan_serial_no
							).save()

			# log.info('partD_start ->'+str(partD_start)+' '+str(partD_end) )
			if partD_start+2 < partD_end-2:
				date_container = 0
				amt_container = 0
				int_container = 0
				for curr_row_partD in xrange(partD_start+2,partD_end-2):
					# partD_list = []
					if worksheet.cell(curr_row_partD,1).value != '':
						if(curr_row_partD == partD_start+2):
							for x in xrange(1,num_rows):
								cell_value = worksheet.cell(curr_row_partD-1, x).value.encode('ascii','ignore')
								if(cell_value == ''):
									break
								if(cell_value.find('Date') != -1 ):
									date_container = x
								if(cell_value.find('Amount') != -1 ):
									amt_container = x
								if(cell_value.find('Interest') != -1 ):
									int_container = x
						Assessment_Year = worksheet.cell(curr_row_partD,1).value
						mode = worksheet.cell(curr_row_partD,2).value
						# 3, 4 = amt, int
						amount_of_refund = worksheet.cell(curr_row_partD,amt_container).value
						interest = worksheet.cell(curr_row_partD,int_container).value
						log.info(Assessment_Year)
					
						cell = worksheet.cell(curr_row_partD, date_container)
						if cell.ctype == xlrd.XL_CELL_DATE:
							date = datetime.datetime(1899, 12, 30)
							get_ = datetime.timedelta(int(cell.value))
							get_col2 = str(date + get_)[:10]
							d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
							get_col = d.strftime('%Y-%m-%d %H:%M')
						else:
							get_col = parse(cell.value)

						payment_date = get_col
						if not refunds.objects.filter(R_Id=pan,payment_date=payment_date).exists():
							refunds.objects.create(
								R_Id=pan,
								Assessment_Year=Assessment_Year,
								mode=mode,
								amount_of_refund=amount_of_refund,
								interest=interest,
								payment_date=payment_date,
							).save()

	return 'success'

@csrf_exempt
def download_ITR(request):
	if request.method=='POST':
		result = {}
		try:
			pan = request.POST.get('pan')
			dob = request.POST.get('dob')
			option = request.POST.get('option')
			client_id = request.POST.get('client_id')
			business_partner_id=request.POST.get('business_partner_id')
			download_user_ITR1 = ""
			year = []
			ITR = []
			income_from_salary = []
			no_of_hp = []
			income_from_hp = []
			interest_payable_borrowed_capital = []
			capital_gain = []
			schedule_si_income = []
			income_from_other_src = []
			interest_gross = []
			gross_total_income = []
			deduction = []
			c_80 = []
			d_80 = []
			tta_80 = []
			ccd_1b_80 = []
			ccg_80 = []
			e_80 = []
			ee_80 = []
			g_80 = []
			gg_80 = []
			total_income = []
			schedule_si_tax = []
			total_tax_interest = []
			tax_paid = []
			adv_tax_self_ass_tax=[]
			total_tcs = []
			total_tds_ITR = []
			business_or_profession = []
			count_itr = 0

			business_partner_instance=Business_Partner.objects.filter(id=business_partner_id)
			if not salatclient.objects.filter(PAN=pan).exists():
				salatclient.objects.create( PAN=pan ).save()

			if(option=='calculate'):
				file_arr = os.listdir(selenium_path+"/ITR_excel")
				for file in file_arr:
					if file.find(pan)>=0 and file.find('.pdf')>=0:
						count_itr = count_itr + 1

				if count_itr < 5 :
					driver = driver_call()
					download_user_ITR1 = download_user_ITR(driver,pan,dob)
					driver.close()
				else:
					download_user_ITR1 = 'success'

				d,m,y=dob.split("/")
				pan1_dob=y+'-'+m+'-'+d
				if not Personal_Details.objects.filter(P_Id=client_id).exists():
					Personal_Details.objects.create(
						P_Id=client_id,
						business_partner_id=business_partner_instance,
						pan=pan,
						pan_type='PAN',
						dob=pan1_dob
					).save()
					log.info(pan + " Entry Created")
				else:
					pd_instance=Personal_Details.objects.get(P_Id=client_id)
					pd_instance.pan = pan
					pd_instance.business_partner_id=business_partner_instance
					pd_instance.pan_type='PAN'
					pd_instance.dob=pan1_dob
					pd_instance.save()
					log.info(pan + " Entry Updated")

			if(option=='show'):
				MYDIR = os.path.dirname(__file__)
				pdf_path = selenium_path+"/ITR_excel"
				pdf = ""
				full_output = ''
				file_path = ''
				if temp_ITR_value.objects.filter(PAN=pan).exists():
					for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
						count_itr += 1
				log.info('Pan : '+pan+' count_itr : '+str(count_itr))
				if(count_itr<5):
					for file in os.listdir(pdf_path):
						if(file.find('.pdf') != -1 and file.find(pan) != -1):
							pdf = pdf+" "+file

							jar_path = MYDIR+'/form16/read_itr.jar'
							file_path = selenium_path+"/ITR_excel/"+file

							p = Popen(['java', '-jar',jar_path,file_path], stdout=PIPE, stderr=STDOUT)
							for line in p.stdout:
								# output = output+line
								output = line
								full_output = full_output+line
					
					for file in os.listdir(pdf_path):
						if(file.find('.xls') != -1 and file.find(pan) != -1):
							filename = selenium_path+"/ITR_excel/"+file
							workbook = xlrd.open_workbook(filename)
							worksheet = workbook.sheet_by_name('PDF Data')

							num_rows = worksheet.nrows - 1
							num_col = worksheet.ncols - 1
							curr_row = 0

							while curr_row < num_rows:
								curr_row += 1
								row = worksheet.row(curr_row) # get each row, list type
								if temp_ITR_value.objects.filter(PAN=pan,year=row[0].value).exists():
									temp_ITR_value.objects.filter(PAN=pan,year=row[0].value).delete()
								
								if not temp_ITR_value.objects.filter(PAN=pan,year=row[0].value).exists():
									temp_ITR_value.objects.create(
										PAN=pan,
										year=row[0].value,
										ITR=row[1].value,
										income_from_salary=row[2].value,
										income_from_hp=row[3].value,
										interest_payable_borrowed_capital=row[4].value,
										capital_gain=row[5].value,
										schedule_si_income=row[6].value,
										income_from_other_src=row[7].value,
										interest_gross=row[8].value,
										gross_total_income=row[9].value,
										deduction=row[10].value,
										c_80=row[11].value,
										d_80=row[12].value,
										tta_80=row[13].value,
										ccd_1b_80=row[14].value,
										ccg_80=row[15].value,
										e_80=row[16].value,
										ee_80=row[17].value,
										g_80=row[18].value,
										gg_80=row[19].value,
										total_income=row[20].value,
										schedule_si_tax=row[21].value,
										total_tax_interest=row[22].value,
										tax_paid=row[23].value,
										adv_tax_self_ass_tax=row[24].value,
										total_tcs=row[25].value,
										total_tds_ITR=row[26].value,
										no_of_hp = row[27].value,
										ccd_2_80 = row[28].value,
										business_or_profession = row[29].value
									).save()

					log.info('Start data fetch.')

				count_itr = 0
				for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
					count_itr += 1
					year.append(itr.year)
					ITR.append(itr.ITR)
					income_from_salary.append(itr.income_from_salary)
					no_of_hp.append(itr.no_of_hp)
					income_from_hp.append(itr.income_from_hp)
					interest_payable_borrowed_capital.append(itr.interest_payable_borrowed_capital)
					capital_gain.append(itr.capital_gain)
					schedule_si_income.append(itr.schedule_si_income)
					income_from_other_src.append(itr.income_from_other_src)
					interest_gross.append(itr.interest_gross)
					gross_total_income.append(itr.gross_total_income)
					deduction.append(itr.deduction)
					c_80.append(itr.c_80)
					d_80.append(itr.d_80)
					tta_80.append(itr.tta_80)
					ccd_1b_80.append(itr.ccd_1b_80)
					ccg_80.append(itr.ccg_80)
					e_80.append(itr.e_80)
					ee_80.append(itr.ee_80)
					g_80.append(itr.g_80)
					gg_80.append(itr.gg_80)
					total_income.append(itr.total_income)
					schedule_si_tax.append(itr.schedule_si_tax)
					total_tax_interest.append(itr.total_tax_interest)
					tax_paid.append(itr.tax_paid)
					adv_tax_self_ass_tax.append(itr.adv_tax_self_ass_tax)
					total_tcs.append(itr.total_tcs)
					total_tds_ITR.append(itr.total_tds_ITR)
					business_or_profession.append(itr.business_or_profession)
			
			if(option=='count'):
				MYDIR = os.path.dirname(__file__)
				pdf_path = selenium_path+"/ITR_excel"
				pdf = ""
				full_output = ''
				file_path = ''
				if temp_ITR_value.objects.filter(PAN=pan).exists():
					for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
						count_itr += 1

			# result['output'] = output
			# result['full_output'] = full_output
			result['pan'] = pan
			result['option'] = option
			result['status'] = download_user_ITR1
			result['count_itr'] = count_itr
			result['year'] = year
			result['ITR'] = ITR
			result['income_from_salary'] = income_from_salary
			result['no_of_hp'] = no_of_hp
			result['income_from_hp'] = income_from_hp
			result['interest_payable_borrowed_capital'] = interest_payable_borrowed_capital
			result['capital_gain'] = capital_gain
			result['schedule_si_income'] = schedule_si_income
			result['income_from_other_src'] = income_from_other_src
			result['interest_gross'] = interest_gross
			result['gross_total_income'] = gross_total_income
			result['deduction'] = deduction
			result['c_80'] = c_80
			result['d_80'] = d_80
			result['tta_80'] = tta_80
			result['ccd_1b_80'] = ccd_1b_80
			result['ccg_80'] = ccg_80
			result['e_80'] = e_80
			result['ee_80'] = ee_80
			result['g_80'] = g_80
			result['gg_80'] = gg_80
			result['total_income'] = total_income
			result['schedule_si_tax'] = schedule_si_tax
			result['total_tax_interest'] = total_tax_interest
			result['tax_paid'] = tax_paid
			result['adv_tax_self_ass_tax'] = adv_tax_self_ass_tax
			result['total_tcs'] = total_tcs
			result['total_tds_ITR'] = total_tds_ITR
			result['business_or_profession'] = business_or_profession
		except Exception as ex:
			result['status']='Error download ITR : %s' % ex
			log.error('Error in : '+traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def calculate_salat_tax(request):
	if request.method=='POST':
		result = {}
		try:
			pan = request.POST.get('pan')
			p_age = request.POST.get('p_age')
			invested_share = request.POST.get('invested_share')
			shares_year = request.POST.get('shares_year')

			year = []
			ITR = []
			s_tax = 0
			salat_tax = []
			capital_gain = []
			count_salattax = 0
			for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
				if(itr.year!='2013'):
					year.append(itr.year)
					ITR.append(itr.ITR)
					s_tax = cal_salat_tax(pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
					salat_tax.append(s_tax)
					capital_gain.append(itr.capital_gain)
					count_salattax = count_salattax + 1

			result['pan'] = pan
			result['p_age'] = p_age
			result['invested_share'] = invested_share
			result['year'] = year
			result['ITR'] = ITR
			result['salat_tax'] = salat_tax
			result['capital_gain'] = capital_gain
			result['count_salattax'] = count_salattax
		except Exception as ex:
			result['status']='Error Salat Tax : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def get_test_form26AS(driver,req_data):
	pan = req_data['pan']
	log.info('start 26 AS '+pan)
	# captch_img_path = '/home/salat/Downloads/images_captcha/view_form26AS_img.png'
	captch_img_path = selenium_path+'/images_captcha/view_form26AS.png'
	login(driver)
	log.info('login 26 AS ')
	time.sleep(1)
	my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
	hover = ActionChains(driver).move_to_element(my_account)
	hover.perform()

	screenshot(driver,'ac000')
	viewform26as = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl/dt/a')[0]
	driver.execute_script("arguments[0].click();", viewform26as)

	screenshot(driver,'ac001')
	PAN = driver.find_element_by_id("View26AS_panNumber")
	# Assuming PAN AND DOB are valid
	PAN.send_keys(pan)
	error = 1
	error_captcha_count = 1
	while error == 1:
		screenshot(driver,'c_26AS'+str(error_captcha_count))
		error_captcha_count += 1
		error = captch_view_form26AS(driver,captch_img_path)
		log.info(error)
	
	try:
		driver.implicitly_wait(2)
		main_window = driver.current_window_handle
		continue_view = driver.find_elements_by_xpath('//*[@id="view26AS"]/table/tbody/tr[4]/td/input')[0]
		driver.execute_script("arguments[0].click();", continue_view)
		time.sleep(1)
		screenshot(driver,'ac0')
		# driver.find_element_by_tag_name("body").send_keys(Keys.ALT + Keys.NUMPAD2)
		driver.switch_to_window(driver.window_handles[1])
		time.sleep(1)

		screenshot(driver,'ac00')
		log.info(driver.title)
	except Exception as e:
		log.info('Error : %s' % e)
		return 'Error : %s' % e

	# try:
	# 	# time.sleep(2)
	# 	screenshot(driver,'ac1')
	# 	driver.implicitly_wait(2)
	# 	i_agree = driver.find_element_by_xpath('//*[@id="Details"]')
	# 	i_agree.click()
	# 	# time.sleep(1)
	# 	driver.implicitly_wait(1)
	# 	screenshot(driver,'ac2')
	# 	proceed = driver.find_elements_by_xpath('//*[@id="btn"]')[0]
	# 	driver.execute_script("arguments[0].click();", proceed)
	# 	# time.sleep(1)
	# 	# driver.implicitly_wait(1)
	# 	screenshot(driver,'ac3')
	# 	find_element = 0
	# 	while find_element == 0:
	# 		try:
	# 			driver.implicitly_wait(1)
	# 			screenshot(driver,'ac03')
	# 			view_TC = driver.find_elements_by_xpath('//*[@id="content"]/div[2]/p[2]/a')[0]
	# 			driver.execute_script("arguments[0].click();", view_TC)
	# 			log.info('view_TC ... ')
	# 			find_element = 1
	# 		except Exception as e:
	# 			log.info('Error : %s' % e)
	# 			find_element = 0

	# 	# view_TC = driver.find_elements_by_xpath('//*[@id="content"]/div[2]/p[2]/a')[0]
	# 	# driver.execute_script("arguments[0].click();", view_TC)
	# 	# log.info('view_TC ... ')
	# except Exception as e:
	# 	log.info('Error : %s' % e)
	# 	return 'Error : %s' % e

	# captcha_save = 1
	# try:
	# 	driver.implicitly_wait(1)
	# 	screenshot(driver,'ac4')
	# 	select_AssessmentYear = Select(driver.find_element_by_id('AssessmentYearDropDown'))
	# 	select_AssessmentYear.select_by_value('2018')
	# 	driver.implicitly_wait(1)
	# 	screenshot(driver,'ac5')
	# 	select_viewType = Select(driver.find_element_by_id('viewType'))
	# 	select_viewType.select_by_value('Text')

	# 	time.sleep(1)
	# 	driver.implicitly_wait(1)
	# 	view_download = driver.find_elements_by_xpath('//*[@id="btnSubmit"]')[0]
	# 	driver.execute_script("arguments[0].click();", view_download)
	# 	log.info('Exit ... ')
	# 	screenshot(driver,'ac6')
	# 	time.sleep(1)
	# 	driver.close()
	# except Exception as e:
	# 	log.info('Error : %s' % e)
	# 	return 'Error : %s' % e
	# 	driver.close()
	
	# time.sleep(1)
	driver.close()
	return 'success1'

def download_user_ITR(driver,pan,dob):
	log.info('Start download_user_ITR')
	captch_img_path = selenium_path+'/images_captcha/download_ITR.png'
	login_status = 2
	while login_status == 2:
		login_status = login_ITR(driver)

	my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
	hover = ActionChains(driver).move_to_element(my_account)
	hover.perform()
	time.sleep(1)
	add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl[2]/dt/a')[0]
	driver.execute_script("arguments[0].click();", add_client)
	PAN = driver.find_element_by_id("ViewReturnsItdHdEri_panNo")
	# Assuming PAN IS valid
	PAN.send_keys(pan)
	webdriver.ActionChains(driver).move_to_element(PAN).send_keys(Keys.ENTER).send_keys(Keys.ENTER).perform()
	pan_exist = 1
	no_of_ITR = 0
	try:
		element = driver.find_elements_by_class_name('error')
		for line in element:
			check_element = line.text
			if (check_element.find('This PAN is not added as a Client') == 0):
				pan_exist = 0
	except Exception:
		error = 0

	result={}
	pan_status = ''
	dob_status = ''
	otp_status = ''
	result['status'] = 'pending'

	salatclient_instance=salatclient.objects.get(PAN=pan)
	if pan_exist == 0:
		result['pan_exist'] = 'This PAN is not added as Salat Client.'
	else:
		result['pan_exist'] = 'This PAN is added as Salat Client.'
		salatclient_instance.Is_salat_client=1
		salatclient_instance.Is_ERI_reg=1
		salatclient_instance.save()

	log.info(pan_exist)

	if pan_exist == 1:
		error = 1
		log.info('ITR DOWNLOAD WHILE LOOP ...')
		while error == 1:
			error = captch_ITR_download(driver,captch_img_path,'ViewReturnsItdHdEri_captchaCode')
		log.info('End WHILE LOOP ...')

		try:
			count = driver.find_elements_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr')
			log.info('Count of ITR : '+ str(len(count)))
			if len(count)>1:
				ack_no_row = 0
				for i in range(2, len(count)+1):
					log.info(i)
					link = '//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td[6]/a'
					ITR = ""
					if ack_no_row == 0:
						try:
							for j in range(1,10):
								if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='Ack. No.':
									ack_no_row = j
									time.sleep(1)
						except Exception as e:
							log.info('Exception in ack_no_row')

					try:
						link = '//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(ack_no_row)+']/a'
						ITR = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td[3]').text
						ack_no = driver.find_elements_by_xpath(link)[0]
						driver.execute_script("arguments[0].click();", ack_no)
					except Exception as e:
						error = 1
						try:
							link = '//*[@id="ViewMyReturnsITR"]/table[2]/tbody/tr['+str(i)+']/td['+str(ack_no_row)+']/a'
							ITR = driver.find_element_by_xpath('//*[@id="ViewMyReturnsITR"]/table[2]/tbody/tr['+str(i)+']/td[3]').text
							ack_no = driver.find_elements_by_xpath(link)[0]
							driver.execute_script("arguments[0].click();", ack_no)
						except Exception as e:
							error = 2
					
					try:
						get_pdf = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div[1]/table[2]/tbody/tr[2]/td[3]/a[3]')
						webdriver.ActionChains(driver).move_to_element(get_pdf[0]).send_keys(Keys.ALT).click(get_pdf[0]).send_keys(Keys.ALT).perform()
					except Exception as e:
						error = 1
						try:
							get_pdf = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div[1]/table[2]/tbody/tr[2]/td[3]/a[2]')
							webdriver.ActionChains(driver).move_to_element(get_pdf[0]).send_keys(Keys.ALT).click(get_pdf[0]).send_keys(Keys.ALT).perform()
						except Exception as e:
							error = 2
					
					downloadPath = selenium_path+'/ITR'
					driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
					params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
					command_result = driver.execute("send_command", params)

					time.sleep(1)
					try:
						back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[2]/div[2]/table/tbody/tr/td/div/a/input').click()
					except Exception as e:
						try:
							back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[3]/table/tbody/tr/td/div/a/input').click()
						except Exception as e:
							back = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri_0"]')
					# back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[2]/div[2]/table/tbody/tr/td/div/a/input').click()
					# log.info(ITR)
					
					file_arr = os.listdir(selenium_path+"/ITR")
					for file in file_arr:
						# log.info(file)
						if file.find(pan)>=0:
							my_list = file.split("_")
							rename = my_list[1]+"_"+my_list[2]+"_"+ITR+".pdf"
							old_file = os.path.join(selenium_path+"/ITR", file)
							new_file = os.path.join(selenium_path+"/ITR_excel", rename)
							log.info(rename+' no_of_ITR -> '+str(no_of_ITR))
							log.info("file exist : "+str(os.path.exists(new_file)) )
							no_of_ITR = no_of_ITR + 1
							if(str(os.path.exists(new_file)) == 'False'):
								os.rename(old_file, new_file)
							else:
								os.remove(old_file)
					if no_of_ITR>5:
						time.sleep(2)
						break
			
			result['status'] = 'complete'
		except Exception:
			error = 0

	else:
		manage_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[5]/p/a')[0]
		hover = ActionChains(driver).move_to_element(manage_client)
		hover.perform()

		time.sleep(1)
		add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[5]/div/dl/dt/a')[0]
		driver.execute_script("arguments[0].click();", add_client)
		# Enter PAN And DOB
		PAN = driver.find_element_by_id("AddClientDetails_eriClient_id_panNo")
		DOB = driver.find_element_by_id("dateField")

		# Assuming PAN AND DOB are valid
		PAN.send_keys(pan)
		DOB.send_keys(dob)
		# DOB.send_keys('01/03/1992')
		screenshot(driver,pan)
		error = 1
		while error == 1:
			time.sleep(1)
			error = captch_add_client(driver,captch_img_path)
			element = driver.find_elements_by_class_name('error')
			for line in element:
				check_element = line.text
				# if (check_element.find(' PAN is not registered ') >= 0):
				# 	print 'PAN not registered'
				if (check_element.find('PAN does not exist.') >= 0):
					pan_status = 'PAN does not exist'
					salatclient_instance.Is_salat_client=2
					salatclient_instance.Is_ERI_reg=2
					salatclient_instance.save()
					error = 0
				if (check_element.find('This PAN is not registered with e-Filing.') >= 0):
					pan_status = 'This PAN is not registered with e-Filing.'
					# salatclient_instance.Is_salat_client=0
					# salatclient_instance.Is_ERI_reg=0
					# salatclient_instance.save()
					error = 0
				if (check_element.find('Invalid Date') >= 0):
					dob_status = 'Invalid Date'
					error = 0
		log.info('pan_status'+pan_status)

		if(pan_status != 'PAN does not exist' and dob_status != 'Invalid Date'and pan_status != 'This PAN is not registered with e-Filing.'):
			time.sleep(1)
			try:
				driver.find_element_by_id('VerifyAddClientDetails_mobilePin')
				otp_status = 'success'
				salatclient_instance.Is_salat_client=0
				salatclient_instance.Is_ERI_reg=0
				salatclient_instance.save()
			except Exception:
				otp_status = 'fail'

			MYDIR = os.path.dirname(__file__)
			filename_mail = MYDIR+'/'+pan+'_salat_mail_otp.txt'
			filename_mobile = MYDIR+'/'+pan+'_salat_mobile_otp.txt'

			error_otp = 1
			timeout = time.time() + 60*10   # 5 minutes from now
			while error_otp == 1:
				if time.time() > timeout:
					otp_status = 'Enter OTP'
					break
				try:
					file = open(filename_mail,'r')
					mail_otp = file.read() 
					file2 = open(filename_mobile, 'r')
					mobile_otp = file2.read()
					log.info(mail_otp)
					log.info(mobile_otp)
					driver.find_element_by_id("VerifyAddClientDetails_emailPin").send_keys(mail_otp)
					driver.find_element_by_id("VerifyAddClientDetails_mobilePin").send_keys(mobile_otp)
					error_otp = 0
					os.remove(filename_mail)
					os.remove(filename_mobile)
				except Exception:
					error_otp = 1
					otp_status = 'waiting'
					break

			if (error_otp == 0):
				try:
					submit_otp = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
					driver.execute_script("arguments[0].click();", submit_otp)
					try:
						time.sleep(2)
						otp_error = driver.find_elements_by_xpath('//*[@id="VerifyAddClientDetails"]/table/tbody/tr[3]/td/div[1]')[0]
						otp_status = 'Invalid Mail OTP'
					except Exception:
						otp_status = 'success'
					try:
						time.sleep(2)
						otp_error = driver.find_elements_by_xpath('//*[@id="VerifyAddClientDetails"]/table/tbody/tr[2]/td/div[2]')[0]
						otp_status = 'Invalid Mobile OTP'
					except Exception:
						otp_status = 'success'
				except TimeoutException:
					otp_status = 'fail'
		else:
			salatclient_instance.Is_salat_client=2
			salatclient_instance.Is_ERI_reg=2
			salatclient_instance.save()

		if(otp_status=='success'):
			salatclient_instance.Is_salat_client=1
			salatclient_instance.Is_ERI_reg=1
			salatclient_instance.save()
			result['status'] = 'Successfully added as Salat Client'

	log.info('FINISH ...')

	result['no_of_ITR'] = no_of_ITR
	result['pan_status'] = pan_status
	result['dob_status'] = dob_status
	result['otp_status'] = otp_status
	return result

def cal_salat_tax(pan,year,ITR, p_age, invested_share, shares_year):
	if(p_age=='' or p_age == 'NA'):
		p_age = 0
	# log.info(pan+" "+year+" "+ITR)
	GTI, total_capital_gain, schedule_si_income, income_from_salary, income_from_hp = (0,)*5
	income_from_os, business_or_profession, c_80, d_80, ccg_80, ccd_1b_80, e_80, ee_80, g_80, gg_80, ccd_2_80, tta_80 = (0,)*12
	schedule_si_tax, tax_paid, Tx_add_value, total_tax_interest, refund = (0,)*5

	capital_gain, cal_GTI, TI, Tx, Tx1, rebate, TxR, tax_leakage, SH, TxSH, EC, ToTax = (0,)*12
	result={}
	c_80_limit, d_80_limit, ccd_1b_80_limit, ccg_80_limit = (0,)*4

	for itr in temp_ITR_value.objects.filter(PAN = pan,year=year):
		total_capital_gain = get_int(itr.capital_gain)
		schedule_si_income = get_int(itr.schedule_si_income)
		income_from_salary = get_int(itr.income_from_salary)
		income_from_hp = get_int(itr.income_from_hp)
		income_from_os = get_int(itr.income_from_other_src)
		ccd_1b_80 = get_int(itr.ccd_1b_80)
		ccd_2_80 = get_int(itr.ccd_2_80)
		e_80 = get_int(itr.e_80)
		ee_80 = get_int(itr.ee_80)
		g_80 = get_int(itr.g_80)
		gg_80 = get_int(itr.gg_80)
		tta_80 = get_int(itr.tta_80)
		schedule_si_tax = get_int(itr.schedule_si_tax)
		tax_paid = get_int(itr.tax_paid)
		business_or_profession = get_int(itr.business_or_profession)
		total_tax_interest = get_int(itr.total_tax_interest)

	if(schedule_si_tax=='NA' or schedule_si_tax=='' ):
		schedule_si_tax = 0
	if(total_capital_gain=='NA' or total_capital_gain=='' ):
		total_capital_gain = 0
	if(schedule_si_income=='NA' or schedule_si_income=='' ):
		schedule_si_income = 0
		
	capital_gain = total_capital_gain - schedule_si_income
	cal_GTI = income_from_salary + income_from_hp + capital_gain + income_from_os + business_or_profession

	# for total income chart values only
	temp_income_hp, total_income_chart, temp_capital_gain = (0,)*3
	if(income_from_hp<0):
		temp_income_hp = 0
	else:
		temp_income_hp = income_from_hp
	if(capital_gain<0):
		temp_capital_gain = 0
	else:
		temp_capital_gain = capital_gain

	total_income_chart = income_from_salary + temp_income_hp + temp_capital_gain + income_from_os + business_or_profession

	cess_cal, SH, p_age_cal = (0,)*3
	
	if(cal_GTI<1200000):
		if(invested_share=='no'):
			ccg_80 = 25000
		if(invested_share=='yes' and int(shares_year)>=2014):
			ccg_80 = 25000

	if(year=='2014'):
		slab1_range = 200000
		slab2_range = 500000
		slab3_range = 1000000
		per_slab0 = 0
		per_slab1 = 0.1
		per_slab2 = 0.2
		per_slab3 = 0.3
		Tx_add_value = 30000
		rebate_limit = 500000
		rebate_value = 2000
		cess_cal = 0.03
		c_80 = 100000
		ccd_1b_80 = 0
		p_age_cal = int(p_age) - 4
		if(p_age_cal>60):
			d_80 = 35000
		else:
			d_80 = 30000

	if(year=='2015'):
		slab1_range = 250000
		slab2_range = 500000
		slab3_range = 1000000
		per_slab0 = 0
		per_slab1 = 0.1
		per_slab2 = 0.2
		per_slab3 = 0.3
		Tx_add_value = 25000
		rebate_limit = 500000
		rebate_value = 2000
		cess_cal = 0.03
		c_80 = 150000
		ccd_1b_80 = 0
		p_age_cal = int(p_age) - 3
		if(p_age_cal>60):
			d_80 = 35000
		else:
			d_80 = 30000

	if(year=='2016'):
		slab1_range = 250000
		slab2_range = 500000
		slab3_range = 1000000
		per_slab0 = 0
		per_slab1 = 0.1
		per_slab2 = 0.2
		per_slab3 = 0.3
		Tx_add_value = 25000
		rebate_limit = 500000
		rebate_value = 2000
		cess_cal = 0.03
		c_80 = 150000
		ccd_1b_80 = 50000
		p_age_cal = int(p_age) - 2
		if(p_age_cal>60):
			d_80 = 55000
		else:
			d_80 = 50000

	if(year=='2017'):
		slab1_range = 250000
		slab2_range = 500000
		slab3_range = 1000000
		per_slab0 = 0
		per_slab1 = 0.1
		per_slab2 = 0.2
		per_slab3 = 0.3
		Tx_add_value = 25000
		rebate_limit = 500000
		rebate_value = 5000
		cess_cal = 0.03
		c_80 = 150000
		ccd_1b_80 = 50000
		p_age_cal = int(p_age) - 1
		if(p_age_cal>60):
			d_80 = 55000
		else:
			d_80 = 50000

	if(year=='2018'):
		slab1_range = 250000
		slab2_range = 500000
		slab3_range = 1000000
		per_slab0 = 0
		per_slab1 = 0.05
		per_slab2 = 0.2
		per_slab3 = 0.3
		Tx_add_value = 12500
		rebate_limit = 350000
		rebate_value = 2500
		cess_cal = 0.03
		c_80 = 150000
		ccd_1b_80 = 50000
		p_age_cal = int(p_age)
		if(p_age_cal>60):
			d_80 = 55000
		else:
			d_80 = 50000
		ccg_80 = 0

	TI = cal_GTI - c_80 - d_80 - ccg_80 - ccd_1b_80 - ccd_2_80 - e_80 - ee_80 - g_80 - gg_80 - tta_80
	
	if(TI>slab3_range):
		Tx = ((TI-slab3_range)* per_slab3 ) + 100000 + Tx_add_value
	elif(TI<slab3_range and TI>slab2_range):
		Tx = ((TI-slab2_range)* per_slab2 ) + Tx_add_value
	elif(TI<slab2_range and TI>slab1_range):
		Tx = ((TI-slab1_range)* per_slab1 )
	elif(TI<slab1_range):
		Tx = 0

	# log.info(str(((TI-slab2_range)* per_slab2 ) + Tx_add_value))
	# log.info(str(TI))
	# log.info(str(slab2_range) )
	# log.info(str(per_slab2) )

	if(TI<rebate_limit):
		rebate = rebate_value
	if(Tx + Tx1 <=0):
		rebate = 0

	# Tx1 = schedule_si_income - schedule_si_tax
	Tx1 = schedule_si_tax

	TxR = (Tx + Tx1) - rebate
	if(year=='2014'):
		if(TI>10000000):
			SH = round(TxR * 0.1)
	if(year=='2015'):
		if(TI>10000000):
			SH = round(TxR * 0.1)
	if(year=='2016'):
		if(TI>10000000):
			SH = round(TxR * 0.12)
	if(year=='2017'):
		if(TI>10000000):
			SH = round(TxR * 0.15)
	if(year=='2018'):
		if(TI>5000000 and TI<10000000 ):
			SH = round(TxR * 0.1)
		if( TI>10000000 ):
			SH = round(TxR * 0.15)

	TxSH = TxR + SH
	EC = round(TxSH * cess_cal)
	if(TI<=0):
		TI = 0
		TxSH = 0
		EC = 0
		
	ToTax = round(TxSH + EC)
	refund = total_tax_interest - tax_paid
	if refund<=0:
		tax_paid = tax_paid - abs(refund)
	tax_leakage = round(ToTax - tax_paid)

	result['ITR'] = ITR
	result['capital_gain'] = capital_gain
	result['year'] = year
	result['cal_GTI'] = cal_GTI
	result['TI'] = TI
	result['Tx'] = round(Tx)
	result['Tx1'] = Tx1
	result['rebate'] = rebate
	result['TxR'] = round(TxR)
	result['SH'] = SH
	result['TxSH'] = round(TxSH)
	result['EC'] = EC
	result['ToTax'] = ToTax
	result['tax_paid'] = tax_paid
	result['tax_leakage'] = tax_leakage
	result['p_age_cal'] = p_age_cal
	result['c_80'] = c_80
	result['d_80'] = d_80
	result['ccg_80'] = ccg_80
	result['ccd_1b_80'] = ccd_1b_80
	result['ccd_2_80'] = ccd_2_80
	result['e_80'] = e_80
	result['ee_80'] = ee_80
	result['g_80'] = g_80
	result['gg_80'] = gg_80
	result['tta_80'] = tta_80
	result['total_income_chart'] = total_income_chart
	result['refund'] = refund
	result['total_tax_interest'] = total_tax_interest
	return result

def get_int(value):
	# log.info(value)
	if(value == 'NA' or value == '' ):
		return 0
	elif('.' in value):
		if(value.split('.')[0].isdigit() ):
			return int(value.split('.')[0])
		else:
			return 0
	else:
		return int(value)

@csrf_exempt
def get_id(request,id):
	if SalatTaxUser.objects.filter(id=id).exists():
		salattaxid = SalatTaxUser.objects.filter(id=id).first()
	else:
		salattaxid = None

	return render(request,'client_registration.html',
		{
		'id':salattaxid,
		})

@csrf_exempt
def get_pan_info(request):
	if request.method=='POST':
		result = {}
		try:
			client_id = request.POST.get('client_id')
			pan = ''
			dob = ''
			p_age = ''
			i_year = ''
			name = ''
			mobile = ''
			no_children = ''
			financial_goal = ''
			business_partner_id=''
			issalat=0
			child_no= []
			child_type = []
			child_age = []
			url = request.META.get('HTTP_REFERER', None)

			log.info(url)

			if Personal_Details.objects.filter(P_Id = client_id).exists():
				for client in Personal_Details.objects.filter(P_Id = client_id):
					if client.pan is None:
						pan = ''
					else:
						pan = client.pan
					if client.dob is None:
						dob = ''
					else:
						dob = client.dob.strftime('%d-%m-%Y')
					p_age = client.dependent_parent_age
					name = client.name
					mobile = client.mobile
					no_children = client.no_of_child
					financial_goal = client.financial_goal
					if client.business_partner_id!=None:
						business_partner_id=client.business_partner_id.id

			if pan!='':
				if salatclient.objects.filter(PAN=pan).exists():
					issalat=salatclient.objects.get(PAN=pan).Is_salat_client
				

			if Investment_Details.objects.filter(P_Id=client_id).exists():
				for i_d in Investment_Details.objects.filter(P_Id = client_id):
					i_year = i_d.year_of_first_share_sold

			if child_info.objects.filter(p_id=client_id).exists():
				for child in child_info.objects.filter(p_id=client_id):
					child_no.append(child.child_no)
					child_type.append(child.child_type)
					child_age.append(child.Child_age)

			result['salatclient']=issalat
			result['pan'] = pan
			result['dob'] = dob
			result['p_age'] = p_age
			result['i_year'] = i_year
			result['name'] = name
			result['mobile'] = mobile
			result['no_children'] = no_children
			result['child_no'] = child_no
			result['child_type'] = child_type
			result['child_age'] = child_age
			result['financial_goal'] = financial_goal
			result['url'] = url
			result['business_partner_id'] = business_partner_id
			result['status'] = 'success'
		except Exception as ex:
			result['status']='Error get_pan_info : ' + traceback.format_exc()
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def update_pan_info(request):
	if request.method=='POST':
		result = {}
		try:
			client_id = request.POST.get('client_id')
			pan = request.POST.get('pan')
			p_age = request.POST.get('p_age')
			shares_year = request.POST.get('shares_year')
			data_age = request.POST.get('data_age')
			req_data = json.loads(data_age)
			max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if not Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
				Return_Details.objects.create(
					R_id = max_r_id['R_id__max']+1,
					P_id = personal_instance,
					business_partner_id=personal_instance.business_partner_id,
					FY = '2018-2019',
					AY = '2017-2018',
				).save()

			if(p_age == ''):
				p_age = 'NA'

			no_children = req_data['no_children']
			if(no_children == '1' or no_children == '2' or no_children == '3' or no_children == '4'):
				type_c1 = req_data['type_c1']
				age_c1 = req_data['age_c1']
			if(no_children == '2' or no_children == '3' or no_children == '4'):
				type_c2 = req_data['type_c2']
				age_c2 = req_data['age_c2']
			if(no_children == '3' or no_children == '4'):
				type_c3 = req_data['type_c3']
				age_c3 = req_data['age_c3']
			if(no_children == '4'):
				type_c4 = req_data['type_c4']
				age_c4 = req_data['age_c4']

			if Personal_Details.objects.filter(P_Id=client_id,pan=pan).exists():
				pd_instance=Personal_Details.objects.get(P_Id=client_id)
				pd_instance.no_of_child = '0'
				pd_instance.dependent_parent_age=p_age
				pd_instance.no_of_child=no_children
				pd_instance.save()
				log.info(pan + " Entry Updated")

			if not Investment_Details.objects.filter(P_Id=client_id).exists():
				Investment_Details.objects.create(
					P_Id=client_id,
					year_of_first_share_sold=shares_year,
				).save()
				# log.info(pan + "S Entry Created")
			else:
				id_instance=Investment_Details.objects.get(P_Id=client_id)
				id_instance.year_of_first_share_sold = shares_year
				id_instance.save()
				# log.info(pan + " Entry Updated")

			if(no_children == '0'):
				if child_info.objects.filter(p_id=client_id).exists():
					child_info.objects.filter(p_id=client_id).delete()
			if(no_children == '1'):
				if child_info.objects.filter(p_id=client_id,child_no='2').exists():
					child_info.objects.filter(p_id=client_id,child_no='2').delete()
				if child_info.objects.filter(p_id=client_id,child_no='3').exists():
					child_info.objects.filter(p_id=client_id,child_no='3').delete()
				if child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.filter(p_id=client_id,child_no='4').delete()
			if(no_children == '2'):
				if child_info.objects.filter(p_id=client_id,child_no='3').exists():
					child_info.objects.filter(p_id=client_id,child_no='3').delete()
				if child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.filter(p_id=client_id,child_no='4').delete()
			if(no_children == '3'):
				if child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.filter(p_id=client_id,child_no='4').delete()

			if(no_children == '1' or no_children == '2' or no_children == '3' or no_children == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='1').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='1',
						child_type=type_c1,
						Child_age=age_c1,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='1')
					child_info_i.child_no = '1'
					child_info_i.child_type = type_c1
					child_info_i.Child_age = age_c1
					child_info_i.save()
			if(no_children == '2' or no_children == '3' or no_children == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='2').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='2',
						child_type=type_c2,
						Child_age=age_c2,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='2')
					child_info_i.child_no = '2'
					child_info_i.child_type = type_c2
					child_info_i.Child_age = age_c2
					child_info_i.save()
			if(no_children == '3' or no_children == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='3').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='3',
						child_type=type_c3,
						Child_age=age_c3,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='3')
					child_info_i.child_no = '3'
					child_info_i.child_type = type_c3
					child_info_i.Child_age = age_c3
					child_info_i.save()
			if(no_children == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='4',
						child_type=type_c4,
						Child_age=age_c4,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='4')
					child_info_i.child_no = '4'
					child_info_i.child_type = type_c4
					child_info_i.Child_age = age_c4
					child_info_i.save()

			result['pan'] = pan
			result['client_id'] = client_id
			result['p_age'] = p_age
			result['shares_year'] = shares_year
			result['no_children'] = no_children
			result['len_no_children'] = len(no_children)
			result['status'] = 'success'
		except Exception as ex:
			result['status']='Error update_pan_info : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def update_subscription_info(request):
	if request.method=='POST':
		result = {}
		try:
			client_id = request.POST.get('client_id')
			name = request.POST.get('name')
			mobile = request.POST.get('mobile')
			pan = request.POST.get('pan')
			employment_state = request.POST.get('employment_state')
			mailid = request.POST.get('mailid')

			name = re.sub( '\s+', ' ', name )

			if Personal_Details.objects.filter(P_Id=client_id,pan=pan).exists():
				pd_instance=Personal_Details.objects.get(P_Id=client_id)
				pd_instance.name = name.replace(" ","|")
				pd_instance.mobile=mobile
				pd_instance.pan=pan
				pd_instance.state_of_employment=employment_state
				pd_instance.save()
				log.info(pd_instance.email)

				if(pd_instance.email ==''):
					pd_instance.email=mailid
					pd_instance.save()
					log.info(pan + " MailId Entry Updated")
				if(pd_instance.email is None):
					pd_instance.email=mailid
					pd_instance.save()
					log.info(pan + " MailId Entry Updated")
				# log.info(pd_instance.email)
				log.info(pan + " Entry Updated")
			else:
				Personal_Details.objects.create(
					P_Id=client_id,
					business_partner_id=business_partner_instance,
					pan=pan,
					pan_type='PAN',
					name=name.replace(" ","|"),
					email=mailid,
					mobile=mobile,
					state_of_employment = employment_state,
				).save()

			# if not Investment_Details.objects.filter(P_Id=client_id).exists():
			# 	Investment_Details.objects.create(
			# 		P_Id=client_id,
			# 		year_of_first_share_sold=shares_year,
			# 	).save()
			# 	# log.info(pan + "S Entry Created")
			# else:
			# 	id_instance=Investment_Details.objects.get(P_Id=client_id)
			# 	id_instance.year_of_first_share_sold = shares_year
			# 	id_instance.save()
				# log.info(pan + " Entry Updated")

			result['name'] = name
			result['pan'] = pan
			result['client_id'] = client_id
			result['mobile'] = mobile
			result['status'] = 'success'
		except Exception as ex:
			result['status']='Error update_subscription_info : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_ITR_data(request):
	if request.method=='POST':
		result = {}
		try:
			pan = request.POST.get('pan')

			year = []
			ITR = []
			income_from_salary = []
			no_of_hp = []
			income_from_hp = []
			interest_payable_borrowed_capital = []
			capital_gain = []
			schedule_si_income = []
			income_from_other_src = []
			interest_gross = []
			gross_total_income = []
			deduction = []
			c_80 = []
			d_80 = []
			tta_80 = []
			ccd_1b_80 = []
			ccg_80 = []
			e_80 = []
			ee_80 = []
			g_80 = []
			gg_80 = []
			total_income = []
			schedule_si_tax = []
			total_tax_interest = []
			tax_paid = []
			adv_tax_self_ass_tax=[]
			total_tcs = []
			total_tds_ITR = []
			business_or_profession = []
			count_itr = 0

			for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
				count_itr += 1
				year.append(itr.year)
				ITR.append(itr.ITR)
				income_from_salary.append(itr.income_from_salary)
				no_of_hp.append(itr.no_of_hp)
				income_from_hp.append(itr.income_from_hp)
				interest_payable_borrowed_capital.append(itr.interest_payable_borrowed_capital)
				capital_gain.append(itr.capital_gain)
				schedule_si_income.append(itr.schedule_si_income)
				income_from_other_src.append(itr.income_from_other_src)
				interest_gross.append(itr.interest_gross)
				gross_total_income.append(itr.gross_total_income)
				deduction.append(itr.deduction)
				c_80.append(itr.c_80)
				d_80.append(itr.d_80)
				tta_80.append(itr.tta_80)
				ccd_1b_80.append(itr.ccd_1b_80)
				ccg_80.append(itr.ccg_80)
				e_80.append(itr.e_80)
				ee_80.append(itr.ee_80)
				g_80.append(itr.g_80)
				gg_80.append(itr.gg_80)
				total_income.append(itr.total_income)
				schedule_si_tax.append(itr.schedule_si_tax)
				total_tax_interest.append(itr.total_tax_interest)
				tax_paid.append(itr.tax_paid)
				adv_tax_self_ass_tax.append(itr.adv_tax_self_ass_tax)
				total_tcs.append(itr.total_tcs)
				total_tds_ITR.append(itr.total_tds_ITR)
				business_or_profession.append(itr.business_or_profession)

			result['pan'] = pan
			result['status'] = 'success'
			result['count_itr'] = count_itr
			result['year'] = year
			result['ITR'] = ITR
			result['income_from_salary'] = income_from_salary
			result['no_of_hp'] = no_of_hp
			result['income_from_hp'] = income_from_hp
			result['interest_payable_borrowed_capital'] = interest_payable_borrowed_capital
			result['capital_gain'] = capital_gain
			result['schedule_si_income'] = schedule_si_income
			result['income_from_other_src'] = income_from_other_src
			result['interest_gross'] = interest_gross
			result['gross_total_income'] = gross_total_income
			result['deduction'] = deduction
			result['c_80'] = c_80
			result['d_80'] = d_80
			result['tta_80'] = tta_80
			result['ccd_1b_80'] = ccd_1b_80
			result['ccg_80'] = ccg_80
			result['e_80'] = e_80
			result['ee_80'] = ee_80
			result['g_80'] = g_80
			result['gg_80'] = gg_80
			result['total_income'] = total_income
			result['schedule_si_tax'] = schedule_si_tax
			result['total_tax_interest'] = total_tax_interest
			result['tax_paid'] = tax_paid
			result['adv_tax_self_ass_tax'] = adv_tax_self_ass_tax
			result['total_tcs'] = total_tcs
			result['total_tds_ITR'] = total_tds_ITR
			result['business_or_profession'] = business_or_profession
		except Exception as ex:
			result['status']='Error get_ITR_data : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def deduction_mismatch(request):
	if request.method=='POST':
		result = {}
		try:
			pan = request.POST.get('pan')
			p_age = request.POST.get('p_age')
			if(p_age=='' or p_age=='NA'):
				p_age = 0
			year = []
			ITR = []
			deduction = []
			c_80 = []
			d_80 = []
			tta_80 = []
			ccd_1b_80 = []
			ccg_80 = []
			tax_slab_14 = 0
			tax_slab_15 = 0
			tax_slab_16 = 0
			tax_slab_17 = 0
			tax_slab_18 = 0
			count_itr = 0
			c_80_limit14, ccd_1b_80_limit14, d_80_limit14 = (0,)*3
			c_80_limit15, ccd_1b_80_limit15, d_80_limit15 = (0,)*3
			c_80_limit16, ccd_1b_80_limit16, d_80_limit16 = (0,)*3
			c_80_limit17, ccd_1b_80_limit17, d_80_limit17 = (0,)*3
			c_80_limit18, ccd_1b_80_limit18, d_80_limit18, ccg_80_limit18 = (0,)*4

			for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
				income_from_salary = get_int(itr.income_from_salary)
				income_from_hp = get_int(itr.income_from_hp)
				income_from_os = get_int(itr.income_from_other_src)
				business_or_profession = get_int(itr.business_or_profession)
				total_capital_gain = get_int(itr.capital_gain)
				schedule_si_income = get_int(itr.schedule_si_income)
				capital_gain = total_capital_gain - schedule_si_income
				cal_GTI = income_from_salary + income_from_hp + capital_gain + income_from_os + business_or_profession

				if(itr.ccg_80=='NA' or itr.ccg_80==''):
					ccg_80.append(0)
				else:
					ccg_80.append(itr.ccg_80)
				TI = cal_GTI-get_int(itr.c_80)-get_int(itr.d_80)-get_int(itr.ccg_80)-get_int(itr.ccd_1b_80)-get_int(itr.ccd_2_80)
				TI = TI-get_int(itr.e_80)-get_int(itr.ee_80)-get_int(itr.g_80)-get_int(itr.gg_80)-get_int(itr.tta_80)

				count_itr += 1
				year.append(itr.year)
				ITR.append(itr.ITR)
				deduction.append(itr.deduction)
				c_80.append(itr.c_80)
				d_80.append(itr.d_80)
				tta_80.append(itr.tta_80)
				ccd_1b_80.append(itr.ccd_1b_80)

				if(itr.year=='2014'):
					slab1_range = 200000
					slab2_range = 500000
					slab3_range = 1000000
					c_80_limit14 = 100000
					ccd_1b_80_limit14 = 0
					p_age_cal = int(p_age) - 4
					if(p_age_cal>60):
						d_80_limit14 = 35000
					else:
						d_80_limit14 = 30000
					if(TI>slab3_range):
						tax_slab_14 = 0.3
					elif(TI<slab3_range and TI>slab2_range):
						tax_slab_14 = 0.2
					elif(TI<slab2_range and TI>slab1_range):
						tax_slab_14 = 0.1
					elif(TI<slab1_range):
						tax_slab_14 = 0

				if(itr.year=='2015'):
					slab1_range = 250000
					slab2_range = 500000
					slab3_range = 1000000
					c_80_limit15 = 150000
					ccd_1b_80_limit15 = 0
					p_age_cal = int(p_age) - 3
					if(p_age_cal>60):
						d_80_limit15 = 35000
					else:
						d_80_limit15 = 30000
					if(TI>slab3_range):
						tax_slab_15 = 0.3
					elif(TI<slab3_range and TI>slab2_range):
						tax_slab_15 = 0.2
					elif(TI<slab2_range and TI>slab1_range):
						tax_slab_15 = 0.1
					elif(TI<slab1_range):
						tax_slab_15 = 0

				if(itr.year=='2016'):
					slab1_range = 250000
					slab2_range = 500000
					slab3_range = 1000000
					c_80_limit16 = 150000
					ccd_1b_80_limit16 = 50000
					p_age_cal = int(p_age) - 2
					if(p_age_cal>60):
						d_80_limit16 = 55000
					else:
						d_80_limit16 = 50000
					if(TI>slab3_range):
						tax_slab_16 = 0.3
					elif(TI<slab3_range and TI>slab2_range):
						tax_slab_16 = 0.2
					elif(TI<slab2_range and TI>slab1_range):
						tax_slab_16 = 0.1
					elif(TI<slab1_range):
						tax_slab_16 = 0

				if(itr.year=='2017'):
					slab1_range = 250000
					slab2_range = 500000
					slab3_range = 1000000
					c_80_limit17 = 150000
					ccd_1b_80_limit17 = 50000
					p_age_cal = int(p_age) - 1
					if(p_age_cal>60):
						d_80_limit17 = 55000
					else:
						d_80_limit17 = 50000
					if(TI>slab3_range):
						tax_slab_17 = 0.3
					elif(TI<slab3_range and TI>slab2_range):
						tax_slab_17 = 0.2
					elif(TI<slab2_range and TI>slab1_range):
						tax_slab_17 = 0.1
					elif(TI<slab1_range):
						tax_slab_17 = 0

				if(itr.year=='2018'):
					slab1_range = 250000
					slab2_range = 500000
					slab3_range = 1000000
					c_80_limit18 = 150000
					ccd_1b_80_limit18 = 50000
					p_age_cal = int(p_age)
					if(p_age_cal>60):
						d_80_limit18 = 55000
					else:
						d_80_limit18 = 50000
					ccg_80_limit18 = 0
					if(TI>slab3_range):
						tax_slab_18 = 0.3
					elif(TI<slab3_range and TI>slab2_range):
						tax_slab_18 = 0.2
					elif(TI<slab2_range and TI>slab1_range):
						tax_slab_18 = 0.1
					elif(TI<slab1_range):
						tax_slab_18 = 0
			
			result['pan'] = pan
			result['p_age'] = p_age
			result['status'] = 'success'
			result['count_itr'] = count_itr
			result['year'] = year
			result['ITR'] = ITR
			result['deduction'] = deduction
			result['c_80'] = c_80
			result['d_80'] = d_80
			result['tta_80'] = tta_80
			result['ccd_1b_80'] = ccd_1b_80
			result['ccg_80'] = ccg_80
			result['c_80_limit14'] = c_80_limit14
			result['c_80_limit15'] = c_80_limit15
			result['c_80_limit16'] = c_80_limit16
			result['c_80_limit17'] = c_80_limit17
			result['c_80_limit18'] = c_80_limit18
			result['ccd_1b_80_limit14'] = ccd_1b_80_limit14
			result['ccd_1b_80_limit15'] = ccd_1b_80_limit15
			result['ccd_1b_80_limit16'] = ccd_1b_80_limit16
			result['ccd_1b_80_limit17'] = ccd_1b_80_limit17
			result['ccd_1b_80_limit18'] = ccd_1b_80_limit18
			result['d_80_limit14'] = d_80_limit14
			result['d_80_limit15'] = d_80_limit15
			result['d_80_limit16'] = d_80_limit16
			result['d_80_limit17'] = d_80_limit17
			result['d_80_limit18'] = d_80_limit18
			result['tax_slab_14'] = tax_slab_14
			result['tax_slab_15'] = tax_slab_15
			result['tax_slab_16'] = tax_slab_16
			result['tax_slab_17'] = tax_slab_17
			result['tax_slab_18'] = tax_slab_18
			result['ccg_80_limit18'] = ccg_80_limit18
		except Exception as ex:
			result['status']='Error deduction_mismatch : %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def get_adhar_xml(request):
	result={}
	if request.method=='POST':
		try:
			pan=request.POST.get('pan')
			b_p_id=request.POST.get('b_p_id')
			aadhar=request.POST.get('aadhar')
			status = 'start'

			pan = 'EGRPK2447C'
			aadhar = '859748396648'

			driver = driver_call()
			aadhar_xml = aadhar_xml_fun(driver,pan,b_p_id,aadhar)
			# result['tab_completed']= register_client_status['tab_completed']
			# result['state']= register_client_status['state']
			# driver.close()

			status = 'success'
			result['status']= status
			result['pan']= pan
			result['b_p_id']= b_p_id
			result['aadhar']= aadhar
			result['aadhar_xml'] = aadhar_xml
		except Exception as ex:
			result['status']='Error in get_adhar_xml: %s' % ex
			log.error('Error in get_adhar_xml: '+traceback.format_exc())

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def check_aadharcaptcha(request):
	result={}
	if request.method=='POST':
		try:
			aadhar=request.POST.get('aadhar')
			status = 'start'

			aadhar = '859748396648'
			file_exist = 0

			try:
				filename_image = '/home/salattax/SalatTax/salattax_app/static/aadhar/'+aadhar+'.png'
				# os.path.exists(filename_image)
				# fh = open(filename_image, 'r')
				file_exist = str(os.path.exists(filename_image))
				if file_exist == 'True':
					file_exist = 1
				else:
					file_exist = 0
			except Exception as ex:
				log.error('Error in file_exist : %s' % ex)
				file_exist = 0

			status = 'success'
			result['status']= status
			result['file_exist']= file_exist
			result['aadhar']= aadhar
		except Exception as ex:
			result['status']='Error in check_aadharcaptcha : %s' % ex
			log.error('Error in check_aadharcaptcha : %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def aadhar_xml_fun(driver,pan,b_p_id,aadhar):
	driver.maximize_window()
	result={}
	result['status'] = 'pending'
	result['download_captcha'] = 'pending'
	driver.get("https://resident.uidai.gov.in/offlineaadhaar")
	# 499852492425
	# //*[@id="uid"]
	aadhar_no = driver.find_element_by_id("uid")
	aadhar_no.send_keys("859748396648")
	time.sleep(1)

	# get captcha
	# //*[@id="aui_3_4_0_1_283"]/div[2]/div/div/div/div[2]/div[1]/div/div/img
	# //*[@id="aui_3_4_0_1_277"]/div[2]/div/div/div/div[2]/div[1]/div/div/img
	error = 0
	captch_img_path = '/home/salattax/SalatTax/salattax_app/static/aadhar/'+aadhar+'.png'
	try:
		# driver.find_element_by_id("AddClientDetails_captchaCode").clear()
		# driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_Aadhaar"]/div[2]/div/div/div[2]/div/div/div/div[2]
		# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_Aadhaar"]/div[2]/div/div/div[2]/div/div/div/div[2]/div[1]
		try:
			# captcha_id = driver.find_elements_by_xpath('//*[@id="aui_3_4_0_1_283"]/div[2]/div/div/div/div[2]/div[1]/div/div/img')[0]
			captcha_id = driver.find_elements_by_xpath('//*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_Aadhaar"]/div[2]/div/div/div[2]/div/div/div/div[2]/div[1]')[0]
		except Exception as ex:
			log.info('Error in captcha xpath: %s' % ex)
		
			try:
				captcha_id = driver.find_elements_by_xpath('//*[@id="aui_3_4_0_1_277"]/div[2]/div/div/div/div[2]/div[1]/div/div/img')[0]
			except Exception as ex:
				log.info('Error in captcha xpath2: %s' % ex)

		screenshot(driver,'f_name')
		location = captcha_id.location
		log.info(location)
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width'] - 230
		bottom = location['y'] + size['height'] - 10
		img = img.crop((int(left)-1, int(top)-1, int(right)-1, int(bottom)-1))
		img.save(captch_img_path)
		result['download_captcha'] = 'done'
	except Exception as ex:
		error = 1
		log.error('Error in adhar captcha: %s' % ex)

	captcha_text = ''
	filename_captcha ='/home/salattax/SalatTax/salattax_app/static/aadhar/'+aadhar+'_captcha.txt'

	captcha_error = ''
	if error == 0 :
		error_captcha = 1
		timeout = time.time() + 60*5   # 8 minutes from now
		while error_captcha == 1:
			if time.time() > timeout:
				captcha_text = 'Enter captcha'
				break
			try:
				file = open(filename_captcha,'r')
				c_text = file.read() 
				# log.info(c_text)
				if c_text == '':
					pass
				else:
					driver.find_element_by_id("_OfflineAadhaar_WAR_OfflineAadhaarportlet_captchaText").send_keys(c_text)
					# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_captchaText"]
					error_captcha = 0
					os.remove(filename_captcha)
					captcha_text = 'submitted'
			except Exception:
				error_captcha = 1
				captcha_text = 'waiting'
				# break

		if captcha_text == 'submitted':
			# //*[@id="Button1"]
			send_otp = driver.find_elements_by_xpath('//*[@id="Button1"]')[0]
			driver.execute_script("arguments[0].click();", send_otp)
			time.sleep(2)
			try:
				# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/div
				check_element = driver.find_element_by_xpath('//*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/div').text
				if (check_element.find('enter valid') == 0):
					captcha_error = 'Invalid'
			except Exception as e:
				captcha_error = 'valid'

	result['captcha_error'] = captcha_error
	result['captcha_text'] = captcha_text

	otp_send = ''
	if captcha_error == 'valid':
		os.remove(captch_img_path)
		# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/p
		try:
			check_element = driver.find_element_by_xpath('//*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/p').text
			log.info(check_element)
			if (check_element.find('Check your mobile') != 0):
				otp_send = 'yes'
		except Exception as e:
			otp_send = 'no'
		
		time.sleep(2)

	result['otp_send'] = otp_send

	otp_text = ''
	otp_error = ''
	filename_otp ='/home/salattax/SalatTax/salattax_app/static/aadhar/'+aadhar+'_otp.txt'
	if otp_send == 'yes':
		error_otp = 1
		timeout = time.time() + 60*5   # 8 minutes from now
		while error_otp == 1:
			if time.time() > timeout:
				otp_text = 'Enter OTP'
				break
			try:
				file = open(filename_otp,'r')
				c_text = file.read() 
				# log.info(c_text)
				if c_text == '':
					pass
				else:
					driver.find_element_by_id("otp").send_keys(c_text)
					# //*[@id="otp"]
					error_otp = 0
					os.remove(filename_otp)
					otp_text = 'submitted'
			except Exception:
				error_otp = 1
				otp_text = 'waiting'
				# break

		if otp_text == 'submitted':
			# //*[@id="sharecode"]
			sharecode = driver.find_element_by_id("sharecode")
			sharecode.send_keys("1234")
			time.sleep(2)
			try:
				# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/div
				# //*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/div
				check_element = driver.find_element_by_xpath('//*[@id="_OfflineAadhaar_WAR_OfflineAadhaarportlet_verifyOtpForm"]/div[2]/div/div').text
				if (check_element.find('OTP Fail') >= 0):
					otp_error = 'Invalid'
			except Exception as e:
				otp_error = 'valid'

	result['otp_text'] = otp_text
	result['otp_error'] = otp_error

	xml_download = ''

	if otp_error == 'valid':
		try:
			downloadPath = '/home/salattax/SalatTax/salattax_app/static/aadhar/'
			driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
			params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
			command_result = driver.execute("send_command", params)
			time.sleep(1)
			
			download_xml = driver.find_elements_by_xpath('//*[@id="downloadBtn"]')[0]
			driver.execute_script("arguments[0].click();", download_xml)
			xml_download = 'completed'
		except Exception as ex:
			log.info('Error in adhar captcha: %s' % ex)
			xml_download = 'pending'
	
	result['xml_download'] = xml_download

	if xml_download == 'completed':
		password = '1234'

		unzip_file = '/home/salattax/SalatTax/salattax_app/static/aadhar/'+pan+'/'
		unzip_path = '/home/salattax/SalatTax/salattax_app/static/aadhar/'
		log.info('Zip file list :')
		for file in os.listdir(unzip_file):
			if(file.find('.zip') != -1):
				log.info(file)
				rename = b_p_id+'_'+pan+'_ADHAR.xml'
				with zipfile.ZipFile(unzip_file+file,"r") as zip_ref:
					zip_ref.extractall(path=unzip_path,pwd=password)
					old_file = os.path.join(unzip_path, file)
					new_file = os.path.join(unzip_path, rename)
					os.rename(old_file, new_file)
					os.remove(unzip_file)


	time.sleep(10)
	return result

@csrf_exempt
def save_adhar_captcha(request):
	if request.method=='POST':
		result={}
		try:
			option = request.POST.get('option')
			aadhar = request.POST.get('aadhar')
			aadhar = '859748396648'
			pan = 'EGRPK2447C'
			result['status']= 'start'

			if option == 'captcha_text':
				captcha_text = request.POST.get('text')
				MYDIR = os.path.dirname(__file__)
				filename_captcha ='/home/salattax/SalatTax/salattax_app/static/aadhar/'+aadhar+'_captcha'

				file = open(filename_captcha+'.txt','w')
				file.write(captcha_text) 
				file.close()

			if option == 'aadhar_otp':
				aadhar_otp = request.POST.get('text')
				MYDIR = os.path.dirname(__file__)
				filename_captcha ='/home/salattax/SalatTax/salattax_app/static/aadhar/'+aadhar+'_otp'

				file = open(filename_captcha+'.txt','w')
				file.write(aadhar_otp) 
				file.close() 

			result['status']= 'success'
		except Exception as ex:
			result['status']='Error in json data: %s' % ex
			log.error('Error in save_adhar_captcha: '+traceback.format_exc())

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


# Thank you for registering in e-Filing. Your Transaction ID is <strong> 5384206918</strong>
# sudo apt-get install xvfb
# pip install pyvirtualdisplay
# ACDPL0065F - 24071986
# django - server - 1.11
