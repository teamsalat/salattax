# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.http import HttpResponse
# Create your models here.
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# from ckeditor.fields import RichTextField 
# from django.contrib.auth.models import AbstractUser
# from activity_log.models import UserMixin
# from salattax import settings
# from django.contrib.auth.models import AnonymousUser
# from django.urls import reverse

# from django.conf import settings
import logging
# from .managers import VisitorManager, PageviewManager
# from .settings import TRACK_USING_GEOIP

# try:
#     from django.contrib.gis.geoip import HAS_GEOIP
# except ImportError:
#     from django.contrib.gis.geoip2 import HAS_GEOIP2 as HAS_GEOIP

# if HAS_GEOIP:
#     try:
#         from django.contrib.gis.geoip import GeoIP, GeoIPException
#     except ImportError:
#         from django.contrib.gis.geoip2 import (
#             GeoIP2 as GeoIP,
#             GeoIP2Exception as GeoIPException,
#         )

# GEOIP_CACHE_TYPE = getattr(settings, 'GEOIP_CACHE_TYPE', 4)

log = logging.getLogger(__file__)


def index(request):
	return HttpResponse("Hello, world. You're at the polls index.")

class demo(models.Model):
	permission = models.IntegerField(default=0)
	name = models.CharField(max_length=200)

class SalatTaxRole(models.Model):
	rolename = models.CharField(max_length=200)
	permission = models.IntegerField(default=0)
	created = models.DateTimeField('created',default=timezone.now)
	updated = models.DateTimeField('updated',default=timezone.now)

class SalatTaxUser(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	phone = models.CharField(max_length=50,null=True)
	email = models.CharField(max_length=100,null=True)
	firstname = models.CharField(max_length=200,null=True)
	lastname = models.CharField(max_length=200,null=True)
	role = models.ForeignKey(SalatTaxRole, null=True)
	referral_code=models.CharField(max_length=50,null=True)
	referred_by_code=models.CharField(max_length=50,null=True)
	created = models.DateTimeField('created',default=timezone.now)
	updated = models.DateTimeField('updated',default=timezone.now)

class ResetPassword(models.Model):
	salattaxuser = models.ForeignKey(SalatTaxUser, on_delete=models.CASCADE,null=True)
	email = models.CharField(max_length=100,null=True)
	username = models.CharField(max_length=100,null=True)
	url = models.TextField(null=True)
	status=models.CharField(max_length=20,null=True)
	timestamp = models.DateTimeField('timestamp',default=timezone.now)

class Business(models.Model):
	main_business_name = models.CharField(max_length=240,null=True)

class Business_Partner(models.Model):
	user_id = models.ForeignKey(SalatTaxUser,null=True)
	main_business = models.ForeignKey(Business,null=True)
	partner_business_name = models.CharField(max_length=240,null=True)
	partner_id=models.CharField(max_length=20,null=True,blank=True)
	status=models.CharField(max_length=20,null=True,blank=True)
	name = models.CharField(max_length=240,null=True)
	pan = models.CharField(max_length=20,null=True)
	email = models.CharField(max_length=120,null=True)
	mobile = models.CharField(max_length=20,null=True)
	primary_business = models.CharField(max_length=150,null=True)
	address = models.CharField(max_length=240,null=True)
	city = models.CharField(max_length=100,null=True)
	state = models.CharField(max_length=100,null=True)
	pincode = models.CharField(max_length=20,null=True)
	ifsc = models.CharField(max_length=20,null=True)
	account_no = models.CharField(max_length=50,null=True)
	account_type = models.CharField(max_length=30,null=True)
	bank_holder_name = models.CharField(max_length=240,null=True)
	bank_proof = models.FileField(upload_to='media', null=True,blank=True)
	created_date = models.DateTimeField('created_date',default=timezone.now)

class Partner_Deposit_Details(models.Model):
	business_partner_id = models.ForeignKey(Business_Partner,null=True)
	deposite_amount=models.CharField(max_length=50,null=True,blank=True)
	deposit_date=models.DateTimeField('created_date',default=timezone.now)
	transaction_id=models.CharField(max_length=100,null=True,blank=True)
	show_in_balance = models.CharField(max_length=100,null=True,blank=True)
	payment_status = models.CharField(max_length=100,null=True,blank=True)
	mode_of_payment = models.CharField(max_length=100,null=True,blank=True)

class Partner_Deposit_Balance(models.Model):
	business_partner_id = models.ForeignKey(Business_Partner,null=True)
	deposit_balance=models.CharField(max_length=50,null=True,blank=True)

class client_info(models.Model):
	username = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	password = models.CharField(max_length=200)
	created_time = models.DateTimeField(default=timezone.now)

class sign_up_otp(models.Model):
	username = models.CharField(max_length=200,null=True)
	mailid = models.CharField(max_length=200)
	otp = models.CharField(max_length=200)
	verified = models.CharField(max_length=200)
	created_time = models.DateTimeField(default=timezone.now, editable=False)

class Personal_Details(models.Model):
	P_Id = models.IntegerField(null=True)
	business_partner_id = models.ForeignKey(Business_Partner,null=True)
	pan = models.CharField(max_length=20)
	pan_type = models.CharField(max_length=20)
	dob = models.DateField(null=True)
	pan_image = models.FileField(upload_to='media',null=True)
	aadhar_no = models.CharField(max_length=100,null=True)
	aadhar_image = models.FileField(upload_to='media',null=True)
	name = models.CharField(max_length=250,null=True)
	email = models.CharField(max_length=100,null=True)
	mobile = models.CharField(max_length=20,null=True)
	father_name = models.CharField(max_length=100,null=True)
	gender = models.CharField(max_length=100,null=True)
	dependent_parent_age = models.CharField(max_length=100,null=True)
	no_of_child = models.CharField(max_length=100,null=True)
	state_of_employment = models.CharField(max_length=50,null=True)
	disability = models.CharField(max_length=100,null=True)
	financial_goal = models.CharField(max_length=100,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)
	updated_by = models.CharField(max_length=200,null=True)


class Address_Details(models.Model):
	p_id = models.ForeignKey(Personal_Details, on_delete=models.CASCADE)
	flat_door_block_no = models.CharField(max_length=250,null=True)
	name_of_premises_building = models.CharField(max_length=250,null=True)
	road_street_postoffice = models.CharField(max_length=250,null=True)
	area_locality = models.CharField(max_length=250,null=True)
	pincode = models.CharField(max_length=20,null=True)
	town_city = models.CharField(max_length=200,null=True)
	state = models.CharField(max_length=200,null=True)
	country = models.CharField(max_length=200,null=True)
	current_place = models.CharField(max_length=100,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Return_Details(models.Model):
	R_id = models.IntegerField(null=True)
	P_id = models.ForeignKey(Personal_Details, on_delete=models.CASCADE)
	business_partner_id = models.ForeignKey(Business_Partner,null=True)
	FY = models.CharField(max_length=250,null=True)
	AY = models.CharField(max_length=250,null=True)
	return_filing_category = models.CharField(max_length=250,null=True)
	return_filing_type= models.CharField(max_length=250,null=True)
	xml_generated = models.DateTimeField(null=True)
	xml_uploaded= models.DateTimeField(null=True)
	return_e_verified = models.CharField(max_length=20,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Original_return_Details(models.Model):
	P_id = models.ForeignKey(Personal_Details, on_delete=models.CASCADE)
	FY = models.CharField(max_length=50,null=True)
	Receipt_no_original_return = models.CharField(max_length=60,null=True)
	Date_of_original_return_filing = models.DateField(null=True)
	notice_no = models.IntegerField(null=True)
	notice_date = models.DateField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Employer_Details(models.Model):
	P_id = models.ForeignKey(Personal_Details, on_delete=models.CASCADE)
	FY = models.CharField(max_length=250,null=True)
	F16_no = models.IntegerField(null=True)
	Name_of_employer = models.CharField(max_length=250,null=True)
	TAN_of_employer = models.CharField(max_length=250,null=True)
	PAN_of_employer = models.CharField(max_length=250,null=True)
	Address_of_employer = models.CharField(max_length=250,null=True)
	Employer_city = models.CharField(max_length=250,null=True)
	Employer_state = models.CharField(max_length=250,null=True)
	Employer_pin = models.CharField(max_length=250,null=True)
	Employer_category = models.CharField(max_length=250,null=True)
	residential_status = models.CharField(max_length=250,null=True)
	start_date = models.DateField(null=True)
	end_date = models.DateField(null=True)
	monthly_basic_salary = models.CharField(max_length=250,null=True)
	monthly_hra = models.CharField(max_length=250,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Bank_Details(models.Model):
	P_id = models.ForeignKey(Personal_Details, on_delete=models.CASCADE)
	Bank_id = models.IntegerField(null=True)
	Default_bank = models.CharField(max_length=250,null=True)
	IFSC_code = models.CharField(max_length=250,null=True)
	Account_no = models.CharField(max_length=250,null=True)
	Account_type = models.CharField(max_length=250,null=True)
	Bank_proof = models.FileField(upload_to='media',null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Form16(models.Model):
	Salary_as_per_provision = models.IntegerField(null=True)
	Value_of_Perquisites = models.IntegerField(null=True)
	Profits_in_lieu_of_Salary = models.IntegerField(null=True)
	Total_Gross_Salary = models.IntegerField(null=True)
	Gross_Salary_netof_Allowance = models.IntegerField(null=True)
	NSC = models.IntegerField(null=True)
	HRA_Allowed = models.IntegerField(null=True)
	Medical_Allowance = models.IntegerField(null=True)
	Conveyance_Allowance_Allowed = models.IntegerField(null=True)
	LTA = models.IntegerField(null=True)
	LSE = models.IntegerField(null=True)
	HRA_Claimed = models.IntegerField(null=True)
	Conveyance_Allowance_Claimed = models.IntegerField(null=True)
	Income_House_Property = models.IntegerField(null=True)
	Home_Loan_Interest = models.IntegerField(null=True)
	Income_Capital_Gains = models.IntegerField(null=True)
	Income_Other_Sources = models.IntegerField(null=True)
	Gratuity = models.IntegerField(null=True)
	ELSS = models.IntegerField(null=True)#
	EPF = models.IntegerField(null=True)#
	PPF = models.IntegerField(null=True)#
	VPF = models.IntegerField(null=True)#
	LIC = models.IntegerField(null=True)#
	NSC_Interest = models.IntegerField(null=True)
	Superannuation = models.IntegerField(null=True)
	Home_Loan_Principal = models.IntegerField(null=True)#
	FD = models.IntegerField(null=True)#
	ULIP = models.IntegerField(null=True)#
	Child_Edu_Fee = models.IntegerField(null=True)#
	Sukanya_Samriddhi_Scheme = models.IntegerField(null=True)#80c
	Stamp_Duty = models.IntegerField(null=True)
	Form_80CCC = models.IntegerField(null=True)
	NPS_Employee = models.IntegerField(null=True)
	NPS_Employer = models.IntegerField(null=True)
	Form_80CCD = models.IntegerField(null=True)
	Form_80CCF = models.IntegerField(null=True)
	Form_80D_Self_Family = models.IntegerField(null=True)
	Form_80D_Parents = models.IntegerField(null=True)
	Medical_Insurance = models.IntegerField(null=True)
	Form_80E = models.IntegerField(null=True)
	Form_80G = models.IntegerField(null=True)
	Form_80TTA = models.IntegerField(null=True)
	Entertainment_Allowance = models.IntegerField(null=True)
	Profession_Tax = models.IntegerField(null=True)
	VIA_Deductions = models.IntegerField(null=True)
	Basic_Salary =models.IntegerField(null=True)
	Dearness_Allowance =models.IntegerField(null=True)
	Personal_Allowance =models.IntegerField(null=True)
	Bonus =models.IntegerField(null=True)
	Performance_Pay =models.IntegerField(null=True)
	Special_Allowance = models.IntegerField(null=True)
	Food_Allowance = models.IntegerField(null=True)
	Extra_Salary = models.IntegerField(null=True)
	RGESS = models.IntegerField(null=True)
	Exemption_Less_Allowance = models.IntegerField(null=True)
	Total_Less_Allowance = models.IntegerField(null=True)
	Attire = models.IntegerField(null=True)
	Professional_Pursuit = models.IntegerField(null=True)
	Telephone_allowance = models.IntegerField(null=True)
	Other_Allowance =  models.IntegerField(null=True)
	Other_Any_Other_Income = models.IntegerField(null=True)
	cid = models.IntegerField(null=True)
	emp_type = models.IntegerField(null=True)
	Email = models.IntegerField(null=True)
	form16_path = models.IntegerField(null=True)
	password = models.IntegerField(null=True)
	P_D = models.IntegerField(null=True)
	S_C = models.IntegerField(null=True)
	C = models.IntegerField(null=True)
	R = models.IntegerField(null=True)
	self_house = models.IntegerField(null=True)
	HRA_given = models.IntegerField(null=True)
	basic = models.IntegerField(null=True)
	DA = models.IntegerField(null=True)
	rent = models.IntegerField(null=True)
	HRA_given_value = models.IntegerField(null=True)
	stay = models.IntegerField(null=True)
	home_loan = models.IntegerField(null=True)
	interest_paid = models.IntegerField(null=True)
	rent_earn = models.IntegerField(null=True)
	disability = models.IntegerField(null=True)
	disability_type = models.IntegerField(null=True)
	time_stamp = models.IntegerField(null=True)
	salat_tax = models.IntegerField(null=True)
	tax_leakage = models.IntegerField(null=True)
	flag = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Income(models.Model):
	R_Id = models.IntegerField(null=True)
	company_name = models.CharField(max_length=250,null=True)
	Salary = models.IntegerField(null=True)
	Allowances_not_exempt = models.IntegerField(null=True)
	Value_of_perquisites = models.IntegerField(null=True)
	Profits_in_lieu_of_salary = models.IntegerField(null=True)
	Entertainment_Allowance = models.IntegerField(null=True)
	LTA = models.IntegerField(null=True)
	Tax_paid_on_non_monetary_perquisite = models.IntegerField(null=True)
	Form16_HRA = models.IntegerField(null=True)
	Other_allowances = models.IntegerField(null=True)
	Profession_Tax = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)


# change FY type DB error
class Other_Income_Common(models.Model):
	R_Id = models.IntegerField(null=True)
	FY = models.CharField(max_length=20,null=True)
	Interest_Deposits = models.IntegerField(null=True)
	Interest_Savings = models.IntegerField(null=True)
	Commission = models.IntegerField(null=True)
	Other_Income = models.IntegerField(null=True)
	Other_Interest = models.IntegerField(null=True)
	FD = models.IntegerField(null=True)
	family_pension = models.IntegerField(null=True)
	post_office_interest = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Other_Income_Rare(models.Model):
	R_Id = models.IntegerField(null=True)
	Taxable_Dividend = models.IntegerField(null=True)
	Rental_Income_other_than_house_property = models.IntegerField(null=True)
	Winnings = models.IntegerField(null=True)
	Amount_borrowed_or_repaid_on_hundi = models.IntegerField(null=True)
	Dividend_over_10_lacs = models.IntegerField(null=True)
	Investment_income_NRI = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Exempt_Income(models.Model):
	R_Id = models.IntegerField(null=True)
	Tax_Free_Interest = models.IntegerField(null=True)
	LTCG_Equity = models.IntegerField(null=True)
	Tax_Free_Dividend = models.IntegerField(null=True)
	Agriculture_Income_Id = models.IntegerField(null=True)
	Other_exempt_income_Id = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class VI_Deductions(models.Model):
	R_Id = models.IntegerField(null=True)
	VI_80_C = models.IntegerField(null=True)
	VI_80_CCC = models.IntegerField(null=True)
	VI_80_CCD_1 = models.IntegerField(null=True)
	VI_80_CCD_1B = models.IntegerField(null=True)
	VI_80_CCD_2 = models.IntegerField(null=True)
	VI_80_CCG = models.IntegerField(null=True)
	VI_80_D = models.IntegerField(null=True)
	VI_80_DD = models.IntegerField(null=True)
	VI_80_DDB = models.IntegerField(null=True)
	VI_80_E = models.IntegerField(null=True)
	VI_80_EE = models.IntegerField(null=True)
	VI_80_G = models.IntegerField(null=True)
	VI_80_GG = models.IntegerField(null=True)
	VI_80_GGA = models.IntegerField(null=True)
	VI_80_GGC = models.IntegerField(null=True)
	VI_80_QQB = models.IntegerField(null=True)
	VI_80_RRB = models.IntegerField(null=True)
	VI_80_TTA = models.IntegerField(null=True)
	VI_80_U = models.IntegerField(null=True)
	VI_80_TTB = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Agriculture_Income(models.Model):
	R_Id = models.IntegerField(null=True)
	Agriculture_Income_Id = models.IntegerField(null=True)
	Gross_Agriculture_Receipts = models.IntegerField(null=True)
	Expenditure_Incurred_on_Agriculture = models.IntegerField(null=True)
	Unabsorbed_agriculture_loss = models.IntegerField(null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Other_Exempt_Income(models.Model):
	R_Id = models.IntegerField(null=True)
	P_Id = models.IntegerField(null=True)
	FY = models.IntegerField(null=True)
	Other_Exempt_Income_Id = models.IntegerField(null=True)
	Other_Exempt_Income_Nature = models.IntegerField(null=True)
	Other_Exempt_Income_Amount = models.IntegerField(null=True)

class Shares_LT(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)
	totalFMV = models.IntegerField(null=True)

class Shares_ST(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)

class Equity_LT(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)
	totalFMV = models.IntegerField(null=True)

class Equity_ST(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)

class Debt_MF_LT(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Indexed_Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)

class Debt_MF_ST(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)

class Listed_Debentures_LT(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)

class Listed_Debentures_ST(models.Model):
	R_Id = models.IntegerField(null=True)
	Total_Sell_Value = models.IntegerField(null=True)
	Cost_of_Acquisition = models.IntegerField(null=True)
	Expenditure = models.IntegerField(null=True)

class House_Property_Details(models.Model):
	R_Id = models.IntegerField(null=True)
	Property_Id = models.IntegerField(null=True)
	Name_of_the_Premises_Building_Village = models.CharField(max_length=250,null=True)
	Road_Street_Post_Office = models.CharField(max_length=250,null=True)
	Area_Locality = models.CharField(max_length=250,null=True)
	Town_City = models.CharField(max_length=250,null=True)
	State = models.CharField(max_length=250,null=True)
	Country = models.CharField(max_length=250,null=True)
	PIN_Code = models.CharField(max_length=250,null=True)
	Your_percentage_of_share_in_Property = models.IntegerField(null=True)
	Type_of_Hosue_Property = models.CharField(max_length=250,null=True)
	Rent_Received = models.IntegerField(null=True)
	Rent_Cannot_be_Realised = models.CharField(max_length=250,null=True)
	Property_Tax = models.IntegerField(null=True)
	Name_of_Tenant = models.CharField(max_length=250,null=True)
	PAN_of_Tenant = models.CharField(max_length=250,null=True)
	Interest_on_Home_loan = models.IntegerField(null=True)

class Property_Owner_Details(models.Model):
	R_Id = models.IntegerField(null=True)
	Property_Id = models.IntegerField(null=True)
	co_owner_no=models.CharField(max_length=10,null=True)
	Name_of_Co_Owner = models.CharField(max_length=250,null=True)
	PAN_of_Co_owner = models.CharField(max_length=250,null=True)
	Percentage_Share_in_Property = models.IntegerField(null=True)

class salatclient(models.Model):
	PAN = models.CharField(max_length=20,null=True)
	Is_salat_client = models.CharField(max_length=20,null=True)
	Is_ERI_reg = models.CharField(max_length=20,null=True)
	ERI_id = models.CharField(max_length=20,null=True)
	Reg_date = models.DateTimeField(default=timezone.now)

class tds_tcs(models.Model):
	R_Id = models.CharField(max_length=20,null=True)
	part = models.CharField(max_length=20,null=True)
	Deductor_Collector_Name = models.CharField(max_length=100,null=True)
	TAN_PAN = models.CharField(max_length=20,null=True)
	section = models.CharField(max_length=20,null=True)
	no_of_transaction = models.IntegerField(null=True)
	amount_paid = models.IntegerField(null=True)
	tax_deducted = models.IntegerField(null=True)
	tds_tcs_deposited = models.IntegerField(null=True)
	year = models.CharField(max_length=20,null=True)

class tax_paid(models.Model):
	R_Id = models.IntegerField(null=True)
	major_head = models.IntegerField(null=True)
	minor_head = models.IntegerField(null=True)
	total_tax = models.IntegerField(null=True)
	BSR_code = models.IntegerField(null=True)
	deposit_date =  models.DateTimeField(null=True)
	challan_serial_no = models.IntegerField(null=True)

class refunds(models.Model):
	R_Id = models.CharField(max_length=20,null=True)
	Assessment_Year = models.CharField(max_length=20,null=True)
	mode = models.CharField(max_length=50,null=True)
	amount_of_refund = models.CharField(max_length=50,null=True)
	interest = models.CharField(max_length=50,null=True)
	payment_date = models.DateTimeField(null=True)
	
class Client_fin_info1(models.Model):
	company_name = models.CharField( max_length=100, null=True)
	company_address = models.CharField( max_length=200, null=True)
	employee_name = models.CharField( max_length=100, null=True)
	employee_address = models.CharField( max_length=200, null=True)
	employee_pan = models.CharField( max_length=30, null=True)
	ay = models.CharField( max_length=50, null=True)
	period_from = models.CharField( max_length=30, null=True)
	period_to = models.CharField( max_length=30, null=True)
	amount_paid_credited = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	amount_of_tax_deducted = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	company_pan = models.CharField( max_length=30, null=True)
	company_tan = models.CharField( max_length=30, null=True)
	salary_as_per_provision = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	value_of_perquisites = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	profits_in_lieu_of_salary = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	total_gross_salary = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	gross_salary_netof_allowance = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	nsc = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	hra_allowed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	medical_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	conveyance_allowance_allowed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	lta = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	lse = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	hra_claimed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	conveyance_allowance_claimed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	income_house_property = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	home_loan_interest = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	income_capital_gains = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	income_other_sources = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	gratuity = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	elss = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	epf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	ppf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	vpf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	lic = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	nsc_interest = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	superannuation = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	home_loan_principal = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	fd = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	ulip = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	child_edu_fee = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	sukanya_samriddhi_scheme = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	stamp_duty = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80ccc = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	nps_employee = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	nps_employer = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80ccd = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80ccf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80d_self_family = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80d_parents = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	medical_insurance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80e = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	number_80g = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	number_80tta = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	entertainment_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	profession_tax = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	via_deductions = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	basic_salary = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	dearness_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	personal_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	bonus = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	performance_pay = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	special_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	food_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	extra_salary = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	rgess = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	exemption_less_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	total_less_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	attire = models.DecimalField(max_digits=10, decimal_places=2, null=True)
	professional_pursuit = models.FloatField(null=True)
	telephone_allowance = models.DecimalField(max_digits=10, decimal_places=2, null=True)
	other_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	other_any_other_income = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	cid = models.IntegerField(null=True)
	emp_type = models.CharField(max_length=20, null=True)
	email = models.CharField(max_length=50, null=True)  # Field name made lowercase.
	form16_path = models.CharField(max_length=200, null=True)
	password = models.CharField(max_length=50, null=True)
	p_d = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	s_c = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	c = models.IntegerField(null=True)  # Field name made lowercase.
	r = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	self_house = models.CharField(max_length=10, null=True)
	hra_given = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	basic = models.FloatField(null=True)
	da = models.FloatField(null=True)  # Field name made lowercase.
	rent = models.FloatField(null=True)
	hra_given_value = models.FloatField(null=True)  # Field name made lowercase.
	stay = models.CharField(max_length=10, null=True)
	home_loan = models.CharField(max_length=10, null=True)
	interest_paid = models.FloatField(null=True)
	rent_earn = models.DecimalField(max_digits=10, decimal_places=2, null=True)
	disability = models.CharField(max_length=10, null=True)
	disability_type = models.CharField(max_length=10, null=True)
	time_stamp = models.DateTimeField()
	salat_tax = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	tax_leakage = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	flag = models.IntegerField(null=True)
	client_id = models.CharField(max_length=10, null=True)
# create TABLE salattax_app_tax_paid( R_Id int(6), major_head int(6), minor_head int(6), total_tax int(6), BSR_code int(6), deposit_date date, challan_serial_no int(6)  )
# ALTER TABLE salattax_app_tax_paid ADD COLUMN id INT AUTO_INCREMENT PRIMARY KEY FIRST;

class State_Details(models.Model):
	nse_state = models.CharField(max_length=50,null=True)
	bse_state = models.CharField(max_length=50,null=True)
	cvl_state = models.CharField(max_length=50,null=True)
	ckyc_state = models.CharField(max_length=50,null=True)
	state = models.CharField(max_length=200,null=True)

class Bank_List(models.Model):
	nse_bank_code = models.CharField(max_length=50,null=True)
	bank_name = models.CharField(max_length=50,null=True)

class State_Code(models.Model):
	code = models.CharField(max_length=50,null=True)
	state = models.CharField(max_length=200,null=True)

class Client_fin_info(models.Model):
	company_name = models.CharField( max_length=100, null=True)
	company_address = models.CharField( max_length=200, null=True)
	employee_name = models.CharField( max_length=100, null=True)
	employee_address = models.CharField( max_length=200, null=True)
	employee_pan = models.CharField( max_length=30, null=True)
	ay = models.CharField( max_length=50, null=True)
	period_from = models.CharField( max_length=30, null=True)
	period_to = models.CharField( max_length=30, null=True)
	amount_paid_credited = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	amount_of_tax_deducted = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	company_pan = models.CharField( max_length=30, null=True)
	company_tan = models.CharField( max_length=30, null=True)
	salary_as_per_provision = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	value_of_perquisites = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	profits_in_lieu_of_salary = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	total_gross_salary = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	gross_salary_netof_allowance = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	nsc = models.DecimalField( max_digits=20, decimal_places=2, null=True) 
	hra_allowed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	medical_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	conveyance_allowance_allowed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	lta = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	lse = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	hra_claimed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	conveyance_allowance_claimed = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	income_house_property = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	home_loan_interest = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	income_capital_gains = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	income_other_sources = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	gratuity = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	elss = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	epf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	ppf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	vpf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	lic = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	nsc_interest = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	superannuation = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	home_loan_principal = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	fd = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	ulip = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	child_edu_fee = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	sukanya_samriddhi_scheme = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	stamp_duty = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80ccc = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	nps_employee = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	nps_employer = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80ccd = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80ccf = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80d_self_family = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80d_parents = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	medical_insurance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	number_80e = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	number_80g = models.DecimalField(max_digits=20, decimal_places=2, null=True) 
	number_80tta = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	entertainment_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	profession_tax = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	via_deductions = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	basic_salary = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	dearness_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	personal_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	bonus = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	performance_pay = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	special_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	food_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	extra_salary = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	rgess = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	exemption_less_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	total_less_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	attire = models.DecimalField(max_digits=10, decimal_places=2, null=True)
	professional_pursuit = models.FloatField(null=True)
	telephone_allowance = models.DecimalField(max_digits=10, decimal_places=2, null=True)
	other_allowance = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	other_any_other_income = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	cid = models.CharField(max_length=10, null=True)
	emp_type = models.CharField(max_length=20, null=True)
	email = models.CharField(max_length=50, null=True)  # Field name made lowercase.
	form16_path = models.CharField(max_length=200, null=True)
	password = models.CharField(max_length=50, null=True)
	p_d = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	s_c = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	c = models.IntegerField(null=True)  # Field name made lowercase.
	r = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	self_house = models.CharField(max_length=10, null=True)
	hra_given = models.CharField(max_length=10, null=True)  # Field name made lowercase.
	basic = models.FloatField(null=True)
	da = models.FloatField(null=True)  # Field name made lowercase.
	rent = models.FloatField(null=True)
	hra_given_value = models.FloatField(null=True)  # Field name made lowercase.
	stay = models.CharField(max_length=10, null=True)
	home_loan = models.CharField(max_length=10, null=True)
	interest_paid = models.FloatField(null=True)
	rent_earn = models.DecimalField(max_digits=10, decimal_places=2, null=True)
	disability = models.CharField(max_length=10, null=True)
	disability_type = models.CharField(max_length=10, null=True)
	time_stamp = models.DateTimeField()
	salat_tax = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	tax_leakage = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	flag = models.IntegerField(null=True)

class Donation_Details(models.Model):
	R_Id = models.CharField(max_length=20,null=True)
	donation_id = models.CharField(max_length=20,null=True)
	donee_name = models.CharField(max_length=120,null=True)
	address = models.CharField(max_length=250,null=True)
	city_town = models.CharField(max_length=20,null=True)
	state_code = models.CharField(max_length=20,null=True)
	pin_code = models.CharField(max_length=20,null=True)
	donee_pan = models.CharField(max_length=20,null=True)
	amt_of_donation = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	donation_percent = models.CharField(max_length=20,null=True)

class computation(models.Model):
	R_Id = models.CharField(max_length=50,null=True)
	salary_income = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	property_income = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	capital_gains = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	gross_total_income = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	total_deduction = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	taxable_income = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	IT_normal_rates = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	IT_special_rates = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	rebate = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	surcharge = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	add_education_cess = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	total_tax = models.DecimalField(max_digits=20, decimal_places=2, null=True)#
	interest_234A = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	interest_234B = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	interest_234C = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	interest_234F = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	interest_on_tax = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	taxpayable_refund = models.DecimalField(max_digits=20, decimal_places=2, null=True)

class temp_ITR_value(models.Model):
	PAN = models.CharField(max_length=20,null=True)
	year = models.CharField(max_length=20,null=True)
	ITR = models.CharField(max_length=20,null=True)
	income_from_salary = models.CharField(max_length=20,null=True)
	no_of_hp = models.CharField(max_length=20,null=True)
	income_from_hp = models.CharField(max_length=20,null=True)
	interest_payable_borrowed_capital = models.CharField(max_length=20,null=True)
	capital_gain = models.CharField(max_length=20,null=True)
	schedule_si_income = models.CharField(max_length=20,null=True)
	income_from_other_src = models.CharField(max_length=20,null=True)
	business_or_profession = models.CharField(max_length=20,null=True)
	interest_gross = models.CharField(max_length=20,null=True)
	gross_total_income = models.CharField(max_length=20,null=True)
	deduction = models.CharField(max_length=20,null=True)
	c_80 = models.CharField(max_length=20,null=True)
	d_80 = models.CharField(max_length=20,null=True)
	tta_80 = models.CharField(max_length=20,null=True)
	ccd_1b_80 = models.CharField(max_length=20,null=True)
	ccg_80 = models.CharField(max_length=20,null=True)
	e_80 = models.CharField(max_length=20,null=True)
	ee_80 = models.CharField(max_length=20,null=True)
	g_80 = models.CharField(max_length=20,null=True)
	gg_80 = models.CharField(max_length=20,null=True)
	ccd_2_80 = models.CharField(max_length=20,null=True)
	total_income = models.CharField(max_length=20,null=True)
	schedule_si_tax = models.CharField(max_length=20,null=True)
	total_tax_interest = models.CharField(max_length=20,null=True)
	tax_paid = models.CharField(max_length=20,null=True)
	adv_tax_self_ass_tax = models.CharField(max_length=20,null=True)
	total_tcs = models.CharField(max_length=20,null=True)
	total_tds_ITR = models.CharField(max_length=20,null=True)
	total_tds_26AS = models.CharField(max_length=20,null=True)

class Investment_Details(models.Model):
	P_Id = models.IntegerField(null=True)
	year_of_first_share_sold = models.CharField(max_length=100,null=True)

class Modules(models.Model):
	module_Id = models.IntegerField(null=True)
	module_name = models.CharField(max_length=100,null=True)

class Invoice_Details(models.Model):
	client_id = models.IntegerField(null=True)
	invoice_no = models.CharField(max_length=100,null=True)
	invoice_date = models.DateTimeField(default=timezone.now)
	invoice_amount = models.IntegerField(null=True)
	CGST = models.CharField(max_length=100,null=True)
	SGST = models.CharField(max_length=100,null=True)
	IGST = models.CharField(max_length=100,null=True)
	discount = models.CharField(max_length=100,null=True)
	offer_code = models.CharField(max_length=100,null=True)
	transaction_id = models.CharField(max_length=100,null=True)
	payment_status = models.CharField(max_length=100,null=True)
	mode_of_payment = models.CharField(max_length=100,null=True)

class Module_Subscription(models.Model):
	client_id = models.IntegerField(null=True)
	business_partner_id = models.ForeignKey(Business_Partner,null=True)
	module_Id =  models.IntegerField(null=True)
	invoice_no = models.CharField(max_length=100,null=True)
	year = models.IntegerField(null=True)
	module_cost = models.CharField(max_length=100,null=True)
	module_detail1 = models.CharField(max_length=100,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class Partner_Settlement(models.Model):
	module_subscription_id=models.ForeignKey(Module_Subscription,null=True)
	partner_share= models.CharField(max_length=10,null=True,blank=True)
	settlement_status=models.CharField(max_length=10,null=True,blank=True)

class Revenue_Sharing(models.Model):
	business_partner_id = models.ForeignKey(Business_Partner,null=True)
	module_id = models.ForeignKey(Modules,null=True)
	partner_share=models.CharField(max_length=50,null=True,blank=True)

class child_info(models.Model):
	p_id = models.IntegerField(null=True)
	child_no =  models.CharField(max_length=20,null=True)
	child_type = models.CharField(max_length=50,null=True)
	Child_dob = models.DateField(null=True)
	Child_age = models.CharField(max_length=50,null=True)

class accommodation_details(models.Model):
	p_id = models.IntegerField(null=True)
	FY =  models.CharField(max_length=20,null=True)
	type_of_accommodation = models.CharField(max_length=50,null=True)
	current_place = models.CharField(max_length=50,null=True)
	annual_rent_paid = models.CharField(max_length=50,null=True)

class details_80c(models.Model):
	R_id = models.IntegerField(null=True)
	elss =  models.CharField(max_length=20,null=True)
	epf = models.CharField(max_length=50,null=True)
	bank_fd = models.CharField(max_length=50,null=True)
	school_fee = models.CharField(max_length=50,null=True)
	ppf = models.CharField(max_length=50,null=True)
	life_insurance = models.CharField(max_length=50,null=True)
	ulip = models.CharField(max_length=50,null=True)
	nsc = models.CharField(max_length=20,null=True)
	sukanya_samriddhi_scheme = models.CharField(max_length=20,null=True)
	post_office_deposit = models.CharField(max_length=20,null=True)
	repayment_principal_home_loan = models.CharField(max_length=20,null=True)
	stamp_duty = models.CharField(max_length=20,null=True)
	home_loan = models.CharField(max_length=20,null=True)

class home_loan(models.Model):
	R_id = models.IntegerField(null=True)
	principal_amount =  models.CharField(max_length=20,null=True)
	interest_amount = models.CharField(max_length=50,null=True)
	rate = models.CharField(max_length=50,null=True)

class details_80d(models.Model):
	R_id = models.IntegerField(null=True)
	mi_self =  models.CharField(max_length=20,null=True)
	mi_parents = models.CharField(max_length=20,null=True)
	mc_self = models.CharField(max_length=20,null=True)
	mc_parents = models.CharField(max_length=20,null=True)
	me_self = models.CharField(max_length=20,null=True)

class allowances(models.Model):
	R_id = models.IntegerField(null=True)
	company_name = models.CharField(max_length=250,null=True)
	entertainment_allowance =  models.CharField(max_length=20,null=True)
	lta = models.CharField(max_length=50,null=True)
	hra = models.CharField(max_length=50,null=True)
	other_allowance = models.CharField(max_length=50,null=True)
	travel_expense_incurred = models.CharField(max_length=50,null=True)
	gratuity= models.CharField(max_length=50,null=True)
	earned_leave_encashment= models.CharField(max_length=50,null=True)

class ERI_user(models.Model):
	eri_name = models.CharField(max_length=20,null=True)
	eri_pan = models.CharField(max_length=20,null=True)
	eri_dob = models.DateField(null=True)
	password = models.CharField(max_length=20,null=True)
	user_id = models.CharField(max_length=20,null=True)
	status = models.CharField(max_length=20,null=True)
	user_pan = models.CharField(max_length=50,null=True)
	date_time = models.DateTimeField(null=True)

class salat_registration(models.Model):
	pan = models.CharField(max_length=20,null=True)
	date_time = models.DateTimeField(null=True)
	login = models.CharField(max_length=20,null=True)
	is_salat_client = models.CharField(max_length=100,null=True)
	otp_trigger = models.CharField(max_length=20,null=True)
	otp_submitted = models.CharField(max_length=20,null=True)
	itr_count = models.CharField(max_length=20,null=True)
	downloaded_itr_count = models.CharField(max_length=20,null=True)
	processed_itr_count = models.CharField(max_length=20,null=True)
	downloaded_26as_count = models.CharField(max_length=20,null=True)
	processed_26as_count = models.CharField(max_length=20,null=True)

# class Investment_Details(models.Model):
# 	P_Id = models.IntegerField(null=True)
# 	year_of_first_share_sold = models.CharField(max_length=100,null=True)

# Only for LAST_ACTIVITY = True
# class Salat_User_tracking(models.Model):
# 	REQUIRED_FIELDS = ('author',)
# 	author = models.ForeignKey(settings.AUTH_USER_MODEL)
# 	identifier = models.CharField(max_length=40, unique=True)
# 	USERNAME_FIELD = 'identifier'
# 	is_authenticated = models.CharField(max_length=20)
# 	# is_anonymous = models.ForeignKey( AnonymousUser )
# 	@property
# 	def is_anonymous(self):
# 		return True
	# def redeem(self, user=None):
	# 	try:
	# 		coupon_user = self.users.get(user=user)
	# 	except CouponUser.DoesNotExist:
	# 		try:  # silently fix unbouned or nulled coupon users
	# 			coupon_user = self.users.get(user__isnull=True)
	# 			coupon_user.user = user
	# 		except CouponUser.DoesNotExist:
	# 			coupon_user = CouponUser(coupon=self, user=user)
	# 	coupon_user.redeemed_at = timezone.now()
	# 	coupon_user.save()
	# 	redeem_done.send(sender=self.__class__, coupon=self)


# class Chapter(models.Model):
	# title = models.CharField(max_length=200, unique=True)
	# slug = models.SlugField(unique=True)
	# date_completed = models.DateTimeField(blank=True, null=True)

	# completed = models.BooleanField(default=False)

	# def __str__(self):
	#     return self.title

	# def get_absolute_url(self):
	#     return reverse("course:subchapter_list", kwargs={"pk": self.pk})

# class SubChapter(models.Model):
	# chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE)
	# title = models.CharField(max_length=200, unique=True)
	# slug = models.SlugField(unique=True)

	# completed = models.BooleanField(default=False)

	# def __str__(self):
	#     return self.title

# class SubSection(models.Model):
	# sub_chapter = models.ForeignKey(SubChapter, on_delete=models.CASCADE)
	# title = models.CharField(max_length=200, unique=True)
	# slug = models.SlugField(unique=True)
	# text = models.TextField(null=True, blank=False)

	# completed = models.BooleanField(default=False)

	# def __str__(self):
	#     return self.title

	# def get_absolute_url(self):
	#     return reverse("course:detail",
	#                    kwargs={"slug": self.sub_chapter.slug,
	#                            "slug2": self.slug,
	#                            "pk": self.sub_chapter.chapter.pk,
	#                            }
	#                    )

# # ##################################
# class Visitor(models.Model):
	# session_key = models.CharField(max_length=40, primary_key=True)
	# user = models.ForeignKey(
	#     settings.AUTH_USER_MODEL,
	#     related_name='visit_history',
	#     null=True,
	#     editable=False,
	#     on_delete=models.CASCADE,
	# )
	# # Update to GenericIPAddress in Django 1.4
	# ip_address = models.CharField(max_length=39, editable=False)
	# user_agent = models.TextField(null=True, editable=False)
	# start_time = models.DateTimeField(default=timezone.now, editable=False)
	# expiry_age = models.IntegerField(null=True, editable=False)
	# expiry_time = models.DateTimeField(null=True, editable=False)
	# time_on_site = models.IntegerField(null=True, editable=False)
	# end_time = models.DateTimeField(null=True, editable=False)

	# objects = VisitorManager()

	# def session_expired(self):
	#     """The session has ended due to session expiration."""
	#     if self.expiry_time:
	#         return self.expiry_time <= timezone.now()
	#     return False
	# session_expired.boolean = True

	# def session_ended(self):
	#     """The session has ended due to an explicit logout."""
	#     return bool(self.end_time)
	# session_ended.boolean = True

	# @property
	# def geoip_data(self):
	#     """Attempt to retrieve MaxMind GeoIP data based on visitor's IP."""
	#     if not HAS_GEOIP or not TRACK_USING_GEOIP:
	#         return

	#     if not hasattr(self, '_geoip_data'):
	#         self._geoip_data = None
	#         try:
	#             gip = GeoIP(cache=GEOIP_CACHE_TYPE)
	#             self._geoip_data = gip.city(self.ip_address)
	#         except GeoIPException:
	#             msg = 'Error getting GeoIP data for IP "{0}"'.format(
	#                 self.ip_address)
	#             log.exception(msg)

	#     return self._geoip_data

	# class Meta(object):
	#     ordering = ('-start_time',)
	#     permissions = (
	#         ('view_visitor', 'Can view visitor'),
	#     )

# class Pageview(models.Model):
	# visitor = models.ForeignKey(
	#     Visitor,
	#     related_name='pageviews',
	#     on_delete=models.CASCADE,
	# )
	# url = models.TextField(null=False, editable=False)
	# referer = models.TextField(null=True, editable=False)
	# query_string = models.TextField(null=True, editable=False)
	# method = models.CharField(max_length=20, null=True)
	# view_time = models.DateTimeField()

	# objects = PageviewManager()

	# class Meta(object):
	#     ordering = ('-view_time',)

# https://github.com/bruth/django-tracking2/tree/master/tracking
# https://www.youtube.com/watch?v=18l6H-rg5uA
# https://analytics.google.com/analytics/web/provision/?authuser=0#/a132417121w192109310p187917689/admin/tracking/user-id/

class user_tracking(models.Model):
	session_key = models.CharField(max_length=50,null=True)
	session_count = models.CharField(max_length=20,null=True)
	client_id = models.CharField(max_length=50,null=True)
	user_id = models.CharField(max_length=50,null=True)
	event = models.CharField(max_length=20,null=True)
	event_name = models.CharField(max_length=200,null=True)
	time_on_site = models.CharField(max_length=20,null=True)
	device_platform = models.CharField(max_length=20,null=True)
	device_type = models.CharField(max_length=50,null=True)
	browser = models.CharField(max_length=100,null=True)
	created_time = models.DateTimeField(default=timezone.now, editable=False)
	class Meta:
		unique_together = ["session_key", "event_name", "time_on_site","created_time"]

class Mail_Trigger(models.Model):
	P_Id = models.IntegerField(null=True)
	mail_id = models.CharField(max_length=100,null=True)
	mail_content = models.CharField(max_length=100,null=True)
	sent = models.CharField(max_length=10,null=True)
	created_time = models.DateTimeField(default=timezone.now, editable=False)

class tax_variables(models.Model):
	R_Id = models.IntegerField(null=True)
	salary_income = models.CharField(max_length=10,null=True)
	house_property = models.CharField(max_length=10,null=True)
	business_or_profession = models.CharField(max_length=10,null=True)
	capital_gain = models.CharField(max_length=10,null=True)
	foreign_assets = models.CharField(max_length=10,null=True)
	taxable_income_exceed_50l = models.CharField(max_length=10,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)


class SalaryBreakUp(models.Model):
	R_Id = models.IntegerField(null=True)
	company_name = models.CharField(max_length=250,null=True)
	basic_salary = models.IntegerField(null=True)
	dearness_allowance = models.IntegerField(null=True)
	conveyance_allowance = models.IntegerField(null=True)
	annuity_or_pension = models.IntegerField(null=True)
	commuted_pension = models.IntegerField(null=True)
	gratuity = models.IntegerField(null=True)
	fees_commission = models.IntegerField(null=True)
	advance_of_salary = models.IntegerField(null=True)
	Other_allowances = models.IntegerField(null=True)
	leave_encashment = models.IntegerField(null=True)
	others = models.IntegerField(null=True)


class PerquisiteBreakUp(models.Model):
	R_Id = models.IntegerField(null=True)
	company_name = models.CharField(max_length=250,null=True)
	accommodation = models.IntegerField(null=True)
	cars_or_other_automotive = models.IntegerField(null=True)
	domestic_help = models.IntegerField(null=True)
	utilitiy_bills = models.IntegerField(null=True)
	concessional_loans = models.IntegerField(null=True)
	holiday_expenses = models.IntegerField(null=True)
	concessional_travel = models.IntegerField(null=True)
	free_meals = models.IntegerField(null=True)
	free_education = models.IntegerField(null=True)
	gifts_vouchers = models.IntegerField(null=True)
	credit_card_expenses = models.IntegerField(null=True)
	club_expenses = models.IntegerField(null=True)
	other_benefits = models.IntegerField(null=True)
	

class ProfitinLieuBreakUp(models.Model):
	R_Id = models.IntegerField(null=True)
	company_name = models.CharField(max_length=250,null=True)
	termination_compensation = models.IntegerField(null=True)
	keyman_insurance = models.IntegerField(null=True)
	other_profit_in_lieu_salary = models.IntegerField(null=True)
	other = models.IntegerField(null=True)

class TenantDetails(models.Model):
	R_Id = models.IntegerField(null=True)
	Property_Id = models.IntegerField(null=True)
	tenant_no=models.CharField(max_length=10,null=True)
	tenant_name = models.CharField(max_length=250,null=True)
	tenant_pan = models.CharField(max_length=250,null=True)

class RFChargesDetails(models.Model):
	return_complexity = models.CharField(max_length=250,null=True)
	charges = models.IntegerField(null=True)

class RFPaymentDetails(models.Model):
	R_Id = models.IntegerField(null=True)
	business_partner_id=models.ForeignKey(Business_Partner, on_delete=models.CASCADE)
	discount_code=models.CharField(max_length=10,null=True)
	basic_charge = models.IntegerField(null=True)
	form16_charge = models.IntegerField(null=True)
	HP_charge = models.IntegerField(null=True)
	CG_shares_charge = models.IntegerField(null=True)
	CG_MF_charge = models.IntegerField(null=True)
	expert_review_charge = models.IntegerField(null=True)
	phone_support_charge = models.IntegerField(null=True)
	express_filing_charge = models.IntegerField(null=True)

class Offer(models.Model):
	offer_code=models.CharField(max_length=100,null=True)
	offer_amount=models.CharField(max_length=100,null=True)
	expiry_date=models.DateTimeField(default=timezone.now)

class UserReferralCode(models.Model):
	salatax_user_id = models.ForeignKey(SalatTaxUser, on_delete=models.CASCADE)
	referral_code=models.CharField(max_length=100,null=True)

class Variables_80d(models.Model):
	R_Id = models.IntegerField(null=True)
	self_health_checkup=models.IntegerField(null=True)
	parent_health_checkup=models.IntegerField(null=True)
	self_insurance_premium=models.IntegerField(null=True)
	parent_insurance_premium=models.IntegerField(null=True)
	self_medical_expenditure=models.IntegerField(null=True)
	parent_medical_expenditure=models.IntegerField(null=True)

class CGTransactions(models.Model):
	businessPartnerId = models.ForeignKey(Business_Partner, on_delete=models.CASCADE)
	intermediaryId=models.CharField(max_length=10,null=True)
	clientId=models.ForeignKey(SalatTaxUser, on_delete=models.CASCADE,null=True)
	assetClass = models.CharField(max_length=250,null=True)
	scripSchemeCode = models.CharField(max_length=250,null=True)
	transactionType = models.CharField(max_length=250,null=True)
	transactionDate = models.CharField(max_length=250,null=True)
	qtyUnits = models.CharField(max_length=250,null=True)	
	priceNAV= models.CharField(max_length=250,null=True)
	amount = models.CharField(max_length=250,null=True)
	transactionCost = models.CharField(max_length=250,null=True)
	folioNo = models.CharField(max_length=250,null=True)

class mfSchemeAssetClass(models.Model):
	amc= models.CharField(max_length=250,null=True)
	schemeCode = models.CharField(max_length=250,null=True)
	productCode= models.CharField(max_length=250,null=True)
	schemeName= models.CharField(max_length=250,null=True)
	schemeType= models.CharField(max_length=250,null=True)
	taxType= models.CharField(max_length=250,null=True)

class CGCalculation(models.Model):
	businessPartnerId = models.ForeignKey(Business_Partner, on_delete=models.CASCADE)
	intermediaryId=models.CharField(max_length=250,null=True)
	clientId=models.ForeignKey(SalatTaxUser, on_delete=models.CASCADE)
	financialYear=models.CharField(max_length=250,null=True)
	cgTransactionId=models.ForeignKey(CGTransactions, on_delete=models.CASCADE)
	assetClass=models.CharField(max_length=250,null=True)
	scripSchemeCode=models.CharField(max_length=250,null=True)
	sellDate=models.DateField(null=True)
	sellQty=models.FloatField(null=True)
	sellPriceNAV=models.FloatField(null=True)
	sellAmount=models.FloatField(null=True)
	purchaseDate=models.DateField(null=True)
	purchasePriceNAV=models.FloatField(null=True)
	FMV=models.FloatField(null=True)
	costofAcquisition=models.FloatField(null=True)
	CGType=models.CharField(max_length=250,null=True)
	capitalGain=models.FloatField(null=True)
	purchaseAmount=models.FloatField(null=True)

class CGSummary(models.Model):
	businessPartnerId = models.ForeignKey(Business_Partner, on_delete=models.CASCADE)
	clientId=models.ForeignKey(SalatTaxUser, on_delete=models.CASCADE)
	financialYear=models.CharField(max_length=250,null=True)
	assetClass=models.CharField(max_length=250,null=True)
	CGType=models.CharField(max_length=250,null=True)
	totalSellAmount=models.FloatField(null=True)
	totalCostofAcquisition=models.FloatField(null=True)
	indexedCostofAcquisition=models.FloatField(null=True)
	expenditure=models.FloatField(null=True)
	totalCapitalGain=models.FloatField(null=True)
	totalFMV=models.FloatField(null=True)

class currentCostInflationIndex(models.Model):
	financialYear=models.CharField(max_length=250,null=True)
	costInflationIndex=models.FloatField(null=True)

class HighestPrice31012018(models.Model):
	productCode= models.CharField(max_length=250,null=True)
	highestPrice=models.FloatField(null=True)

class Mail_Send_Log(models.Model):
	business_partner_id = models.ForeignKey(Business_Partner, on_delete=models.CASCADE, null=True)
	p_id = models.ForeignKey(Personal_Details, on_delete=models.CASCADE, null=True)
	sender_mail = models.CharField(max_length=250, null=True)
	receiver_mail = models.CharField(max_length=250, null=True)
	status = models.CharField(max_length=250, null=True,blank=True)
	mail_type = models.CharField(max_length=250, null=True,blank=True)
	created = models.DateTimeField('created',default=timezone.now)