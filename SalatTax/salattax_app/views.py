# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404

from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
from django.views.generic import TemplateView,ListView # Import TemplateView
import json

import logging

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)


class HomePageView(TemplateView):
    template_name = "index.html"

class upload_form16(TemplateView):
    template_name = "upload_form16.html"

class signin(TemplateView):
    template_name = "signin.html"

# class personal_information(TemplateView):
#     template_name = "personal_information.html"

class client_registration(TemplateView):
    template_name = "client_registration.html"
    
# class other_details(TemplateView):
#     template_name = "other_details.html"

class income_details(TemplateView):
    template_name = "income_details.html"

class deductions(TemplateView):
    template_name = "deductions.html"

class exempt_income(TemplateView):
    template_name = "exempt_income.html"

class taxes_paid(TemplateView):
    template_name = "taxes_paid.html"
    
class NewReview(TemplateView):
    template_name = "NewReview.html"
    
class pricing(TemplateView):
    template_name = "pricing.html"
    
class contact(TemplateView):
    template_name = "contact.html"
    
class services(TemplateView):
    template_name = "services.html"
    
class camsKarvy(TemplateView):
    template_name = "camsKarvy.html"
    
class tsa(TemplateView):
    template_name = "tsa.html"
    
class dashboard(TemplateView):
    template_name = "dashboard.html"

class admin_dashboard(TemplateView):
    template_name = "admin_dashboard.html"

class partner_dashboard(TemplateView):
    template_name = "partner_dashboard.html"
    
class ResetPasswordView(TemplateView):
    template_name = "confirm_password.html"

class ResetUsernameView(TemplateView):
    template_name = "change_username.html"

class ResetUsernameView(TemplateView):
    template_name = "test_s.py"


class salattaxView(TemplateView):
    template_name = "one_time_registration.html"

class calculate_taxView(TemplateView):
    template_name = "calculate_tax.html"   

class financial_goalView(TemplateView):
    template_name = "financial_goal.html"

class tax_reportView(TemplateView):
    template_name = "tax_report.html" 

class subscriptionView(TemplateView):
    template_name = "subscription.html" 


class tax_saving_accountView(TemplateView):
    template_name = "tax_saving_account.html" 

class become_a_partnerView(TemplateView):
    template_name = "partner_onboard.html"                

class tax_planView(TemplateView):
    template_name = "tax_plan.html"

class success_pageView(TemplateView):
    template_name = "success_page.html" 
    
class tax_anaytices_reportView(TemplateView):
    template_name = "tax_analytics_report.html"

class tax_plan_reportView(TemplateView):
    template_name = "tax_plan_year.html" 

class tax_recomView(TemplateView):
    template_name = "tax_recom.html"    

class tax_saving_reportView(TemplateView):
    template_name = "tax_saving_report.html"

class tax_return_reportView(TemplateView):
    template_name = "tax_return_report.html"

class subscribe_payumoneyView(TemplateView):
    template_name = "subscribe_payumoney.html"
    
class successView(TemplateView):
    template_name = "success.html"

class FailureView(TemplateView):
    template_name = "Failure.html"

class payumoneyView(TemplateView):
    template_name = "salat_info.html"

class salattax_dashboardView(TemplateView):
    template_name = "salattax_dashboard.html"

class partner_depositView(TemplateView):
    template_name = "partner_deposit.html"

class cheque_payment_manualView(TemplateView):
    template_name = "cheque_payment_manual.html"


# @login_required
def home(request):
    return render(request, 'core/home.html')


# def client_registration(request,data):
#     return render(request,'client_registration.html',
#     {
#     'pan':data,
#     })


# def client_registration(request,data):
#     test_json = json.loads(data)
#     return render(request,'client_registration.html', {'client': test_json})

