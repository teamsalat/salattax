from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_protect,csrf_exempt
from array import *
from django.db.models import Q

from .models import SalatTaxUser, Business, Business_Partner, SalatTaxRole, Personal_Details, Address_Details, Return_Details, Original_return_Details, Employer_Details, Bank_Details, Form16, Income, Other_Income_Common, Other_Income_Rare, Exempt_Income, VI_Deductions, Agriculture_Income, Other_Exempt_Income, Shares_LT, Shares_ST, Equity_LT, Equity_ST, Debt_MF_LT, Debt_MF_ST, Listed_Debentures_LT, Listed_Debentures_ST, House_Property_Details, Property_Owner_Details, salatclient, tds_tcs, tax_paid, refunds, Client_fin_info1, State_Details, Bank_List, Client_fin_info, Donation_Details, computation, Investment_Details, Module_Subscription, salat_registration, Partner_Deposit_Details, Partner_Deposit_Balance
from .models import Invoice_Details, Module_Subscription, RFPaymentDetails, Modules, Revenue_Sharing
import re

# wand
from wand.image import Image as Image_wand
from wand.color import Color as Wand_Color

#for url request
import requests
import xml.etree.ElementTree as ET

# Python logging package
import logging
import json
import os, sys
import traceback
import base64
import os.path
from PIL import Image
from django.db.models.fields import DateField
from django.db.models.functions import Now

from .index import *
from .path import *
from .mail import *

xml_path=static_path+'/xml'

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

## http://127.0.0.1:8000/personal_information/salattax_user_id

@csrf_exempt
def dashboard(request):
	log.info('dashboard')

	result={}
	try:

		partner_id=0
		user_id=0
		username = request.session['login']
		user_type=''
		partner_type=''
		balance=0
		partner_name=''

		log.info(username)

		if User.objects.filter(username=username).exists():
			atuser = User.objects.get(username=username)
			if SalatTaxUser.objects.filter(user=atuser).exists():
				SalatTaxUser_Obj=SalatTaxUser.objects.get(user=atuser)
				user_id=SalatTaxUser_Obj.id
				user_role=SalatTaxUser_Obj.role.rolename
				

				if user_role!=None and user_role=='Client':
					user_type='client'
				elif user_role!=None and (user_role=='Admin' or user_role=='Partner'):
					user_type='business'
					partner_type=user_role

					if Business_Partner.objects.filter(user_id=SalatTaxUser_Obj).exists():
						bp_inst=Business_Partner.objects.filter(user_id=SalatTaxUser_Obj.id).latest("id")
						partner_id=bp_inst.id
						partner_name=bp_inst.name
						log.info(partner_id)

						Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst)

						if Deposit_Balance_obj.exists():
							Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst).latest('id')

							balance=Deposit_Balance_obj.deposit_balance

		log.info(user_id)
		log.info(partner_id)
		log.info(user_type)

		if(user_id!=0 and partner_id!=0):

			if partner_type=='Admin':

				return render(request,'admin_dashboard.html',{
					'b_id':partner_id,
					'user_id':user_id,
					'username':username,
					'user_type':user_type,
					'business_type':partner_type,
					'deposit_balance':balance,
					'partner_name':partner_name
								
				})
			else:
				return render(request,'dashboard.html',{
					'b_id':partner_id,
					'user_id':user_id,
					'username':username,
					'user_type':user_type,
					'business_type':partner_type,
					'deposit_balance':balance,
					'partner_name':partner_name
								
				})

		else:
			request.session['login']=''
			return  redirect('/')


	except Exception as ex:
		log.info('Error in dashboard '+request.get_host()+' : %s' % traceback.format_exc())
		request.session['login']=''
		return  redirect('/')


@csrf_exempt
def partner_dashboard(request,id):
	log.info('partner_dashboard')
	log.info(id)

	result={}
	try:

		partner_id=0
		user_id=0
		bp_id=id
		user_name = ''
		user_type=''
		partner_type=''
		balance=0
		partner_name=''

		if Business_Partner.objects.filter(id=bp_id).exists():
			bp_inst=Business_Partner.objects.filter(id=bp_id).latest("id")
			partner_id=bp_inst.id
			partner_name=bp_inst.name
			SalatTaxUser_id=bp_inst.user_id.id
			log.info(partner_id)    

			if SalatTaxUser.objects.filter(id=SalatTaxUser_id).exists():
				SalatTaxUser_Obj=SalatTaxUser.objects.get(id=SalatTaxUser_id)
				user_id=SalatTaxUser_Obj.id
				user_role=SalatTaxUser_Obj.role.rolename

				if User.objects.filter(username=SalatTaxUser_Obj.user).exists():
					atuser = User.objects.get(username=SalatTaxUser_Obj.user)
					user_name=atuser.username

				if user_role!=None and user_role=='Client':
					user_type='client'
				elif user_role!=None and (user_role=='Admin' or user_role=='Partner'):
					user_type='business'
					partner_type=user_role

			Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst)
			if Deposit_Balance_obj.exists():
				Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst).latest('id')
				balance=Deposit_Balance_obj.deposit_balance

		log.info(user_id)
		log.info(user_name)
		log.info(partner_id)
		log.info(user_type)

		if(user_id!=0 and partner_id!=0):

			return render(request,'partner_dashboard.html',{
				'b_id':partner_id,
				'user_id':user_id,
				'username':user_name,
				'user_type':user_type,
				'business_type':partner_type,
				'deposit_balance':balance,
				'partner_name':partner_name
							
			})

		# else:
		#     request.session['login']=''
		#     return  redirect('/')


	except Exception as ex:
		log.info('Error in partner_dashboard '+request.get_host()+' : %s' % traceback.format_exc())
		# request.session['login']=''
		# return  redirect('/')


@csrf_exempt
def pagination(request):

	log.info('pagination')

	##log.info(request.get_host())

	result={}
	try:
		bp_id=request.POST.get('b_id')
		current_page=request.POST.get('current_page')
		search_input=request.POST.get('input')
		keyword=request.POST.get('keyword')
		Assessment_Year=request.POST.get('AssessmentYear')

		log.info(Assessment_Year)
		# log.info(current_page)
		# log.info(search_input)
		# log.info(keyword)

		bp_inst=Business_Partner.objects.filter(id=bp_id).latest("id")

		bp_type=bp_inst.user_id.role.rolename
		main_business_id=bp_inst.main_business.id

		if(bp_inst.partner_id=='0'):
			##If Business_Partner is Admin
			businessPartnerObjArr=Business_Partner.objects.filter(main_business=main_business_id)
			business_partner_arr=Business_Partner.objects.filter(main_business=main_business_id).values_list('id')
		else:
			##If Business_Partner is only partner
			businessPartnerObjArr=Business_Partner.objects.filter(id=bp_id)
			business_partner_arr=Business_Partner.objects.filter(id=bp_id).values_list('id')


		result['data_counts']=get_partner_data(business_partner_arr,Assessment_Year)
		
		if keyword=='onload':
			result['status']=getting_client_data(request,business_partner_arr,current_page,Assessment_Year,bp_type)
		elif keyword=='search':
			result['status']=search_dashbaord_data(request,bp_id,search_input,current_page,Assessment_Year)

		##log.info(result)


		return HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		log.info('Error in fetch dashboard data'+request.get_host()+' : %s' % traceback.format_exc())

@csrf_exempt
def search_client_on_dashboard(request):
	result={}
	try:
		b_id=request.POST.get('b_id')
		search_input=request.POST.get('input')
		Assessment_Year=request.POST.get('AssessmentYear')

		log.info(request)

		result['status']=search_dashbaord_data(request,b_id,search_input,1,Assessment_Year)
		
		return HttpResponse(json.dumps(result),content_type='application/json')
		
	except Exception as ex:
		log.error('Error in search_client_on_dashboard: '+request.get_host()+' : %s' % traceback.format_exc())

def search_dashbaord_data(request,bp_id,search_input,current_page,Assessment_Year):
	result={}
	try:

		bp_inst=Business_Partner.objects.filter(id=bp_id).latest("id")

		bp_type=bp_inst.user_id.role.rolename
		main_business_id=bp_inst.main_business.id

		if(bp_inst.partner_id=='0'):
			##If Business_Partner is Admin
			businessPartnerObjArr=Business_Partner.objects.filter(main_business=main_business_id)
			business_partner_arr=Business_Partner.objects.filter(main_business=main_business_id).values_list('id')
		else:
			##If Business_Partner is only partner
			businessPartnerObjArr=Business_Partner.objects.filter(id=bp_id)
			business_partner_arr=Business_Partner.objects.filter(id=bp_id).values_list('id')

		space_count=0
		search_input = search_input.replace('  ',' ')
		search_input = search_input.replace(' ','|')

		per_page_count=10
		limit = int(per_page_count) * int(current_page)
		offset = int(limit) - int(per_page_count)

		log.info(offset)
		log.info(limit)

		pan_data=Personal_Details.objects.filter(Q(business_partner_id__in=business_partner_arr) & (Q(name__contains=search_input) | Q(name=search_input) | Q(pan__contains=search_input) | Q(pan=search_input)))[offset:limit]

		##No of PANs
		pan_count=pan_data.count()

		log.info(pan_count)

		total_pages = pan_count / per_page_count
		if pan_count % per_page_count != 0:
			total_pages += 1 # adding one more page if the last page will contains less contacts 

		pagination = make_pagination_html(current_page, total_pages,'search')

		result['listing']=pagination
		result['pan_count']=pan_count

		i=0
		panDict={}
		panDict=get_dhashboard_data(pan_data,Assessment_Year,current_page,bp_type)

		result['search_data']=panDict
		#log.info(result)

		return result

	except Exception as ex:
		log.error("Error in search_dashbaord_data: %s" % traceback.format_exc())

@csrf_exempt
def mark_filed(request):
	log.info('mark_filed')
	result={}
	try:
		bp_id=request.POST.get('b_id')
		rid=int(request.POST.get('rid'))
		current_page=int(request.POST.get('current_page'))
		search_input=request.POST.get('input')
		keyword=request.POST.get('keyword')
		Assessment_Year=request.POST.get('AssessmentYear')

		if Return_Details.objects.filter(R_id=rid).exists():
			return_obj=Return_Details.objects.filter(R_id=rid).latest('id')
			return_obj.xml_uploaded=Now()
			return_obj.save()

		bp_inst=Business_Partner.objects.filter(id=bp_id).latest("id")

		bp_type=bp_inst.user_id.role.rolename
		main_business_id=bp_inst.main_business.id

		if(bp_inst.partner_id=='0'):
			##If Business_Partner is Admin
			businessPartnerObjArr=Business_Partner.objects.filter(main_business=main_business_id)
			business_partner_arr=Business_Partner.objects.filter(main_business=main_business_id).values_list('id')
		else:
			##If Business_Partner is only partner
			businessPartnerObjArr=Business_Partner.objects.filter(id=bp_id)
			business_partner_arr=Business_Partner.objects.filter(id=bp_id).values_list('id')

		if keyword=='onload':
			result['status']=getting_client_data(request,business_partner_arr,current_page,Assessment_Year,bp_type)
		
		return HttpResponse(json.dumps(result), content_type='application/json')
		
	except Exception as ex:
		log.error('Error in mark_filed: '+request.get_host()+' : %s' % traceback.format_exc())

def getting_client_data(request,business_partner_arr,current_page,Assessment_Year,bp_type):
	result={}
	try:
		per_page_count=10

		limit = int(per_page_count) * int(current_page)
		offset = int(limit) - int(per_page_count)

		log.info(offset)
		log.info(limit)

		pan_data=Personal_Details.objects.filter(business_partner_id__in=business_partner_arr).order_by('-updated_time')[offset:limit]

		##No of PANs
		pan_count=Personal_Details.objects.filter(business_partner_id__in=business_partner_arr).count()

		total_pages = pan_count / per_page_count
		if pan_count % per_page_count != 0:
			total_pages += 1 # adding one more page if the last page will contains less contacts 
			
		pagination = make_pagination_html(current_page, total_pages,'onload')

		result['listing']=pagination
		result['pan_count']=pan_count

		i=0
		panDict={}
		panDict=get_dhashboard_data(pan_data,Assessment_Year,current_page,bp_type)

		result['search_data']=panDict

		##log.info(result)

		return result
	except Exception as ex:
		log.error("Error in getting client data: %s" % traceback.format_exc())
		return result

def get_dhashboard_data(pan_data,Assessment_Year,current_page,bp_type):
	log.info('get_dhashboard_data')
	panDict={}
	try:

		cp=str(current_page)

		Module_obj=Modules.objects.get(module_name='Return Filing')
		ModuleId=Module_obj.module_Id

		update_ay()

		i=0
		if pan_data.exists():
			# log.info('exists')
			for pobj in pan_data:
				pdata={} 
				#pdata['pan']=pobj.pan

				# if pobj.pan=='DFGRE5456G':
				# 	return_submission_mail_partner_to_client(pobj.id)
				# 	return_submission_mail_to_partner(pobj.id)
				# 	return_submission_mail_to_salat(pobj.id,'ITR-1','')

				name=pobj.name
				if name!=None:
					name=name.replace('|',' ')
				else:
					name=''

				if pobj.pan!=None:
					pan=pobj.pan
				else:
					pan=''

				client_url=salat_url+'/personal_information/'+str(pobj.P_Id)
				
				pan_link='<a href="'+client_url+'" target="_blank"><strong><u>'+pan+'</u></strong></a>'
				pdata['pan']=pan_link

				#client_name='<a href="'+client_url+'" target="_blank"><strong><u>'+name+'</u></strong></a>'    
				pdata['client_name']=name

				partner_url=salat_url+'/partner_dashboard/'+str(pobj.business_partner_id.id)
				partnername=pobj.business_partner_id.partner_business_name
				partner_name='<a href="'+partner_url+'" target="_blank"><strong><u>'+partnername+'</u></strong></a>'    
				pdata['partner_name']=partner_name

				pdata['salat_tax_registration']='N'
				pdata['as26_processing']='N'
				if salat_registration.objects.filter(pan=pobj.pan).exists():

					salat_regi_obj=salat_registration.objects.filter(pan=pobj.pan).latest('id')
					IsSalatClient=salat_regi_obj.is_salat_client
					if IsSalatClient!=None:
						if IsSalatClient=='Salat Client':
							pdata['salat_tax_registration']='Y'

					Processed26asCount=salat_regi_obj.processed_26as_count
					if Processed26asCount!=None:
						if Processed26asCount!='':
							pdata['as26_processing']='Y'


				xmlGenerated='N'
				returnFiling='-'
				itrVarification='N'
				paymentStatus='-'
				expert_assisted='N'

				xml_file_name=pobj.pan+'_ITR-1_2019_N_1234.xml'

				if Return_Details.objects.filter(P_id=pobj.id,AY=Assessment_Year).exists():
					paymentStatus='Pending'
					return_obj=Return_Details.objects.filter(P_id=pobj.id,AY=Assessment_Year)
					R_id_list=Return_Details.objects.filter(P_id=pobj.id,AY=Assessment_Year).values_list('R_id')
					#log.info(return_obj.count())

					if return_obj.count()==1:
						rid=return_obj[0].R_id
						if return_obj[0].xml_generated!=None:
							if bp_type=='Admin':
								xmlGeneratedTime=str(return_obj[0].xml_generated)[0:16]
								xml_file=xml_path+'/'+xml_file_name
								if os.path.exists(xml_file):
									xml_url=salat_url+'/'+xml_down_path+'/'+xml_file_name
									xmlGenerated='<a href="'+xml_url+'" download><strong><u>XML</u></strong></a> '+xmlGeneratedTime    
								else:
									xmlGenerated=str(return_obj[0].xml_generated)[0:16]
							else:
								xmlGenerated='Y'
							if return_obj[0].xml_uploaded!=None:
								if bp_type=='Admin':
									returnFiling=str(return_obj[0].xml_uploaded)[0:16]
								else:
									returnFiling='Y'
							else:
								if bp_type=='Admin':
									returnFiling='<button type="button" class="btn info mark_btn" ng-click="MarkFiled('+str(rid)+','+cp+')">MarkFiled</button>'
								else:
									returnFiling='N'

						if return_obj[0].return_e_verified!=None and return_obj[0].return_e_verified=='Y':
							itrVarification='Y'

						if RFPaymentDetails.objects.filter(R_Id=return_obj[0].R_id).exists():
							RFPayment_Obj=RFPaymentDetails.objects.filter(R_Id=return_obj[0].R_id).latest('id')
							if Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exists():
								Module_Subscription_Obj=Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exclude(Q(invoice_no=None) | Q(invoice_no=''))
								if Module_Subscription_Obj.exists():
									InvoiceNo=Module_Subscription_Obj[0].invoice_no
									if Invoice_Details.objects.filter(invoice_no=InvoiceNo).exists():
										Invoice_Obj=Invoice_Details.objects.filter(invoice_no=InvoiceNo).latest('id')
										if Invoice_Obj.payment_status!=None and Invoice_Obj.payment_status=='success':
											paymentStatus=Invoice_Obj.mode_of_payment
											if RFPayment_Obj.expert_review_charge!=None and RFPayment_Obj.expert_review_charge!=0:
												expert_assisted='Y'
					
					if return_obj.count()>1:
						xmlGenerated=''
						returnFiling=''
						itrVarification=''
						paymentStatus=''
						expert_assisted=''
						for retnobj in return_obj:
							#xmlGenerated
							rid=retnobj.R_id
							if retnobj.xml_generated!=None:
								if xmlGenerated=='':
									if bp_type=='Admin':
										xmlGeneratedTime=str(retnobj.xml_generated)[0:16]
										xml_file=xml_path+'/'+xml_file_name
										if os.path.exists(xml_file):
											xml_url=salat_url+'/'+xml_down_path+'/'+xml_file_name
											xmlGenerated='<a href="'+xml_url+'" download><strong><u>XML</u></strong></a> '+xmlGeneratedTime    
										else:
											xmlGenerated=str(retnobj.xml_generated)[0:16]
									else:
										xmlGenerated='Y'
								else:
									if bp_type=='Admin':
										xmlGeneratedTime=str(retnobj.xml_generated)[0:16]
										xml_file=xml_path+'/'+xml_file_name
										if os.path.exists(xml_file):
											xml_url=salat_url+'/'+xml_down_path+'/'+xml_file_name
											xmlGenerated=xmlGenerated+'<br><a href="'+xml_url+'" download><strong><u>XML</u></strong></a> '+xmlGeneratedTime    
										else:
											xmlGenerated=xmlGenerated+'<br>'+str(retnobj.xml_generated)[0:16]
									else:
										xmlGenerated=xmlGenerated+'<br>Y'

								#returnFiling
								if retnobj.xml_uploaded!=None:
									if returnFiling=='':
										if bp_type=='Admin':
											returnFiling=str(retnobj.xml_uploaded)[0:16]
										else:
											returnFiling='Y'
									else:
										if bp_type=='Admin':
											returnFiling=returnFiling+'<br>'+str(retnobj.xml_uploaded)[0:16]
										else:
											returnFiling=returnFiling+'<br>Y'
								else:
									if returnFiling=='':
										if bp_type=='Admin':
											returnFiling='<button type="button" class="btn info mark_btn" ng-click="MarkFiled('+str(rid)+','+cp+')">MarkFiled</button>'
										else:
											returnFiling='N'
									else:
										if bp_type=='Admin':
											returnFiling=returnFiling+'<br><button type="button" class="btn info mark_btn" ng-click="MarkFiled('+str(rid)+','+cp+')">MarkFiled</button>'
										else:
											returnFiling=returnFiling+'<br>N'

							else:		
								if xmlGenerated=='':
									xmlGenerated='N'
								else:
									xmlGenerated=xmlGenerated+'<br>N'

								#returnFiling
								if returnFiling=='':
									returnFiling='-'
								else:
									returnFiling=returnFiling+'<br>-'

							##returnFiling
							# if retnobj.xml_uploaded!=None:
							# 	if returnFiling=='':
							# 		returnFiling=str(retnobj.xml_uploaded)
							# 	else:
							# 		returnFiling=returnFiling+'<br>'+str(retnobj.xml_uploaded)
							# else:
							# 	if returnFiling=='':
							# 		returnFiling='N'
							# 	else:
							# 		returnFiling=returnFiling+'<br>N'

							#itrVarification
							if retnobj.return_e_verified!=None:
								if retnobj.return_e_verified=='Y':
									if itrVarification=='':
										itrVarification='Y'
									else:
										itrVarification=itrVarification+'<br>Y'
								else:
									if itrVarification=='':
										itrVarification='N'
									else:
										itrVarification=itrVarification+'<br>N'
							else:
								if itrVarification=='':
									itrVarification='N'
								else:
									itrVarification=itrVarification+'<br>N'

							payStatus='Pending'
							temp_expert_assisted='N'
							if RFPaymentDetails.objects.filter(R_Id=retnobj.R_id).exists():
								RFPayment_Obj=RFPaymentDetails.objects.filter(R_Id=retnobj.R_id).latest('id')
								if Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exists():
									Module_Subscription_Obj=Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exclude(Q(invoice_no=None) | Q(invoice_no=''))
									if Module_Subscription_Obj.exists():
										InvoiceNo=Module_Subscription_Obj[0].invoice_no
										if Invoice_Details.objects.filter(invoice_no=InvoiceNo).exists():
											Invoice_Obj=Invoice_Details.objects.filter(invoice_no=InvoiceNo).latest('id')
											if Invoice_Obj.payment_status!=None and Invoice_Obj.payment_status=='success':
												payStatus=Invoice_Obj.mode_of_payment
												if RFPayment_Obj.expert_review_charge!=None and RFPayment_Obj.expert_review_charge!=0:
													temp_expert_assisted='Y'
							
							if paymentStatus=='':
								paymentStatus=payStatus
								expert_assisted=temp_expert_assisted
							else:
								paymentStatus=paymentStatus+'<br>'+payStatus
								expert_assisted=expert_assisted+'<br>'+temp_expert_assisted
								

				pdata['xml_generated']=xmlGenerated   
				pdata['return_filing']=returnFiling  
				pdata['itr_varification']=itrVarification
				pdata['payment_status']=paymentStatus 
				pdata['expert_assisted']=expert_assisted  

				pdata['client_confirmation']='-'

				panDict[i]=pdata
				i+=1

		##log.info(panDict)

		return panDict
	except Exception as ex:
		log.error("Error in get_dhashboard_data: %s" % traceback.format_exc())
		return panDict

def make_pagination_html(current_page, total_pages,keyword):

	pagination_string = ""
	# count_limit=0
	# log.info(current_page)
	# log.info(total_pages)

	current_page=int(current_page)


	if current_page > 1:
		log.info('ok')
		pagination_string += '<li><a ng-click="load_page(%s,\'%s\')">&laquo;</a></li>' % (current_page -1,keyword)

	pagination_string += "<li class='active'><a ng-click='load_page(%s,\"%s\")' >%s</a></li>"  % (current_page,keyword,current_page)

	# log.info(pagination_string)

	count_limit = 10

	value = current_page - 1

	while value > 0 and count_limit < 5:

		pagination_string = "<li><a ng-click='load_page(%s,\"%s\")'>%s</a></li>" % (value,keyword,value) + pagination_string

		value -= 1

		count_limit += 1

	value = current_page + 1
	# log.info(pagination_string)

	while value < total_pages  and count_limit < 10:

		pagination_string =  pagination_string +"<li><a ng-click='load_page(%s,\"%s\")'>%s</a></li>" % (value,keyword,value)

		value += 1

		count_limit +=1

	if current_page < total_pages:

		pagination_string += '<li><a ng-click="load_page(%s,\'%s\')">&raquo;</a></li>' % (current_page + 1,keyword)


	# log.info(pagination_string)

	return pagination_string

def get_partner_data(business_partner_arr,Assessment_Year):
	log.info('get_partner_data')
	result={}

	try:

		countDict={}

		Personal_Details_Inst=Personal_Details.objects.filter(business_partner_id__in=business_partner_arr)

		##No of PANs
		pan_count=Personal_Details_Inst.count()
		result['pan_count']=pan_count

		if Personal_Details_Inst.exists():

			Pan_list=Personal_Details.objects.filter(business_partner_id__in=business_partner_arr).values_list('pan')

			##No of PANs Registered 
			registered_pan_count=salat_registration.objects.filter(pan__in=Pan_list,is_salat_client='Salat Client').values('pan','is_salat_client').distinct().count()

			result['registered_pan_count']=registered_pan_count

			##No of XMLs generated
			generated_xml_count=Return_Details.objects.filter(business_partner_id__in=business_partner_arr,AY=Assessment_Year).exclude(Q(xml_generated=None)).count()

			result['generated_xml_count']=generated_xml_count

			#No of returns filed successfully
			returns_filed_successfully=Return_Details.objects.filter(business_partner_id__in=business_partner_arr,AY=Assessment_Year).exclude(Q(xml_uploaded=None)).count()

			result['returns_filed_successfully']=returns_filed_successfully

			#No of returns filed successfully
			returns_filing_unsuccessful=Return_Details.objects.filter(business_partner_id__in=business_partner_arr,xml_uploaded=None,AY=Assessment_Year).count()

			result['returns_filing_unsuccessful']=returns_filing_unsuccessful


		log.info(result)

		return result
	except Exception as ex:
		result['status']="Error"
		log.error('Error in Get Partner Data : '+traceback.format_exc())
		return result

def update_ay():
	log.info('update_ay')
	try:

		Return_Details.objects.filter(FY='2016-2017').update(AY='2017-2018')
		Return_Details.objects.filter(FY='2017-2018').update(AY='2018-2019')
		Return_Details.objects.filter(FY='2018-2019').update(AY='2019-2020')

	except Exception as ex:
		log.error('Error in update_ay: '+request.get_host()+' : %s' % traceback.format_exc())

###################### Earnings ###########################################

@csrf_exempt
def earning_pagination(request):
	log.info('earning_pagination')
	result={}
	try:
		bp_id=request.POST.get('b_id')
		current_page=request.POST.get('current_page')
		search_input=request.POST.get('input')
		keyword=request.POST.get('keyword')
		Assessment_Year=request.POST.get('AssessmentYear')

		log.info(Assessment_Year)
		# log.info(current_page)
		# log.info(search_input)
		# log.info(keyword)

		bp_inst=Business_Partner.objects.filter(id=bp_id).latest("id")

		bp_type=bp_inst.user_id.role.rolename
		main_business_id=bp_inst.main_business.id

		if(bp_inst.partner_id=='0'):
			##If Business_Partner is Admin
			businessPartnerObjArr=Business_Partner.objects.filter(main_business=main_business_id)
			business_partner_arr=Business_Partner.objects.filter(main_business=main_business_id).values_list('id')
		else:
			##If Business_Partner is only partner
			businessPartnerObjArr=Business_Partner.objects.filter(id=bp_id)
			business_partner_arr=Business_Partner.objects.filter(id=bp_id).values_list('id')

		result['earning_summary']=get_earning_summary(business_partner_arr,Assessment_Year)
		
		if keyword=='onload':
			result['status']=getting_earning_data(request,business_partner_arr,current_page,Assessment_Year,bp_type)
		# elif keyword=='search':
		# 	result['status']=search_earning_data(request,bp_id,search_input,current_page,Assessment_Year)


		##log.info(result)

		return HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		log.info('Error in fetch earning_pagination data'+request.get_host()+' : %s' % traceback.format_exc())

@csrf_exempt
def search_client_on_earnings(request):
	log.info('search_client_on_earnings')
	result={}
	try:
		b_id=request.POST.get('b_id')
		search_input=request.POST.get('input')
		Assessment_Year=request.POST.get('AssessmentYear')

		result['status']=search_earnings_data(request,b_id,search_input,1,Assessment_Year)
		
		return HttpResponse(json.dumps(result),content_type='application/json')
		
	except Exception as ex:
		log.error('Error in search_client_on_earnings: '+request.get_host()+' : %s' % traceback.format_exc())

def search_earnings_data(request,bp_id,search_input,current_page,Assessment_Year):
	result={}
	try:

		bp_inst=Business_Partner.objects.filter(id=bp_id).latest("id")

		bp_type=bp_inst.user_id.role.rolename
		main_business_id=bp_inst.main_business.id

		if(bp_inst.partner_id=='0'):
			##If Business_Partner is Admin
			businessPartnerObjArr=Business_Partner.objects.filter(main_business=main_business_id)
			business_partner_arr=Business_Partner.objects.filter(main_business=main_business_id).values_list('id')
		else:
			##If Business_Partner is only partner
			businessPartnerObjArr=Business_Partner.objects.filter(id=bp_id)
			business_partner_arr=Business_Partner.objects.filter(id=bp_id).values_list('id')

		space_count=0
		search_input = search_input.replace('  ',' ')
		search_input = search_input.replace(' ','|')

		per_page_count=10
		limit = int(per_page_count) * int(current_page)
		offset = int(limit) - int(per_page_count)

		log.info(offset)
		log.info(limit)

		pan_data=Personal_Details.objects.filter(Q(business_partner_id__in=business_partner_arr) & (Q(name__contains=search_input) | Q(name=search_input) | Q(pan__contains=search_input) | Q(pan=search_input)))[offset:limit]

		##No of PANs
		pan_count=pan_data.count()

		log.info(pan_count)

		total_pages = pan_count / per_page_count
		if pan_count % per_page_count != 0:
			total_pages += 1 # adding one more page if the last page will contains less contacts 

		pagination = earning_pagination_html(current_page, total_pages,'search')
		result['listing']=pagination
		result['pan_count']=pan_count

		i=0
		panDict={}
		panDict=get_earning_dash_data(pan_data,Assessment_Year,current_page,bp_type)

		result['earning_data']=panDict
		#log.info(result)

		return result

	except Exception as ex:
		log.error("Error in search_earnings_data: %s" % traceback.format_exc())

def getting_earning_data(request,business_partner_arr,current_page,Assessment_Year,bp_type):
	result={}
	try:
		per_page_count=10

		limit = int(per_page_count) * int(current_page)
		offset = int(limit) - int(per_page_count)

		log.info(offset)
		log.info(limit)

		pan_data=Personal_Details.objects.filter(business_partner_id__in=business_partner_arr).order_by('-updated_time')[offset:limit]

		##No of PANs
		pan_count=Personal_Details.objects.filter(business_partner_id__in=business_partner_arr).count()

		total_pages = pan_count / per_page_count
		if pan_count % per_page_count != 0:
			total_pages += 1 # adding one more page if the last page will contains less contacts 
			
		pagination = earning_pagination_html(current_page, total_pages,'onload')

		result['listing']=pagination
		result['pan_count']=pan_count

		i=0
		panDict={}
		panDict=get_earning_dash_data(pan_data,Assessment_Year,current_page,bp_type)

		result['earning_data']=panDict

		##log.info(result)

		return result
	except Exception as ex:
		log.error("Error in getting_earning_data : %s" % traceback.format_exc())
		return result

def get_earning_dash_data(pan_data,Assessment_Year,current_page,bp_type):
	log.info('get_earning_dash_data')
	panDict={}
	try:

		cp=str(current_page)

		Module_obj=Modules.objects.get(module_name='Return Filing')
		ModuleId=Module_obj.module_Id

		i=0
		if pan_data.exists():
			# log.info('exists')
			for pobj in pan_data:
				pdata={} 
				pdata['pan']=pobj.pan
				business_partner_inst=pobj.business_partner_id

				name=pobj.name
				if name!=None:
					name=name.replace('|',' ')
				else:
					name=''

				client_url=salat_url+'/personal_information/'+str(pobj.P_Id)
				client_name='<a href="'+client_url+'" target="_blank"><strong><u>'+name+'</u></strong></a>'    
				pdata['client_name']=client_name

				partner_url=salat_url+'/partner_dashboard/'+str(pobj.business_partner_id.id)
				partnername=pobj.business_partner_id.partner_business_name
				partner_name='<a href="'+partner_url+'" target="_blank"><strong><u>'+partnername+'</u></strong></a>'    
				pdata['partner_name']=partner_name

				returnFiling='-'
				return_filing_fee='-'
				partner_earnig='-'

				if Return_Details.objects.filter(P_id=pobj.id,AY=Assessment_Year).exists():
					paymentStatus='Pending'
					return_obj=Return_Details.objects.filter(P_id=pobj.id,AY=Assessment_Year)
					R_id_list=Return_Details.objects.filter(P_id=pobj.id,AY=Assessment_Year).values_list('R_id')
					#log.info(return_obj.count())

					if return_obj.count()==1:
						rid=return_obj[0].R_id
						if return_obj[0].xml_generated!=None:
							if return_obj[0].xml_uploaded!=None:
								returnFiling=str(return_obj[0].xml_uploaded)[0:16]
							else:
								returnFiling='-'

						paymentSF=''
						if RFPaymentDetails.objects.filter(R_Id=return_obj[0].R_id).exists():
							RFPayment_Obj=RFPaymentDetails.objects.filter(R_Id=return_obj[0].R_id).latest('id')
							if Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exists():
								Module_Subscription_Obj=Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exclude(Q(invoice_no=None) | Q(invoice_no=''))
								if Module_Subscription_Obj.exists():
									return_filing_fee=Module_Subscription_Obj.module_cost
									InvoiceNo=Module_Subscription_Obj[0].invoice_no
									if Invoice_Details.objects.filter(invoice_no=InvoiceNo).exists():
										Invoice_Obj=Invoice_Details.objects.filter(invoice_no=InvoiceNo).latest('id')
										if Invoice_Obj.payment_status!=None and Invoice_Obj.payment_status=='success':
											paymentStatus=Invoice_Obj.mode_of_payment
											paymentSF=Invoice_Obj.payment_status
					
						if paymentSF!=None and paymentSF=='success':
						 	revenue_sharing_condition={ 'business_partner_id':business_partner_inst,
														 'module_id':ModuleId}
							if Revenue_Sharing.objects.filter(**revenue_sharing_condition).exists():
								partner_share_per=Revenue_Sharing.objects.get(**revenue_sharing_condition).partner_share
							else:
								partner_share_per=0

							if return_filing_fee.isdigit():
								partner_earnig=(int(return_filing_fee)*int(partner_share_per))/100

					if return_obj.count()>1:
						returnFiling=''
						return_filing_fee=''
						partner_earnig=''
						for retnobj in return_obj:
							#xmlGenerated
							rid=retnobj.R_id
							if retnobj.xml_generated!=None:
								#returnFiling
								if retnobj.xml_uploaded!=None:
									if returnFiling=='':
										returnFiling=str(retnobj.xml_uploaded)[0:16]
									else:
										returnFiling=returnFiling+'<br>'+str(retnobj.xml_uploaded)[0:16]
								else:
									if returnFiling=='':
										returnFiling='-'
									else:
										returnFiling=returnFiling+'<br>-'

							else:		
								#returnFiling
								if returnFiling=='':
									returnFiling='-'
								else:
									returnFiling=returnFiling+'<br>-'

							payStatus='Pending'
							temp_return_filing_fee='-'
							temp_partner_earnig='-'
							paymentSF=''
							if RFPaymentDetails.objects.filter(R_Id=retnobj.R_id).exists():
								RFPayment_Obj=RFPaymentDetails.objects.filter(R_Id=retnobj.R_id).latest('id')
								if Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exists():
									Module_Subscription_Obj=Module_Subscription.objects.filter(client_id=pobj.P_Id,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exclude(Q(invoice_no=None) | Q(invoice_no=''))
									if Module_Subscription_Obj.exists():
										temp_return_filing_fee=Module_Subscription_Obj[0].module_cost
										InvoiceNo=Module_Subscription_Obj[0].invoice_no
										if Invoice_Details.objects.filter(invoice_no=InvoiceNo).exists():
											Invoice_Obj=Invoice_Details.objects.filter(invoice_no=InvoiceNo).latest('id')
											if Invoice_Obj.payment_status!=None and Invoice_Obj.payment_status=='success':
												payStatus=Invoice_Obj.mode_of_payment
												paymentSF=Invoice_Obj.payment_status

							if paymentSF!=None and paymentSF=='success':
								revenue_sharing_condition={ 'business_partner_id':business_partner_inst,
														 'module_id':ModuleId}
								if Revenue_Sharing.objects.filter(**revenue_sharing_condition).exists():
									partner_share_per=Revenue_Sharing.objects.get(**revenue_sharing_condition).partner_share
								else:
									partner_share_per=0

								if temp_return_filing_fee.isdigit():
									temp_partner_earnig=(int(temp_return_filing_fee)*int(partner_share_per))/100

							if return_filing_fee=='':
								return_filing_fee=temp_return_filing_fee
							else:
								return_filing_fee=return_filing_fee+'<br>'+temp_return_filing_fee

							if partner_earnig=='':
								partner_earnig=str(temp_partner_earnig)
							else:
								partner_earnig=partner_earnig+'<br>'+str(temp_partner_earnig)

							if paymentStatus=='':
								paymentStatus=payStatus
							else:
								paymentStatus=paymentStatus+'<br>'+payStatus
								
				pdata['engagement_type']='-'
				pdata['return_filing']=returnFiling  
				pdata['return_filing_fee']=return_filing_fee
				pdata['partner_earnig']=partner_earnig
				pdata['settlement_status']='-'  

				panDict[i]=pdata
				i+=1

		##log.info(panDict)

		return panDict
	except Exception as ex:
		log.error("Error in get_earning_dash_data : %s" % traceback.format_exc())
		return panDict

def earning_pagination_html(current_page, total_pages,keyword):

	pagination_string = ""
	# count_limit=0
	# log.info(current_page)
	# log.info(total_pages)

	current_page=int(current_page)


	if current_page > 1:
		log.info('ok')
		pagination_string += '<li><a ng-click="load_earning_page(%s,\'%s\')">&laquo;</a></li>' % (current_page -1,keyword)

	pagination_string += "<li class='active'><a ng-click='load_earning_page(%s,\"%s\")' >%s</a></li>"  % (current_page,keyword,current_page)

	# log.info(pagination_string)

	count_limit = 10

	value = current_page - 1

	while value > 0 and count_limit < 5:

		pagination_string = "<li><a ng-click='load_earning_page(%s,\"%s\")'>%s</a></li>" % (value,keyword,value) + pagination_string

		value -= 1

		count_limit += 1

	value = current_page + 1
	# log.info(pagination_string)

	while value < total_pages  and count_limit < 10:

		pagination_string =  pagination_string +"<li><a ng-click='load_earning_page(%s,\"%s\")'>%s</a></li>" % (value,keyword,value)

		value += 1

		count_limit +=1

	if current_page < total_pages:

		pagination_string += '<li><a ng-click="load_earning_page(%s,\'%s\')">&raquo;</a></li>' % (current_page + 1,keyword)


	# log.info(pagination_string)

	return pagination_string

def get_earning_summary(business_partner_arr,Assessment_Year):
	log.info('get_earning_summary')
	result={}
	try:
		countDict={}

		Module_obj=Modules.objects.get(module_name='Return Filing')
		ModuleId=Module_obj.module_Id

		##No of Returns Filed
		return_filed_count=Return_Details.objects.filter(business_partner_id__in=business_partner_arr,AY=Assessment_Year).exclude(Q(xml_uploaded=None)).count()
		result['return_filed_count']=return_filed_count

		Return_Details_inst=Return_Details.objects.filter(business_partner_id__in=business_partner_arr,AY=Assessment_Year).exclude(Q(xml_uploaded=None))

		filing_earnings=0
		referral_earnings=0

		for retnobj in Return_Details_inst:

			business_partner_inst=retnobj.business_partner_id
			clientId=retnobj.P_id.P_Id

			temp_expert_assisted='N'
			temp_return_filing_fee='-'
			temp_partner_earnig=''
			paymentStatus=''
			if RFPaymentDetails.objects.filter(R_Id=retnobj.R_id).exists():
				RFPayment_Obj=RFPaymentDetails.objects.filter(R_Id=retnobj.R_id).latest('id')
				if Module_Subscription.objects.filter(client_id=clientId,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exists():
					Module_Subscription_Obj=Module_Subscription.objects.filter(client_id=clientId,module_Id=ModuleId,module_detail1=RFPayment_Obj.id).exclude(Q(invoice_no=None) | Q(invoice_no=''))
					if Module_Subscription_Obj.exists():
						temp_return_filing_fee=Module_Subscription_Obj[0].module_cost
						InvoiceNo=Module_Subscription_Obj[0].invoice_no
						if Invoice_Details.objects.filter(invoice_no=InvoiceNo).exists():
							Invoice_Obj=Invoice_Details.objects.filter(invoice_no=InvoiceNo).latest('id')
							if Invoice_Obj.payment_status!=None and Invoice_Obj.payment_status=='success':
								payStatus=Invoice_Obj.mode_of_payment
								paymentStatus=Invoice_Obj.payment_status
								

			if paymentStatus!=None and paymentStatus=='success':
				revenue_sharing_condition={ 'business_partner_id':business_partner_inst,
										 'module_id':ModuleId}
				if Revenue_Sharing.objects.filter(**revenue_sharing_condition).exists():
					partner_share_per=Revenue_Sharing.objects.get(**revenue_sharing_condition).partner_share
				else:
					partner_share_per=0

				if temp_return_filing_fee.isdigit():
					temp_partner_earnig=(int(temp_return_filing_fee)*int(partner_share_per))/100
					filing_earnings+=temp_partner_earnig


		total_earnings=filing_earnings+referral_earnings

		result['filing_earnings']=filing_earnings
		result['no_of_referrals']=0
		result['referral_earnings']=referral_earnings
		result['total_earnings']=total_earnings

		log.info(result)

		return result
	except Exception as ex:
		result['status']="Error"
		log.error('Error in get_earning_summary : '+traceback.format_exc())
		return result

############################################################################

@csrf_exempt
def partner_deposit(request):
	log.info('partner_deposit')

	result={}
	try:

		partner_id=0
		user_id=0
		username = request.session['login']
		user_type=''
		partner_type=''
		partner_name=''

		log.info(username)

		if User.objects.filter(username=username).exists():
			atuser = User.objects.get(username=username)
			if SalatTaxUser.objects.filter(user=atuser).exists():
				SalatTaxUser_Obj=SalatTaxUser.objects.get(user=atuser)
				user_id=SalatTaxUser_Obj.id
				user_role=SalatTaxUser_Obj.role.rolename
				

				if user_role!=None and user_role=='Client':
					user_type='client'
				elif user_role!=None and (user_role=='Admin' or user_role=='Partner'):
					user_type='business'
					partner_type=user_role

					if Business_Partner.objects.filter(user_id=SalatTaxUser_Obj).exists():
						bp_inst=Business_Partner.objects.filter(user_id=SalatTaxUser_Obj.id).latest("id")
						partner_id=bp_inst.id
						partner_name=bp_inst.name
						log.info(partner_id)

		log.info(user_id)
		log.info(partner_id)
		log.info(user_type)

		if(user_id!=0 and partner_id!=0):

			return render(request,'partner_deposit.html',{
				'b_id':partner_id,
				'user_id':user_id,
				'username':username,
				'user_type':user_type,
				'business_type':partner_type,
				'partner_name':partner_name

							
			})

		else:
			request.session['login']=''
			return  redirect('/')


	except Exception as ex:
		log.info('Error in partner_deposit '+request.get_host()+' : %s' % traceback.format_exc())
		request.session['login']=''
		return  redirect('/')

@csrf_exempt
def submit_deposite(request):
	log.info('submit_deposite')

	result={}
	try:

		req_data = request.POST.get('data')
		req_data = json.loads(req_data)

		deposit_amount_plan=req_data['deposit_amount']
		payment_mode=req_data['payment_mode']
		b_id=req_data['b_id']
		username=req_data['username']
		user_id=req_data['user_id']
		user_type=req_data['user_type']
		business_type=req_data['business_type']

		if deposit_amount_plan=='plan1':
			deposit_amount='2500'
		elif deposit_amount_plan=='plan2':
			deposit_amount='5000'
		elif deposit_amount_plan=='plan3':
			deposit_amount='10000'

		if Business_Partner.objects.filter(id=b_id).exists():
			bp_inst=Business_Partner.objects.filter(id=b_id).latest("id")
			partner_id=bp_inst.id
			email=bp_inst.email
			mobile=bp_inst.mobile
			log.info(partner_id)

			result['b_id']=partner_id
			result['email']=email
			result['mobile']=mobile

			Deposit_create = Partner_Deposit_Details(business_partner_id=bp_inst,
				deposite_amount=deposit_amount,
				mode_of_payment=payment_mode,
				payment_status='Pending')

			Deposit_create.save()

			Partner_Deposit_inst=Partner_Deposit_Details.objects.filter(id=Deposit_create.id)

			deposite_id = Partner_Deposit_inst[0].id

			result['deposite_id']=deposite_id
			result['amount']=deposit_amount

			result['status']='Success'

		else:
			result['status']='Error'
		
		return  HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		log.info('Error in submit deposite data '+request.get_host()+' : %s' % traceback.format_exc())
		result['status']='Error'
		return  HttpResponse(json.dumps(result), content_type='application/json')


# card no - 4012001037141112
# Expire - 05/20
# CVV - 123
# Name = Test
# Password = Salattax@123


@csrf_exempt
def deposit_payment(request):
	log.info('deposit_payment')
	result={}
	try:

		username = request.session['login']
		bp_exist=0
		if User.objects.filter(username=username).exists():
			atuser = User.objects.get(username=username)
			if SalatTaxUser.objects.filter(user=atuser).exists():
				SalatTaxUser_Obj=SalatTaxUser.objects.get(user=atuser)
				user_id=SalatTaxUser_Obj.id
				user_role=SalatTaxUser_Obj.role.rolename
				
				if Business_Partner.objects.filter(user_id=SalatTaxUser_Obj).exists():
					bp_inst=Business_Partner.objects.filter(user_id=SalatTaxUser_Obj.id).latest("id")
					partner_id=bp_inst.id

					Partner_Deposit_obj = Partner_Deposit_Details.objects.filter(business_partner_id=bp_inst,
						payment_status='Pending',
						mode_of_payment='Online')

					if Partner_Deposit_obj.exists():
						Partner_Deposit_obj = Partner_Deposit_Details.objects.filter(business_partner_id=bp_inst,
							payment_status='Pending',
							mode_of_payment='Online').latest("id")
						
						bp_exist=1

		if bp_exist==1:
			log.info('Partner is Exist')

			amount=Partner_Deposit_obj.deposite_amount
			deposite_id=Partner_Deposit_obj.id

			b_id=bp_inst.id
			mobile=bp_inst.mobile
			email=bp_inst.email

			client_id=bp_inst.user_id.id
			Fullname=bp_inst.name

			Fullname = Fullname.replace('  ',' ')
			Fullname = Fullname.split(' ')

			fname=Fullname[0]

			lname=''
			if len(Fullname)>=3:
				lname=Fullname[2]
			elif len(Fullname)==2:
				lname=Fullname[1]

			log.info(b_id)

			module_detail='Partner_Deposit'

			#################################

			PAYU_BASE_URL = PAYUMONEY_BASE_URL
			MERCHANT_KEY = "iv05Eqin"
			key=""
			SALT = "X31IOZyzi6"
			action = ''
			posted={}
			hash_object = hashlib.sha256(b'randint(0,20)')
			txnid=hash_object.hexdigest()[0:20]
			hashh = ''
			posted['txnid']=txnid
			hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|||||"
			posted['key']=MERCHANT_KEY
			posted['amount']=amount
			posted['productinfo']=module_detail
			posted['firstname']=fname
			posted['phone']=mobile
			posted['email']=email
			posted['lastname']=lname
			posted['address1']='address1'
			posted['address2']='address2'
			posted['city']='city'
			posted['state']='state'
			posted['country']='country'
			posted['zipcode']=''
			posted['udf1']=client_id
			posted['udf2']=''
			posted['udf3']=''
			posted['udf4']=''
			posted['udf5']=''
			posted['PG']='PG'
			posted['salt']=SALT
			posted['surl']= payum_redirection+'/deposit_success/'
			posted['furl']= payum_redirection+'/Failure/'
			posted['curl']= payum_redirection+'/curl/'

			log.info(salat_url+'/Failure/')
			hash_string=''
			hashVarsSeq=hashSequence.split('|')
			for i in hashVarsSeq:
				try:
					hash_string+=str(posted[i])
				except Exception:
					hash_string+=''
				hash_string+='|'
			hash_string+=SALT
			log.info(hash_string)
			hashh=hashlib.sha512(hash_string).hexdigest().lower()
			action =PAYU_BASE_URL
			if(posted.get("key")!=None and posted.get("txnid")!=None and posted.get("productinfo")!=None and posted.get("firstname")!=None and posted.get("email")!=None):
				log.info('if')
				return render(request,'subscribe_payumoney.html',
				{
				'posted':posted,
				'amount':amount,
				'hashh':hashh,
				'txnid':txnid,
				'MERCHANT_KEY':MERCHANT_KEY,
				'hash_string':hash_string,
				'action':PAYUMONEY_BASE_URL
				})
			else:
				# return render_to_response('subscribe_payumoney.html',RequestContext(request,{"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"." }))
				log.info('else')
				return render(request,'subscribe_payumoney.html',{
				'posted':posted,
				'amount':amount,
				'hashh':hashh,
				'txnid':txnid,
				'MERCHANT_KEY':MERCHANT_KEY,
				'hash_string':hash_string,
				'action':".",
				})

				result['status']='Success'

		else:
			log.info('Partner not exist')
			result['status']='Error'

		
	except Exception as ex:
		log.info('Error in deposite payment '+request.get_host()+' : %s' % traceback.format_exc())
		result['status']='Error'

@csrf_protect
@csrf_exempt
def deposit_success(request):
	log.info('deposit_success')
	try:
		username = request.session.get('login')
		client_id = 0
		if username is None:
			pass
		else:
			if username!='':
				atuser = User.objects.get(username=username)
				for c in SalatTaxUser.objects.filter(user=atuser):
					client_id = c.id
		# log.info(client_id)
		# c = {}
		# c.update(csrf(request))
		log.info(request)
		status=''
		firstname=''
		amount=''
		txnid=''
		posted_hash=''
		key=''
		productinfo=''
		email=''
		salt=''
		status=request.POST["status"]
		firstname=request.POST["firstname"]
		amount=request.POST["amount"]
		txnid=request.POST["txnid"]
		posted_hash=request.POST["hash"]
		key=request.POST["key"]
		productinfo=request.POST["productinfo"]
		email=request.POST["email"]
		salt="X31IOZyzi6"
		amount_compare = amount.encode('utf-8')
		amount_compare = int(float(amount_compare) )
		log.info( status )
		try:
			additionalCharges=request.POST["additionalCharges"]
			retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
		except Exception:
			retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
		hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
		if(hashh !=posted_hash):
			log.info( "Invalid Transaction. Please try again")
			log.info( "Your Transaction ID for this transaction is ",txnid)
			# log.info( type(amount_compare))
		else:
			log.info( "Thank You. Your order status is ", status)
			log.info( "Your Transaction ID for this transaction is ",txnid)
			log.info( "We have received a payment of Rs. ", amount ,". Your order will soon be shipped.")
			# log.info( type(amount_compare))
		# return render_to_response('success.html',RequestContext(request,{"txnid":txnid,"status":status,"amount":amount}))
		log.info( status )
		log.info(productinfo)

		if status == 'success':

			if User.objects.filter(username=username).exists():
				atuser = User.objects.get(username=username)
				if SalatTaxUser.objects.filter(user=atuser).exists():
					SalatTaxUser_Obj=SalatTaxUser.objects.get(user=atuser)
					user_id=SalatTaxUser_Obj.id
					user_role=SalatTaxUser_Obj.role.rolename

					if Business_Partner.objects.filter(user_id=SalatTaxUser_Obj).exists():
						bp_inst=Business_Partner.objects.filter(user_id=SalatTaxUser_Obj.id).latest("id")
						partner_id=bp_inst.id

						Partner_Deposit_obj = Partner_Deposit_Details.objects.filter(business_partner_id=bp_inst,
							payment_status='Pending',
							mode_of_payment='Online')

						if Partner_Deposit_obj.exists():
							Partner_Deposit_obj = Partner_Deposit_Details.objects.filter(business_partner_id=bp_inst,
							payment_status='Pending',
							mode_of_payment='Online').latest('id')

							Partner_Deposit_obj.transaction_id=txnid
							Partner_Deposit_obj.payment_status=status
							Partner_Deposit_obj.save()

							deposite_amount=Partner_Deposit_obj.deposite_amount

							##Mail
							partner_deposit_mail_to_partner(bp_inst.id,deposite_amount)
							partner_deposit_mail_to_salat(bp_inst.id,deposite_amount)
							
							Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst)

							if Deposit_Balance_obj.exists():
								Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst).latest('id')

								balance=Deposit_Balance_obj.deposit_balance
								balance=int(balance)
								deposite_amount=int(deposite_amount)

								new_balance=balance+deposite_amount
								Deposit_Balance_obj.deposit_balance=new_balance
								Deposit_Balance_obj.save()

								Partner_Deposit_obj.show_in_balance='Y'
								Partner_Deposit_obj.save()

							else:
								Balance_create = Partner_Deposit_Balance(business_partner_id=bp_inst,
									deposit_balance=deposite_amount)
								Balance_create.save()

								Partner_Deposit_obj.show_in_balance='Y'
								Partner_Deposit_obj.save()


							bp_exist=1

			return render(request,'success_page.html',
				{
				'status':status,
				'txnid':txnid,
				'amount':amount,
				'productinfo':productinfo,
				})
		else:
			log.info('transaction failure')


	except Exception as ex:
		log.info('Error when redirection to deposite success payment page '+traceback.format_exc())

@csrf_exempt
def fetch_partner_data(request):
	log.info('fetch_partner_data')

	result={}
	try:

		result['PartnerList']=''

		##Get Main Business
		main_business_instance = Business.objects.get(main_business_name='salattax')
		Business_Partner_list=Business_Partner.objects.filter(main_business=main_business_instance).values('id','name','pan')

		result['PartnerList']=list(Business_Partner_list)
		result['status']='Success'
		return  HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		log.info('Error in fetch_partner_data '+request.get_host()+' : %s' % traceback.format_exc())
		result['status']='Error'
		return  HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def clear_deposit_cheque(request):
	log.info('clear_deposit_cheque')

	result={}
	try:

		req_data = request.POST.get('data')
		req_data = json.loads(req_data)

		deposit_amount_plan=req_data['deposit_amount']
		partner_id=req_data['PartnerId']

		if deposit_amount_plan=='plan1':
			deposit_amount='2500'
		elif deposit_amount_plan=='plan2':
			deposit_amount='5000'
		elif deposit_amount_plan=='plan3':
			deposit_amount='10000'

		if Business_Partner.objects.filter(id=partner_id).exists():
			bp_inst=Business_Partner.objects.filter(id=partner_id).latest("id")
			partner_id=bp_inst.id
			log.info(partner_id)


			Partner_Deposit_obj = Partner_Deposit_Details.objects.filter(business_partner_id=bp_inst,
				deposite_amount=deposit_amount,
				payment_status='Pending',
				mode_of_payment='Cash')

			if Partner_Deposit_obj.exists():
				Partner_Deposit_obj = Partner_Deposit_Details.objects.filter(business_partner_id=bp_inst,
					deposite_amount=deposit_amount,
					payment_status='Pending',
					mode_of_payment='Cash').latest('id')

				Partner_Deposit_obj.payment_status='success'
				Partner_Deposit_obj.save()

				deposite_amount=Partner_Deposit_obj.deposite_amount

				#Mail
				partner_deposit_mail_to_partner(bp_inst.id,deposite_amount)
				partner_deposit_mail_to_salat(bp_inst.id,deposite_amount)

				Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst)

				if Deposit_Balance_obj.exists():
					Deposit_Balance_obj = Partner_Deposit_Balance.objects.filter(business_partner_id=bp_inst).latest('id')

					balance=Deposit_Balance_obj.deposit_balance
					balance=int(balance)
					deposite_amount=int(deposite_amount)

					new_balance=balance+deposite_amount
					Deposit_Balance_obj.deposit_balance=new_balance
					Deposit_Balance_obj.save()

					Partner_Deposit_obj.show_in_balance='Y'
					Partner_Deposit_obj.save()

				else:
					Balance_create = Partner_Deposit_Balance(business_partner_id=bp_inst,
						deposit_balance=deposite_amount)
					Balance_create.save()

					Partner_Deposit_obj.show_in_balance='Y'
					Partner_Deposit_obj.save()

				result['status']='Success'
			else:
				result['status']='Not_exist'

		else:
			result['status']='Error'
		
		return  HttpResponse(json.dumps(result), content_type='application/json')

	except Exception as ex:
		log.info('Error in clear_deposit_cheque '+request.get_host()+' : %s' % traceback.format_exc())
		result['status']='Error'
		return  HttpResponse(json.dumps(result), content_type='application/json')