
import xlwt
import xlrd
import datetime
# sudo pip install xlrd


def unique(list1):
	# intilize a null list
	unique_list = []
	# traverse for all elements
	for x in list1:
		# check if exists in unique_list or not
		if x not in unique_list:
			unique_list.append(x)
	
	return unique_list

filename = "/home/salattax/SalatTax/salattax_app/static/file_26AS/ANRPB0497J.xls"
workbook = xlrd.open_workbook(filename)
worksheet = workbook.sheet_by_name('prerana16')

print 'Sheet :'+' prerana16'
num_rows = worksheet.nrows - 1
curr_row = -1

# print worksheet.cell(3,2) # row,column
partA_status = 0
partA1_status = 0
partA2_status = 0
partB_status = 0
partC_status = 0
partD_status = 0
partE_status = 0
partF_status = 0
partG_status = 0
part_A_count = 0
part_D_count = 0

no_of_TAN = 0
no_of_TAN_partA1 = 0
no_of_TAN_partA2 = 0
no_of_TAN_partB = 0
# tan1_start = 0
# tan1_end = 0
# tan2_start = 0
# tan2_end = 0
# tan3_start = 0
# tan3_end = 0
# tan4_start = 0
# tan4_end = 0
# tan5_start = 0
# tan5_end = 0
# tan6_start = 0
# tan6_end = 0
# tan7_start = 0
# tan7_end = 0
# tan8_start = 0
# tan8_end = 0
# tan9_start = 0
# tan9_end = 0
# tan10_start = 0
# tan10_end = 0
# tan11_start = 0
# tan11_end = 0
# tan12_start = 0
partA_start = 0
partA_end = 0

partA1_start = 0
partA1_end = 0
partA2_start = 0
partA2_end = 0
partB_start = 0
partB_end = 0
partC_start = 0
partC_end = 0
partD_start = 0
partD_end = 0
isunicode = 0

while curr_row < num_rows:
	curr_row += 1
	row = worksheet.row(curr_row) # get each row, list type
	# print row[0]
	# PART A count
	if( isinstance(row[1].value, unicode) ):
		isunicode = 1
		row_value = row[1].value.encode('utf-8')
	else:
		isunicode = 0
		row_value = row[1].value

	if ( row[1].value == 'PART A - Details of Tax Deducted at Source'):
		partA_status = 1
		partA_start = curr_row
	if row[1].value == 'PART A1 - Details of Tax Deducted at Source for 15G / 15H':
		partA_status = 0
		partA1_status = 1
		partA1_start = curr_row
		partA_end = curr_row
	if row[1].value == 'PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)' :
		partA1_status = 0
		partA2_status = 1
		partA2_start = curr_row
		partA1_end = curr_row
	if row[1].value == 'PART B - Details of Tax Collected at Source' :
		partA2_status = 0
		partB_status = 1
		partB_start = curr_row
		partA2_end = curr_row
	if row[1].value == 'PART C - Details of Tax Paid (other than TDS or TCS)' :
		partB_status = 0
		partC_status = 1
		partB_end = curr_row
	if row[1].value == 'PART D - Details of Paid Refund':
		partC_status = 0
		partD_status = 1
		partC_end = curr_row
	if row[1].value == 'PART E - Details of AIR Transaction':
		partD_status = 0
		partD_end = curr_row

	if partA_status == 1:
		part_A_count += 1
		if (row[0].value!=''):
			no_of_TAN += 1

	if partA1_status == 1:
		if (row[0].value!=''):
			no_of_TAN_partA1 += 1

	if partA2_status == 1:
		if (row[0].value!=''):
			no_of_TAN_partA2 += 1
	
	if partB_status == 1:
		if (row[0].value!=''):
			no_of_TAN_partB += 1

	if partC_status == 1:
		if (row[0].value==1):
			partC_start = curr_row

	if partD_status == 1:
		part_D_count += 1
		if (row[0].value==1):
			partD_start = curr_row

# #### PARTR A ######
def get_partA(start,end):
	# print start
	# print end
	section_list = []
	for curr_row_tan1 in xrange(start+2,end):
		if worksheet.cell(curr_row_tan1,2).value != '':
			section_list.append(worksheet.cell(curr_row_tan1,2).value)
	section_list = unique(section_list)

	for section in section_list:
		tan1_list = []
		tan1_list.append(worksheet.cell(start,1).value)
		tan1_list.append(worksheet.cell(start,2).value)
		tan1_list.append(section)
		tan1_amount = 0
		tan1_tax_deducted = 0
		tan1_TDS = 0
		for curr_row_tan1 in xrange(start+2,end):
			if worksheet.cell(curr_row_tan1,2).value == section:
				tan1_amount += worksheet.cell(curr_row_tan1,7).value
				tan1_tax_deducted += worksheet.cell(curr_row_tan1,8).value
				tan1_TDS += worksheet.cell(curr_row_tan1,9).value
		tan1_list.append(tan1_amount)
		tan1_list.append(tan1_tax_deducted)
		tan1_list.append(tan1_TDS)
		print tan1_list

if no_of_TAN-1!= 0:
	print 'Part A :'
	# print partA_start
	# print partA_end
	for i in xrange(1,no_of_TAN):
		start = 0
		end =0
		for x in xrange(partA_start,partA_end):
			# print worksheet.cell(x,0)
			if (worksheet.cell(x,0).value == i):
				start = x
			if (worksheet.cell(x,0).value == i+1):
				end = x
			if (i == no_of_TAN-1):
				end = partA_end

		get_partA(start,end)
else:
	print 'NO PART A'

if no_of_TAN_partA1-1!= 0:
	print 'Part A1 :'
	for i in xrange(1,no_of_TAN_partA1):
		start = 0
		end =0
		for x in xrange(partA1_start,partA1_end):
			# print worksheet.cell(x,0)
			if (worksheet.cell(x,0).value == i):
				start = x
			if (worksheet.cell(x,0).value == i+1):
				end = x
			if (i == no_of_TAN_partA1-1):
				end = partA1_end

		get_partA(start,end)
else:
	print 'NO PART A1'

if no_of_TAN_partA2-1!= 0:
	print 'Part A2 :'
	for i in xrange(1,no_of_TAN_partA2):
		start = 0
		end =0
		for x in xrange(partA2_start,partA2_end):
			# print worksheet.cell(x,0)
			if (worksheet.cell(x,0).value == i):
				start = x
			if (worksheet.cell(x,0).value == i+1):
				end = x
			if (i == no_of_TAN_partA2-1):
				end = partA2_end

		get_partA(start,end)
else:
	print 'NO PART A2'

if no_of_TAN_partB-1!= 0:
	print 'Part B :'
	for i in xrange(1,no_of_TAN_partB):
		start = 0
		end =0
		for x in xrange(partB_start,partB_end):
			# print worksheet.cell(x,0)
			if (worksheet.cell(x,0).value == i):
				start = x
			if (worksheet.cell(x,0).value == i+1):
				end = x
			if (i == no_of_TAN_partB-1):
				end = partB_end

		get_partA(start,end)
else:
	print 'NO PART B'

if partC_start!= 0:
	print 'Part C : '
	for curr_row_partC in xrange(partC_start,partC_end-2):
		partC_list = []
		if worksheet.cell(curr_row_partC,1).value != '':
			pass
			partC_list.append(worksheet.cell_value(curr_row_partC,1))
			partC_list.append(worksheet.cell_value(curr_row_partC,2))
			partC_list.append(worksheet.cell_value(curr_row_partC,7))
			partC_list.append(worksheet.cell_value(curr_row_partC,8))

			cell = worksheet.cell(curr_row_partC, 9)
			if cell.ctype == xlrd.XL_CELL_DATE:
				date = datetime.datetime(1899, 12, 30)
				get_ = datetime.timedelta(int(cell.value))
				get_col2 = str(date + get_)[:10]
				d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
				get_col = d.strftime('%d-%m-%Y')
			else:
				get_col = unicode(int(cell.value))

			partC_list.append(get_col)
			partC_list.append(worksheet.cell_value(curr_row_partC,10))
		print partC_list
else :
	print 'NO PART C'

if partD_start!= 0:
	print 'Part D : '
	for curr_row_partD in xrange(partD_start,partD_end-2):
		# print worksheet.cell(curr_row_partD,1).value
		partD_list = []
		if worksheet.cell(curr_row_partD,1).value != '':
			pass
			partD_list.append(worksheet.cell(curr_row_partD,1).value)
			partD_list.append(worksheet.cell(curr_row_partD,2).value)
			partD_list.append(worksheet.cell(curr_row_partD,3).value)
			partD_list.append(worksheet.cell(curr_row_partD,4).value)
		
			cell = worksheet.cell(curr_row_partD, 5)
			if cell.ctype == xlrd.XL_CELL_DATE:
				date = datetime.datetime(1899, 12, 30)
				get_ = datetime.timedelta(int(cell.value))
				get_col2 = str(date + get_)[:10]
				d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
				get_col = d.strftime('%d-%m-%Y')
			else:
				get_col = unicode(int(cell.value))

			partD_list.append(get_col)
		print partD_list
else :
	print 'NO PART D'

# new credentials only for product
# ERIU111010
# Derivatives4$
