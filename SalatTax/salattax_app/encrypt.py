import random
from django.http import HttpResponse


chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + \
   'abcdefghijklmnopqrstuvwxyz' + \
   '0123456789'


def generate_key():
   """Generate an key for our cipher"""
   shuffled = sorted(chars, key=lambda k: random.random())
   return dict(zip(chars, shuffled))

def encrypt(key, plaintext):
   """Encrypt the string and return the ciphertext"""
   return ''.join(key[l] for l in plaintext)

def decrypt(key, ciphertext):
   """Decrypt the string and return the plaintext"""
   flipped = {v: k for k, v in key.items()}
   return ''.join(flipped[l] for l in ciphertext)

key = generate_key()

def encrypt_code(request,code):
   encrypted = encrypt(key, code)
   return HttpResponse('Encrypted code :' +encrypted)

def decrypt_code(request,code):
   decrypted = decrypt(key, code)
   return HttpResponse('decrypted code :' +decrypted)
  