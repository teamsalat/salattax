from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

import requests
from .models import SalatTaxUser
from .models import index, Personal_Details, Address_Details, Client_fin_info1,salatclient
from .models import Return_Details,Employer_Details,State_Code,Bank_List,VI_Deductions,Income,salat_registration
from .models import temp_ITR_value,Investment_Details,user_tracking, Variables_80d
import base64
import os, sys
from PIL import Image
# Python logging package
import logging
import json
from datetime import date, datetime
from django.db.models import Avg, Max, Min, Sum
import re
import traceback
import locale
from .path import *

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)


def json_serial(obj):
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))

@csrf_exempt
def fileupload(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            pan=request.POST.get('pan')
            document_type=request.POST.get('type')
            req_data = json.loads(data)
            filename=req_data['filename']
            filetype=req_data['filetype']
            a,ext=filetype.split("/")
            filetype=ext
            base64=req_data['base64']

        except Exception as ex:
            log.error('Error : %s' % ex)

        status=upload(pan,filetype,base64,document_type)
        result['status1']=status

        result['status']="success"
        return HttpResponse(json.dumps(result), content_type='application/json')

    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def upload(pan,filetype,file_base64,document_type):
    try:
        if pan!='':
            MYDIR = os.path.dirname(__file__)
            # createFolder(MYDIR+'/mail')
            # createFolder('/home/salattax/SalatTax/salattax_app/static/form16')
            # path = '/home/salatTax/SalatTax/static/form16'
            path = selenium_path+'/form16'
            if document_type=='PAN':
                path = selenium_path+'/pan'
            if document_type=='ADHAR':
                path = selenium_path+'/adhar'
            if document_type=='ConsoAccoStatement':
                path = selenium_path+'/ConsolidatedAccountStatement'    
            createFolder(path)

            # renaming File
            img_name=pan+'_'+document_type
            # img_name='P'+str(pan_id)+'_'+document_type
           
            infile = path+"/"+img_name+"."+filetype

            # upload base 64 file
            img_decoded = base64.b64decode(file_base64)

            with open(infile, "wb") as fh:
                fh.write(img_decoded)

            f, e = os.path.splitext(infile)
            outfile = f + ".png"
            log.info(outfile)
            if infile != outfile:
                try:
                    Image.open(infile).save(outfile, "png")
                    os.unlink(infile)
                    log.info("Converted")
                except IOError as ex:
                    log.info("cannot convert: %s" % ex )

            return img_name;
    except Exception as ex:
        log.error('Error: %s' % ex)
        return 'Error: %s' % ex;

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError as ex:
        log.info('Error: Creating directory : %s' % ex )

@csrf_exempt
def personal_details_DB(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            email=request.POST.get('email')
            client_id=request.POST.get('client_id')
            req_data = json.loads(data)
            pan1=req_data['pan1'];
            mobile=req_data['mobile'];
            # log.info(req_data)
            flatno = req_data['pan1_flatno'];
            premise = req_data['pan1_premise'];
            road = req_data['pan1_road'];
            area = req_data['pan1_area'];
            pin = req_data['pan1_pin'];
            area = req_data['pan1_area'];
            city = req_data['pan1_city'];
            state = req_data['pan1_state'];
            disability = req_data['disability'];

            b_id=req_data['business_partner_id'];
            name=''

            if req_data['pan1_middle_name'] and not req_data['pan1_last_name']:
                name=req_data['pan1_first_name']+'|'+req_data['pan1_middle_name']+'|'
            elif not req_data['pan1_middle_name'] and req_data['pan1_last_name']:
                name=req_data['pan1_first_name']+'|'+'|'+req_data['pan1_last_name']
            elif req_data['pan1_middle_name'] and req_data['pan1_last_name']:
                name=req_data['pan1_first_name']+'|'+req_data['pan1_middle_name']+'|'+req_data['pan1_last_name']
            elif not req_data['pan1_middle_name'] and not req_data['pan1_last_name']:
                name=req_data['pan1_first_name']+'|'+'|'
            # log.info(name)

            dob=req_data['pan1_dob']
            # dob='2014-12-12'
            d,m,y=dob.split("-")
            pan1_dob=y+'-'+m+'-'+d
            log.info(str(name)+ ' , DOB: ' +str(pan1_dob) )

            img_name=''
            # if req_data['pan1_file'] and Personal_Details.objects.filter(P_Id=client_id).exists():
            #     img_name=pan1+'_PAN.png'
            # elif req_data['pan1_file'] and not Personal_Details.objects.filter(P_Id=client_id).exists():
            #     img_name=pan1+'_PAN.png'
            # else:
            #     img_name=''
            ###INsert/Update
            # if not Personal_Details.objects.filter(pan=pan1,P_Id=client_id).exists():
           
            if not Personal_Details.objects.filter(P_Id=client_id).exists():
                Personal_Details.objects.create(
                    P_Id=client_id,
                    # Business_Id=1,
                    # Partner_Id=1,
                    pan=req_data['pan1'],
                    pan_type='PAN',
                    dob=pan1_dob,
                    pan_image=img_name,
                    aadhar_no=req_data['pan1_adhar'],
                    aadhar_image=img_name,
                    name=name,
                    mobile=mobile,
                    gender=req_data['pan1_gender'],
                    disability=disability,
                ).save()

                log.info("PAN1 Entry Created")
            else:
                # if req_data['pan1_file']:
                #     pid=Personal_Details.objects.get(P_Id=client_id)
                #     pan_id=pid.id
                #     img_name='P'+str(pan_id)+'_PAN.png'
                #     log.info(img_name)
                #     personal_details_instance=Personal_Details.objects.get(P_Id=client_id)
                #     personal_details_instance.pan_image=img_name
                #Update Personal details
                personal_details_instance=Personal_Details.objects.get(P_Id=client_id)
                personal_details_instance.pan=req_data['pan1']
                personal_details_instance.name=name
                personal_details_instance.dob=pan1_dob
                personal_details_instance.aadhar_no=req_data['pan1_adhar']
                personal_details_instance.mobile=mobile
                personal_details_instance.gender=req_data['pan1_gender']
                personal_details_instance.disability=req_data['disability']
                personal_details_instance.save()

                log.info("PAN Entry Updated")

            if Personal_Details.objects.filter(P_Id=client_id).exists():
                personal_details_instance=Personal_Details.objects.get(P_Id=client_id)
                if email!='' and personal_details_instance.email=='':
                    personal_details_instance.email=email
                    log.info("Email Entry Updated")
            
            #Address Insert/Update
            
            personal_instance=Personal_Details.objects.get(P_Id=client_id,pan=pan1)
            if not Address_Details.objects.filter(p_id=personal_instance).exists():
                Address_Details.objects.create(
                    p_id=personal_instance,
                    flat_door_block_no=flatno,
                    name_of_premises_building=premise,
                    road_street_postoffice=road,
                    area_locality=area,
                    pincode=pin,
                    town_city=city,
                    state=state,
                    country='INDIA',
                    current_place='',
                ).save()

                log.info("Address Entry Created")
            else:
                Address_Details_instance=Address_Details.objects.get(p_id=personal_instance)
                Address_Details_instance.flat_door_block_no=flatno
                Address_Details_instance.name_of_premises_building=premise
                Address_Details_instance.road_street_postoffice=road
                Address_Details_instance.area_locality=area
                Address_Details_instance.pincode=pin
                Address_Details_instance.town_city=city
                Address_Details_instance.state=state
                Address_Details_instance.save()
                log.info("Address Entry Updated")

            result['status']="success"
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def get_personal_information(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')

            name,PAN,dob,address,gender, mobile,flat_door_block_no,name_of_premises_building = ('',)*8
            adhar, road_street_postoffice, area_locality, pincode, town_city, state, disability = ('',)*7
            p_d_exist = 0
            business_partner_id=''

            if Personal_Details.objects.filter(P_Id = client_id).exclude(pan='').exists():
                p_d_exist = 1
                for client in Personal_Details.objects.filter(P_Id = client_id):
                    if client.name!=None:
                        name = client.name.replace('|',' ')
                    else:
                        name=''

                    PAN = client.pan
                    pan1_dob = client.dob.strftime('%d-%m-%Y')
                    # temp_date = json_serial(pan1_dob)
                    # dob = temp_date.split('T')[0]
                    dob = pan1_dob
                    gender = client.gender
                    mobile = client.mobile
                    adhar = client.aadhar_no
                    disability = client.disability
                    business_partner_id=client.business_partner_id.id

                    personal_instance=Personal_Details.objects.get(P_Id=client_id)
                    for a in Address_Details.objects.filter(p_id=personal_instance):
                        flat_door_block_no = a.flat_door_block_no
                        name_of_premises_building = a.name_of_premises_building
                        road_street_postoffice = a.road_street_postoffice
                        area_locality = a. area_locality
                        pincode = a.pincode
                        town_city = a.town_city
                        state = a.state
            else:
                for client in Client_fin_info1.objects.filter(client_id = client_id):
                    name = client.employee_name.replace('|',' ')
                    PAN = client.employee_pan

            result['status'] = 'success'
            result['name'] = name
            result['adhar'] = adhar
            result['PAN'] = PAN
            result['dob'] = dob
            result['gender'] = gender
            result['mobile'] = mobile
            result['flat_door_block_no'] = flat_door_block_no.upper()
            result['name_of_premises_building'] = name_of_premises_building.upper()
            result['road_street_postoffice'] = road_street_postoffice.upper()
            result['area_locality'] = area_locality.upper()
            result['pincode'] = pincode.upper()
            result['town_city'] = town_city.upper()
            result['state'] = state
            result['disability'] = disability
            result['client_id'] = client_id
            result['p_d_exist'] = p_d_exist
            result['business_partner_id']=business_partner_id
        except Exception as ex:
            result['status'] = 'Error in json data: %s' % ex

        return HttpResponse(json.dumps(result), content_type='application/json')

    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def check_update_registered_pan(request):
    if request.method=='POST':
        result={}
        try:
            pan=request.POST.get('pan')
            query=request.POST.get('query')
            entry_exist = 0

            ###INsert/Update
            if not salatclient.objects.filter(PAN=pan).exists():
                if query=='update':
                    salatclient.objects.create(
                        PAN=pan,
                        Is_ERI_reg=1,
                    ).save()
            else:
                entry_exist = 1
           
            result['status'] = "success"
            result['pan'] = pan
            result['entry_exist'] = entry_exist
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def get_pin_API(request):
    if request.method=='POST':
        result={}
        try:
            pin=request.POST.get('pin')

            resp = requests.get("https://api.data.gov.in/resource/0a076478-3fd3-4e2c-b2d2-581876f56d77?format=json&api-key=579b464db66ec23bdd0000019f9deeedda054c8066636274a8b93734&filters[pincode]=%s" % pin)
            # log.info(resp.json())
            result['pin']=resp.json()

            result['status'] = "success"
            # result['pin'] = pin
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def get_state(request,id):
    # BankList = Bank_List.objects.all()
    # log.info('id : '+str(int(id,16) ))
    if SalatTaxUser.objects.filter(id=id).exists():
        salattaxid = SalatTaxUser.objects.filter(id=id).first()
    else:
        salattaxid = None

    state_list = State_Code.objects.all()
    BankList = Bank_List.objects.all()
    return render(request,'personal_information.html',
        {
        'state_list':state_list,
        'Bank_List':BankList,
        'id':salattaxid,
        'client_id':id
        })

@csrf_exempt
def change_pid(request):
    if request.method=='POST':
        result={}
        try:
            # personal_details_instance=Personal_Details.objects.get(pan='AYIPA8530D')
            # personal_details_instance.P_Id=6
            # personal_details_instance.save()

            result['status'] = "success"
            # result['pin'] = pin
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

def get_city(resp):
    if len(resp['records']) == 0:
        return ''
    else:
        return resp['records'][0]['taluk']
    # return str( type(resp) )

@csrf_exempt
def update_DB(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            option=request.POST.get('option')

            state_list = State_Code.objects.all()
            F16_No = 1
            n_80c = 0
            n_80d = 0
            n_80ccc = 0
            n_80ccd = 0
            n_80tta = 0
            n_80e = 0
            n_80g = 0
            state = ''
            c_pincode = ''
            c_city = ''
            max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
            R_ID = get_rid(client_id)

            salary, amtNexpt, Vop, pls, ea, lta, taxNonMp, hra, oa = (0,)*9
            personal_instance=Personal_Details.objects.get(P_Id=client_id)
            if option=='insert':
                for c in Client_fin_info1.objects.filter(client_id = client_id,flag=0):
                    ca = (c.company_address or '')
                    for s in state_list:
                        if re.search(s.state, c.company_address, re.IGNORECASE):
                            state = s.state
                    pin_list = re.findall(r"\d{6}", c.company_address)
                    c_pincode = pin_list[0]
                    if c_pincode != '':
                        ca_new = ca.split(c_pincode)[0]
                        resp = requests.get("https://api.data.gov.in/resource/0a076478-3fd3-4e2c-b2d2-581876f56d77?format=json&api-key=579b464db66ec23bdd0000019f9deeedda054c8066636274a8b93734&filters[pincode]=%s" % c_pincode)
                        c_city = get_city(resp.json())
                    else:
                        ca_new = ca

                    if not Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=c.company_name).exists():
                        Employer_Details.objects.create(
                            P_id=personal_instance,
                            FY=c.ay,
                            F16_no=F16_No,
                            Name_of_employer=c.company_name,
                            PAN_of_employer='',
                            Address_of_employer= ca_new,
                            Employer_category = 'Other',
                            residential_status = 'RES',
                            Employer_state = state,
                            Employer_pin = c_pincode,
                            Employer_city = c_city,
                        ).save()
                        log.info("Employer_Details Entry Created")
                        # Don't change Name_of_employer --> dependency in income_details.py
                    F16_No += 1

                    n_80c += (c.elss or 0) + (c.epf or 0) + (c.ppf or 0) + (c.vpf or 0) + (c.lic or 0)
                    n_80c += (c.nsc_interest or 0) + (c.superannuation or 0) + (c.home_loan_principal or 0)
                    n_80c += (c.fd or 0) + (c.ulip or 0) + (c.child_edu_fee or 0)
                    n_80c += (c.sukanya_samriddhi_scheme or 0) + (c.stamp_duty or 0)
                    n_80d += (c.number_80d_self_family or 0) + (c.number_80d_parents or 0)
                    n_80d += (c.medical_insurance or 0)
                    n_80ccc += (c.number_80ccc or 0)
                    n_80ccd += (c.number_80ccd or 0)
                    n_80tta += (c.number_80tta or 0)
                    n_80g += (c.number_80g or 0)
                    n_80e += (c.number_80e or 0)

                    salary += (c.salary_as_per_provision or 0)
                    Vop += (c.value_of_perquisites or 0)
                    pls += (c.profits_in_lieu_of_salary or 0)
                    ea += (c.entertainment_allowance or 0)
                    lta += (c.lta or 0)
                    hra += (c.hra_claimed or 0)
                    oa += (c.other_allowance or 0) + (c.medical_allowance or 0) + (c.conveyance_allowance_allowed or 0)

                    if not Income.objects.filter(R_Id=R_ID,company_name=c.company_name).exists():
                        Income.objects.create(
                            R_Id = R_ID,
                            company_name = c.company_name,
                            Salary = float(c.salary_as_per_provision or 0)-abs(float(c.hra_allowed or 0)-float(c.lta or 0)-float(c.other_allowance or 0) + float(c.medical_allowance or 0) + float(c.conveyance_allowance_allowed or 0)),
                            Value_of_perquisites = c.value_of_perquisites,
                            Profits_in_lieu_of_salary = c.profits_in_lieu_of_salary,
                            Entertainment_Allowance = c.entertainment_allowance,
                            LTA = c.lta,
                            Form16_HRA = c.hra_allowed,
                            Other_allowances = (c.other_allowance or 0) + (c.medical_allowance or 0) + (c.conveyance_allowance_allowed or 0),
                            Profession_Tax = c.profession_tax,
                        ).save()
                        log.info("Income Entry Created")

                if not Employer_Details.objects.filter(P_id=personal_instance).exists():
                    Employer_Details.objects.create(
                        Name_of_employer='',
                        P_id = personal_instance,
                        Employer_category = 'Other',
                        residential_status= 'RES',
                    ).save()
                    log.info("Employer_Details Entry Created")


                ###### Update VI_Deductions ########################
                if Employer_Details.objects.filter(P_id=personal_instance).exists():
                    Employer_Obj=Employer_Details.objects.filter(P_id=personal_instance)

                    S_80C=0
                    S_80D=0

                    for employer in Employer_Obj:
                        employer_name=employer.Name_of_employer
                        if Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).exists():
                            form16 = Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).latest('time_stamp')
                            ###According to new update superannuation=80C
                            ###According to new update number_80d_self_family=80D
                            S80C = form16.superannuation
                            S80D = form16.number_80d_self_family

                            if S80C!=None:
                                S_80C+=S80C
                            if S80D!=None:
                                S_80D+=S80D

                    if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
                        VI_Deductions.objects.create(
                            R_Id = R_ID,
                            VI_80_C = S_80C,
                            VI_80_D = S_80D,
                            VI_80_CCC = n_80ccc,
                            VI_80_CCD_1B = n_80ccd,
                            VI_80_TTA = n_80tta,
                            VI_80_G = n_80g,
                            VI_80_E = n_80e,
                        ).save()
                        log.info("VI_Deductions Entry Created")
                    else:
                        Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)
                        Deductions_instance.VI_80_C = S_80C
                        Deductions_instance.VI_80_D = S_80D
                        Deductions_instance.save()

                    ############# Insert/Update 80D in Variables_80d ##########

                    S_HealthCheckup=0
                    P_HealthCheckup=0
                    S_InsurancePremium=S_80D
                    P_InsurancePremium=0
                    S_MedicalExpenditure=0
                    P_MedicalExpenditure=0

                    if not Variables_80d.objects.filter(R_Id=R_ID).exists():
                        Variables_80d.objects.create(
                            R_Id = R_ID,
                            self_health_checkup = S_HealthCheckup,
                            parent_health_checkup= P_HealthCheckup,
                            self_insurance_premium = S_InsurancePremium,
                            parent_insurance_premium = P_InsurancePremium,
                            self_medical_expenditure = S_MedicalExpenditure,
                            parent_medical_expenditure = P_MedicalExpenditure,
                        ).save()
                    else:
                        Variables_80d_inst = Variables_80d.objects.get(R_Id=R_ID)
                        Variables_80d_inst.self_insurance_premium = S_InsurancePremium
                        Variables_80d_inst.save()

                # if not Return_Details.objects.filter(P_id=personal_instance).exists():
                    # Return_Details.objects.create(
                    #     R_id = client_id,
                    #     P_id = personal_instance,
                    #     FY = '2018-2019',
                    #     AY = '2017-2018',
                    # ).save()
                max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
                if max_r_id['R_id__max']==None:
                    latest_r_id=0
                else:
                    latest_r_id=max_r_id['R_id__max']+1
                    
                if not Return_Details.objects.filter(P_id=personal_instance).exists():
                    Return_Details.objects.create(
                        R_id = latest_r_id,
                        P_id = personal_instance,
                        business_partner_id=personal_instance.business_partner_id,
                        FY = '2018-2019',
                        AY = '2019-2020',
                    ).save()
                    log.info("Return_Details Entry Created")
                else:
                    Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
                    log.info('Return ID : ' + str(Return_Details_instance.R_id) )

                # if not Income.objects.filter(R_Id=client_id).exists():
                    # Income.objects.create(
                    #     R_Id = client_id,
                    #     Salary = salary,
                    #     Value_of_perquisites = Vop,
                    #     Profits_in_lieu_of_salary = pls,
                    #     Entertainment_Allowance = ea,
                    #     LTA = lta,
                    #     Form16_HRA = hra,
                    #     Other_allowances = oa,
                    # ).save()

            result['status'] = "success"
            result['client_id'] = client_id
            result['F16_No'] = F16_No
            result['c_city'] = c_city
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def get_salattax_client(request):
    # state_list = State_Code.objects.all()
    # log.info(state_list)
    data = []

    # if Personal_Details.objects.exists():
    #     for client in Personal_Details.objects.filter(Business_Id = 1):
    #         result = {}
    #         username = ''
    #         result['pan'] = client.pan
    #         if client.name is None:
    #             result['name'] = ''
    #         else:
    #             result['name'] = client.name.replace('|',' ')
    #             username = client.name.replace('|',' ')
    #         result['mail'] = client.email
    #         result['phone'] = client.mobile
    #         created_login = client.P_Id
    #         result['created_login'] = created_login

    #         # if User.objects.filter(id=client.P_Id).exists() and username != '':
    #         #     atuser = User.objects.get(id=client.P_Id)
    #         #     username = atuser.username

    #         if username == '':
    #             username = client.P_Id

    #         salat_client = 0
    #         no_of_itr = ''
    #         if salat_registration.objects.filter(pan=client.pan).exists():
    #             salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
    #             if salat_registration_instance.is_salat_client == 'Salat Client':
    #                 salat_client = 1
    #             no_of_itr = salat_registration_instance.downloaded_itr_count

    #         latest_income = 0
    #         total_tax_leakage = 0
    #         p_age = client.dependent_parent_age or ''
    #         invested_share = 'no'
    #         shares_year = 0
    #         if Investment_Details.objects.filter(P_Id=client.P_Id).exists():
    #             for investment in Investment_Details.objects.filter(P_Id = client.P_Id):
    #                 if(investment.year_of_first_share_sold is not None):
    #                     if(investment.year_of_first_share_sold != ''):
    #                         invested_share = 'yes'
    #                         shares_year = investment.year_of_first_share_sold

    #         for itr in temp_ITR_value.objects.filter(PAN = client.pan).order_by('-year').reverse():
    #             if(itr.year!='2013'):
    #                 s_tax = cal_salat_tax(client.pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
    #                 if(itr.year!='2018'):
    #                     latest_income = s_tax['total_income_chart']
    #                 total_tax_leakage += s_tax['tax_leakage']
    #                 # log.info(client.P_Id)
    #                 # log.info(client.pan)
    #                 # log.info(s_tax)

    #         went_on_last_slide = 0
    #         click_on_open_tsa = 0
    #         click_on_know_more = 0
    #         checked_offer = 0
    #         subscribed = 0
    #         if user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide').exists():
    #             for ut in user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide'):
    #                 went_on_last_slide = 1
    #         if user_tracking.objects.filter(client_id=client.P_Id,event_name='Open my Tax Savings Account').exists():
    #             click_on_open_tsa = 1
    #         if user_tracking.objects.filter(client_id=client.P_Id,event_name='Know More').exists():
    #             click_on_know_more = 1
    #         if user_tracking.objects.filter(client_id=client.P_Id,event_name='Get a Discount',event='Yes btn click').exists():
    #             checked_offer = 1
    #         if user_tracking.objects.filter(client_id=client.P_Id,event_name='success',event='payment').exists():
    #             subscribed = 1

    #         total_tax_leakage = total_tax_leakage * -1
    #         result['username'] = username
    #         result['salat_client'] = salat_client
    #         result['no_of_itr'] = no_of_itr
    #         # locale.setlocale(locale.LC_MONETARY, 'en_IN')
    #         # result['latest_income'] = locale.currency(latest_income, grouping=True)
    #         result['latest_income'] = latest_income
    #         # result['total_tax_leakage'] = locale.currency(total_tax_leakage, grouping=True)
    #         result['total_tax_leakage'] = total_tax_leakage
    #         result['went_on_last_slide'] = went_on_last_slide
    #         result['click_on_open_tsa'] = click_on_open_tsa
    #         result['click_on_know_more'] = click_on_know_more
    #         result['checked_offer'] = checked_offer
    #         result['subscribed'] = subscribed
    #         data.append(result)

    for stu in SalatTaxUser.objects.exclude(id=1):
        result = {}
        client_id = stu.id
        username = stu.id
        salat_client = 2
        no_of_itr = ''
        latest_income = 0
        total_tax_leakage = 0
        went_on_last_slide = 0
        click_on_open_tsa = 0
        click_on_know_more = 0
        checked_offer = 0
        subscribed = ''
        pan = ''
        name = ''
        mail = stu.email
        phone = ''
        created_login = stu.created.strftime('%Y-%m-%d %H:%m:%S')
        timer_ended = 0
        otp_trigger = 2
        otp_submitted = 2

        for client in Personal_Details.objects.filter(P_Id = stu.id):
            pan = client.pan
            if client.name is None:
                name = ''
            else:
                name = client.name.replace('|',' ')
                username = client.name.replace('|',' ')
            mail = stu.email
            phone = client.mobile
            # created_login = stu.id

            if User.objects.filter(id=client.P_Id).exists() and username != '':
                atuser = User.objects.get(id=client.P_Id)
                username = atuser.username

            if salat_registration.objects.filter(pan=client.pan).exists():
                salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
                if salat_registration_instance.is_salat_client == 'Salat Client':
                    salat_client = 1
                    otp_trigger = 1
                    otp_submitted = 1
                else:
                    salat_client = 0
                    if salat_registration_instance.otp_trigger == '1':
                        otp_trigger = 1
                    else:
                        otp_trigger = 0
                    if salat_registration_instance.otp_submitted == '1':
                        otp_submitted = 1
                    else:
                        otp_submitted = 0
                no_of_itr = salat_registration_instance.downloaded_itr_count
            else:
                no_of_itr = 'NA'

            p_age = client.dependent_parent_age or ''
            invested_share = 'no'
            shares_year = 0
            if Investment_Details.objects.filter(P_Id=client.P_Id).exists():
                for investment in Investment_Details.objects.filter(P_Id = client.P_Id):
                    if(investment.year_of_first_share_sold is not None):
                        if(investment.year_of_first_share_sold != ''):
                            invested_share = 'yes'
                            shares_year = investment.year_of_first_share_sold or 0

            for itr in temp_ITR_value.objects.filter(PAN = client.pan).order_by('-year').reverse():
                if(itr.year!='2013'):
                    s_tax = cal_salat_tax(client.pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
                    if(itr.year!='2018'):
                        latest_income = s_tax['total_income_chart']
                    total_tax_leakage += s_tax['tax_leakage']

            if user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide').exists():
                for ut in user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide'):
                    went_on_last_slide = 1
            if user_tracking.objects.filter(client_id=client.P_Id,event_name='Open my Tax Savings Account').exists():
                click_on_open_tsa = 1
            if user_tracking.objects.filter(client_id=client.P_Id,event_name='Know More').exists():
                click_on_know_more = 1
            if user_tracking.objects.filter(client_id=client.P_Id,event_name='Get a Discount',event='Yes btn click').exists():
                checked_offer = 1
            if user_tracking.objects.filter(client_id=client.P_Id,event_name='success',event='payment').exists():
                subscribed = 'Success'
            elif user_tracking.objects.filter(client_id=client.P_Id,event_name='failure',event='payment').exists():
                subscribed = 'Failure'
            else:
                subscribed = 'NA'

            for ut in user_tracking.objects.filter(client_id=client.P_Id,event_name='Waiting Timer Ended'):
                timer_ended = timer_ended + 1


        total_tax_leakage = total_tax_leakage * -1
        result['username'] = username
        result['created_login'] = created_login
        result['pan'] = pan
        result['name'] = name
        result['mail'] = mail
        result['phone'] = phone
        result['client_id'] = client_id
        result['salat_client'] = salat_client
        result['no_of_itr'] = no_of_itr
        result['otp_trigger'] = otp_trigger
        result['otp_submitted'] = otp_submitted
        result['timer_ended'] = timer_ended
        locale.setlocale(locale.LC_MONETARY, 'en_IN')
        result['latest_income'] = locale.currency(latest_income, grouping=True)
        # result['latest_income'] = latest_income
        result['total_tax_leakage'] = locale.currency(total_tax_leakage, grouping=True)
        # result['total_tax_leakage'] = total_tax_leakage
        result['went_on_last_slide'] = went_on_last_slide
        result['click_on_open_tsa'] = click_on_open_tsa
        result['click_on_know_more'] = click_on_know_more
        result['checked_offer'] = checked_offer
        result['subscribed'] = subscribed
        data.append(result)

    # a = {'hour': None}
    # for key in a:
    #     a[key] = list()
    # for i in xrange(3):
    #     a['hour'].append(i)
        # for j in xrange(3):
        #     a[i].append(i+j)

    # log.info(data)
    return render(request,'salattax_dashboard.html',
        {
        # 'state_list':state_list,
        'data':data,
        # 'id':salattaxid,
        })

def cal_salat_tax(pan,year,ITR, p_age, invested_share, shares_year):
    if(p_age=='' or p_age == 'NA' or p_age == '-'):
        p_age = 0
    elif p_age=='OC':
        p_age=40
    elif p_age=='SC' or p_age=='SSC':
        p_age=65
    # log.info(pan+" "+year+" "+ITR)
    GTI, total_capital_gain, schedule_si_income, income_from_salary, income_from_hp = (0,)*5
    income_from_os, business_or_profession, c_80, d_80, ccg_80, ccd_1b_80, e_80, ee_80, g_80, gg_80, ccd_2_80, tta_80 = (0,)*12
    schedule_si_tax, tax_paid, Tx_add_value, total_tax_interest, refund = (0,)*5

    capital_gain, cal_GTI, TI, Tx, Tx1, rebate, TxR, tax_leakage, SH, TxSH, EC, ToTax = (0,)*12
    result={}
    c_80_limit, d_80_limit, ccd_1b_80_limit, ccg_80_limit = (0,)*4

    for itr in temp_ITR_value.objects.filter(PAN = pan,year=year):
        total_capital_gain = get_int(itr.capital_gain)
        schedule_si_income = get_int(itr.schedule_si_income)
        income_from_salary = get_int(itr.income_from_salary)
        income_from_hp = get_int(itr.income_from_hp)
        income_from_os = get_int(itr.income_from_other_src)
        ccd_1b_80 = get_int(itr.ccd_1b_80)
        ccd_2_80 = get_int(itr.ccd_2_80)
        e_80 = get_int(itr.e_80)
        ee_80 = get_int(itr.ee_80)
        g_80 = get_int(itr.g_80)
        gg_80 = get_int(itr.gg_80)
        tta_80 = get_int(itr.tta_80)
        schedule_si_tax = get_int(itr.schedule_si_tax)
        tax_paid = get_int(itr.tax_paid)
        business_or_profession = get_int(itr.business_or_profession)
        total_tax_interest = get_int(itr.total_tax_interest)

    if(schedule_si_tax=='NA' or schedule_si_tax=='' ):
        schedule_si_tax = 0
    if(total_capital_gain=='NA' or total_capital_gain=='' ):
        total_capital_gain = 0
    if(schedule_si_income=='NA' or schedule_si_income=='' ):
        schedule_si_income = 0
        
    capital_gain = total_capital_gain - schedule_si_income
    cal_GTI = income_from_salary + income_from_hp + capital_gain + income_from_os + business_or_profession

    # for total income chart values only
    temp_income_hp, total_income_chart, temp_capital_gain = (0,)*3
    if(income_from_hp<0):
        temp_income_hp = 0
    else:
        temp_income_hp = income_from_hp
    if(capital_gain<0):
        temp_capital_gain = 0
    else:
        temp_capital_gain = capital_gain

    total_income_chart = income_from_salary + temp_income_hp + temp_capital_gain + income_from_os + business_or_profession

    cess_cal, SH, p_age_cal = (0,)*3
    
    if(cal_GTI<1200000):
        if(invested_share=='no'):
            ccg_80 = 25000
        if(invested_share=='yes' and int(shares_year)>=2014):
            ccg_80 = 25000

    if(year=='2014'):
        slab1_range = 200000
        slab2_range = 500000
        slab3_range = 1000000
        per_slab0 = 0
        per_slab1 = 0.1
        per_slab2 = 0.2
        per_slab3 = 0.3
        Tx_add_value = 30000
        rebate_limit = 500000
        rebate_value = 2000
        cess_cal = 0.03
        c_80 = 100000
        ccd_1b_80 = 0
        p_age_cal = int(p_age) - 4
        if(p_age_cal>60):
            d_80 = 35000
        else:
            d_80 = 30000

    if(year=='2015'):
        slab1_range = 250000
        slab2_range = 500000
        slab3_range = 1000000
        per_slab0 = 0
        per_slab1 = 0.1
        per_slab2 = 0.2
        per_slab3 = 0.3
        Tx_add_value = 25000
        rebate_limit = 500000
        rebate_value = 2000
        cess_cal = 0.03
        c_80 = 150000
        ccd_1b_80 = 0
        p_age_cal = int(p_age) - 3
        if(p_age_cal>60):
            d_80 = 35000
        else:
            d_80 = 30000

    if(year=='2016'):
        slab1_range = 250000
        slab2_range = 500000
        slab3_range = 1000000
        per_slab0 = 0
        per_slab1 = 0.1
        per_slab2 = 0.2
        per_slab3 = 0.3
        Tx_add_value = 25000
        rebate_limit = 500000
        rebate_value = 2000
        cess_cal = 0.03
        c_80 = 150000
        ccd_1b_80 = 50000
        p_age_cal = int(p_age) - 2
        if(p_age_cal>60):
            d_80 = 55000
        else:
            d_80 = 50000

    if(year=='2017'):
        slab1_range = 250000
        slab2_range = 500000
        slab3_range = 1000000
        per_slab0 = 0
        per_slab1 = 0.1
        per_slab2 = 0.2
        per_slab3 = 0.3
        Tx_add_value = 25000
        rebate_limit = 500000
        rebate_value = 5000
        cess_cal = 0.03
        c_80 = 150000
        ccd_1b_80 = 50000
        p_age_cal = int(p_age) - 1
        if(p_age_cal>60):
            d_80 = 55000
        else:
            d_80 = 50000

    if(year=='2018'):
        slab1_range = 250000
        slab2_range = 500000
        slab3_range = 1000000
        per_slab0 = 0
        per_slab1 = 0.05
        per_slab2 = 0.2
        per_slab3 = 0.3
        Tx_add_value = 12500
        rebate_limit = 350000
        rebate_value = 2500
        cess_cal = 0.03
        c_80 = 150000
        ccd_1b_80 = 50000
        p_age_cal = int(p_age)
        if(p_age_cal>60):
            d_80 = 55000
        else:
            d_80 = 50000
        ccg_80 = 0

    TI = cal_GTI - c_80 - d_80 - ccg_80 - ccd_1b_80 - ccd_2_80 - e_80 - ee_80 - g_80 - gg_80 - tta_80
    
    if(TI>slab3_range):
        Tx = ((TI-slab3_range)* per_slab3 ) + 100000 + Tx_add_value
    elif(TI<slab3_range and TI>slab2_range):
        Tx = ((TI-slab2_range)* per_slab2 ) + Tx_add_value
    elif(TI<slab2_range and TI>slab1_range):
        Tx = ((TI-slab1_range)* per_slab1 )
    elif(TI<slab1_range):
        Tx = 0

    # log.info(str(((TI-slab2_range)* per_slab2 ) + Tx_add_value))
    # log.info(str(TI))
    # log.info(str(slab2_range) )
    # log.info(str(per_slab2) )

    if(TI<rebate_limit):
        rebate = rebate_value
    if(Tx + Tx1 <=0):
        rebate = 0

    # Tx1 = schedule_si_income - schedule_si_tax
    Tx1 = schedule_si_tax

    TxR = (Tx + Tx1) - rebate
    if(year=='2014'):
        if(TI>10000000):
            SH = round(TxR * 0.1)
    if(year=='2015'):
        if(TI>10000000):
            SH = round(TxR * 0.1)
    if(year=='2016'):
        if(TI>10000000):
            SH = round(TxR * 0.12)
    if(year=='2017'):
        if(TI>10000000):
            SH = round(TxR * 0.15)
    if(year=='2018'):
        if(TI>5000000 and TI<10000000 ):
            SH = round(TxR * 0.1)
        if( TI>10000000 ):
            SH = round(TxR * 0.15)

    TxSH = TxR + SH
    EC = round(TxSH * cess_cal)
    if(TI<=0):
        TI = 0
        TxSH = 0
        EC = 0
        
    ToTax = round(TxSH + EC)
    refund = total_tax_interest - tax_paid
    if refund<=0:
        tax_paid = tax_paid - abs(refund)
    tax_leakage = round(ToTax - tax_paid)

    result['ITR'] = ITR
    result['capital_gain'] = capital_gain
    result['year'] = year
    result['cal_GTI'] = cal_GTI
    result['TI'] = TI
    result['Tx'] = round(Tx)
    result['Tx1'] = Tx1
    result['rebate'] = rebate
    result['TxR'] = round(TxR)
    result['SH'] = SH
    result['TxSH'] = round(TxSH)
    result['EC'] = EC
    result['ToTax'] = ToTax
    result['tax_paid'] = tax_paid
    result['tax_leakage'] = tax_leakage
    result['p_age_cal'] = p_age_cal
    result['c_80'] = c_80
    result['d_80'] = d_80
    result['ccg_80'] = ccg_80
    result['ccd_1b_80'] = ccd_1b_80
    result['ccd_2_80'] = ccd_2_80
    result['e_80'] = e_80
    result['ee_80'] = ee_80
    result['g_80'] = g_80
    result['gg_80'] = gg_80
    result['tta_80'] = tta_80
    result['total_income_chart'] = total_income_chart
    result['refund'] = refund
    result['total_tax_interest'] = total_tax_interest
    return result

def get_int(value):
    if(value == 'NA' or value == '' ):
        return 0
    elif('.' in value):
        if(value.split('.')[0].isdigit() ):
            return int(value.split('.')[0])
        else:
            return 0
    else:
        return int(value)

def unique(list1):
    # intilize a null list
    unique_list = []
    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x.startswith( 'viewed' ):
            # log.info(x)
            x = x.split(' ')[1]
            if x not in unique_list and x != '':
                unique_list.append(x)
    
    return unique_list

def unique_arr(list1):
    unique_list = []
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    
    return unique_list

def get_flow(start_end,total_page):
    data = []
    source = []
    target = []
    value = []

    s = unique_arr(start_end)
    log.info(start_end)
    # log.info(total_page)
    for x in xrange(0,len(s)-1):
        count = 0
        temp_s = '-';
        temp_t = '-';
        for ele in start_end:
            if (ele == s[x]): 
                count = count + 1

        for i in xrange(0,len(total_page)):
            if(s[x].split(' ')[0] == total_page[i]):
                temp_s = i
                break
        #     else:
        #         log.info('*** '+s[x].split(' ')[0] +' == ' + total_page[i])
        # if(temp_s == '-'):
        #     log.info(s[x].split(' ')[0])

        for j in xrange(0,len(total_page)):
            if(s[x].split(' ')[1] == total_page[j]):
                temp_t = j
                break

        if temp_s!='-' and temp_t!='-' and temp_s!=temp_t:
            value.append(count)
            source.append(temp_s)
            target.append(temp_t)


    result = {}
    result['total_page'] = total_page
    result['source'] = source
    result['target'] = target
    result['value'] = value
    data.append(result)
    return data

@csrf_exempt
def get_client_flow(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')

            event_flow = []
            event_name_flow = []
            user_id_flow = []
            platform_flow = []
            date_flow = []
            count_start = 0
            page_name_1 = []
            total_page = []
            redirect_1_2 = []
            session_count_arr = []
            if user_tracking.objects.filter(client_id=client_id).exists():
                for ut in user_tracking.objects.filter(client_id=client_id).order_by('-created_time'):
                    event_flow.append(ut.event)
                    event_name_flow.append(ut.event_name)
                    user_id_flow.append(ut.user_id)
                    platform_flow.append(ut.device_platform)
                    date_flow.append(str(ut.created_time.strftime('%d-%m-%Y %H:%M %S')) )
                    session_count_arr.append(ut.session_count)
                    # if(ut.time_on_site == '00:00' and "http" in ut.event_name):
                    #     # count_start += 1
                    #     p_name_1 = ut.event_name.split('/')[len(ut.event_name.split('/'))-2]
                    #     page_name_1.append(ut.event+ ' ' +p_name_1)
                    #     r_1_2 = user_tracking.objects.filter(client_id=client_id,session_count=ut.session_count).order_by('created_time')[1]
                    #     if "http" in r_1_2.event_name:
                    #         rd_1_2 = r_1_2.event_name.split('/')[len(r_1_2.event_name.split('/'))-2]
                    #     else:
                    #         rd_1_2 = r_1_2.event_name
                    #     redirect_1_2.append(r_1_2.event+ ' ' +rd_1_2 )
                        # redirect_1_2.append(ut.session_count)
                    if( "http" in ut.event_name or "8000" in ut.event_name):
                        p_name_1 = ut.event_name.split('/')[len(ut.event_name.split('/'))-2]
                        total_page.append(ut.event+ ' ' +p_name_1)

                total_page = unique(total_page)

            log.info(client_id)
            if client_id==0 or client_id=='0':
                log.info(client_id)
                for ut in user_tracking.objects.all().order_by('-created_time'):
                    event_flow.append(ut.event)
                    event_name_flow.append(ut.event_name)
                    user_id_flow.append(ut.user_id)
                    platform_flow.append(ut.device_platform)
                    date_flow.append(str(ut.created_time.strftime('%d-%m-%Y %H:%M %S')) )
                    session_count_arr.append(str(ut.session_count)+' '+str(ut.client_id) )
                    if( "http" in ut.event_name or "8000" in ut.event_name):
                        p_name_1 = ut.event_name.split('/')[len(ut.event_name.split('/'))-2]
                        total_page.append(ut.event+ ' ' +p_name_1)

                total_page = unique(total_page)

            count_start = len(total_page)
            session_count_arr = unique_arr(session_count_arr)

            start_end = []
            if client_id==0 or client_id=='0':
                for s in session_count_arr:
                    flow_per_session = []
                    for ut in user_tracking.objects.filter(client_id=s.split(' ')[1],session_count=s.split(' ')[0]).order_by('created_time'):
                        if( "http" in ut.event_name):
                            # log.info(ut.session_count +' - ' +ut.event_name +' ' + str( ut.created_time) )
                            flow_per_session.append(ut.event_name.split('/')[len(ut.event_name.split('/'))-2])
                    log.info(flow_per_session)

                    for x in xrange(0,len(flow_per_session)-1):
                        start_end.append(flow_per_session[x]+' '+flow_per_session[x+1])
            else:
                for s in session_count_arr:
                    flow_per_session = []
                    for ut in user_tracking.objects.filter(client_id=client_id,session_count=s).order_by('created_time'):
                        if( "http" in ut.event_name):
                            # log.info(ut.session_count +' - ' +ut.event_name +' ' + str( ut.created_time) )
                            flow_per_session.append(ut.event_name.split('/')[len(ut.event_name.split('/'))-2])
                    # log.info(flow_per_session)

                    for x in xrange(0,len(flow_per_session)-1):
                        start_end.append(flow_per_session[x]+' '+flow_per_session[x+1])
            
            log.info(start_end)
            client_flow = get_flow(start_end,total_page)
            # log.info(client_flow)

            # result['status'] = "success"
            result['event_flow'] = event_flow
            result['event_name_flow'] = event_name_flow
            result['user_id_flow'] = user_id_flow
            result['platform_flow'] = platform_flow
            result['date_flow'] = date_flow
            result['count_start'] = count_start
            result['total_page'] = total_page
            # result['page_name_1'] = page_name_1
            # result['redirect_1_2'] = redirect_1_2
            result['session_count_arr'] = session_count_arr
            result['start_end'] = start_end
            result['client_flow'] = client_flow
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def get_user_tracking(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')

            visit_tsa = 0

            if user_tracking.objects.filter(client_id=client_id).exists():
                for ut in user_tracking.objects.filter(client_id=client_id).order_by('-created_time'):
                    if "/tax_saving_account/" in ut.event_name:
                        visit_tsa = visit_tsa+1
            
            result['status'] = "success"
            result['client_id'] = client_id
            result['visit_tsa'] = visit_tsa
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in json data: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def client_current_state(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            if client_id != '':
                client_id = int(client_id)
            else:
                client_id = 0

            sign_in = 0
            salat_client = 0
            cal_tax_submit = 0
            next_fin_goal = 0
            view_tax_report = 0
            seen_last_slide = 0
            view_tsa = 0
            checked_offer = 0
            click_open_tsa = 0
            click_submit_btn = 0
            success = 0
            failure = 0

            submit_pan_dob = 0
            submit_otp = 0

            if client_id != 0 and client_id != 1:
                sign_in = 1
                for client in Personal_Details.objects.filter(P_Id = client_id):
                    if salat_registration.objects.filter(pan=client.pan).exists():
                        salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
                        if salat_registration_instance.is_salat_client == 'Salat Client':
                            salat_client = 1
                        # no_of_itr = salat_registration_instance.downloaded_itr_count
                # for testing only ==>
                # salat_client = 0
                if salat_client == 1:
                    if user_tracking.objects.filter(client_id=client_id).exists():
                        for ut in user_tracking.objects.filter(client_id=client_id):
                            if(ut.event_name=='calculate tax Submit'):
                                cal_tax_submit = 1
                            if(ut.event_name=='Select Financial Goal'):
                                next_fin_goal = 1
                            if "/tax_report/" in ut.event_name:
                                view_tax_report = 1
                            if(ut.event_name=='Seen Last Slide'):
                                seen_last_slide = 1
                            if "/tax_saving_account/" in ut.event_name:
                                view_tsa = 1
                            if (ut.event_name=="Get a Discount" and "Yes" in ut.event):
                                checked_offer = 1
                            if "Open my Tax Savings Account" in ut.event_name:
                                click_open_tsa = 1
                            if "Billing Submit Button" in ut.event_name:
                                click_submit_btn = 1
                            if "/success_page/" in ut.event_name:
                                success = 1
                            if "/Failure/" in ut.event_name:
                                failure = 1

                    if cal_tax_submit == 0:
                        cal_tax_submit = 2
                    if next_fin_goal == 0:
                        next_fin_goal = 2
                    if view_tax_report == 0:
                        view_tax_report = 2
                    if seen_last_slide == 0:
                        seen_last_slide = 2
                    if view_tsa == 0:
                        view_tsa = 2
                    if checked_offer == 0:
                        checked_offer = 2
                    if click_open_tsa == 0:
                        click_open_tsa = 2
                    if click_submit_btn == 0:
                        click_submit_btn = 2
                else :
                    if user_tracking.objects.filter(client_id=client_id).exists():
                        for ut in user_tracking.objects.filter(client_id=client_id):
                            if(ut.event_name=='Register as Salat Client'):
                                submit_pan_dob = 1
                            if(ut.event_name=='Submit OTP'):
                                submit_otp = 1

                    if submit_pan_dob == 0:
                        submit_pan_dob = 2
                    if submit_pan_dob == 1 and submit_otp == 0:
                        submit_otp = 2

            if success == 1:
                failure = 0

            result['status'] = "success"
            result['client_id'] = client_id
            result['sign_in'] = sign_in
            result['salat_client'] = salat_client
            result['cal_tax_submit'] = cal_tax_submit
            result['next_fin_goal'] = next_fin_goal
            result['view_tax_report'] = view_tax_report
            result['seen_last_slide'] = seen_last_slide
            result['view_tsa'] = view_tsa
            result['checked_offer'] = checked_offer
            result['click_open_tsa'] = click_open_tsa
            result['click_submit_btn'] = click_submit_btn
            result['success'] = success
            result['failure'] = failure
            result['submit_pan_dob'] = submit_pan_dob
            result['submit_otp'] = submit_otp
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in client_current_state: %s' % ex)
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def get_session(request):
    if request.method=='POST':
        result={}
        try:
            user_id = 0
            temp_user_id = []

            if user_tracking.objects.exists():
                for ut in user_tracking.objects.order_by('-created_time'):
                    if ut.session_key is not None:
                        if "user id" in ut.session_key and "None" not in ut.session_key:
                            # log.info(ut.session_key.split('-'))
                            # user_id = int(ut.session_key.split('-')[1])+1
                            if ut.session_key.split('-')[1] != '' :
                                temp_user_id.append(int(ut.session_key.split('-')[1]))

            t_user_id = unique_arr(temp_user_id)

            if len(t_user_id) == 0:
                log.info( 0 )
                user_id = 0
            else:
                log.info( max(temp_user_id) )
                user_id = max(temp_user_id)+1
            
            result['status'] = "success"
            result['user_id'] = str(user_id)
        except Exception as ex:
            result['status']="Error : %s" % ex
            # log.error('Error in json data: %s' % ex)
            log.error('Error in : '+traceback.format_exc())
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

@csrf_exempt
def update_fy(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            fy=request.POST.get('fy') 

            personal_instance=Personal_Details.objects.get(P_Id=client_id)
            # for rd in Return_Details.objects.filter(P_id=personal_instance):
            #     log.info(rd.R_id)
            #     log.info(rd.FY)

            ay = ''
            if fy == '2015-2016':
                ay = '2016-2017'
            if fy == '2016-2017':
                ay = '2017-2018'
            if fy == '2017-2018':
                ay = '2018-2019'
            if fy == '2018-2019':
                ay = '2019-2020'

            result['status'] = "start"
            # if R_ID != 0 :
                # if Return_Details.objects.filter(R_id = R_ID).exists():
                #     for rd in Return_Details.objects.filter(R_id = R_ID):
                #         rd.FY = fy
                #         rd.AY = ay
                #         rd.save()
            max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
            if max_r_id['R_id__max']==None:
                latest_r_id=0
            else:
                latest_r_id=max_r_id['R_id__max']+1

            if not Return_Details.objects.filter(P_id=personal_instance,FY = fy,AY = ay).exists():
                Return_Details.objects.create(
                    R_id = latest_r_id,
                    P_id = personal_instance,
                    business_partner_id=personal_instance.business_partner_id,
                    FY = fy,
                    AY = ay,
                ).save()
            else:
                for rd in Return_Details.objects.filter(P_id=personal_instance,FY = fy,AY = ay):
                    rd.FY = fy
                    rd.save()

            R_ID = get_rid(client_id)

            result['status'] = "success"
            result['client_id'] = str(client_id)
            result['R_ID'] = str(R_ID)
            result['fy'] = str(fy)
        except Exception as ex:
            result['status']="Error : %s" % ex
            log.error('Error in : '+traceback.format_exc())
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        return False

def get_rid(client_id):
    R_ID = 0
    personal_instance=Personal_Details.objects.get(P_Id=client_id)
    if Return_Details.objects.filter(P_id=personal_instance).exists():
        Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
        # for rd in Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]:
        R_ID = Return_Details_instance.R_id

    return R_ID

# AKSPM5263B
