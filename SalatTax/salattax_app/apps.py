# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SalattaxAppConfig(AppConfig):
    name = 'salattax_app'
