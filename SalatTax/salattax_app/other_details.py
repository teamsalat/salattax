from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
# 
import logging
import json

from .models import SalatTaxUser,Return_Details
from .models import Bank_Details,Personal_Details,Employer_Details,Bank_List,Address_Details,tax_variables
#for url request
import requests

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

@csrf_exempt
def ifsc_api(request):
	result = {}
	pin_regex = "(([0-9]{6})|([0-9]{3}[\s][0-9]{3}))"
	baddr_regex1 = "(([-])|([\s])|([ ])|([,])|([.]))"

	try:
		if request.method == "POST":
			ifsc=request.POST.get('ifsc')

			resp = requests.get("http://api.techm.co.in/api/v1/ifsc/%s" % ifsc)
			ifsc=resp.json()
			
			result['IFSC']=ifsc
			
			return HttpResponse(json.dumps(result), content_type='application/json')
		else:
			returnesult['status']="Error"
			return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error('Error in json data: %s' % ex)

@csrf_exempt
def save_bank_details(request):
	result={}
	if request.method=='POST':
		try:
			data=request.POST.get('data')
			req_data = json.loads(data)

			client_id=req_data['client_id']
			bankID=req_data['bankID']
			bank_name=req_data['bank_name']
			ifsc=req_data['ifsc']
			acc_no=req_data['acc_no']
			acc_type=req_data['acc_type']

			result['status']= 'start'
			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			# change select query logic
			# p_id_instance=Personal_Details.objects.get(P_Id=client_id)
			if Bank_Details.objects.filter(P_id=personal_instance).exists():
				Bank_Details.objects.filter(P_id=personal_instance).update(Bank_proof='de-active')

			if not Bank_Details.objects.filter(P_id=personal_instance,IFSC_code=ifsc,Account_no=acc_no).exists():
				Bank_Details.objects.create(
					P_id = personal_instance,
					Bank_id = bankID,
					Default_bank= bank_name,
					IFSC_code = ifsc,
					Account_no = acc_no,
					Account_type = acc_type,
					Bank_proof = 'active',
				).save()
			else:
				Bank_Details.objects.filter(P_id=personal_instance,IFSC_code=ifsc,Account_no=acc_no).update(Default_bank=bank_name)
				Bank_Details.objects.filter(P_id=personal_instance,IFSC_code=ifsc,Account_no=acc_no).update(IFSC_code=ifsc)
				Bank_Details.objects.filter(P_id=personal_instance,IFSC_code=ifsc,Account_no=acc_no).update(Account_no=acc_no)
				Bank_Details.objects.filter(P_id=personal_instance,IFSC_code=ifsc,Account_no=acc_no).update(Account_type=acc_type)
				Bank_Details.objects.filter(P_id=personal_instance,IFSC_code=ifsc,Account_no=acc_no).update(Bank_proof='active')

			# if not Employer_Details.objects.filter(P_id=personal_instance).exists():
				# Employer_Details.objects.create(
				# 	P_id = personal_instance,
				# 	Employer_category = bankID,
				# 	residential_status= bank_name,
				# ).save()
			# else:
				# Employer_Details.objects.filter(P_id=personal_instance).update(Employer_category=bank_name)
				# Employer_Details.objects.filter(P_id=personal_instance).update(residential_status=ifsc)

			result['status']= 'success'
			result['req_data']= req_data
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def save_other_details(request):
	if request.method=='POST':
		result={}

		try:
			data=request.POST.get('data')
			req_data = json.loads(data)

			client_id=req_data['client_id']
			R_ID = get_rid(client_id)

			employer_category=req_data['employer_category']
			residential_status=req_data['residential_status']
			foreign_asset=req_data['foreign_asset']
			tax_income_exceed=req_data['tax_income_exceed']

			result['status']= 'start'
			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			p_id_instance=Personal_Details.objects.get(P_Id=client_id)
		
			if not Employer_Details.objects.filter(P_id=personal_instance).exists():
				Employer_Details.objects.create(
					Name_of_employer='',
					P_id = p_id_instance,
					Employer_category = employer_category,
					residential_status= residential_status,
				).save()
			else:
				Employer_Details.objects.filter(P_id=personal_instance).update(Employer_category=employer_category)
				Employer_Details.objects.filter(P_id=personal_instance).update(residential_status=residential_status)

			if not tax_variables.objects.filter(R_Id=R_ID).exists():
				tax_variables.objects.create(
					R_Id = R_ID,
					foreign_assets = foreign_asset,
					taxable_income_exceed_50l= tax_income_exceed,
				).save()
			else:
				tax_variables.objects.filter(R_Id=R_ID).update(foreign_assets = foreign_asset)
				tax_variables.objects.filter(R_Id=R_ID).update(taxable_income_exceed_50l= tax_income_exceed)

			result['status']= 'success'
			result['req_data']= req_data
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_other_details(request):
    if request.method=='POST':
        result={}

        try:
            client_id=request.POST.get('client_id')

            Employer_category = ''
            residential_status = ''
            foreign_asset = ''
            tax_income_exceed = ''
            bankID= []
            bank_name= []
            ifsc= []
            acc_no= []
            acc_type= []

            personal_instance=Personal_Details.objects.get(P_Id=client_id)
            if Employer_Details.objects.filter(P_id=personal_instance).exists():
                for client in Employer_Details.objects.filter(P_id = personal_instance):
                    Employer_category = client.Employer_category
                    residential_status = client.residential_status

            R_ID = get_rid(client_id)
            if tax_variables.objects.filter(R_Id=R_ID).exists():
                for t_v in tax_variables.objects.filter(R_Id=R_ID):
                    foreign_asset = t_v.foreign_assets
                    tax_income_exceed = t_v.taxable_income_exceed_50l

            no_of_bank = Bank_Details.objects.filter(P_id=personal_instance,Bank_proof = 'active').count()
            result['no_of_bank']= no_of_bank

            for bank in Bank_Details.objects.filter(P_id=personal_instance,Bank_proof = 'active'):
            	bankID.append( bank.Bank_id )
            	bank_name.append( bank.Default_bank )
            	ifsc.append( bank.IFSC_code )
            	acc_no.append( bank.Account_no )
            	acc_type.append( bank.Account_type )

            result['status'] = 'success'
            result['Employer_category'] = Employer_category
            result['residential_status'] = residential_status
            result['foreign_asset'] = foreign_asset
            result['tax_income_exceed'] = tax_income_exceed
            result['client_id'] = client_id
            result['bankID'] = bankID
            result['bank_name'] = bank_name
            result['ifsc'] = ifsc
            result['acc_no'] = acc_no
            result['acc_type'] = acc_type
            # result['atuser'] = atuser
        except Exception as ex:
            result['status'] = 'Error in json data: %s' % ex

        return HttpResponse(json.dumps(result), content_type='application/json')

    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_bank_list(request,id):
	if SalatTaxUser.objects.filter(id=id).exists():
		salattaxid = SalatTaxUser.objects.filter(id=id).first()
	else:
		salattaxid = None
	BankList = Bank_List.objects.all()
	return render(request,'other_details.html',
		{
		'Bank_List':BankList,
		'id':salattaxid,
		})
    # if request.method=='POST':
    #     result={}

    #     try:
    #         bank_name = []

    #         for bank in Bank_List.objects.all():
    #         	bank_name.append( bank.bank_name )

    #         result['status'] = 'success'
    #         result['bank_name'] = bank_name
    #     except Exception as ex:
    #         result['status'] = 'Error in json data: %s' % ex

    #     return HttpResponse(json.dumps(result), content_type='application/json')

    # else:
    #     result['status']="Error"
    #     return HttpResponse(json.dumps(result), content_type='application/json')

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance).exists():
		Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
		R_ID = Return_Details_instance.R_id
		# for rd in Return_Details.objects.filter(P_id=personal_instance):
		# 	log.info(rd.R_id)

	return R_ID




