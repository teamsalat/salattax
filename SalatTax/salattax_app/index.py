﻿from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone

import os, sys
import time
from io import BytesIO
from StringIO import StringIO
import logging
import json
from .models import sign_up_otp,SalatTaxUser,SalatTaxRole,Business,Business_Partner
from .models import ResetPassword,Personal_Details,user_tracking
#for url request
import requests
from datetime import date, datetime
import random
# for Email
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import hashlib
import traceback

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)
import re
from urllib2 import urlopen
import urllib2
from django.db.models import Max

from .encrypt import *

@csrf_exempt
def send_otp(request):
    if request.method=='POST':
        result={}
        try:
            mail=request.POST.get('mail')
            username=request.POST.get('username')

            otp = random.randint(100000, 999999)
            otp = str(otp)

            get_user_email = mail
            # get_user_email = 'jagrutikadam15@gmail.com'

            if not sign_up_otp.objects.filter(username=username,mailid=mail,verified='N').exists():
                sign_up_otp.objects.create(
                    username = username,
                    mailid = mail,
                    otp = otp,
                    verified = 'N',
                ).save()
            else:
                sign_up_otp_instance = sign_up_otp.objects.get(username=username,mailid=mail,verified='N')
                sign_up_otp_instance.otp=otp
                sign_up_otp_instance.save()

            # sender = 'salat.technologies@gmail.com'
            sender = 'salattax@gmail.com'
            reciever = mail
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "Salat Tax Email Verification OTP"
            msg['From'] = sender
            msg['To'] = reciever
            # <img src="../static/images/logo_blue.png" alt="Salat-logo" height="100">

            # Create the body of the message (a plain-text and an HTML version).
            html ='<html><title>Salat Tax Account</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet><style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style><body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0><table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%><tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff><tr><td style="border-top:10px solid #00A0E3"><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center><tr><td style="padding:20px 0"><table border=0 cellpadding=0 cellspacing=0 class=logo style=width:220px align=left><tr><td><a href="" target=_blank><img alt="Salat Tax" src="https:salattax.com/static/images/logo_blue.png" border=0></a></table></table><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style="width:640px;border-top:thin solid #00A0E3" align=center><tr><td style="padding:20px 0 10px;font-weight:700;font-size:18px;font-family:Verdana"class=header-title noto=""sans=""><span style=font-size:14px;font-family:verdana;font-weight:400;line-height:20px> This email is to verify your email address which you provided for sign in on <span style=color:#00A0E3> <a href="https://salattax.com/" target="_blank">salattax.com</a> </span><span><a style="font-weight:600;color:#00A0E3;cursor:pointer;text-align:center;text-decoration: none;" href=# target="_blank"></a></span>.<br>Your one time password (OTP) is <b></b>'+otp+'<br>If you did not request this just ignore this email.<br><br>Sincere Regards,<br><span style=color:#00A0E3>Salat Tax</span></span><tr><td style=padding-bottom:20px;text-align:center class=header-divider></table><br><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style="width:700px;border-bottom:10px solid #00A0E3"align=center><tr><td style="padding:0 0px 20px 20px"><table class=container style="width:640px;border-top:thin solid #00A0E3"><tr><td style=font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:10px class=footer-copy>If you have any queries regarding this email or mutual funds in general please contact on us at <span style=color:#00A0E3>salattax@gmail.com</span> or call us at <span style=color:#00A0E3>020 - 41225277</span>.</table></table></table></table></html>'
            # Record the MIME types of both parts - text/plain and text/html.
            part2 = MIMEText(html, 'html')

            # Attach parts into message container.
            # According to RFC 2046, the last part of a multipart message, in this case
            # the HTML message, is best and preferred.

            msg.attach(part2)
            # Send the message via local SMTP server.
            mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
            mail.ehlo()
            mail.starttls()
            mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
            mail.sendmail(sender, reciever, msg.as_string()) 

            result['status']= 'success'
            # result['mail']= mail
            result['username']= username
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def check_otp(request):
    if request.method=='POST':
        result={}
        try:
            mail=request.POST.get('mail')
            username=request.POST.get('username')
            password=request.POST.get('password')
            OTP=request.POST.get('OTP')

            saved_otp = ''
            error = ''
            verified_otp = 0
            verified_otp = 1 #only for testing

            for client in sign_up_otp.objects.filter(username=username,verified='N'):
                saved_otp = client.otp

            if saved_otp=='':
                error = '0 entry for username :'+username
            else:
                if OTP==saved_otp:
                    sign_up_otp_instance = sign_up_otp.objects.get(username=username,mailid=mail,verified='N')
                    sign_up_otp_instance.verified='Y'
                    sign_up_otp_instance.save()
                    verified_otp =1

            result['status']= 'success'
            result['OTP']= OTP
            result['saved_otp']= saved_otp
            result['verified_otp']= verified_otp
            result['error']= error
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def login(request, user):
    if request.method=='POST':
        result={}
        try:
            request.session['login'] = user

            log.info(request.session['login'])

            result['status']='success'
        except Exception as ex:
            result['status']= 'Error in json data: %s' % ex

        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def logout(request):
    result={}
    if request.method=='POST':
        try:
            request.session['login'] = ''

            log.info(request.session['login'])

            username = request.session.get('login')
            result['status'] = 'success'
            result['username'] = username
        except Exception as ex:
            result['status']= 'Error in json data: %s' % ex

        return result
    else:
        result['status']="Error"
        return result

@csrf_exempt
def logout_user(request):
    if request.method=='POST':
        result={}
        try:
            logout1 = logout(request)
            result['status']= 'success'
            result['logout']= logout1

        except Exception as ex:
            result['status']= 'Error : %s' % ex

        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')


# @csrf_exempt
# def get_client(request):
# 	result={}
# 	# request.session['login'] = 'jagruti'
# 	username = request.session.get('login')
# 	client_id = 0
# 	email = ''
# 	if username is None:
# 		pass
# 	else:
# 		if username!='':
# 			atuser = User.objects.get(username=username)
# 			for c in SalatTaxUser.objects.filter(user=atuser):
# 				client_id = c.id
# 				email = c.email

# 	result['username']=username
# 	result['client_id']=client_id
# 	result['email']=email

# 	return HttpResponse(json.dumps(result), content_type='application/json')
@csrf_exempt
def get_client(request):

	try:
		result={}
		# request.session['login'] = 'jagruti'
		username = request.session.get('login')
		log.info(username)
		client_id = 0
		email = ''
		business_partner_id=''
		role=''

		if username==None or username=='':
			pass
		else:
			atuser = User.objects.get(username=username)
			for c in SalatTaxUser.objects.filter(user=atuser):
				client_id = c.id
				email = c.email
				role= c.role.id

			role=SalatTaxRole.objects.get(id=role).rolename
			if role=='Partner' or role=='partner':
				partner_id=''
			elif role=='Client' or role=='client':
				partner_id='0'
			elif role=='Admin' or role=='admin':
				partner_id='0'

			business_partner_id=get_business_partner_id(client_id,partner_id)

		result['username']=username
		result['client_id']=client_id
		result['email']=email
		result['role']=role
		result['business_partner_id']=business_partner_id
	except Exception as ex:
		log.info('Error in getting client id '+traceback.format_exc())
		result['client_id']=client_id
		result['data']='Error in getting client id '+traceback.format_exc()

	return HttpResponse(json.dumps(result), content_type='application/json')


def get_business_partner_id(client_id,partner_id):
	business_partner_id=''
	try:

		business_condition={'main_business_name':'salattax',}

		if Business.objects.filter(**business_condition).exists():
			business_instance=Business.objects.get(**business_condition)

			if partner_id=='0':
				business_partner_condition={
											'main_business':business_instance,
											'partner_id':partner_id
										}
			else:
				business_partner_condition={
											'user_id':client_id,
											'main_business':business_instance,
										}


			log.info(business_partner_condition)

			business_partner_data=Business_Partner.objects.filter(**business_partner_condition).exists()
			if business_partner_data:
				business_partner_id=Business_Partner.objects.get(**business_partner_condition).id
			else:
				log.info('Business partner entry not exists in db')
		else:
			log.info('Salat Tax Entry not exists in db')

	except Exception as ex:
		log.info('Error in getting business_partner_id '+traceback.format_exc())

	return business_partner_id

@csrf_exempt
def get_client_byID(request):
    if request.method=='POST':
        result={}
        try:
            username=request.POST.get('username')
            client_id = 0
            email = ''
            if username is None:
                pass
            else:
                if username!='':
                    atuser = User.objects.get(username=username)
                    for c in SalatTaxUser.objects.filter(user=atuser):
                        client_id = c.id
                        email = c.email

            result['status']= 'success'
            result['username']= username
            result['client_id']=client_id
            result['email']=email
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')


def create_user_login(request,username,email,raw_password,role,referred_by_code):
    user_exist = 0
    log.info(username)
    log.info(email)
    try:
            if not ( User.objects.filter(username=username,email=email).exists() ):
                #insert data into User model 
                if not User.objects.filter(username=username).exists():
                    User.objects.create_user(username, email, raw_password)
                else:
                    user_exist = 1
                
                while(True):
                    referral_code=generate_referral_code('C')
                    log.info(referral_code)
                    if not SalatTaxUser.objects.filter(referral_code=referral_code).exists():
                        break

                if(referred_by_code!=''):
                    referred_by=decrypt(key, referred_by_code)
                    if SalatTaxUser.objects.filter(referred_by_code=referred_by).exists():
                        referred_by_code_db=referred_by
                    else:
                        referred_by_code_db=None
                        log.info('Invalid referred by code')
                else:
                    referred_by_code_db=None

                atuser = User.objects.get(username=username)
                if not SalatTaxUser.objects.filter(user=atuser).exists():
                    SalatTaxUser.objects.create(
                        user = atuser,
                        email = email,
                        referral_code=referral_code,
                        referred_by_code=referred_by_code_db
                    ).save()

                #create instance of User model base on username
                salattaxUser = SalatTaxUser.objects.get(user=atuser)

                #create instance of SalatTaxRole model base on rolename
                log.info('SalatTaxRole.objects.get(rolename=role)')
                role_instance = SalatTaxRole.objects.get(rolename=role)

                #update remaining data of SalatTaxUser model
                # salattaxUser.ip_address=ip
                salattaxUser.role = role_instance
                salattaxUser.save()

                #if user is authorized then go to home page with logged
                # user = authenticate(username=username, password=raw_password)
                user = username
                login(request, user)
                log.info('Login Account Created')
            else:
                log.info('Login Account Already Exists')
                user_exist = 1

    except Exception as ex:
        log.info('Error in creating user '+traceback.format_exc())

    return user_exist

@csrf_exempt
def save_user(request):
    if request.method=='POST':
        result={}
        try:
            email=request.POST.get('mail')
            username=request.POST.get('username')
            raw_password=request.POST.get('password')
            role = request.POST.get('role')
            referred_by_code = request.POST.get('code')
            user = ''

            user_exist=create_user_login(request,username,email,raw_password,role,referred_by_code)

            result['status']= 'success'
            result['raw_password']= raw_password
            result['user_exist']= user_exist
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def generate_referral_code (keyword,size=4, chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
	random_character=''.join(random.choice(chars) for _ in range(size))
	return keyword+random_character

@csrf_exempt
def check_username(request):
    if request.method=='POST':
        result={}
        try:
            username=request.POST.get('username')
            password=request.POST.get('password')
            user_exist = 0
            password_exist = 0

            log.info(User.objects.filter(username=username).exists())
            user_type=''
            if not ( User.objects.filter(username=username).exists() ):
                user_exist = 0
            else:
                user_exist = 1
                atuser = User.objects.get(username=username)
                if SalatTaxUser.objects.filter(user=atuser).exists():
                    SalatTaxUser_Obj=SalatTaxUser.objects.get(user=atuser)
                    user_role=SalatTaxUser_Obj.role.rolename
                    if user_role!=None and user_role=='Client':
                        user_type='client'
                    elif user_role!=None and (user_role=='Admin' or user_role=='Partner'):
                        user_type='business'

            # user = authenticate(username=username, password=password)
            # user = User.objects.get(password=password,username=username)
            log.info(username)
            log.info(password)
            if User.objects.filter(password=password,username=username).exists():
                log.info(password)
                login(request, username)
                log.info(password)
                password_exist = 1
            else:
                password_exist = 0
                # logout(request)

            result['status']= 'success'
            result['user_type']= user_type
            result['user_exist']= user_exist
            result['password_exist']= password_exist

        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def check_email(request):
    if request.method=='POST':
        result={}
        try:
            username=request.POST.get('username')
            mail=request.POST.get('mail')
            mailid_exist = 0

            if not ( User.objects.filter(email=mail,is_superuser=0).exists() ):
                mailid_exist = 0
            else:
                mailid_exist = 1
                mailid_exist = 0

            result['status']= 'success'
            result['mailid_exist']= mailid_exist
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def send_verification_mail(request):
    if request.method=='POST':
        result={}
        try:
            status = 'success'
            email=request.POST.get('mail')
            change_id=request.POST.get('id')
            username = ''
            log.info(change_id)

            if change_id=='password':
                username=request.POST.get('mail')
                if User.objects.filter(username=username).exists():
                    user_i = User.objects.get(username=username)
                    email = user_i.email
                else:
                    email = ""
                    status = 'username does not exist'
            if change_id=='username':
                email=request.POST.get('mail')

            if status == 'success':
                hash_object = hashlib.sha1(request.POST.get('mail'))
                hex_dig = hash_object.hexdigest()
                # now = datetime.datetime.now()

                reciever = email
                get_username = User.objects.filter(email=email,is_superuser=0).values('id')[0]['id']
                # log.info(get_username)
                get_user = SalatTaxUser.objects.get(user_id=get_username)

                forgot_password_data=ResetPassword(salattaxuser=get_user,
                    email=email,
                    username=username,
                    url=hex_dig,
                    status='0')
                forgot_password_data.save()
                log.info(forgot_password_data)

                # sender='salat.technologies@gmail.com'
                sender='salattax@gmail.com'
                msg = MIMEMultipart('alternative')
                msg['From'] = sender
                msg['To'] = reciever

                url = ''

                if change_id=='password':
                    # url = 'http://127.0.0.1:8000/reset_password/'+hex_dig
                    url = 'https://salattax.com/reset_password/'+hex_dig
                    msg['Subject'] = "Salat Tax Password RESET Verification Link"
                if change_id=='username':
                    # url = 'http://127.0.0.1:8000/reset_username/'+hex_dig
                    url = 'https://salattax.com/reset_username/'+hex_dig
                    msg['Subject'] = "Salat Tax Username RESET Verification Link"
                log.info(url)

                # Create the body of the message (a plain-text and an HTML version).
                if change_id=='password':
                    html = '<!DOCTYPE html><html><meta content="text/html; charset=UTF-8"http-equiv=Content-Type><meta content="IE=edge"http-equiv=X-UA-Compatible><meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"name=viewport><title>Salat Tax Account</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet><style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style><body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0><table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%><tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff><tr><td style="border-top:10px solid #00A0E3"><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center><tr><td style="padding:12px 0"><table border=0 cellpadding=0 cellspacing=0 width=120 class=logo style=width:120px align=left><tr><td><a href= target=_blank><img alt="scom-dashboard" src="https://salattax.com/static/images/logo_blue.png" border=0></a></table></table><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style="width:640px;border-top:thin solid #009ee3"align=center><tr><td style="padding:20px 0 10px;font-weight:700;font-size:18px;font-family:Verdana"class=header-title noto=""sans=""><span style=color:gold></span><span style=font-size:14px;font-family:verdana;font-weight:400;line-height:20px> We have received a request to reset your password for  <span style=color:#00A0E3>Salat Tax.</span> <br>Please Click on the below button to reset the password.</span><tr><td style=padding-bottom:20px;text-align:center class=header-divider></table><table border=0 cellpadding=0 cellspacing=0 width=440 class=container style=width:440px align=center><tr><td style="padding:0px 0 10px 0"class=col align=center valign=middle><a style="background:#009ee3;padding:10px 10px 10px 10px;font-weight:600;color:#FFF;cursor:pointer;text-align:center;text-decoration: none;" href='+url+' target="_blank">Reset Password</a></table><br><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style="width:700px;border-bottom:10px solid #00A0E3"align=center><tr><td style="padding:0 0px 20px 20px"><table class=container style="width:640px;border-top:thin solid #009ee3"><tr><td style=font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:10px class=footer-copy>You are receiving this email because you are a subscriber to Salat Tax. For details regarding your subscription visit <a class=email href=https://salattax.com style=color:#00A0E3;cursor:pointer;text-decoration:none;font-size:12px target=_blank> salattax.com</a></table></table></table></table></html>'
                if change_id=='username':
                    html = '<!DOCTYPE html><html><meta content="text/html; charset=UTF-8"http-equiv=Content-Type><meta content="IE=edge"http-equiv=X-UA-Compatible><meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"name=viewport><title>Salat Tax Account</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet><style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style><body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0><table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%><tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff><tr><td style="border-top:10px solid #00A0E3"><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center><tr><td style="padding:12px 0"><table border=0 cellpadding=0 cellspacing=0 width=120 class=logo style=width:120px align=left><tr><td><a href= target=_blank><img alt="scom-dashboard" src="https://salattax.com/static/images/logo_blue.png" border=0></a></table></table><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style="width:640px;border-top:thin solid #009ee3"align=center><tr><td style="padding:20px 0 10px;font-weight:700;font-size:18px;font-family:Verdana"class=header-title noto=""sans=""><span style=color:gold></span><span style=font-size:14px;font-family:verdana;font-weight:400;line-height:20px> We have received a request to reset your username for  <span style=color:#00A0E3>Salat Tax.</span> <br>Please Click on the below button to reset the Username.</span><tr><td style=padding-bottom:20px;text-align:center class=header-divider></table><table border=0 cellpadding=0 cellspacing=0 width=440 class=container style=width:440px align=center><tr><td style="padding:0px 0 10px 0"class=col align=center valign=middle><a style="background:#009ee3;padding:10px 10px 10px 10px;font-weight:600;color:#FFF;cursor:pointer;text-align:center;text-decoration: none;" href='+url+' target="_blank">Reset Username</a></table><br><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style="width:700px;border-bottom:10px solid #00A0E3"align=center><tr><td style="padding:0 0px 20px 20px"><table class=container style="width:640px;border-top:thin solid #009ee3"><tr><td style=font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:10px class=footer-copy>You are receiving this email because you are a subscriber to Salat Tax. For details regarding your subscription visit <a class=email href=https://salattax.com style=color:#00A0E3;cursor:pointer;text-decoration:none;font-size:12px target=_blank> salattax.com</a></table></table></table></table></html>'
                # Record the MIME types of both parts - text/plain and text/html.
                part2 = MIMEText(html, 'html')

                # Attach parts into message container.
                # According to RFC 2046, the last part of a multipart message, in this case
                # the HTML message, is best and preferred.

                msg.attach(part2)
                # Send the message via local SMTP server.
                mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com', 587)
                mail.ehlo()
                mail.starttls()
                mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
                mail.sendmail(sender, reciever, msg.as_string()) 

            result['status']= status
            result['mail']= email

        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def reset_password(request,data):
    get_current_id=ResetPassword.objects.filter(url=data).values('id')[0]['id']
    get_email = ResetPassword.objects.get(id=get_current_id).username
    if(get_email is None):
        get_email = ResetPassword.objects.get(id=get_current_id).email
    return render(request,'confirm_password.html',{
        'get_current_id':get_current_id,
        'get_email':get_email,
        })

@csrf_exempt
def reset_username(request,data):
    get_current_id=ResetPassword.objects.filter(url=data).values('id')[0]['id']
    get_email = ResetPassword.objects.get(id=get_current_id).email
    return render(request,'change_username.html',{
        'get_current_id':get_current_id,
        'get_email':get_email,
        })

@csrf_exempt
def change_password(request):
    if request.method=='POST':
        result={}
        try:
            mail=request.POST.get('mail')
            password=request.POST.get('password')

            #user = User.objects.get(email=mail,is_superuser=0)
            #user.password = password
            #user.save()
            if User.objects.filter(username=mail).exists():
                User.objects.filter(username=mail,is_superuser=0).update(password=password)
                
            # if User.objects.filter(email=mail,is_superuser=0).exists():
            #   for ut in User.objects.filter(email=mail,is_superuser=0):
            #       ut.password = password

            # user = User.objects.get(password=password,user.username=username)
            # if user is not None:
            #   password_exist = 1
            # else:
            #   password_exist = 0

            result['status']= 'success'
            result['mail']= mail
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def update_password(request):
    if request.method=='POST':
        result={}
        try:
            username=request.POST.get('username')
            password=request.POST.get('password')

            user = User.objects.get(username=username,is_superuser=0)
            user.password = password
            user.save()

            # user = User.objects.get(password=password,user.username=username)
            # if user is not None:
            #   password_exist = 1
            # else:
            #   password_exist = 0

            result['status']= 'success'
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def change_username(request):
    if request.method=='POST':
        result={}
        try:
            mail=request.POST.get('mail')
            username=request.POST.get('username')

            User.objects.filter(email=mail,is_superuser=0).update(username=username)

            user = User.objects.get(email=mail,is_superuser=0)
            user.username = username
            user.save()

            result['status']= 'success'
            result['mail']= mail
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_new_client(request):
    if request.method=='POST':
        result={}
        try:
            email=request.POST.get('mail')
            username=request.POST.get('username')
            raw_password= email.split('@')[0]+'@123'
            role = request.POST.get('role')
            b_id = request.POST.get('b_id')
            partner_id = request.POST.get('partner_id')
            user_exist = 0

            if not ( User.objects.filter(username=username).exists() ):
                #insert data into User model 
                if not User.objects.filter(username=username).exists():
                    User.objects.create_user(username, email, raw_password)
                else:
                    user_exist = 1
                
                atuser = User.objects.get(username=username)
                if not SalatTaxUser.objects.filter(user=atuser).exists():
                    SalatTaxUser.objects.create(
                        user = atuser,
                        email = email,
                    ).save()

                    salattaxUser = SalatTaxUser.objects.get(user=atuser)

                    log.info('SalatTaxRole.objects.get(rolename=role)')
                    role_instance = SalatTaxRole.objects.get(rolename=role)

                    salattaxUser.role = role_instance
                    salattaxUser.save()

                    user = username
                    if b_id != 1:
                        login(request, user)
            else:
                user_exist = 1

            client_id = 0
            atuser = User.objects.get(username=username)
            for c in SalatTaxUser.objects.filter(user=atuser):
                client_id = c.id

            if not ( Personal_Details.objects.filter(P_Id=client_id).exists() ):
                Personal_Details.objects.create(
                    P_Id=client_id,
                    Business_Id=b_id,
                    Partner_Id=partner_id,
                ).save()

            result['status'] = 'success'
            result['raw_password'] = raw_password
            result['client_id'] = client_id
            result['user_exist'] = user_exist
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def user_tracking_func(request):
    result={}
    if request.method=='POST':
        try:
            event=request.POST.get('event')
            event_name=request.POST.get('event_name')
            client_id=request.POST.get('client_id')
            UserId=request.POST.get('UserId')

            if event == 'viewed':
                event_name = request.META.get('HTTP_REFERER', None)

            # session_key = request.session.session_key
            session_key = 'user id-'+str(UserId)
            session_count = 0
            user_id = ''
            device_platform = ''
            if client_id==1 or client_id=='1' or client_id==0 or client_id=='0' :
                # user_id = 'not-logged-in'
                user_id = session_key
            else:
                pan = ''
                name = ''
                if Personal_Details.objects.filter(P_Id = client_id).exists():
                    for client in Personal_Details.objects.filter(P_Id = client_id):
                        if client.pan is None:
                            pan = ''
                        else:
                            pan = client.pan
                        if client.name is None:
                            name = ''
                        else:
                            name = client.name
                user_id = name+'_'+pan

            if(request.user_agent.is_pc ) :
                device_platform = 'Desktop'
            elif(request.user_agent.is_mobile ):
                device_platform = 'Mobile'
            else:
                device_platform = 'Other'

            browser = request.user_agent.browser.family

            time_on_site = "00:00"
            if user_tracking.objects.filter(session_key = 'user id-'+str(UserId)).exists():
                user_tracking_i = user_tracking.objects.filter(session_key = 'user id-'+str(UserId)).order_by('-created_time')[0]
                if user_tracking_i.session_count is None:
                    session_count = 0
                else:
                    time_diff = timezone.now() - user_tracking_i.created_time
                    minute = int(time_diff.seconds / 60)
                    hour = minute / 60
                    # log.info(minute)
                    # log.info(hour)
                    if(minute>=20 and hour >= 0):
                        session_count = int(user_tracking_i.session_count) + 1
                        time_on_site = "00:00"
                    else:
                        # log.info('else')
                        session_count = user_tracking_i.session_count
                        session_start = user_tracking.objects.filter(session_key = 'user id-'+str(UserId),session_count=session_count).order_by('created_time')[0]
                        time_diff = timezone.now() - session_start.created_time 
                        sec = str(time_diff.seconds % 60)
                        if time_diff.seconds % 60 <= 9:
                            sec = '0'+str(time_diff.seconds % 60)
                        minute = str(int(time_diff.seconds / 60) % 60)
                        if int(time_diff.seconds / 60) % 60 <= 9:
                            minute = '0'+ str(int(time_diff.seconds / 60) % 60)
                        time_on_site = minute + ":" + sec # time_diff.seconds / 60

            # if user_tracking.objects.filter(session_key=session_key).exists():
            #   user_tracking_instance = user_tracking.objects.filter(session_key=session_key).order_by('created_time')[0]
            #   time_diff = timezone.now() - user_tracking_instance.created_time 
            #   sec = str(time_diff.seconds % 60)
            #   if time_diff.seconds % 60 <= 9:
            #       sec = '0'+str(time_diff.seconds % 60)
            #   minute = str(int(time_diff.seconds / 60) % 60)
            #   if int(time_diff.seconds / 60) % 60 <= 9:
            #       minute = '0'+ str(int(time_diff.seconds / 60) % 60)
            #   time_on_site = minute + ":" + sec # time_diff.seconds / 60
            # else:
            #   time_on_site = "00:00"
            log.info(event_name)
            if( "/selenium" in event_name):
                pass
            else:
                if time_on_site!='00:00':
                    if not user_tracking.objects.filter(session_key=session_key,session_count=session_count,event=event,event_name=event_name,time_on_site=time_on_site).exists():
                        user_tracking.objects.create(
                            session_key = session_key,
                            session_count = str(session_count),
                            client_id = client_id,
                            user_id = user_id,
                            event = event,
                            event_name = event_name,
                            time_on_site = time_on_site,
                            device_platform = device_platform,
                            browser = browser,
                            device_type = request.user_agent.device.family
                        ).save()
                else:
                    user_tracking.objects.create(
                        session_key = session_key,
                        session_count = str(session_count),
                        client_id = client_id,
                        user_id = user_id,
                        event = event,
                        event_name = event_name,
                        time_on_site = time_on_site,
                        device_platform = device_platform,
                        browser = browser,
                        device_type = request.user_agent.device.family
                    ).save()

            # log.info(request.META.get('HTTP_REFERER', None))
            # url = 'http://ipinfo.io/json'
            # response = urlopen(url)
            # data = json.load(response)

            # send_url = 'http://freegeoip.net/json'
            # r = requests.get(send_url)
            # j = json.loads(r.text)
            # log.info(j)
            # r = requests.get('https://api.ipdata.co?api-key=test').json()
            # r['country_name']
            # log.info(r)

            # send_url = "http://api.ipstack.com"
            # geo_req = requests.get(send_url)
            # geo_json = json.loads(geo_req.text)

            # f = urllib2.urlopen(request.META.get('HTTP_REFERER', None))
            # json_string = f.read()
            # # log.info(json_string)
            # f.close()
            # log.info(request.META.get('HTTP_REFERER', None))
            # location = json.loads(json_string,'utf-8')
            # log.info( location )


            # log.info(request.user_agent.is_mobile)
            # log.info(request.user_agent.is_pc)
            # log.info(request.user_agent.device)
            # log.info(request.user_agent.device.family)

            temp_client = 1
            temp_UserId = 'user id-'+str(UserId)
            if user_tracking.objects.filter(session_key=temp_UserId).exists():
                for ut in user_tracking.objects.filter(session_key=temp_UserId).order_by('-created_time'):
                    if ut.client_id!=1 or ut.client_id!='1' or ut.client_id!=0 or ut.client_id!='0' :
                        temp_client = ut.client_id
                    if temp_client != 1 and temp_client==client_id:
                        for ut in user_tracking.objects.filter(session_key=temp_UserId):
                            if ut.client_id==1 or ut.client_id=='1' or ut.client_id==0 or ut.client_id=='0' :
                                ut.client_id = temp_client
                                ut.save()

            # send_subscribed_mail(request,client_id)

            result['status']= 'success'
            result['event']= event
            result['event_name']= event_name
            result['client_id']= client_id
            result['time_on_site']= time_on_site
            result['UserId'] = UserId
            result['temp_client'] = temp_client
        except Exception as ex:
            result['status']= 'Error : %s' % ex
            log.error('Error in : '+traceback.format_exc())
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def send_subscribed_mail(request,client_id):
    username = request.session.get('login')
    client_id = 0
    email = ''
    if username is None:
        pass
    else:
        if username!='':
            atuser = User.objects.get(username=username)
            for c in SalatTaxUser.objects.filter(user=atuser):
                # client_id = c.id
                email = c.email

    # log.info(username)
    # log.info(client_id)
    log.info(email)

    if(email!=''):
        # get_user_email = email
        sender = 'salattax@gmail.com'
        reciever = email
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Your Salat Tax Savings Account is now active"
        msg['From'] = sender
        msg['To'] = reciever

        html ='<html><title>Salat Tax</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet>';
        html += '<style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style>';
        html += '<body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0>';
        html += '<table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%>';
        html += '<tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top>';
        html += '<table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff>';
        html += '<tr><td style="border-top:10px solid #00A0E3">';
        html += '<table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center>';
        html += '<tr><td style="padding:20px 0">';
        html += '<table border=0 cellpadding=0 cellspacing=0 class=logo style=width:220px align=left>';
        html += '<tr><td><a href="" target=_blank><img alt="Salat Tax" src="https://salattax.com/static/images/logo_blue.png" border=0></a>';
        html += '</table></table>';
        html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="m_-6119505529415927820container" style="width:640px;border-top:thin solid #009ee3" width="640">';
        
        html += '<tbody>';
        html += '<tr>';
        html += '<td class="m_-6119505529415927820header-divider" style="padding-bottom:5px;text-align:center">';
        html += '<p style="text-align:left"><span style="font-size:14px">Hi&nbsp;</span></p>';
        html += '<p style="text-align:left"><span style="font-size:13px"><span style="font-size:14px">Congratulations! Your Salat Tax Savings Account is now active. You have subscribed to a "Assisted Plan".<br><br>';
        html += 'You are now a part of smart group of people who wish to pay minimum taxes through prudent tax planning. So well done!<span style="font-family:verdana">&nbsp;</span></span><img alt="Well done!" src="https://ci4.googleusercontent.com/proxy/aIMjbHZcxrm0PnKT7FOtJmYH-Ml3hPX-jP75WPOF-6Rld8lgycxtQT5T0WyyZ-OyFNLlGNzakFJZNwZcVJCvltqL1Pn4qpkIwGIrgjrTO6Npbcyf1nxSHb1swLGP3_iRvJNf3TGSyge_hg1RHA=s0-d-e1-ft#https://bucket.mlcdn.com/a/440/440943/images/6aca1643d9e782c172fd96798ae4320bcbcd6028.jpeg" style="text-align:center" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 825px; top: 490px;"><div id=":v7" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div></span></p>';
        html += '<p style="text-align:left"><span style="font-size:13px"><span style="font-size:14px">This is how you can make most of your Salat Tax Savings Account</span>.</span></p>';
        html += '<ol>';
        html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><u><span style="font-size:16px">Check out your Tax Analytics Report</span></u></span><br><span style="font-size:14px">Tax Analytics Report contains detailed analysis of your tax leakages for up to last 5 years. You can see for yourself what could have been done better and how you could have saved more in taxes. You will surely love this!</span></span><br>&nbsp;</li>';
        html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><u><span style="font-size:16px">Plan your taxes smartly with Salat Tax Planner</span></u></span><br><span style="font-size:14px">With Salat Tax Planner you can very easily plan you taxes for current financial year. Just fill out the information asked and tailor made tax saving recommendations will be given to you.&nbsp;</span></span><br>&nbsp;</li>';
        html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><span style="font-size:16px"><u>Free Income Tax Return Filing</u></span></span><br><span style="font-size:14px">You are eligible for free IT return filing for FY 2018-19. The return filing portal will&nbsp; be&nbsp;opened in the month of June. We will keep you updated when it is up.</span></span><br>&nbsp;</li>';
        html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><span style="font-size:16px"><u>Earn Rewards</u></span></span><br><span style="font-size:14px">You can make Salat Tax Savings Account free for you. Just share the coupon code <strong>EARN18</strong> with your friends and you shall get a Rs.<strong>200 cashback</strong> when any of your friend opens an account using your code. Your friend shall also get a Rs. 200 discount on the subscription amount.</span></span><br>&nbsp;</li>';
        html += '</ol>';
        html += '<p style="text-align:justify"><span style="font-size:14px">So Happy Tax Saving!</span></p>';
        html += '<p style="text-align:left"><span style="font-size:14px">Team Salat Tax</span></p>';
        html += '</td>';
        html += '</tr>';
        html += '</tbody>';
        
        html += '</table>';
        html += '<br><table align="center" border="0" cellpadding="0" cellspacing="0" class="m_-1230022283182079595container" style="width:700px;border-bottom:10px solid #00a0e3" width="700"><tbody><tr><td style="padding:0px 0 20px 0"><table class="m_-1230022283182079595container" style="width:640px;border-top:thin solid #009ee3"><tbody><tr><td class="m_-1230022283182079595footer-copy" style="font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:10px">If you have any queries regarding the product or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="m_-1230022283182079595email"> </a><a href="http://salatinvestments.com/sendy/unsubscribe-success.php?c=98" style="color:#00a0e3;text-decoration:none;font-size:13px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://salatinvestments.com/sendy/unsubscribe-success.php?c%3D98&amp;source=gmail&amp;ust=1551152760728000&amp;usg=AFQjCNGEMpTGfk9Qxtp-KjGrWx6DEKBRPw"> unsubscribe here</a> </td></tr></tbody></table>';
        html += '</td></tr></tbody></table></table></table></table></html>';
        part2 = MIMEText(html, 'html')

        msg.attach(part2)
        # Send the message via local SMTP server.
        mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
        mail.ehlo()
        mail.starttls()
        mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
        mail.sendmail(sender, reciever, msg.as_string())

        log.info('success')


# | auth_user | CREATE TABLE `auth_user` (
#   `id` int(11) NOT NULL AUTO_INCREMENT,
#   `password` varchar(128) NOT NULL,
#   `last_login` datetime(6) DEFAULT NULL,
#   `is_superuser` tinyint(1) NOT NULL,
#   `username` varchar(150) NOT NULL,
#   `first_name` varchar(30) NOT NULL,
#   `last_name` varchar(30) NOT NULL,
#   `email` varchar(254) NOT NULL,
#   `is_staff` tinyint(1) NOT NULL,
#   `is_active` tinyint(1) NOT NULL,
#   `date_joined` datetime(6) NOT NULL,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `username` (`username`)
# ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 |
# salattaxuser :| id | phone | email    | firstname | lastname | created | updated| role_id | user_id 
# auth_user : id|password|last_login|is_superuser|username|first_name|last_name|email|is_staff|is_active|date_joined

def login_access(request,data):
    try:
        log.info(data)
        request.session['login'] = ''
        log.info(request.session.get('login'))

        request.session['login'] = data

        return redirect('/')
    except Exception as ex:
        log.info('Error in direct login '+str(ex))