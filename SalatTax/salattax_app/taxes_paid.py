from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
# 
import logging
import json

from .models import SalatTaxUser
from .models import Bank_Details,tds_tcs,tax_paid,Personal_Details,Return_Details
#for url request
import requests
from datetime import date, datetime

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

def json_serial(obj):
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))

@csrf_exempt
def get_26AS_tds(request):
	if request.method=='POST':
		result={}
		try:
			client_id=request.POST.get('client_id')
			PAN = ''

			if Personal_Details.objects.filter(P_Id = client_id).exists():
				for client in Personal_Details.objects.filter(P_Id = client_id):
					PAN = client.pan

			R_ID = get_rid(client_id)

			section = []
			part = []
			d_c_name = []
			tan_pan = []
			amt_paid = []
			tds_deposited = []
			total_tds_paid = 0
			return_year = 0
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]

			no_of_tds = tds_tcs.objects.filter(R_Id=R_ID,year=return_year).count()
			result['no_of_tds']= no_of_tds

			for tds in tds_tcs.objects.filter(R_Id = R_ID,year=return_year):
				section.append(tds.section)
				part.append(tds.part)
				d_c_name.append(tds.Deductor_Collector_Name)
				tan_pan.append(tds.TAN_PAN)
				amt_paid.append(tds.amount_paid)
				tds_deposited.append(tds.tds_tcs_deposited)
				total_tds_paid += float(tds.tds_tcs_deposited or 0)

			result['status']= 'success'
			result['section']= section
			result['client_id']= client_id
			result['part']= part
			result['countA']= part.count('A')
			result['countOther']= no_of_tds-part.count('A')

			result['d_c_name']= d_c_name
			result['tan_pan']= tan_pan
			result['amt_paid']= amt_paid
			result['tds_deposited']= tds_deposited
			result['total_tds_paid']= total_tds_paid
			result['return_year'] = return_year
			result['R_ID'] = R_ID
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_26AS_taxes_advTax(request):
	if request.method=='POST':
		result={}
		try:
			r_id=request.POST.get('r_id')
			R_ID = get_rid(r_id)

			no_of_tax = 0

			# no_of_tax = tax_paid.objects.filter(R_Id=R_ID).count()
			# result['no_of_tax']= no_of_tax
			return_year = 0
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]

			count_adv = 0
			bsr_code = []
			d_date = []
			challan_no = []
			tax_p = []
			date = datetime(int(return_year) ,03,31)
			# date = datetime(2018,03,31)
			date = date.replace(tzinfo=None)

			for tax in tax_paid.objects.filter(R_Id = R_ID):
				if date >tax.deposit_date.replace(tzinfo=None) :
					no_of_tax = no_of_tax + 1
					bsr_code.append( tax.BSR_code )
					dep_date = json_serial(tax.deposit_date.replace(tzinfo=None))
					d_date.append( dep_date.split('T')[0] )
					challan_no.append( tax.challan_serial_no)
					tax_p.append( tax.total_tax)
					count_adv = count_adv+1

			result['no_of_tax']= no_of_tax
			result['status']= 'success'
			result['bsr_code']= bsr_code
			result['d_date']= d_date
			result['challan_no']= challan_no
			result['tax_p']= tax_p
			result['count_adv']= count_adv
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_26AS_taxes_self_a(request):
	if request.method=='POST':
		result={}
		try:
			r_id=request.POST.get('r_id')
			R_ID = get_rid(r_id)

			no_of_tax = 0

			# result['no_of_tax']= tax_paid.objects.filter(R_Id=R_ID).count()
			return_year = 0
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]

			count_adv = 0
			bsr_code = []
			d_date = []
			challan_no = []
			tax_p = []
			date = datetime(int(return_year) ,03,31)
			# date = datetime(2018,03,31)
			date = date.replace(tzinfo=None)

			for tax in tax_paid.objects.filter(R_Id = R_ID):
				if date <tax.deposit_date.replace(tzinfo=None) :
					no_of_tax = no_of_tax + no_of_tax
					bsr_code.append( tax.BSR_code )
					dep_date = json_serial(tax.deposit_date.replace(tzinfo=None))
					d_date.append( dep_date.split('T')[0] )
					challan_no.append( tax.challan_serial_no)
					tax_p.append( tax.total_tax)
					count_adv = count_adv+1

			result['status']= 'success'
			result['no_of_tax'] = no_of_tax
			result['bsr_code']= bsr_code
			result['d_date']= d_date
			result['challan_no']= challan_no
			result['tax_p']= tax_p
			result['count_adv']= count_adv
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_26AS_taxes(request):
	if request.method=='POST':
		result={}
		try:
			r_id=request.POST.get('r_id')
			R_ID = get_rid(r_id)
			no_of_tax = 0
			# result['no_of_tax']= tax_paid.objects.filter(R_Id=R_ID).count()
			return_year = 0
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
				return_year = return_year.split('-')[1]


			total_adv_tax_paid = 0
			bsr_code = []
			d_date = []
			challan_no = []
			tax_p = []
			date = datetime(int(return_year) ,03,31)
			# date = datetime(2018,03,31)
			date = date.replace(tzinfo=None)

			for tax in tax_paid.objects.filter(R_Id = R_ID):
				no_of_tax = no_of_tax + 1
				bsr_code.append( tax.BSR_code )
				dep_date = json_serial(tax.deposit_date.replace(tzinfo=None))
				d_date.append( dep_date.split('T')[0] )
				challan_no.append( tax.challan_serial_no)
				tax_p.append( tax.total_tax)
				total_adv_tax_paid += float(tax.total_tax or 0)

			result['status']= 'success'
			result['no_of_tax'] = no_of_tax
			result['bsr_code']= bsr_code
			result['d_date']= d_date
			result['challan_no']= challan_no
			result['tax_p']= tax_p
			result['total_adv_tax_paid']= total_adv_tax_paid

		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_id(request,id):
	if SalatTaxUser.objects.filter(id=id).exists():
		salattaxid = SalatTaxUser.objects.filter(id=id).first()
	else:
		salattaxid = None

	return render(request,'taxes_paid.html',
		{
		'id':salattaxid,
		'client_id':id
		})

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance,FY='2018-2019').exists():
		Return_Details_instance=Return_Details.objects.get(P_id=personal_instance,FY='2018-2019')
		R_ID = Return_Details_instance.R_id
	else:
		log.info('R_ID Entry not exists')
		# for rd in Return_Details.objects.filter(P_id=personal_instance):
		# 	log.info(rd.R_id)

	return R_ID

