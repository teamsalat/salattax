from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

# from .models import index, Personal_Details, Address_Details
import base64
import os, sys
from PIL import Image
# Python logging package
from .models import SalatTaxUser
from .models import Client_fin_info1,Client_fin_info,Return_Details,Personal_Details
import logging
import json
import subprocess
from subprocess import Popen, PIPE, STDOUT
import os, re
import glob
import MySQLdb
import datetime
from .path import *
import traceback

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

@csrf_exempt
def run_jar(request):
    if request.method=='POST':
        result={}

        try:
            data=request.POST.get('data')
            file_name=request.POST.get('path')
            client_id=request.POST.get('client_id')
            file_count=request.POST.get('file_count')
            passwordA=str(request.POST.get('passwordA') )
            passwordB=str(request.POST.get('passwordB') )
            company=request.POST.get('company')

            MYDIR = os.path.dirname(__file__)
            log.info(MYDIR)
            # path = '/home/salatTax/SalatTax/static/form16/'
            path = selenium_path+'/form16/'

            section_list = []
            # for name in glob.glob(MYDIR+'/static/form16/form16*['+company+'].pdf'):
            for name in glob.glob(path+client_id+'_form16*['+company+'].pdf'):
                # pattern : form16(something)(company).pdf
                section_list.append(name) 
            # result['status1']=section_list
            file1 = ''
            file2 = ''

            if (file_count == '1'):
                # file1 = MYDIR+'/static/form16/'+file_name
                file1 = file_name+'.pdf'
                file2 = ''
            else:
                # file1 = 'file:/'+section_list[0]
                file1 = section_list[0].replace(path, '')
                file2 = section_list[1].replace(path, '')

            # file1 & file2 path is https://salattax.com/mail/tax/ (variable)

            jar_path = MYDIR+'/form16/form16reader.jar'
            # jar_path = MYDIR+'/form16/test1.jar'
            # jar_path = MYDIR+'/form16/test1_path.jar'
            # https://salattax.com/static/form16/form16_Part1_2.pdf

            p = Popen(['java', '-jar',jar_path,file1,passwordA,file2,passwordB,client_id], stdout=PIPE, stderr=STDOUT)
            # p = Popen(['java', '-jar',jar_path,'atemp/Tata_partA.pdf',passwordA,'atemp/Tata_partB.pdf',passwordB,client_id], stdout=PIPE, stderr=STDOUT)
            # p = Popen(['java', '-jar',jar_path,'atemp/RISHI_PARTA.pdf',passwordA,'atemp/RISHI_PARTB.pdf',passwordB,client_id], stdout=PIPE, stderr=STDOUT)
            # p = Popen(['java', '-jar',jar_path,'atemp/EY_Vipul_BSHPK5958D.pdf',passwordA,'',passwordB,client_id], stdout=PIPE, stderr=STDOUT)
            full_output = ''
            for line in p.stdout:
                # output = output+line
                output = line
                full_output = full_output+line
            # output = os.getcwd()
            result['output'] = output
            result['full_output'] = full_output
            # result['file_count'] = file_count
            result['file1'] = file1
            result['file2'] = file2
            result['status'] = "success"
        except Exception as ex:
            log.error('Error in run_jar Form16: '+request.get_host()+': '+traceback.format_exc())
            result['status']= 'Error : %s' % ex
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def get_DB_data(request):
    if request.method=='POST':
        result={}
        try:
            path=request.POST.get('path')
            client_id=request.POST.get('client_id')
            selected_fy=request.POST.get('selected_fy')
            result['client_id'] = client_id
            # conn = MySQLdb.connect (user='root', password='salat',host='localhost')
            conn = MySQLdb.connect ("localhost","root","salat","salattax")
            # sql = 'SELECT * FROM client_fin_info1 where client_id = 0'
            sql = 'SELECT * FROM client_fin_info1 where client_id = '+str(client_id)+' and flag is null order by time_stamp desc'
            cursor = conn.cursor()
            # databases = ("show databases")
            # cursor.execute("SELECT VERSION()")
            sql_latest = 'SELECT * FROM client_fin_info1 where client_id = '+str(client_id)+' and flag is null order by time_stamp desc limit 1'
            cursor_latest = conn.cursor()
            cursor_latest.execute(sql_latest)
            result['rowcount_latest'] = cursor_latest.rowcount

            db_ay = ''
            read_ay = ''
            if cursor_latest.rowcount == 1:
                read_ay = ''
                for row1 in cursor_latest:
                    read_ay = row1[5]
                    if len(read_ay)==7:
                        read_ay=read_ay
                        read_ay = read_ay[:4] + '-20' + read_ay[5:]
            result['read_ay'] = read_ay
            result['selected_fy'] = selected_fy

            log.info(read_ay)

            cursor.execute(sql)
            result['rowcount'] = cursor.rowcount
            data = cursor.fetchone()
            date_time = []
            pan_list = []

            results = cursor.fetchall()
            for row in cursor:
                if not Client_fin_info1.objects.filter(company_address=row[1],employee_pan=row[4],period_from=row[6],client_id=row[96],flag=0).exists():
                    company_name=row[0]
                    Client_fin_info1.objects.create(
                        company_name = row[0],
                        company_address = row[1],
                        employee_name = row[2],
                        employee_address = row[3],
                        employee_pan = row[4],
                        ay = row[5],
                        period_from = row[6],
                        period_to = row[7],
                        amount_paid_credited = row[8],
                        amount_of_tax_deducted = row[9],
                        company_pan = row[10],
                        company_tan = row[11],
                        salary_as_per_provision = row[12],
                        value_of_perquisites = row[13],
                        profits_in_lieu_of_salary = row[14],
                        total_gross_salary = row[15],
                        gross_salary_netof_allowance = row[16],
                        nsc = row[17],
                        hra_allowed = row[18],
                        medical_allowance = row[19],
                        conveyance_allowance_allowed = row[20],
                        lta = row[21],
                        lse = row[22],
                        hra_claimed = row[23],
                        conveyance_allowance_claimed = row[24],
                        income_house_property = row[25],
                        home_loan_interest = row[26],
                        income_capital_gains = row[27],
                        income_other_sources = row[28],
                        gratuity = row[29],
                        elss = row[30],
                        epf = row[31],
                        ppf = row[32],
                        vpf = row[33],
                        lic = row[34],
                        nsc_interest = row[35],
                        superannuation = row[36],
                        home_loan_principal = row[37],
                        fd = row[38],
                        ulip = row[39],
                        child_edu_fee = row[40],
                        sukanya_samriddhi_scheme = row[41],
                        stamp_duty = row[42],
                        number_80ccc = row[43],
                        nps_employee = row[44],
                        nps_employer = row[45],
                        number_80ccd = row[46],
                        number_80ccf = row[47],
                        number_80d_self_family = row[48],
                        number_80d_parents = row[49],
                        medical_insurance = row[50],
                        number_80e = row[51],
                        number_80g = row[52],
                        number_80tta = row[53],
                        entertainment_allowance = row[54],
                        profession_tax = row[55],
                        via_deductions = row[56],
                        basic_salary = row[57],
                        dearness_allowance = row[58],
                        personal_allowance = row[59],
                        bonus = row[60],
                        performance_pay = row[61],
                        special_allowance = row[62],
                        food_allowance = row[63],
                        extra_salary = row[64],
                        rgess = row[65],
                        exemption_less_allowance = row[66],
                        total_less_allowance = row[67],
                        attire = row[68],
                        professional_pursuit = row[69],
                        telephone_allowance = row[70],
                        other_allowance = row[71],
                        other_any_other_income = row[72],
                        cid = row[73],
                        emp_type = row[74],
                        email = row[75],
                        form16_path = row[76],
                        password = row[77],
                        p_d = row[78],
                        s_c = row[79],
                        c = row[80],
                        r = row[81],
                        self_house = row[82],
                        hra_given = row[83],
                        basic = row[84],
                        da = row[85],
                        rent = row[86],
                        hra_given_value = row[87],
                        stay = row[88],
                        home_loan = row[89],
                        interest_paid = row[90],
                        rent_earn = row[91],
                        disability = row[92],
                        disability_type = row[93],
                        time_stamp = row[94],
                        salat_tax = row[95],
                        tax_leakage = row[96],
                        flag = 0,
                        client_id = client_id,
                    ).save()
                    log.info('Client_fin_info1 entry created for : '+str(row[4]))
                else:
                    log.info('Client_fin_info1 entry already exists for : '+str(row[4])+' '+str(row[1]))
                
                date_time.append( row[94].strftime("%Y-%m-%d %H:%M:%S"))
                pan_list.append( row[4])

                # result['query']="UPDATE client_fin_info1 SET flag=1 WHERE employee_pan='"+row[4]+"' and client_id="+client_id+" and flag is null "

                cursor.execute("UPDATE client_fin_info1 SET flag=1 WHERE period_from='"+row[6]+"'and period_to='"+row[7]+"' and employee_pan='"+row[4]+"' and client_id="+client_id+" and flag is null")
                conn.commit()
            # result['DB_list'] = cursor.execute("SHOW DATABASES")
            # result['date_time'] = date_time
            result['pan_list'] = pan_list

            personal_instance=Personal_Details.objects.get(P_Id=client_id)
            if Return_Details.objects.filter(P_id=personal_instance,FY=selected_fy).exists():
                Return_Details_instance = Return_Details.objects.filter(P_id=personal_instance,FY=selected_fy).order_by('-updated_time')[0]
                # Return_Details_instance=Return_Details.objects.get(P_id=personal_instance)
                db_ay = Return_Details_instance.AY
            result['db_ay'] = db_ay


            year_match='N'
            if db_ay!=None and db_ay!='' and read_ay!=None and read_ay!='':
                if db_ay==read_ay:
                    year_match='Y'

            if year_match=='N':
                if Client_fin_info1.objects.filter(client_id=client_id,company_name=company_name,flag=0).exists():
                    Client_fin_Obj=Client_fin_info1.objects.get(client_id=client_id,company_name=company_name,flag=0)
                    Client_fin_Obj.flag=2
                    Client_fin_Obj.save()

            result['year_match'] = year_match
            result['status'] = 'success'

        except Exception as ex:
            log.error('Error in get_DB_data Form16: '+request.get_host()+': '+traceback.format_exc())
            result['status']= 'Error : %s' % ex
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def client_fin_info(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            result['client_id'] = client_id
            now = datetime.datetime.now()

            cn , ca, emp_name, emp_add, PAN, CompanyPan, CompanyTan, ay, pf, pto, form16_path, emp_type = ('',)*10
            amt_t_d, amt_p_c, sapp, vop, pls, tgs, gsna, nsc, hra_a, m_a, caa, lta1, lse, hra_c = (0,)*14
            cac, ihp, hli, icg, ios, Gratuity, elss, epf, ppf, vpf, lic, nsc_i, s, hlp, fd, ulip = (0,)*16
            cef, sss, sd, ccc, nps_e1, nps_e2, ccd, ccf, dsf, dp, mins, e, g, tta, ea, pt, via, bs = (0,)*18
            da, pall, bonus, pp, sa, f, es, RGESS, Email, P_D1, S_C1, C1, R1 = (0,)*13
            self_house1, HRA_given1, basic1, DA1, rent1, HRA_given_value1, stay1, home_loan1 = (0,)*8
            interest_paid1, disability1, disability_type1 = (0,)*3
            ela, tla, attire, pursuit, teleA, other_a, any_other_a = (0,)*7

            data = []
            for form16 in Client_fin_info1.objects.filter(client_id = client_id,flag=0):
                # data.append(form16.company_name)
                cn = form16.company_name+', '+cn
                cn = re.sub("'","", cn)
                ca = form16.company_address+', '+ca
                ca = re.sub("'","", ca)
                ca = re.sub('[^a-zA-Z0-9-_*." "]', '', ca)
                emp_name = form16.employee_name
                emp_add = form16.employee_address+', '+emp_add
                PAN = form16.employee_pan
                ay = form16.ay+', '+ay
                pf = form16.period_from+', '+pf
                pto = form16.period_to+', '+pto
                amt_p_c = form16.amount_paid_credited+amt_p_c
                amt_t_d = form16.amount_of_tax_deducted+amt_t_d
                CompanyPan=form16.company_pan+', '+company_pan
                CompanyTan=form16.company_tan+', '+company_tan
                sapp = form16.salary_as_per_provision+sapp
                vop = form16.value_of_perquisites+vop
                pls = form16.profits_in_lieu_of_salary+pls
                tgs = form16.total_gross_salary+tgs
                gsna = form16.gross_salary_netof_allowance+gsna
                nsc = (form16.nsc or 0) + nsc
                hra_a = (form16.hra_allowed or 0) +hra_a
                m_a = (form16.medical_allowance or 0) +m_a
                caa = (form16.conveyance_allowance_allowed or 0) +caa
                lta1 = (form16.lta or 0) +lta1
                lse = (form16.lse or 0) +lse
                hra_c = (form16.hra_claimed or 0) +hra_c
                cac = (form16.conveyance_allowance_claimed or 0) +cac
                ihp = (form16.income_house_property or 0) +ihp
                hli = (form16.home_loan_interest or 0) +hli
                icg = (form16.income_capital_gains or 0) +icg
                ios = (form16.income_other_sources or 0) +ios
                Gratuity = (form16.gratuity or 0) +Gratuity
                elss = (form16.elss or 0) +elss
                epf = (form16.epf or 0) +epf
                ppf = (form16.ppf or 0) +ppf
                vpf = (form16.vpf or 0) +vpf
                lic = (form16.lic or 0) +lic
                nsc_i = (form16.nsc_interest or 0) +nsc_i
                s = (form16.superannuation or 0) +s
                hlp = (form16.home_loan_principal or 0) +hlp
                fd = (form16.fd or 0) +fd
                ulip = (form16.ulip or 0) +ulip
                cef = (form16.child_edu_fee or 0) +cef
                sss = (form16.sukanya_samriddhi_scheme or 0) +sss
                sd = (form16.stamp_duty or 0) +sd
                ccc = (form16.number_80ccc or 0) +ccc
                nps_e1 = (form16.nps_employee or 0) +nps_e1
                nps_e2 = (form16.nps_employer or 0) +nps_e2
                ccd = (form16.number_80ccd or 0) +ccd
                ccf = (form16.number_80ccf or 0) +ccf
                dsf = (form16.number_80d_self_family or 0) +dsf
                dp = (form16.number_80d_parents or 0) +dp
                mins = (form16.medical_insurance or 0) +mins
                e = (form16.number_80e or 0) +e
                g = (form16.number_80g or 0) +g
                tta = (form16.number_80tta or 0) +tta
                ea = (form16.entertainment_allowance or 0) +ea
                pt = (form16.profession_tax or 0) +pt
                via = (form16.via_deductions or 0) +via
                bs = (form16.basic_salary or 0) +bs
                da = (form16.dearness_allowance or 0) +da
                pall = (form16.personal_allowance or 0) +pall
                bonus = (form16.bonus or 0) +bonus
                pp = (form16.performance_pay or 0) +pp
                sa = (form16.special_allowance or 0) +sa
                f = (form16.food_allowance or 0) +f
                es = (form16.extra_salary or 0) +es
                RGESS = (form16.rgess or 0) +RGESS
                ela = (form16.exemption_less_allowance or 0) +ela
                tla = (form16.total_less_allowance or 0) +tla
                attire = (form16.attire or 0) +attire
                pursuit = (form16.professional_pursuit or 0) +pursuit
                teleA = (form16.telephone_allowance or 0) +teleA
                other_a = (form16.other_allowance or 0) +other_a
                any_other_a = (form16.other_any_other_income or 0) +any_other_a
                emp_type = emp_type
                Email = form16.email
                form16_path = (form16.form16_path or '')+ ', ' +form16_path
                P_D1 = form16.p_d 
                S_C1 = form16.s_c
                C1 = form16.c
                R1 = form16.r
                self_house1 = (form16.self_house or 0) +self_house1
                HRA_given1 = (form16.hra_given or 0) +HRA_given1
                basic1 = (form16.basic or 0) +basic1
                DA1 = (form16.da or 0) +DA1
                rent1 = (form16.rent or 0) +rent1
                HRA_given_value1 = (form16.hra_given_value or 0) +HRA_given_value1
                stay1 = (form16.stay or 0) +stay1
                home_loan1 = (form16.home_loan or 0) +home_loan1


                # C_fin_info_instance = Client_fin_info1.objects.get(client_id=client_id,employee_pan=form16.employee_pan,flag=0)
                # C_fin_info_instance.flag=1
                # C_fin_info_instance.save()

            # result['data'] = ca

            if not Client_fin_info.objects.filter(employee_pan = PAN,cid = client_id,flag=0).exists():
                Client_fin_info.objects.create(
                    company_name = cn,
                    company_address = ca,
                    employee_name = emp_name,
                    employee_address = emp_add,
                    employee_pan = PAN,
                    ay = ay,
                    period_from = pf,
                    period_to = pto,
                    amount_paid_credited = amt_p_c,
                    amount_of_tax_deducted = amt_t_d,
                    company_pan=CompanyPan,
                    company_tan=CompanyTan,
                    salary_as_per_provision = sapp,
                    value_of_perquisites = vop,
                    profits_in_lieu_of_salary = pls,
                    total_gross_salary = tgs,
                    gross_salary_netof_allowance = gsna,
                    nsc = nsc,
                    hra_allowed = hra_a,
                    medical_allowance = m_a,
                    conveyance_allowance_allowed = caa,
                    lta = lta1,
                    lse = lse,
                    hra_claimed = hra_c,
                    conveyance_allowance_claimed = cac,
                    income_house_property = ihp,
                    home_loan_interest = hli,
                    income_capital_gains = icg,
                    income_other_sources = ios,
                    gratuity = Gratuity,
                    elss = elss,
                    epf = epf,
                    ppf = ppf,
                    vpf = vpf,
                    lic = lic,
                    nsc_interest = nsc_i,
                    superannuation = s,
                    home_loan_principal = hlp,
                    fd = fd,
                    ulip = ulip,
                    child_edu_fee = cef,
                    sukanya_samriddhi_scheme = sss,
                    stamp_duty = sd,
                    number_80ccc = ccc,
                    nps_employee = nps_e1,
                    nps_employer = nps_e2,
                    number_80ccd = ccd,
                    number_80ccf = ccf,
                    number_80d_self_family = dsf,
                    number_80d_parents = dp,
                    medical_insurance = mins,
                    number_80e = e,
                    number_80g = g,
                    number_80tta = tta,
                    entertainment_allowance = ea,
                    profession_tax = pt,
                    via_deductions = via,
                    basic_salary = bs,
                    dearness_allowance = da,
                    personal_allowance = pall,
                    bonus = bonus,
                    performance_pay = pp,
                    special_allowance = sa,
                    food_allowance = f,
                    extra_salary = es,
                    rgess = RGESS,
                    exemption_less_allowance = ela,
                    total_less_allowance = tla,
                    attire = attire,
                    professional_pursuit = pursuit,
                    telephone_allowance = teleA,
                    other_allowance = other_a,
                    other_any_other_income = any_other_a,
                    cid = client_id,
                    emp_type = emp_type,
                    email = Email,
                    form16_path = form16_path,
                    p_d = P_D1,
                    s_c = S_C1,
                    c = C1,
                    r = R1,
                    self_house = self_house1,
                    hra_given = HRA_given1,
                    basic = basic1,
                    da = DA1,
                    rent = rent1,
                    hra_given_value = HRA_given_value1,
                    stay = stay1,
                    home_loan = home_loan1,
                    interest_paid = interest_paid1,
                    time_stamp = now.strftime("%Y-%m-%d %H:%M"),
                    flag = 0
                ).save()

            result['status'] = 'success'
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_id(request,id):
    # log.info(id)
    if SalatTaxUser.objects.filter(id=id).exists():
        salattaxid = SalatTaxUser.objects.filter(id=id).first()
    else:
        salattaxid = None

    return render(request,'upload_form16.html',
        {
        'id':salattaxid,
        })


# note :
# -- 1 - missing PART A
# -- 2 - missing PART B
# -- 0 - success

