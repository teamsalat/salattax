#!/usr/bin/python
from __future__ import print_function
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from .models import index, Personal_Details, Address_Details
import os, sys
# Python logging package
import logging
import json

stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

from selenium import webdriver
# from selenium.webdriver.common.keys import Keys


@csrf_exempt
def run_selenium(request):

	if request.method=='POST':
		log.info('Start Running Selenium file testing')
		result={}

		try:
			# log.info(os.getcwd())
			# driver = webdriver.Chrome(executable_path="/home/salattax/SalatTax/salattax_app/static/exe/chromedriver")
			# driver = webdriver.Firefox(executable_path="/home/salattax/SalatTax/salattax_app/static/exe/geckodriver")
			driver = webdriver.Firefox('/home/salat/Downloads/geckodriver.exe')
			# driver.get('http://www.ubuntu.com/')
			log.info('START')
			# driver.maximize_window()
			# driver.get("https://docs.djangoproject.com/")
			# search_box = driver.find_element_by_name("q")
			# search_box.send_keys("testing")
			# search_box.send_keys(Keys.RETURN)
			# assert "Search" in driver.title
			# # Locate first result in page using css selectors.
			# result = driver.find_element_by_css_selector("div#search-results a")
			# result.click()
			# log.info(result)
			# assert "testing" in driver.title.lower()
			driver.quit()
		except Exception as ex:
			log.error('Error in json data: %s' % ex)

			# return HttpResponse('kkkkk')
        status=True
        result['status1']=status

        if status==True:
            result['status']="success"
            return HttpResponse(json.dumps(result), content_type='application/json')

        else:
        	result['status']="Error"
        	return HttpResponse(json.dumps(result), content_type='application/json')

