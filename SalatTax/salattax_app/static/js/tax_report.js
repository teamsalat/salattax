
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('tax_report_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {

	close_model = 0;
	client_id = 0;
	pan = '';
	p_age = '';
	i_year = '';
	$scope.total_salat_tax= "";
	$scope.div_2014 = true;
	$scope.div_2015 = true;
	$scope.div_2016 = true;
	$scope.div_2017 = true;
	$scope.div_2018 = true;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					$.ajax({
						url:'/get_pan_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							client_id = response.client_id;
							dob = response.dob;
							pan = response.pan;
							p_age = response.p_age;
							i_year = response.i_year;
							financial_goal = response.financial_goal;
							if(financial_goal == 'foreign')
								$scope.financial_goal = 'your next Foreign Trip';
							else if(financial_goal == 'shopping')
								$scope.financial_goal = 'shopping';
							else if(financial_goal == 'phone')
								$scope.financial_goal = 'your new phone';
							else if(financial_goal == 'donation')
								$scope.financial_goal = 'donation';
							else if(financial_goal == 'jewellery')
								$scope.financial_goal = 'jewellery';
							else if(financial_goal == 'gift')
								$scope.financial_goal = 'gift for loved one';
							else if(financial_goal == 'apparels')
								$scope.financial_goal = 'apparels';
							else if(financial_goal == 'loan')
								$scope.financial_goal = 'pay of Loan';
							else if(financial_goal == 'medical')
								$scope.financial_goal = 'Medical treatment';
							else if(financial_goal == 'venture')
								$scope.financial_goal = 'starting a new venture';
							else if(financial_goal == 'car')
								$scope.financial_goal = 'your car';
							else if(financial_goal == 'bike')
								$scope.financial_goal = 'your bike';
							$scope.get_26as(pan,dob);
							$scope.cal_tax(pan,p_age,i_year);
						}
					});
				}
			}
		});
	},1000);

	$scope.cal_tax = function(pan,p_age,i_year){
		var pan_status = false;
		// var invested_share = $scope.user.invested_share;
		var invested_share = 'no';
		// var shares_year = $scope.user.shares_year;
		var shares_year = i_year;
		var age_status = false;
		var shares_status = false;
		var total_salat_tax = 0;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";
		
		if(p_age==null || p_age=="undefined" )
			$scope.age_err= "Parent's Age is required";
		else if(p_age ==''){
			$scope.age_err= "";
			age_status = true;
		}else{
			$scope.age_err= "";
			age_status = true;
		}

		if(i_year==''){
			invested_share = 'no';
			shares_status = true;
		}
		else{
			invested_share = 'yes';
			shares_status = true;
		}

		console.log(pan_status);
		console.log(age_status);
		console.log(shares_status);
		
		if( pan_status == true && age_status == true && shares_status==true){
			$.ajax({
				url:'/calculate_salat_tax/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'p_age':p_age,'invested_share':invested_share,'shares_year':shares_year},
				success: function(response){
					console.log(response);
					if(response.count_salattax == 0)
						window.location.href = "/tax_saving_account";
					var s_obj = response.salat_tax ;
					var val_i_14 = 0;
					var val_i_15 = 0;
					var val_i_16 = 0;
					var val_i_17 = 0;
					var val_i_18 = 0;
					var val_tp_14 = 0;
					var val_tp_15 = 0;
					var val_tp_16 = 0;
					var val_tp_17 = 0;
					var val_tp_18 = 0;
					var val_st_14 = 0;
					var val_st_15 = 0;
					var val_st_16 = 0;
					var val_st_17 = 0;
					var val_st_18 = 0;
					var val_tl_14 = 0;
					var val_tl_15 = 0;
					var val_tl_16 = 0;
					var val_tl_17 = 0;
					var val_tl_18 = 0;
					var tax_paid_total = 0;
					var salat_tax_total = 0;
					var tax_leakage_total = 0;
					var data_2014 = 0;
					var data_2015 = 0;
					var data_2016 = 0;
					var data_2017 = 0;
					var data_2018 = 0;
					var total_year = 0;

					for(var i = 0; i < response.count_salattax; i++) {
						total_year = total_year + 1;
						var total_income_chart = $scope.money_format(s_obj[i].total_income_chart);
						var tax_paid = $scope.money_format(s_obj[i].tax_paid);
						var ToTax = $scope.money_format(s_obj[i].ToTax);
						var tax_leakage = $scope.money_format(s_obj[i].tax_leakage*-1);
						// console.log(s_obj[i].total_income_chart);

						tax_paid_total = tax_paid_total + s_obj[i].tax_paid;
						salat_tax_total = salat_tax_total + s_obj[i].ToTax;
						tax_leakage_total = tax_leakage_total + (s_obj[i].tax_leakage*-1);

						if(s_obj[i].year=='2014'){
							$scope.salary_earned_14 = total_income_chart;
							$scope.tax_paid_14 = tax_paid;
							$scope.salat_tax_14 = ToTax;
							$scope.tax_leakage_14 = tax_leakage;
							val_i_14 = s_obj[i].total_income_chart;
							val_tp_14 = s_obj[i].tax_paid;
							val_st_14 = s_obj[i].ToTax;
							val_tl_14 = s_obj[i].tax_leakage*-1;
							data_2014 = 1;
						}
						if(s_obj[i].year=='2015'){
							$scope.salary_earned_15 = total_income_chart;
							$scope.tax_paid_15 = tax_paid;
							$scope.salat_tax_15 = ToTax;
							$scope.tax_leakage_15 = tax_leakage;
							val_i_15 = s_obj[i].total_income_chart;
							val_tp_15 = s_obj[i].tax_paid;
							val_st_15 = s_obj[i].ToTax;
							val_tl_15 = s_obj[i].tax_leakage*-1;
							data_2015 = 1;
						}
						if(s_obj[i].year=='2016'){
							$scope.salary_earned_16 = total_income_chart;
							$scope.tax_paid_16 = tax_paid;
							$scope.salat_tax_16 = ToTax;
							$scope.tax_leakage_16 = tax_leakage;
							val_i_16 = s_obj[i].total_income_chart;
							val_tp_16 = s_obj[i].tax_paid;
							val_st_16 = s_obj[i].ToTax;
							val_tl_16 = s_obj[i].tax_leakage*-1;
							data_2016 = 1;
						}
						if(s_obj[i].year=='2017'){
							$scope.salary_earned_17 = total_income_chart;
							$scope.tax_paid_17 = tax_paid;
							$scope.salat_tax_17 = ToTax;
							$scope.tax_leakage_17 = tax_leakage;
							val_i_17 = s_obj[i].total_income_chart;
							val_tp_17 = s_obj[i].tax_paid;
							val_st_17 = s_obj[i].ToTax;
							val_tl_17 = s_obj[i].tax_leakage*-1;
							data_2017 = 1;
						}
						if(s_obj[i].year=='2018'){
							$scope.salary_earned_18 = total_income_chart;
							$scope.tax_paid_18 = tax_paid;
							$scope.salat_tax_18 = ToTax;
							$scope.tax_leakage_18 = tax_leakage;
							val_i_18 = s_obj[i].total_income_chart;
							val_tp_18 = s_obj[i].tax_paid;
							val_st_18 = s_obj[i].ToTax;
							val_tl_18 = s_obj[i].tax_leakage*-1;
							data_2018 = 1;
						}
					}

					$scope.total_year = total_year;
					$scope.tax_paid_total = $scope.money_format(tax_paid_total);
					$scope.tax_paid_total_text = $scope.nFormatter( tax_paid_total,0 );
					$scope.salat_tax_total = $scope.money_format(salat_tax_total);
					$scope.tax_leakage_total = $scope.money_format(tax_leakage_total);
					$scope.tax_leakage_total_text= $scope.nFormatter(tax_leakage_total,0);
					
					show_chart_salary(val_i_14,val_i_15,val_i_16,val_i_17,val_i_18,data_2014,data_2015,
						data_2016,data_2017,data_2018);
					show_chart_tax_paid(val_tp_14,val_tp_15,val_tp_16,val_tp_17,val_tp_18,data_2014,
						data_2015,data_2016,data_2017,data_2018);
					show_chart_salat_tax(val_tp_14,val_tp_15,val_tp_16,val_tp_17,val_tp_18,val_st_14,
						val_st_15,val_st_16,val_st_17,val_st_18,data_2014,data_2015,data_2016,data_2017,
						data_2018);
					show_chart_tax_leakage(val_tp_14,val_tp_15,val_tp_16,val_tp_17,val_tp_18,val_st_14,
						val_st_15,val_st_16,val_st_17,val_st_18,val_tl_14,val_tl_15,val_tl_16,val_tl_17,
						val_tl_18,data_2014,data_2015,data_2016,data_2017,data_2018);

					if(data_2014 == 0){
						$scope.div_2014 = false;
					}
					if(data_2015 == 0){
						$scope.div_2015 = false;
					}
					if(data_2016 == 0){
						$scope.div_2016 = false;
					}
					if(data_2017 == 0){
						$scope.div_2017 = false;
					}
					if(data_2018 == 0){
						$scope.div_2018 = false;
					}
					$scope.$apply();
				}
			});
		}
	}
	
	$scope.get_26as = function(pan,dob){
		// var pan = $scope.user.pan;
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			var pass_obj = { "pan":pan};
			pass_obj["dob"] = dob;
			var data_pass = JSON.stringify(pass_obj);
			console.log('get_form_26AS Ajax Started');
			$.ajax({
				url:'/get_form_26AS/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){
					console.log(response);
					if (response.status == 'success') {
						$.ajax({
							url:'/unzip_txt/',
							type:'POST',
							dataType: 'json',
							data:{'data':data_pass},
							success: function(response){
								// console.log(response);
								status = JSON.stringify(response['status']).replace(/\"/g, "");
								if (status == 'success') {
									$.ajax({
										url:'/excelToDB/',
										type:'POST',
										dataType: 'json',
										data:{'data':data_pass,'client_id':'1'},
										success: function(response){
											console.log(response);
										}
									});
								}
							}
						});
					}
				},
				error: function(response){
					$scope.get_26as(pan,dob);
				}
			});
		}
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(!isNaN(x)){
			// x = x.toString();
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.nFormatter = function(num, digits) {
		var si = [
			{ value: 1, symbol: "" },
			{ value: 1E3, symbol: " k" },
			{ value: 1E5, symbol: " lacs" },
			{ value: 1E7, symbol: " M" },
			{ value: 1E9, symbol: "G" },
			{ value: 1E12, symbol: "T" },
			{ value: 1E15, symbol: "P" },
		];
		var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
		var i;
		for (i = si.length - 1; i > 0; i--) {
			if (num >= si[i].value) {
				break;
			}
		}
		if(si[i].symbol=='' || si[i].symbol == ' k')
			return (num / si[i].value).toFixed(0).replace(rx, "$1") + si[i].symbol;
		else
			return (num / si[i].value).toFixed(1).replace(rx, "$1") + si[i].symbol;
	}

}]);