var app = angular.module('myapp', ['naif.base64']);


app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.constant('temp', {
    'BASE_URL' : 'http://ec2-13-232-182-180.ap-south-1.compute.amazonaws.com/',
    'TEMP' : 'TEMP',
})