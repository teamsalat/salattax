
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

/*date / add */
app.directive('myDate', dateInput);
function dateInput($filter, $parse){
	return {
		restrict: 'A',
		require: 'ngModel',
		replace: true,
		transclude: true,
		template: '<input ng-transclude ui-mask="39/19/2999" ui-mask-raw="false" placeholder="DD/MM/YYYY"/>',
		link: function(scope, element, attrs, controller) {
			scope.limitToValidDate = limitToValidDate;
			var dateFilter = $filter("date");
			var today = new Date();
			var date = {};
			function isValidDay(day) {
				return day > 0 && day < 32;
			}
			function isValidMonth(month) {
				return month >= 0 && month < 12;
			}
			function isValidYear(year) {
				return year > (today.getFullYear() - 115) && year < (today.getFullYear() + 115);
			}
			function isValidDate(inputDate) {
				inputDate = new Date(formatDate(inputDate));
				if (!angular.isDate(inputDate)) {
					return false;
				}
				date.day = inputDate.getDate();
				date.month = inputDate.getMonth();
				date.year = inputDate.getFullYear();
				// console.log(date.day)
				// console.log(date.month)
				// console.log(date.year)
				return (isValidDay(date.day)  &&  isValidMonth(date.month) && isValidYear(date.year));
			}
			function formatDate(newDate) {
				var modelDate = $parse(attrs.ngModel);
				newDate = dateFilter(newDate, "dd/mm/yyyy");
				// console.log(newDate)
				modelDate.assign(scope, newDate);
				return newDate;
	        }
	        controller.$validators.date = function(modelValue) {
	        	return angular.isDefined(modelValue) && isValidDate(modelValue);
	        };
	        var pattern = "^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)\\d\\d$" +
	          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)\\d$" +
	          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)$" +
	          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[12]$" +
	          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])$" +
	          "|^(0[1-9]|1[012])([0-3])$" +
	          "|^(0[1-9]|1[012])$" +
	          "|^[01]$";

			var regexp = new RegExp(pattern);

			function limitToValidDate(event) {
				var key = event.charCode ? event.charCode : event.keyCode;
				if ((key >= 48 && key <= 57) || key === 9 || key === 46) {
					var character = String.fromCharCode(event.which);
					var start = element[0].selectionStart;
					var end = element[0].selectionEnd;
					var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\//g, "");
					if (!(regexp.test(testValue))) {
						event.preventDefault();
					}
				}
			}
		}
	}
};
/*end date /*//**/
/*datepicker directives*/
app.directive('datepicker', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		compile: function() {
			return {
				pre: function(scope, element, attrs, ngModelCtrl) {
					var format, dateObj;
					format = (!attrs.dpFormat) ? 'dd/mm/yyyy' : attrs.dpFormat;
					if (!attrs.initDate && !attrs.dpFormat) {
						// If there is no initDate attribute than we will get todays date as the default
						dateObj = new Date();
						scope[attrs.ngModel] = dateObj.getDate() + '/' + (dateObj.getMonth() + 1) + '/' + dateObj.getFullYear();
					} else if (!attrs.initDate) {
						scope[attrs.ngModel] = attrs.initDate;
					} else {
						// I could put some complex logic that changes the order of the date string I
						// create from the dateObj based on the format, but I'll leave that for now
						// Or I could switch case and limit the types of formats...
						$('.datepicker-dropdown').hide();
					}
					// Initialize the date-picker
					$(element).datepicker({
						format: format,
					}).on('changeDate', function(ev) {
						// To me this looks cleaner than adding $apply(); after everything.
						scope.$apply(function () {
							ngModelCtrl.$setViewValue(ev.format(format));
						});
						$('.datepicker-dropdown').hide();
						// console.log(ngModelCtrl.$setViewValue(ev.format(format)));
						// console.log(ev.format(format));
						// console.log(ev.target.id);
					});
				}
			}
		}
	}
	
});


/*
app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "dd/mm/yy",
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  }
});*/

/*end datepicker*/

app.controller('tax_plan_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {
	client_id = 0;
	
	/*select default value 0 on how many children*/
	var emp_1 = { 'background': '#F8CACB' };
	var emp_2 = { 'background': '#FFD573' };
	var emp_3 = { 'background': '#60A9D7' };
	$scope.showCalendar = false;
	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				$scope.email = response.email;
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					$.ajax({
						url:'/get_payment_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							if(response.paid == 1){
								$.ajax({
									url:'/get_pan_info/',
									type:'POST',
									dataType: 'json',
									data:{'client_id':client_id},
									success: function(response){
										// console.log(response);
									}
								});
							}else
								window.location.href = "/tax_saving_account";
						}
					});
					$scope.get_tax_plan();
				}
			}
		});
		$scope.user.type_c1 = '';
		$scope.user.type_c2 = '';
		$scope.user.type_c3 = '';
		$scope.user.type_c4 = '';
		$scope.user.state_of_accommodation = '';
		$scope.user.children_value = '';
		$scope.user.current_city = '';
		// console.log($scope.user.current_city);
	},1000);
	//css on tax plan empty data

	$scope.validation = function(type,value){
		$scope.m_bs_err = '';
		if(type == "employment_state"){
			if(value == null || value == '')
				return false;
			else
				return true;
		}
		if(type == "financial_year"){
			if(value == null || value == '')
				return false;
			else
				return true;
		}
		if(type == "joining_last_date"){
			$scope.err_date = '';
			var financial_year = $scope.user.financial_year;
			if(financial_year == null || financial_year == '')
				return false;
			var doj1 = $scope.user.doj1;
			var doj2 = $scope.user.doj2;
			var doj3 = $scope.user.doj3;
			var ld1 = $scope.user.ld1;
			var ld2 = $scope.user.ld2;
			console.log(doj1);
			console.log(doj2);
			console.log(doj3);
			console.log(ld1);
			console.log(ld2);
			if(doj1 != undefined)
				if(doj1.split('/').length == 3)
					doj1 = doj1.split('/')[1]+'/'+doj1.split('/')[0]+'/'+doj1.split('/')[2];
			if(doj2 != undefined)
				if(doj2.split('/').length == 3)
					doj2 = doj2.split('/')[1]+'/'+doj2.split('/')[0]+'/'+doj2.split('/')[2];
			if(doj3 != undefined)
				if(doj3.split('/').length == 3)
					doj3 = doj3.split('/')[1]+'/'+doj3.split('/')[0]+'/'+doj3.split('/')[2];
			if(ld1 != undefined)
				if(ld1.split('/').length == 3)
					ld1 = ld1.split('/')[1]+'/'+ld1.split('/')[0]+'/'+ld1.split('/')[2];
			if(ld2 != undefined)
				if(ld2.split('/').length == 3)
					ld2 = ld2.split('/')[1]+'/'+ld2.split('/')[0]+'/'+ld2.split('/')[2];
			var doj_status = true;
			var doj1_status = true;
			var doj2_status = true;
			var doj3_status = true;
			var ld1_status = true;
			var ld2_status = true;
			var start_date = new Date('04/01/2018');
			var working_since = get_working_since();
			
			if(financial_year == '1' || financial_year == '2' || financial_year == '3' ){
				if(doj1=='' && working_since=='no')
					doj1_status = false;
				if(working_since=='no')
					doj1_status = $scope.validation('doj1',$scope.user.doj1);
			}
			if(financial_year == '2' || financial_year == '3' ){
				var doj1date = new Date(doj1);
				var ld1date = new Date(ld1);
				if(ld1=='' || (doj1date>ld1date) ){
					ld1_status = false;
					$scope.err_date = 'Check last Date of 1st company';
				}
				if(ld1.split('/').length != 3)
					ld1_status = false;
				else
					ld1_status = $scope.validation('ld1',$scope.user.ld1);
				var doj2date = new Date(doj2);
				var ld1date = new Date(ld1);
				if(doj2=='' || (ld1date>doj2date) ){
					doj2_status = false;
					$scope.err_date = 'Check doj of 2nd company';
				}
				if(doj2.split('/').length != 3)
					doj2_status = false;
				else
					ld1_status = $scope.validation('doj2',$scope.user.doj2);
			}
			if(financial_year == '3'){
				var doj2date = new Date(doj2);
				var ld2date = new Date(ld2);
				if(ld2 =='' || (doj2date>ld2date) ){
					ld2_status = false;
					$scope.err_date = 'Check last Date of 2nd company';
				}
				if(ld2.split('/').length != 3)
					ld2_status = false;
				else
					ld1_status = $scope.validation('ld2',$scope.user.ld2);
				var doj3date = new Date(doj3);
				var ld2date = new Date(ld2);
				if(doj3=='' || (ld2date>doj3date) ){
					doj3_status = false;
					$scope.err_date = 'Check doj of 3rd company';
				}
				if(doj3.split('/').length != 3)
					doj3_status = false;
				else
					ld1_status = $scope.validation('doj3',$scope.user.doj3);
			}
			// console.log(doj_status);
			// console.log(doj1_status);
			// console.log(doj2_status);
			// console.log(doj3_status);
			// console.log(ld1_status);
			// console.log(ld2_status);
			if(doj_status == true && doj1_status == true && doj2_status == true && doj3_status == true && ld1_status == true && ld2_status == true)
				return true;
			else
				return false;
		}
		if(type == "m_bs"){
			var financial_year = $scope.user.financial_year;
	 		var bs1 = $scope.user.m_bs_comp1;
			var bs2 = $scope.user.m_bs_comp2;
			var bs3 = $scope.user.m_bs_comp3;
			var bs1_status = true;
			var bs2_status = true;
			var bs3_status = true;
			var working_since = get_working_since();
			if(financial_year == '1' || financial_year == '2' || financial_year == '3')
				if( (bs1 == '' || isNaN(bs1)) && working_since=='no' ){
					bs1_status = false;
					$scope.m_bs_err = 'Enter Correct BS for Company 1';
				}
			if(financial_year == '2' || financial_year == '3')
				if(bs2 == '' || isNaN(bs2) ){
					bs2_status = false;
					$scope.m_bs_err = 'Enter Correct BS for Company 2';
				}
			if(financial_year == '3')
				if(bs3 == '' || isNaN(bs3) ){
					bs3_status = false;
					$scope.m_bs_err = 'Enter Correct BS for Company 3';
				}
			if(bs1_status == true && bs2_status == true && bs3_status == true )
				return true;
			else
				return false
		}
		if(type == "current_city"){
			$scope.err_current_city = '';
			if(value == null || value == ''){
				$scope.err_current_city = 'Select Current residing city.';
				return false;
			}
			else
				return true;
		}
		if(type == "hra"){
			$scope.err_accommodation = '';
			if(value == 'yes'){
				$scope.user.state_of_accommodation = '';
				$scope.user.rent_paid = '';
				$scope.user.hra_comp1 = '';
				$scope.user.hra_comp2 = '';
				$scope.user.hra_comp3 = '';
				return true;
			}
			else{
				var state_of_accommodation = $scope.user.state_of_accommodation;
				if(state_of_accommodation == null || state_of_accommodation == ''){
					$scope.err_accommodation = 'Select state of accommodation';
					return false;
				}
				if(state_of_accommodation == 'Rented'){
					var rent_paid = $scope.user.rent_paid;
					if(rent_paid == '' || isNaN(rent_paid) ){
						$scope.err_rent_paid = 'Enter valid Amount.';
						return false;
					}
				}
				if(state_of_accommodation == 'Family_Relatives_Friends' || state_of_accommodation == 'Rented'){
					var comp_given_hra = get_comp_given_hra();
					var comp_given_hra_status = true;
					if(comp_given_hra == 'yes'){
						var financial_year = $scope.user.financial_year;
						var hra1 = $scope.user.hra_comp1;
						var hra2 = $scope.user.hra_comp2;
						var hra3 = $scope.user.hra_comp3;
						var hra1_status = true;
						var hra2_status = true;
						var hra3_status = true;
						var working_since = get_working_since();
						if(financial_year == '1' || financial_year == '2' || financial_year == '3')
							if( (hra1 == '' || isNaN(hra1)) && working_since=='no' ){
								hra1_status = false;
								$scope.m_hra_err = 'Enter Correct HRA for Company 1';
							}
						if(financial_year == '2' || financial_year == '3')
							if(hra2 == '' || isNaN(hra2) ){
								hra2_status = false;
								$scope.m_hra_err = 'Enter Correct HRA for Company 2';
							}
						if(financial_year == '3')
							if(hra3 == '' || isNaN(hra3) ){
								hra3_status = false;
								$scope.m_hra_err = 'Enter Correct HRA for Company 3';
							}
						if(hra1_status == true && hra2_status == true && hra3_status == true)
							comp_given_hra_status = true;
						else
							comp_given_hra_status = false;
					}
					if(comp_given_hra_status == true)
						return true;
					else
						return false;
				}
				if(state_of_accommodation = 'Company_Provided'){
					$scope.user.rent_paid = '';
					$scope.user.hra_comp1 = '';
					$scope.user.hra_comp2 = '';
					$scope.user.hra_comp3 = '';
					return true;
				}
			}
		}
		if(type == 'parent_age'){
			$scope.err_parent_age = '';
			if(value == '')
				return true;
			else if(isNaN(value)){
				$scope.err_parent_age = 'Enter valid Age';
				return false;
			}
			else if(value.toString().length == 2 || value.toString().length == 3){
				if(value<40){
					$scope.err_parent_age = 'Enter valid Age';
					return false;
				}
				else
					return true;
			}
			else{
				$scope.err_parent_age = 'Enter valid Age';
				return false;
			}
		}
		if(type == "reset_child_error"){
			$scope.err_dob_c1 = '';
			$scope.err_type_c1 = '';
			$scope.err_dob_c2 = '';
			$scope.err_type_c2 = '';
			$scope.err_dob_c3 = '';
			$scope.err_type_c3 = '';
			$scope.err_dob_c4 = '';
			$scope.err_type_c4 = '';
		}
		if(type == "children_value"){
			$scope.err_children_value = '';
			if(value == '1'){
				$scope.err_dob_c1 = '';
				$scope.err_type_c1 = '';
			}
			if(value == '2'){
				$scope.err_dob_c2 = '';
				$scope.err_type_c2 = '';
			}
			if(value == '3'){
				$scope.err_dob_c3 = '';
				$scope.err_type_c3 = '';
			}
			if(value == '4'){
				$scope.err_dob_c4 = '';
				$scope.err_type_c4 = '';
			}
			var children1_status = true;
			var children2_status = true;
			var children3_status = true;
			var children4_status = true;
			var today = new Date();
			var start_date = new Date('01/01/1961');
			if(value == null || value == ''){
				$scope.err_children_value = 'Select Number of Children.';
				return false;
			}
			else{
				if(value == '1' || value == '2' || value == '3' || value == '4'){
					var dob_c1 = $scope.user.dob_c1;
					if(typeof dob_c1 === 'undefined' || dob_c1 == '' || dob_c1.split('/').length != 3){
						$scope.err_dob_c1 = 'Enter valid Date of Birth';
						children1_status = false;
					}
					else{
						if($scope.check_date_validation(dob_c1)){
							dob_c1 = dob_c1.split('/')[1]+'/'+dob_c1.split('/')[0]+'/'+dob_c1.split('/')[2];
							var date_dob_c1 = new Date(dob_c1);
							if($scope.user.type_c1 == null || $scope.user.type_c1 == '' || date_dob_c1>today || date_dob_c1<start_date ){
								if($scope.user.type_c1 == null || $scope.user.type_c1 == '')
									$scope.err_type_c1 = 'Select Daughter/Son';
								else{
									$scope.err_dob_c1 = 'Enter valid Date of Birth';
									$scope.err_type_c1 = '';
								}
								children1_status = false;
							}
						}else{
							$scope.err_dob_c1 = 'Enter valid Date of Birth';
							children1_status = false;
						}
					}
				}
				if(value == '2' || value == '3' || value == '4'){
					var dob_c2 = $scope.user.dob_c2;
					if(typeof dob_c2 === 'undefined' || dob_c2 == '' || dob_c2.split('/').length != 3){
						$scope.err_dob_c2 = 'Enter valid Date of Birth';
						children2_status = false;
					}
					else{
						if($scope.check_date_validation(dob_c2)){
							dob_c2 = dob_c2.split('/')[1]+'/'+dob_c2.split('/')[0]+'/'+dob_c2.split('/')[2];
							var date_dob_c2 = new Date(dob_c2);
							if($scope.user.type_c2 == null || $scope.user.type_c2 == '' || date_dob_c2>today || date_dob_c2<start_date ){
								if($scope.user.type_c2 == '' || $scope.user.type_c2 == null)
									$scope.err_type_c2 = 'Select Daughter/Son';
								else{
									$scope.err_dob_c2 = 'Enter valid Date of Birth';
									$scope.err_type_c2 = '';
								}
								children2_status = false;
							}
						}else{
							$scope.err_dob_c2 = 'Enter valid Date of Birth';
							children1_status = false;
						}
					}
				}
				if(value == '3' || value == '4'){
					var dob_c3 = $scope.user.dob_c3;
					if(typeof dob_c3 === 'undefined' || dob_c3 == '' || dob_c3.split('/').length != 3){
						$scope.err_dob_c3 = 'Enter valid Date of Birth';
						children3_status = false;
					}
					else{
						if(dob_c3.split('/').length == 3 && $scope.check_date_validation(dob_c3)){
							dob_c3 = dob_c3.split('/')[1]+'/'+dob_c3.split('/')[0]+'/'+dob_c3.split('/')[2];
							var date_dob_c3 = new Date(dob_c3);
							if($scope.user.type_c3 == null || $scope.user.type_c3 == '' || date_dob_c3>today || date_dob_c3<start_date ){
								if($scope.user.type_c3 == '' || $scope.user.type_c3 == null)
									$scope.err_type_c3 = 'Select Daughter/Son';
								else{
									$scope.err_dob_c3 = 'Enter valid Date of Birth';
									$scope.err_type_c3 = '';
								}
								children3_status = false;
							}
						}else{
							$scope.err_dob_c3 = 'Enter valid Date of Birth';
							children1_status = false;
						}
					}
				}
				if(value == '4'){
					var dob_c4 = $scope.user.dob_c4;
					if(typeof dob_c4 === 'undefined' || dob_c4 == '' || dob_c4.split('/').length != 3){
						$scope.err_dob_c4 = 'Enter valid Date of Birth';
						children4_status = false;
					}
					else{
						if(dob_c4.split('/').length == 3 && $scope.check_date_validation(dob_c4)){
							dob_c4 = dob_c4.split('/')[1]+'/'+dob_c4.split('/')[0]+'/'+dob_c4.split('/')[2];
							var date_dob_c4 = new Date(dob_c4);
							if($scope.user.type_c4 == null || $scope.user.type_c4 == '' || date_dob_c4>today || date_dob_c4<start_date ){
								if($scope.user.type_c4 == '' || $scope.user.type_c4 == null)
									$scope.err_type_c4 = 'Select Daughter/Son';
								else{
									$scope.err_type_c4 = '';
									$scope.err_dob_c4 = 'Enter valid Date of Birth';
								}
								children4_status = false;
							}
						}else{
							$scope.err_dob_c4 = 'Enter valid Date of Birth';
							children1_status = false;
						}
					}
				}
				if(children1_status == true && children2_status == true && children3_status == true && children4_status == true)
					return true;
				else
					return false;
			}
		}
		if(type == "incurred_travel_expense"){
			$scope.err_total_travel_expense = '';
			if(value == 'no')
				return true;
			else{
				var total_travel_expense = $scope.user.total_travel_expense;
				if(total_travel_expense == '' || isNaN(total_travel_expense) ){
					$scope.err_total_travel_expense = 'Enter valid Amount';
					return false;
				}
				else
					return true;
			}
		}
		if(type == 'home_loan_taken'){
			$scope.err_principal_amt = '';
			$scope.err_interest_amt = '';
			if(value == 'no')
				return true;
			else{
				var principal_amt = $scope.user.principal_amt;
				var interest_amt = $scope.user.interest_amt;
				if(principal_amt == '' || isNaN(principal_amt) ){
					$scope.err_principal_amt = 'Enter valid Amount';
					return false;
				}
				else if( interest_amt == '' || isNaN(interest_amt) ){
					$scope.err_interest_amt = 'Enter valid Amount';
					return false;
				}
				else
					return true;
			}
		}
		if(type == 'deduction_80c'){
			$scope.err_80c = '';
			var principal_amt = 0;
			if(get_home_loan_taken() == 'yes')
				principal_amt = $scope.user.principal_amt;
			var epf = $scope.user.epf;
			var school_fee = $scope.user.school_fee;
			var lic = $scope.user.lic;
			var elss = $scope.user.elss;
			var ulip = $scope.user.ulip;
			var fd = $scope.user.fd;
			var ppf = $scope.user.ppf;
			var other_80c = $scope.user.other_80c;
			var nsc = $scope.user.nsc;
			var sukanya = $scope.user.sukanya;
			var post_office_deposit = $scope.user.post_office_deposit;
			var r_principal_home_loan = $scope.user.r_principal_home_loan;
			var stamp_duty = $scope.user.stamp_duty;

			var principal_amt_status = true;
			var epf_status = true;
			var school_fee_status = true;
			var lic_status = true;
			var elss_status = true;
			var ulip_status = true;
			var fd_status = true;
			var ppf_status = true;
			var other_80c_status = true;
			var nsc_status = true;
			var sukanya_status = true;
			var post_office_deposit_status = true;
			var r_principal_home_loan_status = true;
			var stamp_duty_status = true;
			if(isNaN(principal_amt) )
				principal_amt_status = false;
			else
				principal_amt_status = true;
			if(isNaN(epf) )
				epf_status = false;
			else
				epf_status = true;
			if(isNaN(school_fee) )
				school_fee_status = false;
			else
				school_fee_status = true;
			if(isNaN(lic) )
				lic_status = false;
			else
				lic_status = true;
			if(isNaN(elss) )
				elss_status = false;
			else
				elss_status = true;
			if(isNaN(ulip) )
				ulip_status = false;
			else
				ulip_status = true;
			if(isNaN(fd) )
				fd_status = false;
			else
				fd_status = true;
			if(isNaN(ppf) )
				ppf_status = false;
			else
				ppf_status = true;
			if(isNaN(other_80c) )
				other_80c_status = false;
			else
				other_80c_status = true;
			if(isNaN(nsc) )
				nsc_status = false;
			if(isNaN(sukanya) )
				sukanya_status = false;
			if(isNaN(post_office_deposit) )
				post_office_deposit_status = false;
			if(isNaN(r_principal_home_loan) )
				r_principal_home_loan_status = false;
			if(isNaN(stamp_duty) )
				stamp_duty_status = false;
			
			if(epf_status == true && school_fee_status == true && lic_status == true && elss_status == true 
			 && ulip_status == true && fd_status == true && ppf_status == true && other_80c_status == true 
			 && principal_amt_status == true && nsc_status == true && sukanya_status == true 
			 && post_office_deposit_status == true && r_principal_home_loan_status == true && stamp_duty_status == true){
				$scope.calculate_total_80c();
				return true;
			}
			else{
				$scope.err_80c = 'Enter valid Amount';
				return false;
			}
		}
		if(type == 'nps'){
			$scope.err_nps = '';
			if(isNaN(value) ){
				$scope.err_nps = 'Enter valid Amount';
				return false;
			}
			else
				return true;
		}
		if(type == 'mi_your'){
			$scope.err_mi_your = '';
			if(isNaN(value) ){
				$scope.err_mi_your = 'Enter valid Amount';
				return false;
			}
			else
				return true;
		}
		if(type == 'mi_parent'){
			$scope.err_mi_parent = '';
			if(isNaN(value) ){
				$scope.err_mi_parent = 'Enter valid Amount';
				return false;
			}
			else
				return true;
		}
		if(type == 'mc_your'){
			$scope.err_mc_your = '';
			if(isNaN(value) ){
				$scope.err_mc_your = 'Enter valid Amount';
				return false;
			}
			else
				return true;
		}
		if(type == 'education_loan'){
			$scope.err_education_loan = '';
			if(isNaN(value) ){
				$scope.err_education_loan = 'Enter valid Amount';
				return false;
			}
			else
				return true;
		}
		if(type == 'doj1'){
			value = assign_date('doj1');
			$scope.user.doj1 = assign_date('doj1');
			$scope.err_doj1 = '';
			date_status = true;
			var working_since = get_working_since();
			if(working_since == 'no'){
				if(value == null || value == '')
					date_status = false;
				else if(value.split('/').length != 3)
					date_status = false;
				else
					date_status = $scope.check_date_validation(value);
			}
			if(date_status == false)
				$scope.err_doj1 = 'Enter valid Date1.';
			else{
				$scope.check_monthly_bs($scope.financial_year);
				$scope.check_monthly_hra($scope.financial_year);
			}
			return date_status;
		}
		if(type == 'ld1'){
			value = assign_date('ld1');
			$scope.user.ld1 = assign_date('ld1');
			$scope.err_ld1 = '';
			console.log(value);
			date_status = true;
			if(value == null || value == '')
				date_status = false;
			else if(value.split('/').length != 3)
				date_status = false;
			else
				date_status = $scope.check_date_validation(value);
			if(date_status == false)
				$scope.err_ld1 = 'Enter valid Date2.';
			else{
				$scope.check_monthly_bs($scope.financial_year);
				$scope.check_monthly_hra($scope.financial_year);
			}
			return date_status;
		}
		if(type == 'doj2'){
			value = assign_date('doj2');
			$scope.user.doj2 = assign_date('doj2');
			$scope.err_doj2 = '';
			date_status = true;
			if(value == null || value == '')
				date_status = false;
			else if(value.split('/').length != 3)
				date_status = false;
			else
				date_status = $scope.check_date_validation(value);
			if(date_status == false)
				$scope.err_doj2 = 'Enter valid Date.';
			else
				$scope.check_monthly_bs($scope.financial_year);
			return date_status;
		}
		if(type == 'ld2'){
			value = assign_date('ld2');
			$scope.user.ld2 = assign_date('ld2');
			$scope.err_ld2 = '';
			date_status = true;
			if(value == null || value == '')
				date_status = false;
			else if(value.split('/').length != 3)
				date_status = false;
			else
				date_status = $scope.check_date_validation(value);
			if(date_status == false)
				$scope.err_ld2 = 'Enter valid Date.';
			else
				$scope.check_monthly_bs($scope.financial_year);
			return date_status;
		}
		if(type == 'doj3'){
			value = assign_date('doj3');
			$scope.user.doj3 = assign_date('doj3');
			$scope.err_doj3 = '';
			date_status = true;
			if(value == null || value == '')
				date_status = false;
			else if(value.split('/').length != 3)
				date_status = false;
			else
				date_status = $scope.check_date_validation(value);
			if(date_status == false)
				$scope.err_doj3 = 'Enter valid Date.';
			else
				$scope.check_monthly_bs($scope.financial_year);
			return date_status;
		}
		if(type == 'state_of_accommodation')
			$scope.err_accommodation = '';
		if(type == 'rent_paid'){
			$scope.err_rent_paid = '';
			if(value == '' || isNaN(value) ){
				$scope.err_rent_paid = 'Enter valid Amount.';
				return false;
			}else{
				$scope.cal_annual_rent();
				return true;
			}
		}
		if(type == 'total_travel_expense'){
			$scope.err_total_travel_expense = '';
			if(get_incurred_travel_expense() == 'yes'){
				if(value == '' || isNaN(value) ){
					$scope.err_total_travel_expense = 'Enter valid Amount';
					return false;
				}
				else
					return true;
			} else
				return true;
		}
		if(type == 'principal_amt'){
			$scope.err_principal_amt = '';
			if(get_home_loan_taken() == 'yes'){
				if(value == '' || isNaN(value) ){
					$scope.err_principal_amt = 'Enter valid Amount';
					return false;
				}
				else
					return true;
			}else
				return true;
		}
		if(type == 'interest_amt'){
			$scope.err_interest_amt = '';
			if(get_home_loan_taken() == 'yes'){
				if(value == '' || isNaN(value) ){
					$scope.err_interest_amt = 'Enter valid Amount';
					return false;
				}
				else
					return true;
			}else
				return true;
		}
		if(type == 'type_c1'){
			$scope.err_type_c1 = '';
			if($scope.user.dob_c1 != ''){
				if(value == null ){
					$scope.err_type_c1 = 'Select Daughter/Son';
					return false;
				}
				else
					return true;
			}
		}
		if(type == 'type_c2'){
			$scope.err_type_c2 = '';
			if($scope.user.dob_c2 != ''){
				if(value == null ){
					$scope.err_type_c2 = 'Select Daughter/Son';
					return false;
				}
				else
					return true;
			}
		}
		if(type == 'type_c3'){
			$scope.err_type_c3 = '';
			if($scope.user.dob_c3 != ''){
				if(value == null ){
					$scope.err_type_c3 = 'Select Daughter/Son';
					return false;
				}
				else
					return true;
			}
		}
		if(type == 'type_c4'){
			$scope.err_type_c4 = '';
			if($scope.user.dob_c4 != ''){
				if(value == null ){
					$scope.err_type_c4 = 'Select Daughter/Son';
					return false;
				}
				else
					return true;
			}
		}
	}

	$scope.company_change = function(){
		var financial_year = $scope.user.financial_year;
		if(financial_year == 1){
			$scope.working_with_comp_since = '2.1 Are you working with this company since 1st April 2018';
			$scope.doj1_text = '2.2 Date of joining this company';
		}
		if(financial_year == 2 || financial_year == 3 ){
			$scope.working_with_comp_since = '2.1 Were you working with the first company since 1st April 2018';
			$scope.doj1_text = '2.2 Date of joining the first company';
		}
	}

	$scope.check_date = function(value){
		$scope.user.doj1 = assign_date('doj1');
		$scope.user.doj2 = assign_date('doj2');
		$scope.user.doj3 = assign_date('doj3');
		$scope.user.ld1 = assign_date('ld1');
		$scope.user.ld2 = assign_date('ld2');
		var doj1 = $scope.user.doj1;
		var doj2 = $scope.user.doj2;
		var doj3 = $scope.user.doj3;
		var ld1 = $scope.user.ld1;
		var ld2 = $scope.user.ld2;
		if(doj1 != undefined)
			doj1 = doj1.split('/')[1]+'/'+doj1.split('/')[0]+'/'+doj1.split('/')[2];
		if(doj2 != undefined)
			doj2 = doj2.split('/')[1]+'/'+doj2.split('/')[0]+'/'+doj2.split('/')[2];
		if(doj3 != undefined)
			doj3 = doj3.split('/')[1]+'/'+doj3.split('/')[0]+'/'+doj3.split('/')[2];
		if(ld1 != undefined)
			ld1 = ld1.split('/')[1]+'/'+ld1.split('/')[0]+'/'+ld1.split('/')[2];
		if(ld2 != undefined)
			ld2 = ld2.split('/')[1]+'/'+ld2.split('/')[0]+'/'+ld2.split('/')[2];
		var doj_status = true;
		var doj1_status = true;
		var doj2_status = true;
		var doj3_status = true;
		var ld1_status = true;
		var ld2_status = true;
		var start_date = new Date('04/01/2018');
		var end_date = new Date();
		var working_since = get_working_since();
		if(working_since=='yes')
			doj1 = '04/01/2018';
		var start_date1 = new Date(doj1);
		if(start_date>=start_date1){
			doj_status = false;
		}
		if(value == 'doj1'){
			if(doj1=='' && working_since=='no'){
				doj1_status = false;
			}
		}
		if(value == 'doj2'){
			$scope.err_date = '';
			var doj2date = new Date(doj2);
			var ld1date = new Date(ld1);
			if(doj2=='' || (ld1date>doj2date) ){
				doj2_status = false;
				$scope.err_date = 'Check doj of 2nd company';
			}
		}
		if(value == 'doj3'){
			$scope.err_date = '';
			var doj3date = new Date(doj3);
			var ld2date = new Date(ld2);
			console.log(doj3date);
			if(doj3=='' || (ld2date>doj3date) ){
				doj3_status = false;
				$scope.err_date = 'Check doj of 3rd company';
			}
		}
		if(value == 'ld1'){
			$scope.err_date = '';
			var doj1date = new Date(doj1);
			var ld1date = new Date(ld1);
			if(ld1=='' || (doj1date>ld1date) ){
				ld1_status = false;
				$scope.err_date = 'Check last Date of 1st company';
			}
		}
		if(value == 'ld2'){
			$scope.err_date = '';
			var doj2date = new Date(doj2);
			var ld2date = new Date(ld2);
			if(ld2 =='' || (doj2date>ld2date) ){
				ld2_status = false;
				$scope.err_date = 'Check last Date of 2nd company';
			}
		}
	}

	$scope.check_monthly_bs = function(value){
		$scope.m_bs_err = '';
		var doj1 = $scope.user.doj1;
		var doj2 = $scope.user.doj2;
		var doj3 = $scope.user.doj3;
		var ld1 = $scope.user.ld1;
		var ld2 = $scope.user.ld2;
		if(doj1 != undefined)
			doj1 = doj1.split('/')[1]+'/'+doj1.split('/')[0]+'/'+doj1.split('/')[2];
		if(doj2 != undefined)
			doj2 = doj2.split('/')[1]+'/'+doj2.split('/')[0]+'/'+doj2.split('/')[2];
		if(doj3 != undefined)
			doj3 = doj3.split('/')[1]+'/'+doj3.split('/')[0]+'/'+doj3.split('/')[2];
		if(ld1 != undefined)
			ld1 = ld1.split('/')[1]+'/'+ld1.split('/')[0]+'/'+ld1.split('/')[2];
		if(ld2 != undefined)
			ld2 = ld2.split('/')[1]+'/'+ld2.split('/')[0]+'/'+ld2.split('/')[2];
		var bs1 = $scope.user.m_bs_comp1;
		var bs2 = $scope.user.m_bs_comp2;
		var bs3 = $scope.user.m_bs_comp3;
		var bs1_status = true;
		var bs2_status = true;
		var bs3_status = true;
		var working_since = get_working_since();
		if(working_since=='yes')
			doj1 = '04/01/2018';
		var date_apr = new Date('04/01/2018');
		var date_may = new Date('05/01/2018');
		var date_jun = new Date('06/01/2018');
		var date_jul = new Date('07/01/2018');
		var date_aug = new Date('08/01/2018');
		var date_sep = new Date('09/01/2018');
		var date_oct = new Date('10/01/2018');
		var date_nov = new Date('11/01/2018');
		var date_dec = new Date('12/01/2018');
		var date_jan = new Date('01/01/2018');
		var date_feb = new Date('02/01/2018');
		var date_mar = new Date('03/01/2018');
		var end_date = new Date('03/31/2019');

		if(value == '1' || value == '2' || value == '3'){
			if( bs1 == '' || isNaN(bs1) ){
				bs1_status = false;
				$scope.m_bs_err = 'Enter Correct Basic Salary for Company 1';
			}
		}
		if(value == '2' || value == '3'){
			if(bs2 == '' || isNaN(bs2) ){
				bs2_status = false;
				$scope.m_bs_err = 'Enter Correct Basic Salary for Company 2';
			}
		}
		if(value == '3'){
			if(bs3 == '' || isNaN(bs3) ){
				bs3_status = false;
				$scope.m_bs_err = 'Enter Correct Basic Salary for Company 3';
			}
		}
		if(bs1_status == true && bs2_status == true && bs3_status == true ){
			if($scope.user.financial_year == '1'){
				var doj1date = new Date(doj1);
				$scope.date_range(doj1date,end_date,bs1);
			}
			if($scope.user.financial_year == '2'){
				var doj1date = new Date(doj1);
				var doj2date = new Date(doj2);
				var ld1date = new Date(ld1);
				$scope.date_range2(doj1date,ld1date,doj2date,end_date,bs1,bs2);
			}
			if($scope.user.financial_year == '3'){
				var doj1date = new Date(doj1);
				var doj2date = new Date(doj2);
				var doj3date = new Date(doj3);
				var ld1date = new Date(ld1);
				var ld2date = new Date(ld2);
				$scope.date_range3(doj1date,ld1date,doj2date,ld2date,doj3date,end_date,bs1,bs2,bs3);
			}
		}
	}

	$scope.date_range = function(start_date,end_date,bs){
		$scope.bs_apr = 0;	$scope.bs_may = 0;	$scope.bs_jun = 0;	$scope.bs_jul = 0;
		$scope.bs_aug = 0;	$scope.bs_sep = 0;	$scope.bs_oct = 0;	$scope.bs_nov = 0;
		$scope.bs_dec = 0;	$scope.bs_jan = 0;	$scope.bs_feb = 0;	$scope.bs_mar = 0;
		$scope.bg_apr = '';	$scope.bg_may = '';	$scope.bg_jun = '';	$scope.bg_jul = '';
		$scope.bg_aug = '';	$scope.bg_sep = '';	$scope.bg_oct = '';	$scope.bg_nov = '';
		$scope.bg_dec = '';	$scope.bg_jan = '';	$scope.bg_feb = '';	$scope.bg_mar = '';
		var bs_3 = 0;
		var bs_4 = 0;
		var bs_5 = 0;
		var bs_6 = 0;
		var bs_7 = 0;
		var bs_8 = 0;
		var bs_9 = 0;
		var bs_10 = 0;
		var bs_11 = 0;
		var bs_0 = 0;
		var bs_1 = 0;
		var bs_2 = 0;
		var start_d = new Date('01/04/2018');
		if(start_date<start_d)
			start_date = start_d;
		var d1 = start_date;
		for (var d = d1; d <= end_date; d.setDate(d.getDate() + 1) ) {
			// console.log(new Date(d));
			if(d.getMonth()==3){
				bs_3 = bs_3 + bs/30;
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				bs_4 = bs_4 + bs/31;
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_1;
			}
			if(d.getMonth()==5){
				bs_5 = bs_5 + bs/30;
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				bs_6 = bs_6 + bs/31;
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				bs_7 = bs_7 + bs/31;
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				bs_8 = bs_8 + bs/30;
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				bs_9 = bs_9 + bs/31;
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				bs_10 = bs_10 + bs/30;
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				bs_11 = bs_11 + bs/31;
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					bs_0 = bs_0 + bs/31;
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					bs_1 = bs_1 + bs/28;
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					bs_2 = bs_2 + bs/31;
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_1;
				}
			}
		}
		// console.log(start_date);
		// console.log(end_date);
	}

	$scope.date_range2 = function(doj1date,ld1date,doj2date,end_date,bs1,bs2){
		$scope.bs_apr = 0;	$scope.bs_may = 0;	$scope.bs_jun = 0;
		$scope.bs_jul = 0;	$scope.bs_aug = 0;	$scope.bs_sep = 0;
		$scope.bs_oct = 0;	$scope.bs_nov = 0;	$scope.bs_dec = 0;
		$scope.bs_jan = 0;	$scope.bs_feb = 0;	$scope.bs_mar = 0;
		$scope.bg_apr = '';	$scope.bg_may = '';	$scope.bg_jun = '';
		$scope.bg_jul = '';	$scope.bg_aug = '';	$scope.bg_sep = '';
		$scope.bg_oct = '';	$scope.bg_nov = '';	$scope.bg_dec = '';
		$scope.bg_jan = '';	$scope.bg_feb = '';	$scope.bg_mar = '';
		var bs_3 = 0;
		var bs_4 = 0;
		var bs_5 = 0;
		var bs_6 = 0;
		var bs_7 = 0;
		var bs_8 = 0;
		var bs_9 = 0;
		var bs_10 = 0;
		var bs_11 = 0;
		var bs_0 = 0;
		var bs_1 = 0;
		var bs_2 = 0;
		var start_d = new Date('01/04/2018');
		if(doj1date<start_d)
			doj1date = start_d;
		var d1 = doj1date;
		for (var d = d1; d <= ld1date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				bs_3 = bs_3 + bs1/30;
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				bs_4 = bs_4 + bs1/31;
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_1;
			}
			if(d.getMonth()==5){
				bs_5 = bs_5 + bs1/30;
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				bs_6 = bs_6 + bs1/31;
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				bs_7 = bs_7 + bs1/31;
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				bs_8 = bs_8 + bs1/30;
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				bs_9 = bs_9 + bs1/31;
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				bs_10 = bs_10 + bs1/30;
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				bs_11 = bs_11 + bs1/31;
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					bs_0 = bs_0 + bs1/31;
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					bs_1 = bs_1 + bs1/28;
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					bs_2 = bs_2 + bs1/31;
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_1;
				}
			}
		}
		var d1 = ld1date;
		for (var d = d1; d <= doj2date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_1;
			}
			if(d.getMonth()==5){
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_1;
				}
			}
		}
		var d1 = doj2date;
		for (var d = d1; d <= end_date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				bs_3 = bs_3 + bs2/30;
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_2;
			}
			if(d.getMonth()==4){
				bs_4 = bs_4 + bs2/31;
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_2;
			}
			if(d.getMonth()==5){
				bs_5 = bs_5 + bs2/30;
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_2;
			}
			if(d.getMonth()==6){
				bs_6 = bs_6 + bs2/31;
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_2;
			}
			if(d.getMonth()==7){
				bs_7 = bs_7 + bs2/31;
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_2;
			}
			if(d.getMonth()==8){
				bs_8 = bs_8 + bs2/30;
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_2;
			}
			if(d.getMonth()==9){
				bs_9 = bs_9 + bs2/31;
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_2;
			}
			if(d.getMonth()==10){
				bs_10 = bs_10 + bs2/30;
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_2;
			}
			if(d.getMonth()==11){
				bs_11 = bs_11 + bs2/31;
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_2;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					bs_0 = bs_0 + bs2/31;
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_2;
				}
				if(d.getMonth()==1){
					bs_1 = bs_1 + bs2/28;
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_2;
				}
				if(d.getMonth()==2){
					bs_2 = bs_2 + bs2/31;
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_2;
				}
			}
		}
	}

	$scope.date_range3 = function(doj1date,ld1date,doj2date,ld2date,doj3date,end_date,bs1,bs2,bs3){
		$scope.bs_apr = 0;	$scope.bs_may = 0;	$scope.bs_jun = 0;	$scope.bs_jul = 0;
		$scope.bs_aug = 0;	$scope.bs_sep = 0;	$scope.bs_oct = 0;	$scope.bs_nov = 0;
		$scope.bs_dec = 0;	$scope.bs_jan = 0;	$scope.bs_feb = 0;	$scope.bs_mar = 0;
		$scope.bg_apr = '';	$scope.bg_may = '';	$scope.bg_jun = '';	$scope.bg_jul = '';
		$scope.bg_aug = '';	$scope.bg_sep = '';	$scope.bg_oct = '';	$scope.bg_nov = '';
		$scope.bg_dec = '';	$scope.bg_jan = '';	$scope.bg_feb = '';	$scope.bg_mar = '';
		var bs_3 = 0;
		var bs_4 = 0;
		var bs_5 = 0;
		var bs_6 = 0;
		var bs_7 = 0;
		var bs_8 = 0;
		var bs_9 = 0;
		var bs_10 = 0;
		var bs_11 = 0;
		var bs_0 = 0;
		var bs_1 = 0;
		var bs_2 = 0;
		var start_d = new Date('01/04/2018');
		if(doj1date<start_d)
			doj1date = start_d;
		var d1 = doj1date;
		for (var d = d1; d <= ld1date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				bs_3 = bs_3 + bs1/30;
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				bs_4 = bs_4 + bs1/31;
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_1;
			}
			if(d.getMonth()==5){
				bs_5 = bs_5 + bs1/30;
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				bs_6 = bs_6 + bs1/31;
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				bs_7 = bs_7 + bs1/31;
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				bs_8 = bs_8 + bs1/30;
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				bs_9 = bs_9 + bs1/31;
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				bs_10 = bs_10 + bs1/30;
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				bs_11 = bs_11 + bs1/31;
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					bs_0 = bs_0 + bs1/31;
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					bs_1 = bs_1 + bs1/28;
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					bs_2 = bs_2 + bs1/31;
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_1;
				}
			}
		}
		var d1 = ld1date;
		for (var d = d1; d <= doj2date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_1;
			}
			if(d.getMonth()==5){
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_1;
				}
			}
		}
		var d1 = doj2date;
		for (var d = d1; d <= ld2date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				bs_3 = bs_3 + bs2/30;
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_2;
			}
			if(d.getMonth()==4){
				bs_4 = bs_4 + bs2/31;
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_2;
			}
			if(d.getMonth()==5){
				bs_5 = bs_5 + bs2/30;
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_2;
			}
			if(d.getMonth()==6){
				bs_6 = bs_6 + bs2/31;
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_2;
			}
			if(d.getMonth()==7){
				bs_7 = bs_7 + bs2/31;
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_2;
			}
			if(d.getMonth()==8){
				bs_8 = bs_8 + bs2/30;
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_2;
			}
			if(d.getMonth()==9){
				bs_9 = bs_9 + bs2/31;
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_2;
			}
			if(d.getMonth()==10){
				bs_10 = bs_10 + bs2/30;
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_2;
			}
			if(d.getMonth()==11){
				bs_11 = bs_11 + bs2/31;
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_2;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					bs_0 = bs_0 + bs2/31;
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_2;
				}
				if(d.getMonth()==1){
					bs_1 = bs_1 + bs2/28;
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_2;
				}
				if(d.getMonth()==2){
					bs_2 = bs_2 + bs2/31;
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_2;
				}
			}
		}
		var d1 = ld2date;
		for (var d = d1; d <= doj3date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_2;
			}
			if(d.getMonth()==4){
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_2;
			}
			if(d.getMonth()==5){
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_2;
			}
			if(d.getMonth()==6){
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_2;
			}
			if(d.getMonth()==7){
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_2;
			}
			if(d.getMonth()==8){
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_2;
			}
			if(d.getMonth()==9){
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_2;
			}
			if(d.getMonth()==10){
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_2;
			}
			if(d.getMonth()==11){
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_2;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_2;
				}
				if(d.getMonth()==1){
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_2;
				}
				if(d.getMonth()==2){
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_2;
				}
			}
		}
		var d1 = doj3date;
		for (var d = d1; d <= end_date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				bs_3 = bs_3 + bs3/30;
				$scope.bs_apr = parseInt(Math.round(bs_3));
				$scope.bg_apr = emp_3;
			}
			if(d.getMonth()==4){
				bs_4 = bs_4 + bs3/31;
				$scope.bs_may = parseInt(Math.round(bs_4));
				$scope.bg_may = emp_3;
			}
			if(d.getMonth()==5){
				bs_5 = bs_5 + bs3/30;
				$scope.bs_jun = parseInt(Math.round(bs_5));
				$scope.bg_jun = emp_3;
			}
			if(d.getMonth()==6){
				bs_6 = bs_6 + bs3/31;
				$scope.bs_jul = parseInt(Math.round(bs_6));
				$scope.bg_jul = emp_3;
			}
			if(d.getMonth()==7){
				bs_7 = bs_7 + bs3/31;
				$scope.bs_aug = parseInt(Math.round(bs_7));
				$scope.bg_aug = emp_3;
			}
			if(d.getMonth()==8){
				bs_8 = bs_8 + bs3/30;
				$scope.bs_sep = parseInt(Math.round(bs_8));
				$scope.bg_sep = emp_3;
			}
			if(d.getMonth()==9){
				bs_9 = bs_9 + bs3/31;
				$scope.bs_oct = parseInt(Math.round(bs_9));
				$scope.bg_oct = emp_3;
			}
			if(d.getMonth()==10){
				bs_10 = bs_10 + bs3/30;
				$scope.bs_nov = parseInt(Math.round(bs_10));
				$scope.bg_nov = emp_3;
			}
			if(d.getMonth()==11){
				bs_11 = bs_11 + bs3/31;
				$scope.bs_dec = parseInt(Math.round(bs_11));
				$scope.bg_dec = emp_3;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					bs_0 = bs_0 + bs3/31;
					$scope.bs_jan = parseInt(Math.round(bs_0));
					$scope.bg_jan = emp_3;
				}
				if(d.getMonth()==1){
					bs_1 = bs_1 + bs3/28;
					$scope.bs_feb = parseInt(Math.round(bs_1));
					$scope.bg_feb = emp_3;
				}
				if(d.getMonth()==2){
					bs_2 = bs_2 + bs3/31;
					$scope.bs_mar = parseInt(Math.round(bs_2));
					$scope.bg_mar = emp_3;
				}
			}
		}
	}

	$scope.check_monthly_hra = function(value){
		$scope.m_hra_err = "";
		var doj1 = $scope.user.doj1;
		var doj2 = $scope.user.doj2;
		var doj3 = $scope.user.doj3;
		var ld1 = $scope.user.ld1;
		var ld2 = $scope.user.ld2;
		if(doj1 != undefined)
			doj1 = doj1.split('/')[1]+'/'+doj1.split('/')[0]+'/'+doj1.split('/')[2];
		if(doj2 != undefined)
			doj2 = doj2.split('/')[1]+'/'+doj2.split('/')[0]+'/'+doj2.split('/')[2];
		if(doj3 != undefined)
			doj3 = doj3.split('/')[1]+'/'+doj3.split('/')[0]+'/'+doj3.split('/')[2];
		if(ld1 != undefined)
			ld1 = ld1.split('/')[1]+'/'+ld1.split('/')[0]+'/'+ld1.split('/')[2];
		if(ld2 != undefined)
			ld2 = ld2.split('/')[1]+'/'+ld2.split('/')[0]+'/'+ld2.split('/')[2];
		var hra1 = $scope.user.hra_comp1;
		var hra2 = $scope.user.hra_comp2;
		var hra3 = $scope.user.hra_comp3;
		var hra1_status = true;
		var hra2_status = true;
		var hra3_status = true;
		var working_since = get_working_since();
		if(working_since=='yes')
			doj1 = '04/01/2018';
		var date_apr = new Date('04/01/2018');
		var date_may = new Date('05/01/2018');
		var date_jun = new Date('06/01/2018');
		var date_jul = new Date('07/01/2018');
		var date_aug = new Date('08/01/2018');
		var date_sep = new Date('09/01/2018');
		var date_oct = new Date('10/01/2018');
		var date_nov = new Date('11/01/2018');
		var date_dec = new Date('12/01/2018');
		var date_jan = new Date('01/01/2018');
		var date_feb = new Date('02/01/2018');
		var date_mar = new Date('03/01/2018');
		var end_date = new Date('03/31/2019');

		if(value == '1' || value == '2' || value == '3'){
			if( (hra1 == '' || isNaN(hra1)) && working_since=='no' ){
				hra1_status = false;
				$scope.m_hra_err = 'Enter Correct HRA for Company 1';
			}
		}
		if(value == '2' || value == '3'){
			if(hra2 == '' || isNaN(hra2) ){
				hra2_status = false;
				$scope.m_hra_err = 'Enter Correct HRA for Company 2';
			}
		}
		if(value == '3'){
			if(hra3 == '' || isNaN(hra3) ){
				hra3_status = false;
				$scope.m_hra_err = 'Enter Correct HRA for Company 3';
			}
		}
		if(hra1_status == true && hra2_status == true && hra3_status == true ){
			if($scope.user.financial_year == '1'){
				var doj1date = new Date(doj1);
				$scope.hra_date_range(doj1date,end_date,hra1);
			}
			if($scope.user.financial_year == '2'){
				var doj1date = new Date(doj1);
				var doj2date = new Date(doj2);
				var ld1date = new Date(ld1);
				$scope.hra_date_range2(doj1date,ld1date,doj2date,end_date,hra1,hra2);
			}
			if($scope.user.financial_year == '3'){
				var doj1date = new Date(doj1);
				var doj2date = new Date(doj2);
				var doj3date = new Date(doj3);
				var ld1date = new Date(ld1);
				var ld2date = new Date(ld2);
				$scope.hra_date_range3(doj1date,ld1date,doj2date,ld2date,doj3date,end_date,hra1,hra2,hra3);
			}
		}
	}

	$scope.hra_date_range = function(start_date,end_date,bs){
		$scope.hra_apr = 0; $scope.hra_may = 0; $scope.hra_jun = 0; $scope.hra_jul = 0;
		$scope.hra_aug = 0; $scope.hra_sep = 0; $scope.hra_oct = 0; $scope.hra_nov = 0;
		$scope.hra_dec = 0; $scope.hra_jan = 0; $scope.hra_feb = 0; $scope.hra_mar = 0;
		$scope.hra_bg_apr = ''; $scope.hra_bg_may = ''; $scope.hra_bg_jun = ''; $scope.hra_bg_jul = '';
		$scope.hra_bg_aug = ''; $scope.hra_bg_sep = ''; $scope.hra_bg_oct = ''; $scope.hra_bg_nov = '';
		$scope.hra_bg_dec = ''; $scope.hra_bg_jan = ''; $scope.hra_bg_feb = ''; $scope.hra_bg_mar = '';
		var hra_3 = 0;
		var hra_4 = 0;
		var hra_5 = 0;
		var hra_6 = 0;
		var hra_7 = 0;
		var hra_8 = 0;
		var hra_9 = 0;
		var hra_10 = 0;
		var hra_11 = 0;
		var hra_0 = 0;
		var hra_1 = 0;
		var hra_2 = 0;
		var start_d = new Date('01/04/2018');
		if(start_date<start_d)
			start_date = start_d;
		var d1 = start_date;
		for (var d = d1; d <= end_date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				hra_3 = hra_3 + bs/30;
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				hra_4 = hra_4 + bs/31;
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_1;
			}
			if(d.getMonth()==5){
				hra_5 = hra_5 + bs/30;
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				hra_6 = hra_6 + bs/31;
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				hra_7 = hra_7 + bs/31;
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				hra_8 = hra_8 + bs/30;
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				hra_9 = hra_9 + bs/31;
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				hra_10 = hra_10 + bs/30;
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				hra_11 = hra_11 + bs/31;
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					hra_0 = hra_0 + bs/31;
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					hra_1 = hra_1 + bs/28;
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					hra_2 = hra_2 + bs/31;
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_1;
				}
			}
		}
	}

	$scope.hra_date_range2 = function(doj1date,ld1date,doj2date,end_date,bs1,bs2){
		$scope.hra_apr = 0; $scope.hra_may = 0; $scope.hra_jun = 0; $scope.hra_jul = 0;
		$scope.hra_aug = 0; $scope.hra_sep = 0; $scope.hra_oct = 0; $scope.hra_nov = 0;
		$scope.hra_dec = 0; $scope.hra_jan = 0; $scope.hra_feb = 0; $scope.hra_mar = 0;
		$scope.hra_bg_apr = ''; $scope.hra_bg_may = ''; $scope.hra_bg_jun = ''; $scope.hra_bg_jul = '';
		$scope.hra_bg_aug = ''; $scope.hra_bg_sep = ''; $scope.hra_bg_oct = ''; $scope.hra_bg_nov = '';
		$scope.hra_bg_dec = ''; $scope.hra_bg_jan = ''; $scope.hra_bg_feb = ''; $scope.hra_bg_mar = '';
		var hra_3 = 0;
		var hra_4 = 0;
		var hra_5 = 0;
		var hra_6 = 0;
		var hra_7 = 0;
		var hra_8 = 0;
		var hra_9 = 0;
		var hra_10 = 0;
		var hra_11 = 0;
		var hra_0 = 0;
		var hra_1 = 0;
		var hra_2 = 0;
		var start_d = new Date('01/04/2018');
		if(doj1date<start_d)
			doj1date = start_d;
		var d1 = doj1date;
		for (var d = d1; d <= ld1date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				hra_3 = hra_3 + bs1/30;
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				hra_4 = hra_4 + bs1/31;
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_1;
			}
			if(d.getMonth()==5){
				hra_5 = hra_5 + bs1/30;
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				hra_6 = hra_6 + bs1/31;
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				hra_7 = hra_7 + bs1/31;
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				hra_8 = hra_8 + bs1/30;
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				hra_9 = hra_9 + bs1/31;
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				hra_10 = hra_10 + bs1/30;
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				hra_11 = hra_11 + bs1/31;
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					hra_0 = hra_0 + bs1/31;
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					hra_1 = hra_1 + bs1/28;
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					hra_2 = hra_2 + bs1/31;
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_1;
				}
			}
		}
		var d1 = ld1date;
		for (var d = d1; d <= doj2date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_1;
			}
			if(d.getMonth()==5){
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_1;
				}
			}
		}
		var d1 = doj2date;
		for (var d = d1; d <= end_date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				hra_3 = hra_3 + bs2/30;
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_2;
			}
			if(d.getMonth()==4){
				hra_4 = hra_4 + bs2/31;
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_2;
			}
			if(d.getMonth()==5){
				hra_5 = hra_5 + bs2/30;
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_2;
			}
			if(d.getMonth()==6){
				hra_6 = hra_6 + bs2/31;
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_2;
			}
			if(d.getMonth()==7){
				hra_7 = hra_7 + bs2/31;
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_2;
			}
			if(d.getMonth()==8){
				hra_8 = hra_8 + bs2/30;
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_2;
			}
			if(d.getMonth()==9){
				hra_9 = hra_9 + bs2/31;
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_2;
			}
			if(d.getMonth()==10){
				hra_10 = hra_10 + bs2/30;
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_2;
			}
			if(d.getMonth()==11){
				hra_11 = hra_11 + bs2/31;
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_2;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					hra_0 = hra_0 + bs2/31;
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_2;
				}
				if(d.getMonth()==1){
					hra_1 = hra_1 + bs2/28;
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_2;
				}
				if(d.getMonth()==2){
					hra_2 = hra_2 + bs2/31;
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_2;
				}
			}
		}
	}

	$scope.hra_date_range3 = function(doj1date,ld1date,doj2date,ld2date,doj3date,end_date,bs1,bs2,bs3){
		$scope.hra_apr = 0; $scope.hra_may = 0; $scope.hra_jun = 0; $scope.hra_jul = 0;
		$scope.hra_aug = 0; $scope.hra_sep = 0; $scope.hra_oct = 0; $scope.hra_nov = 0;
		$scope.hra_dec = 0; $scope.hra_jan = 0; $scope.hra_feb = 0; $scope.hra_mar = 0;
		$scope.hra_bg_apr = ''; $scope.hra_bg_may = ''; $scope.hra_bg_jun = ''; $scope.hra_bg_jul = '';
		$scope.hra_bg_aug = ''; $scope.hra_bg_sep = ''; $scope.hra_bg_oct = ''; $scope.hra_bg_nov = '';
		$scope.hra_bg_dec = ''; $scope.hra_bg_jan = ''; $scope.hra_bg_feb = ''; $scope.hra_bg_mar = '';
		var hra_3 = 0;
		var hra_4 = 0;
		var hra_5 = 0;
		var hra_6 = 0;
		var hra_7 = 0;
		var hra_8 = 0;
		var hra_9 = 0;
		var hra_10 = 0;
		var hra_11 = 0;
		var hra_0 = 0;
		var hra_1 = 0;
		var hra_2 = 0;
		var start_d = new Date('01/04/2018');
		if(doj1date<start_d)
			doj1date = start_d;
		var d1 = doj1date;
		for (var d = d1; d <= ld1date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				hra_3 = hra_3 + bs1/30;
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				hra_4 = hra_4 + bs1/31;
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_1;
			}
			if(d.getMonth()==5){
				hra_5 = hra_5 + bs1/30;
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				hra_6 = hra_6 + bs1/31;
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				hra_7 = hra_7 + bs1/31;
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				hra_8 = hra_8 + bs1/30;
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				hra_9 = hra_9 + bs1/31;
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				hra_10 = hra_10 + bs1/30;
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				hra_11 = hra_11 + bs1/31;
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					hra_0 = hra_0 + bs1/31;
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					hra_1 = hra_1 + bs1/28;
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					hra_2 = hra_2 + bs1/31;
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_1;
				}
			}
		}
		var d1 = ld1date;
		for (var d = d1; d <= doj2date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_1;
			}
			if(d.getMonth()==4){
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_1;
			}
			if(d.getMonth()==5){
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_1;
			}
			if(d.getMonth()==6){
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_1;
			}
			if(d.getMonth()==7){
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_1;
			}
			if(d.getMonth()==8){
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_1;
			}
			if(d.getMonth()==9){
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_1;
			}
			if(d.getMonth()==10){
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_1;
			}
			if(d.getMonth()==11){
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_1;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_1;
				}
				if(d.getMonth()==1){
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_1;
				}
				if(d.getMonth()==2){
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_1;
				}
			}
		}
		var d1 = doj2date;
		for (var d = d1; d <= ld2date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				hra_3 = hra_3 + bs2/30;
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_2;
			}
			if(d.getMonth()==4){
				hra_4 = hra_4 + bs2/31;
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_2;
			}
			if(d.getMonth()==5){
				hra_5 = hra_5 + bs2/30;
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_2;
			}
			if(d.getMonth()==6){
				hra_6 = hra_6 + bs2/31;
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_2;
			}
			if(d.getMonth()==7){
				hra_7 = hra_7 + bs2/31;
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_2;
			}
			if(d.getMonth()==8){
				hra_8 = hra_8 + bs2/30;
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_2;
			}
			if(d.getMonth()==9){
				hra_9 = hra_9 + bs2/31;
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_2;
			}
			if(d.getMonth()==10){
				hra_10 = hra_10 + bs2/30;
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_2;
			}
			if(d.getMonth()==11){
				hra_11 = hra_11 + bs2/31;
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_2;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					hra_0 = hra_0 + bs2/31;
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_2;
				}
				if(d.getMonth()==1){
					hra_1 = hra_1 + bs2/28;
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_2;
				}
				if(d.getMonth()==2){
					hra_2 = hra_2 + bs2/31;
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_2;
				}
			}
		}
		var d1 = ld2date;
		for (var d = d1; d <= doj3date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_2;
			}
			if(d.getMonth()==4){
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_2;
			}
			if(d.getMonth()==5){
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_2;
			}
			if(d.getMonth()==6){
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_2;
			}
			if(d.getMonth()==7){
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_2;
			}
			if(d.getMonth()==8){
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_2;
			}
			if(d.getMonth()==9){
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_2;
			}
			if(d.getMonth()==10){
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_2;
			}
			if(d.getMonth()==11){
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_2;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_2;
				}
				if(d.getMonth()==1){
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_2;
				}
				if(d.getMonth()==2){
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_2;
				}
			}
		}
		var d1 = doj3date;
		for (var d = d1; d <= end_date; d.setDate(d.getDate() + 1) ) {
			if(d.getMonth()==3){
				hra_3 = hra_3 + bs3/30;
				$scope.hra_apr = parseInt(Math.round(hra_3));
				$scope.hra_bg_apr = emp_3;
			}
			if(d.getMonth()==4){
				hra_4 = hra_4 + bs3/31;
				$scope.hra_may = parseInt(Math.round(hra_4));
				$scope.hra_bg_may = emp_3;
			}
			if(d.getMonth()==5){
				hra_5 = hra_5 + bs3/30;
				$scope.hra_jun = parseInt(Math.round(hra_5));
				$scope.hra_bg_jun = emp_3;
			}
			if(d.getMonth()==6){
				hra_6 = hra_6 + bs3/31;
				$scope.hra_jul = parseInt(Math.round(hra_6));
				$scope.hra_bg_jul = emp_3;
			}
			if(d.getMonth()==7){
				hra_7 = hra_7 + bs3/31;
				$scope.hra_aug = parseInt(Math.round(hra_7));
				$scope.hra_bg_aug = emp_3;
			}
			if(d.getMonth()==8){
				hra_8 = hra_8 + bs3/30;
				$scope.hra_sep = parseInt(Math.round(hra_8));
				$scope.hra_bg_sep = emp_3;
			}
			if(d.getMonth()==9){
				hra_9 = hra_9 + bs3/31;
				$scope.hra_oct = parseInt(Math.round(hra_9));
				$scope.hra_bg_oct = emp_3;
			}
			if(d.getMonth()==10){
				hra_10 = hra_10 + bs3/30;
				$scope.hra_nov = parseInt(Math.round(hra_10));
				$scope.hra_bg_nov = emp_3;
			}
			if(d.getMonth()==11){
				hra_11 = hra_11 + bs3/31;
				$scope.hra_dec = parseInt(Math.round(hra_11));
				$scope.hra_bg_dec = emp_3;
			}
			if(d>=new Date('01/01/2019')){
				if(d.getMonth()==0){
					hra_0 = hra_0 + bs3/31;
					$scope.hra_jan = parseInt(Math.round(hra_0));
					$scope.hra_bg_jan = emp_3;
				}
				if(d.getMonth()==1){
					hra_1 = hra_1 + bs3/28;
					$scope.hra_feb = parseInt(Math.round(hra_1));
					$scope.hra_bg_feb = emp_3;
				}
				if(d.getMonth()==2){
					hra_2 = hra_2 + bs3/31;
					$scope.hra_mar = parseInt(Math.round(hra_2));
					$scope.hra_bg_mar = emp_3;
				}
			}
		}
	}

	$scope.submit = function(){ 
        $scope.common_err = '';
		var doj1 = $scope.user.doj1;
		var doj2 = $scope.user.doj2;
		var doj3 = $scope.user.doj3;
		var emp_state = $scope.user.employment_state;
		var financial_year = $scope.user.financial_year;
		var current_city = $scope.user.current_city;
		// console.log(doj1)
		var hra = get_house_owned_by_you();
		var parent_age = $scope.user.parent_age;
		var children_value = $scope.user.children_value;
		var incurred_travel_expense = get_incurred_travel_expense();
		var home_loan_taken = get_home_loan_taken();
		var nps = $scope.user.nps;
		var mi_your = $scope.user.mi_your;
		var mi_parent = $scope.user.mi_parent;
		var mc_your = $scope.user.mc_your;
		var education_loan = $scope.user.education_loan;
		var emp_state_status = $scope.validation('employment_state',emp_state);
		var financial_year_status = $scope.validation('financial_year',financial_year);
		var jd_ld_status = $scope.validation('joining_last_date','');
		var m_bs_status = $scope.validation('m_bs','');
		var current_city_status = $scope.validation('current_city', current_city);
		var hra_status = $scope.validation('hra', hra);
		var parent_age_status = $scope.validation('parent_age', parent_age)
		var children_value_status = $scope.validation('children_value', children_value);
		var incurred_travel_expense_status = $scope.validation('incurred_travel_expense', incurred_travel_expense);
		var home_loan_taken_status = $scope.validation('home_loan_taken', home_loan_taken);
		var deduction_80c_status = $scope.validation('deduction_80c', '');
		var nps_status = $scope.validation('nps',nps);
		var mi_your_status = $scope.validation('mi_your',mi_your);
		var mi_parent_status = $scope.validation('mi_parent',mi_parent);
		var mc_your_status = $scope.validation('mc_your',mc_your);
		var education_loan_status = $scope.validation('education_loan',education_loan);
		console.log(emp_state_status);
		console.log(financial_year_status);
		console.log(jd_ld_status);
		console.log(m_bs_status);
		console.log(current_city_status);
		console.log(hra_status);
		console.log(parent_age_status);
		console.log(children_value_status);
		console.log(incurred_travel_expense_status);
		console.log(home_loan_taken_status);
		console.log(deduction_80c_status);
		console.log(nps_status);
		console.log(mi_your_status);
		console.log(mi_parent_status);
		console.log(mc_your_status);
		console.log(education_loan_status);
		var hereby = get_hereby();
		if(emp_state_status == true && financial_year_status == true && jd_ld_status == true && m_bs_status == true &&
			current_city_status == true && hra_status == true && parent_age_status == true && 
			children_value_status == true && incurred_travel_expense_status == true && home_loan_taken_status ==true &&
			deduction_80c_status == true && nps_status == true && mi_your_status == true && mi_parent_status ==true &&
			mc_your_status == true && education_loan_status == true && hereby=='yes'){
			var working_since = get_working_since();
			if($scope.user.doj1 != undefined)
				var doj1 = $scope.user.doj1;
			else
				var doj1 = '';
			if($scope.user.doj2 != undefined)
				var doj2 = $scope.user.doj2;
			else
				var doj2 = '';
			if($scope.user.doj3 != undefined)
				var doj3 = $scope.user.doj3;
			else
				var doj3 = '';
			if($scope.user.ld1 != undefined)
				var ld1 = $scope.user.ld1;
			else
				var ld1 = '';
			if($scope.user.ld2 != undefined)
				var ld2 = $scope.user.ld2;
			else
				var ld2 = '';
			if(working_since=='yes')
				doj1 = '01/04/2018';
			var bs1 = $scope.user.m_bs_comp1;
			var bs2 = $scope.user.m_bs_comp2;
			var bs3 = $scope.user.m_bs_comp3;
			var hra1 = $scope.user.hra_comp1;
			var hra2 = $scope.user.hra_comp2;
			var hra3 = $scope.user.hra_comp3;
			var pass_obj = { "emp_state":emp_state };
			pass_obj["financial_year"] = financial_year;
			pass_obj["doj1"] = doj1;
			pass_obj["doj2"] = doj2;
			pass_obj["doj3"] = doj3;
			pass_obj["ld1"] = ld1;
			pass_obj["ld2"] = ld2;
			pass_obj["bs1"] = bs1;
			pass_obj["bs2"] = bs2;
			pass_obj["bs3"] = bs3;
			pass_obj["current_city"] = current_city;
			if(get_comp_given_hra() == 'no'){
				pass_obj["hra1"] = '';
				pass_obj["hra2"] = '';
				pass_obj["hra3"] = '';
			}else{
				pass_obj["hra1"] = hra1;
				pass_obj["hra2"] = hra2;
				pass_obj["hra3"] = hra3;
			}
			pass_obj["parent_age"] = parent_age;
			pass_obj["children_value"] = children_value;
			pass_obj["type_c1"] = $scope.user.type_c1;
			pass_obj["type_c2"] = $scope.user.type_c2;
			pass_obj["type_c3"] = $scope.user.type_c3;
			pass_obj["type_c4"] = $scope.user.type_c4;
			pass_obj["dob_c1"] = $scope.user.dob_c1;
			pass_obj["dob_c2"] = $scope.user.dob_c2;
			pass_obj["dob_c3"] = $scope.user.dob_c3;
			pass_obj["dob_c4"] = $scope.user.dob_c4;
			pass_obj["state_of_accommodation"] = $scope.user.state_of_accommodation;
			pass_obj["rent_paid"] = parseInt($scope.user.rent_paid)*12;
			pass_obj["incurred_travel_expense"] = incurred_travel_expense
			pass_obj["total_travel_expense"] = $scope.user.total_travel_expense;
			pass_obj["home_loan_taken"] = home_loan_taken;
			pass_obj["principal_amt"] = $scope.user.principal_amt;
			pass_obj["interest_amt"] = $scope.user.interest_amt;
			pass_obj["epf"] = $scope.user.epf;
			pass_obj["school_fee"] = $scope.user.school_fee;
			pass_obj["lic"] = $scope.user.lic;
			pass_obj["elss"] = $scope.user.elss;
			pass_obj["ulip"] = $scope.user.ulip;
			pass_obj["fd"] = $scope.user.fd;
			pass_obj["ppf"] = $scope.user.ppf;
			pass_obj["other_80c"] = $scope.user.other_80c;
			pass_obj["nsc"] = $scope.user.nsc;
			pass_obj["sukanya"] = $scope.user.sukanya;
			pass_obj["post_office_deposit"] = $scope.user.post_office_deposit;
			pass_obj["r_principal_home_loan"] = $scope.user.r_principal_home_loan;
			pass_obj["stamp_duty"] = $scope.user.stamp_duty;
			pass_obj["nps"] = $scope.user.nps;
			pass_obj["mi_your"] = $scope.user.mi_your;
			pass_obj["mi_parent"] = $scope.user.mi_parent;
			pass_obj["mc_your"] = $scope.user.mc_your;
			pass_obj["education_loan"] = $scope.user.education_loan;
			pass_obj["Disibility"] = get_Disibility();
			// console.log(pass_obj);
			var data_pass = JSON.stringify(pass_obj);
			$.ajax({
				url:'/save_tax_plan/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'data_pass':data_pass},
				success: function(response){
					console.log(response);
					window.location.href = "/tax_recom";
				}
			});
		}else{
			if(hereby=='no')
				check_dec_error();
			else{
				$scope.common_err = 'Please fill all mandatory fields.'
				// alert('Error');
				// $scope.$apply();
			}
		}
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return 0

		if(!isNaN(x)){
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.check_date_validation=function(value){
		if(value!=undefined && value!='' && value!='null/undefined/undefined') {
			check_date=value.split('/');
			date=check_date[0];
			month=check_date[1];
			year=check_date[2];

			var dateCheck = /^(0?[1-9]|[12][0-9]|3[01])$/;
			var monthCheck = /^(0[1-9]|1[0-2])$/;
			// var yearCheck = /^\d{4}$/;
			var yearCheck =/^\b(19|[2-9][0-9])\d{2}$/;
			if (month.match(monthCheck) && date.match(dateCheck) && year.match(yearCheck)) {
				var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
				if (month == 1 || month > 2) {
					if (date > ListofDays[month - 1])
						return false;
				}
				if (month == 2) {
					var leapYear = false;
					if ((!(year % 4) && year % 100) || !(year % 400))
						leapYear = true;
					if ((leapYear == false) && (date >= 29))
						return false;
					if ((leapYear == true) && (date > 29))
						return false;
				}
				return true;
			}
			else
				return false;
		}
	}

	$scope.get_tax_plan = function(){
		$.ajax({
			url:'/get_tax_plan/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				$scope.user.financial_year = response.financial_year.toString();
				get_company(response.financial_year.toString());
				$scope.company_change();
				if(response.financial_year > 0){
					set_yes_no_value('working_since');
					if(response.m_hra[response.financial_year - 1]!='')
						set_yes_no_value('comp_given_hra');
				}
				for (var i = 1; i <= response.financial_year; i++) {
					if(i == 1){
						$scope.user.m_bs_comp1 = response.m_basic_salary[0];
						$scope.user.hra_comp1 = response.m_hra[0];
						$scope.user.doj1 = response.start_date[0];
						$scope.check_monthly_bs('1');
					}
					if(i == 2){
						$scope.user.m_bs_comp2 = response.m_basic_salary[1];
						$scope.user.hra_comp2 = response.m_hra[1];
						$scope.user.doj2 = response.start_date[1];
						$scope.user.ld1 = response.end_date[0];
						$scope.check_monthly_bs('2');
					}
					if(i == 3){
						$scope.user.m_bs_comp3 = response.m_basic_salary[2];
						$scope.user.hra_comp3 = response.m_hra[2];
						$scope.user.doj3 = response.start_date[2];
						$scope.user.ld2 = response.end_date[1];
						$scope.check_monthly_bs('1');
					}
				}
				$scope.user.current_city = response.current_city;
				$scope.user.employment_state = response.emp_state;
				$scope.user.state_of_accommodation = response.state_of_accommodation;
				if(response.state_of_accommodation == 'Rented')
					if(!isNaN(parseInt(response.rent_paid)))
						$scope.user.rent_paid = parseInt(response.rent_paid)/12;
				get_accommodation(response.state_of_accommodation);
				if(response.parent_age!='' && response.parent_age!='NA')
					$scope.user.parent_age = parseInt(response.parent_age);
				
				$scope.user.children_value = response.no_of_child;
				no_children_value1(response.no_of_child);
				if(response.no_of_child != 'NA' && response.no_of_child != '')
					set_yes_no_value('children_value');
				for (var i = 0; i < parseInt(response.no_of_child); i++) {
					if(i == 0){
						$scope.user.type_c1 = response.child_type[i];
						$scope.user.dob_c1 = response.child_dob[i];
					}
					if(i == 1){
						$scope.user.type_c2 = response.child_type[i];
						$scope.user.dob_c2 = response.child_dob[i];
					}
					if(i == 2){
						$scope.user.type_c3 = response.child_type[i];
						$scope.user.dob_c3 = response.child_dob[i];
					}
					if(i == 3){
						$scope.user.type_c4 = response.child_type[i];
						$scope.user.dob_c4 = response.child_dob[i];
					}
				}
				
				if(response.total_travel_expense != 'NA')
					$scope.user.total_travel_expense = parseInt(response.total_travel_expense);
				if(response.total_travel_expense != 'NA' && response.total_travel_expense != '')
					set_yes_no_value('incurred_travel_expense');
				if(response.principal_amt != 'NA')
					$scope.user.principal_amt = parseInt(response.principal_amt);
				if(response.interest_amt != 'NA')
					$scope.user.interest_amt = parseInt(response.interest_amt);
				if(response.principal_amt != 'NA' && response.interest_amt != 'NA' && response.interest_amt != '')
					set_yes_no_value('home_loan_taken');
				if(response.elss == '')
					$scope.user.elss = 0;
				else
					$scope.user.elss = response.elss;
				if(response.epf == '')
					$scope.user.epf = 0;
				else
					$scope.user.epf = response.epf;
				if(response.fd == '')
					$scope.user.fd = 0;
				else
					$scope.user.fd = response.fd;
				if(response.school_fee == '')
					$scope.user.school_fee = 0;
				else
					$scope.user.school_fee = response.school_fee;
				if(response.ppf == '')
					$scope.user.ppf = 0;
				else
					$scope.user.ppf = response.ppf;
				if(response.lic == '')
					$scope.user.lic = 0;
				else
					$scope.user.lic = response.lic;
				if(response.ulip == '')
					$scope.user.ulip = 0;
				else
					$scope.user.ulip = response.ulip;
				if(response.nsc == '')
					$scope.user.nsc = 0;
				else
					$scope.user.nsc = response.nsc;
				if(response.sukanya == '')
					$scope.user.sukanya = 0;
				else
					$scope.user.sukanya = response.sukanya;
				if(response.post_office_deposit == '')
					$scope.user.post_office_deposit = 0;
				else
					$scope.user.post_office_deposit = response.post_office_deposit;
				if(response.r_principal_home_loan == '')
					$scope.user.r_principal_home_loan = 0;
				else
					$scope.user.r_principal_home_loan = response.r_principal_home_loan;
				if(response.stamp_duty == '')
					$scope.user.stamp_duty = 0;
				else
					$scope.user.stamp_duty = response.stamp_duty;
				if(response.nps == '')
					$scope.user.nps = 0;
				else
					$scope.user.nps = response.nps;
				if(response.education_loan == '')
					$scope.user.education_loan = 0;
				else
					$scope.user.education_loan = response.education_loan;
				if(response.mi_your == '')
					$scope.user.mi_your = 0;
				else
					$scope.user.mi_your = response.mi_your;
				if(response.mi_parent == '')
					$scope.user.mi_parent = 0;
				else
					$scope.user.mi_parent = response.mi_parent;
				if(response.mc_your == '')
					$scope.user.mc_your = 0;
				else
					$scope.user.mc_your = response.mc_your;
				if(response.disability == 'yes')
					set_yes_no_value('disability');

				$scope.$apply();
				if(response.no_of_child!=null && response.no_of_child!='')
					$scope.validation('children_value',response.no_of_child);
				$scope.calculate_total_80c();
				$scope.check_monthly_hra(response.financial_year.toString());
				$scope.$apply();

				//css on empty data
				// console.log($scope.user.current_city);
				if($scope.user.current_city==''){
					$('.do_son_dob').addClass('current_age_css_empty_first_city');
					$('.current_age_css').addClass('current_age_css_data');
					$('.current_age_css').removeClass('current_age_css_empty_first');
					$('.current_age_css').removeClass('current_age_css_empty');
					$("#myonoffswitch4").on('change', function() {
			        if ($(this).is(':checked')) {
			            $(this).attr('value', 'true');
			            $('#current_state').hide();
			            $('#annual_rent').hide();
			            $('#company_hra').hide();
			            house_owned_by_you = 'yes';
			            $('.re_acc_css').addClass('re_acc_css_on');
			            $('.house_css').addClass('house_css_on_fill');
			            $('.re_acc_css').removeClass('re_acc_css_b');
			            console.log('true')
			        }
			        else {
			            $(this).attr('value', 'false');
			            $('#current_state').show();
			            $('#family_data').show();
			            if(accommodation == 'Rented' || accommodation == 'Family_Relatives_Friends')
			                $('#company_hra').show();
			            if(accommodation == 'Rented')
			                $('#annual_rent').show();
			            $('.re_acc_css').removeClass('re_acc_css_on');
			            $('.house_css').removeClass('house_css_on');
			            $('.house_css').removeClass('house_css_on_fill');
			            $('.re_acc_css').addClass('empty_all');
			            house_owned_by_you = 'no';
			            console.log('false');
			        }
			    	});
				}
			}
		});
	}

	$scope.calculate_total_80c =function(){
		var epf = 0;
		var school_fee = 0;
		var lic = 0;
		var elss = 0;
		var ulip = 0;
		var fd = 0;
		var ppf = 0;
		var other_80c = 0;
		var nsc = 0;
		var sukanya = 0;
		var post_office_deposit = 0;
		var r_principal_home_loan = 0;
		var stamp_duty = 0;

		if($scope.user.epf != '')
			epf = parseInt($scope.user.epf);
		else
			$scope.user.epf = 0;
		if($scope.user.school_fee != '')
			school_fee = parseInt($scope.user.school_fee);
		else
			$scope.user.school_fee = 0;
		if($scope.user.lic != '')
			lic = parseInt($scope.user.lic);
		else
			$scope.user.lic = 0;
		if($scope.user.elss != '')
			elss = parseInt($scope.user.elss);
		else
			$scope.user.elss = 0;
		if($scope.user.ulip != '')
			ulip = parseInt($scope.user.ulip);
		else
			$scope.user.ulip = 0;
		if($scope.user.fd != '')
			fd = parseInt($scope.user.fd);
		else
			$scope.user.fd = 0;
		if($scope.user.ppf != '')
			ppf = parseInt($scope.user.ppf);
		else
			$scope.user.ppf = 0;
		if($scope.user.other_80c != '')
			other_80c = parseInt($scope.user.other_80c);
		else
			$scope.user.other_80c = 0;
		if($scope.user.nsc != '')
			nsc = parseInt($scope.user.nsc);
		else
			$scope.user.nsc = 0;
		if($scope.user.sukanya != '')
			sukanya = parseInt($scope.user.sukanya);
		else
			$scope.user.sukanya = 0;
		if($scope.user.post_office_deposit != '')
			post_office_deposit = parseInt($scope.user.post_office_deposit);
		else
			$scope.user.post_office_deposit = 0;
		if($scope.user.r_principal_home_loan != '')
			r_principal_home_loan = parseInt($scope.user.r_principal_home_loan);
		else
			$scope.user.r_principal_home_loan = 0;
		if($scope.user.stamp_duty != '')
			stamp_duty = parseInt($scope.user.stamp_duty);
		else
			$scope.user.stamp_duty = 0;
		var home_loan_principal = 0;
		if(get_home_loan_taken() == 'yes')
			if(!isNaN($scope.user.principal_amt))
				home_loan_principal = parseInt($scope.user.principal_amt);
		$scope.total_80c = $scope.money_format(epf+school_fee+lic+elss+ulip+fd+ppf+other_80c+
			home_loan_principal+nsc+sukanya+post_office_deposit+r_principal_home_loan+stamp_duty);
	}

	$scope.cal_annual_rent = function(){
		$scope.total_rent = $scope.user.rent_paid*12;
	}

// https://mf-account.in/411037.php
}]);