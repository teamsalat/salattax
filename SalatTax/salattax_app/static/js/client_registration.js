
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('client_registration_controller', ['$scope', function($scope,$http,CONFIG,$location) {
	var client_id = 1;
	var email = 'jagrutikadam15@gmail.com';
	$scope.isDisabled = false;
	$scope.isDisabled1 = false;
	$scope.isDisabled_26AS = false;
	$scope.salat_registration_style = { "display" : "block" };
    $scope.efiling_registration_style = { "display" : "block" };
    $scope.captcha_url = '../static/images/family.png';
    // $scope.captcha_image_src = '../static/images/family.png'+ new Date().getTime();
    // $scope.image = {src:"/home/salat/Downloads/images_captcha/EGRPK2447C_captcha.png"};

	$scope.validation_text = function(type,value,$event) {
		//salat registration
		if(type == "salat_mail_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "salat_mail_otp"){
					$scope.otperr= "";
					return true;
				}
			}else{
				if(type == "salat_mail_otp"){
					$scope.otperr= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "salat_mail_otp") && value==undefined){
			if(type == "salat_mail_otp"){
				$scope.otperr= "Please Check Your E-mail OTP";
				return false;
			}
		}
		if(type == "salat_mobile_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "salat_mobile_otp"){
					$scope.otperr= "";
					return true;
				}
			}else{
				if(type == "salat_mobile_otp"){
					$scope.otperr= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "salat_mobile_otp") && value==undefined){
			if(type == "salat_mobile_otp"){
				$scope.otperr= "Please Check Your Mobile OTP";
				return false;
			}
		}
		if(type == "efiling_mail_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "efiling_mail_otp"){
					$scope.otp1err= "";
					return true;
				}
			}else{
				if(type == "efiling_mail_otp"){
					$scope.otp1err= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "efiling_mail_otp") && value==undefined){
			if(type == "efiling_mail_otp"){
				$scope.otp1err= "Please Check Your E-mail OTP";
				return false;
			}
		}
		if(type == "efiling_mobile_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "efiling_mobile_otp"){
					$scope.otp1err= "";
					return true;
				}
			}else{
				if(type == "efiling_mobile_otp"){
					$scope.otp1err= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "efiling_mobile_otp") && value==undefined){
			if(type == "efiling_mobile_otp"){
				$scope.otp1err= "Please Check Your Mobile OTP";
				return false;
			}
		}
		if(type == "captcha_26AS"  && value!==undefined){
			var text_length=value.length;
			if(text_length != ''){
				if(type == "captcha_26AS"){
					$scope.captcha_err= "";
					return true;
				}
			}else{
				if(type == "captcha_26AS"){
					$scope.captcha_err= "Please Enter Captcha";
					return false;
				}
			}
		}else if((type == "captcha_26AS") && value==undefined){
			if(type == "captcha_26AS"){
				$scope.captcha_err= "Please Check Entered Captcha";
				return false;
			}
		}
		$scope.$apply();
	}
	var start = 0;

	$scope.$watch("demoInput", function(){
		console.log('ID : '+$scope.user.id);
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				if (response.client_id!=0) {
					client_id = response.client_id;
					email = response.email;
					$scope.get_client_registration();
				}
				else if ($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				if ($scope.user.id != '') {
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						data:{'username':$scope.user.id},
						success: function(response){
							console.log(response);
							client_id = response.client_id;
							email = response.email;
							$scope.get_client_registration();
						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
			}
		});
		// console.log($scope.user);
	    /////// add hidden input field for all required data ///////
	},100);

	$scope.validationEfiling = function() {
		// tushar.patil23692@gmail.com
		var status_validation = true;
		var data=JSON.stringify($scope.user);
		$.ajax({
			url:'/register_user_otp/',
			type:'POST',
			dataType: 'json',
			data:{'data':data,'mail':email},
			success: function(response){
				console.log(response);
				status = JSON.stringify(response['status']).replace(/\"/g, "");
				if(status == 'fail'){
					$scope.otp1err = 'Please try again';
					status_validation = false;
				}
				if(status == "Invalid Mail OTP"){
					$scope.otp1err = 'Invalid OTP. Please retry';
					status_validation = false;
				}
				if(status == 'Invalid Mobile OTP'){
					$scope.otp1err = 'Invalid OTP. Please retry';
					status_validation = false;
				}
				if(status == 'Enter OTP'){
					$scope.otp1err = 'Please Enter OTP and Submit';
					status_validation = false;
				}
				if(status == 'Client Added'){
					$.ajax({
						url:'/update_isSalat/',
						type:'POST',
						dataType: 'json',
						data:{'pan':$scope.user.pan},
						success: function(response){
							console.log(response);
						}
					});
					$scope.otp1err = '';
					$scope.salat_registration_style = {
						"display" : "block"
					}
					$scope.efiling_registration_style = {
						"display" : "none"
					}
				}
				$scope.$apply() ;
				if(status_validation == false){
					$scope.isDisabled1 = false;
				}
			}
		});
	}
	$scope.validation = function() {
		var data = JSON.stringify($scope.user);
		// alert($scope.user.pan);
		var status_validation = true;
		$.ajax({
			url:'/check_isSalat/',
			type:'POST',
			dataType: 'json',
			data:{'pan':$scope.user.pan},
			success: function(response){
				console.log(response);
				// alert(response.entry_exist);
				if (response.entry_exist == 1) {
					$scope.pan_exist = 'This PAN is already added as Salat Client';
					$scope.salat_registration_style = { "display" : "none" };
				}
				else{
					$.ajax({
						url:'/selenium_addclient/',
						type:'POST',
						dataType: 'json',
						data:{'data':data, 'pan':$scope.user.pan},
						success: function(response){
							console.log(response);
							status = JSON.stringify(response['status']).replace(/\"/g, "");
							if(status == 'fail'){
								$scope.otperr = 'Please try again';
								status_validation = false;
							}
							if(status == 'Invalid Mail OTP'){
								$scope.otperr = 'Invalid OTP. Please retry';
								status_validation = false;
							}
							if(status == 'Invalid Mobile OTP'){
								$scope.otperr = 'Invalid OTP. Please retry';
								status_validation = false;
							}
							if(status == 'Enter OTP'){
								$scope.otperr = 'Please Enter OTP and Submit';
								status_validation = false;
							}
							if(status == 'Client Added'){
								$scope.otperr = '';
								$.ajax({
									url:'/update_isSalat/',
									type:'POST',
									dataType: 'json',
									data:{'pan':$scope.user.pan},
									success: function(response){
										console.log(response);
										window.location.href = redirect_url+'/client_registration/';
										window.location.reload();
									}
								});
							}
							if(status == 'PAN already added'){
								$.ajax({
									url:'/update_isSalat/',
									type:'POST',
									dataType: 'json',
									data:{'pan':$scope.user.pan},
									success: function(response){
										console.log(response);
									}
								});
								$scope.otperr = 'This PAN is already added as Salat Client';
								$scope.pan_exist = 'This PAN is already added as Salat Client';
								status_validation = false;
							}
							if(status == 'PAN not registered'){
								$scope.otperr = 'This PAN is not registered with e-filing';
								status_validation = false;
							}
							if(status == 'PAN does not exist'){
								$scope.otperr = 'This PAN does not exist';
								status_validation = false;
							}
							if(status_validation == false){
								$scope.isDisabled = false;
							}
						}
					});
				}
				$scope.$apply() ;
			}
		});
	}

	$scope.get_otp = function(user,$event) {
		$scope.isDisabled = true;
		start = start+1;
		var salat_mail_otp_status = $scope.validation_text("salat_mail_otp",$scope.user.salat_mail_otp);
		var salat_mobile_otp_status = $scope.validation_text("salat_mobile_otp",$scope.user.salat_mobile_otp);
		if (salat_mail_otp_status == false && salat_mobile_otp_status == true) {
			salat_mail_otp_status = $scope.validation_text("salat_mail_otp",$scope.user.salat_mail_otp);
		}
		var salat_mail_otp = user.salat_mail_otp;
		var salat_mobile_otp = user.salat_mobile_otp;
		var pan = user.pan;
		if(salat_mail_otp_status == true && salat_mobile_otp_status == true && $scope.otperr == ''){
			$.ajax({
				url:'/save_salat_otp/',
				type:'POST',
				dataType: 'json',
				data:{'salat_mail_otp':salat_mail_otp,'salat_mobile_otp':salat_mobile_otp,'pan':pan},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					if(status == 'success'){
						$scope.validation();
					}
				}
			});
		}else{
			$scope.isDisabled = false;
		}
	}
	$scope.get_otp1 = function(user,pan,name,dob,$event) {
		$scope.isDisabled1 = true;
		var efiling_mail_otp_status = $scope.validation_text("efiling_mail_otp",$scope.user.efiling_mail_otp);
		var efiling_mobile_otp_status = $scope.validation_text("efiling_mobile_otp",$scope.user.efiling_mobile_otp);
		if (efiling_mail_otp_status == false && efiling_mobile_otp_status == true) {
			efiling_mail_otp_status = $scope.validation_text("efiling_mail_otp",$scope.user.efiling_mail_otp);
		}
		var efiling_mail_otp = $scope.user.efiling_mail_otp;
		var efiling_mobile_otp = $scope.user.efiling_mobile_otp;
		if(efiling_mail_otp_status == true && efiling_mobile_otp_status == true && $scope.otp1err == ''){
			$.ajax({
				url:'/save_efiling_otp/',
				type:'POST',
				dataType: 'json',
				data:{'efiling_mail_otp':efiling_mail_otp,'efiling_mobile_otp':efiling_mobile_otp,'pan':pan},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					if(status == 'success'){
						$scope.otp1err = '';
						$scope.validationEfiling();
					}
				}
			});
		}else{
			$scope.isDisabled1 = false;
		}
	}

	$scope.get_26AS = function(user,$event) {
		$scope.isDisabled_26AS = true;
		var pan = $scope.user.pan;
		console.log(pan);
		// alert('26 AS');
		var data=JSON.stringify($scope.user);
		/*$.ajax({
			url:'/excelToDB/',
			type:'POST',
			dataType: 'json',
			data:{'data':data},
			success: function(response){
				console.log(response);
				status = JSON.stringify(response['status']).replace(/\"/g, "");
			}
		});*/
		$.ajax({
			url:'/get_form_26AS/',
			type:'POST',
			dataType: 'json',
			data:{'data':data},
			success: function(response){
				// clearInterval(set_captcha_img);
				$scope.isDisabled_26AS = false;
				console.log(response);
				status = JSON.stringify(response['status']).replace(/\"/g, "");
				if (status == 'success') {
					$.ajax({
						url:'/unzip_txt/',
						type:'POST',
						dataType: 'json',
						data:{'data':data},
						success: function(response){
							console.log(response);
							status = JSON.stringify(response['status']).replace(/\"/g, "");
							if (status == 'success') {
								$.ajax({
									url:'/excelToDB/',
									type:'POST',
									dataType: 'json',
									data:{'data':data,'client_id':client_id},
									success: function(response){
										console.log(response);
										status = JSON.stringify(response['status']).replace(/\"/g, "");
									}
								});
							}
						}
					});
				}
			}
		});

		/*setTimeout(function(){
			var img_path = '/static/captcha/'+user.pan+'_captcha.png';
			set_captcha_img = setInterval(check_captcha_img, 12000); //every 12sec

			function check_captcha_img() {
				var xhr = new XMLHttpRequest();
				xhr.open('HEAD', img_path, false);
				xhr.send();
				if (xhr.status == "404") {
					// alert(xhr.status);
				}else {
					// alert(img_path);
					$scope.user.captcha_url = '/static/captcha/'+ $scope.user.pan +'_captcha.png?'+ new Date().getTime();
					$scope.$apply();
					document.getElementById('captcha_img').click();
					clearInterval(set_captcha_img);
				}
			}
		}, 30000);*/
	}

	$scope.save_captcha = function(user,$event) {
		var pan = user.pan;
		var captcha_26AS = $scope.user.captcha_26AS;
		var captcha_26AS_status = $scope.validation_text("captcha_26AS",$scope.user.captcha_26AS);
		// alert(captcha_26AS);
		if(captcha_26AS != '' && captcha_26AS_status==true){
			$.ajax({
				url:'/save_captcha_26AS/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'captcha_26AS':captcha_26AS},
				success: function(response){
					// console.log(response);
					setTimeout(function(){
						$.ajax({
							url:'/read_captcha_status/',
							type:'POST',
							dataType: 'json',
							data:{'pan':pan},
							success: function(response){
								console.log(response);
								// console.log('read_captcha_status');
								status = JSON.stringify(response['status']).replace(/\"/g, "");
								content = JSON.stringify(response['content']).replace(/\"/g, "");
								if (content == 'wrong') {
									$scope.captcha_err= "Text entered does not match with image data. Please Retry.";
									$scope.$apply();
								}
								if (content == 'correct') {
									$scope.captcha_err= "File Downloaded.";
									$scope.$apply();
									document.getElementById('close_model').click();
								}
							}
						});
					}, 4000);
				}
			});
		}
	}
	$scope.refresh_captcha = function(user,$event) {
		var pan = user.pan;
		$.ajax({
			url:'/refresh_status/',
			type:'POST',
			dataType: 'json',
			data:{'pan':pan},
			success: function(response){
				console.log('refresh_captcha');
				// console.log(response);
				setTimeout(function(){
					$scope.user.captcha_url = '/static/captcha/'+ $scope.user.pan +'_captcha.png?'+ new Date().getTime();
					$scope.$apply();
				}, 3000);
				setTimeout(function(){
					$scope.user.captcha_url = '/static/captcha/'+ $scope.user.pan +'_captcha.png?'+ new Date().getTime();
					$scope.$apply();
				}, 5000);
			}
		});
	}

	$scope.get_client_registration = function(){
		$.ajax({
			url:'/get_client_registration/',
			type:'POST',
			dataType: 'json',
			data:{'mail':email},
			success: function(response){
				// console.log(response);
				var myobj = JSON.parse(response);
				$scope.user.registered_pan = JSON.stringify(myobj['registered_pan']).replace(/\"/g, "");
				$scope.user.first_name = JSON.stringify(myobj['pan1_first_name']).replace(/\"/g, "");
				$scope.user.middle_name = JSON.stringify(myobj['pan1_middle_name']).replace(/\"/g, "");
				$scope.user.last_name = JSON.stringify(myobj['pan1_last_name']).replace(/\"/g, "");
				$scope.user.dob = JSON.stringify(myobj['pan1_dob']).replace(/\"/g, "");
				$scope.user.pan = JSON.stringify(myobj['pan1']).replace(/\"/g, "");
				$scope.user.mobile = JSON.stringify(myobj['mobile']).replace(/\"/g, "");
				$scope.user.flatno = JSON.stringify(myobj['pan1_flatno']).replace(/\"/g, "");
				$scope.user.premise = JSON.stringify(myobj['pan1_premise']).replace(/\"/g, "");
				$scope.user.road = JSON.stringify(myobj['pan1_road']).replace(/\"/g, "");
				$scope.user.area = JSON.stringify(myobj['pan1_area']).replace(/\"/g, "");
				$scope.user.pin = JSON.stringify(myobj['pan1_pin']).replace(/\"/g, "");
				$scope.user.city = JSON.stringify(myobj['pan1_city']).replace(/\"/g, "");
				$scope.user.Residential_status = JSON.stringify(myobj['Residential_status']).replace(/\"/g, "");
				$scope.user.gender = JSON.stringify(myobj['pan1_gender']).replace(/\"/g, "");
				$scope.user.pan1_gender = JSON.stringify(myobj['pan1_gender']).replace(/\"/g, "");
				$scope.user.Email = JSON.stringify(myobj['Email']).replace(/\"/g, "");

				$scope.user.captcha_url = '/static/captcha/'+ $scope.user.pan +'_captcha.png?'+ new Date().getTime();
				if ($scope.user.registered_pan == 'YES') {
					$scope.validation();
					$scope.efiling_registration_style = {
						"display" : "none"
					}
				}
				if ($scope.user.registered_pan == 'NO') {
					$scope.salat_registration_style = {
						"display" : "none"
					}
				}
				$scope.$apply() ;
				// console.log($scope.user);
			}
		});
	}

}]);



