var mapp = angular.module('partner_app', ['naif.base64']);

mapp.config(function ($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});


mapp.controller('partner_onboard', function($rootScope,$scope,$http,$parse,$sce,$compile,$window) {

    $('.loader').hide();

    var eamil_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var mobile_re = /^[6789]\d{9}$/;
    var pan_re = /[A-Z]{5}\d{4}[A-Z]{1}/;
    var pincode_re = /^\d{6}$/;
    var ifsc_re = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;

    $rootScope.bankProofObj='';
    $rootScope.partner_business_error='';
    $rootScope.primary_business_error='';
    $rootScope.pan_error='';
    $rootScope.name_error='';
    $rootScope.email_error='';
    $rootScope.mobile_error='';
    $rootScope.address_error='';
    $rootScope.pincode_error='';
    $rootScope.city_error='';
    $rootScope.state_error='';
    $rootScope.user_name_error='';
    $rootScope.password_error='';
    $rootScope.confirm_password_error='';
    $rootScope.ifsc_error='';
    $rootScope.account_no_error='';
    $rootScope.account_type_error='';
    $rootScope.bank_holder_name_error='';
    $rootScope.bank_proof_error='';
    $rootScope.check_agree_error ='';
    $rootScope.onboard_partner_error ='';

    $('.partner_business_error_cl').hide();
    $('.primary_business_error_cl').hide();
    $('.pan_error_cl').hide();
    $('.name_error_cl').hide();
    $('.email_error_cl').hide();
    $('.mobile_error_cl').hide();
    $('.address_error_cl').hide();
    $('.pincode_error_cl').hide();
    $('.city_error_cl').hide();
    $('.state_error_cl').hide();
    $('.user_name_error_cl').hide();
    $('.password_error_cl').hide();
    $('.confirm_password_error_cl').hide();
    $('.ifsc_error_cl').hide();
    $('.account_no_error_cl').hide();
    $('.account_type_error_cl').hide();
    $('.bank_holder_name_error_cl').hide();
    $('.bank_proof_error_cl').hide();
    $('.check_agree_error_cl').hide();

    $scope.InputValidation = function(type,value){

      validation = false

      //console.log(value)

      if(type=='partner_business'){

        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.partner_business_error_cl').show();
          $scope.partner_business_color="text_color_danger";
          $rootScope.partner_business_error='Please enter business name';
          validation=false
        }else{
          $('.partner_business_error_cl').hide();
          $scope.partner_business_color="text_color_success";
          $rootScope.partner_business_error='';
          validation=true
        }

      }else if(type=='primary_business'){

        if(value=='0' || value=='' || value==undefined){
          $('.primary_business_error_cl').show();
          $scope.primary_business_color="text_color_danger";
          $rootScope.primary_business_error='Please select primary business';
          validation=false
        }else{
          $('.primary_business_error_cl').hide();
          $scope.primary_business_color="text_color_success";
          $rootScope.primary_business_error='';
          validation=true
        }

      }else if(type=='name'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value==''){
          $('.name_error_cl').show();
          $scope.name_color="text_color_danger";
          $rootScope.name_error='Please enter name';
          validation=false
        }else{
          $('.name_error_cl').hide();
          $scope.name_color="text_color_success";
          $rootScope.name_error='';
          validation=true
        }

      }else if(type=='address'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.address_error_cl').show();
          $scope.address_color="text_color_danger";
          $rootScope.address_error='Please enter address';
          validation=false
        }else{
          $('.address_error_cl').hide();
          $scope.address_color="text_color_success";
          $rootScope.address_error='';
          validation=true
        }

      }else if(type=='pincode'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(pincode_re.test(value)){
          $('.pincode_error_cl').hide();
          $scope.pincode_color="text_color_success";
          $rootScope.pincode_error='';
          validation=true
        }else{
          $('.pincode_error_cl').show();
          $scope.pincode_color="text_color_danger";
          $rootScope.pincode_error='Please enter valid pincode';
          validation=false
        }

      }else if(type=='city'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.city_error_cl').show();
          $scope.city_color="text_color_danger";
          $rootScope.city_error='Please enter city';
          validation=false
        }else{
          $('.city_error_cl').hide();
          $scope.city_color="text_color_success";
          $rootScope.city_error='';
          validation=true
        }

      }else if(type=='state'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.state_error_cl').show();
          $scope.state_color="text_color_danger";
          $rootScope.state_error='Please enter state';
          validation=false
        }else{
          $('.state_error_cl').hide();
          $scope.state_color="text_color_success";
          $rootScope.state_error='';
          validation=true
        }

      }else if(type=='user_name'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.user_name_error_cl').show();
          $scope.user_name_color="text_color_danger";
          $rootScope.user_name_error='Please enter user name';
          validation=false
        }else{
          $('.user_name_error_cl').hide();
          $scope.user_name_color="text_color_success";
          $rootScope.user_name_error='';
          validation=true
        }

      }else if(type=='ifsc'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(ifsc_re.test(value)){
          $('.ifsc_error_cl').hide();
          $scope.ifsc_color="text_color_success";
          $rootScope.ifsc_error='';
          validation=true
        }else{
          $('.ifsc_error_cl').show();
          $scope.ifsc_color="text_color_danger";
          $rootScope.ifsc_error='Please enter valid ifsc';
          validation=false
        }

      }else if(type=='account_no'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.account_no_error_cl').show();
          $scope.account_no_color="text_color_danger";
          $rootScope.account_no_error='Please enter account no';
          validation=false
        }else{
          $('.account_no_error_cl').hide();
          $scope.account_no_color="text_color_success";
          $rootScope.account_no_error='';
          validation=true
        }

      }else if(type=='bank_holder_name'){
        if(value==undefined){
          value=''
        }
        value=value.trim();
        if(value=='' || value==undefined){
          $('.bank_holder_name_error_cl').show();
          $scope.bank_holder_name_color="text_color_danger";
          $rootScope.bank_holder_name_error='Please enter bank holder name';
          validation=false
        }else{
          $('.bank_holder_name_error_cl').hide();
          $scope.bank_holder_name_color="text_color_success";
          $rootScope.bank_holder_name_error ='';
          validation=true
        }

      }else if(type=='check_agree'){
        
        if(value==false){
          $('.check_agree_error_cl').show();
          $scope.check_agree_color="text_color_danger";
          $rootScope.check_agree_error='Please select agreement checkbox ';
          validation=false
        }else{
          $('.check_agree_error_cl').hide();
          $scope.check_agree_color="text_color_success";
          $rootScope.check_agree_error ='';
          validation=true
        }

      }

      return validation
    }

    $scope.pan_validation = function (pan){

      valid = false
      if(pan==undefined){
        pan=''
      }
      pan_length=pan.length;
      if(pan!=undefined && pan_length==10)
      {
        if(pan_re.test(pan))
        { 
          valid=true
        }
      }

      if(valid==false){
        $('.pan_error_cl').show();
        $scope.pan_color="text_color_danger";
        $rootScope.pan_error='Please enter valid pan';
      }else{
        $('.pan_error_cl').hide();
        $scope.primary_business_color="text_color_success";
        $rootScope.pan_error='';
      }

      return valid

    }

    $scope.eamil_validation = function (email){

      valid = false
      if(email==undefined){
        email=''
      }
      email=email.trim()
      if(email!='')
      {
        if(eamil_re.test(email))
        { 
          valid=true
        }
      }

      if(valid==false){
        $('.email_error_cl').show();
        $scope.email_color="text_color_danger";
        $rootScope.email_error='Please enter valid email';
      }else{
        $('.email_error_cl').hide();
        $scope.email_color="text_color_success";
        $rootScope.email_error='';
      }

      return valid

    }

    $scope.mobile_validation = function(mobile){

      valid = false
      if(mobile==undefined){
        mobile=''
      }
      mobile=mobile.trim()
      mobile_length=mobile.length;
      if(mobile_re.test(mobile) && mobile_length==10)
      {
        $('.mobile_error_cl').hide();
        $scope.mobile_color="text_color_success";
        $rootScope.mobile_error='';
        valid=true
      }else{
        $('.mobile_error_cl').show();
        $scope.mobile_color="text_color_danger";
        $rootScope.mobile_error='Please enter valid mobile no';
      }

      return valid
    }

    $scope.check_user_isexist = function (){

      var user_name = $scope.user.user_name;

      user_notexist=false
      failed=true

      if(user_name!=''){

        $.ajax({
          url:'/check_user_isexist/',
          type:'POST',
          dataType:"json",
          async:false,
          data:{'user_name':user_name},
          success:function (response) { 
                console.log(response)
              if(response['status']=="Success"){
                failed=false
                if(response['user_exist']==1){
                  $('.user_name_error_cl').show();
                  $scope.user_name_color="text_color_danger";
                  $rootScope.user_name_error='User name already exist';
                  console.log('User name already exist');
                  user_notexist=false
                }else if(response['user_exist']==0){
                  $('.user_name_error_cl').hide();
                  $scope.user_name_color="text_color_success";
                  $rootScope.user_name_error='';
                  console.log('User is not Exist');
                  user_notexist=true
                }
   
              }else{
                console.log('Error in check user name is exist');
              }
          },
          failure:function () {
            console.log('Error in check user name is exist');
          }
        });

      }else{
        console.log('Enter user name')
        user_notexist=false
      }

      if(user_notexist==false && failed==true){
        $('.user_name_error_cl').show();
        $scope.user_name_color="text_color_danger";
        $rootScope.user_name_error='Error in User name checking';
      }

      user_notexist=true

      return user_notexist

    }

    $scope.check_password_length = function (){

      var password = $("#password").val();

      valid_password=false
      console.log(password)
      console.log(password.length)

      if(password.length>=6){
        $('.password_error_cl').hide();
        $scope.password_color="text_color_success";
        $rootScope.password_error='';
        valid_password=true
      }else{
        $('.password_error_cl').show();
        $scope.password_color="text_color_danger";
        $rootScope.password_error='Password is too short';
        console.log('Password is too short')
        valid_password=false
      }

      return valid_password

    }

    $scope.compare_password = function (){

      var password = $("#password").val();
      var confirm_password = $("#confirm_password").val();

      valid_password=false

      if(confirm_password==undefined){
        confirm_password=''
      }

      if(confirm_password!=''){

        if(password==confirm_password){
          $('.confirm_password_error_cl').show();
          console.log('Password is matched')
          $scope.confirm_password_color="text_color_success";
          $rootScope.confirm_password_error='Password is matched';
          valid_password=true
        }else{
          $('.confirm_password_error_cl').show();
          console.log('Password does not match')
          $scope.confirm_password_color="text_color_danger";
          $rootScope.confirm_password_error='Password does not match';
          valid_password=false
        }

      }

      return valid_password

    }

    $scope.account_type_validation = function (){

      var account_type = $scope.user.account_type;
      valid = false

      if(account_type=='SB' || account_type=='C' || account_type=='CC'){
        $('.account_type_error_cl').hide();
        $scope.account_type_color="text_color_success";
        $rootScope.account_type_error='';
        valid = true
      }else{
        $('.account_type_error_cl').show();
        $scope.account_type_color="text_color_danger";
        $rootScope.account_type_error='Please select account type';
        valid=false
      }

      return valid

    }

    $scope.account_proof_onLoad = function (e, reader, file, fileList, fileOjects, fileObj){
      console.log('account_proof_onLoad');
      file_source='account_proof';
      console.log(e);
      console.log(reader);
      console.log(file);

      console.log(file['name']);

      file_name = file['name'];

      file_name=file_name.toLowerCase();
      console.log(file_name)

      file_format = file_name.split(".").pop();
      console.log(file_format)

      valid_format_list=['png','jpg','jpeg','tiff','tif']

      valid_format=valid_format_list.indexOf(file_format);
      console.log(valid_format)

      if(valid_format==-1){
        $('.bank_proof_error_cl').show();
        $scope.bank_proof_color="text_color_danger";
        $rootScope.bank_proof_error='Please select valid format file. (eg. png, jpg)';
      }else{
        $('.bank_proof_error_cl').hide();
        $scope.bank_proof_color="text_color_success";
        $rootScope.bank_proof_error='';
        $rootScope.bankProofObj=fileObj;
        //$scope.uploadFile($scope.user.pan,file_source,fileObj)
      }
   
      
    }

    /*$scope.uploadFile = function (pan,file_source,fileObj){
   
      console.log("uploadFile");

      if(/[A-Za-z]{5}\d{4}[A-Za-z]{1}/.test(pan)){

        console.log(pan);
        console.log(file_source);
        //console.log(fileObj);
        
        var data=JSON.stringify(fileObj);
        temp='';
        $('.loader').show();
        $.ajax({
          url:'/bulk_file_upload/',
          type:'POST',
          //async:false,
          dataType: 'json',
          data:{'data':data,'bpid':bp_id,'file_source':file_source},
          success: function(response){
            $('.loader').hide();

              if(response['status']=="Success"){
                $scope.CAMS_color="bulk_text_success";   
                $rootScope.cams_status='CAMS File uploaded Successfully';
                $scope.$apply();
                console.log($rootScope.cams_status);
              }else if(response['status']!=undefined && response['status']!=''){
                $scope.CAMS_color="bulk_text_danger";
                $rootScope.cams_status=response['status'];
                $scope.$apply();
                console.log($rootScope.cams_status);
              }else{
                $scope.CAMS_color="bulk_text_danger";
                $rootScope.cams_status='Error in CAMS File uploading';
                $scope.$apply();
                console.log($rootScope.cams_status);
              }


            
           
          },
          failure:function () {
            $('.loader').hide();
           
          } 
        })

      }else{
        console.log("Please Enter Valid Pan");
      }
      
    }*/


    $scope.AllDataValidation = function(user){
      console.log("AllDataValidation");

      Allvalidation = true

      //Business Name
      partner_business_resp=$scope.InputValidation('partner_business',$scope.user.partner_business)
      if(partner_business_resp==false){
        Allvalidation=partner_business_resp
      }

      console.log(Allvalidation)

      //Primary Business
      primary_business_resp=$scope.InputValidation('primary_business',$scope.user.primary_business)
      if(primary_business_resp==false){
        Allvalidation=primary_business_resp
      }

      console.log(Allvalidation)

      //Pan
      pan=$scope.user.pan
      panValidation=$scope.pan_validation(pan)

      if(panValidation==false){
        Allvalidation=panValidation
      }

      console.log(Allvalidation)

      //Name
      name_resp=$scope.InputValidation('name',$scope.user.name)
      if(name_resp==false){
        Allvalidation=name_resp
      }

      console.log(Allvalidation)

      //Confirm Password
      compare_password_resp=$scope.compare_password()
      if(compare_password_resp==false){
        Allvalidation=compare_password_resp
      }

      //Email
      email = $scope.user.email;
      email_resp=$scope.eamil_validation(email)
      if(email_resp==false){
        Allvalidation=email_resp
      }

      console.log(Allvalidation)

      //Mobile
      mobile = $scope.user.mobile;
      mobile_resp=$scope.mobile_validation(mobile)
      if(mobile_resp==false){
        Allvalidation=mobile_resp
      }

      console.log(Allvalidation)

      //Address
      address_resp=$scope.InputValidation('address',$scope.user.address)
      if(address_resp==false){
        Allvalidation=address_resp
      }

      console.log(Allvalidation)

      //Pincode
      pincode_resp=$scope.InputValidation('pincode',$scope.user.pincode)
      if(pincode_resp==false){
        Allvalidation=pincode_resp
      }

      console.log(Allvalidation)

      //City
      city_resp=$scope.InputValidation('city',$scope.user.city)
      if(city_resp==false){
        Allvalidation=city_resp
      }

      console.log(Allvalidation)

      //State
      state_resp=$scope.InputValidation('state',$scope.user.state)
      if(state_resp==false){
        Allvalidation=state_resp
      }

      console.log(Allvalidation)

      //User Name
      user_name_resp=$scope.InputValidation('user_name',$scope.user.user_name)
      if(user_name_resp==false){
        Allvalidation=user_name_resp
      }else{
        user_exist_resp=$scope.check_user_isexist();
        if(user_exist_resp==false){
          Allvalidation=user_exist_resp
        }
      }

      console.log(Allvalidation)

      //IFSC
      ifsc_resp=$scope.InputValidation('ifsc',$scope.user.ifsc)
      if(ifsc_resp==false){
        Allvalidation=ifsc_resp
      }

      console.log(Allvalidation)

      //Account No
      account_no_resp=$scope.InputValidation('account_no',$scope.user.account_no)
      if(account_no_resp==false){
        Allvalidation=account_no_resp
      }

      console.log(Allvalidation)

      //Account Type
      account_type_resp=$scope.account_type_validation()
      if(account_type_resp==false){
        Allvalidation=account_type_resp
      }

      console.log(Allvalidation)

      //Bank Holder Name
      bank_holder_name_resp=$scope.InputValidation('bank_holder_name',$scope.user.bank_holder_name)
      if(bank_holder_name_resp==false){
        Allvalidation=bank_holder_name_resp
      }

      console.log(Allvalidation)

      //Agreement Check box
      check_agree_resp=$scope.InputValidation('check_agree',$scope.user.check_agree)
      if(check_agree_resp==false){
        Allvalidation=check_agree_resp
      }

      console.log(Allvalidation)

      return Allvalidation

    };

    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    console.log('SignOut')
                }
            }
        });
    }

    $scope.onboard_partner = function(user){

        Allvalidation=$scope.AllDataValidation(user);

        if(Allvalidation==true){

          console.log($scope.user.partner_business);

          console.log("onboard_partner");

          var bankProofFile=JSON.stringify($rootScope.bankProofObj);

          var data = angular.copy(user);
          var data = alldata = JSON.stringify(data);

          $('.loader').show();
          $.ajax({
            url:'/partner_onboard/',
            type:'POST',
            dataType:"json",
            //async:false,
            data:{'data':data,'bank_proof':bankProofFile},
            success:function (response) { 
                console.log(response)
                if(response['status']=="Success"){
                  
                  if(response['partner_create']==1){

                    $.ajax({
                      url:'/update_password/',
                      type:'POST',
                      dataType: 'json',
                      data:{'username':$scope.user.user_name,'password':$scope.user.password},
                      success: function(response){
                        console.log(response);

                        $.ajax({
                          url:'/login_user/',
                          type:'POST',
                          dataType: 'json',
                          data:{'username':$scope.user.user_name,'password':$scope.user.password},
                          success: function(response){
                            $('.loader').hide();
                            console.log(response);
                            if(response['status']=="Success"){
                              $popup = window.location.href= '/partner_deposit/';
                            }
                          },error:function(response) {
                            $('.loader').hide();
                            console.log(response);
                          }
                        });

                      },error:function(response) {
                        $('.loader').hide();
                        console.log(response);
                      }
                    });
                    $scope.signOut();
                    swal('Partner Registered Successfully.');
                    //alert("Partner Registered Successfully");
                    $scope.onboard_partner_color="text_color_success";
                    $rootScope.onboard_partner_error='Partner Registered Successfully';
                  }else{
                    //alert('Partner not created');
                    $scope.onboard_partner_color="text_color_danger";
                    $rootScope.onboard_partner_error='Error in Partner Onboarding';
                    swal('Partner not created');
                    if(response['user_exist']==1){
                      swal('User name already exist');
                      //alert('User name already exist');
                    }else if(response['user_exist']==0){
                      console.log('User created Successfully');
                    }

                  }
                  
                }else if(response['status']=="Error"){
                  $('.loader').hide();
                  $scope.onboard_partner_color="text_color_danger";
                  $rootScope.onboard_partner_error='Error in Partner Onboarding';
                  swal('Error in Partner Registration');
                }else{
                  $('.loader').hide();
                  $scope.onboard_partner_color="text_color_danger";
                  $rootScope.onboard_partner_error='Error in Partner Onboarding';
                  swal('Error in Partner Registration');
                }
            },
            failure:function () {
              $('.loader').hide();
              swal('Error in Partner Onboarding');
              console.log('Error in Partner Registration');
            }
          });

        }else{
          console.log('Validation false')
          swal('Error in data, please check data');
        }


    };
   


});