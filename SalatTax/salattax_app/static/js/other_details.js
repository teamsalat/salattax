

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('other_details_controller', ['$scope','$compile', function($scope,$compile,CONFIG) {

	redirect_url = $scope.url;
	var client_id = 1;
	console.log('Client ID : '+$scope.user.client_id);
    client_id=$scope.user.client_id
	$scope.$watch("demoInput", function(){
    	console.log('ID : '+$scope.user.id);
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				if (response.client_id!=0 && $scope.user.id == '') {
					// client_id = response.client_id;
					$scope.get_other_details();
				}
				else if ($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				if ($scope.user.id != '') {
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						data:{'username':$scope.user.id},
						success: function(response){
							// console.log(response);
							// client_id = response.client_id;
							email = response.email;
							$scope.get_other_details();
						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
			}
		});
	},100);

	$scope.validation = function(type,value,$event) {
	  	//employer_category/Residential Status/ No of banks
		if((type == "employer_category" || type=="residential_status" || type=="noofbanks") && value!==undefined){
			if(value!='' && value!=null){
				if(type == "employer_category")
					$scope.employer_category_error= "";
				else if(type == "residential_status")
					$scope.residential_status_error= "";
				else if(type == "noofbanks"){
					$scope.noofbanks_error= "";
					if (value==0) {
						$scope.noofbanks_error= "Please Select No. of Banks";
						return false;
					}
				}
				return true;
			}else{
				if(type == "employer_category")
					$scope.employer_category_error= "Please Select Employer Category";
				else if(type == "residential_status")
					$scope.residential_status_error= "Please Select Residential Status";
				else if(type == "noofbanks")
					$scope.noofbanks_error= "Please Select No. of Banks";
				return false;
			}
		}else if((type == "employer_category" || type=="residential_status" || type=="noofbanks") && value==undefined){
			if(type == "employer_category")
				$scope.employer_category_error= "Employer Category is required";
			else if(type == "residential_status")
				$scope.residential_status_error= "Residential Status is required";
			else if(type == "noofbanks")
				$scope.noofbanks_error= "No. of Banks is required";
			return false;
		}

		//Income Tax Ward 
		if(type == "income_tax_ward" && value!==undefined){
			var income_tax_ward_length=value.length;
			if(income_tax_ward_length >= 3){ 
				$scope.income_tax_ward_error= "";
				return true;
			}else{
				$scope.income_tax_ward_error= "Please Enter Income Tax Ward";
				return false;
			}
		}else if(type == "income_tax_ward"  && value==undefined){
			$scope.income_tax_ward_error= "Income Tax Ward is required";
			return false;
		}

		//IFSC
		if((type == "od_ifsc1" || type=="od_ifsc2" || type=="od_ifsc3" || type=="od_ifsc4" || type=="od_ifsc5") && (value!==undefined)){
			var ifsc_length = value.length;
			if((/^\S+$/.test(value)) && ifsc_length ==11){
				if(type == "od_ifsc1"){
					$scope.get_ifsc(value,'1');
					$scope.ifsc1err= "";
				}else if(type=="od_ifsc2"){
					$scope.get_ifsc(value,'2');
					$scope.ifsc2err= "";
				}else if(type=="od_ifsc3"){
					$scope.get_ifsc(value,'3');
					$scope.ifsc3err= "";
				}else if(type=="od_ifsc4"){
					$scope.get_ifsc(value,'4');
					$scope.ifsc4err= "";
				}else if(type=="od_ifsc5"){
					$scope.get_ifsc(value,'5');
					$scope.ifsc5err= "";
				}
				return true;
			}else{
				if(type == "od_ifsc1")
					$scope.ifsc1err= "Please Enter Valid IFSC Code";
				else if(type == "od_ifsc2")
					$scope.ifsc2err= "Please Enter Valid IFSC Code";
				else if(type == "od_ifsc3")
					$scope.ifsc3err= "Please Enter Valid IFSC Code";
				else if(type == "od_ifsc4")
					$scope.ifsc4err= "Please Enter Valid IFSC Code";
				else if(type == "od_ifsc5")
					$scope.ifsc5err= "Please Enter Valid IFSC Code";
				return false;
			}
		}else if((type == "od_ifsc1" || type=="od_ifsc2" || type=="od_ifsc3" || type=="od_ifsc4" || type=="od_ifsc5") && (value==undefined)){
			if(type == "od_ifsc1")
				$scope.ifsc1err= "IFSC Code is required";
			else if(type == "od_ifsc2")
				$scope.ifsc2err= "IFSC Code is required";
			else if(type == "od_ifsc3")
				$scope.ifsc3err= "IFSC Code is required";
			else if(type == "od_ifsc4")
				$scope.ifsc4err= "IFSC Code is required";
			else if(type == "od_ifsc5")
				$scope.ifsc5err= "IFSC Code is required";
			return false;
	   	}

	   	// bank name 1,2,3,4,5
		if((type == "od_bank_name1" || type=="od_bank_name2" || type=="od_bank_name3" || type=="od_bank_name4" || type=="od_bank_name5") && value!==undefined){
			if(value!=''){
				if(type == "od_bank_name1")
					$scope.od_bank_name1_error= "";
				else if(type == "od_bank_name2")
					$scope.od_bank_name2_error= "";
				else if(type == "od_bank_name3")
					$scope.od_bank_name3_error= "";
				else if(type == "od_bank_name4")
					$scope.od_bank_name4_error= "";
				else if(type == "od_bank_name5")
					$scope.od_bank_name5_error= "";
				return true;
			}else{
				if(type == "od_bank_name1")
					$scope.od_bank_name1_error= "Please Select Bank 1 Name";
				else if(type == "od_bank_name2")
					$scope.od_bank_name2_error= "Please Select Bank 2 Name";
				else if(type == "od_bank_name3")
					$scope.od_bank_name3_error= "Please Select Bank 3 Name";
				else if(type == "od_bank_name4")
					$scope.od_bank_name4_error= "Please Select Bank 4 Name";
				else if(type == "od_bank_name5")
					$scope.od_bank_name5_error= "Please Select Bank 5 Name";
				return false;
			}
		}else if((type == "od_bank_name1" || type=="od_bank_name2" || type=="od_bank_name3" || type=="od_bank_name4" || type=="od_bank_name5") && value==undefined){
			if(type == "od_bank_name1")
				$scope.od_bank_name1_error= "Bank 1 Name is required";
			else if(type == "od_bank_name2")
				$scope.od_bank_name2_error= "Bank 2 Name is required";
			else if(type == "od_bank_name3")
				$scope.od_bank_name3_error= "Bank 3 Name is required";
			else if(type == "od_bank_name4")
				$scope.od_bank_name4_error= "Bank 4 Name is required";
			else if(type == "od_bank_name5")
				$scope.od_bank_name5_error= "Bank 5 Name is required";
			return false;
		}

		//Account Number 1,2,3,4,5
		if((type == "od_account_no1" || type=="od_account_no2" || type=="od_account_no3" || type=="od_account_no4" || type=="od_account_no5") && value!==undefined){
			if(/^[0-9]+$/.test(value)){
				if(type=="od_account_no1")
					$scope.accountno1err= "";
				else if(type=="od_account_no2")
					$scope.accountno2err= "";
				else if(type=="od_account_no3")
					$scope.accountno3err= "";
				else if(type=="od_account_no4")
					$scope.accountno4err= "";
				else if(type=="od_account_no5")
					$scope.accountno5err= "";
				return true;
			}else{
				if(type=="od_account_no1")
					$scope.accountno1err= "Please Enter Valid Account No.";
				else if(type=="od_account_no2")
					$scope.accountno2err= "Please Enter Valid Account No.";
				else if(type=="od_account_no3")
					$scope.accountno3err= "Please Enter Valid Account No.";
				else if(type=="od_account_no4")
					$scope.accountno4err= "Please Enter Valid Account No.";
				else if(type=="od_account_no5")
					$scope.accountno5err= "Please Enter Valid Account No.";
				return false;
			}
		}else if((type == "od_account_no1" || type=="od_account_no2" || type=="od_account_no3" || type=="od_account_no4" || type=="od_account_no5") && value==undefined){
			if(type == "od_account_no1")
				$scope.accountno1err= "Account No. is required";
			else if(type == "od_account_no2")
				$scope.accountno2err= "Account No. is required";
			else if(type == "od_account_no3")
				$scope.accountno3err= "Account No. is required";
			else if(type == "od_account_no4")
				$scope.accountno4err= "Account No. is required";
			else if(type == "od_account_no5")
				$scope.accountno5err= "Account No. is required";
			return false;
		}

		//Account Type 1,2,3,4,5
		if((type == "account_type1" || type == "account_type2" || type == "account_type3" || type == "account_type4" || type == "account_type5" ) && value!==undefined){
			if(value=='savings' || value == 'current'){
				if(type == "account_type1")
					$scope.account_type1_error= "";
				else if(type == "account_type2")
					$scope.account_type2_error= "";
				else if(type == "account_type3")
					$scope.account_type3_error= "";
				else if(type == "account_type4")
					$scope.account_type4_error= "";
				else if(type == "account_type5")
					$scope.account_type5_error= "";
				return true;
			}else{
				if(type == "account_type1")
					$scope.account_type1_error= "Select Account Type";
				else if(type == "account_type2")
					$scope.account_type2_error= "Select Account Type";
				else if(type == "account_type3")
					$scope.account_type3_error= "Select Account Type";
				else if(type == "account_type4")
					$scope.account_type4_error= "Select Account Type";
				else if(type == "account_type5")
					$scope.account_type5_error= "Select Account Type";
				return false;
			}
		}else if((type == "account_type1" || type == "account_type2" || type == "account_type3" || type == "account_type4" || type == "account_type5" ) && value==undefined){
			if(type == "account_type1")
				$scope.account_type1_error= "Account Type Selection required";
			else if(type == "account_type2")
				$scope.account_type2_error= "Account Type Selection required";
			else if(type == "account_type3")
				$scope.account_type3_error= "Account Type Selection required";
			else if(type == "account_type4")
				$scope.account_type4_error= "Account Type Selection required";
			else if(type == "account_type5")
				$scope.account_type5_error= "Account Type Selection required";
		}

		//Cash Deposited
		if(type == "cash_deposited" && value!==undefined){
			var cash_length=value.length;
			if(cash_length!=0){
				$scope.cash_deposited_error= "";
				return true;
			}else{
				$scope.cash_deposited_error= "Please Enter Cash Deposited";
				return false;
			}
		}else if((type == "cash_deposited") && value==undefined){
			$scope.cash_deposited_error= "Cash Deposited is required";
			return false;
		}

		// tax_income_exceed and for
		if( (type == "foreign_asset" || type == "tax_income_exceed") && value!==undefined){
			if(type == "foreign_asset")
				$scope.foreign_asseterr= "";
			if(type == "tax_income_exceed")
				$scope.tax_income_exceederr= "";
			return true;
		}else if((type == "foreign_asset" || type == "tax_income_exceed") && value==undefined){
			if(type == "foreign_asset")
				$scope.foreign_asseterr= "Do you have any Foreign Assets?";
			if(type == "tax_income_exceed")
				$scope.tax_income_exceederr= "Does your Taxable Income Exceed Rs. 50 Lakhs?";
			return false;
		}
		//////////424
	}

	$scope.Taxable_income_exceed = function(value) {
		//send mail & then display msg
		$scope.tax_income_exceederr= "";
		if (value == 'yes')
			$scope.tax_income_exceedmsg = 'As your taxable income is more than Rs.50 lakhs, you are required to provide your assets & liabilities details. This is required by Income Tax department. We have emailed you the format for the same. Kindly respond back at the earliest.';
		else
			$scope.tax_income_exceedmsg = '';
	}

	$scope.foreign_asset_check = function(value){
		// alert($scope.user.foreign_asset);
		$scope.foreign_asseterr= "";
	}

	$scope.gotoSaveOtherDetails = function(type,value,$event) {
		var status=$scope.validation_check();
		$scope.status = status;

		if(status==true){
			var no_of_bank = $scope.user.noofbanks;
			var B1 = true;
			var B2 = true;
			var B3 = true;
			var B4 = true;
			var B5 = true;

			var pass_obj1 = { "employer_category":$scope.user.employer_category};
			pass_obj1["client_id"] = client_id;
			pass_obj1["residential_status"] = $scope.user.residential_status;
			pass_obj1["foreign_asset"] = $scope.user.foreign_asset;
			pass_obj1["tax_income_exceed"] = $scope.user.tax_income_exceed;
			var data_pass1 = JSON.stringify(pass_obj1);
			$.ajax({
				url:'/save_other_details/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass1},
				success: function(response){
					console.log(response);
					if(response.status == 'success')
						$scope.user_tracking('submitted','Residential Status');
				}
			});
			// alert(no_of_bank);
			if(no_of_bank==1 || no_of_bank==2 || no_of_bank==3 || no_of_bank==4 || no_of_bank==5 ){
				var B1_IFSCStatus = $scope.validation("od_ifsc1",$scope.user.od_ifsc1);
				var B1_bankstatus = $scope.validation("od_bank_name1",$scope.user.od_bank_name1);
				var B1_accNoStatus = $scope.validation("od_account_no1",$scope.user.od_account_no1);
				var B1_accTypeStatus = $scope.validation("account_type1",$scope.user.account_type1);

				if(B1_IFSCStatus==true && B1_bankstatus==true && B1_accNoStatus==true && B1_accTypeStatus==true){
					B1 = true;
					var pass_obj = { "bankID":0,"propertyType":$scope.user.house_property_type1};
					pass_obj["client_id"] = client_id;
					pass_obj["bank_name"] = $scope.user.od_bank_name1;
					pass_obj["ifsc"] = $scope.user.od_ifsc1;
					pass_obj["acc_no"] = $scope.user.od_account_no1;
					pass_obj["acc_type"] = $scope.user.account_type1;
					// console.log(pass_obj);
      				var data_pass = JSON.stringify(pass_obj);

      				$.ajax({
						url:'/save_bank_details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							if(response.status == 'success')
								$scope.user_tracking('submitted','Bank 1 Details');
						}
					});
				}
				else{
					B1 = false;
					if(!B1_IFSCStatus)
						$scope.user_tracking('error in data','Bank 1 IFSC');
					if(!B1_accNoStatus)
						$scope.user_tracking('error in data','Bank 1 Account No');
					if(!B1_accTypeStatus)
						$scope.user_tracking('error in data','Bank 1 Account Type');
				}
			}
			if( no_of_bank==2 || no_of_bank==3 || no_of_bank==4 || no_of_bank==5 ){
				var B2_IFSCStatus = $scope.validation("od_ifsc2",$scope.user.od_ifsc2);
				var B2_bankstatus = $scope.validation("od_bank_name2",$scope.user.od_bank_name2);
				var B2_accNoStatus = $scope.validation("od_account_no2",$scope.user.od_account_no2);
				var B2_accTypeStatus = $scope.validation("account_type2",$scope.user.account_type2);

				if(B2_IFSCStatus==true && B2_accNoStatus==true && B2_accTypeStatus==true){
					B2 = true;
					var pass_obj = { "bankID":1,"propertyType":$scope.user.house_property_type2};
					pass_obj["client_id"] = client_id;
					pass_obj["bank_name"] = $scope.user.od_bank_name2;
					pass_obj["ifsc"] = $scope.user.od_ifsc2;
					pass_obj["acc_no"] = $scope.user.od_account_no2;
					pass_obj["acc_type"] = $scope.user.account_type2;
      				var data_pass = JSON.stringify(pass_obj);

      				$.ajax({
						url:'/save_bank_details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							if(response.status == 'success')
								$scope.user_tracking('submitted','Bank 2 Details');
						}
					});
				}
				else{
					B2 = false;
					if(!B2_IFSCStatus)
						$scope.user_tracking('error in data','Bank 2 IFSC');
					if(!B2_accNoStatus)
						$scope.user_tracking('error in data','Bank 2 Account No');
					if(!B2_accTypeStatus)
						$scope.user_tracking('error in data','Bank 2 Account Type');
				}
			}
			if( no_of_bank==3 || no_of_bank==4 || no_of_bank==5 ){
				var B3_IFSCStatus = $scope.validation("od_ifsc3",$scope.user.od_ifsc3);
				// var B3_bankstatus = $scope.validation("od_ifsc3",$scope.user.od_ifsc3);
				var B3_accNoStatus = $scope.validation("od_account_no3",$scope.user.od_account_no3);
				var B3_accTypeStatus = $scope.validation("account_type3",$scope.user.account_type3);

				if(B3_IFSCStatus==true && B3_accNoStatus==true && B3_accTypeStatus==true){
					B3 = true;
					var pass_obj = { "bankID":2,"propertyType":$scope.user.house_property_type3};
					pass_obj["client_id"] = client_id;
					pass_obj["bank_name"] = $scope.user.od_bank_name3;
					pass_obj["ifsc"] = $scope.user.od_ifsc3;
					pass_obj["acc_no"] = $scope.user.od_account_no3;
					pass_obj["acc_type"] = $scope.user.account_type3;
      				var data_pass = JSON.stringify(pass_obj);

      				$.ajax({
						url:'/save_bank_details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							if(response.status == 'success')
								$scope.user_tracking('submitted','Bank 3 Details');
						}
					});
				}
				else{
					B3 = false;
					if(!B3_IFSCStatus)
						$scope.user_tracking('error in data','Bank 3 IFSC');
					if(!B3_accNoStatus)
						$scope.user_tracking('error in data','Bank 3 Account No');
					if(!B3_accTypeStatus)
						$scope.user_tracking('error in data','Bank 3 Account Type');
				}
			}
			if( no_of_bank==4 || no_of_bank==5 ){
				var B4_IFSCStatus = $scope.validation("od_ifsc4",$scope.user.od_ifsc4);
				// var B4_bankstatus = $scope.validation("od_ifsc4",$scope.user.od_ifsc4);
				var B4_accNoStatus = $scope.validation("od_account_no4",$scope.user.od_account_no4);
				var B4_accTypeStatus = $scope.validation("account_type4",$scope.user.account_type4);

				if(B4_IFSCStatus==true && B4_accNoStatus==true && B4_accTypeStatus==true){
					B4 = true;
					var pass_obj = { "bankID":3,"propertyType":$scope.user.house_property_type4};
					pass_obj["client_id"] = client_id;
					pass_obj["bank_name"] = $scope.user.od_bank_name4;
					pass_obj["ifsc"] = $scope.user.od_ifsc4;
					pass_obj["acc_no"] = $scope.user.od_account_no4;
					pass_obj["acc_type"] = $scope.user.account_type4;
      				var data_pass = JSON.stringify(pass_obj);

      				$.ajax({
						url:'/save_bank_details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							if(response.status == 'success')
								$scope.user_tracking('submitted','Bank 4 Details');
						}
					});
				}
				else{
					B4 = false;
					if(!B4_IFSCStatus)
						$scope.user_tracking('error in data','Bank 4 IFSC');
					if(!B4_accNoStatus)
						$scope.user_tracking('error in data','Bank 4 Account No');
					if(!B4_accTypeStatus)
						$scope.user_tracking('error in data','Bank 4 Account Type');
				}
			}
			if( no_of_bank==5 ){
				var B5_IFSCStatus = $scope.validation("od_ifsc5",$scope.user.od_ifsc5);
				// var B5_bankstatus = $scope.validation("od_ifsc5",$scope.user.od_ifsc5);
				var B5_accNoStatus = $scope.validation("od_account_no5",$scope.user.od_account_no5);
				var B5_accTypeStatus = $scope.validation("account_type5",$scope.user.account_type5);

				if(B5_IFSCStatus==true && B5_accNoStatus==true && B5_accTypeStatus==true){
					B5 = true;
					var pass_obj = { "bankID":4,"propertyType":$scope.user.house_property_type5};
					pass_obj["client_id"] = client_id;
					pass_obj["bank_name"] = $scope.user.od_bank_name5;
					pass_obj["ifsc"] = $scope.user.od_ifsc5;
					pass_obj["acc_no"] = $scope.user.od_account_no5;
					pass_obj["acc_type"] = $scope.user.account_type5;
      				var data_pass = JSON.stringify(pass_obj);

      				$.ajax({
						url:'/save_bank_details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							// console.log(response);
							if(response.status == 'success')
								$scope.user_tracking('submitted','Bank 5 Details');
						}
					});
				}
				else{
					B5 = false;
					if(!B5_IFSCStatus)
						$scope.user_tracking('error in data','Bank 5 IFSC');
					if(!B5_accNoStatus)
						$scope.user_tracking('error in data','Bank 5 Account No');
					if(!B5_accTypeStatus)
						$scope.user_tracking('error in data','Bank 5 Account Type');
				}
			}
			// alert('B1 : '+B1);
			// alert('B2 : '+B2);
			// alert('B3 : '+B3);
			// alert('B4 : '+B3);
			// alert('B5 : '+B3);
			// alert('saving DB data');
			if(B1==true && B2==true && B3==true && B4==true && B5==true){
				// alert('redirect');
				$scope.user_tracking('submitted','Other Details');
				window.location.href = redirect_url+'/income_details/'+client_id;
			}
		}else{
			error_check = $scope.error_check();
			$scope.user_tracking('error in data',error_check);
		}
	}

	$scope.validation_check = function() {

		var employer_category_status =true
		// var employer_category_status = $scope.validation("employer_category",$scope.user.employer_category);
		var residential_status_status = $scope.validation("residential_status",$scope.user.residential_status);
		var noofbanks_status = $scope.validation("noofbanks",$scope.user.noofbanks);

		var foreign_asset_status = $scope.validation("foreign_asset",$scope.user.foreign_asset);
		var tax_income_exceed_status = $scope.validation("tax_income_exceed",$scope.user.tax_income_exceed);

		if(employer_category_status==true && residential_status_status==true && 
			noofbanks_status==true && foreign_asset_status== true && tax_income_exceed_status== true){
			return true;
		}else
			return false;
	}

	$scope.error_check = function() {
		var employer_category_status = $scope.validation("employer_category",$scope.user.employer_category);
		var residential_status_status = $scope.validation("residential_status",$scope.user.residential_status);
		var noofbanks_status = $scope.validation("noofbanks",$scope.user.noofbanks);
		var error = '';

		if( employer_category_status==false )
			error = 'Employer_category validation';
		if( residential_status_status==false )
			error = 'Residential Status validation';
		if( noofbanks_status==false )
			error = 'No. of Bank validation';
			
		return error;
	}

	$scope.get_ifsc = function(value,bank){
		$.ajax({
			url:'/ifsc_api/',
			type:'POST',
			dataType: 'json',
			data:{'ifsc':value},
			success: function(response){
				// console.log(response);
				if(response.IFSC.status == 'success'){
					if (bank == '1')
						$scope.user.od_bank_name1 = response.IFSC.data.BANK;
				}
				else
					$scope.user.od_bank_name1 = '';
			}
		});
	}

	$scope.get_other_details = function(value,bank){
		$.ajax({
			url:'/get_other_details/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(JSON.stringify(response));
				$scope.user.employer_category = response.Employer_category;

				if(response.residential_status=='')
					$scope.user.residential_status ='RES'
				else
					$scope.user.residential_status = response.residential_status;

				if (response.foreign_asset == 'yes')
					$scope.user.foreign_asset = 'yes'
				else
					$scope.user.foreign_asset = 'no'
				if (response.tax_income_exceed == 'yes')
					$scope.user.tax_income_exceed = 'yes'
				else
					$scope.user.tax_income_exceed = 'no'

				no_of_bank = JSON.stringify(response.no_of_bank).replace(/\"/g, "");

				if(no_of_bank==0)
					$scope.user.noofbanks=''
				else
					$scope.user.noofbanks = no_of_bank;
				
				no_banks(no_of_bank);
				if (no_of_bank==1 || no_of_bank==2 || no_of_bank==3  || no_of_bank==4  || no_of_bank==5 ) {
					$scope.user.od_ifsc1= response.ifsc[0];
					$scope.user.od_bank_name1= response.bank_name[0];
					$scope.user.od_account_no1= response.acc_no[0];
					$scope.user.account_type1= response.acc_type[0];
				}
				if (no_of_bank==2 || no_of_bank==3 || no_of_bank==4 || no_of_bank== 5 ) {
					$scope.user.od_ifsc2= response.ifsc[1];
					$scope.user.od_bank_name2= response.bank_name[1];
					$scope.user.od_account_no2= response.acc_no[1];
					$scope.user.account_type2= response.acc_type[1];
				}
				if (no_of_bank==3 || no_of_bank==4 || no_of_bank==5 ) {
					$scope.user.od_ifsc3= response.ifsc[2];
					$scope.user.od_bank_name3= response.bank_name[2];
					$scope.user.od_account_no3= response.acc_no[2];
					$scope.user.account_type3= response.acc_type[2];
				}
				if (no_of_bank==4 || no_of_bank==5 ) {
					$scope.user.od_ifsc4= response.ifsc[3];
					$scope.user.od_bank_name4= response.bank_name[3];
					$scope.user.od_account_no4= response.acc_no[3];
					$scope.user.account_type4= response.acc_type[3];
				}

				$('.loader').hide()
				$scope.$apply();
				// if($scope.user.od_ifsc1 != '')
				// 	label_float('ifsc1_div');
				// if($scope.user.od_ifsc2 != '')
				// 	label_float('ifsc2_div');
				// if($scope.user.od_ifsc3 != '')
				// 	label_float('ifsc3_div');
				// if($scope.user.od_ifsc4 != '')
				// 	label_float('ifsc4_div');
				// if($scope.user.od_ifsc5 != '')
				// 	label_float('ifsc5_div');

				// if($scope.user.od_account_no1 != '')
				// 	label_float('acc1_div');
				// if($scope.user.od_account_no2 != '')
				// 	label_float('acc2_div');
				// if($scope.user.od_account_no3 != '')
				// 	label_float('acc3_div');
				// if($scope.user.od_account_no4 != '')
				// 	label_float('acc4_div');
				// if($scope.user.od_account_no5 != '')
				// 	label_float('acc5_div');
			}
		});
	}

}]);
