

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('personal_info_controller', ['$scope','$rootScope','$compile', function($scope,$rootScope,$compile,$http,CONFIG) {

  redirect_url = $scope.url;
  var client_id = 1;
  var email = '';
  //var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
  $scope.isDisabled = false;
  var pan_file_flag=0;
  var registered_pan= 'NO';
  var check_pan_dob_flag = 0;

  $('.loader').show();
  $(window).load(function() {
    console.log('ID : '+$scope.user.id);
    console.log('Client ID : '+$scope.user.client_id);
    client_id=$scope.user.client_id
    // if($scope.user.id == '')
    //   alert('ID is empty');
    $scope.pan1_dob_error = '';
    $scope.panerr = '';
    $scope.pan1_last_name_error = '';
    
    $.ajax({
      url:'/get_client/',
      type:'POST',
      dataType: 'json',
      async:false,
      data:{'mail':'mail'},
      success: function(response){
        console.log(response);
        
        if(response.client_id != 0 ) {
          // client_id = response.client_id;
          // email = response.email;
          $scope.get_personal_information();
        }
        else if($scope.user.id == ''){
          $scope.display_logout = {"display" : "block"};
          $scope.display_login = {"display" : "none"};
        }
        if($scope.user.id != '') {
          $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            async:false,
            dataType: 'json',
            data:{'username':$scope.user.id},
            success: function(response){
              console.log(response);
              // client_id = response.client_id;
              email = response.email;
              $scope.get_personal_information();
            }
          });
        }
        else{
          $scope.display_logout = {"display" : "block"};
          $scope.display_login = {"display" : "none"};
        }


      }
    });


  });

  $scope.change_css = function(){
    setTimeout(function(){
      if($scope.user.pan1_first_name != '')
        label_float('fname_div');
      if($scope.user.pan1_middle_name != '')
        label_float('mname_div');
      if($scope.user.pan1_last_name != '')
        label_float('lname_div');
      if($scope.user.pan1_dob != '')
        label_float('dob_div');
      if($scope.user.pan1 != '')
        label_float('pan_div');
      if($scope.user.pan1_adhar != '')
        label_float('adhar_div');
      if($scope.user.mobile != '')
        label_float('mobile_div');
      if($scope.user.pan1_flatno != '')
        label_float('flat_div');
      if($scope.user.pan1_premise != '')
        label_float('premise_div');
      if($scope.user.pan1_road != '')
        label_float('road_div');
      if($scope.user.pan1_area != '')
        label_float('area_div');
      if($scope.user.pan1_pin != '')
        label_float('pin_div');
      if($scope.user.pan1_city != '')
        label_float('city_div');
      if($scope.user.pan1_country != '')
        label_float('country_div');
    }, 200);
  }
  $scope.validation = function(type,value,$event) {
    //PAN
    if(type == "pan"  && value!==undefined){
      var pan_length=value.length;
      if((/[A-Z]{5}\d{4}[A-Z]{1}/.test(value)) && pan_length==10){ 
        $scope.panerr= "";
        return true;
      }else{
        $scope.panerr= "Please Enter Valid PAN";
        return false;
      }
    }else if((type == "pan") && value==undefined){
      $scope.panerr= "PAN is required";
      return false;
    }

    //adhar
    if((type == "pan1_adhar" ) && value!==undefined){
      var adhar_length=value.length;
      if( ((/\d{6}/.test(value)) && adhar_length==12) || value==''){
        $scope.pan1_adharerr= "";
        return true;
      }else{
        $scope.pan1_adharerr= "Enter Valid Aadhar No.";
        return false;
      }
    }else if((type == "pan1_adhar") && value==undefined)
      return true;

    //First Name
    if(type == "pan1_first_name" && value!==undefined){
      var pan_length=value.length;
      if(pan_length >= 3){ 
        $scope.pan1_first_name_error= "";
        return true;
      }else{
        $scope.pan1_first_name_error= "Please Enter First Name";
        $scope.user.pan1_client_id=null;
        return false;
      }
    }else if(type == "pan1_first_name"  && value==undefined){
      $scope.pan1_first_name_error= "First Name is required";
      return false;
    }
    //Middle Name
    if(type == "pan1_middle_name" && value!==undefined){
      var pan_length=value.length;
      if(pan_length >= 3){ 
        $scope.pan1_middle_name_error= "";
        return true;
      }else{
        $scope.pan1_middle_name_error= "Please Enter Middle Name";
        $scope.user.pan1_client_id=null;
        return false;
      }
    }else if(type == "pan1_middle_name"  && value==undefined){
      $scope.pan1_middle_name_error= "Middle Name is required";
      return false;
    }
    //Last Name
    if(type == "pan1_last_name" && value!==undefined){
      var pan_length=value.length;
      if(pan_length >= 3){ 
        $scope.pan1_last_name_error= "";
        return true;
      }else{
        $scope.pan1_last_name_error= "Please Enter Last Name";
        return false;
      }
    }else if(type == "pan1_last_name" && value==undefined){
      $scope.pan1_last_name_error= "Last Name is required";
      return false;
    }

    //gender
    if((type == "pan1_gender") && value!==undefined){
      if(value==''){
        $scope.pan1_gender_error= "Select Gender";
        return false;
      }else{
        $scope.pan1_gender_error= "";
        return true;
      }
    }else if((type == "pan1_gender") && value==undefined){
      $scope.pan1_gender_error= "Gender Selection required";
      return false;
    }

    //dob
    if((type == "dob1") && value!==undefined){
      var pan_length=value.length;
      if(pan_length==0){
        $scope.pan1_dob_error= "Enter Date Of Birth";
        return false;
      }else{
        $scope.pan1_dob_error= "";
        return true;
      }
    }else if((type == "dob1") && value==undefined){
      $scope.pan1_dob_error= "Enter Valid Date Of Birth";
      return false;
    }

    //address
    if((type == "pan1_flatno" || type=="pan1_premise" || type=="pan1_road" || type=="pan1_area" || type=="pan1_city") && value!==undefined){
      if(value!=''){
        if(type == "pan1_flatno")
          $scope.pan1_flatno_error= "";
        else if(type == "pan1_premise")
          $scope.pan1_premise_error= "";
        else if(type == "pan1_road")
          $scope.pan1_road_error= "";
        else if(type == "pan1_area")
          $scope.pan1_area_error= "";
        else if(type == "pan1_city")
          $scope.pan1_city_error= "";
        return true;
      }else{
        if(type == "pan1_flatno")
          $scope.pan1_flatno_error= "Please Enter Flat/Door/Block No";
        else if(type == "pan1_premise")
          $scope.pan1_premise_error= "Please Enter Permise";
        else if(type == "pan1_road")
           $scope.pan1_road_error= "Please Enter Road/Street";
        else if(type == "pan1_area")
          $scope.pan1_area_error= "Please Enter Area/Locality";
        else if(type == "pan1_city")
          $scope.pan1_city_error= "Please Enter City";
        
        return false;
      }
    }else if((type == "pan1_flatno" || type=="pan1_premise" || type=="pan1_road" || type=="pan1_area" || type=="pan1_city") && value==undefined){
      if(type == "pan1_flatno")
        $scope.pan1_flatno= "Flat/Door/Block No is required";
      else if(type == "pan2_permanant_address")
         $scope.pan1_premise= "Permise is required";
      else if(type == "pan1_road")
         $scope.pan1_road_error= "Road/Street is required";
      else if(type == "pan1_area")
        $scope.pan1_area_error= "Area/Locality is required";
      else if(type == "pan1_city")
        $scope.pan1_city_error= "City is required";
      
      return false;
    }

    //Pin
    if(type == "pan1_pin" && value!==undefined){
      var pin_length=value.length;
      if((/\d{6}/.test(value)) && pin_length==6){ 
        $scope.pan1_pin_error= "";
        $.ajax({
          url:'/get_pin_API/',
          type:'POST',
          dataType: 'json',
          data:{'pin':value},
          success: function(response){
            // console.log(response);
            if(response.pin.records.length == 0) {
              $scope.user.pan1_city = '';
              $scope.user.pan1_state = '';
            }else{
              $scope.user.pan1_city = response.pin.records['0']['taluk'];
              $scope.user.pan1_state = response.pin.records['0']['statename'];
            }
            if($scope.user.pan1_city != '')
              label_float('city_div');
            $scope.$apply();
          }
        });
        return true;
      }else{
        $scope.pan1_pin_error= "Please Enter Valid PIN";
        return false;
      }
    }else if((type == "pan1_pin") && value==undefined){
      $scope.pan1_pin_error= "PIN is required";
      return false;
    }

    // State/country
    if((type == "pan1_state" || type=="pan1_country") && value!==undefined){
      if(value!=''){ 
        if(type == "pan1_state"){
          $scope.pan1_state_error= "";
        }else if(type == "pan1_country"){
          $scope.pan1_country_error= "";
        }
        return true;
      }else{
        if(type == "pan1_state"){
          $scope.pan1_state_error= "Please Select State";
        }else if(type == "pan1_country"){
          $scope.pan1_country_error= "Please Select Country";
        }
        return false;
      }
    }else if((type == "pan1_state" || type=="pan1_country") && value==undefined){
      if(type == "pan1_state"){
        $scope.pan1_state_error= "State is required";
      }else if(type == "pan1_country"){
        $scope.pan1_country_error= "Country is required";
      }
      return false;
    }

    //Residential_status
    if((type == "Residential_status") && value!==undefined){
      if(value==''){
        $scope.Residential_status_error= "Select Residential Status";
        return false;
      }else{
        $scope.Residential_status_error= "";
        return true;
      }
    }else if((type == "Residential_status") && value==undefined){
      $scope.Residential_status_error= "Residential Status Selection required";
      return false;
    }

    //disability
    if((type == "disability") && value!==undefined){
      if(value==''){
        $scope.disability_error= "Select Disability";
        return false;
      }else{
        $scope.disability_error= "";
        return true;
      }
    }else if((type == "disability") && value==undefined){
      $scope.disability_error= "Disability Selection required";
      return false;
    }

    //Mobile
    if(type == "mobile" && value!==undefined){
      var mobile_length=value.length;
      if((/\d{10}/.test(value)) && mobile_length==10){ 
        $scope.mobile_error= "";
        return true;
      }else{
        $scope.mobile_error= "Please Enter Valid Mobile Number";
        return false;
      }
    }else if((type == "mobile") && value==undefined){
      $scope.mobile_error= "Mobile Number is required";
      return false;
    }
  }

  $scope.gotoSavePersonalInformation = function(user,$event) {
    if($scope.pan1_dob_error == '' && $scope.panerr == ''){
      var status=$scope.validation_check();
    }
    if(status==true){
      // console.log($scope.user);
      // console.log(email);
      var data=JSON.stringify($scope.user);
      $.ajax({
        url:'/personal_details_DB/',
        type:'POST',
        dataType: 'json',
        data:{'data':data,'client_id':client_id,'email':email},
        success: function(response){
          console.log(response);
          status = JSON.stringify(response['status']).replace(/\"/g, "");
          if(status == 'success'){

            $scope.show_other_income()
            $scope.update_detail();
            // $.ajax({
            //   url:'/goto_client_registration/',
            //   type:'POST',
            //   dataType: 'json',
            //   data:{'data':data},
            //   success: function(response){ 
            //     // console.log(response);
            //     status = JSON.stringify(response['status']).replace(/\"/g, "");
            //     if(status == 'success')
            //       window.location.href = redirect_url+'/client_registration/'+client_id;
            //   }
            // });
          }else
            // $scope.user_tracking('error in data',status);

          $scope.$apply();
        }
      });
      // $scope.isDisabled = true;
      
      // if(check_pan_dob_flag == 1)
        // $scope.validation_check_onefiling();
      
      // var pass_obj = { "pan":user.pan1, "registered_pan":registered_pan, "name":user.pan1_first_name };
      // var data_pass = JSON.stringify(pass_obj);
      // var data=JSON.stringify(user);
    }else{
      error_check = $scope.error_check();
      // $scope.user_tracking('error in data',error_check);
    }
  }

  $scope.validation_check = function() {
    var tax_status = $scope.validation("pan",$scope.user.pan1);
    // var adhar_status = $scope.validation("pan1_adhar",$scope.user.pan1_adhar);
    var f_name_status = $scope.validation("pan1_first_name",$scope.user.pan1_first_name);
    // var m_name_status = $scope.validation("pan1_middle_name",$scope.user.pan1_middle_name);

    var l_name_status = false;
    if($scope.pan1_last_name_error == '' ){
      var l_name_status1 = $scope.validation("pan1_last_name",$scope.user.pan1_last_name);
      l_name_status = l_name_status1;
    }

    var gender_status = $scope.validation("pan1_gender",$scope.user.pan1_gender);
    var dob_status = $scope.validation("dob1",$scope.user.pan1_dob);
    var pin_status = $scope.validation("pan1_pin",$scope.user.pan1_pin);
    var Residential_status = $scope.validation("Residential_status",$scope.user.Residential_status);
    var disability_status = $scope.validation("disability",$scope.user.disability);
    var mobile_status = $scope.validation("mobile",$scope.user.mobile);
    var flat_status = $scope.validation("pan1_flatno",$scope.user.pan1_flatno);
    var area_status = $scope.validation("pan1_area",$scope.user.pan1_area);
    var city_status = $scope.validation("pan1_city",$scope.user.pan1_city);
    var status = true;

    if(tax_status==true && f_name_status==true && l_name_status==true ){
      status = true;
    }else
      status = false;

    if(status == true && gender_status==true && dob_status==true && pin_status==true && Residential_status==true && disability_status==true){
      status = true;
    }else
      status = false;

    if(status == true && mobile_status==true && flat_status==true && area_status==true && city_status==true){
      status = true;
    }else
      status = false;

    // console.log('tax_status : '+tax_status);
    // console.log('validation_check : '+status);
    return status;
  }
  $scope.error_check = function() {
    var tax_status = $scope.validation("pan",$scope.user.pan1);
    // var adhar_status = $scope.validation("pan1_adhar",$scope.user.pan1_adhar);
    var f_name_status = $scope.validation("pan1_first_name",$scope.user.pan1_first_name);
    // var m_name_status = $scope.validation("pan1_middle_name",$scope.user.pan1_middle_name);

    var l_name_status = false;
    if($scope.pan1_last_name_error == '' ){
      var l_name_status1 = $scope.validation("pan1_last_name",$scope.user.pan1_last_name);
      l_name_status = l_name_status1;
    }

    var gender_status = $scope.validation("pan1_gender",$scope.user.pan1_gender);
    var dob_status = $scope.validation("dob1",$scope.user.pan1_dob);
    var pin_status = $scope.validation("pan1_pin",$scope.user.pan1_pin);
    var Residential_status = $scope.validation("Residential_status",$scope.user.Residential_status);
    var mobile_status = $scope.validation("mobile",$scope.user.mobile);
    var flat_status = $scope.validation("pan1_flatno",$scope.user.pan1_flatno);
    var area_status = $scope.validation("pan1_area",$scope.user.pan1_area);
    var city_status = $scope.validation("pan1_city",$scope.user.pan1_city);
    var error = '';

    if(tax_status==false )
      error = 'PAN validation';
    else if( f_name_status==false )
      error = 'First Name validation';
    else if( l_name_status==false )
      error = 'Last Name validation';
    else if( gender_status==false )
      error = 'Last Name validation';
    else if( dob_status==false )
      error = 'DOB validation';
    else if( pin_status==false )
      error = 'Pin validation';
    else if( Residential_status==false )
      error = 'Residential status validation';
    else if( mobile_status==false )
      error = 'Mobile No validation';
    else if( flat_status==false )
      error = 'Flat/Block No validation';
    else if( area_status==false && city_status==false)
      error = 'Area/Locality validation';
    else if( city_status==false)
      error = 'City validation';

    console.log('error_check : '+error);
    return error;
  }

  $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
    var pan=$scope.user.pan1;
    var data=JSON.stringify(fileObj);
    $.ajax({
      url:'/file_upload/',
      type:'POST',
      dataType: 'json',
      data:{'data':data,'pan':pan,'type':'PAN'},
      success: function(response){ pan_file_flag=1; }
    });
  };

  $scope.onLoad_adhar = function (e, reader, file, fileList, fileOjects, fileObj) {
    var adhaar=$scope.user.pan1_adhar;
    var data=JSON.stringify(fileObj);
    $.ajax({
      url:'/file_upload/',
      type:'POST',
      dataType: 'json',
      data:{'data':data,'pan':adhaar,'type':'ADHAR'},
      success: function(response){ }
    });
  };
  //disable
  $scope.check_pan_dob = function (user) {
    // $scope.isDisabled = true;########
    var status_validation = true;
    var pan_status = $scope.validation("pan",$scope.user.pan1);
    var dob_status = $scope.validation("dob1",$scope.user.pan1_dob);
    var last_name_status = $scope.validation("pan1_last_name",$scope.user.pan1_last_name);

    if(pan_status == true && dob_status == true && last_name_status == true){
      var pan=$scope.user.pan1;
      var dob=$scope.user.pan1_dob;
      var last_name=$scope.user.pan1_last_name;
      $.ajax({
        //check Is Salat Client
        url:'/check_update_registered_pan/',
        type:'POST',
        dataType: 'json',
        data:{'pan':pan,query:'select'},
        success: function(response){
          console.log('check registered PAN');
          console.log(response);
          if(response.entry_exist == 0) {
            //for valid PAN and DOB and Last name
            $.ajax({
              url:'/selenium_efiling/',
              type:'POST',
              dataType: 'json',
              data:{'pan':pan,'dob':dob,'last_name':last_name},
              success: function(response){
                console.log(response);
                status = JSON.stringify(response['status']).replace(/\"/g, "");
                pan_status = JSON.stringify(response['pan_status']).replace(/\"/g, "");
                dob_status = JSON.stringify(response['dob_status']).replace(/\"/g, "");
                valid_last_name = JSON.stringify(response['valid_last_name']).replace(/\"/g, "");
                
                if(pan_status != 'NO' && pan_status.indexOf("already") == -1) {
                  $scope.panerr = "Please Enter Valid PAN";
                  status_validation = false;
                }
                if(pan_status == 'PAN does not exist.') {
                  $scope.panerr = "Please Enter Valid PAN";
                  status_validation = false;
                }
                if(pan_status.indexOf("already") > -1){
                  registered_pan ='YES';
                  $.ajax({
                    url:'/check_update_registered_pan/',
                    type:'POST',
                    dataType: 'json',
                    data:{'pan':pan,query:'update'},
                    success: function(response){
                      // console.log(response);
                    }
                  });
                  if(dob_status == 'valid')
                    check_pan_dob_flag = 1;
                }
                if(pan_status == 'NO'){
                  $scope.panerr = "";
                  if(dob_status == 'valid')
                    check_pan_dob_flag = 1;
                }
                if(dob_status != 'valid') {
                  $scope.pan1_dob_error= "Please Enter DOB as per PAN";
                  status_validation = false;
                  check_pan_dob_flag = 0;
                }
                if(valid_last_name == 'Invalid') {
                  $scope.pan1_last_name_error= "Please Last Name as per PAN";
                  check_pan_dob_flag = 0;
                }
                if(valid_last_name == 'Invalid' || valid_last_name == 'check') {
                  status_validation = false;
                  check_pan_dob_flag = 0;
                }
                if(status_validation == false)
                  $scope.isDisabled = false;
              }
            });
            $scope.$apply();
          }
          else{
            $scope.isDisabled = false;
            check_pan_dob_flag = 1;
            registered_pan = 'YES';
          }
          $scope.$apply();
        }
      });
    }
  };

  $scope.validation_check_onefiling = function() {
    $scope.user['registered_pan'] = registered_pan;
    $scope.user['Email'] = email;
    var data=JSON.stringify($scope.user);
    var redirect_next = 'yes';
    //tushar.patil23692@gmail.com
    // alert(registered_pan);
    // alert(email);
    // $scope.$apply();
    console.log('validation_check_onefiling');
    console.log($scope.pan1_dob_error);
    console.log($scope.panerr);
    console.log(registered_pan);
    if($scope.pan1_dob_error == '' && $scope.panerr == '' && registered_pan == 'NO'){
      //Register User On Income Tax
      $.ajax({
        url:'/selenium_register_user/',
        type:'POST',
        dataType: 'json',
        data:{'data':data,'mail':email},
        success: function(response){
          // console.log(response);
          status = JSON.stringify(response['status']).replace(/\"/g, "");
          last_name_status = JSON.stringify(response['last_name_status']).replace(/\"/g, "");
          mobile_status = JSON.stringify(response['mobile_status']).replace(/\"/g, "");
          mail_status = JSON.stringify(response['mail_status']).replace(/\"/g, "");
          pin_status = JSON.stringify(response['pin_status']).replace(/\"/g, "");
          city_status = JSON.stringify(response['city_status']).replace(/\"/g, "");
          state = JSON.stringify(response['state']).replace(/\"/g, "");

          if(last_name_status != 'valid' && last_name_status != 'check') {
            $scope.pan1_last_name_error = "Please Enter Last Name as per PAN";
            redirect_next = 'no';
          }
          if(mobile_status != 'valid' && mobile_status != 'check') {
            $scope.mobile_error = "Please Enter valid Mobile Number";
            redirect_next = 'no';
          }
          if(city_status == 'Invalid') {
            $scope.pan1_city_error = "Please Enter valid City";
            redirect_next = 'no';
          }
          if(pin_status == 'Invalid') {
            $scope.pan1_pin_error = "Please Enter valid PIN";
            redirect_next = 'no';
          }
          if(city_status == 'check pin' && pin_status == 'Invalid') {
            $scope.pan1_pin_error = "Please check PIN";
            redirect_next = 'no';
          }
          if(mail_status == 'Invalid') {
            $scope.Residential_status_error = "Please check E-mail ID";
            redirect_next = 'no';
          }

          $scope.user.pan1_country = 'INDIA';
          $scope.user.pan1_state = state;
          if(redirect_next == 'yes'){
            $.ajax({
              url:'/personal_details_DB/',
              type:'POST',
              dataType: 'json',
              data:{'data':data,'client_id':client_id,'email':email},
              success: function(response){
                console.log(response);
                status = JSON.stringify(response['status']).replace(/\"/g, "");
                if(status == 'success'){
                  $scope.update();
                  $.ajax({
                    url:'/goto_client_registration/',
                    type:'POST',
                    dataType: 'json',
                    data:{'data':data},
                    success: function(response){ 
                      // console.log(response);
                      status = JSON.stringify(response['status']).replace(/\"/g, "");
                      if(status == 'success')
                        window.location.href = redirect_url+'/client_registration/'+client_id;
                    }
                  });
                }
                $scope.$apply();
              }
            });
          }
        }
      });
    }else if(registered_pan == 'YES') {
      $.ajax({
        url:'/personal_details_DB/',
        type:'POST',
        dataType: 'json',
        data:{'data':data,'client_id':client_id,'email':email},
        success: function(response){
          console.log(response);
          status = JSON.stringify(response['status']).replace(/\"/g, "");
          if(status == 'success'){
            $scope.update();
            $.ajax({
              url:'/goto_client_registration/',
              type:'POST',
              dataType: 'json',
              data:{'data':data},
              success: function(response){ 
                // console.log(response);
                status = JSON.stringify(response['status']).replace(/\"/g, "");
                if(status == 'success')
                  window.location.href = redirect_url+'/client_registration/'+client_id;
              }
            });
          }
          $scope.$apply();
        }
      });
    }
    else
      redirect_next = 'no';
  }

  $scope.update = function(){
    $.ajax({
      url:'/update_DB/',
      type:'POST',
      dataType: 'json',
      data:{'client_id':client_id,'option':'insert'},
      success: function(response){
        console.log(response);
        status = JSON.stringify(response['status']).replace(/\"/g, "");
        return status;
      }
    });
  }
  $scope.update_detail = function(){
    $.ajax({
      url:'/update_DB/',
      type:'POST',
      dataType: 'json',
      data:{'client_id':client_id,'option':'insert'},
      success: function(response){
        console.log(response);
        status = JSON.stringify(response['status']).replace(/\"/g, "");
        
        // if(status == 'success'){
        //   // $scope.user_tracking('submitted','Personal Information');
      
        //   // window.location.href = redirect_url+'/other_details/'+client_id;
        // }
        // else
          // $scope.user_tracking('error in data',status);
      }
      
    });
  }

  $scope.show_other_income=function(){
       $('#personalDetailsTab').removeClass('active');
       $('#otherDetailsTab').tab('show');
       $("html, body").animate({ scrollTop: 0 }, "slow");
  }

  $scope.gotopersonaldetailstab=function(){
      $('#otherDetailsTab').removeClass('active');
      $('#personalDetailsTab').tab('show');
      $("html, body").animate({ scrollTop: 0 }, "slow");
  }

  $scope.gotofilingtype=function(){
    window.location.href = redirect_url+'/chooseFillingType/'+client_id;
  }

  $scope.get_personal_information = function(){
    $.ajax({
      url:'/get_personal_information/',
      type:'POST',
      dataType: 'json',
      async:false,
      data:{'client_id':client_id},
      success: function(response){
        console.log(response);

        if(response.name!=null){

          $rootScope.username=response.name
          if(response.name.split(' ').length>=3) {
            $scope.user.pan1_first_name = response.name.split(' ')[0];
            $scope.user.pan1_middle_name = response.name.split(' ')[1];
            $scope.user.pan1_last_name = response.name.split(' ')[2];
          }else{
            $scope.user.pan1_first_name = response.name.split(' ')[0];
            $scope.user.pan1_last_name = response.name.split(' ')[1];
          }

        }else{
          $rootScope.username=''
        }

        if(response.business_partner_id!=''){
          business_partner_id=response.business_partner_id
          $scope.user.business_partner_id=business_partner_id
        }
          
        
        $scope.user.pan1_gender = response.gender;
        $scope.user.pan1_adhar = response.adhar;
        $scope.user.pan1_dob = response.dob;
        $scope.user.pan1 = response.PAN;
        $scope.user.mobile = response.mobile;
        $scope.user.pan1_flatno = response.flat_door_block_no;
        $scope.user.pan1_premise = response.name_of_premises_building;
        $scope.user.pan1_road = response.road_street_postoffice;
        $scope.user.pan1_area = response.area_locality;
        $scope.user.pan1_pin = response.pincode;
        $scope.user.pan1_city = response.town_city;
        $scope.user.pan1_state = response.state;
        $scope.user.pan1_country = 'INDIA';
        $scope.user.Residential_status = 'Resident';

        if(response.disability == 'yes')
          $scope.user.disability = 'yes';
        else
          $scope.user.disability = 'no';
        
        // $scope.check_pan_dob($scope.user);
        if(response.p_d_exist == 0)
          $scope.pan1_dob_error = "";
        // $scope.$apply();
        // $scope.change_css();

        $('.loader').hide()

         $scope.$apply();
      }
    });
  }

}]);

// DAVPP6576C
// 23-06-1992

// http://127.0.0.1:8000/client_registration/%7B%22pan1_first_name%22:%22TUSHAR%22,%22pan1_middle_name%22:%22RAVINDRA%22,
// %22pan1_last_name%22:%22PATIL%22,%22pan1_dob%22:%2223-06-1992%22,%22pan1_adhar%22:%22%22,%22pan1%22:%22DAVPP6576C%22,
// %22mobile%22:%228390103461%22,%22pan1_flatno%22:%22PLOTNO.40%22,%22pan1_premise%22:%22NAVCHAITNYA%22,%22pan1_road%22:%22N.D.A.ROAD%22,
// %22pan1_area%22:%22WARJE%22,%22pan1_pin%22:%22411058%22,%22pan1_city%22:%22PUNE%22,%22Residential_status%22:%22Resident%22,
// %22pan1_state%22:%22MAHARASHTRA%22,%22pan1_client_id%22:null,%22pan1_gender%22:%22male%22,%22pan1_country%22:%22INDIA%22,%22registered_pan%22:%22NO%22%7D

