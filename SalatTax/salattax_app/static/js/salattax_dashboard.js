
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('salattax_dashboard_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {
	close_model = 0;
	client_id = 0;
	pan = '';
	p_age = '';
	i_year = '';
	$scope.total_salat_tax= "";
	$scope.select_image_err = false;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					$.ajax({
						url:'/get_pan_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							pan = response.pan;
						}
					});
				}
			}
		});
	},1000);

	$scope.send_mail = function() {
		// $.ajax({
		// 	url:'/send_marketing_mail/',
		// 	type:'POST',
		// 	dataType: 'json',
		// 	data:{'option':'tax_leakage_report'},
		// 	success: function(response){
		// 		console.log(response);
		// 		// pan = response.pan;
		// 	}
		// });
		// $.ajax({
		// 	url:'/send_marketing_mail/',
		// 	type:'POST',
		// 	dataType: 'json',
		// 	data:{'option':'sales_and_tax_leakage_report'},
		// 	success: function(response){
		// 		console.log(response);
		// 		// pan = response.pan;
		// 	}
		// });
		$.ajax({
			url:'/random_query/',
			type:'POST',
			dataType: 'json',
			data:{'option':''},
			success: function(response){
				console.log(response);
				// pan = response.pan;
			}
		});
	}


	$scope.nFormatter = function(num, digits) {
		var si = [{ value: 1, symbol: "" },
		{ value: 1E3, symbol: " k" },
		{ value: 1E5, symbol: " lacs" },
		{ value: 1E7, symbol: "M" },
		{ value: 1E9, symbol: "G" },
		{ value: 1E12, symbol: "T" },
		{ value: 1E15, symbol: "P" },
		{ value: 1E18, symbol: "E" }
		];
		var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
		var i;
		for (i = si.length - 1; i > 0; i--) {
			if (num >= si[i].value) {
				break;
			}
		}
		return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

}]);