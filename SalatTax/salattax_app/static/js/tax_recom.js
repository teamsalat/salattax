
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('tax_recom_controller', ['$scope', function($scope,$http,CONFIG) {
	client_id = 0;
	$scope.hra_deduction = true;
	$scope.incurred_travel_expense = true;
	$scope.children_fee = true;
	$scope.home_loan_taken = true;
	$scope.education_loan = true;
	$scope.disability = true;
	$scope.travel_expense_no = 3;
	$scope.child_deduction_no = 4;
	$scope.home_loan_no = 5;
	$scope.deduction_80c_no = 6;
	$scope.nps_no = 7;
	$scope.nps_employer_no = 8;
	$scope.deduction_80d_no = 9;
	$scope.education_loan_no = 10;
	$scope.disability_no = 11;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				$scope.email = response.email;
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					$.ajax({
						url:'/get_payment_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							if(response.paid == 1){
								$.ajax({
									url:'/get_pan_info/',
									type:'POST',
									dataType: 'json',
									data:{'client_id':client_id},
									success: function(response){
										console.log(response);
										pan = response.pan;
										dob = response.dob;
										p_age = response.p_age;
										i_year = response.i_year;
										$scope.get_tax_plan();
									}
								});
							}else
								window.location.href = "/tax_saving_account";
						}
					});
				}
			}
		});
	},1000);

	$scope.get_tax_plan = function(){
		$.ajax({
			url:'/get_tax_plan/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				// if(response.state_of_accommodation == '' || response.state_of_accommodation == 'Company_Provided')
				// 	$scope.hra_deduction = false;
				if(response.m_hra[response.financial_year - 1]=='')
					$scope.hra_deduction = false;
				if(response.total_travel_expense == 'NA' || response.total_travel_expense == '')
					$scope.incurred_travel_expense = false;
				else
					$scope.total_travel_expense_text = $scope.money_format(parseInt(response.total_travel_expense));
				if(response.principal_amt == 'NA' && response.interest_amt == 'NA')
					$scope.home_loan_taken = false;
				if(response.principal_amt == '' && response.interest_amt == '')
					$scope.home_loan_taken = false;
				if(response.education_loan == '' || parseInt(response.education_loan) < 0)
					$scope.education_loan = false;
				else
					$scope.education_loan_text = $scope.money_format(response.education_loan);
				if(response.disability == 'no' )
					$scope.disability = false;
				if(parseInt(response.nps) >= 50000 )
					$scope.nps_text = 'Congratulations you have availed the full limit.';
				else
					$scope.nps_text = 'To know more about NPS please click here.';

				var m_bs = 0;
				var m_hra = 0;
				for (var i = 0; i < response.financial_year; i++) {
					m_bs = parseInt(response.m_basic_salary[i]);
					m_hra = parseInt(response.m_hra[i]);
				}
				$scope.monthly_bs = $scope.money_format(m_bs) ;
				if(response.state_of_accommodation!='' && response.state_of_accommodation!='Company_Provided')
					$scope.total_hra_text = $scope.money_format(m_hra*12);
				var deduction_80c = 0;
				deduction_80c = deduction_80c + parseInt(response.epf);
				deduction_80c = deduction_80c + parseInt(response.school_fee);
				deduction_80c = deduction_80c + parseInt(response.lic);
				deduction_80c = deduction_80c + parseInt(response.elss);
				deduction_80c = deduction_80c + parseInt(response.ulip);
				deduction_80c = deduction_80c + parseInt(response.fd);
				deduction_80c = deduction_80c + parseInt(response.ppf);
				if(deduction_80c >= 150000 )
					$scope.deduction_80c_text = 'Congratulations you have availed the full limit';
				else
					$scope.deduction_80c_text = 'You have not yet fully utilized the 80C limit. Please invest Rs. '+$scope.money_format(150000-deduction_80c)+' to take maximum 80C deduction. Check out best options for 80C investment here';
				
				if( parseInt(response.interest_amt)>=200000 )
					$scope.interest_amt_text = '2,00,000';
				else
					$scope.interest_amt_text = $scope.money_format(parseInt(response.interest_amt));

				var child_age_4 = 0;
				var count_child_age_4 = 0;
				if(response.no_of_child == 'NA' || response.no_of_child == '')
					$scope.children_fee = false;
				else if(parseInt(response.no_of_child)==0)
					$scope.children_fee = false;
				else{
					var one_day=1000*60*60*24*365;
					for(var i = 0; i < parseInt(response.no_of_child); i++){
						var parts = response.child_dob[i].split('/');
						// Please pay attention to the month (parts[1]); JavaScript counts months from 0:
						var today = new Date(); 
						var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 
						// parseInt(response.child_age[i])>4
						if( (today-mydate)/one_day > 4 ){
							count_child_age_4 = count_child_age_4 + 1;
							child_age_4 = 1;
						}
					}
				}
				if(child_age_4==1){
					if(count_child_age_4 == 1)
						$scope.child_deduction_text = 'Rs. 1200 ';
					if(count_child_age_4 > 1)
						$scope.child_deduction_text = 'Rs. 2400 ';
				}else
					$scope.children_fee = false;

				if($scope.hra_deduction == false){
					$scope.travel_expense_no = $scope.travel_expense_no - 1;
					$scope.child_deduction_no = $scope.child_deduction_no - 1;
					$scope.home_loan_no = $scope.home_loan_no - 1;
					$scope.deduction_80c_no = $scope.deduction_80c_no - 1;
					$scope.nps_no = $scope.nps_no - 1;
					$scope.nps_employer_no = $scope.nps_employer_no - 1;
					$scope.deduction_80d_no = $scope.deduction_80d_no - 1;
					$scope.education_loan_no = $scope.education_loan_no - 1;
					$scope.disability_no = $scope.disability_no -1;
				}
				if($scope.incurred_travel_expense == false){
					$scope.child_deduction_no = $scope.child_deduction_no - 1;
					$scope.home_loan_no = $scope.home_loan_no - 1;
					$scope.deduction_80c_no = $scope.deduction_80c_no - 1;
					$scope.nps_no = $scope.nps_no - 1;
					$scope.nps_employer_no = $scope.nps_employer_no - 1;
					$scope.deduction_80d_no = $scope.deduction_80d_no - 1;
					$scope.education_loan_no = $scope.education_loan_no - 1;
					$scope.disability_no = $scope.disability_no -1;
				}
				if(child_age_4 !=1){
					$scope.home_loan_no = $scope.home_loan_no - 1;
					$scope.deduction_80c_no = $scope.deduction_80c_no - 1;
					$scope.nps_no = $scope.nps_no - 1;
					$scope.nps_employer_no = $scope.nps_employer_no - 1;
					$scope.deduction_80d_no = $scope.deduction_80d_no - 1;
					$scope.education_loan_no = $scope.education_loan_no - 1;
					$scope.disability_no = $scope.disability_no -1;
				}
				if($scope.education_loan == false){
					$scope.disability_no = $scope.disability_no -1;
				}
				$scope.$apply();
			}
		});
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return 0

		if(!isNaN(x)){
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

}]);
