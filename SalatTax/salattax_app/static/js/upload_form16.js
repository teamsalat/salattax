

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('uploadForm_controller', ['$scope', function($scope,$http,CONFIG) {
	// var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
	// var TEMP=JSON.stringify(CONFIG['TEMP']).replace(/\"/g, "");
	redirect_url = $scope.url;
	var client_id = 1;
	var form16_1_no = 1;
	var switch_form16_1 = 1;
	var form16_2_no = 1;
	var switch_form16_2 = 1;
	var form16_3_no = 1;
	var switch_form16_3 = 1;
	var selected_fy='2018-2019';

	$scope.form16_1_fileCount=0;
	$scope.form16_2_fileCount=0;
	$scope.form16_3_fileCount=0;
	$scope.form16_1_fileObj1='';
	$scope.form16_1_fileObj2='';
	$scope.form16_2_fileObj1='';
	$scope.form16_2_fileObj2='';
	$scope.form16_3_fileObj1='';
	$scope.form16_3_fileObj2='';

	$('.loader').hide();

	$scope.$watch("demoInput", function(){
		console.log('ID : '+$scope.user.id);
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				if (response.client_id!=0)
					client_id = response.client_id;
				else if ($scope.user.id == ''){
					// $scope.display_logout = {"display" : "block"};
					// $scope.display_login = {"display" : "none"};
				}
				if($scope.user.id != ''){
					client_id = $scope.user.id;
					$scope.display_logout = {"display" : "none"};
					$scope.display_login = {"display" : "block"};
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				$scope.$apply();
			}
		});
	},100);

	$scope.validation = function(type,value,$event) {
		if((type == "no_of_form16") && value!==undefined){
			if(value!=''){ 
				$scope.no_of_form16_error= "";
				// switch_form16_1 = 1;
				// switch_form16_2 = 1;
				// switch_form16_3 = 1;

				return true;
			}else{
				$scope.no_of_form16_error= "Please Select Number of Form16 Upload";
				return false;
			}
		}else if((type == "no_of_form16") && value==undefined){
			$scope.no_of_form16_error= "Please Select Number of Form16 Upload";
			return false;
		}

		if((type == "form1_passwordA" || type=="form1_passwordB" || type=="form2_passwordA" || type=="form2_passwordB" || type=="form3_passwordA" || type=="form3_passwordB") && value!==undefined){
			if(value!=''){ 
				if(type == "form1_passwordA")
					$scope.form1_passwordA_error= ""; 
				else if(type == "form1_passwordB")
					$scope.form1_passwordB_error= ""; 
				else if(type == "form2_passwordA")
					$scope.form2_passwordA_error= ""; 
				else if(type == "form2_passwordB")
					$scope.form2_passwordB_error= ""; 
				else if(type == "form3_passwordA")
					$scope.form3_passwordA_error= ""; 
				else if(type == "form3_passwordB")
					$scope.form3_passwordB_error= ""; 

				return true;
			}else
				return true;

		}else if((type == "form1_passwordA" || type=="form1_passwordB" || type=="form2_passwordA" || type=="form2_passwordB" || type=="form3_passwordA" || type=="form3_passwordB") && value==undefined){
			return true;	
		}
	}

	$scope.gotoSaveForm16 = function(user,$event) {
		var status=$scope.validation_check();
		if(status==true){
			console.log(switch_form16_1);
			console.log(switch_form16_2);
			console.log(switch_form16_3);
			if (switch_form16_1 == 1 && switch_form16_2 == 1 && switch_form16_3 == 1) {
				start_proceed();
				$.ajax({
					url:'/client_fin_info/',
					type:'POST',
					dataType: 'json',
					data:{'client_id':client_id },
					success: function(response){
						console.log(response);
						//window.location.href = redirect_url+'/personal_information/'+client_id;
						window.location.href = redirect_url+'/chooseFillingType/'+client_id;
					}
				});
			}else
				$scope.user_tracking('error in upload','uploading form16');
		}else
			$scope.user_tracking('error in data','upload form16 validation');
	}

	$scope.validation_check = function() {
		var no_of_form16_status = $scope.validation("no_of_form16",$scope.user.no_of_form16);
		var count_of_form16 = $scope.user.no_of_form16;

		// alert('Checking validation'+count_of_form16);
		if(count_of_form16==1){
			if($scope.form16_1_error =='')
				return true;
			else{
				$scope.form16_1_error = 'Upload Form16 in PDF Format';
				return false;
			}
		}else if(count_of_form16==2){
			if($scope.form16_2_error =='')
				return true;
			else{
				$scope.form16_2_error = 'Upload Form16 in PDF Format';
				return false;
			}
		}else if(count_of_form16==3){
			if($scope.form16_3_error =='')
				return true;
			else{
				$scope.form16_3_error = 'Upload Form16 in PDF Format';
				return false;
			}
		}else
			return false;
	}

	$scope.reset_filename = function() {
		form16_1_no = 1;
		count_run1 = 0;
	}
	$scope.reset_filename2 = function() {
		form16_2_no = 1;
		count_run2 = 0;
	}
	$scope.reset_filename3 = function() {
		form16_3_no = 1;
		count_run3 = 0;
	}

	$scope.upload_form16_1_error='';
	$scope.upload_form16_2_error='';
	$scope.upload_form16_3_error='';
	$('.upload_form16_1_error_cl').hide();
	$('.upload_form16_2_error_cl').hide();
	$('.upload_form16_3_error_cl').hide();

	/*var cur1_file_no = 0;
	$scope.onLoad_form16_1 = function (e, reader, file, fileList, fileOjects, fileObj) {
		
		$scope.form1_passwordA_error= "";
		$scope.form1_passwordB_error= "";
		switch_form16_1 = 0;
		var file_name =client_id+'_form16_Part'+form16_1_no;
		form16_1_no++;
		file_count = fileList.length;
		passwordA = $scope.user.form1_passwordA
		passwordB = $scope.user.form1_passwordB

		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		var expr = /.pdf/;
		
		var check_pdf = filetype.match(expr);
		// alert('check_pdf');
		
		if(check_pdf != null){
			$('.loader').show();
			$.ajax({
				url:'/file_upload/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'pan':file_name ,'type':'1'},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
					cur1_file_no++;
					console.log(file_count)
					console.log(cur1_file_no)
					////if(response['status'] == 'success' && file_count==form16_1_no-1 && count_run1==0){
					if(status == 'success' && file_count==cur1_file_no && count_run1==0){
						count_run1 = 1;
						$.ajax({
							url:'/form16_run_jar/',
							type:'POST',
							dataType: 'json',
							data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'1','client_id':client_id },
							success: function(response){
								console.log(response);
								output = JSON.stringify(response['output']).replace(/\"/g, "");
								full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
								if (output==0) {
									
									// alert('Saved in DB');
									$.ajax({
										url:'/get_DB_data/',
										type:'POST',
										dataType: 'json',
										data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
										success: function(response){
											console.log(response);
											$('.loader').hide(); 
											if(response['status']=='success'){
												if(response['year_match']=='Y'){
													$scope.update();
													switch_form16_1 = 1;
													$scope.form16_1_error= "";
												}else{
													$scope.form16_1_error= 'Mismatch in A.Y.';
													swal_message='Mismatch in Assessment Year';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
												}		
											}else{
									            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info') 
											}
										},error:function(response) {
								            $('.loader').hide(); 
								            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
								        }
									});
								}
								else if (output == 1) {
									$('.loader').hide(); 
									$scope.user_tracking('error in data','Part A missing 1st Form16');
									$scope.form16_1_error= 'Part A missing';
									//alert('Part A missing');
									swal_message='Part A missing';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
								}
								else if (output == 2) {
									$('.loader').hide(); 
									$scope.user_tracking('error in data','Part B missing 1st Form16');
									$scope.form16_1_error= 'Part B missing';
									//alert('Part B missing');
									swal_message='Part B missing';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
								}
								else if (output == -1) {
									$('.loader').hide(); 
									//alert('Something Went Wrong.');
									$scope.user_tracking('error in data','1st Form16');
									if(full_output.indexOf("InvalidPasswordException")!=-1){
										$scope.user_tracking('error in data','Invalid Password 1st Form16');
										$scope.user.form16_1 = '';
										$scope.form1_passwordA_error= "Please Enter Correct Password";
										$scope.$apply();
										swal_message='Please Enter Correct Password';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}else{
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								}
								$scope.$apply();
							},error:function(response) {
					            $('.loader').hide(); 
					            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info')
					        }
						});
					}else{
						if(file_count==cur1_file_no){
							$('.loader').hide();
							if(response['status'] != 'success'){
								console.log('hide');
								$scope.form16_1_error= "Error in Uploading";
							}
							swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
							$scope.Form16sweetalert('Ooops !',swal_message,'info')
						}
					}
				},error:function(response) {
		            $('.loader').hide();
		            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
					$scope.Form16sweetalert('Ooops !',swal_message,'info') 
		        }
			});
			$scope.form16_1_error= "";
			return true;
		}else{
			console.log('hide');
			$scope.form16_1_error= "Upload PDF file Only.";
			return false;
		}
		$scope.$apply();
	};*/

	form16_1_count=0
	$scope.onLoad_form16_1 = function (e, reader, file, fileList, fileOjects, fileObj){

		$scope.upload_form16_1_error='';
		$scope.form1_passwordA_error='';
		$scope.$apply();

		$scope.form16_1_fileCount = file_count = fileList.length;

		if(form16_1_count==0){
			$scope.form16_1_fileObj1='';
			$scope.form16_1_fileObj2='';
		}
	
		console.log(file['name']);
		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		console.log(filetype);
		var expr = /.pdf/;
		var check_pdf = filetype.match(expr);
		console.log(check_pdf);

		form16_1_count++;

		if(check_pdf != null){
			if(form16_1_count==1){
				$scope.form16_1_fileObj1=fileObj;
			}else if(form16_1_count==2){
				$scope.form16_1_fileObj2=fileObj;
			}
			
			$scope.form16_1_error= "";
			return true;
		}else{
			$scope.form16_1_error= "Upload Only PDF file.";
			return false;
		}
	}

	$scope.upload_form16_1 = function (){
		form16_1_count=0

		var form16_1_no = 1;
		var switch_form16_1 = 1;
		var cur1_file_no = 0;

		if($scope.form16_1_fileCount!=0 &&  $scope.form16_1_fileObj1!=''){
				
			for(i=1;i<=$scope.form16_1_fileCount;i++){
				switch_form16_1 = 0;
				var file_name =client_id+'_form16_Part'+form16_1_no;
				
				passwordA = $scope.user.form1_passwordA
				passwordB = $scope.user.form1_passwordB

				file_count = $scope.form16_1_fileCount;

				if(form16_1_no==1){
					var fileObj=$scope.form16_1_fileObj1;
				}else if (form16_1_no==2){
					var fileObj=$scope.form16_1_fileObj2;
				}

				var data=JSON.stringify(fileObj);

				form16_1_no++;

				$('.loader').show();
				$.ajax({
					url:'/file_upload/',
					type:'POST',
					dataType: 'json',
					data:{'data':data,'pan':file_name ,'type':'1'},
					success: function(response){
						console.log(response);
						status = JSON.stringify(response['status']).replace(/\"/g, "");
						pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
						cur1_file_no++;
						console.log(file_count)
						console.log(cur1_file_no)
						////if(response['status'] == 'success' && file_count==form16_1_no-1 && count_run1==0){
						if(response['status'] == 'success' && file_count==cur1_file_no && count_run1==0){
							count_run1 = 1;
							$.ajax({
								url:'/form16_run_jar/',
								type:'POST',
								dataType: 'json',
								data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'1','client_id':client_id },
								success: function(response){
									console.log(response);
									if(response['status']=='success'){
										output = JSON.stringify(response['output']).replace(/\"/g, "");
										full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
										switch_form16_1 = 1;
										if(output==0){
											//alert('Saved in DB');
											$.ajax({
												url:'/get_DB_data/',
												type:'POST',
												dataType: 'json',
												data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
												success: function(response){
													console.log(response);
													$('.loader').hide(); 
													//$scope.Form16_save_Employer_Details(1);
													if(response['status']=='success'){
														if(response['year_match']=='Y'){
															$scope.Form16_save_Employer_Details(1);
														}else{
															swal_message='Mismatch in Assessment Year';
															$scope.Form16sweetalert('Ooops !',swal_message,'info') 
														}		
													}else{
											            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
														$scope.Form16sweetalert('Ooops !',swal_message,'info') 
													}
													//setTimeout(function(){ window.location.reload(); }, 500);
													
												},error:function(response) {
										            $('.loader').hide(); 
										            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
										        }
											});
										}
										else if(output == 1){
											$('.loader').hide();
											//alert('Part A missing');
											swal_message='Part A missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == 2){
											$('.loader').hide();
											//alert('Part B missing');
											swal_message='Part B missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == -1){
											$('.loader').hide();
											//alert('Something Went Wrong.');
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_1 = '';
												$scope.form1_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
											
										}else{
											$('.loader').hide();
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_1 = '';
												$scope.form1_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
									}else{
										$('.loader').hide();
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								},error:function(response) {
						            $('.loader').hide(); 
						            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
						        }
							});
						}else{
							if(file_count==cur1_file_no){
								$('.loader').hide();
								swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
							}
						}
					},error:function(response) {
			            $('.loader').hide();
			            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info') 
			        }
				});
				$scope.form16_1_upload_error= "";
			}
			return true;
		}else{
			$scope.form16_1_upload_error= "Please upload correct file";
			return false;
		}
	};

	/*var cur2_file_no = 0;
	$scope.onLoad_form16_2 = function (e, reader, file, fileList, fileOjects, fileObj) {
		$scope.form2_passwordA_error= "";
		$scope.form2_passwordB_error= "";
		switch_form16_2 = 0;
		var file_name =client_id+'_form16_Part'+form16_2_no;
		form16_2_no++;
		file_count = fileList.length;
		passwordA = $scope.user.form2_passwordA
		passwordB = $scope.user.form2_passwordB

		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		var expr = /.pdf/;
		
		var check_pdf = filetype.match(expr);
		if(check_pdf != null){
			$('.loader').show();
			$.ajax({
				url:'/file_upload/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'pan':file_name,'type':'2'},
				success: function(response){
					// console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
					cur2_file_no++;
					console.log(file_count)
					console.log(cur2_file_no)
					////if(response['status'] == 'success' && file_count==form16_2_no-1 && count_run2==0){
					if(status == 'success' && file_count==cur2_file_no && count_run2==0){
						count_run2 = 1;
						$.ajax({
							url:'/form16_run_jar/',
							type:'POST',
							dataType: 'json',
							data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'2','client_id':client_id  },
							success: function(response){
								console.log(response);
								output = JSON.stringify(response['output']).replace(/\"/g, "");
								full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
								if (output==0) {
									// switch_form16_2 = 1;
									// alert('Saved in DB');
									$.ajax({
										url:'/get_DB_data/',
										type:'POST',
										dataType: 'json',
										data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
										success: function(response){
											console.log(response);
											$('.loader').hide(); 
											if(response['status']=='success'){
												if(response['year_match']=='Y'){
													$scope.update();
													switch_form16_2 = 1;
													$scope.form16_2_error= "";
												}else{
													$scope.form16_2_error= 'Mismatch in A.Y.';
													swal_message='Mismatch in Assessment Year';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
												}		
											}else{
									            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info') 
											}	
										},error:function(response) {
								            $('.loader').hide(); 
								            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
								        }
									});
								}
								else if (output == 1) {
									$('.loader').hide();
									$scope.user_tracking('error in data','Part A missing 2nd Form16');
									$scope.form16_2_error= 'Part A missing';
									//alert('Part A missing');
									swal_message='Part A missing';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
								}
								else if (output == 2) {
									$('.loader').hide();
									$scope.user_tracking('error in data','Part A missing 2nd Form16');
									$scope.form16_2_error= 'Part B missing';
									//alert('Part B missing');
									swal_message='Part B missing';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
								}
								else if (output == -1) {
									$('.loader').hide();
									//alert('Something Went Wrong.');
									$scope.user_tracking('error in data','2nd Form16');
									if(full_output.indexOf("InvalidPasswordException")!=-1){
										$scope.user_tracking('error in data','Invalid Password 2nd Form16');
										$scope.form2_passwordA_error= "Please Enter Correct Password";
										swal_message='Please Enter Correct Password';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}else{
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								}
								$scope.$apply();
							},error:function(response) {
					            $('.loader').hide();
					            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
					        }
						});
					}else{
						if(file_count==cur2_file_no){
							$('.loader').hide();
							$scope.form16_2_error= "Error in Uploading";
							swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
							$scope.Form16sweetalert('Ooops !',swal_message,'info')
						}
					}
						
				},error:function(response) {
		            $('.loader').hide(); 
		            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
					$scope.Form16sweetalert('Ooops !',swal_message,'info')
		        }
			});
			$scope.form16_2_error= "";
			return true;
		}else{
			$scope.form16_2_error= "Upload PDF file Only.";
			return false;
		}
		$scope.$apply();
	};*/

	form16_2_count=0
	$scope.onLoad_form16_2 = function (e, reader, file, fileList, fileOjects, fileObj){

		$scope.upload_form16_2_error='';
		$scope.form2_passwordA_error='';
		$scope.$apply();

		$scope.form16_2_fileCount = file_count = fileList.length;

		if(form16_2_count==0){
			$scope.form16_2_fileObj1='';
			$scope.form16_2_fileObj2='';
		}
	
		console.log(file['name']);
		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		console.log(filetype);
		var expr = /.pdf/;
		var check_pdf = filetype.match(expr);
		console.log(check_pdf);

		form16_2_count++;

		if(check_pdf != null){
			if(form16_2_count==1){
				$scope.form16_2_fileObj1=fileObj;
			}else if(form16_2_count==2){
				$scope.form16_2_fileObj2=fileObj;
			}

			$scope.form16_2_error= "";
			return true;
		}else{
			$scope.form16_2_error= "Upload Only PDF file.";
			return false;
		}
	}

	$scope.upload_form16_2 = function (){
		form16_2_count=0

		var form16_2_no = 1;
		var switch_form16_2 = 1;
		var cur2_file_no = 0;
			
		if($scope.form16_2_fileCount!=0 &&  $scope.form16_2_fileObj1!=''){

			for(i=1;i<=$scope.form16_2_fileCount;i++){	

				switch_form16_2 = 0;
				var file_name =client_id+'_form16_Part'+form16_2_no;
				
				passwordA = $scope.user.form2_passwordA
				passwordB = $scope.user.form2_passwordB

				file_count = $scope.form16_2_fileCount;

				if(form16_2_no==1){
					var fileObj=$scope.form16_2_fileObj1;
				}else if (form16_2_no==2){
					var fileObj=$scope.form16_2_fileObj2;
				}

				var data=JSON.stringify(fileObj);

				form16_2_no++;
				
				$('.loader').show();
				$.ajax({
					url:'/file_upload/',
					type:'POST',
					dataType: 'json',
					data:{'data':data,'pan':file_name ,'type':'2'},
					success: function(response){
						console.log(response);
						status = JSON.stringify(response['status']).replace(/\"/g, "");
						pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
						cur2_file_no++;
						console.log(file_count)
						console.log(cur2_file_no)
						////if(response['status'] == 'success' && file_count==form16_2_no-1 && count_run2==0){
						if(response['status'] == 'success' && file_count==cur2_file_no && count_run2==0){
							count_run2 = 1;
							$.ajax({
								url:'/form16_run_jar/',
								type:'POST',
								dataType: 'json',
								data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'2','client_id':client_id },
								success: function(response){
									console.log(response);
									if(response['status']=='success'){
										output = JSON.stringify(response['output']).replace(/\"/g, "");
										full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
										switch_form16_2 = 1;
										if(output==0){
											//alert('Saved in DB');
											$.ajax({
												url:'/get_DB_data/',
												type:'POST',
												dataType: 'json',
												data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
												success: function(response){
													console.log(response);
													$('.loader').hide(); 
													//$scope.Form16_save_Employer_Details(2);
													if(response['status']=='success'){
														if(response['year_match']=='Y'){	
															$scope.Form16_save_Employer_Details(2);
														}else{
															swal_message='Mismatch in Assessment Year';
															$scope.Form16sweetalert('Ooops !',swal_message,'info') 
														}		
													}else{
											            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
														$scope.Form16sweetalert('Ooops !',swal_message,'info') 
													}
													//setTimeout(function(){ window.location.reload(); }, 500);
												},error:function(response) {
										            $('.loader').hide();
										            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
										        }
											});
										}
										else if(output == 1){
											$('.loader').hide(); 
											//alert('Part A missing');
											swal_message='Part A missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == 2){
											$('.loader').hide(); 
											//alert('Part B missing');
											swal_message='Part B missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == -1){
											$('.loader').hide(); 
											//alert('Something Went Wrong.');
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_2 = '';
												$scope.form2_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
										else{
											$('.loader').hide();
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_2 = '';
												$scope.form2_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
									}else{
										$('.loader').hide();
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								},error:function(response) {
						            $('.loader').hide(); 
						            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
						        }
							});
						}else{
							if(file_count==cur2_file_no){
								$('.loader').hide();
								swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
							}
						}
					},error:function(response) {
			            $('.loader').hide(); 
			            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
			        }
				});
				$scope.form16_2_upload_error= "";
				
			}
			return true;
		}else{
			$scope.form16_2_upload_error= "Please upload correct file";
			return false;
		}
	};

	/*var cur3_file_no = 0;
	$scope.onLoad_form16_3 = function (e, reader, file, fileList, fileOjects, fileObj) {
		$scope.form3_passwordA_error= "";
		$scope.form3_passwordB_error= "";
		switch_form16_3 = 0;
		var file_name =client_id+'_form16_Part'+form16_3_no;
		form16_3_no++;
		file_count = fileList.length;
		passwordA = $scope.user.form3_passwordA
		passwordB = $scope.user.form3_passwordB

		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		var expr = /.pdf/;
		
		var check_pdf = filetype.match(expr);
		$scope.form16_3_error= "";
		if(check_pdf != null){
			$('.loader').show();
			$.ajax({
				url:'/file_upload/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'pan':file_name,'type':'3'},
				success: function(response){
					// console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
					cur3_file_no++;
					console.log(file_count)
					console.log(cur3_file_no)
					////if(response['status'] == 'success' && file_count==form16_3_no-1 && count_run3==0){
					if(status == 'success' && file_count==cur3_file_no && count_run3==0){
						count_run3 = 1;
						$.ajax({
							url:'/form16_run_jar/',
							type:'POST',
							dataType: 'json',
							data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'3','client_id':client_id  },
							success: function(response){
								console.log(response);
								output = JSON.stringify(response['output']).replace(/\"/g, "");
								full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
								if (output==0) {
									// switch_form16_3 = 1;
									// alert('Saved in DB');
									$.ajax({
										url:'/get_DB_data/',
										type:'POST',
										dataType: 'json',
										data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
										success: function(response){
											console.log(response);
											$('.loader').hide(); 
											if(response['status']=='success'){
												if(response['year_match']=='Y'){
													$scope.update();
													switch_form16_3 = 1;
													$scope.form16_3_error= "";
												}else{
													$scope.form16_3_error= 'Mismatch in A.Y.';
													swal_message='Mismatch in Assessment Year';
													$scope.Form16sweetalert('Ooops !',swal_message,'info') 
												}		
											}else{
									            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info') 
											}
										},error:function(response) {
								            $('.loader').hide(); 
								            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
								        }
									});
								}
								else if (output == 1) {
									$('.loader').hide();
									//alert('Part A missing');
									$scope.form16_3_error= 'Part A missing';
									$scope.user_tracking('error in data','Part A missing 3rd Form16');
									swal_message='Part A missing';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
								}
								else if (output == 2) {
									$('.loader').hide();
									//alert('Part B missing');
									$scope.form16_3_error= 'Part B missing';
									$scope.user_tracking('error in data','Part B missing 3rd Form16');
									swal_message='Part B missing';
									$scope.Form16sweetalert('Ooops !',swal_message,'info')
								}
								else if (output == -1) {
									$('.loader').hide();
									//alert('Something Went Wrong.');
									$scope.user_tracking('error in data','3rd Form16');
									if(full_output.indexOf("InvalidPasswordException")!=-1){
										$scope.user_tracking('error in data','Invalid Password 3rd Form16');
										$scope.form3_passwordA_error= "Please Enter Correct Password";
										swal_message='Please Enter Correct Password';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}else{
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								}
								$scope.$apply();
							},error:function(response) {
					            $('.loader').hide();
					            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
					        }
						});
					}else{
						if(file_count==cur3_file_no){
							$('.loader').hide();
							$scope.form16_3_error= "Error in Uploading";
							swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
							$scope.Form16sweetalert('Ooops !',swal_message,'info')
						}
					}
						
				},error:function(response) {
		            $('.loader').hide();
		            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
					$scope.Form16sweetalert('Ooops !',swal_message,'info') 
		        }
			});
			$scope.form16_3_error= "";
			return true;
		}else{
			$scope.form16_3_error= "Upload PDF file Only.";
			return false;
		}
	};*/

	form16_3_count=0
	$scope.onLoad_form16_3 = function (e, reader, file, fileList, fileOjects, fileObj){

		$scope.upload_form16_3_error='';
		$scope.form3_passwordA_error='';
		$scope.$apply();

		if(form16_3_count==0){
			$scope.form16_3_fileObj1='';
			$scope.form16_3_fileObj2='';
		}

		$scope.form16_3_fileCount = file_count = fileList.length;
	
		console.log(file['name']);
		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		console.log(filetype);
		var expr = /.pdf/;
		var check_pdf = filetype.match(expr);
		console.log(check_pdf);

		form16_3_count++;

		if(check_pdf != null){
			if(form16_3_count==1){
				$scope.form16_3_fileObj1=fileObj;
			}else if(form16_3_count==2){
				$scope.form16_3_fileObj2=fileObj;
			}

			$scope.form16_3_error= "";
			return true;
		}else{
			$scope.form16_3_error= "Upload Only PDF file.";
			return false;
		}
	}

	$scope.upload_form16_3 = function (e, reader, file, fileList, fileOjects, fileObj){
		form16_3_count=0
			
		var form16_3_no = 1;
		var switch_form16_3 = 1;
		var cur3_file_no = 0;

		if($scope.form16_3_fileCount!=0 &&  $scope.form16_3_fileObj1!=''){

			for(i=1;i<=$scope.form16_3_fileCount;i++){	

				switch_form16_3 = 0;
				var file_name =client_id+'_form16_Part'+form16_3_no;
				
				passwordA = $scope.user.form3_passwordA
				passwordB = $scope.user.form3_passwordB

				file_count = $scope.form16_3_fileCount;

				if(form16_3_no==1){
					var fileObj=$scope.form16_3_fileObj1;
				}else if(form16_3_no==2){
					var fileObj=$scope.form16_3_fileObj2;
				}

				var data=JSON.stringify(fileObj);

				form16_3_no++;
				
				$('.loader').show();
				$.ajax({
					url:'/file_upload/',
					type:'POST',
					dataType: 'json',
					data:{'data':data,'pan':file_name ,'type':'3'},
					success: function(response){
						console.log(response);
						status = JSON.stringify(response['status']).replace(/\"/g, "");
						pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
						cur3_file_no++;
						console.log(file_count)
						console.log(cur3_file_no)
						////if(response['status'] == 'success' && file_count==form16_3_no-1 && count_run3==0){
						if(response['status'] == 'success' && file_count==cur3_file_no && count_run3==0){
							count_run3 = 1;
							$.ajax({
								url:'/form16_run_jar/',
								type:'POST',
								dataType: 'json',
								data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'3','client_id':client_id },
								success: function(response){
									console.log(response);
									if(response['status']=='success'){
										output = JSON.stringify(response['output']).replace(/\"/g, "");
										full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
										switch_form16_3 = 1;
										if(output==0){
											//alert('Saved in DB');
											$.ajax({
												url:'/get_DB_data/',
												type:'POST',
												dataType: 'json',
												data:{'path':'pdf_name','client_id':client_id,'selected_fy':selected_fy},
												success: function(response){
													console.log(response);
													$('.loader').hide(); 
													//$scope.Form16_save_Employer_Details(3);
													if(response['status']=='success'){
														if(response['year_match']=='Y'){	
															$scope.Form16_save_Employer_Details(3);
														}else{
															swal_message='Mismatch in Assessment Year';
															$scope.Form16sweetalert('Ooops !',swal_message,'info') 
														}		
													}else{
											            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
														$scope.Form16sweetalert('Ooops !',swal_message,'info') 
													}
													//setTimeout(function(){ window.location.reload(); }, 500);
												},error:function(response) {
										            $('.loader').hide(); 
										            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
													$scope.Form16sweetalert('Ooops !',swal_message,'info')
										        }
											});
										}
										else if(output == 1){
											$('.loader').hide(); 
											//alert('Part A missing');
											swal_message='Part A missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == 2){
											$('.loader').hide(); 
											//alert('Part B missing');
											swal_message='Part B missing';
											$scope.Form16sweetalert('Ooops !',swal_message,'info')
										}
										else if(output == -1 || output == 'Exiting Now....'){
											$('.loader').hide(); 
											//alert('Something Went Wrong.');
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_3 = '';
												$scope.form3_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
										else{
											$('.loader').hide();
											if(full_output.indexOf("InvalidPasswordException")!=-1){
												$scope.user.form16_3 = '';
												$scope.form3_passwordA_error= "Please Enter Correct Password";
												$scope.$apply();
												swal_message='Please Enter Correct Password';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}else{
												swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
												$scope.Form16sweetalert('Ooops !',swal_message,'info')
											}
										}
									}else{
										$('.loader').hide();
										swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
										$scope.Form16sweetalert('Ooops !',swal_message,'info')
									}
								},error:function(response) {
						            $('.loader').hide();
						            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
									$scope.Form16sweetalert('Ooops !',swal_message,'info') 
						        }
							});
						}else{
							if(file_count==cur3_file_no){
								$('.loader').hide();
								swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
								$scope.Form16sweetalert('Ooops !',swal_message,'info') 
							}
						}
					},error:function(response) {
			            $('.loader').hide(); 
			            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
			        }
				});
				$scope.form16_3_upload_error= "";
				
			}
			return true;
		}else{
			$scope.form16_3_upload_error= "Please upload correct file";
			return false;
		}
	
	};

	//this function is not use 
	//insted of this use Form16_save_Employer_Details()
	$scope.update = function(){
		$.ajax({
			url:'/update_DB/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id,'option':'insert'},
			success: function(response){
				console.log(response);
				if(response.status != undefined){
					if(response.status == 'success'){
						swal_message='Awesome! Your Form 16 was read successfully and the data has been updated for relevant fields. This easily saved 10 minutes of your valuable time.';
						$scope.Form16sweetalert('Success',swal_message,'success');
					}else{
						swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
					}
				}else{
					swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
					$scope.Form16sweetalert('Ooops !',swal_message,'info')
				}
			},error:function(response) {
	            swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
				$scope.Form16sweetalert('Ooops !',swal_message,'info') 
		    }
		});
	}

	$scope.Form16_save_Employer_Details = function(form16_no){
		$.ajax({
			url:'/edit_Employer_Details/',
			type:'POST',
			dataType: 'json',
			data:{'option':'add','client_id':client_id,'form16_no':form16_no},
			success: function(response){
				console.log(response);
				if(response.status != undefined){
					if(response.status == 'success'){
						//alert('Awesome! Your Form 16 was read successfully and the data has been updated for relevant fields. This easily saved 10 minutes of your valuable time.')
						if(form16_no==1){
							$('.upload_form16_1_error_cl').show();
				          	$scope.upload_form16_1_color="text_color_success";
				          	$scope.upload_form16_1_error='Form 16 uploaded';
				          	$scope.$apply();
						}else if(form16_no==2){
							$('.upload_form16_2_error_cl').show();
				          	$scope.upload_form16_2_color="text_color_success";
				          	$scope.upload_form16_2_error='Form 16 uploaded';
				          	$scope.$apply();
						}else if(form16_no==3){
							$('.upload_form16_3_error_cl').show();
				          	$scope.upload_form16_3_color="text_color_success";
				          	$scope.upload_form16_3_error='Form 16 uploaded';
				          	$scope.$apply();
						}
						swal_message='Awesome! Your Form 16 was read successfully and the data has been updated for relevant fields. This easily saved 10 minutes of your valuable time.';
						$scope.Form16sweetalert('Success',swal_message,'success');
					}else{
						//alert('Oops! there was some problem in reading your Form 16. This typically happens if ')
						swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
						$scope.Form16sweetalert('Ooops !',swal_message,'info')
					}
				}else{
					swal_message='There was some problem in reading your Form 16. <p>This typically happens if <br>a) the pdf is not in a readable format<br>b) It could not find Part A and B of form 16 or<br>c) we are just having a bad day.<br><br>You can still go ahead and fill the data manually.';
					$scope.Form16sweetalert('Ooops !',swal_message,'info')
				}
				
			}
		});
	}

	$scope.Form16sweetalert=function(title,message,status){

		console.log(title)
		swal({
		      title: title,
		      text: message,
		      type: status,
		      showCancelButton: false,
		      closeOnConfirm: false,
		      html: true,
		      showConfirmButton:true,
		      confirmButtonText: "OK",
		}, function(isConfirm) {
		    if(isConfirm){
		    	if(status=='success'){
		    		//window.location.reload();
		    		swal.close();
		    	}else{
		    		//window.location.reload();
		    		swal.close();
		    	}
	        }
		})
	}

	$scope.match_ay = function(db_ay,read_ay){
		var temp_db_ay = '';
		var temp_read_ay = '';

		if(db_ay != ''){
			n = db_ay.includes("-");
			if(n)
				temp_db_ay = db_ay.split('-')[0];
		}
		if(read_ay != ''){
			n = read_ay.includes("-");
			if(n)
				temp_read_ay = read_ay.split('-')[0];
		}

		if(temp_read_ay==temp_db_ay)
			return 1;
		else
			return 0;
	}

}]);