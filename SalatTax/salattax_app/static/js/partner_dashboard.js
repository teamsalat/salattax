var dashapp = angular.module('partner_dashboard_app', ['naif.base64']);

dashapp.config(function ($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});


dashapp.controller('partner_dashboard_ctrl', function($rootScope,$scope,$http,$parse,$sce,$compile,$window) {

    //$scope.partnerId = $window.partnerId;

    //alert($scope.partnerId)

    domainName = document.location.origin;
    $scope.url = domainName;

    $scope.user={};

    $scope.user.b_id='';
    $scope.user.search='';

    $rootScope.no_of_pans='';
    $rootScope.registered_pans='';
    $rootScope.generated_xml='';
    $rootScope.returns_filed_successfully='';
    $rootScope.returns_filing_unsuccessful='';
   
    console.log('Partner Dashboard')

    $('.loader').hide();

    var eamil_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var mobile_re = /^[6789]\d{9}$/;
    var pan_re = /[A-Z]{5}\d{4}[A-Z]{1}/;
    var pincode_re = /^\d{6}$/;
    var ifsc_re = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;


    $scope.reset=function(){
      $scope.user.search='';
      $scope.load_page(1,'onload');
    }


    $scope.apply_datatable=function(jsonData){

      console.log('apply_datatable');

      $('#example-datatable').dataTable().fnClearTable();
      $('#example-datatable').dataTable().fnDestroy();

      var tbody_container=angular.element(document.querySelector('#example-datatable1'));

      i=0

      if($scope.user.business_type=='Admin'){
        ////Admin Dashboard
        for (var key in jsonData) 
        {
            //console.log(jsonData[key].client_name)
            //console.log(jsonData[key].xml_generated)
            markup='<tr><td>'+jsonData[key].client_name+'</td><td>'+jsonData[key].pan+'</td><td>'+jsonData[key].partner_name+'</td><td>'+jsonData[key].expert_assisted+'</td><td>'+jsonData[key].salat_tax_registration+'</td><td>'+jsonData[key].as26_processing+'</td><td>'+jsonData[key].client_confirmation+'</td><td>'+jsonData[key].xml_generated+'</td><td>'+jsonData[key].payment_status+'</td><td>'+jsonData[key].return_filing+'</td><td>'+jsonData[key].itr_varification+'</td></tr>'
            var elem = $compile(markup)($scope);
            tbody_container.append(elem);
        }
      }else{
        ////Partner Dashboard
        for (var key in jsonData) 
        {
            //console.log(jsonData[key].client_name)
            // console.log(jsonData[key].client_name)
            markup='<tr><td>'+jsonData[key].client_name+'</td><td>'+jsonData[key].pan+'</td><td>'+jsonData[key].salat_tax_registration+'</td><td>'+jsonData[key].as26_processing+'</td><td>'+jsonData[key].client_confirmation+'</td><td>'+jsonData[key].xml_generated+'</td><td>'+jsonData[key].payment_status+'</td><td>'+jsonData[key].return_filing+'</td><td>'+jsonData[key].itr_varification+'</td></tr>'
            var elem = $compile(markup)($scope);
            tbody_container.append(elem);
        }
      }


      $('#example-datatable').DataTable({
        
        "columnDefs": [
              { "width": "20px", "targets": [0] },
              { "width": "10px", "targets": [2] },
              {"className": "dt-center", "targets": "_all"}
           ],
          "ordering": false,
          searching: false, 
          paging: false,
          autoWidth: true,
          aaSorting:[],
          "dom": 'Bfrtip',
              "buttons": ['excel']
          
      });

    }

    
    $scope.search=function(e,input){
      console.log('search called')
      console.log(input)
      console.log(input.length)
      AssessmentYear=$scope.user.AssessmentYear;
      if(input.length>=3){
        console.log('search called')
        var b_id=$scope.user.b_id;

        $.ajax({

            url:'/search_client_on_dashboard/',
            type:'POST',
            dataType: 'json',
            async:false,
            data:{'b_id':b_id,'input':input,'AssessmentYear':AssessmentYear},
            success: function(response)
            {

              var jsonData=response.status.search_data

              console.log(response)

              console.log(response.listing)

              $('.pagination1').hide();
              $('.pagination').show();
              $('.pagination').html('');

              var pagination=angular.element(document.querySelector('#pagination'));

              markup=response.listing
              var elem = $compile(markup)($scope);
              pagination.append(elem);

              $scope.apply_datatable(jsonData)

            },error:function(response) {

              console.log(response)
             
            }


        });

      }

    }

    $scope.search_on_enter=function(e,input){

      console.log('search_on_enter')
      console.log(e.keyCode)
      console.log(input)

      if(e.keyCode==13){
        console.log('search')
        $scope.search(e,input)
      }

    }

    $scope.load_page=function(page,keyword){

      console.log('load_page');

      console.log($scope.user.username)
      console.log($scope.user.b_id)
      console.log($scope.user.search)
      AssessmentYear=$scope.user.AssessmentYear;
      $('.loader').show();
      $.ajax({

          url:'/pagination/',
          type:'POST',
          async:false,
          dataType: 'json',
          data:{'current_page':page,'b_id':$scope.user.b_id,'input':$scope.user.search,'keyword':keyword,'AssessmentYear':AssessmentYear},
          success: function(response){
            $('.loader').hide();
            console.log(response)

            if(response.data_counts!=undefined){

              console.log(response.data_counts.pan_count);

              /*$rootScope.no_of_pans=response.data_counts.pan_count;
              $rootScope.registered_pans=response.data_counts.registered_pan_count;
              $rootScope.generated_xml=response.data_counts.generated_xml_count;
              $rootScope.returns_filed_successfully=response.data_counts.returns_filed_successfully;
              $rootScope.returns_filing_unsuccessful=response.data_counts.returns_filing_unsuccessful;
              $scope.$apply();*/

              $('#no_of_pans').html(response.data_counts.pan_count);
              $("#registered_pans").html(response.data_counts.registered_pan_count);
              $("#generated_xml").html(response.data_counts.generated_xml_count);
              $("#returns_filed_successfully").html(response.data_counts.returns_filed_successfully);
              $("#returns_filing_unsuccessful").html(response.data_counts.returns_filing_unsuccessful);
              
            }


            if(keyword=='onload'){
              $scope.apply_datatable(response.status.search_data);
            }else if(keyword=='search'){
              $scope.apply_datatable(response.status.result);
            }

            // console.log(response.status.listing)
            $('.pagination1').hide();
            $('.pagination').show();
            $('.pagination').html('');

            var pagination=angular.element(document.querySelector('#pagination'));

            markup=response.status.listing
            var elem = $compile(markup)($scope);
            pagination.append(elem);

            console.log(response.status.pan_count)


          },error:function(response) {
            $('.loader').hide();
            console.log(response.status)
             
          }

      });
    
    }

    $scope.add_new_client=function(){

      console.log('add_new_client');

      console.log($scope.user.b_id)
      console.log($scope.user.search)
     
      $popup = $window.open('/salat_reg/')
      //$popup.b_id = b_id;
    
    }

    $scope.MarkFiled=function(rid,cp){

        console.log('MarkFiled')
        AssessmentYear=$scope.user.AssessmentYear;
        var b_id=$scope.user.b_id;
        keyword='onload'

        $.ajax({

            url:'/mark_filed/',
            type:'POST',
            dataType: 'json',
            async:false,
            data:{'b_id':b_id,'rid':rid,'current_page':cp,'input':$scope.user.search,'keyword':keyword,'AssessmentYear':AssessmentYear},
            success: function(response)
            {

              if(keyword=='onload'){
                $scope.apply_datatable(response.status.search_data);
              }else if(keyword=='search'){
                $scope.apply_datatable(response.status.result);
              }

              // console.log(response.status.listing)
              $('.pagination1').hide();
              $('.pagination').show();
              $('.pagination').html('');

              var pagination=angular.element(document.querySelector('#pagination'));

              markup=response.status.listing
              var elem = $compile(markup)($scope);
              pagination.append(elem);

            },error:function(response) {

              console.log(response)
             
            }


        });

    }

    $scope.topup=function(){

      console.log('topup');

      $popup = $window.open('/partner_deposit/')
    
    }

    ////////////////////////Earning////////////////////////////////////////////

    $scope.earning_reset=function(){
      $scope.user.earning_search='';
      $scope.load_earning_page(1,'onload');
    }

    $scope.apply_earning_datatable=function(jsonData){

      console.log('apply_earning_datatable');

      $('#earning-datatable').dataTable().fnClearTable();
      $('#earning-datatable').dataTable().fnDestroy();

      var earning_tbody_container=angular.element(document.querySelector('#earning-datatable-body'));

      i=0

      if($scope.user.business_type=='Admin'){
        ////Admin Dashboard
        for (var key in jsonData) 
        {
            //console.log(jsonData[key].client_name)
            //console.log(jsonData[key].xml_generated)
            markup='<tr><td>'+jsonData[key].client_name+'</td><td>'+jsonData[key].pan+'</td><td>'+jsonData[key].partner_name+'</td><td>'+jsonData[key].engagement_type  +'</td><td>'+jsonData[key].return_filing+'</td><td>'+jsonData[key].return_filing_fee+'</td><td>'+jsonData[key].partner_earnig+'</td><td>'+jsonData[key].settlement_status+'</td></tr>'
            var elem = $compile(markup)($scope);
            earning_tbody_container.append(elem);
        }
      }else{
        ////Partner Dashboard
        for (var key in jsonData) 
        {
            //console.log(jsonData[key].client_name)
            // console.log(jsonData[key].client_name)
            markup='<tr><td>'+jsonData[key].client_name+'</td><td>'+jsonData[key].pan+'</td><td>'+jsonData[key].engagement_type  +'</td><td>'+jsonData[key].return_filing+'</td><td>'+jsonData[key].return_filing_fee+'</td><td>'+jsonData[key].partner_earnig+'</td><td>'+jsonData[key].settlement_status+'</td></tr>'
            var elem = $compile(markup)($scope);
            earning_tbody_container.append(elem);
        }
      }


      $('#earning-datatable').DataTable({
        
        "columnDefs": [
              { "width": "20px", "targets": [0] },
              { "width": "10px", "targets": [2] },
              {"className": "dt-center", "targets": "_all"}
           ],
          "ordering": false,
          searching: false, 
          paging: false,
          autoWidth: true,
          aaSorting:[],
          "dom": 'Bfrtip',
              "buttons": ['excel']
          
      });

    }

    $scope.earning_search=function(e,input){
      console.log('earning_search called')
      console.log(input)
      console.log(input.length)
      AssessmentYear=$scope.user.AssessmentYear;
      if(input.length>=3){
        console.log('earning_search called')
        var b_id=$scope.user.b_id;

        $.ajax({

            url:'/search_client_on_earnings/',
            type:'POST',
            dataType: 'json',
            async:false,
            data:{'b_id':b_id,'input':input,'AssessmentYear':AssessmentYear},
            success: function(response)
            {
              var jsonData=response.status.earning_data
              console.log(response)
              console.log(response.listing)

              $('#earning-pagination1').hide();
              $('#earning-pagination').show();
              $('#earning-pagination').html('');

              var earningPagination=angular.element(document.querySelector('#earning-pagination'));

              markup=response.listing
              var elem = $compile(markup)($scope);
              earningPagination.append(elem);
              $scope.apply_earning_datatable(jsonData)

            },error:function(response) {

              console.log(response)
            }
        });

      }

    }

    $scope.earning_search_on_enter=function(e,input){

      console.log('earning_search_on_enter')
      console.log(e.keyCode)
      console.log(input)

      if(e.keyCode==13){
        $scope.earning_search(e,input)
      }

    }

    $scope.load_earning_page=function(page,keyword){

      console.log('load_earning_page');

      console.log($scope.user.username)
      console.log($scope.user.b_id)
      console.log($scope.user.search)
      AssessmentYear=$scope.user.AssessmentYear;
      $('.loader').show();
      $.ajax({

          url:'/earning_pagination/',
          type:'POST',
          async:false,
          dataType: 'json',
          data:{'current_page':page,'b_id':$scope.user.b_id,'input':$scope.user.search,'keyword':keyword,'AssessmentYear':AssessmentYear},
          success: function(response){
            $('.loader').hide();
            console.log(response)

            if(response.earning_summary!=undefined){

              $('#er_no_of_returns_filed').html(response.earning_summary.return_filed_count);
              $("#er_filing_earnings").html(response.earning_summary.filing_earnings);
              $("#er_no_of_referrals").html(response.earning_summary.no_of_referrals);
              $("#er_referral_earnings").html(response.earning_summary.referral_earnings);
              $("#er_total_earnings").html(response.earning_summary.total_earnings);
              
            }

            if(keyword=='onload'){
              $scope.apply_earning_datatable(response.status.earning_data);
            }else if(keyword=='search'){
              $scope.apply_earning_datatable(response.status.result);
            }

            // console.log(response.status.listing)
            $('#earning-pagination1').hide();
            $('#earning-pagination').show();
            $('#earning-pagination').html('');

            var earningPagination=angular.element(document.querySelector('#earning-pagination'));

            markup=response.status.listing
            var elem = $compile(markup)($scope);
            earningPagination.append(elem);

          },error:function(response) {
            $('.loader').hide();
            console.log(response.status)
            $scope.load_earning_page(1,'onload');
             
          }

      });
    
    }

    $(window).load(function() {
      $scope.load_page(1,'onload');
      $scope.load_earning_page(1,'onload');
    });



    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }

    $scope.user_tracking =function(event,event_name){
        $.ajax({
            url:'/user_tracking_func/',
            type:'POST',
            dataType: 'json',
            data:{'event':event,'event_name':event_name,'client_id':client_id_main,'UserId':UserId},
            success: function(response){
                console.log(response);
            },error:function(response){
                console.log(response)
            }
        });
    }

    

 
   


});