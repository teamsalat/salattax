  $(document).ready(function(){
    if ($(window).width() > 768){ 
      $(".core-menu li").hover(
      function(){
        //i used the parent ul to show submenu
          $(this).children('ul').slideDown('fast');
      }, 
        //when the cursor away 
      function () {
          $('ul', this).slideUp('fast');
      });
  }

  //this feature only show on 600px device width
    $(".mobile-e").click(function(){
      $('#myDropdown').hide();
      $('#myDropdown_mobile').hide();
      if ($(this).siblings('ul').hasClass('open')){
        $(this).siblings('ul').removeClass('open').slideToggle();
      }else{
        $(this).siblings('ul').addClass('open').slideToggle();
      }
    
    });

    //submenu of service
    $(".income_hover").click(function(){
      $('#tax_mobile').slideToggle('fast');
      if ($(this).siblings('ul').hasClass('open')){
        $(this).siblings('ul').addClass('open').slideToggle();
      }else{
        $(this).siblings('ul').removeClass('open').slideToggle();
      }
    });

  //hover on section on the page
    $('.row-main').hover(
      function(){
        //i used the parent ul to show submenu
        $('#income_dropdown').hide();
        $('#myDropdown').hide();
        $('#myDropdown_mobile').hide();
    });

    //mobile profile icon click
    $(".img_css").click(function(){
      $("#myDropdown_mobile").slideToggle("fast");
      $(".collapse").removeClass('in');
      // console.log('in');
    });

    //click on business login
    $(".mobile_log").click(function(){
      $(".collapse").removeClass('in');
      // console.log('in');
    });

    //navbar icon
    $('.ion-android-menu').click(function(){
      $("#myDropdown").hide();
      $("#myDropdown_mobile").hide();
    })

    $("#tax_income_dropdown").click(function(){
      $(".collapse").removeClass('in');
      // console.log('in');
    });

});