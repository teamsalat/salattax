
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('tax_analytics_report_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {
	close_model = 0;
	client_id = 0;
	pan = '';
	p_age = '';
	i_year = '';
 
	$scope.tds_26as_14 = '';
	$scope.tds_26as_15 = '';
	$scope.tds_26as_16 = '';
	$scope.tds_26as_17 = '';
	$scope.tds_26as_18 = '';
	$scope.tds_itr_14 = '';
	$scope.tds_itr_15 = '';
	$scope.tds_itr_16 = '';
	$scope.tds_itr_17 = '';
	$scope.tds_itr_18 = '';
	$scope.div_2014 = true;
	$scope.div_2015 = true;
	$scope.div_2016 = true;
	$scope.div_2017 = true;
	$scope.div_2018 = true;
	$scope.col_sal = true;
	$scope.col_prp = true;
	$scope.col_cg = true;
	$scope.col_os = true;
	$scope.col_bp = true;
	$scope.tds_mismatch_div = true;
	$scope.tds_mismatch_point = '3.';
	$scope.c_80_14_text = '';
	$scope.d_80_14_text = '';
	$scope.ccd_80_14_text = '';
	$scope.c_80_15_text = '';
	$scope.d_80_15_text = '';
	$scope.ccd_80_15_text = '';
	$scope.c_80_16_text = '';
	$scope.d_80_16_text = '';
	$scope.ccd_80_16_text = '';
	$scope.c_80_17_text = '';
	$scope.d_80_17_text = '';
	$scope.ccd_80_17_text = '';
	$scope.c_80_18_text = '';
	$scope.d_80_18_text = '';
	$scope.ccd_80_18_text = '';
	$scope.c_hover_14 = false;
	$scope.d_hover_14 = false;
	$scope.ccd_hover_14 = false;
	$scope.c_hover_15 = false;
	$scope.d_hover_15 = false;
	$scope.ccd_hover_15 = false;
	$scope.c_hover_16 = false;
	$scope.d_hover_16 = false;
	$scope.ccd_hover_16 = false;
	$scope.c_hover_17 = false;
	$scope.d_hover_17 = false;
	$scope.ccd_hover_17 = false;
	$scope.c_hover_18 = false;
	$scope.d_hover_18 = false;
	$scope.ccd_hover_18 = false;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				$scope.email = response.email;
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					$.ajax({
						url:'/get_payment_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							if(response.paid == 1){
								$.ajax({
									url:'/get_pan_info/',
									type:'POST',
									dataType: 'json',
									data:{'client_id':client_id},
									success: function(response){
										console.log(response);
										$scope.full_name = response.name.replace('|',' ');
										$scope.full_name = $scope.full_name.replace('|',' ');
										$scope.client_name = titleCase($scope.full_name);
										pan = response.pan;
										dob = response.dob;
										p_age = response.p_age;
										i_year = response.i_year;
										if(dob=='' || (p_age==null || p_age=='') || (response.no_children==null || response.no_children=='') ){
											alert('Please register yourself as Salat Client.');
											window.location.href = "/salat_reg";
										}else{
											$scope.get_26as(pan,dob);
											$scope.cal_tax(pan,p_age,i_year);
											$scope.show_26as(pan);
											setTimeout(function(){
												$scope.tds_mismatch();
											}, 3000);
										}
									}
								});
							}else
								window.location.href = "/tax_saving_account";
						}
					});
				}
			}
		});
	},1000);

	$scope.cal_tax = function(pan,p_age,i_year){
		var pan_status = false;
		var invested_share = 'no';
		var shares_year = i_year;
		var age_status = false;
		var shares_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";
		
		if(p_age==null || p_age=="undefined" )
			$scope.age_err= "Parent's Age is required";
		else if(p_age ==''){
			$scope.age_err= "";
			age_status = true;
		}else{
			$scope.age_err= "";
			age_status = true;
		}

		if(i_year==''){
			invested_share = 'no';
			shares_status = true;
		}
		else{
			invested_share = 'yes';
			shares_status = true;
		}
		// alert(pan_status);
		// alert(age_status);
		// alert(shares_status);
		if( pan_status == true && age_status == true && shares_status==true){
			$.ajax({
				url:'/calculate_salat_tax/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'p_age':p_age,'invested_share':invested_share,'shares_year':shares_year},
				success: function(response){
					console.log(response);
					if(response.count_salattax == 0)
						window.location.href = "/tax_saving_account";
					var val_i_14 = 0;
					var val_i_15 = 0;
					var val_i_16 = 0;
					var val_i_17 = 0;
					var val_i_18 = 0;
					var s_obj = response.salat_tax ;
					var tax_paid_total = 0;
					var tp_14 = 0 ;
					var tp_15 = 0 ;
					var tp_16 = 0 ;
					var tp_17 = 0 ;
					var tp_18 = 0 ;
					var st_14 = 0 ;
					var st_15 = 0 ;
					var st_16 = 0 ;
					var st_17 = 0 ;
					var st_18 = 0 ;
					var tl_14 = 0 ;
					var tl_15 = 0 ;
					var tl_16 = 0 ;
					var tl_17 = 0 ;
					var tl_18 = 0 ;
					var data_2014 = 0;
					var data_2015 = 0;
					var data_2016 = 0;
					var data_2017 = 0;
					var data_2018 = 0;

					$scope.count_year = response.count_salattax;

					for(var i = 0; i < response.count_salattax; i++) {
						tax_paid_total = tax_paid_total + s_obj[i].tax_paid;
						var ToTax = $scope.money_format(s_obj[i].ToTax);
						var tax_leakage = $scope.money_format(s_obj[i].tax_leakage*-1);

						if(s_obj[i].year=='2014'){
							data_2014 = 1;
							$scope.total_tax_paid_14 = $scope.money_format(s_obj[i].tax_paid);
							tp_14 = s_obj[i].tax_paid;
							st_14 = s_obj[i].ToTax;
							tl_14 = s_obj[i].tax_leakage*-1;
							$scope.salat_tax_14 = ToTax;
							$scope.tax_leakage_14 = tax_leakage;
							val_i_14 = s_obj[i].total_income_chart;
						}
						if(s_obj[i].year=='2015'){
							data_2015 = 1;
							$scope.total_tax_paid_15 = $scope.money_format(s_obj[i].tax_paid);
							tp_15 = s_obj[i].tax_paid;
							st_15 = s_obj[i].ToTax;
							tl_15 = s_obj[i].tax_leakage*-1;
							$scope.salat_tax_15 = ToTax;
							$scope.tax_leakage_15 = tax_leakage;
							val_i_15 = s_obj[i].total_income_chart;
						}
						if(s_obj[i].year=='2016'){
							data_2016 = 1;
							$scope.total_tax_paid_16 = $scope.money_format(s_obj[i].tax_paid);
							tp_16 = s_obj[i].tax_paid;
							st_16 = s_obj[i].ToTax;
							tl_16 = s_obj[i].tax_leakage*-1;
							$scope.salat_tax_16 = ToTax;
							$scope.tax_leakage_16 = tax_leakage;
							val_i_16 = s_obj[i].total_income_chart;
						}
						if(s_obj[i].year=='2017'){
							data_2017 = 1;
							$scope.total_tax_paid_17 = $scope.money_format(s_obj[i].tax_paid);
							tp_17 = s_obj[i].tax_paid;
							st_17 = s_obj[i].ToTax;
							tl_17 = s_obj[i].tax_leakage*-1;
							$scope.salat_tax_17 = ToTax;
							$scope.tax_leakage_17 = tax_leakage;
							val_i_17 = s_obj[i].total_income_chart;
						}
						if(s_obj[i].year=='2018'){
							data_2018 = 1;
							$scope.total_tax_paid_18 = $scope.money_format(s_obj[i].tax_paid);
							tp_18 = s_obj[i].tax_paid;
							st_18 = s_obj[i].ToTax;
							tl_18 = s_obj[i].tax_leakage*-1;
							$scope.salat_tax_18 = ToTax;
							$scope.tax_leakage_18 = tax_leakage;
							val_i_18 = s_obj[i].total_income_chart;
						}
					}
					show_chart_salary(val_i_14,val_i_15,val_i_16,val_i_17,val_i_18,data_2014,data_2015,
						data_2016,data_2017,data_2018);
					chart_taxes_paid(tp_14,tp_15,tp_16,tp_17,tp_18,data_2014,data_2015,data_2016,
						data_2017,data_2018);
					show_chart_tax_leakage(tp_14,tp_15,tp_16,tp_17,tp_18,st_14,st_15,st_16,st_17,st_18,
						tl_14,tl_15,tl_16,tl_17,tl_18,data_2014,data_2015,data_2016,data_2017,data_2018);
					$scope.tax_paid_total = $scope.money_format(tax_paid_total);

					$scope.$apply();
				}
			});
			
			$.ajax({
				url:'/get_ITR_data/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan},
				success: function(response){
					console.log(response);
					var gr_start = 0;
					var gr_18 = 0;
					var n = 0;
					var data_2014 = 0;
					var data_2015 = 0;
					var data_2016 = 0;
					var data_2017 = 0;
					var data_2018 = 0;
					var data_sal = 0;
					var data_prp = 0;
					var data_cg = 0;
					var data_os = 0;
					var data_bp = 0;

					for(var i = 0; i < response.count_itr; i++) {
						var salary = response.income_from_salary[i];
						var hp = response.income_from_hp[i];
						var cg = response.capital_gain[i];
						var os = response.income_from_other_src[i];
						var bp = response.business_or_profession[i];
						var tds_ITR = response.total_tds_ITR[i];
						if(salary=='NA')
							salary = 0;
						if(hp=='NA')
							hp = 0;
						if(cg=='NA')
							cg = 0;
						if(os=='NA')
							os = 0;
						if(bp=='NA')
							bp = 0;
						if(parseInt(salary)!=0)
							data_sal = 1;
						if(parseInt(hp)!=0)
							data_prp = 1;
						if(parseInt(cg)!=0)
							data_cg = 1;
						if(parseInt(os)!=0)
							data_os = 1;
						if(parseInt(bp)!=0)
							data_bp = 1;

						if(response.year[i]=='2014'){
							data_2014 = 1;
							n = n+1;
							$scope.salary_14=$scope.money_format(salary);
							$scope.property_14=$scope.money_format(hp);
							$scope.cg_14=$scope.money_format(cg);
							$scope.os_14=$scope.money_format(os);
							$scope.bp_14=$scope.money_format(bp);
							$scope.tds_itr_14=$scope.money_format(tds_ITR);
							$scope.Total_income_14 = $scope.money_format(parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp));
							gr_start = parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp);
							// alert(gr_start);
						}
						if(response.year[i]=='2015'){
							data_2015 = 1;
							n = n+1;
							$scope.salary_15=$scope.money_format(salary);
							$scope.property_15=$scope.money_format(hp);
							$scope.cg_15=$scope.money_format(cg);
							$scope.os_15=$scope.money_format(os);
							$scope.bp_15=$scope.money_format(bp);
							$scope.tds_itr_15=$scope.money_format(tds_ITR);
							$scope.Total_income_15 = $scope.money_format(parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp));
							if( gr_start == 0 )
								gr_start = parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp);
						}
						if(response.year[i]=='2016'){
							data_2016 = 1;
							n = n+1;
							$scope.salary_16=$scope.money_format(salary);
							$scope.property_16=$scope.money_format(hp);
							$scope.cg_16=$scope.money_format(cg);
							$scope.os_16=$scope.money_format(os);
							$scope.bp_16=$scope.money_format(bp);
							$scope.tds_itr_16=$scope.money_format(tds_ITR);
							$scope.Total_income_16 = $scope.money_format(parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp));
							if( gr_start == 0 )
								gr_start = parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp);
						}
						if(response.year[i]=='2017'){
							data_2017 = 1;
							n = n+1;
							$scope.salary_17=$scope.money_format(salary);
							$scope.property_17=$scope.money_format(hp);
							$scope.cg_17=$scope.money_format(cg);
							$scope.os_17=$scope.money_format(os);
							$scope.bp_17=$scope.money_format(bp);
							$scope.tds_itr_17=$scope.money_format(tds_ITR);
							$scope.Total_income_17 = $scope.money_format(parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp));
							if( gr_start == 0 )
								gr_start = parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp);
						}
						if(response.year[i]=='2018'){
							data_2018 = 1;
							n = n+1;
							$scope.salary_18=$scope.money_format(salary);
							$scope.property_18=$scope.money_format(hp);
							$scope.cg_18=$scope.money_format(cg);
							$scope.os_18=$scope.money_format(os);
							$scope.bp_18=$scope.money_format(bp);
							$scope.tds_itr_18=$scope.money_format(tds_ITR);
							$scope.Total_income_18 = $scope.money_format(parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp));
							if( gr_start == 0 )
								gr_start = parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp);
								
							gr_18 = parseInt(salary)+parseInt(hp)+parseInt(cg)+parseInt(os)+parseInt(bp);
							show_chart_income(salary,hp,cg,os,bp);
							// major_income
							var max_val = Math.max(parseInt(salary),parseInt(hp),parseInt(cg),parseInt(os),parseInt(bp));
							if(max_val == parseInt(salary))
								$scope.major_income = 'your Salary';
							if(max_val == parseInt(hp))
								$scope.major_income = 'your Propery';
							if(max_val == parseInt(cg))
								$scope.major_income = 'Capital Gain';
							if(max_val == parseInt(os))
								$scope.major_income = 'Other Source';
							if(max_val == parseInt(bp))
								$scope.major_income = 'your Business or Profession';
						}
					}

					if(data_2014 == 0)
						$scope.div_2014 = false;
					if(data_2015 == 0)
						$scope.div_2015 = false;
					if(data_2016 == 0)
						$scope.div_2016 = false;
					if(data_2017 == 0)
						$scope.div_2017 = false;
					if(data_2018 == 0)
						$scope.div_2018 = false;
					if(data_sal == 0)
						$scope.col_sal = false;
					if(data_prp == 0)
						$scope.col_prp = false;
					if(data_cg == 0)
						$scope.col_cg = false;
					if(data_os == 0)
						$scope.col_os = false;
					if(data_bp == 0)
						$scope.col_bp = false;
					var growth_rate = Math.pow( gr_18/gr_start,1/n)-1;
					$scope.Total_income_GR = Math.round(growth_rate* 100);
					$scope.$apply();
				},
				error: function(response){ }
			});
			
			setTimeout(function(){
				$.ajax({
					url:'/deduction_mismatch/',
					type:'POST',
					dataType: 'json',
					data:{'pan':pan,'p_age':p_age},
					success: function(response){
						console.log(response);
						for(var i = 0; i < response.count_itr; i++) {
							if(response.year[i]=='2014'){
								var tax_l_14 = parseInt($scope.tax_leakage_14.replace(/,/g, ''));
								$scope.c_80_14 = $scope.money_format(response.c_80[i]);
								$scope.d_80_14 = $scope.money_format(response.d_80[i]);
								$scope.ccd_80_14 = $scope.money_format(response.ccd_1b_80[i]);
								$scope.ccg_80_14 = $scope.money_format(response.ccg_80[i]);
								if(tax_l_14 != 0){
									if(response.c_80[i]<response.c_80_limit14){
										// alert('mismatch 80C 2014');
										$scope.c80_14 = {"background":"#FF9330"};
										$scope.c_80_14_text = 'You could have saved upto Rs. ';
										$scope.c_80_14_text += $scope.money_format(Math.min( response.tax_slab_14*(response.c_80_limit14-response.c_80[i]), tax_l_14 ) );
										$scope.c_80_14_text += ' in taxes by investing in tax saving (80C) investment.';
										$scope.c_hover_14 = true;
									}
									if(response.d_80[i]<response.d_80_limit14){
										$scope.d80_14 = {"background":"#FF9330"};
										$scope.d_80_14_text = 'You could have saved upto Rs. ';
										$scope.d_80_14_text += $scope.money_format(Math.min( response.tax_slab_14*(response.d_80_limit14-response.d_80[i]), tax_l_14 ) );
										$scope.d_80_14_text += ' in taxes by taking a medical insurance for yourself & your family.';
										$scope.d_hover_14 = true;
									}
									if(response.ccd_1b_80[i]<response.ccd_1b_80_limit14){
										$scope.ccd80_14 = {"background":"#FF9330"};
										$scope.ccd_80_14_text = 'You could have saved upto Rs. ';
										$scope.ccd_80_14_text += $scope.money_format(Math.min( response.tax_slab_14*(25000-(response.ccd_1b_80[i]/2) ), tax_l_14 ) );
										$scope.ccd_80_14_text += ' in taxes by investing in National Pension Scheme.';
										$scope.ccd_hover_14 = true;
									}
									if(response.ccg_80[i]<response.ccg_80_limit14)
										$scope.ccg80_14 = {"background":"#FF9330"};
								}
							}
							if(response.year[i]=='2015'){
								var tax_l_15 = parseInt($scope.tax_leakage_15.replace(/,/g, ''));
								$scope.c_80_15 = $scope.money_format(response.c_80[i]);
								$scope.d_80_15 = $scope.money_format(response.d_80[i]);
								$scope.ccd_80_15 = $scope.money_format(response.ccd_1b_80[i]);
								$scope.ccg_80_15 = $scope.money_format(response.ccg_80[i]);
								if(tax_l_15 != 0){
									if(response.c_80[i]<response.c_80_limit15){
										$scope.c80_15 = {"background":"#FF9330"};
										$scope.c_80_15_text = 'You could have saved upto Rs. ';
										$scope.c_80_15_text += $scope.money_format(Math.min( response.tax_slab_15*(response.c_80_limit15-response.c_80[i]), tax_l_15 ) );
										$scope.c_80_15_text += ' in taxes by investing in tax saving (80C) investment.';
										$scope.c_hover_15 = true;
									}
									if(response.d_80[i]<response.d_80_limit15){
										$scope.d80_15 = {"background":"#FF9330"};
										$scope.d_80_15_text = 'You could have saved upto Rs. ';
										$scope.d_80_15_text += $scope.money_format(Math.min( response.tax_slab_15*(response.d_80_limit15-response.d_80[i]), tax_l_15 ) );
										$scope.d_80_15_text += ' in taxes by taking a medical insurance for yourself & your family.';
										$scope.d_hover_15 = true;
									}
									if(response.ccd_1b_80[i]<response.ccd_1b_80_limit15){
										$scope.ccd80_15 = {"background":"#FF9330"};
										$scope.ccd_80_15_text = 'You could have saved upto Rs. ';
										$scope.ccd_80_15_text += $scope.money_format(Math.min( response.tax_slab_15*(25000-(response.ccd_1b_80[i]/2) ), tax_l_15 ) );
										$scope.ccd_80_15_text += ' in taxes by investing in National Pension Scheme.';
										$scope.ccd_hover_15 = true;
									}
									if(response.ccg_80[i]<response.ccg_80_limit15)
										$scope.ccg80_15 = {"background":"#FF9330"};
								}
							}
							if(response.year[i]=='2016'){
								var tax_l_16 = parseInt($scope.tax_leakage_16.replace(/,/g, ''));
								$scope.c_80_16 = $scope.money_format(response.c_80[i]);
								$scope.d_80_16 = $scope.money_format(response.d_80[i]);
								$scope.ccd_80_16 = $scope.money_format(response.ccd_1b_80[i]);
								$scope.ccg_80_16 = $scope.money_format(response.ccg_80[i]);
								if(tax_l_16 != 0){
									if(response.c_80[i]<response.c_80_limit16){
										$scope.c80_16 = {"background":"#FF9330"};
										$scope.c_80_16_text = 'You could have saved upto Rs. ';
										$scope.c_80_16_text += $scope.money_format(Math.min( response.tax_slab_16*(response.c_80_limit16-response.c_80[i]),tax_l_16 ) );
										$scope.c_80_16_text += ' in taxes by investing in tax saving (80C) investment.';
										$scope.c_hover_16 = true;
									}
									if(response.d_80[i]<response.d_80_limit16){
										$scope.d80_16 = {"background":"#FF9330"};
										$scope.d_80_16_text = 'You could have saved upto Rs. ';
										$scope.d_80_16_text += $scope.money_format(Math.min( response.tax_slab_16*(response.d_80_limit16-response.d_80[i]),tax_l_16 ) );
										$scope.d_80_16_text += ' in taxes by taking a medical insurance for yourself & your family.';
										$scope.d_hover_16 = true;
									}
									if(response.ccd_1b_80[i]<response.ccd_1b_80_limit16){
										$scope.ccd80_16 = {"background":"#FF9330"};
										$scope.ccd_80_16_text = 'You could have saved upto Rs. ';
										$scope.ccd_80_16_text += $scope.money_format(Math.min( response.tax_slab_16*(25000-(response.ccd_1b_80[i]/2) ),tax_l_16 ) );
										$scope.ccd_80_16_text += ' in taxes by investing in National Pension Scheme.';
										$scope.ccd_hover_16 = true;
									}
									if(response.ccg_80[i]<response.ccg_80_limit16)
										$scope.ccg80_16 = {"background":"#FF9330"};
								}
							}
							if(response.year[i]=='2017'){
								var tax_l_17 = parseInt($scope.tax_leakage_17.replace(/,/g, ''));
								$scope.c_80_17 = $scope.money_format(response.c_80[i]);
								$scope.d_80_17 = $scope.money_format(response.d_80[i]);
								$scope.ccd_80_17 = $scope.money_format(response.ccd_1b_80[i]);
								$scope.ccg_80_17 = $scope.money_format(response.ccg_80[i]);
								if(tax_l_17 != 0){
									if(response.c_80[i]<response.c_80_limit17){
										$scope.c80_17 = {"background":"#FF9330"};
										$scope.c_80_17_text = 'You could have saved upto Rs. ';
										$scope.c_80_17_text += $scope.money_format(Math.min( response.tax_slab_17*(response.c_80_limit17-response.c_80[i]), tax_l_17 ) );
										$scope.c_80_17_text += ' in taxes by investing in tax saving (80C) investment.';
										$scope.c_hover_17 = true;
									}
									if(response.d_80[i]<response.d_80_limit17){
										$scope.d80_17 = {"background":"#FF9330"};
										$scope.d_80_17_text = 'You could have saved upto Rs. ';
										$scope.d_80_17_text += $scope.money_format(Math.min( response.tax_slab_17*(response.d_80_limit17-response.d_80[i]), tax_l_17 ) );
										$scope.d_80_17_text += ' in taxes by taking a medical insurance for yourself & your family.';
										$scope.d_hover_17 = true;
									}
									if(response.ccd_1b_80[i]<response.ccd_1b_80_limit17){
										$scope.ccd80_17 = {"background":"#FF9330"};
										$scope.ccd_80_17_text = 'You could have saved upto Rs. ';
										$scope.ccd_80_17_text += $scope.money_format(Math.min( response.tax_slab_17*(25000-(response.ccd_1b_80[i]/2) ), tax_l_17 ) );
										$scope.ccd_80_17_text += ' in taxes by investing in National Pension Scheme.';
										$scope.ccd_hover_17 = true;
									}
									if(response.ccg_80[i]<response.ccg_80_limit17)
										$scope.ccg80_17 = {"background":"#FF9330"};
								}
							}
							if(response.year[i]=='2018'){
								var tax_l_18 = parseInt($scope.tax_leakage_18.replace(/,/g, ''));
								$scope.c_80_18 = $scope.money_format(response.c_80[i]);
								$scope.d_80_18 = $scope.money_format(response.d_80[i]);
								$scope.ccd_80_18 = $scope.money_format(response.ccd_1b_80[i]);
								$scope.ccg_80_18 = $scope.money_format(response.ccg_80[i]);
								$scope.c_80_limit18 = $scope.money_format(response.c_80_limit18);
								if(tax_l_18 != 0){
									if(response.c_80[i]<response.c_80_limit18){
										// alert('mismatch 80C 2014');
										$scope.c80_18 = {"background":"#FF9330"};
										$scope.c80_mismatch = '80C - You could have claimed Rs. '+$scope.money_format(response.c_80_limit18)+' from your Total Taxable Income. But you have taken only Rs. '+$scope.money_format(response.c_80[i]);
										if(response.c_80[i]==0 )
											$scope.c80_mismatch = '80C - You could have claimed Rs. '+$scope.money_format(response.c_80_limit18)+' from your Total Taxable Income. ';
										$scope.c_80_18_text = 'You could have saved upto Rs. ';
										$scope.c_80_18_text += $scope.money_format(Math.min( response.tax_slab_18*(response.c_80_limit18-response.c_80[i]), tax_l_18 ) );
										$scope.c_80_18_text += ' in taxes by investing in tax saving (80C) investment.';
										$scope.c_hover_18 = true;
									}
									if(response.d_80[i]<response.d_80_limit18){
										$scope.d80_18 = {"background":"#FF9330"};
										$scope.d80_mismatch = '80D - You could have claimed Rs. '+$scope.money_format(response.d_80_limit18)+' from your Total Taxable Income. But you have taken only Rs. '+$scope.money_format(response.d_80[i]);
										if(response.d_80[i]==0 )
											$scope.d80_mismatch = '80D - You could have claimed Rs. '+$scope.money_format(response.d_80_limit18)+' from your Total Taxable Income.';
										$scope.d_80_18_text = 'You could have saved upto Rs. ';
										$scope.d_80_18_text += $scope.money_format(Math.min( response.tax_slab_18*(response.d_80_limit18-response.d_80[i]), tax_l_18 ) );
										$scope.d_80_18_text += ' in taxes by taking a medical insurance for yourself & your family.';
										$scope.d_hover_18 = true;
									}
									if(response.ccd_1b_80[i]<response.ccd_1b_80_limit18){
										$scope.ccd80_18 = {"background":"#FF9330"};
										$scope.ccd80_mismatch = '80CCD(1B) - You could have claimed Rs. '+$scope.money_format(response.ccd_1b_80_limit18)+' from your Total Taxable Income. But you have taken only Rs. '+$scope.money_format(response.ccd_1b_80[i]);
										if(response.ccd_1b_80[i]==0 )
											$scope.ccd80_mismatch = '80CCD(1B) - You could have claimed Rs. '+$scope.money_format(response.ccd_1b_80_limit18)+' from your Total Taxable Income.';
										$scope.ccd_80_18_text = 'You could have saved upto Rs. ';
										$scope.ccd_80_18_text += $scope.money_format(Math.min( response.tax_slab_18*(25000-(response.ccd_1b_80[i]/2) ), tax_l_18 ) );
										$scope.ccd_80_18_text += ' in taxes by investing in National Pension Scheme.';
										$scope.ccd_hover_18 = true;
									}
									if(response.ccg_80[i]<response.ccg_80_limit18){
										$scope.ccg80_18 = {"background":"#FF9330"};
										$scope.ccg80_mismatch = '80CCG - You could have claimed Rs. '+$scope.money_format(response.ccg_80_limit18)+' from your Total Taxable Income. But you have taken only Rs. '+$scope.money_format(response.ccg_80[i]);
										if(response.ccg_80[i]==0 )
											$scope.ccg80_mismatch = '80CCG - You could have claimed Rs. '+$scope.money_format(response.ccg_80_limit18)+' from your Total Taxable Income.';
									}
								}
							}
						}
						$scope.$apply();
					},
					error: function(response){}
				});
			}, 1800);
		}
	}

	$scope.show_26as = function(pan){
		$.ajax({
			url:'/show_26as/',
			type:'POST',
			dataType: 'json',
			data:{'pan':pan,'client_id':client_id},
			success: function(response){
				console.log(response);
				var tds_2014 = 0;
				var tds_2015 = 0;
				var tds_2016 = 0;
				var tds_2017 = 0;
				var tds_2018 = 0;
				for(var i = 0; i < response.count; i++){
					if(response.year[i] == '2014')
						tds_2014 = tds_2014 + response.tds_26as[i];
					if(response.year[i] == '2015')
						tds_2015 = tds_2015 + response.tds_26as[i];
					if(response.year[i] == '2016')
						tds_2016 = tds_2016 + response.tds_26as[i];
					if(response.year[i] == '2017')
						tds_2017 = tds_2017 + response.tds_26as[i];
					if(response.year[i] == '2018')
						tds_2018 = tds_2018 + response.tds_26as[i];
				}
				$scope.tds_26as_14 = $scope.money_format(tds_2014);
				$scope.tds_26as_15 = $scope.money_format(tds_2015);
				$scope.tds_26as_16 = $scope.money_format(tds_2016);
				$scope.tds_26as_17 = $scope.money_format(tds_2017);
				$scope.tds_26as_18 = $scope.money_format(tds_2018);
								
				$scope.tds_mismatch();
				$scope.$apply();
			}
		});
	}

	$scope.get_26as = function(pan,dob){
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			var pass_obj = { "pan":pan};
			pass_obj["dob"] = dob;
			var data_pass = JSON.stringify(pass_obj);
			console.log('get_form_26AS Ajax Started');
			// $.ajax({
			// 	url:'/get_form_26AS/',
			// 	type:'POST',
			// 	dataType: 'json',
			// 	data:{'data':data_pass},
			// 	success: function(response){
			// 		console.log(response);
			// 		if (response.status == 'success') {
			// 			$.ajax({
			// 				url:'/unzip_txt/',
			// 				type:'POST',
			// 				dataType: 'json',
			// 				data:{'data':data_pass},
			// 				success: function(response){
			// 					// console.log(response);
			// 					status = JSON.stringify(response['status']).replace(/\"/g, "");
			// 					if (status == 'success') {
			// 						$.ajax({
			// 							url:'/excelToDB/',
			// 							type:'POST',
			// 							dataType: 'json',
			// 							data:{'data':data_pass,'client_id':client_id},
			// 							success: function(response){
			// 								console.log(response);
			// 								// $scope.show_26as(pan)
			// 							}
			// 						});
			// 					}
			// 				}
			// 			});
			// 		}
			// 	},
			// 	error: function(response){}
			// });
		}
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return 0

		if(!isNaN(x)){
			// x = x.toString();
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.nFormatter = function(num, digits) {
		var si = [
			{ value: 1, symbol: "" },
			{ value: 1E3, symbol: "k" },
			{ value: 1E5, symbol: " lacs" },
			{ value: 1E7, symbol: "M" },
			{ value: 1E9, symbol: "G" },
			{ value: 1E12, symbol: "T" },
		];
		var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
		var i;
		for (i = si.length - 1; i > 0; i--) {
			if (num >= si[i].value)
				break;
		}
		return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
	}

	$scope.feedback = function(){
		var email = $scope.email;
		var full_name = $scope.full_name;
		var message = $scope.message;
		alert($scope.message);
		var name_status = false;
		var email_status = false;
		if($scope.full_name.length >= 2)
			name_status = true;
		if(email!==''){
			if( /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email) ){ 
				$scope.mail_err= "";
				email_status = true;
			}else
				$scope.mail_err= "Please Enter Valid Email ID";
		}else
			$scope.mail_err= "Email is required";
		if(name_status==true && email_status==true){
			$.ajax({
				url:'/send_feedback/',
				type:'POST',
				dataType: 'json',
				data:{'email':email,'full_name':full_name,'message':message},
				success: function(response){
					console.log(response);
				}
			});
		}
	}

	$scope.click_to_communicate = function(){
		var message_feedback = $scope.message_feedback;
		alert(message_feedback);
		$.ajax({
			url:'/send_communication_feedback/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id,'message_feedback':message_feedback},
			success: function(response){
				console.log(response);
			}
		});
	}

	$scope.refer_friend = function(){
		alert('refer_friend');
	}

	$scope.number_format = function(value){
		if(value=='' || value == 'undefined')
			return 0 ;
		value = value.replace(/,/g, '');
		return value;
	}

	$scope.tds_mismatch = function(){
		$scope.tds_14 = {"background":"#f9f9f9"};
		$scope.tds_15 = {"background":"#f9f9f9"};
		$scope.tds_16 = {"background":"#f9f9f9"};
		$scope.tds_17 = {"background":"#f9f9f9"};
		$scope.tds_18 = {"background":"#f9f9f9"};
		var tds_26as_14 = $scope.number_format($scope.tds_26as_14);
		var tds_26as_15 = $scope.number_format($scope.tds_26as_15);
		var tds_26as_16 = $scope.number_format($scope.tds_26as_16);
		var tds_26as_17 = $scope.number_format($scope.tds_26as_17);
		var tds_26as_18 = $scope.number_format($scope.tds_26as_18);
		var tds_itr_14 = $scope.number_format($scope.tds_itr_14);
		var tds_itr_15 = $scope.number_format($scope.tds_itr_15);
		var tds_itr_16 = $scope.number_format($scope.tds_itr_16);
		var tds_itr_17 = $scope.number_format($scope.tds_itr_17);
		var tds_itr_18 = $scope.number_format($scope.tds_itr_18);

		if(tds_26as_14-tds_itr_14>100 || tds_itr_14-tds_26as_14>100){
			$scope.tds_mismatch_14 = 'TDS mismatch';
			$scope.tds_14 = {"background":"#FF9330"};
		}
		if(tds_26as_15-tds_itr_15>100 || tds_itr_15-tds_26as_15>100){
			$scope.tds_mismatch_15 = 'TDS mismatch';
			$scope.tds_15 = {"background":"#FF9330"};
		}
		if(tds_26as_16-tds_itr_16>100 || tds_itr_16-tds_26as_16>100){
			$scope.tds_mismatch_16 = 'TDS mismatch';
			$scope.tds_16 = {"background":"#FF9330"};
		}
		if(tds_26as_17-tds_itr_17>100 || tds_itr_17-tds_26as_17>100){
			$scope.tds_mismatch_17 = 'TDS mismatch';
			$scope.tds_17 = {"background":"#FF9330"};
		}
		if(tds_26as_18-tds_itr_18>100 || tds_itr_18-tds_26as_18>100){
			$scope.tds_mismatch_18 = 'TDS mismatch';
			$scope.tds_18 = {"background":"#FF9330"};
			if(tds_itr_18-tds_26as_18>0)
				$scope.less_excess = 'excess';
			else
				$scope.less_excess = 'less';
			$scope.tds_mismatch_amt = $scope.money_format(Math.abs(tds_itr_18-tds_26as_18) );
		}else{
			$scope.tds_mismatch_div = false;
			$scope.tds_mismatch_point = '2.';
		}
		// console.log(tds_26as_14);
		// console.log(tds_itr_14);
		$scope.$apply();
	}

}]);
// https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_tooltip