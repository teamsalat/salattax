
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('deductions_controller', ['$scope','$compile', function($scope,$compile,CONFIG) {
	var client_id = 1;
	redirect_url = $scope.url;
	$('.loader').show();
	$(window).load(function() {

		// alert($scope.user_d.state_code)
		$scope.items=$scope.user_d.state_code
		$scope.percents=[{'percent':'100'},{'percent':'50'}]
		$scope.user_d.parent_dependent='no'
		$scope.user_d.parent_senier_citizen='no'
		console.log('ID : '+$scope.user.id);
		console.log('Client ID : '+$scope.user.client_id);
    	client_id=$scope.user.client_id
    	
		$scope.donee_tbl = { 'display': 'none'};
		$.ajax({
			url:'/get_client/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				if (response.client_id != 0) {
					// client_id = response.client_id;
					$scope.get_deductions();
				}
				else if ($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}

				if ($scope.user.id != '') {
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						async:false,
						data:{'username':$scope.user.id},
						success: function(response){
							// client_id = response.client_id;
							email = response.email;
							$scope.get_deductions();
						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}

				
			}
    	});
		
	});

	$scope.gotoincomedetails=function(){
		window.location.href = redirect_url+'/income_details/'+client_id;
	}

	$scope.gotocommondeductionstab=function(){
		$('#otherDeductionsTab').removeClass('active');
      	$('#commonDeductionsTab').tab('show');
      	$("html, body").animate({ scrollTop: 0 }, "slow");
	}
  
	$scope.status1= 'F';
	$scope.validation = function(type,value,$event) {
		// Common Deductions
		if((type == "section_80c" || type == "section_80ccd" || type == "section_80d" || type == "section_80g" || type == "section_80ttb") && value!==undefined){
			var pin_length=value.length;
			value = $scope.number_format(value);
			if( /^\d+$/.test(value) || pin_length==0){ 
				if(type == "section_80c"){
					$scope.section_80c_error= "";
					$scope.user_d.section_80c = $scope.money_format(value);
				}
				else if(type == "section_80ccd"){
					$scope.section_80ccd_error= "";
					$scope.user_d.section_80ccd = $scope.money_format(value);
				}
				else if(type == "section_80d"){
					$scope.section_80d_error= "";
					$scope.user_d.section_80d = $scope.money_format(value);
				}
				else if(type == "section_80g"){
					$scope.section_80g_error= "";
					$scope.user_d.section_80g = $scope.money_format(value);
				}
				else if(type == "section_80ttb"){
					$scope.section_80ttb_error= "";
					$scope.user_d.section_80ttb = $scope.money_format(value);
				}
				return true;
			}else{
				if(type == "section_80c")
					$scope.section_80c_error= "Enter Valid Section 80C";
				else if(type == "section_80ccd")
					$scope.section_80ccd_error= "Enter Valid Section 80CCD";
				else if(type == "section_80d")
					$scope.section_80d_error= "Enter Valid Section 80D";
				else if(type == "section_80g")
					$scope.section_80g_error= "Enter Valid Section 80G";
				else if(type == "section_80ttb")
					$scope.section_80ttb_error= "Enter Valid Section 80TTB";
				return false;
			}
		}else if((type == "section_80c" || type == "section_80ccd" || type == "section_80d" || type == "section_80g" || type == "section_80ttb") && value==undefined)
			return true;
		
		if((type == "section_80ccd1" || type == "section_80ccd2") && value!==undefined){
			var pin_length=value.length;
			value = $scope.number_format(value);
			if( /^\d+$/.test(value) || pin_length==0){ 
				if(type == "section_80ccd1"){
					$scope.section_80ccd1_error= "";
					$scope.user_d.section_80ccd1 = $scope.money_format(value);
				}
				else if(type == "section_80ccd2"){
					$scope.section_80ccd2_error= "";
					$scope.user_d.section_80ccd2 = $scope.money_format(value);
				}
				return true;
			}else{
				if(type == "section_80ccd1")
					$scope.section_80ccd1_error= "Enter Valid Section 80CCD Employee or SE";
				else if(type == "section_80ccd2")
					$scope.section_80ccd2_error= "Enter Valid Section 80CCD Employer";
				return false;
			}
		}else if((type == "section_80ccd1" || type == "section_80ccd2") && value==undefined)
			return true;

		// Other Deductions
		if((type=="section_80ccg" || type=="section_80e" || type=="section_80dd" || type=="section_80u" || type=="section_80ddb" || type=="section_80ggc") && value!==undefined){
			var pin_length=value.length;
			value = $scope.number_format(value);
			if( /^\d+$/.test(value) || pin_length == 0 ){ 
				if(type == "section_80ccg"){
					$scope.section_80ccg_error= "";
					$scope.user_d.section_80ccg = $scope.money_format(value);
				}
				else if(type == "section_80e"){
					$scope.section_80e_error= "";
					$scope.user_d.section_80e = $scope.money_format(value);
				}
				else if(type == "section_80dd"){
					$scope.section_80dd_error= "";
					$scope.user_d.section_80dd = $scope.money_format(value);
				}
				else if(type == "section_80u"){
					$scope.section_80u_error= "";
					$scope.user_d.section_80u = $scope.money_format(value);
				}
				else if(type == "section_80ddb"){
					$scope.section_80ddb_error= "";
					$scope.user_d.section_80ddb = $scope.money_format(value);
				}
				else if(type == "section_80ggc"){
					$scope.section_80ggc_error= "";
					$scope.user_d.section_80ggc = $scope.money_format(value);
				}
				return true;
			}else{
				if(type == "section_80ccg")
					$scope.section_80ccg_error= "Enter Valid Section 80CCG";
				else if(type == "section_80e")
					$scope.section_80e_error= "Enter Valid Section 80E";
				else if(type == "section_80dd")
					$scope.section_80dd_error= "Enter Valid Section 80DD";
				else if(type == "section_80u")
					$scope.section_80u_error= "Enter Valid Section 80U";
				else if(type == "section_80ddb")
					$scope.section_80ddb_error= "Enter Valid Section 80DDB";
				else if(type == "section_80ggc")
					$scope.section_80ggc_error= "Enter Valid Section 80GGC";
				return false;
			}
		}else if((type=="section_80ccg" || type=="section_80e" || type=="section_80dd" || type=="section_80u" || type=="section_80ddb" || type=="section_80ggc") && value==undefined)
			return true;

		if((type == "section_80ee" || type == "section_80gg" || type == "section_80gga" ) && value!==undefined){
			var pin_length=value.length;
			value = $scope.number_format(value);
			if( /^\d+$/.test(value) || pin_length ==0 ){ 
				if(type == "section_80ee"){
					$scope.section_80ee_error= "";
					$scope.user_d.section_80ee = $scope.money_format(value);
				}
				else if(type == "section_80gg"){
					$scope.section_80gg_error= "";
					$scope.user_d.section_80gg = $scope.money_format(value);
				}
				else if(type == "section_80gga"){
					$scope.section_80gga_error= "";
					$scope.user_d.section_80gga = $scope.money_format(value);
				}
				return true;
			}else{
				if(type == "section_80ee")
					$scope.section_80ee_error= "Enter Valid Section 80EE";
				else if(type == "section_80gg")
					$scope.section_80gg_error= "Enter Valid Section 80GG";
				else if(type == "section_80gga")
					$scope.section_80gga_error= "Enter Valid Section 80GGA";
				return false;
			}
		}else if((type == "section_80ee" || type == "section_80gg" || type == "section_80dd" ) && value==undefined)
			return true;
	}



	$scope.deductions_save1 = function(type,value,$event) {
		var status=$scope.validation_check1();
	    $scope.status1= 'F';

		if(status==true){
			var pass_obj = { "client_id":client_id};
			pass_obj["S_80C"] = $scope.number_format($scope.user_d.section_80c);
			pass_obj["S_80CCD"] = $scope.number_format($scope.user_d.section_80ccd);
			pass_obj["S_80CCD1"] = $scope.number_format($scope.user_d.section_80ccd1);
			pass_obj["S_80CCD2"] = $scope.number_format($scope.user_d.section_80ccd2);
			pass_obj["S_80D"] = $scope.number_format($scope.user_d.section_80d);
			pass_obj["S_80DD"] = $scope.number_format($scope.user_d.section_80dd);
			pass_obj["S_80G"] = $scope.number_format($scope.user_d.section_80g);
			pass_obj["S_80TTB"] = $scope.number_format($scope.user_d.section_80ttb);

			pass_obj['parent_dependent']=$scope.number_format($scope.user_d.parent_dependent);
			pass_obj['parent_senier_citizen']=$scope.number_format($scope.user_d.parent_senier_citizen);
			pass_obj['self_citizen_type']=$scope.user_d.citizen_type
			
			pass_obj['self_health_checkup']=$scope.number_format($scope.user_d.self_health_checkup);
			pass_obj['parent_health_checkup']=$scope.number_format($scope.user_d.parent_health_checkup);
			pass_obj['self_insurance_premium']=$scope.number_format($scope.user_d.self_insurance_premium);
			pass_obj['parent_insurance_premium']=$scope.number_format($scope.user_d.parent_insurance_premium);
			pass_obj['self_medical_expenditure']=$scope.number_format($scope.user_d.self_medical_expenditure);
			pass_obj['parent_medical_expenditure']=$scope.number_format($scope.user_d.parent_medical_expenditure);


  			var data_pass = JSON.stringify(pass_obj);

	    	$.ajax({
				url:'/save_common_deductions/',
				type:'POST',
				dataType: 'json',
				data:{'data_pass':data_pass},
				success: function(response){
					console.log(response);
					if (response.status == 'success') {
						$scope.status1= 'T';
						$('#capitalGainsTab').removeClass('active');
				        $('#otherDeductionsTab').tab('show');
				        $("html, body").animate({ scrollTop: 0 }, "slow");
						// $scope.user_tracking('submitted','Deductions - Common Deductions');
					}else{
						// $scope.user_tracking('error in data',response.status);
					}
					$scope.$apply();
				}
			});
			var value_80G = $scope.user_d.section_80g;
			var section_80g_status = $scope.validation("section_80g",$scope.user_d.section_80g);
			
			if (section_80g_status == true && value_80G!=0 && value_80G!='') {
				arr = donation_arr();
				var no_of_donee = arr.length;
				for (var i = 0; i < arr.length; i++) {
					var j = arr[i];

					var obj = { "client_id":client_id,"doneeID":i};
					obj["name"] = $scope.fetch_dynamic_variable(j,'name');
					obj["address"] = $scope.fetch_dynamic_variable(j,'address');
					obj["city"] = $scope.fetch_dynamic_variable(j,'city');
					obj["state"] = $scope.fetch_dynamic_variable(j,'state');

					// alert(obj["state"])

					obj["pin"] = $scope.fetch_dynamic_variable(j,'pin');
					obj["pan"] = $scope.fetch_dynamic_variable(j,'pan');
					obj["amount"] = $scope.fetch_dynamic_variable(j,'amount');
					obj["e_amount"] = $scope.fetch_dynamic_variable(j,'e_amount');
					obj["percent"] = $scope.fetch_dynamic_variable(j,'percent');
      				var data_pass = JSON.stringify(obj);
					$.ajax({
						url:'/save_donee_details/',
						type:'POST',
						dataType: 'json',
						data:{'data':data_pass},
						success: function(response){
							console.log(response);
							// if(response.status == 'success')
								// $scope.user_tracking('submitted','Donee '+i+' Details');
						}
					});
				}
			}
		}else{
			// $scope.user_tracking('error in data','common deductions validation');
	    	$scope.status1= 'F';
		}
	}
	$scope.gotoDeductions = function(type,value,$event) {
		var status=$scope.validation_check2();

		if(status==true){
			var pass_obj = { "client_id":client_id};
			pass_obj["S_80CCG"] = $scope.user_d.section_80ccg;
			pass_obj["S_80E"] = $scope.number_format($scope.user_d.section_80e);
			pass_obj["S_80EE"] = $scope.number_format($scope.user_d.section_80ee);
			pass_obj["S_80GG"] = $scope.number_format($scope.user_d.section_80gg);
			pass_obj["S_80GGA"] = $scope.number_format($scope.user_d.section_80gga);
			// pass_obj["S_80DD"] = $scope.user_d.section_80dd;
			pass_obj["S_80U"] = $scope.number_format($scope.user_d.section_80u);
			pass_obj["S_80DDB"] = $scope.user_d.section_80ddb;
			pass_obj["S_80GGC"] = $scope.user_d.section_80ggc;
  			var data_pass = JSON.stringify(pass_obj);
  			console.log(data_pass);
			// 
			$.ajax({
				url:'/save_other_deductions/',
				type:'POST',
				dataType: 'json',
				data:{'data_pass':data_pass},
				success: function(response){
					console.log(response);
					if (response.status == 'success') {

						console.log(response.status)
						// $scope.user_tracking('submitted','Deductions - Other Deductions');
						window.location.href = redirect_url+'/taxes_paid/'+client_id;
					}else{
						// $scope.user_tracking('error in data',response.status);
					}
					$scope.$apply();
				}
			});
		}else{
			// $scope.user_tracking('error in data','other deductions validation');
		}
	}
	$scope.validation_check1 = function() {
		var section_80c_status = $scope.validation("section_80c",$scope.user_d.section_80c);
		var section_80ccd_status = $scope.validation("section_80ccd",$scope.user_d.section_80ccd);
		var section_80ccd1_status = $scope.validation("section_80ccd1",$scope.user_d.section_80ccd1);
		var section_80ccd2_status = $scope.validation("section_80ccd2",$scope.user_d.section_80ccd2);
		var section_80d_status = $scope.validation("section_80d",$scope.user_d.section_80d);
		var section_80g_status = $scope.validation("section_80g",$scope.user_d.section_80g);
		var section_80dd_status = $scope.validation("section_80dd",$scope.user_d.section_80dd);
		var section_80ttb_status = $scope.validation("section_80ttb",$scope.user_d.section_80ttb);
		var table_80g_status = true;

		if ($scope.user_d.section_80g != 0 && $scope.user_d.section_80g != '') {
			var name_valid = $scope.validation_donation('name');
			var add_valid = $scope.validation_donation('address');
			var city_valid = $scope.validation_donation('city');
			var state_valid = $scope.validation_donation('state');
			var pin_valid = $scope.validation_donation('pin');
			var pan_valid = $scope.validation_donation('pan');
			var amt_valid = $scope.validation_donation('amount');
			var e_amt_valid = $scope.validation_donation('e_amount');
			// console.log(name_valid);
			// console.log(add_valid);
			// console.log(city_valid);
			// console.log(state_valid);
			// console.log(pin_valid);
			// console.log(pan_valid);
			// console.log(amt_valid);
			// console.log(e_amt_valid);
			if(name_valid==true && add_valid==true && city_valid==true && state_valid==true && 
				pin_valid==true && pan_valid==true && amt_valid==true && e_amt_valid==true)
				table_80g_status = true;
			else
				table_80g_status = false;
			console.log(table_80g_status);
			// alert(table_80g_status);
			$scope.donee_tbl_error = 'Fill data';
		}

		if( section_80c_status==true && section_80ccd_status==true && section_80ccd1_status==true && section_80ccd2_status==true && section_80d_status==true && 
			section_80g_status==true && section_80dd_status==true && table_80g_status==true && section_80ttb_status==true){
			return true;
		}else
			return false;
	}

	$scope.validation_check2 = function() {
		var section_80ccg_status = $scope.validation("section_80ccg",$scope.user_d.section_80ccg);
		var section_80e_status = $scope.validation("section_80e",$scope.user_d.section_80e);
		var section_80ee_status = $scope.validation("section_80ee",$scope.user_d.section_80ee);
		var section_80gg_status = $scope.validation("section_80gg",$scope.user_d.section_80gg);
		var section_80gga_status = $scope.validation("section_80gga",$scope.user_d.section_80gga);
		// var section_80dd_status = $scope.validation("section_80dd",$scope.user_d.section_80dd);
		var section_80u_status = $scope.validation("section_80u",$scope.user_d.section_80u);
		var section_80ddb_status = $scope.validation("section_80ddb",$scope.user_d.section_80ddb);
		var section_80ggc_status = $scope.validation("section_80ggc",$scope.user_d.section_80ggc);

		if( section_80e_status==true && section_80u_status==true && section_80ccg_status==true && section_80ee_status==true &&
		section_80gg_status==true && section_80gga_status==true && section_80ddb_status==true && section_80ggc_status==true )
			return true;
		else
			return false;
	}

	$scope.update_donee_tbl = function() {
		var row_count = donation_row_count();
		var value_80G = $scope.user_d.section_80g;
		var section_80g_status = $scope.validation("section_80g",$scope.user_d.section_80g);
		
		console.log('row_count '+row_count)
		console.log('value_80G '+value_80G)
		console.log('section_80g_status '+section_80g_status)

		if (section_80g_status == true && value_80G!=0 && value_80G!='') {
			if (row_count==1)
				$scope.add_row_donation();
			$scope.donee_tbl = { 'display': 'block'};
		}else
			$scope.donee_tbl = { 'display': 'none'};

		// $scope.$apply();
		// $('.loader').hide();
	}

	$scope.get_deductions = function(type,value,$event) {
		$.ajax({
			url:'/get_deductions/',
			type:'POST',
			dataType: 'json',
			async:false,
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				if (response.d_80c != 0) {
					$scope.user_d.section_80c = $scope.money_format(response.d_80c);
					// label_float('div_80c');
				}
				if (response.d_80ccd != 0) {
					$scope.user_d.section_80ccd = $scope.money_format(response.d_80ccd);
					// label_float('div_80ccd');
				}
				if (response.d_80ccd1 != 0) {
					$scope.user_d.section_80ccd1 = $scope.money_format(response.d_80ccd1);
					// label_float('div_80ccd1');
				}
				if (response.d_80ccd2 != 0) {
					$scope.user_d.section_80ccd2 = $scope.money_format(response.d_80ccd2);
					// label_float('div_80ccd2');
				}
				if (response.d_80d != 0) {
					$scope.user_d.section_80d = $scope.money_format(response.d_80d);
					// label_float('div_80d');
				}
				if (response.d_80dd != 0) {
					$scope.user_d.section_80dd = $scope.money_format(response.d_80dd);
					// label_float('div_80dd');
				}
				if (response.d_80g != 0 && response.d_80g != '') {
					// $scope.update_donee_tbl();
					$scope.show_donee_tbl();
					$scope.user_d.section_80g = $scope.money_format(response.d_80g);
					// label_float('div_80g');
				}
				if (response.d_80ttb != 0 || response.d_80tta != 0) {
					$scope.user_d.section_80ttb = $scope.money_format(Math.max(response.d_80tta,response.d_80ttb));
					// label_float('div_80ttb');
				}
				if (response.d_80e != 0) {
					$scope.user_d.section_80e = $scope.money_format(response.d_80e);
					// label_float('div_80e');
				}
				if (response.d_80u != 0) {
					$scope.user_d.section_80u = $scope.money_format(response.d_80u);
					// label_float('div_80u');
				}
				if (response.d_80ccg != 0) {
					$scope.user_d.section_80ccg = $scope.money_format(response.d_80ccg);
					// label_float('div_80ccg');
				}
				if (response.d_80ee != 0) {
					$scope.user_d.section_80ee = $scope.money_format(response.d_80ee);
					// label_float('div_80ee');
				}
				if (response.d_80gg != 0) {
					$scope.user_d.section_80gg = $scope.money_format(response.d_80gg);
					// label_float('div_80gg');
				}
				if (response.d_80gga != 0) {
					$scope.user_d.section_80gga = $scope.money_format(response.d_80gga);
					// label_float('div_80gga');
				}
				if (response.d_80ddb != 0) {
					$scope.user_d.section_80ddb = $scope.money_format(response.d_80ddb);
					// label_float('div_80ddb');
				}
				if (response.d_80ggc != 0) {
					$scope.user_d.section_80ggc = $scope.money_format(response.d_80ggc);
					// label_float('div_80ggc');
				}

				if(response.parent_citizen_type=='SC' || response.parent_citizen_type=='SSC'){
					$scope.user_d.parent_senier_citizen='yes'
				}else{
					$scope.user_d.parent_senier_citizen='no'
				}


				if(response.parent_dependent=='Y'){
					$scope.user_d.parent_dependent='yes'
				}else{
					$scope.user_d.parent_dependent='no'
				}

				if(response.self_health_checkup!=0){
					$scope.user_d.self_health_checkup=$scope.money_format(response.self_health_checkup)
				}

				if(response.self_insurance_premium!=0){
					$scope.user_d.self_insurance_premium=$scope.money_format(response.self_insurance_premium)
				}

				if(response.self_medical_expenditure!=0){
					$scope.user_d.self_medical_expenditure=$scope.money_format(response.self_medical_expenditure)
				}


				if(response.parent_health_checkup!=0){
					$scope.user_d.parent_health_checkup=$scope.money_format(response.parent_health_checkup)
				}


				if(response.parent_insurance_premium!=0){
					$scope.user_d.parent_insurance_premium=$scope.money_format(response.parent_insurance_premium)
				}


				if(response.parent_medical_expenditure!=0){
					$scope.user_d.parent_medical_expenditure=$scope.money_format(response.parent_medical_expenditure)
				}


				$scope.user_d.citizen_type=response.citizen_type
				// $scope.$apply();

				$('.loader').hide();

				$scope.$apply();
				
			}
		});
	}

	$scope.show_donee_tbl = function(){

		console.log('show_donee_tbl')

		$.ajax({
			url:'/get_donation/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				// console.log($scope.user_d.state_code)
				arr = donation_arr();

				for (var i = 0; i < arr.length; i++) {
					delete_all_donee();
				}

				var container = angular.element( document.querySelector( '#donation_tbody' ));
				var div = '<div class="md-form-group float-label">';
				for (var i = 0; i < response.donee_count; i++) {
					markup = "<tr id='donee_tbl'><td id='"+i+"'><input type='checkbox' name='record'></td>";
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dname'+i+'" ng-keyup="user_d.dname'+i+'=user_d.dname'+i+'.toUpperCase();" ng-init='+"'dname'"+'></div></td>';
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dadd'+i+'" ng-keyup="user_d.dadd'+i+'=user_d.dadd'+i+'.toUpperCase();validation_donation('+"'dadd"+i+"'"+',user_d.dadd'+i+');" ng-init='+"'dadd'"+'></div></td>';
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dcity'+i+'" ng-keyup="user_d.dcity'+i+'=user_d.dcity'+i+'.toUpperCase();validation_donation('+"'dcity"+i+"'"+',user_d.dcity'+i+');" ng-init='+"'dcity'"+'></div></td>';

					markup += '<td>'+div+'<select class="md-input" ng-model="user_d.dstate'+i+'" ng-change="validation_donation('+"'dstate"+i+"'"+',user_d.dstate'+i+');" style="top: 2px;"><option value="">Select State</option> <option ng-repeat="field in items" value="<%field.state%>"><%field.state%></option></select></div></td>';
					
					// markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dstate'+i+'" ng-keyup="validation_donation('+"'dstate"+i+"'"+',user_d.dstate'+i+');" ng-init='+"'dstate'"+'></div></td>';
					
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dpin'+i+'" ng-keyup="validation_donation('+"'dpin"+i+"'"+',user_d.dpin'+i+');" ng-init='+"'dpin'"+'></div></td>';
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dpan'+i+'" ng-keyup="user_d.dpan'+i+'=user_d.dpan'+i+'.toUpperCase();" ng-init='+"'dpan'"+'></div></td>';
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.damt'+i+'" ng-keyup="validation_donation('+"'damt"+i+"'"+',user_d.damt'+i+');calculate_eligible_donation_amt(user_d.dper'+i+',user_d.damt'+i+','+i+')" ng-init='+"'damt'"+'></div></td>';

					markup += '<td>'+div+'<select class="md-input" ng-model="user_d.dper'+i+'" ng-change="validation_donation('+"'dper"+i+"'"+',user_d.dper'+i+');calculate_eligible_donation_amt(user_d.dper'+i+',user_d.damt'+i+','+i+')" style="top: 2px;"><option value="">Select Percent</option> <option ng-repeat="field in percents" value="<%field.percent%>"><%field.percent%></option></select></div></td>';
					// markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dper'+i+'" ng-keyup="validation_donation('+"'dper"+i+"'"+',user_d.dper'+i+');calculate_eligible_donation_amt(user_d.dper'+i+',user_d.damt'+i+','+i+')" ng-init='+"'dper'"+'></div></td>';
					markup += '<td>'+div+'<input class="md-input" ng-model="user_d.deamt'+i+'" ng-keyup="validation_donation('+"'deamt"+i+"'"+',user_d.deamt'+i+');" ng-init='+"'deamt'"+'></div></td>';
					markup += "</tr>";
					var temp = $compile(markup)($scope);
					container.append( temp );

					
					$scope.user_d['dname'+i] = response.name[i];
					$scope.user_d['dadd'+i] = response.address[i];
					$scope.user_d['dcity'+i] = response.city[i];
					$scope.user_d['dstate'+i] = response.state[i];
					$scope.user_d['dpin'+i] = response.pin[i];
					$scope.user_d['dpan'+i] = response.pan[i];
					$scope.user_d['damt'+i] = response.amount[i];
					$scope.user_d['dper'+i] = response.percent[i];

					// $scope.$apply();

					if((response.percent[i]!='' || response.percent[i]!=undefined) && (response.amount[i]!='' || response.amount[i]!=undefined)){
						eligible_amount=$scope.number_format(response.amount[i])*$scope.number_format(response.percent[i])/100
						// console.log(eligible_amount)
						$scope.user_d['deamt'+i]=eligible_amount
					}

				}
				//empty table before assigning anything
				$scope.update_donee_tbl();

			}
		});
	}

	$scope.calculate_eligible_donation_amt=function(percent,amount,i){
		
		if((percent!='' || percent!=undefined) && (amount!='' || amount!=undefined)){
			eligible_amount=$scope.number_format(amount)*$scope.number_format(percent)/100
			// console.log(eligible_amount)
			$scope.user_d['deamt'+i]=eligible_amount
		}

		$scope.calculate_80g();

		// $scope.$apply();
	}

	$scope.calculate_80g=function(){

		arr = donation_arr();
		
		total=0
		for (var i = 0; i <= arr.length; i++) {
			
			if($scope.user_d['deamt'+i]==undefined || $scope.user_d['deamt'+i]=='')
				value=0
			else
				value=parseInt($scope.number_format($scope.user_d['deamt'+i]))

			// console.log(value)
			total=total+value
		}

		// console.log(total)
		$scope.user_d.section_80g=$scope.money_format(total)
	}

	$scope.add_row_donation = function(){
		arr = donation_arr();
		if(arr.length == 0)
			i = 1;
		else
			i = parseInt(arr[arr.length-1])+1;
		// i = donation_row_count();
		var container = angular.element( document.querySelector( '#donation_tbody' ));
		var div = '<div class="md-form-group float-label">';
		markup = "<tr id='donee_tbl'><td id='"+i+"'><input type='checkbox' name='record'></td>";
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dname'+i+'" ng-keyup="user_d.dname'+i+'=user_d.dname'+i+'.toUpperCase();" ng-init='+"'dname'"+'></div></td>';
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dadd'+i+'" ng-keyup="user_d.dadd'+i+'=user_d.dadd'+i+'.toUpperCase();validation_donation('+"'dadd"+i+"'"+',user_d.dadd'+i+');" ng-init='+"'dadd'"+'></div></td>';
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dcity'+i+'" ng-keyup="user_d.dcity'+i+'=user_d.dcity'+i+'.toUpperCase();validation_donation('+"'dcity"+i+"'"+',user_d.dcity'+i+');" ng-init='+"'dcity'"+'></div></td>';
		
		markup += '<td>'+div+'<select class="md-input" ng-model="user_d.dstate'+i+'" ng-keyup="validation_donation('+"'dstate"+i+"'"+',user_d.dstate'+i+');" style="top: 2px;"><option selected="" disabled="" value="">Select State</option> <option ng-repeat="field in items" value="<%field.state%>"><%field.state%></option></select></div></td>';
		// markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dstate'+i+'" ng-keyup="validation_donation('+"'dstate"+i+"'"+',user_d.dstate'+i+');" ng-init='+"'dstate'"+'></div></td>';
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dpin'+i+'" ng-keyup="validation_donation('+"'dpin"+i+"'"+',user_d.dpin'+i+');" ng-init='+"'dpin'"+'></div></td>';
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dpan'+i+'" ng-keyup="user_d.dpan'+i+'=user_d.dpan'+i+'.toUpperCase();" ng-init='+"'dpan'"+'></div></td>';
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.damt'+i+'" ng-keyup="validation_donation('+"'damt"+i+"'"+',user_d.damt'+i+');calculate_eligible_donation_amt(user_d.dper'+i+',user_d.damt'+i+','+i+')" ng-init='+"'damt'"+'></div></td>';
		// markup += '<td>'+div+'<input class="md-input" ng-model="user_d.dper'+i+'" ng-keyup="validation_donation('+"'dper"+i+"'"+',user_d.dper'+i+');calculate_eligible_donation_amt(user_d.dper'+i+',user_d.damt'+i+','+i+')" ng-init='+"'dper'"+'></div></td>';
		markup += '<td>'+div+'<select class="md-input" ng-model="user_d.dper'+i+'" ng-change="validation_donation('+"'dper"+i+"'"+',user_d.dper'+i+');calculate_eligible_donation_amt(user_d.dper'+i+',user_d.damt'+i+','+i+')" style="top: 2px;"><option value="">Select Percent</option> <option ng-repeat="field in percents" value="<%field.percent%>"><%field.percent%></option></select></div></td>';
		markup += '<td>'+div+'<input class="md-input" ng-model="user_d.deamt'+i+'" ng-keyup="validation_donation('+"'deamt"+i+"'"+',user_d.deamt'+i+');" ng-init='+"'deamt'"+'></div></td>';
		markup += "</tr>";
		var temp = $compile(markup)($scope);
		container.append( temp );
		console.log(i);
	}

	$scope.validation_donation = function(variable){
		var status = true;
		arr = donation_arr();

		for (var i = 0; i < arr.length; i++) {
			var j = arr[i];
			if(variable == 'name'){
				// alert( $scope.fetch_dynamic_variable(i) );
				temp_name = $scope.fetch_dynamic_variable(j,variable);
				if(temp_name == '' || temp_name == undefined || temp_name.length <= 1)
					status = false;
			}
			if(variable == 'address'){
				temp_add = $scope.fetch_dynamic_variable(j,variable);
				if(temp_add == '' || temp_add == undefined)
					status = false;
			}
			if(variable == 'city'){
				temp_city = $scope.fetch_dynamic_variable(j,variable);
				if(temp_city == '' || temp_city == undefined)
					status = false;
			}
			if(variable == 'state'){
				temp_state = $scope.fetch_dynamic_variable(j,variable);
				if(temp_state == '' || temp_state == undefined)
					status = false;
			}
			if(variable == 'pin'){
				temp_pin = $scope.fetch_dynamic_variable(j,variable);
				if(temp_pin == '' || temp_pin == undefined)
					status = false;
				if((/\d{6}/.test(temp_pin)) && temp_pin.length==6)
					status = true;
				else
					status = false;
			}
			if(variable == 'pan'){
				temp_pan = $scope.fetch_dynamic_variable(j,variable);
				if(temp_pan == '' || temp_pan == undefined)
					status = false;
				if((/[A-Z]{5}\d{4}[A-Z]{1}/.test(temp_pan)) && temp_pan.length==10)
					status = true;
				else
					status = false;
			}
			if(variable == 'amount'){
				temp_amt = $scope.fetch_dynamic_variable(j,variable);
				if(temp_amt == '' || temp_amt == undefined)
					status = false;
				if( /^\d+$/.test(temp_amt) )
					status = true;
				else
					status = false;
			}
			if(variable == 'e_amount'){
				temp_e_amt = $scope.fetch_dynamic_variable(j,variable);
				if(temp_e_amt == '' || temp_e_amt == undefined)
					status = false;
				if( /^\d+$/.test(temp_e_amt) )
					status = true;
				else
					status = false;
			}
		}
		if (status) 
			return true;
		else
			return false;
	}

	$scope.fetch_dynamic_variable = function(value,variable){
		if (variable == 'name')
			return $scope.user_d['dname'+value]
		else if (variable == 'address')
			return $scope.user_d['dadd'+value]
		else if (variable == 'city')
			return $scope.user_d['dcity'+value]
		else if (variable == 'state')
			return $scope.user_d['dstate'+value]
		else if (variable == 'pin')
			return $scope.user_d['dpin'+value]
		else if (variable == 'pan')
			return $scope.user_d['dpan'+value]
		else if (variable == 'amount')
			return $scope.user_d['damt'+value]
		else if (variable == 'e_amount')
			return $scope.user_d['deamt'+value]
		else if (variable == 'percent')
			return $scope.user_d['dper'+value]
		else
			return ''
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return ''

		if(!isNaN(x)){
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.number_format = function(val){

		if(val!=undefined){
			val = val.toString();
			val = val.replace(/[,]/g, "");
			return val;
		}else{
			return 0
		}
		
		
		
	}

	$scope.self_insurance_premium_disabled=false
	$scope.self_medical_expenditure_disabled=false

	$scope.parent_insurance_premium_disabled=false
	$scope.parent_medical_expenditure_disabled=false
	
	$scope.restrict_to_fill=function(keyword,value){

		// console.log(keyword)
		// console.log(value)
		if(keyword=='self_insurance_premium'){
			if(value!=0 && value!=''){
				$scope.self_medical_expenditure_disabled=true
			}else{
				$scope.self_medical_expenditure_disabled=false
			}
		}else if(keyword=='self_medical_expenditure'){
			if(value!=0 && value!=''){
				$scope.self_insurance_premium_disabled=true
			}else{
				$scope.self_insurance_premium_disabled=false
			}
		}else if(keyword=='parent_insurance_premium'){
			if(value!=0 && value!=''){
				$scope.parent_medical_expenditure_disabled=true
			}else{
				$scope.parent_medical_expenditure_disabled=false
			}
		}else if(keyword=='parent_medical_expenditure'){
			if(value!=0 && value!=''){
				$scope.parent_insurance_premium_disabled=true
			}else{
				$scope.parent_insurance_premium_disabled=false
			}
		}
	}

}]);