
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

/*date / add */
app.directive('myDate', dateInput);
function dateInput($filter, $parse){
	return {
      restrict: 'A',
      require: 'ngModel',
      replace: true,
      transclude: true,
      template: '<input ng-transclude ui-mask="39/19/2999" ui-mask-raw="false" placeholder="DD/MM/YYYY"/>',
      link: function(scope, element, attrs, controller) {
        scope.limitToValidDate = limitToValidDate;
        var dateFilter = $filter("date");
        var today = new Date();
        var date = {};

        function isValidDay(day) {
          return day > 0 && day < 32;
        }

        function isValidMonth(month) {
          return month >= 0 && month < 12;
        }

        function isValidYear(year) {
          return year > (today.getFullYear() - 115) && year < (today.getFullYear() + 115);
        }

        function isValidDate(inputDate) {
          inputDate = new Date(formatDate(inputDate));
          if (!angular.isDate(inputDate)) {
            return false;
          }
          date.day = inputDate.getDate();
          date.month = inputDate.getMonth();
          date.year = inputDate.getFullYear();
          return (isValidDay(date.day)  &&  isValidMonth(date.month) && isValidYear(date.year));
        }

        function formatDate(newDate) {
          var modelDate = $parse(attrs.ngModel);
          newDate = dateFilter(newDate, "dd/mm/yyyy");
          // console.log(newDate)
          modelDate.assign(scope, newDate);
          return newDate;
        }

        controller.$validators.date = function(modelValue) {
          return angular.isDefined(modelValue) && isValidDate(modelValue);
        };

        var pattern = "^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)\\d\\d$" +
          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)\\d$" +
          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)$" +
          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[12]$" +
          "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])$" +
          "|^(0[1-9]|1[012])([0-3])$" +
          "|^(0[1-9]|1[012])$" +
          "|^[01]$";
        var regexp = new RegExp(pattern);

        function limitToValidDate(event) {
          var key = event.charCode ? event.charCode : event.keyCode;
          if ((key >= 48 && key <= 57) || key === 9 || key === 46) {
            var character = String.fromCharCode(event.which);
            var start = element[0].selectionStart;
            var end = element[0].selectionEnd;
            var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\//g, "");
            if (!(regexp.test(testValue))) {
              event.preventDefault();
            }
          }
        }
      }
    }
};
/*end date /*//**/

app.controller('salat_tax_controller', ['$scope','$rootScope','$compile', function($scope,$rootScope,$compile,$http,CONFIG) {
	get_txt = 0;
	client_id = 1;
	$('.loader').hide();
	otp_status = '';
	$scope.wait_modal_text1 = 'Verifying your PAN';
	$scope.wait_modal_text2 = 'Registering you on Salat Tax';
	$scope.show_resend_btn = true;
	$scope.otp_msg = false;
	var wait_modal_time = 0;
	$scope.captcha_img = '';

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					business_partner_id=response.business_partner_id
					$rootScope.role=response.role
					// console.log($rootScope.role)
					role=response.role
					login_email=response.email

					$scope.$apply();

					if(role=='Client'){

						$.ajax({
							url:'/get_pan_info/',
							type:'POST',
							dataType: 'json',
							data:{'client_id':client_id},
							success: function(response){
								console.log(response);
								pan = response.pan;
								if(pan!='')
									$scope.user.pan = pan;
								if(response.dob!='')
									$scope.user.dob = response.dob.replace(/-/g,'/');
								
								if(response.business_partner_id!=''){
									business_partner_id=response.business_partner_id
								}

								console.log(business_partner_id)

								$scope.$apply();
							}
						});

					}
				
				}
				
				// $.ajax({
				// 	url:'/user_tracking_func/',
				// 	type:'POST',
				// 	dataType: 'json',
				// 	data:{'event':'viewed','event_name':redirect_url,'client_id':client_id},
				// 	success: function(response){
				// 		console.log(response);
				// 	}
				// });
			}
		});
	},1000);

	$scope.validation_text = function(type,value,$event) {
		$scope.otperr = "";
		if(type == "salat_mail_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "salat_mail_otp"){
					$scope.otperr= "";
					return true;
				}
			}else{
				if(type == "salat_mail_otp"){
					$scope.otperr= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "salat_mail_otp") && value==undefined){
			if(type == "salat_mail_otp"){
				$scope.otperr= "Please Check Your E-mail OTP";
				return false;
			}
		}
		if(type == "salat_mobile_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "salat_mobile_otp"){
					$scope.otperr= "";
					return true;
				}
			}else{
				if(type == "salat_mobile_otp"){
					$scope.otperr= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "salat_mobile_otp") && value==undefined){
			if(type == "salat_mobile_otp"){
				$scope.otperr= "Please Check Your Mobile OTP";
				return false;
			}
		}
		$scope.$apply();
	}

	$scope.forgot_password = function(pan){
		$.ajax({
			url:'/forgot_password/',
			type:'POST',
			dataType: 'json',
			data:{'pan':pan},
			success: function(response){
				get_txt = 1;
				console.log(response);
				// AVOPD6885F - 24/08/1989
				// AYHPT6350R - 10/10/1995
				if(response.text.split(" ").length == 2){
					$scope.email_txt = response.text.split(" ")[0];
					$scope.mobile_txt = response.text.split(" ")[1];
				}
				if(response.text == " "){
					$scope.email_txt = 'Email ID Registered in IT';
					$scope.mobile_txt = 'Mobile No. Registered in IT';
				}
				$scope.$apply();
			},error:function(){
				console.log(response)
			}
		});
	}

	$scope.get_ITR = function(){
		console.log('get ITR function')
		if(get_wait_modal_status() == 1)
			$('#wait_modal').modal('hide');
		otp_status = '';
		// cleartimer();
		var pan = $scope.user.pan;
		var dob = $scope.user.dob;
		var pan_status = false;
		var dob_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if(dob!==''){
			dob_status = true;
			$scope.mail_err= "";
		}
		else
			$scope.mail_err= "DOB is required";

		if( pan_status == true && dob_status==true){
			var IsSalat = $scope.IsSalat(pan);
			if(get_wait_modal_status() == 0)
				$('#wait_modal').modal('show');
			if(get_txt == 0)
				$scope.forgot_password(pan);
			// timer();
			// $('.loader').show();
			$.ajax({
				url:'/download_ITR/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'option':'calculate','dob':dob,'client_id':client_id,'business_partner_id':business_partner_id},
				success: function(response){
					clearInterval(set_isSalat);
					var container = angular.element( document.querySelector( '#ITR_content' ));
					container.empty();
					// $('.loader').hide();
					console.log(response);

					if(typeof response.status == 'string'){
						stoptimer();
						if(response.status.indexOf('index')>0 || response.status.indexOf('Error download ITR')>=0){
							setTimeout(function(){
								if(get_wait_modal_status() == 1)
									$('#wait_modal').modal('hide');
								// $('#modal_success').modal('hide');
							}, 1000);
							$scope.get_ITR();
						}
					}	
					/*  status: {pan_exist: "This PAN is added as Salat Client.", pan_status: "", no_of_ITR: 5, otp_status: "", dob_status: ""}
						status:
						dob_status: ""
						no_of_ITR: 5
						otp_status: ""
						pan_exist: "This PAN is added as Salat Client."
						pan_status: ""
						{status: "Error download ITR : list index out of range"}
					*/
					if(response.count_itr > 0){
						$scope.ITR_Data = {'display':'block'};
						$scope.ITR_entry_count= "";
					}
					if(response.status.pan_status == 'This PAN is not registered with e-Filing.'){
						$scope.pan_err = 'This PAN is not registered with e-Filing.';
						clearInterval(set_isSalat);
						if(get_wait_modal_status() == 1)
							$('#wait_modal').modal('hide');
					}
					if(response.status.dob_status == 'Invalid Date'){
						$scope.pan_err = 'Invalid Date Of Birth';
						clearInterval(set_isSalat);
						if(get_wait_modal_status() == 1)
							$('#wait_modal').modal('hide');
					}
					if(response.status.otp_status == 'Invalid Mobile OTP'){
						otp_status = 'Invalid Mobile OTP';
						$scope.user.mobile_otp = '';
						// $('.loader').hide();
						$scope.otperr = 'Invalid Mobile OTP. Please re-enter';
						$('#modal_otp').modal('show');
					}
					if(response.status.otp_status == 'Invalid Mail OTP'){
						otp_status = 'Invalid Mail OTP';
						$scope.user.mail_otp = '';
						// $('.loader').hide();
						$scope.otperr = 'Invalid E-mail OTP. Please re-enter';
						$('#modal_otp').modal('show');
					}
					if(response.status.pan_status == 'PAN does not exist'){
						$scope.pan_err = 'Please Enter Valid PAN';
					}
					//otp_status: "success"
					if(response.status.otp_status == 'waiting'){
						otp_status = 'waiting';
						$('#modal_otp').modal('show');
						if(get_wait_modal_status() == 1)
							$('#wait_modal').modal('hide');
					}
					if(response.status.otp_status == 'success'){
						otp_status = 'success';
						$scope.user.mail_otp = '';
						$scope.user.mobile_otp = '';
						// $('.loader').hide();
						$('#modal_otp').modal('hide');
						$scope.get_ITR();
					}
					if(response.status.status == 'complete'){
						setTimeout(function(){
							if(get_wait_modal_status() == 1)
								$('#wait_modal').modal('hide');
						}, 3000);
						setTimeout(function(){
							$('#modal_success').modal('show');
							setTimeout(function(){
								window.location.href = "/calculate_tax";
							}, 4000);
						}, 3000);
					}
					if(response.count_itr>1 && response.option=='calculate' && response.status=='success'){
						setTimeout(function(){
							if(get_wait_modal_status() == 1)
								$('#wait_modal').modal('hide');
						}, 3000);
						setTimeout(function(){
							$('#modal_success').modal('show');
							setTimeout(function(){
								window.location.href = "/calculate_tax";
							}, 4000);
						}, 3000);
					}
					$scope.$apply();
					stoptimer();
				},
				error: function(response){
					// $('.loader').hide();
					stoptimer();
					$scope.get_ITR();
				}
			});
		}
	}

	$scope.IsSalat = function(pan){
		console.log('function call "IsSalat"');
		set_isSalat = setInterval(check_isSalat, 4000); //every 5sec
		function check_isSalat() {
			$.ajax({
				url:'/check_isSalat/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan},
				success: function(response){
					console.log(response);
					if(response.flag == '2' || response.flag == '1' ){
						clearInterval(set_isSalat);
						if(get_wait_modal_status() == 1)
							$('#wait_modal').modal('hide');
						if(get_wait_modal_status() == 0)
							$('#wait_modal').modal('show');
						$scope.wait_modal_text1 = 'Confirming your registration on Salat Tax';
						$scope.wait_modal_text2 = 'Good things take time.';
					}
					if(response.flag == '0'){
						// $('.loader').hide();
						clearInterval(set_isSalat);
						if(otp_status == 'waiting'){
							if(get_wait_modal_status() == 1)
								$('#wait_modal').modal('hide');
							$('#modal_otp').modal('show');
						}
					}
					$scope.$apply();
				}
			});
		}
	}
	/*$scope.wait_modal_timer = function(option){
		console.log(option);
		set_timer = setInterval(check_timer, 100); //every 5sec
		function check_timer() {
			wait_modal_time = wait_modal_time + 1;
		}
	}*/

	$scope.get_otp = function(user,$event) {
		var salat_mail_otp_status = $scope.validation_text("salat_mail_otp",user.mail_otp);
		var salat_mobile_otp_status = $scope.validation_text("salat_mobile_otp",user.mobile_otp);
		if (salat_mail_otp_status == false && salat_mobile_otp_status == true) {
			salat_mail_otp_status = $scope.validation_text("salat_mail_otp",user.mail_otp);
		}
		var salat_mail_otp = user.mail_otp;
		var salat_mobile_otp = user.mobile_otp;
		var pan = user.pan;
		// alert(salat_mail_otp_status);
		// alert(salat_mobile_otp_status);
		// alert($scope.otperr);
		if(salat_mail_otp_status == true && salat_mobile_otp_status == true && $scope.otperr == ''){
			// $('.loader').show();
			$.ajax({
				url:'/save_salat_otp/',
				type:'POST',
				dataType: 'json',
				data:{'salat_mail_otp':salat_mail_otp,'salat_mobile_otp':salat_mobile_otp,'pan':pan},
				success: function(response){
					console.log(response);
					// $scope.wait_modal_text1 = 'Confirming your registration on Salat Tax';
					// $scope.wait_modal_text2 = 'Good things take time.';
					$('#modal_otp').modal('hide');
					$scope.multiple_eri();
					// $scope.$apply();
				}
			});
		}
	}

	$scope.multiple_eri = function(){
		console.log('multiple_eri function')
		if(get_wait_modal_status() == 1)
			$('#wait_modal').modal('hide');
		otp_status = '';
		// cleartimer();
		var pan = $scope.user.pan;
		var dob = $scope.user.dob;
		var email=$scope.user.email;
		var pan_status = false;
		var dob_status = false;
		var email_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if(dob!==''){
			dob_status = true;
			$scope.mail_err= "";
		}
		else
			$scope.mail_err= "DOB is required";

		if(role=='Partner' || role=='Admin'){
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(email!==''){
				if(re.test(email) ){
					$scope.mail1_err= "";
					email_status = true;
				}else
					$scope.mail1_err= "Please Enter Valid Email";
			}
			else
				$scope.mail1_err= "Email is required";

			user_email=email
		}else{
			email_status = true;
			user_email=login_email
		}
		
		// for testing forgot_password() only
		// pan_status = false;
		// $scope.forgot_password(pan);
		
		if( pan_status == true && dob_status==true && email_status==true) {
			if(get_wait_modal_status() == 0)
				$('#wait_modal').modal('show');
			$.ajax({
				url:'/check_isSalat/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'role':role,'email':user_email,'b_id':business_partner_id},
				success: function(response){
					console.log(response);

					if(response.entry_exist==1 && response.status==1){
						$scope.pan_err='PAN already registered under other partner'
						$('#wait_modal').modal('hide');
						$scope.$apply()
					}else{


						client_id=response.client_id
						$rootScope.client_id=client_id
					
						if( response.flag == '1' ){
							if(get_wait_modal_status() == 1)
								$('#wait_modal').modal('hide');
								
							$('#otp_modal').modal('show');
							$.ajax({
								url:'/multiple_eri/',
								type:'POST',
								dataType: 'json',
								data:{'pan':pan,'dob':dob,'option':'salatclient','client_id':client_id,'b_id':business_partner_id},
								success: function(response){
									console.log(response);
								}
							});
							setTimeout(function(){
								$('#otp_modal').modal('hide');
								if(get_wait_modal_status() == 1)
									$('#wait_modal').modal('hide');
								$('#modal_success').modal('show');
								setTimeout(function(){
									//window.location.href = "/calculate_tax";
									$scope.goto(role);
								}, 3000);
							}, 15000);
							ga('send','event','Registration','Completed','Label',1)
							$scope.user_tracking('Registration','Completed');
						}
						else{
							ga('send','event','Registration','Pending','Label',1)
							$scope.user_tracking('Registration','Pending');
							if(get_txt == 0)
								$scope.forgot_password(pan);
							$.ajax({
								url:'/multiple_eri/',
								type:'POST',
								dataType: 'json',
								data:{'pan':pan,'dob':dob,'option':'notsalatclient','client_id':client_id,'b_id':business_partner_id},
								success: function(response){
									// if(get_wait_modal_status() == 1)
									// 	$('#wait_modal').modal('hide');
									console.log(response);
									if(typeof response.status == 'string'){
										stoptimer();
										if(response.status.indexOf('index')>0 || response.status.indexOf('Error download ITR')>=0){
											setTimeout(function(){
												if(get_wait_modal_status() == 1)
													$('#wait_modal').modal('hide');
											}, 1000);
											$scope.multiple_eri();
										}
									}
									if(response.status.pan_status == 'This PAN is not registered with e-Filing.'){
										$scope.pan_err = 'This PAN is not registered with e-Filing.';
										clearInterval(set_isSalat);
										if(get_wait_modal_status() == 1)
											$('#wait_modal').modal('hide');
									}
									if(response.status.dob_status == 'Invalid Date'){
										$scope.pan_err = 'Invalid Date Of Birth';
										if(get_wait_modal_status() == 1)
											$('#wait_modal').modal('hide');
									}
									if(response.status.otp_status == 'Invalid Mobile OTP'){
										otp_status = 'Invalid Mobile OTP';
										$scope.user.mobile_otp = '';
										$scope.user.mail_otp = '';
										$scope.otperr = 'Invalid OTP. Please re-enter';
										$('#modal_otp').modal('show');
									}
									if(response.status.otp_status == 'Invalid Mail OTP'){
										otp_status = 'Invalid Mail OTP';
										$scope.user.mobile_otp = '';
										$scope.user.mail_otp = '';
										$scope.otperr = 'Invalid OTP. Please re-enter';
										$('#modal_otp').modal('show');
									}
									if(response.status.pan_status == 'PAN does not exist'){
										$scope.pan_err = 'Please Enter Valid PAN';
									}
									if(response.status.otp_status == 'waiting'){
										otp_status = 'waiting';
										$('#modal_otp').modal('show');
										if(get_wait_modal_status() == 1)
											$('#wait_modal').modal('hide');
									}
									if(response.status.otp_status == 'success' || response.status.pan_exist == 'This PAN is added as Salat Client.' ){
										otp_status = 'success';
										$scope.user.mail_otp = '';
										$scope.user.mobile_otp = '';
										$('#modal_otp').modal('hide');
										if(get_wait_modal_status() == 1)
											$('#wait_modal').modal('hide');
										$scope.multiple_eri();
									}
									$scope.$apply();
								},
								error: function(response){

									console.log(response)
									stoptimer();
									$scope.multiple_eri();
								}
							});
						}

					}

					
				}
			});
		}
	}

	$scope.resend_otp = function(user,$event){
		$('#modal_otp').modal('hide');
		$scope.multiple_eri();
	}

	$scope.hide_resend = function(user){
		$scope.show_resend_btn = true;
		var mail_otp_valid = (/^([0-9]{6})$/.test(user.mail_otp)) ;
		var mobile_otp_valid = (/^([0-9]{6})$/.test(user.mobile_otp)) ;
		// console.log( mail_otp_valid + ' ' + mobile_otp_valid);
		if(mail_otp_valid == true && mobile_otp_valid == true){
			$scope.show_resend_btn = false;
		}
	}

	$scope.show_msg = function(user){
		$scope.otp_msg = false;
		var pan = user.pan;
		var dob = user.dob;
		var pan_status = false;
		var dob_status = false;
		var regex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) )
				pan_status = true;
		}
		if(dob!==''){
			if( regex.test(dob) )
				dob_status = true;
		}

		if(pan_status == true && dob_status == true)
			$scope.otp_msg = true;
	}

	$scope.goto = function(role){
		console.log(role)
		var btnclick = getCookie('btnclick');
		// alert('btnclick');
		if(btnclick == 'file your return' || (role=='Partner' || role=='Admin')){
			
			console.log('if')
			$('#modal_success').modal('hide');
			$('#m-a-a').modal('show');
		}
		else
			window.location.href = "/calculate_tax";
	}

	// for Adhar
	$scope.get_adhar_xml = function(){
		// alert('get_adhar_xml');
		set_aadharcaptcha = setInterval(check_aadharcaptcha, 2000); //every 2sec
		function check_aadharcaptcha() {
			$.ajax({
				url:'/check_aadharcaptcha/',
				type:'POST',
				dataType: 'json',
				data:{'aadhar':'','option':'check'},
				success: function(response){
					console.log(response);
					if(response.file_exist == 1 ){
						clearInterval(set_aadharcaptcha)
						setTimeout(function(){
							$scope.captcha_img = '../static/aadhar/859748396648.png';
							$scope.$apply();
						}, 2000);
					}
				}
			});
		}
		$.ajax({
			url:'/get_adhar_xml/',
			type:'POST',
			dataType: 'json',
			data:{'pan':'pan','b_p_id':'B5','aadhar':''},
			success: function(response){
				console.log(response);
				
			}
		});
	}

	$scope.submit_captcha = function(captcha_text){
		$.ajax({
			url:'/save_adhar_captcha/',
			type:'POST',
			dataType: 'json',
			data:{'text':captcha_text,'aadhar':'','option':'captcha_text'},
			success: function(response){
				console.log(response);
			}
		});
	}

	$scope.submit_adhar_otp = function(aadhar_otp){
		$.ajax({
			url:'/save_adhar_captcha/',
			type:'POST',
			dataType: 'json',
			data:{'text':aadhar_otp,'aadhar':'','option':'aadhar_otp'},
			success: function(response){
				console.log(response);
			}
		});
	}

}]);

// <div class="note mart10" style="text-align: justify; font-weight: 500;">
// Please check for email from Department for OTP in your Inbox as well as the Spam or Junk folder(mail id - <strong>DONOTREPLY@incometaxindiaefiling.gov.in</strong>)
// </div>
// 763420118883
// 989689046076