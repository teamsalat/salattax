
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});


app.controller('exempt_income_controller', ['$scope', function($scope,CONFIG) {
  
	redirect_url = $scope.url;
	var client_id = 1;
	console.log('Client ID : '+$scope.user.client_id);
    client_id=$scope.user.client_id

	$scope.$watch("demoInput", function(){
		console.log('ID : '+$scope.user.id);
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				if (response.client_id != 0 && $scope.user.id == '') {
					client_id = response.client_id;
					email = response.email;
					$scope.get_exempt();
				}
				else if ($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				if ($scope.user.id != '') {
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						data:{'username':$scope.user.id},
						success: function(response){
							// console.log(response);
							// client_id = response.client_id;
							email = response.email;
							$scope.get_exempt();
						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
			}
		});
	},200);

	$scope.validation = function(type,value,$event) {
		// Common Deductions
		if((type =="house_rent_allownce" || type =="leave_travel_allownce" || type =="other_allownce" || type =="dividend" || type =="agri_income" || type =="other_exempt_income") && value!==undefined){
			var pin_length=value.length;
			value = $scope.number_format(value);
			if( /^\d+$/.test(value) || pin_length==0){
				if(type == "house_rent_allownce")
					$scope.house_rent_allownce_error= ""; 
				else if(type == "leave_travel_allownce")
					$scope.leave_travel_allownce_error= ""; 
				else if(type == "other_allownce")
					$scope.other_allownce_error= ""; 
				else if(type == "dividend")
					$scope.dividend_error= ""; 
				else if(type == "agri_income")
					$scope.agri_income_error= ""; 
				else if(type == "other_exempt_income")
					$scope.other_exempt_income_error= ""; 

				return true;
			}else{
				if(type == "house_rent_allownce")
					$scope.house_rent_allownce_error= "Enter Valid House Rent Allownce";
				else if(type == "leave_travel_allownce")
					$scope.leave_travel_allownce_error= "Enter Valid Leave Travel Allownce";
				else if(type == "other_allownce")
					$scope.other_allownce_error= "Enter Valid Allownce";
				else if(type == "dividend")
					$scope.dividend_error= "Enter Valid Dividend";
				else if(type == "agri_income")
					$scope.agri_income_error= "Enter Valid Agriculture income";
				else if(type == "other_exempt_income")
					$scope.other_exempt_income_error= "Enter Valid Exempt Income";

				return false;
			}
		}else if((type =="house_rent_allownce" || type =="leave_travel_allownce" || type =="other_allownce" || type =="dividend" || type =="agri_income" || type =="other_exempt_income") && value==undefined){
			return true;
		}
	}

	$scope.gotoExempt = function($event) {
		var status=$scope.validation_check();

		if(status==true){
			var pass_obj = { "house_rent_allownce":$scope.number_format($scope.user.house_rent_allownce)};
			pass_obj["client_id"] = client_id;
			pass_obj["leave_travel_allownce"] = $scope.number_format($scope.user.leave_travel_allownce);
			// pass_obj["other_allownce"] = $scope.user.other_allownce;
			pass_obj["dividend"] = $scope.number_format($scope.user.dividend);
			pass_obj["agri_income"] = $scope.number_format($scope.user.agri_income);
			pass_obj["other_exempt_income"] = $scope.number_format($scope.user.other_exempt_income);
			var data_pass = JSON.stringify(pass_obj);
			$.ajax({
				url:'/save_exempt_income/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					if (response.status=='success') {
						$scope.user_tracking('submitted','Exempt Income');
						$scope.show_capital_gain();
						// window.location.href = 'http://127.0.0.1:8000/taxes_paid/';
						// window.location.href = redirect_url+'/deductions/'+client_id;
						// window.location.href = redirect_url+'/taxes_paid/'+client_id;
					}
					else
						$scope.user_tracking('error in data',response.status);
				}
			});
		}else
      		$scope.user_tracking('error in data','exempt income validation');
	}

	$scope.show_capital_gain=function(){
			console.log('show capital gain')
		    $('#exemptIncomeTab').removeClass('active');
            $('#capitalGainsTab').tab('show');
	}

	$scope.validation_check = function() {
		var house_rent_allownce_status = $scope.validation("house_rent_allownce",$scope.user.house_rent_allownce);
		var leave_travel_allownce_status = $scope.validation("leave_travel_allownce",$scope.user.leave_travel_allownce);
		var other_allownce_status = $scope.validation("other_allownce",$scope.user.other_allownce);
		var dividend_status = $scope.validation("dividend",$scope.user.dividend);
		var agri_income_status = $scope.validation("agri_income",$scope.user.agri_income);
		var other_exempt_income_status = $scope.validation("other_exempt_income",$scope.user.other_exempt_income);

		//alert('Checking validation');
		if( house_rent_allownce_status==true && leave_travel_allownce_status==true && other_allownce_status==true && dividend_status==true && agri_income_status==true && other_exempt_income_status==true){
			return true;
		}else
			return false;
	}

	$scope.get_exempt = function() {
		$.ajax({
			url:'/get_exempt_income/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				// console.log(response);
				if (response.HRA != 0)
					$scope.user.house_rent_allownce = $scope.money_format(response.HRA);
				if (response.LTA != 0)
					$scope.user.leave_travel_allownce = $scope.money_format(response.LTA);
				if (response.agri_income != 0)
					$scope.user.agri_income = $scope.money_format(response.agri_income);
				if (response.dividend != 0)
					$scope.user.dividend = $scope.money_format(response.dividend);
				if (response.other_exempt_income != 0)
					$scope.user.other_exempt_income = $scope.money_format(response.other_exempt_income);
				$scope.$apply();
			}
		});
		// setTimeout(function(){
		// 	console.log($scope.user.other_exempt_income)
		// 	$scope.exempt_income_float_label();
			
		// }, 1000);
	}

	// $scope.exempt_income_float_label=function(){
	// 	if($scope.user.house_rent_allownce != '')
	// 		label_float('HRA_div');
	// 	if($scope.user.leave_travel_allownce != '')
	// 		label_float('LTA_div');
	// 	if($scope.user.dividend != '')
	// 		label_float('dividend_div');
	// 	if($scope.user.agri_income != '')
	// 		label_float('agri_income_div');
	// 	if($scope.user.other_exempt_income != '')
	// 		label_float('other_exempt_income_div');
	// }

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return 0

		if(!isNaN(x)){
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.number_format = function(val){
		val = val.toString();
		val = val.replace(/[,]/g, "");
		
		return val;
	}

}]);