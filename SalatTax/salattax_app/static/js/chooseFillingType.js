app.controller('MyController', ['$scope','$rootScope','$compile', function($scope,$rootScope,$compile,$http,CONFIG) {
   
    $('.loader').hide();
    $.ajax({
        url:'/get_client/',
        type:'POST',
        dataType: 'json',
        data:{'mail':'mail'},
        success: function(response){
            $scope.cid=response.client_id
            $scope.bpid=response.business_partner_id
        }
    });


    redirect_url = $scope.url;  
    client_id=$scope.user.client_id;
    $scope.IsVisible = false;
    $scope.IsVisiblee = false;
    $scope.ShowBlock = function (value) {
        $scope.IsVisible = value == "Y";
        $scope.IsVisiblee = value == "N";
    }
    $scope.ButtonClick = function () {

        $scope.captchaError='';
        
        if($scope.email==''){
            $scope.emailError='Enter Email id';
        }else if($scope.captcha==''){
            $scope.captchaError='Enter Valid Captcha';
        }else{
            $('.loader').show();
            $.ajax({
                    url:"/capacthaSubmit/"+$scope.captcha+"/"+$scope.email,
                    type:'GET',
                    dataType: 'text',
                    success: function(response){
                        /*alert(response);*/
                        $('.loader').show();
                    }
            });
        }    
    }
    $scope.ButtonClick1 = function () {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.emailError='';
        if($scope.email==''){
            $scope.emailError='Enter Email id';
        }else if(!re.test($scope.email)){
            $scope.emailError='Enter Valid Email id';
        }else{
            $scope.emailError='';
            $('.loader').show(); 
                $.ajax({
                    url:"/camsMailBackReq/"+$scope.email,
                    type:'GET',
                    dataType: 'text',
                    success: function(response){
                        $('.loader').hide();
                        swal(response);
                    }
                });

                $.ajax({
                    url:"/capacthaFetche/"+$scope.email,
                    type:'GET',
                    dataType: 'text',
                    success: function(response){
                        $scope.Customer=response+'?dummy='+Math.floor(Math.random()*99999999999);
                        $scope.$apply();
                        $('.loader').hide();
                    }
                });

                /*$.ajax({
                    url:'/multiple_eri1/',
                    type:'POST',
                    dataType: 'json',
                    data:{'option':'salatclient','b_id':'1','R_id':'0'},
                    success: function(response){
                        alert(JSON.stringify(response));
                    }
                });*/
        }        
    }

    $scope.capitalGainsFileUpload = function (e, reader, file, fileList, fileOjects, fileObj){
        var file_name =client_id;

        var data=JSON.stringify(fileObj);
        var parse_data = JSON.parse(data);
        var filetype = parse_data['filetype'];
        var expr = /.pdf/;
        
        var check_pdf = filetype.match(expr);
        console.log(check_pdf);

        if(check_pdf){
            $('.loader').show();
            $.ajax({
                    url:'/file_upload/',
                    type:'POST',
                    dataType: 'json',
                    data:{'data':data,'pan':file_name ,'type':'ConsoAccoStatement'},
                    success: function(response){
                        status = JSON.stringify(response['status']).replace(/\"/g, "");
                        pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
                        password="salat123";
                        $('.loader').hide();
                        if(status=='success'){
                            $('.loader').show();
                            $.ajax({
                                    url:'/capitalGainReadingCalculation/',
                                    type:'POST',
                                    dataType: 'text',
                                    data:{'file_name':pdf_name,'password':password,'cid':$scope.cid,'bpid':$scope.bpid },
                                    success: function(response){
                                        swal(response);
                                        $('.loader').hide();
                                    }
                            });
                        }
                    }
            });    
        }else{
            alert('FileTypeNotValid');
            $('.loader').hide();
        }   
    }

    $scope.ButtonNnext = function () {
        if($('#selectOptionn').val()!=''){
            $('.loader').show();
            $.ajax({
                    url:'/chooseFillingType/',
                    type:'POST',
                    dataType: 'json',
                    data:{'client_id':$scope.cid,'b_id':$scope.bpid ,'expert_review_charge':$('#selectOptionn').val()},
                    success: function(response){
                        $('.loader').hide();
                        window.location.href = redirect_url+'/personal_information/'+client_id;
                    }
            });
        }else{
            swal('Please Choose flavours for IT return filing.');
        }
    }
}]);