
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('financial_goal_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {
	close_model = 0;
	client_id = 0;
	pan = '';
	p_age = '';
	i_year = '';
	shares_year = '';
	invested_share = '';
	$scope.total_salat_tax= "";
	$scope.select_image_err = false;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					$.ajax({
						url:'/get_pan_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							pan = response.pan;
							p_age = response.p_age;
							i_year = response.i_year;
							$scope.cal_tax(pan,p_age,i_year);
						}
					});
				}
			}
		});
	},1000);

	$scope.cal_tax = function(pan,p_age,i_year){
		var pan_status = false;
		// var invested_share = $scope.user.invested_share;
		invested_share = 'no';
		// var shares_year = $scope.user.shares_year;
		shares_year = i_year;
		var age_status = false;
		var shares_status = false;
		var total_salat_tax = 0;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";
		
		if(p_age=="undefined" )
			$scope.age_err= "Parent's Age is required";
		else{
			$scope.age_err= "";
			age_status = true;
		}

		if(i_year==''){
			invested_share = 'no';
			shares_status = true;
		}
		else{
			invested_share = 'yes';
			shares_status = true;
		}
		// alert(pan_status);
		// alert(age_status);
		// alert(shares_status);
		if( pan_status == true && age_status == true && shares_status==true){
			$.ajax({
				url:'/calculate_salat_tax/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'p_age':p_age,'invested_share':invested_share,'shares_year':shares_year},
				success: function(response){
					console.log(response);
					var s_obj = response.salat_tax ;
					if(response.count_salattax == 0)
						total_salat_tax = 10000;
					// 	window.location.href = "/tax_saving_account";

					for(var i = 0; i < response.count_salattax; i++) {
						total_salat_tax = total_salat_tax + s_obj[i].tax_leakage;
						// console.log("Current : "+s_obj[i].ToTax);
						// console.log("Total : "+total_salat_tax);
						$scope.total_salat_tax= $scope.nFormatter(total_salat_tax*-1);
					}
					hide_show_image(total_salat_tax);
					$scope.$apply();
				}
			});
		}
	}

	$scope.save_goal = function(){
		var selected_goal = get_selected_goal();
		$scope.select_image_err = false;
		if (selected_goal=='') {
			$scope.select_image_err = true;
		}else{
			$.ajax({
				url:'/update_financial_goal/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'selected_goal':selected_goal,'client_id':client_id},
				success: function(response){
					console.log(response);
					if(response.status == 'success'){
						// window.location.href = "/tax_report";
						$.ajax({
							url:'/calculate_salat_tax/',
							type:'POST',
							dataType: 'json',
							data:{'pan':pan,'p_age':p_age,'invested_share':invested_share,'shares_year':shares_year},
							success: function(response){
								console.log(response);
								var s_obj = response.salat_tax ;
								if(response.count_salattax == 0)
									window.location.href = "/tax_saving_account";
								else
									window.location.href = "/tax_report";
							}
						});
					}
				}
			});
		}
	}

	$scope.nFormatter = function(num, digits) {
		var si = [{ value: 1, symbol: "" },
		{ value: 1E3, symbol: " k" },
		{ value: 1E5, symbol: " lacs" },
		{ value: 1E7, symbol: "M" },
		{ value: 1E9, symbol: "G" },
		{ value: 1E12, symbol: "T" },
		{ value: 1E15, symbol: "P" },
		{ value: 1E18, symbol: "E" }
		];
		var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
		var i;
		for (i = si.length - 1; i > 0; i--) {
			if (num >= si[i].value) {
				break;
			}
		}
		return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

}]);