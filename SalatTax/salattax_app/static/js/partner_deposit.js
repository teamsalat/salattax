var dashapp = angular.module('partner_deposit_app', ['naif.base64']);

dashapp.config(function ($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});


dashapp.controller('partner_deposit_ctrl', function($rootScope,$scope,$http,$parse,$sce,$compile,$window) {

    //$scope.partnerId = $window.partnerId;

    //alert($scope.partnerId)

    domainName = document.location.origin;
    $scope.url = domainName;

    $scope.user={};

    $scope.PartnerList = {};
    $('.Partner_select').select2({
        placeholder: 'SELECT Partner'
    });
    $rootScope.cash_pay_error='';

    $scope.user.b_id='';

    $scope.bank_details_hide = true;

    $(".errr_msg").hide()
   
    console.log('Partner Dashboard')

    $('.loader').hide();

    var eamil_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var mobile_re = /^[6789]\d{9}$/;
    var pan_re = /[A-Z]{5}\d{4}[A-Z]{1}/;
    var pincode_re = /^\d{6}$/;
    var ifsc_re = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;

    $scope.all_validation=function(type,value){

      console.log(type)
      console.log(value)

      if((type =="plan_price") && value!==undefined){
        if(value==''){
            $scope.plan_price_err = "Select amount you want to deposit";
            $(".yes").html(",")
            return false;
        }else{
            $scope.plan_price_err = ""; 
            $(".yes").html("")
            $(".errr_msg").hide()
            return true;
            
        }
      }else if((type == "plan_price") && value==undefined){
          $scope.plan_price_err = "Select amount you want to deposit"; 
          $(".yes").html(",")
          return false;
      }

      if((type =="payment_mode") && value!==undefined){
        if(value==''){
            $scope.pay_mode_err = "Select mode of payment";
            $(".yes").html(",")
            return false;
        }else{
            $scope.pay_mode_err = ""; 
            $(".yes").html("")
            $(".errr_msg").hide()
            return true;
            
        }
      }else if((type == "payment_mode") && value==undefined){
          $scope.pay_mode_err = "Select mode of payment"; 
          $(".yes").html(",")
          return false;
      }

    }

    $scope.topup=function(){

      console.log('topup');

      $popup = $window.open('/partner_deposit/')
    
    }

    $scope.show_bank_details=function(mode){
    
      if(mode=="Cash")
      {
        $scope.bank_details_hide = false;
        
      }else if(mode=="Online")
      {
        $scope.bank_details_hide = true;

      }
    }

    $scope.submit_deposite=function(user){

      validation1 = $scope.all_validation('plan_price',$scope.user.deposit_amount)
      validation2 = $scope.all_validation('payment_mode',$scope.user.payment_mode)

      console.log($scope.user.b_id)
      console.log($scope.user.username)
      console.log($scope.user.user_id)
      console.log($scope.user.user_type)
      console.log($scope.user.business_type)

      user_id=$scope.user.user_id


      if(validation1==true && validation2==true){
        console.log('validation True')

        var data = angular.copy(user);
        var data = alldata = JSON.stringify(data);

        $.ajax({
            url:'/submit_deposite/',
            type:'POST',
            dataType:"json",
            async:false,
            data:{'data':data},
            success:function (response) { 
                console.log(response)
                if(response['status']=="Success"){

                  deposite_id=response['deposite_id']
                  amount=response['amount']
                  b_id=response['b_id']
                  email=response['email']
                  mobile=response['mobile']

                  swal('Data submited successfully');


                  var data={
                  "amount":amount,
                  "deposite_id":deposite_id,
                  "mobile":mobile,
                  "email":email,
                  "b_id":b_id}

                var data=JSON.stringify(data);
                if($scope.user.payment_mode=='Online'){
                    window.location.href = '/deposit_payment/';
                }else{
                  console.log('Data submited successfully')
                  swal('Data submited successfully')
                  var $popup = window.location.href = '/dashboard';
                }


                }else if(response['status']=="Error"){
                  swal('Error in Payment');
                }else{
                  
                  swal('Error in Payment');
                }
            },
            failure:function () {
              swal('Error in Payment');
              console.log('Error in Payment');
            }
          });


        //$(".errr_msg").show()
        //$scope.pay_mode_err = "Error in payment"; 
      }else{
        $(".errr_msg").show()
        console.log('validation False')
      }
    

    }


    //////// Clear cheque manually ////////////////////

    $scope.get_partner_data = function(){

      console.log('get_partner_data')

      $.ajax({
          url:'/fetch_partner_data/',
          type:'POST',
          dataType:"json",
          async:false,
          data:{'data':'data'},
          success:function (response) { 
              console.log(response)
              if(response['status']=="Success"){
                console.log('Partner data fetched successfully');

                console.log(response['PartnerList'])

                if(response['PartnerList']!=undefined && response['PartnerList']!=''){

                  //console.log(response.data.TeamMemberList);

                  var PartnerList_obj=response['PartnerList'];

                  //console.log(Object.keys(TeamMember_obj).length);

                  var PartnerList_length=Object.keys(PartnerList_obj).length;

                  for(i=0;i<PartnerList_length;i++){
                    if(PartnerList_obj[i].pan!=undefined && PartnerList_obj[i].pan!='null' && PartnerList_obj[i].pan!=''){
                      PartnerList_obj[i].name=PartnerList_obj[i].name+' : '+PartnerList_obj[i].pan
                    }
                  }

                  $scope.PartnerList = PartnerList_obj;
                  //$scope.$apply();
                    

                }else{
                  console.log($scope.PartnerList);
                  $scope.PartnerList = {};
                }

              }else if(response['status']=="Error"){
                console.log('Error in fetch partner data');
              }
          },
          failure:function () {
            console.log('Error in fetch partner data');
          }
        });
    }

    $scope.selectPartnerList = function(){
      console.log('selectRelationManList');
      $scope.PartnerList=$scope.PartnerList;
      console.log($scope.PartnerList);
    }

    $(".Partner_select").change(function(){
      $scope.user.PartnerId=$(".Partner_select").val();
      console.log($scope.user.PartnerId);
    });


    $scope.clear_cheque = function(user){
      console.log('clear_cheque');
      deposit_amount=$scope.user.deposit_amount;
      PartnerId=$scope.user.PartnerId;

      amount_flag=false
      partner_flag=false

      cheque_price_err=''
      partner_name_err=''

      if( deposit_amount!==1){
        if(deposit_amount==''){
            cheque_price_err = "Select deposit amount";
            amount_flag=false
        }else{
            $scope.cheque_price_err = ""; 
            amount_flag=true
            
        }
      }else if(deposit_amount==undefined){
          cheque_price_err = "Select deposit amount"; 
          amount_flag=false
      }

      if( PartnerId!==undefined){
        if(PartnerId=='0' || PartnerId==''){
            partner_name_err = "Select Partner";
            partner_flag=false
        }else{
            partner_name_err = ""; 
            partner_flag=true
            
        }
      }else if(PartnerId==undefined){
          partner_name_err = "Select Partner";
          partner_flag=false
      }

      console.log(amount_flag)
      console.log(partner_flag)

      $rootScope.cash_pay_error=''

      if(amount_flag==true && partner_flag==true){

        console.log('validation True')

        var data = angular.copy(user);
        var data = alldata = JSON.stringify(data);

        $('.loader').show();
        $.ajax({
            url:'/clear_deposit_cheque/',
            type:'POST',
            dataType:"json",
            //async:false,
            data:{'data':data},
            success:function (response) { 
                $('.loader').hide();
                console.log(response)
                if(response['status']=="Success"){
                  console.log('Data submited successfully');
                  swal('Data submited successfully');
                  $scope.cash_pay_color="text_color_success";
                  $rootScope.cash_pay_error = "Data submited successfully";
                }else if(response['status']=="Not_exist"){
                  console.log('Submited amount entry not exist');
                  swal('Submited amount entry not exist for this partner');
                  $scope.cash_pay_color="text_color_danger";
                  $rootScope.cash_pay_error = "Submited amount entry not exist";
                }else if(response['status']=="Error"){
                  swal('Error in data submission');
                  $scope.cash_pay_color="text_color_danger";
                  $rootScope.cash_pay_error = "Error in data submission";
                }
            },
            failure:function () {
              $('.loader').hide();
              swal('Error in data submission');
              console.log('Error in data submission');
              $scope.cash_pay_color="text_color_danger";
              $rootScope.cash_pay_error = "Error in data submission";
            }
          });

      }else{
        error_txt=''
        if(amount_flag==false){
          error_txt=cheque_price_err
        }
        if(partner_flag==false){
          if(error_txt!=''){
            error_txt=error_txt+', '+partner_name_err
          }else{
            error_txt=partner_name_err
          }
        }

        $scope.cash_pay_color="text_color_danger";
        $rootScope.cash_pay_error=error_txt
        
        
      }



    }






    

 
   


});