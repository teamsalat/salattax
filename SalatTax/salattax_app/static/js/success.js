
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('success_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {

	close_model = 0;
	client_id = 0;
	pan = '';
	p_age = '';
	i_year = '';
	$scope.total_salat_tax= "";
	$scope.div_2014 = true;
	$scope.div_2015 = true;
	$scope.div_2016 = true;
	$scope.div_2017 = true;
	$scope.div_2018 = true;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				client_id = response.client_id;
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					$.ajax({
						url:'/get_pan_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							// $scope.get_26as(pan,dob);
							$scope.show_ITR(response.pan);
						}
					});
				}
			}
		});
	},1000);

	$scope.show_ITR = function(pan){
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			$.ajax({
				url:'/download_ITR/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'option':'count'},
				success: function(response){
					console.log(response);
					$scope.count_itr = response.count_itr;
					$scope.$apply();
				},
				error: function(response){ }
			});
		}
		else{
			console.log('Wrong PAN');
		}
	}
	
}]);