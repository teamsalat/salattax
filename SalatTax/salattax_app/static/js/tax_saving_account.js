
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('tsa_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {
	close_model = 0;
	client_id = 0;
	mailid = 0;
	pan = '';
	p_age = '';
	i_year = '';
  	redirect_url = $scope.url;
  	$scope.logged_in = false;
  	$scope.logged_out = true;
  	$scope.do_it_yourself = false;
	var otp_sent = 0;
	$scope.otp_div = { 'display': 'none'};
	var valid_otp = 0;
	$scope.offer_table = true;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				if(response.client_id == 0 || response.client_id == 1){ }
				else{
					$scope.logged_in = true;
  					$scope.logged_out = false;
					client_id = response.client_id;
					mailid = response.email;
					$.ajax({
						url:'/get_pan_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							pan = response.pan;
							$scope.client_pan = response.pan;
							p_age = response.p_age;
							i_year = response.i_year;
							if(response.name != null){
								$scope.user.name = response.name.replace('|',' ');
								$scope.user.name = $scope.user.name.replace('|',' ');
							}
							$scope.user.mobile = response.mobile;
							$scope.$apply();
						}
					});
					$.ajax({
						url:'/get_user_tracking/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							if(parseInt(response.visit_tsa)>=1)
								$scope.offer_table = false;
							
							$scope.$apply();
						}
					});
				}
			}
		});
		$scope.employment_state ='';
	},1000);

	$scope.open_account = function(){
		var selected_plan = get_plan();
		if(selected_plan=='starter'){
			$scope.selected_tsa_plan = 'Do It Yourself';
			$scope.do_it_yourself = true;
			$scope.amount_payable = 1400;
			if(get_apply_offer() == 'yes')
				$scope.amount_payable = 980;
		}
		else{
			$scope.selected_tsa_plan = 'Expert Assisted';
			$scope.do_it_yourself = false;
			$scope.amount_payable = 2100;
			if(get_apply_offer() == 'yes')
				$scope.amount_payable = 1470;
		}
		// alert(selected_plan);
		$('#subscribe_modal').modal('show');
	}

	$scope.save_name_mobile = function(user,$event){
		var selected_plan = get_plan();
		var mobile = $scope.user.mobile ;
		var name = $scope.user.name ;
		var employment_state = $scope.employment_state ;

		var mobile_status = false;
		var name_status = false;
		var employment_state_status = false;
		var login_status = false;
		// alert($scope.employment_state);
		$scope.emp_state_err = '';
		$scope.emp_state = '';
		$scope.signup_username_err = '';
		$scope.signup_mail_err = '';
		$scope.password_err = '';
		if(client_id !=0 && client_id != 1)
			login_status = true;
		else if(valid_otp == 1){
			login_status = true;
			pan = $scope.user.pan;
			if(pan!==''){
				if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) )
					$scope.pan_err= "";
				else{
					$scope.pan_err= "Please Enter Valid PAN";
					login_status = true;
				}
			}else{
				$scope.pan_err= "PAN is required";
				login_status = true;
			}
		}else{
			$scope.signup_username_err = 'Enter Username';
			$scope.signup_mail_err = 'Enter E-mail ID';
			$scope.password_err = 'Enter Password';
		}

		if(mobile!==''){
			if( /\d{10}/.test(mobile) ){ 
				$scope.mobile_err= "";
				mobile_status = true;
			}else
				$scope.mobile_err= "Enter Valid Mobile No.";
		}else
			$scope.mobile_err= "Mobile No. is required";

		if(name.length>=2){
			$scope.name_err= "";
			name_status = true;
		}else
			$scope.name_err= "Name is required";

		if(employment_state==null || employment_state == ''){
			$scope.emp_state_err = 'Select State of Employment';
		}else if(employment_state=='Self Employed'){
			$scope.emp_state = 'Tax Savings Account for Self Employed and Professionals is coming soon.';
		}else
			employment_state_status = true;

		// alert(login_status);
		if(name_status==true && mobile_status==true && employment_state_status==true && login_status == true){
			var selected_plan = get_plan();
			var amount_pay = 0;
			if(selected_plan=='starter'){
				amount_pay = 1400;
				if(get_apply_offer() == 'yes')
					amount_pay = 980;
			}
			else{
				amount_pay = 2100;
				if(get_apply_offer() == 'yes')
					amount_pay = 1470;
			}
			$.ajax({
				url:'/update_subscription_info/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'name':name,'mobile':mobile,'pan':pan,'employment_state':employment_state,'mailid':mailid},
				success: function(response){
					console.log(response);
					if(response.status=='success'){
						$('#subscribe_modal').modal('hide');
						if(selected_plan == 'starter')
							window.location.href = redirect_url+'/subscribe_payumoney/'+client_id+'amt'+amount_pay;
						if(selected_plan == 'professional')
							window.location.href = redirect_url+'/subscribe_payumoney/'+client_id+'amt'+amount_pay;
					}
				}
			});
		}
	}

	$scope.emp_state_error = function(){
		$scope.emp_state_err = '';
	}

	$scope.reset_password = function(type,value,$event) {
		$scope.signup_username_err = "";
		$scope.signup_mail_err = "";
		$scope.signup_otp_err = "";
		$scope.signup_password = '';
	}

	$scope.check_otp = function(){
		$scope.signup_otp_err = '';
		var otp = $scope.user.signup_OTP;
		if(isNaN(otp) || otp.length != 6)
			$scope.signup_otp_err = 'Enter valid OTP';
		else
			$scope.gotoSignup();
	}

	$scope.gotoSignup = function() {
		var username = $scope.user.signup_username;
		var mail = $scope.user.signup_email;
		var password = $scope.user.signup_password;
		var OTP = $scope.user.signup_OTP;
		otp_status = false;

		if(OTP!==''){
			var length_otp = OTP.length;
			if( (/\d{6}/.test(OTP)) && length_otp == 6 ){ 
				$scope.signup_otp_err= "";
				otp_status = true;
			}else
				$scope.signup_otp_err= "Please Enter Valid OTP";
		}else
			$scope.signup_otp_err= "Enter OTP";

		if (otp_sent==1 && otp_status==true) {
			$.ajax({
				url:'/check_otp/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'username':username,'password':password,'OTP':OTP},
				success: function(response){
					console.log(response);
					if (response.verified_otp==0)
						$scope.signup_otp_err= "Please Enter Correct OTP";
					else{
						$.ajax({
							url:'/save_user/',
							type:'POST',
							dataType: 'json',
							data:{'mail':mail,'username':username,'password':password,'role':'Client'},
							success: function(response){
								console.log(response);
								$scope.signup_mail_err= "Registration Sucessfull.";
								if (response.user_exist==1) {
									$scope.signup_username_err= "Username already exist.";
									$scope.user.signup_email = "";
									$scope.user.signup_password = "";
									$scope.user.signup_OTP = "";
									$scope.otp_div = { 'display': 'none' };
									$scope.$apply();
								}
								if (response.user_exist==0 && response.status=='success') {
									$.ajax({
										url:'/update_password/',
										type:'POST',
										dataType: 'json',
										data:{'username':username,'password':response.raw_password},
										success: function(response){
											console.log(response);
										}
									});
									$.ajax({
										url:'/get_client/',
										type:'POST',
										dataType: 'json',
										data:{'mail':'mail'},
										success: function(response){
											console.log(response);
											client_id = response.client_id;
										}
									});
									$scope.user.username = "";
									$scope.user.signup_email = "";
									$scope.user.signup_password = "";
									$scope.user.signup_OTP = "";
									var ele1 = angular.element( document.querySelector( '#signup_model' ));
									$scope.otp_submit = true;
									valid_otp = 1;
									$scope.$apply();
								}
							}
						});
					}
				}
			});
		}
	}

	$scope.send_otp = function(type,value,$event) {
		var mail = $scope.user.signup_email;
		var username = $scope.user.signup_username;
		var mail_status = false;
		var username_status = false;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if(mail!==''){
			if( re.test(mail) ){ 
				$scope.signup_mail_err= "";
				mail_status = true;
			}else
				$scope.signup_mail_err= "Please Enter Valid Email ID";
		}else 
			$scope.signup_mail_err= "Email is required";
		if ($scope.signup_mail_err== "") {
			if(username!==''){
				var length = username.length;
				if( length>3 ){ 
					$scope.signup_username_err= "";
					username_status = true;
				}else
					$scope.signup_username_err= "Please Enter Valid Username";
			}else
				$scope.signup_username_err= "Username is required";
		}
		// alert(mail_status);
		// alert(username_status);
		// alert(otp_sent);
		if (mail_status == true && username_status == true && otp_sent==0) {
			$.ajax({
				url:'/check_username/',
				type:'POST',
				dataType: 'json',
				data:{'username':username,'password':mail},
				success: function(response){
					// console.log(response);
					if (response.user_exist == 0) {
						$.ajax({
							url:'/check_email/',
							type:'POST',
							dataType: 'json',
							data:{'mail':mail,'username':username},
							success: function(response){
								console.log(response);
								if (response.mailid_exist == 0) {
									$scope.otp_div = { 'display': '' };
									otp_sent = 1;
									$.ajax({
										url:'/send_otp/',
										type:'POST',
										dataType: 'json',
										data:{'mail':mail,'username':username},
										success: function(response){
											console.log(response);
											if (response.status == 'success')
												otp_sent = 1;
										}
									});
								}
								else{
									$scope.signup_mail_err = "Email ID already exist.";
									$scope.otp_div = { 'display': 'none' };
									$scope.$apply();
								}
							}
						});
					}
					else{
						$scope.signup_username_err= "Username already exist.";
						$scope.otp_div = { 'display': 'none' };
						$scope.$apply();
					}
				}
			});
		}
	}

}]);