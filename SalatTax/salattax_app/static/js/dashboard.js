
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('dashboard_controller', ['$scope','$compile', function($scope,$compile,CONFIG) {
	
	console.log('dashboard_controller')

	redirect_url = $scope.url;
	var client_id = 1;
	var client_id_user = 1;
	var business_name = 1;
	var otp_sent = 0;
	$scope.display_non_business = {"display" : "none"};
	
	$scope.$watch("demoInput", function(){
		$scope.dashboard_details = [{}];
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				business_name = response.username;
				if (response.client_id!=0) {
					client_id = response.client_id;
					$scope.display_logout_d = {"display" : "block"};
					$scope.display_login_d = {"display" : "none"};
				}
				$.ajax({
					url:'/check_business/',
					type:'POST',
					dataType: 'json',
					data:{'client_id':client_id},
					success: function(response){
						console.log(response);
						if (response.is_business != 'admin') {
							$scope.display_logout_d = {"display" : "none"};
							$scope.display_login_d = {"display" : "none"};
							$scope.display_non_business = {"display" : "block"};
						}else{
							get_business_client(client_id);
							/*$.ajax({
								url:'/get_business_client/',
								type:'POST',
								dataType: 'json',
								data:{'client_id':client_id},
								success: function(response){
									$scope.business_name = business_name;
									console.log(response);
									var table = angular.element(document.querySelector('#client_tbl'));
									for (var i = 0; i < response.client_count; i++) {
										if (response.form16[i] == 'Processed' || response.form16[i] == 'Manual')
											var href = 'https://salattax.com/personal_information/'+response.CID[i];
										else
											var href = 'https://salattax.com/upload_form/'+response.CID[i];

										var PAN = "<a href="+href+" target='_blank'>"+response.PAN[i]+"</a>";
										

										markup = "<tr><td>"+response.name[i]+"</td><td>"+PAN+"</td>";
										markup += "<td>"+response.form16[i]+"</td><td>"+response.client_reg[i]+"</td>";
										markup += "<td>"+response.AS26[i]+"</td><td>"+response.taxpayble[i]+"</td>";
										markup += "<td>"+response.payment[i]+"</td><td>"+response.xml[i]+"</td>";
										markup += "<td>"+response.itr_verify[i]+"</td></tr>";
										var temp = $compile(markup)($scope);
										table.append( temp );
									}
									$scope.$apply();
								}
							});*/
						}
						$scope.$apply();
					}
				});
				$scope.$apply();
			}
		});
	},100);

	$scope.gotoSignup = function(type,value,$event) {
			var username = $scope.user.signup_username;
		// alert(username);
		var mail = $scope.user.signup_email;
		var mail_status = false;
		if(mail!==''){
			if( /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail) ){ 
				$scope.signup_mail_err= "";
				mail_status = true;
			}else
				$scope.signup_mail_err= "Please Enter Valid Emaid ID";
		}else 
			$scope.signup_mail_err= "Email is required";

		if (username != '' && mail != '' && mail_status == true) {
			$.ajax({
				url:'/save_new_client/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'username':username,'role':'Client','b_id':client_id,'partner_id':1},
				success: function(response){
					console.log(response);
					if (response.user_exist==1)
						$scope.signup_mail_err= "Username already exist please retry.";
					
					if (response.user_exist==0 && response.status=='success') {
						$.ajax({
							url:'/update_password/',
							type:'POST',
							dataType: 'json',
							data:{'username':username,'password':response.raw_password},
							success: function(response){ }
						});
						alert('User created.');
						$scope.user.signup_username = "";
						$scope.user.signup_email = "";
						// $('#signup_model').modal('hide');
						client_id_user = response.client_id ;
						setTimeout(function(){
						$('#start_modal').modal('show');
						}, 500);
						/*$.ajax({
							url:'/get_client/',
							type:'POST',
							dataType: 'json',
							data:{'mail':'mail'},
							success: function(response){
								console.log(response);
								client_id = response.client_id;
								if (client_id!=1) {
									setTimeout(function(){
									$('#start_modal').modal('show');
									// window.location.href = redirect_url+'/upload_form/'+client_id;
									}, 500);
								}
							}
						});*/
					}
					$scope.$apply();
				}
			});
		}else if (username == '')
			$scope.signup_mail_err= "Enter Username";
		else if (mail == '')
			$scope.signup_mail_err= "Enter EmailId";
	}

	$scope.goUpload_form16 = function() {
		if(client_id_user==1 || client_id_user==0)
			alert('Kindly Login to continue.');
		else
			window.location.href = redirect_url+'/upload_form/'+client_id_user;
	};
	$scope.gotoManual_entry = function() {
		if(client_id_user==1 || client_id_user==0)
			alert('Kindly Login to continue.');
		else
			window.location.href = redirect_url+'/personal_information/'+client_id_user;
	};

}]);

