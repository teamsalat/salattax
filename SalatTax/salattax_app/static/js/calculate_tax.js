
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('calculate_tax_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {

	close_model = 0;
	var client_id = 0;
	var pan = '';

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				if(response.client_id == 0 || response.client_id == 1){
					alert('Please login to continue.');
					window.location.href = "/";
				}
				else{
					client_id = response.client_id;
					$.ajax({
						url:'/get_pan_info/',
						type:'POST',
						dataType: 'json',
						data:{'client_id':client_id},
						success: function(response){
							console.log(response);
							pan = response.pan;
							$scope.show_ITR(pan);
							if(response.p_age != 'NA' && response.p_age != '' && response.p_age != null){
								parent_age(parseInt(response.p_age));
							}
							$scope.user.p_age = parseInt(response.p_age);
							if(response.i_year != 'NA' && response.i_year != ''){
								invest_share(parseInt(response.i_year));
							}
							$scope.user.shares_year = response.i_year;
							var no_children = parseInt(response.no_children);
							$scope.no_children = response.no_children;
							married_fun(no_children);
							select_num(response.no_children);
							for (var i = 0; i < no_children; i++) {
								if(response.child_no[i] == '1'){
									$scope.type_c1 = response.child_type[0].toString();
									if(response.child_age[0] != null)
										$scope.age_c1 = response.child_age[0].toString();
								}
								if(response.child_no[i] == '2'){
									$scope.type_c2 = response.child_type[1].toString();
									if(response.child_age[1] != null)
										$scope.age_c2 = response.child_age[1].toString();
								}
								if(response.child_no[i] == '3'){
									$scope.type_c3 = response.child_type[2].toString();
									if(response.child_age[2] != null)
										$scope.age_c3 = response.child_age[2].toString();
								}
								if(response.child_no[i] == '4'){
									$scope.type_c4 = response.child_type[3].toString();
									if(response.child_age[3] != null)
										$scope.age_c4 = response.child_age[3].toString();
								}
							}
							$scope.$apply();
						}
					});
				}
			}
		});
		$scope.type_c1 = '';
		$scope.type_c2 = '';
		$scope.type_c3 = '';
		$scope.type_c4 = '';
	},1000);

	$scope.validation = function(type,value){
		$scope.c1_type_err = '';
		$scope.c1_age_err = '';
		$scope.c2_type_err = '';
		$scope.c2_age_err = '';
		$scope.c3_type_err = '';
		$scope.c3_age_err = '';
		$scope.c4_type_err = '';
		$scope.c4_age_err = '';
		if(type == 'no_children'){
			// alert('no_children');
			status_age = true;
			if (value == null)
				return false;
			if(value == '1' || value == '2' || value == '3' || value == '4'){
				if($scope.type_c1 == null || $scope.type_c1 == ''){
					$scope.c1_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c1 == '' || isNaN($scope.age_c1) ){
					status_age = false;
					$scope.c1_age_err = 'Enter valid Age.';
				}
			}
			if(value == '2' || value == '3' || value == '4'){
				if($scope.type_c2 == null || $scope.type_c2 == ''){
					$scope.c2_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c2 == '' || isNaN($scope.age_c2) ){
					status_age = false;
					$scope.c2_age_err = 'Enter valid Age.';
				}
			}
			if(value == '3' || value == '4'){
				if($scope.type_c3 == null || $scope.type_c3 == ''){
					$scope.c3_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c3 == '' || isNaN($scope.age_c3) ){
					status_age = false;
					$scope.c3_age_err = 'Enter valid Age.';
				}
			}
			if(value == '4'){
				if($scope.type_c4 == null || $scope.type_c4 == ''){
					$scope.c4_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c4 == '' || isNaN($scope.age_c4) ){
					status_age = false;
					$scope.c4_age_err = 'Enter valid Age.';
				}
			}
			return status_age;
		}
		if(type == 'type_c1'){
			$scope.c1_type_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if(value == null || value == '')
					$scope.c1_type_err = 'Please Select.';
			}
		}
		if(type == 'type_c2'){
			$scope.c2_type_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if(value == null || value == '')
					$scope.c2_type_err = 'Please Select.';
			}
		}
		if(type == 'type_c3'){
			$scope.c3_type_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if(value == null || value == '')
					$scope.c3_type_err = 'Please Select.';
			}
		}
		if(type == 'type_c4'){
			$scope.c4_type_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if(value == null || value == '')
					$scope.c4_type_err = 'Please Select.';
			}
		}
		if(type == 'age_c1'){
			$scope.c1_type_err = '';
			$scope.c1_age_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if($scope.type_c1 == null || $scope.type_c1 == ''){
					$scope.c1_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c1 == '' || isNaN($scope.age_c1) ){
					status_age = false;
					$scope.c1_age_err = 'Enter valid Age.';
				}
			}
		}
		if(type == 'age_c2'){
			$scope.c2_type_err = '';
			$scope.c2_age_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if($scope.type_c2 == null || $scope.type_c2 == ''){
					$scope.c2_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c2 == '' || isNaN($scope.age_c2) ){
					status_age = false;
					$scope.c2_age_err = 'Enter valid Age.';
				}
			}
		}
		if(type == 'age_c3'){
			$scope.c3_type_err = '';
			$scope.c3_age_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if($scope.type_c3 == null || $scope.type_c3 == ''){
					$scope.c3_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c3 == '' || isNaN($scope.age_c3) ){
					status_age = false;
					$scope.c3_age_err = 'Enter valid Age.';
				}
			}
		}
		if(type == 'age_c4'){
			$scope.c4_type_err = '';
			$scope.c4_age_err = '';
			if($scope.no_children != undefined && $scope.no_children != null && $scope.no_children != ''){
				if($scope.type_c4 == null || $scope.type_c4 == ''){
					$scope.c4_type_err = 'Please Select';
					status_age = false;
				}
				if($scope.age_c4 == '' || isNaN($scope.age_c4) ){
					status_age = false;
					$scope.c4_age_err = 'Enter valid Age.';
				}
			}
		}
	}

	$scope.show_ITR = function(pan){
		// cleartimer();
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			close_model = 1;
			// timer();
			/*$.ajax({
				url:'/download_ITR/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'option':'show'},
				success: function(response){
					close_model = 1;
					stoptimer();
					console.log(response);
				},
				error: function(response){
					close_model = 1;
					stoptimer();
				}
			});*/
		}
		else
			close_model = 1;
	}

	$scope.submit = function(){
		$scope.err_common = '';
		var pan_status = false;
		var p_age = $scope.user.p_age;
		var p_age_status = false;
		var shares_year = $scope.user.shares_year;
		var shares_status = false;
		var child_status = false;
		var married = married_check();
		var no_children = $scope.no_children;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		var dep_parent = dep_parent_check();
		var sold_shares = sold_shares_check();
		
		if((sold_shares=='yes' && shares_year!="") || sold_shares=='no'){
			$scope.shares_err= "";
			shares_status = true;
		}
		else
			$scope.shares_err= "Year is required";

		if(sold_shares=='no')
			shares_year = '';

		if(married == 'no'){
			child_status = true;
			no_children = '0';
		}
		else{
			if(no_children == null)
				child_status = false;
			if (no_children == '0')
				child_status = true;
			else
				child_status = $scope.validation('no_children',no_children);
		}

		if(dep_parent=='yes'){
			if(p_age==null || p_age=="undefined" || p_age=='')
				$scope.age_err= "Parent's Age is required";
			else if(isNaN(p_age) || p_age<40)
				$scope.age_err= "Enter valid age.";
			else{
				$scope.age_err= "";
				p_age_status = true;
			}
		}else{
			p_age = '';
			$scope.age_err= "";
			p_age_status = true;
		}

		var pass_obj = { "no_children":no_children };
		pass_obj["type_c1"] = $scope.type_c1;
		pass_obj["type_c2"] = $scope.type_c2;
		pass_obj["type_c3"] = $scope.type_c3;
		pass_obj["type_c4"] = $scope.type_c4;
		pass_obj["age_c1"] = $scope.age_c1;
		pass_obj["age_c2"] = $scope.age_c2;
		pass_obj["age_c3"] = $scope.age_c3;
		pass_obj["age_c4"] = $scope.age_c4;
		var data_age = JSON.stringify(pass_obj);
		// $scope.$apply();
		console.log($scope.shares_err);
		if(p_age_status==true && shares_status == true && child_status == true){
			$('#wait_modal').modal('show');
			$.ajax({
				url:'/update_pan_info/',
				type:'POST',
				dataType: 'json',
				data:{'client_id':client_id,'pan':pan,'p_age':p_age,'shares_year':shares_year,'data_age':data_age},
				success: function(response){
					console.log(response);
				}
			});

			if(close_model == 1){
				setTimeout(function(){
					$('#wait_modal').modal('hide');
				}, 17500);
			} else{
				set_submit = setInterval(check_submit, 1000); //every sec
				function check_submit() {
					if(close_model == 1){
						clearInterval(set_submit);
						$('#wait_modal').modal('hide');
					}
				}
			}
		}else{
			$scope.err_common = 'Please Fill All Mandatory Field.';
			// $scope.$apply();
		}

	}

}]);