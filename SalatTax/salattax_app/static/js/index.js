
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});


/*date / add */
// app.directive('myDate', dateInput);
// function dateInput($filter, $parse){
// 	return {
//       restrict: 'A',
//       require: 'ngModel',
//       replace: true,
//       transclude: true,
//       template: '<input ng-transclude ui-mask="39/19/2999" ui-mask-raw="false" placeholder="DD/MM/YYYY"/>',
//       link: function(scope, element, attrs, controller) {
//         scope.limitToValidDate = limitToValidDate;
//         var dateFilter = $filter("date");
//         var today = new Date();
//         var date = {};

//         function isValidDay(day) {
//           return day > 0 && day < 32;
//         }

//         function isValidMonth(month) {
//           return month >= 0 && month < 12;
//         }

//         function isValidYear(year) {
//           return year > (today.getFullYear() - 115) && year < (today.getFullYear() + 115);
//         }

//         function isValidDate(inputDate) {
//           inputDate = new Date(formatDate(inputDate));
//           if (!angular.isDate(inputDate)) {
//             return false;
//           }
//           date.day = inputDate.getDate();
//           date.month = inputDate.getMonth();
//           date.year = inputDate.getFullYear();
//           return (isValidDay(date.day)  &&  isValidMonth(date.month) && isValidYear(date.year));
//         }

//         function formatDate(newDate) {
//           var modelDate = $parse(attrs.ngModel);
//           newDate = dateFilter(newDate, "dd/mm/yyyy");
//           // console.log(newDate)
//           modelDate.assign(scope, newDate);
//           return newDate;
//         }

//         controller.$validators.date = function(modelValue) {
//           return angular.isDefined(modelValue) && isValidDate(modelValue);
//         };

//         var pattern = "^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)\\d\\d$" +
//           "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)\\d$" +
//           "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(19|20)$" +
//           "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[12]$" +
//           "|^(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])$" +
//           "|^(0[1-9]|1[012])([0-3])$" +
//           "|^(0[1-9]|1[012])$" +
//           "|^[01]$";
//         var regexp = new RegExp(pattern);

//         function limitToValidDate(event) {
//           var key = event.charCode ? event.charCode : event.keyCode;
//           if ((key >= 48 && key <= 57) || key === 9 || key === 46) {
//             var character = String.fromCharCode(event.which);
//             var start = element[0].selectionStart;
//             var end = element[0].selectionEnd;
//             var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\//g, "");
//             if (!(regexp.test(testValue))) {
//               event.preventDefault();
//             }
//           }
//         }
//       }
//     }
// };

app.controller('index_controller', ['$scope','$rootScope','$location',function($scope,$rootScope,$location,$http,CONFIG,$window) {

	redirect_url = $scope.url;
	var client_id = 1;
	var otp_sent = 0;
	$scope.Start_Now = 'Start Now';
	$scope.Start_btn = 'Check your Tax Leakage for Free';
	$scope.otp_div = { 'display': 'none'};
	var button_click = 0;
	$scope.subscribed = {"display" : "none"};
	$scope.notsubscribed = {"display" : ""};
	var db_pan = '';
	var current_url = '';
	role=''
	
	$scope.$watch("demoInput", function(){


		UserId = $scope.UserId;
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(JSON.stringify(response));
				$scope.username=response.username
				if (response.client_id!=0) {

					// if(response.role=='Client')
					client_id=response.client_id
					role=response.role
					// else
					// 	var client_id=$rootScope.client_id

					// console.log(client_id)

					if(response.role!=undefined){
			          if(response.role=="Admin" || response.role=="Partner"){
			            $('.chooseFillingType').hide();
			          }
			        }
					
					$scope.display_logout = {"display" : ""};
					$scope.display_login = {"display" : "none"};
					$scope.Start_Now = 'File Your Return Now';
					$scope.Start_btn = 'Check your Tax Leakage for Free';

					if(response.role=='Client'){

						$.ajax({
								url:'/get_payment_info/',
								type:'POST',
								dataType: 'json',
								data:{'client_id':client_id},
								success: function(response){

									$scope.user.username=response.username
									$scope.$apply()
									// console.log(response);
									$.ajax({
										url:'/get_pan_info/',
										type:'POST',
										dataType: 'json',
										data:{'client_id':client_id},
										success: function(response){
											console.log(JSON.stringify(response));
											issalat=response.salatclient
											db_pan = response.pan;

											p_age=response.p_age

											i_year=response.i_year

											if(response.name!=null){
												login_name=response.name.split('|')
												$scope.user.username=$scope.toTitleCase(login_name.join(' '))
											}
											current_url = response.url;

											if(response.url.indexOf('/salat_reg/') !== -1)
												$scope.redirect_leakage();

											$scope.$apply();
										}
									});
									if(response.paid == 1){
										$scope.subscribed = {"display" : ""};
										$scope.notsubscribed = {"display" : "none"};
									}else{
										$scope.subscribed = {"display" : "none"};
										$scope.notsubscribed = {"display" : ""};
									}
									$scope.$apply();
								}
							});

					}

					
				}else{
					$scope.subscribed = {"display" : "none"};
					$scope.notsubscribed = {"display" : ""};
				}


				if(role=='Client'){

					$.ajax({
						url:'/user_tracking_func/',
						type:'POST',
						dataType: 'json',
						data:{'event':'viewed','event_name':redirect_url,'client_id':client_id,'UserId':UserId},
						success: function(response){
							console.log(response);
						}
					});

				}
				
				$scope.$apply();
			}
		});
	},900);

	$scope.toTitleCase=function(str) {
	        return str.replace(
	            /\w\S*/g,
	            function(txt) {
	                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	            }
	        );
	}

	$scope.goUpload_form16 = function(fy) {
		//location.href='upload_form16.html'https://salattax.com/
		if(client_id==1 || client_id==0){
			alert('Kindly Login to continue.');
		}else{
			$scope.update_fy('upload_form',fy);
			// window.location.href = '/upload_form/'+client_id;
		}
	};
	$scope.gotoManual_entry = function(fy) {
		if(client_id==1 || client_id==0){
			alert('Kindly Login to continue.');
		}else{
			$scope.update_fy('chooseFillingType',fy);
			// window.location.href = '/personal_information/'+client_id;
		}
	};

	$scope.reset_password = function(type,value,$event) {
		$scope.signup_username_err = "";
		$scope.signup_mail_err = "";
		$scope.signup_otp_err = "";
		$scope.user.signup_password = '';
		// $scope.$apply();
	}

	$scope.gotoSignin = function($event) {
		type='business'
		var username = $scope.user.signin_username;
		var password = $scope.user.signin_password;
		$scope.signin_password_err = '';
		$scope.signin_username_err = '';
		if (username!='' && password!='') {
			$.ajax({
				url:'/check_username/',
				type:'POST',
				dataType: 'json',
				data:{'username':username,'password':password},
				success: function(response){
					console.log(response);
					if (response.user_exist==0) {
						$scope.signin_username_err = 'Username does not exist. Please check Username';
					}else{
						$scope.signin_username_err = '';
						//type=client or business
						type=response.user_type
						if (response.password_exist == 1) {
							$scope.signin_password_err = '';
							$.ajax({
								url:'/get_client/',
								type:'POST',
								dataType: 'json',
								data:{'mail':'mail'},
								success: function(response){
									console.log(response);
									client_id = response.client_id;
									$rootScope.role=response.role
									// alert(client_id);
									$('#signin_model').modal('hide');
									$.ajax({
										url:'/check_business/',
										type:'POST',
										dataType: 'json',
										data:{'client_id':client_id},
										success: function(response){
											console.log(response);
											//alert(response.is_business);
											partnerId=response.partnerId;
											if ((response.is_business == 'Admin' || response.is_business == 'Partner') && type == 'business' && (partnerId!=0 && partnerId!='null')) {
												//alert(response.is_business);

												if (response.is_business == 'Admin') {
													var $popup = window.location.href = '/admin_dashboard';
												}else if (response.is_business == 'Partner') {
													var $popup = window.location.href = '/dashboard';
												}
												//$popup.partnerId = partnerId;

											}
											else if (response.is_business != 'Admin' && type == 'business') {
												$scope.signin_password_err = 'This Business Username does not exist.';
												$scope.$apply();
												setTimeout(function(){
													$.ajax({
														url:'/logout_user/',
														type:'POST',
														dataType: 'json',
														data:{'data':'data'},
														success: function(response){
														console.log(response);
															if (response.logout.status == 'success') {
																window.location.href = '/';
															}
														}
													});
												}, 3500);
											}
											else if (response.is_business != 'Admin' && type == 'client') {
												if (client_id!=1) {
													if(button_click == 1)
														window.location.href = '/salat_reg';
													else
														window.location.href = '/';
												}
											}
											else if (response.is_business == 'Admin' && type == 'client') {
												$scope.signin_password_err = 'This Client Username does not exist.';
												$scope.$apply();
												alert('client does not exist');
												setTimeout(function(){
													$.ajax({
														url:'/logout_user/',
														type:'POST',
														dataType: 'json',
														data:{'data':'data'},
														success: function(response){
														console.log(response);
															if (response.logout.status == 'success') {
																window.location.href = '/';
															}
														}
													});
												}, 4000);
											}
										}
									});
								}
							});
						}else{
							$scope.signin_password_err = 'Incorrect Password';
						}
					}
					$scope.$apply();
				}
			});
		}
	}

	$scope.gotoSignup = function(type,value,$event) {
		var username = $scope.user.signup_username;
		var mail = $scope.user.signup_email;
		var password = $scope.user.signup_password;
		var OTP = $scope.user.signup_OTP;

		var code=''
		if($location.absUrl().includes('code')){
			url_split=$location.absUrl().split('=')
			code=url_split[1]
		}

		console.log(code)

		otp_status = false;
		var pan_status = false;
	

		if(username!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(username) ){
				$scope.signup_mail_err= "";
				pan_status = true;
			}else
				$scope.signup_mail_err= "Please Enter Valid PAN";
		}else
			$scope.signup_mail_err= "PAN is required";

		if(OTP!==''){
			var length_otp = OTP.length;
			if( (/\d{6}/.test(OTP)) && length_otp == 6 ){ 
				$scope.signup_otp_err= "";
				otp_status = true;
			}else{
				$scope.signup_otp_err= "Please Enter Valid OTP";
			}
		}else {
			$scope.signup_otp_err= "Enter OTP";
		}

		if (otp_sent==1 && otp_status==true && pan_status==true) {
			$.ajax({
				url:'/check_otp/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'username':username,'password':password,'OTP':OTP},
				success: function(response){
					console.log(response);
					if (response.verified_otp==0) {
						$scope.signup_otp_err= "Please Enter Correct OTP";
					}
					else{
						$.ajax({
							url:'/save_user/',
							type:'POST',
							dataType: 'json',
							data:{'mail':mail,'username':username,'password':password,'role':'Client','code':code},
							success: function(response){
								console.log(response);
								/*$.ajax({
									url:'/change_password/',
									type:'POST',
									dataType: 'json',
									data:{'mail':mail,'password':password},
									success: function(response){
										console.log(response);
									}
								});*/
								$scope.signup_mail_err= "Registration Sucessfull.";
								// $('#signup_model').modal('hide');
								if (response.user_exist==1) {
									$scope.signup_mail_err= "Username already exist please retry.";
									$scope.user.signup_email = "";
									$scope.user.signup_password = "";
									$scope.user.signup_OTP = "";
									$scope.otp_div = { 'display': 'none' };
									$scope.$apply();
								}
								if (response.user_exist==0 && response.status=='success') {
									// alert('User created.');
									$.ajax({
										url:'/update_password/',
										type:'POST',
										dataType: 'json',
										data:{'username':username,'password':response.raw_password},
										success: function(response){
											console.log(response);
											// console.log(button_click);
											// if(button_click == 1)
											window.location.href = "/salat_reg";
										}
									});
									$scope.user.username = "";
									$scope.user.signup_email = "";
									$scope.user.signup_password = "";
									$scope.user.signup_OTP = "";
									var ele1 = angular.element( document.querySelector( '#signup_model' ));
									$('#signup_model').modal('hide');
									$.ajax({
										url:'/get_client/',
										type:'POST',
										dataType: 'json',
										data:{'mail':'mail'},
										success: function(response){
											console.log(response);
											client_id = response.client_id;
											if (client_id!=1) {
												$scope.Start_Now = 'File Your Return Now';
												$scope.$apply();
											}
										}
									});
									// window.location.href = "/";
								}
								ga('send','event','Sign Up','Completed','Label',1)
								$scope.user_tracking('Sign Up','Completed');
								if (client_id!=1) {
									if(button_click == 1)
										window.location.href = '/salat_reg';
									else
										window.location.href = '/';
								}
							}
						});
					}
				}
			});
		}
	}
	$scope.b_gotoSignup = function(type,value,$event) {
		var username = $scope.user.signup_username;
		var mail = $scope.user.signup_email;
		var password = $scope.user.signup_password;
		var OTP = $scope.user.signup_OTP;
		otp_status = false;

		if(OTP !==''){
			var length_otp = OTP.length;
			if( (/\d{6}/.test(OTP)) && length_otp == 6 ){ 
				$scope.signup_otp_err= "";
				otp_status = true;
			}else{
				$scope.signup_otp_err= "Please Enter Valid OTP";
			}
		}else {
			$scope.signup_otp_err= "Enter OTP";
		}

		if (otp_sent==1 && otp_status==true) {
			$.ajax({
				url:'/check_otp/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'username':username,'password':password,'OTP':OTP},
				success: function(response){
					console.log(response);
					if (response.verified_otp==0) {
						$scope.signup_otp_err= "Please Enter Correct OTP";
					}
					else{
						$.ajax({
							url:'/save_user/',
							type:'POST',
							dataType: 'json',
							data:{'mail':mail,'username':username,'password':password,'role':'Admin'},
							success: function(response){
								console.log(response);
								/*$.ajax({
									url:'/change_password/',
									type:'POST',
									dataType: 'json',
									data:{'mail':mail,'password':password},
									success: function(response){
										console.log(response);
									}
								});*/
								$('#signup_model').modal('hide');
								if (response.user_exist==1) {
									$scope.signup_mail_err= "Username already exist please retry.";
									$scope.user.signup_email = "";
									$scope.user.signup_password = "";
									$scope.user.signup_OTP = "";
									$scope.otp_div = { 'display': 'none' };
									$scope.$apply();
								}
								if (response.user_exist==0 && response.status=='success') {
									// alert('User created.');
									$.ajax({
										url:'/update_password/',
										type:'POST',
										dataType: 'json',
										data:{'username':username,'password':response.raw_password},
										success: function(response){
											console.log(response);
										}
									});
									$scope.user.username = "";
									$scope.user.signup_email = "";
									$scope.user.signup_password = "";
									$scope.user.signup_OTP = "";
									var ele1 = angular.element( document.querySelector( '#signup_model' ));
									$('#signup_model').modal('hide');
									$.ajax({
										url:'/get_client/',
										type:'POST',
										dataType: 'json',
										data:{'mail':'mail'},
										success: function(response){
											console.log(response);
											client_id = response.client_id;
											if (client_id!=1) {
												$scope.Start_Now = 'File Your Return Now';
												$scope.$apply();
											}
										}
									});
								}
							}
						});
					}
				}
			});
		}
	}

	$scope.send_otp = function(type,value,$event) {
		var mail = $scope.user.signup_email;
		var username = $scope.user.signup_username;
		var mail_status = false;
		var username_status = false;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if(mail!==''){
			if( re.test(mail) ){ 
				$scope.signup_mail_err= "";
				mail_status = true;
			}else{
				$scope.user.signup_password = '';
				$scope.signup_mail_err= "Please Enter Valid Email ID";
			}
		}else {
			$scope.signup_mail_err= "Email is required";
		}
		if ($scope.signup_mail_err== "") {
			if(username!==''){
				if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(username) ){
					$scope.signup_username_err= "";
					username_status = true;
				}else
					$scope.signup_username_err= "Please Enter Valid PAN";
			}else
				$scope.signup_username_err= "PAN is required";

		}


		// alert(mail_status);
		// alert(username_status);
		// alert(otp_sent);
		if (mail_status == true && username_status == true && otp_sent==0) {
			$.ajax({
				url:'/check_username/',
				type:'POST',
				dataType: 'json',
				data:{'username':username,'password':mail},
				success: function(response){
					// console.log(response);
					if (response.user_exist == 0) {
						$.ajax({
							url:'/check_email/',
							type:'POST',
							dataType: 'json',
							data:{'mail':mail,'username':username},
							success: function(response){
								console.log(response);
								if (response.mailid_exist == 0) {
									$scope.otp_div = { 'display': 'block' };
									otp_sent = 1;
									$.ajax({
										url:'/send_otp/',
										type:'POST',
										dataType: 'json',
										data:{'mail':mail,'username':username},
										success: function(response){
											console.log(response);
											if (response.status == 'success') {
												otp_sent = 1;
											}
										}
									});
								}
								else{
									$scope.signup_mail_err = "Email ID already exist please retry.";
									$scope.otp_div = { 'display': 'none' };
									$scope.$apply();
								}
							}
						});
					}
					else{
						$scope.signup_mail_err= "Username already exist please retry.";
						$scope.otp_div = { 'display': 'none' };
						$scope.$apply();
					}
				}
			});
		}
	}

	$scope.get_client = function(type,value,$event) {
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				//alert(response);
				client_id = response.client_id;
				if (client_id!=1) {
					$scope.Start_Now = 'File Your Return Now';
					$scope.$apply();
				}
			}
		});
	}

	$scope.verification_mail = function(type,value,$event) {
		var mail = $scope.user.fp_mail;
		var mail_status = false;
		// var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		// if(mail!==''){
		// 	if( re.test(mail) ){ 
		// 		$scope.signin_fp_mail_err= "";
		// 		mail_status = true;
		// 	}else
		// 		$scope.signin_fp_mail_err= "Please Enter Valid Emaid ID";
		// }else
		// 	$scope.signin_fp_mail_err= "Email is required";

		if(mail!=='')
			mail_status = true;
		else
			$scope.signin_fp_mail_err= "Username is required";

		if (mail_status == true) {
			$.ajax({
				url:'/send_verification_mail/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,id:"password"},
				success: function(response){
					console.log(response);
					if(response.status == 'username does not exist')
						$scope.signin_fp_mail_err= "Username does not exist";
					
					if(response.status == 'success'){
						$scope.signin_fp_mail_err= "Please Check Your Email.";
						setTimeout(function(){
							$('#forgot_password').modal('hide');
							$scope.signin_fp_mail_err= "";
							$scope.user.fp_mail = "";
						}, 3500);
					}
					$scope.$apply();
				}
			});
		}
	}

	$scope.verification_mail_username = function(type,value,$event) {
		var mail = $scope.user.fu_mail;
		var mail_status = false;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(mail!==''){
			if( re.test(mail) ){ 
				$scope.signin_fu_mail_err= "";
				mail_status = true;
			}else
				$scope.signin_fp_mail_err= "Please Enter Valid Emaid ID";
		}else
			$scope.signin_fp_mail_err= "Email is required";

		if (mail_status == true) {
			$.ajax({
				url:'/send_verification_mail/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,id:"username"},
				success: function(response){
					console.log(response);
					if(response.status == 'success'){
						$scope.signin_fp_mail_err= "Please Check Your Email.";
						setTimeout(function(){
							$('#forgot_username').modal('hide');
							$scope.signin_fp_mail_err= "";
							$scope.user.fu_mail = "";
						}, 3500);
					}
				}
			});
		}
	}

	// function call from confirm_password
	// use flag(status) in ResetPassword table & reset it
	$scope.New_Password = function(new_data,$event){
		var mail = new_data.salatuser_id;
		console.log(mail);
		var reset_password = $scope.new_data.reset_password;
		var confirm_password = $scope.new_data.confirm_password;

		if(reset_password != confirm_password){
			$scope.new_data.confirm_password = '';
			$scope.password_err = 'Re-enter Confirm Password';
		}
		if(confirm_password=='')
			$scope.password_err = 'Confirm Password is required.';
		if(reset_password=='')
			$scope.password_err = 'New Password is required.';

		if(reset_password == confirm_password && reset_password!=''){
			$.ajax({
				url:'/change_password/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'password':reset_password},
				success: function(response){
					console.log(response);
					if(response.status == 'success'){
						$scope.password_err = '';
						setTimeout(function(){
							window.location.href = '/';
						}, 3500);
					}
				}
			});
		}
	}
	$scope.New_Username = function(new_data,$event){
		var mail = new_data.salatuser_id;
		var new_username = $scope.new_data.new_username;

		if(new_username=='')
			$scope.username_err = 'New Username is required.';
		if(mail != '' && new_username!=''){
			$.ajax({
				url:'/change_username/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'username':new_username},
				success: function(response){
					console.log(response);
					if(response.status == 'success'){
						$scope.password_err = '';
						setTimeout(function(){
							window.location.href = '/';
						}, 3500);
					}
				}
			});
		}
	}

	$scope.goto_salat_reg = function(){
		// alert(client_id);
		button_click = 1;
		if(client_id!=0 && client_id!=1)
			window.location.href = '/salat_reg';
		else
			$('#signup_model').modal('show');
	}

	$scope.redirect_leakage = function(){
		// alert(db_pan);
		btnclick = getCookie("btnclick");
		// alert(btnclick);

		if(db_pan != '' && db_pan.length >= 10 && issalat!=0)
			if(btnclick == 'file your return')
				$('#m-a-a').modal('show');
			else if(btnclick == 'calculate tax Leakage')
				window.location.href = "/calculate_tax";

		// if(db_pan != '' && db_pan.length >= 10 )
		// 	window.location.href = "/calculate_tax";

		// else
		// 	window.location.href = "/salat_reg";
	}

	$scope.redirect_leakage_header = function(){

		if(client_id == 0 || client_id == 1){
			// alert('Please login to continue.');
			$('#signup_model').modal('show');
		}else{
			if(db_pan.length >= 10 && p_age!=null)
				window.location.href = "/tax_report";
			else if(db_pan=='')
				window.location.href = "/salat_reg";
			else
				window.location.href = "/calculate_tax";
		}
	}

	// user_tracking('click','calculate tax Leakage');
	/*$scope.user_tracking =function(event,event_name){
		$.ajax({
			url:'/user_tracking_func/',
			type:'POST',
			dataType: 'json',
			data:{'event':event,'event_name':event_name,'client_id':client_id},
			success: function(response){
				console.log(response);
			}
		});
	}*/

	$scope.update_fy = function(redirect_to,fy){

		// console.log(role)
		// console.log(client_id)
		if(role=='Client')
			var client_id1=client_id
		else
			var client_id1=$rootScope.client_id

		console.log(client_id1)

		if(client_id!=undefined){

			$.ajax({
				url:'/update_fy/',
				type:'POST',
				dataType: 'json',
				data:{'fy':fy,'client_id':client_id1},
				success: function(response){
					console.log(response);
					if(response.status == 'success')
						window.location.href = '/'+redirect_to+'/'+client_id1;
				}
			});

		}
		
	}

	

}]);

