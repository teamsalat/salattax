var app = angular.module('myapp', ['ui.utils.masks','ui.mask','naif.base64']);

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});


app.directive('dynamicModel', ['$compile', '$parse','$timeout',function ($compile, $parse,$timeout) {
    return {
        restrict: 'A',
        terminal: true,
        priority: 100000,
        link: function (scope, elem) {

            // scope.$watch(function() {
                var name = $parse(elem.attr('dynamic-model'))(scope);
                elem.removeAttr('dynamic-model');
                elem.attr('ng-model', name);
                $compile(elem)(scope);
            // })

        }
    };


}]);


// this.app.directive('dynamicModel', ['$compile', '$parse', function ($compile, $parse) {
//     return {
//         restrict: 'A',
//         terminal: true,
//         priority: 100000,
//         link: function (scope, elem) {
//             var name = $parse(elem.attr('dynamic-model'))(scope);
//             elem.removeAttr('dynamic-model');
//             elem.attr('ng-model', name);
//             $compile(elem)(scope);
//         }
//     };
// }]);


app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);


app.controller('index_cntrl', ['$scope', function($scope) {
  	//var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
    // $scope.url = 'https://salattax.com';
    var UserId = '';
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        // alert(document.cookie);
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function checkCookie() {
        UserId = getCookie("UserId");
        if (UserId != "") {
            // alert("Welcome again " + UserId);
        } else {
            $.ajax({
                url:'/get_session/',
                type:'POST',
                dataType: 'json',
                data:{'mail':'mail'},
                success: function(response){
                    console.log(response);
                    // UserId = prompt("Please enter your name:", "");
                    UserId = response.user_id;
                    if (UserId != "" && UserId != null) {
                        setCookie("UserId", UserId, 90);
                    }
                    $scope.UserId = UserId;
                }
            });
        }
    }
    checkCookie();
    domainName = document.location.origin;
    // console.log(domainName)
    $scope.url = domainName;
    $scope.UserId = UserId;
    $scope.display_login = {"display" : ""};
    $scope.display_logout = {"display" : "none"};
    $scope.start_now = function() {
    };
    var client_id_main = 1;

    $scope.goSignin = function() {
    };
    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $.ajax({
        url:'/get_client/',
        type:'POST',
        dataType: 'json',
        data:{'mail':'mail'},
        success: function(response){
            if (response.client_id!=0) {
                client_id_main = response.client_id;
                role=response.role
                // console.log(role)
            }
        }
    });
    $scope.user_tracking =function(event,event_name){

        if(role=='Client'){

            $.ajax({
                url:'/user_tracking_func/',
                type:'POST',
                dataType: 'json',
                data:{'event':event,'event_name':event_name,'client_id':client_id_main,'UserId':UserId},
                success: function(response){
                    console.log(response);
                },error:function(response){
                    console.log(response)
                }
            });

        }
       
    }
}]);

app.controller('uploadForm_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.gotoPersonalInfo = function() {
        window.location.href = $scope.url+'/personal_information/';
    };
}]);

app.controller('personal_info_cntrl', ['$scope', function($scope) {
    var client_id = 1;
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};

    // hex_client_id=id.toString(16)
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
        // window.location.href = $scope.url+'/income_details/';
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
        // window.location.href = $scope.url+'/deductions/';
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
        // window.location.href = $scope.url+'/exempt_income/';
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
        // window.location.href = $scope.url+'/taxes_paid/';
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
        // window.location.href = $scope.url+'/NewReview/';
    };
    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                // console.log(response);
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }

}]);

app.controller('client_registration_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
	$scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        alert('signOut');
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('other_details_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        alert('signOut');
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('income_details_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        alert('signOut');
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                // console.log(response);
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('deductions_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('exempt_income_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('taxes_paid_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        alert('signOut');
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('review_cntrl', ['$scope', function($scope) {
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.chooseFillingType = function(id) {
        cid = $scope.get_client_ID(id,'chooseFillingType');
    };
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
    };
    $scope.signOut = function(){
        alert('signOut');
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }
}]);

app.controller('chooseFillingType_cntrl', ['$scope', function($scope) {
    var client_id = 1;
    domainName = document.location.origin;
    $scope.url = domainName;
    $scope.display_login = {"display" : "block"};
    $scope.display_logout = {"display" : "none"};
    $scope.gotoPersonalInformation = function(id) {
        cid = $scope.get_client_ID(id,'personal_information');
        // window.location.href = $scope.url+'/personal_information/';
    };
    $scope.gotoOtherDetails = function(id) {
        cid = $scope.get_client_ID(id,'other_details');
    };
    $scope.gotoIncomeDetails = function(id) {
        cid = $scope.get_client_ID(id,'income_details');
        // window.location.href = $scope.url+'/income_details/';
    };
    $scope.gotoDeductions = function(id) {
        cid = $scope.get_client_ID(id,'deductions');
        // window.location.href = $scope.url+'/deductions/';
    };
    $scope.gotoExempt = function(id) {
        cid = $scope.get_client_ID(id,'exempt_income');
        // window.location.href = $scope.url+'/exempt_income/';
    };
    $scope.gotoTaxesPaid = function(id) {
        cid = $scope.get_client_ID(id,'taxes_paid');
        // window.location.href = $scope.url+'/taxes_paid/';
    };
    $scope.gotoNewReview = function(id) {
        cid = $scope.get_client_ID(id,'NewReview');
        // window.location.href = $scope.url+'/NewReview/';
    };
    $scope.signOut = function(){
        $.ajax({
            url:'/logout_user/',
            type:'POST',
            dataType: 'json',
            data:{'data':'data'},
            success: function(response){
            console.log(response);
                if (response.logout.status == 'success') {
                    window.location.href = $scope.url+'/';
                }
            }
        });
    }
    $scope.get_client_ID = function(id,goto){
        $.ajax({
            url:'/get_client_byID/',
            type:'POST',
            dataType: 'json',
            data:{'username':id},
            success: function(response){
                // console.log(response);
                window.location.href = $scope.url+'/'+goto+'/'+response.client_id;
            }
        });
    }

}]);
// personal_information.html
// other_details.html
// income_details.html
// deductions.html
// exempt_income.html
// taxes_paid.html
// NewReview.html
