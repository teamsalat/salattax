
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('camsKarvy_controller', ['$scope','$compile', function($scope,$compile,$http,CONFIG) {
	redirect_url = $scope.url;
	var client_id = 1;
	$scope.ITR_Data = {'display':'none'};
	get_txt = 0;
	$scope.salat_tax = {'display':'none'};
	$scope.table_26as = {'display':'none'};
	$scope.invested_share_div = {'display':'none'};
	$('.loader').hide();
	var i = 0;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				client_id = response.client_id;
				// if (client_id!=1) {
				// 	$scope.Start_Now = 'File Your Return Now';
				// 	$scope.$apply();
				// }
			}
		});
	},1000);

	$scope.forgot_password = function(pan){
		$.ajax({
			url:'/forgot_password/',
			type:'POST',
			dataType: 'json',
			data:{'pan':pan},
			success: function(response){
				get_txt = 1;
				// console.log(response);
				// AVOPD6885F
				// 24/08/1989
				// AYHPT6350R
				// 10/10/1995
				if(response.text.split(" ").length == 2){
					$scope.email_txt = response.text.split(" ")[0];
					$scope.mobile_txt = response.text.split(" ")[1];
				}
				$scope.$apply();
			}
		});
	}
	
	$scope.validation_text = function(type,value,$event) {
		//salat registration
		if(type == "salat_mail_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "salat_mail_otp"){
					$scope.otperr= "";
					return true;
				}
			}else{
				if(type == "salat_mail_otp"){
					$scope.otperr= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "salat_mail_otp") && value==undefined){
			if(type == "salat_mail_otp"){
				$scope.otperr= "Please Check Your E-mail OTP";
				return false;
			}
		}
		if(type == "salat_mobile_otp"  && value!==undefined){
			var text_length=value.length;
			if((/\d{6}/.test(value)) && text_length==6){
				if(type == "salat_mobile_otp"){
					$scope.otperr= "";
					return true;
				}
			}else{
				if(type == "salat_mobile_otp"){
					$scope.otperr= "Please Enter Valid OTP";
					return false;
				}
			}
		}else if((type == "salat_mobile_otp") && value==undefined){
			if(type == "salat_mobile_otp"){
				$scope.otperr= "Please Check Your Mobile OTP";
				return false;
			}
		}
		$scope.$apply();
	}

	$scope.trigger_mail = function(){
		var mail = $scope.user.mailid;
		var pan = $scope.user.pan;
		var mail_status = false;
		var pan_status = false;
		
		if(mail!==''){
			if( /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail) ){ 
				$scope.mail_err= "";
				mail_status = true;
			}else
				$scope.mail_err= "Please Enter Valid Email ID";
		}else
			$scope.mail_err= "Email is required";
		
		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if(mail_status == true && pan_status == true){
			console.log('Start Ajax');
			$.ajax({
				url:'/trigger_mail_karvy/',
				type:'POST',
				dataType: 'json',
				data:{'mail':mail,'pan':pan},
				success: function(response){
					console.log(response);
				}
			});
		}
	}

	$scope.get_attachement = function(){
		/*$.ajax({
			url:'/get_mail_attachment/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
			}
		});*/
	}

	$scope.get_ITR = function(){
		cleartimer();
		var pan = $scope.user.pan;
		var dob = $scope.user.dob;
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			if(get_txt == 0)
				$scope.forgot_password(pan);
			timer();
			$('.loader').show();
			var IsSalat = $scope.IsSalat(pan);
			$.ajax({
				url:'/download_ITR/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'option':'calculate','dob':dob},
				success: function(response){
					var container = angular.element( document.querySelector( '#ITR_content' ));
					container.empty();
					$('.loader').hide();
					console.log(response);
					/*if(){
						status: {pan_exist: "This PAN is added as Salat Client.", pan_status: "", no_of_ITR: 5, otp_status: "", dob_status: ""}
						status:
						dob_status: ""
						no_of_ITR: 5
						otp_status: ""
						pan_exist: "This PAN is added as Salat Client."
						pan_status: ""

						{status: "Error download ITR : list index out of range"}
					}*/
					if(response.count_itr > 0){
						$scope.ITR_Data = {'display':'block'};
						$scope.ITR_entry_count= "";
					}

					if(response.status.pan_status == 'PAN does not exist'){
						$scope.pan_err = 'Please Enter Valid PAN';
					}
					if(response.status.pan_status == 'This PAN is not registered with e-Filing.'){
						$scope.pan_err = 'This PAN is not registered with e-Filing.';
						clearInterval(set_isSalat);
					}
					if(response.status.dob_status == 'Invalid Date'){
						$scope.pan_err = 'Invalid Date Of Birth';
						clearInterval(set_isSalat);
					}
					if(response.status.otp_status == 'Invalid Mobile OTP'){
						$scope.user.mobile_otp = '';
						$scope.otperr = 'Invalid Mobile OTP. Please re-enter';
					}
					if(response.status.otp_status == 'Invalid Mail OTP'){
						$scope.user.mail_otp = '';
						$scope.otperr = 'Invalid Mobile OTP. Please re-enter';
					}
					//otp_status: "success"
					if(response.status.otp_status == 'success'){
						$scope.user.mail_otp = '';
						$scope.user.mobile_otp = '';
						$('#modal_otp').modal('hide');
						$scope.get_ITR();
					}
					$scope.$apply();
					stoptimer();
				},
				error: function(response){
					$('.loader').hide();
					stoptimer();
					$scope.get_ITR();
				}
			});
		}
	}

	$scope.get_26as = function(){
		cleartimer();
		var pan = $scope.user.pan;
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			timer();
			$('.loader').show();
			var pass_obj = { "pan":$scope.user.pan};
			pass_obj["dob"] = $scope.user.dob;
			var data_pass = JSON.stringify(pass_obj);
			console.log('get_form_26AS Ajax Started');
			$.ajax({
				url:'/get_form_26AS/',
				type:'POST',
				dataType: 'json',
				data:{'data':data_pass},
				success: function(response){
					$('.loader').hide();
					stoptimer();
					console.log(response);
					if (response.status == 'success') {
						$.ajax({
							url:'/unzip_txt/',
							type:'POST',
							dataType: 'json',
							data:{'data':data_pass},
							success: function(response){
								console.log(response);
								status = JSON.stringify(response['status']).replace(/\"/g, "");
								// alert(status);
								if (status == 'success') {
									$.ajax({
										url:'/excelToDB/',
										type:'POST',
										dataType: 'json',
										data:{'data':data_pass,'client_id':'1'},
										success: function(response){
											console.log(response);
											status = JSON.stringify(response['status']).replace(/\"/g, "");
										}
									});
								}
							}
						});
					}
				},
				error: function(response){
					$('.loader').hide();
					stoptimer();
				}
			});
		}
	}

	$scope.invested_share = function(){
		var invested_share = $scope.user.invested_share;
		if(invested_share=="yes"){
			$scope.invested_share_div = {'display':'block'};
		}else{
			$scope.invested_share_div = {'display':'none'};
		}
	}

	$scope.cal_tax = function(){
		var pan = $scope.user.pan;
		var pan_status = false;
		var p_age = $scope.user.p_age;
		var invested_share = $scope.user.invested_share;
		var shares_year = $scope.user.shares_year;
		var age_status = false;
		var shares_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";
		
		if(p_age==null || p_age=="undefined" )
			$scope.age_err= "Parent's Age is required";
		else{
			$scope.age_err= "";
			age_status = true;
		}

		if((invested_share=='yes' && shares_year!="") || invested_share=='no'){
			$scope.shares_err= "";
			shares_status = true;
		}
		else
			$scope.shares_err= "year is required";

		if( pan_status == true && age_status == true && shares_status==true){
			$.ajax({
				url:'/calculate_salat_tax/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'p_age':p_age,'invested_share':invested_share,'shares_year':shares_year},
				success: function(response){
					var container = angular.element( document.querySelector('#salat_tax_content'));
					container.empty();
					console.log(response);
					var s_obj = response.salat_tax ;

					if(response.count_salattax > 0){
						$scope.salat_tax = {'display':'block'};
						$scope.salat_tax_count= "";
					} else{
						$scope.salat_tax = {'display':'none'};
						$scope.salat_tax_count= "No Data Found";
					}

					for(var i = 0; i < response.count_salattax; i++) {
						var capital_gain = $scope.money_format(s_obj[i].capital_gain);
						var cal_GTI = $scope.money_format(s_obj[i].cal_GTI);
						var TI = $scope.money_format(s_obj[i].TI);
						var c_80 = $scope.money_format(s_obj[i].c_80);
						var d_80 = $scope.money_format(s_obj[i].d_80);
						var ccg_80 = $scope.money_format(s_obj[i].ccg_80);
						var ccd_1b_80 = $scope.money_format(s_obj[i].ccd_1b_80);
						var ccd_2_80 = $scope.money_format(s_obj[i].ccd_2_80);
						var e_80 = $scope.money_format(s_obj[i].e_80);
						var ee_80 = $scope.money_format(s_obj[i].ee_80);
						var g_80 = $scope.money_format(s_obj[i].g_80);
						var gg_80 = $scope.money_format(s_obj[i].gg_80);
						var tta_80 = $scope.money_format(s_obj[i].tta_80);
						var Tx = $scope.money_format(s_obj[i].Tx);
						var Tx1 = $scope.money_format(s_obj[i].Tx1);
						var rebate = $scope.money_format(s_obj[i].rebate);
						var TxR = $scope.money_format(s_obj[i].TxR);
						var SH = $scope.money_format(s_obj[i].SH);
						var TxSH = $scope.money_format(s_obj[i].TxSH);
						var EC = $scope.money_format(s_obj[i].EC);
						var refund = $scope.money_format(s_obj[i].refund);
						var ToTax = $scope.money_format(s_obj[i].ToTax);
						var tax_paid = $scope.money_format(s_obj[i].tax_paid);
						var tax_leakage = $scope.money_format(s_obj[i].tax_leakage);
						markup = "<tr><td>"+s_obj[i].year+"</td><td>"+capital_gain+"</td><td>"+cal_GTI+"</td>";
						markup += "<td>"+c_80+"</td><td>"+d_80+"</td><td>"+ccg_80+"</td><td>"+ccd_1b_80+"</td>";
						markup += "<td>"+ccd_2_80+"</td><td>"+e_80+"</td><td>"+ee_80+"</td><td>"+g_80+"</td>";
						markup += "<td>"+gg_80+"</td><td>"+tta_80+"</td><td>"+TI+"</td><td>"+Tx+"</td>";
						markup += "<td>"+Tx1+"</td><td>"+rebate+"</td><td>"+TxR+"</td><td>"+SH+"</td>";
						markup += "<td>"+TxSH+"</td><td>"+EC+"</td><td>"+refund+"</td><td>"+ToTax+"</td>";
						markup += "<td>"+tax_paid+"</td><td>"+tax_leakage+"</td></tr>";
						var temp = $compile(markup)($scope);
						container.append( temp );
					}
					$scope.$apply();
				}
			});
		}
	}

	$scope.show_ITR = function(){
		cleartimer();
		var pan = $scope.user.pan;
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			timer();
			$('.loader').show();
			$.ajax({
				url:'/download_ITR/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'option':'show'},
				success: function(response){
					var container = angular.element( document.querySelector( '#ITR_content' ));
					container.empty();
					stoptimer();
					$('.loader').hide();
					console.log(response);
					if(response.count_itr > 0){
						$scope.ITR_Data = {'display':'block'};
						$scope.ITR_entry_count= "";
					}
					else
						$scope.ITR_entry_count= "No Data Found";

					for(var i = 0; i < response.count_itr; i++) {
						var href = "https://salattax.com/static/ITR_excel/"+pan+"_"+response.year[i]+"_"+response.ITR[i]+".pdf";
						var link = "<a href="+href+" target='_blank'>"+response.year[i]+"</a>";

						var salary = $scope.money_format(response.income_from_salary[i]);
						if(response.income_from_hp[i] != 'NA')
							var hp = $scope.money_format(response.income_from_hp[i]);
						else
							var hp = response.income_from_hp[i];
						if(response.capital_gain[i] != 'NA')
							var capital_gain = $scope.money_format(response.capital_gain[i]);
						else
							var capital_gain = response.capital_gain[i];
						if(response.schedule_si_income[i] == 'NA' || response.schedule_si_income[i] == '')
							var schedule_si_income = 'NA';
						else
							var schedule_si_income = $scope.money_format(response.schedule_si_income[i]);
						// var schedule_si_income = parseInt(response.schedule_si_income[i]).toFixed(1).replace(/(\d)(?=(\d{2})+\.)/g, '$1,');
						var other_src = $scope.money_format(response.income_from_other_src[i]);
						var business_or_profession = $scope.money_format(response.business_or_profession[i]);
						if(response.interest_gross[i] == 'NA' || response.interest_gross[i] == '')
							var int_gross = 'NA';
						else
							var int_gross = $scope.money_format(response.interest_gross[i]);
						var gti = $scope.money_format(response.gross_total_income[i]);
						if(response.deduction[i] == 'NA' || response.deduction[i] == '')
							var deduction = 'NA';
						else
							var deduction = $scope.money_format(response.deduction[i]);
						var c_80 = $scope.money_format(response.c_80[i]);
						var d_80 = $scope.money_format(response.d_80[i]);
						var tta_80 = $scope.money_format(response.tta_80[i]);
						if(response.ccd_1b_80[i] != 'NA')
							var ccd_1b_80 = $scope.money_format(response.ccd_1b_80[i]);
						else
							var ccd_1b_80 = response.ccd_1b_80[i];
						var tta_80 = $scope.money_format(response.tta_80[i]);
						if(response.ccg_80[i] == 'NA' || response.ccg_80[i] == '')
							var ccg_80 = 'NA';
						else
							var ccg_80 = $scope.money_format(response.ccg_80[i]);
						if(response.e_80[i] != 'NA')
							var e_80 = $scope.money_format(response.e_80[i]);
						else
							var e_80 = response.e_80[i];
						if(response.ee_80[i] != 'NA')
							var ee_80 = $scope.money_format(response.ee_80[i]);
						else
							var ee_80 = response.ee_80[i];
						if(response.g_80[i] != 'NA')
							var g_80 = $scope.money_format(response.g_80[i]);
						else
							var g_80 = response.g_80[i];
						if(response.gg_80[i] != 'NA')
							var gg_80 = $scope.money_format(response.gg_80[i]);
						else
							var gg_80 = response.gg_80[i];
						var total_income = $scope.money_format(response.total_income[i]);
						var schedule_si_tax = $scope.money_format(response.schedule_si_tax[i]);
						if(response.total_tax_interest[i] != 'NA')
							var total_tax_interest = $scope.money_format(response.total_tax_interest[i]);
						else
							var total_tax_interest = response.total_tax_interest[i];
						var tax_paid = $scope.money_format(response.tax_paid[i]);
						var adv_tax_self_ass_tax = $scope.money_format(response.adv_tax_self_ass_tax[i]);
						if(response.total_tcs[i] != 'NA')
							var total_tcs = $scope.money_format(response.total_tcs[i]);
						else
							var total_tcs = response.total_tcs[i];
						var total_tds_ITR = $scope.money_format(response.total_tds_ITR[i]);

						markup = "<tr><td>"+link+"</td><td>"+response.ITR[i]+"</td><td>"+salary+"</td>";
						markup += "<td>"+parseInt(response.no_of_hp[i]).toFixed(0)+"</td><td>"+response.income_from_hp[i]+"</td>";
						markup += "<td>"+response.interest_payable_borrowed_capital[i]+"</td><td>"+capital_gain+"</td>";
						markup += "<td>"+schedule_si_income+"</td><td>"+other_src+"</td><td>"+business_or_profession+"</td><td>"+int_gross+"</td>";
						markup += "<td>"+gti+"</td><td>"+deduction+"</td><td>"+c_80+"</td><td>"+d_80+"</td><td>"+tta_80+"</td>";
						markup += "<td>"+ccd_1b_80+"</td><td>"+ccg_80+"</td><td>"+e_80+"</td><td>"+ee_80+"</td><td>"+g_80+"</td>";
						markup += "<td>"+gg_80+"</td><td>"+total_income+"</td><td>"+schedule_si_tax+"</td><td>"+total_tax_interest+"</td>";
						markup += "<td>"+tax_paid+"</td><td>"+adv_tax_self_ass_tax+"</td><td>"+total_tcs+"</td><td>"+total_tds_ITR+"</td></tr>";
						var temp = $compile(markup)($scope);
						if(response.year[i]!='2013')
							container.append( temp );
					}
					$scope.$apply();
				},
				error: function(response){
					stoptimer();
					$('.loader').hide();
				}
			});
		}
	}

	$scope.IsSalat = function(pan){
		set_isSalat = setInterval(check_isSalat, 5000); //every 5sec

		function check_isSalat() {
			$.ajax({
				url:'/check_isSalat/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan},
				success: function(response){
					console.log(response);
					if(response.flag == '2' || response.flag == '1' )
						clearInterval(set_isSalat);
					if(response.flag == '0'){
						$('.loader').hide();
						clearInterval(set_isSalat);
						$('#modal_otp').modal('show');
					}
				}
			});
		}
	}

	$scope.show_26as = function(){
		var pan = $scope.user.pan;
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true){
			$.ajax({
				url:'/show_26as/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan,'client_id':'1'},
				success: function(response){
					console.log(response);
					var container = angular.element( document.querySelector( '#table_26as_content' ));
					container.empty();
					if(response.count>0)
						$scope.table_26as = {'display':'block'};
					// table_26as_content
					for(var i = 0; i < response.count; i++) {
						/*<th>PART</th>
                                <th>Deductor Collector Name</th>
                                <th>TAN</th>
                                <th>Section</th>
                                <th>No of Transaction</th>
                                <th>Amount Paid</th>
                                <th>Tax Deduction</th>
                                <th>TDS/TCS Deposited</th>
                                <th>Year</th>*/
                        var href = "https://salattax.com/static/zip_26AS/"+pan+"-"+response.year[i]+".xls";
						var link = "<a href="+href+" target='_blank'>"+response.year[i]+"</a>";

						markup = "<tr><td>"+response.part[i]+"</td><td>"+response.Deductor_Collector_Name[i]+"</td>";
						markup += "<td>"+response.TAN_PAN[i]+"</td>";
						markup += "<td>"+response.section[i]+"</td><td>"+response.no_of_transaction[i]+"</td>";
						markup += "<td>"+response.amount_paid[i]+"</td><td>"+response.tax_deducted[i]+"</td>";
						markup += "<td>"+response.tds_26as[i]+"</td><td>"+link+"</td></tr>";
						var temp = $compile(markup)($scope);
						container.append( temp );
					}
					$scope.$apply();
				}
			});
		}
	}

	$scope.get_otp = function(user,$event) {
		var salat_mail_otp_status = $scope.validation_text("salat_mail_otp",user.mail_otp);
		var salat_mobile_otp_status = $scope.validation_text("salat_mobile_otp",user.mobile_otp);
		if (salat_mail_otp_status == false && salat_mobile_otp_status == true) {
			salat_mail_otp_status = $scope.validation_text("salat_mail_otp",user.mail_otp);
		}
		var salat_mail_otp = user.mail_otp;
		var salat_mobile_otp = user.mobile_otp;
		var pan = user.pan;
		if(salat_mail_otp_status == true && salat_mobile_otp_status == true && $scope.otperr == ''){
			$.ajax({
				url:'/save_salat_otp/',
				type:'POST',
				dataType: 'json',
				data:{'salat_mail_otp':salat_mail_otp,'salat_mobile_otp':salat_mobile_otp,'pan':pan},
				success: function(response){
					console.log(response);
				}
			});
		}
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(!isNaN(x)){
			// x = x.toString();
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

	$scope.multiple_eri = function(){
		var pan = $scope.user.pan;
		var dob = $scope.user.dob;
		var pan_status = false;

		if(pan!==''){
			if( /[A-Z]{5}\d{4}[A-Z]{1}/.test(pan) ){ 
				$scope.pan_err= "";
				pan_status = true;
			}else
				$scope.pan_err= "Please Enter Valid PAN";
		}else
			$scope.pan_err= "PAN is required";

		if( pan_status == true ){
			$.ajax({
				url:'/check_isSalat/',
				type:'POST',
				dataType: 'json',
				data:{'pan':pan},
				success: function(response){
					console.log(response);
					//pass client_id
					if( response.flag == '1' ){
						$.ajax({
							url:'/multiple_eri/',
							type:'POST',
							dataType: 'json',
							data:{'pan':pan,'dob':dob,'option':'salatclient'},
							success: function(response){
								console.log(response);
							}
						});
					}
					else{
						$.ajax({
							url:'/multiple_eri/',
							type:'POST',
							dataType: 'json',
							data:{'pan':pan,'dob':dob,'option':'notsalatclient'},
							success: function(response){
								console.log(response);
							}
						});
					}
				}
			});
		}
	}

}]);

