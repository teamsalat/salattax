
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('taxes_paid_controller', ['$scope','$compile', function($scope,$compile,$http, $location,CONFIG) {
	var r_id = 1;
	var client_id = '1';
	var total_tax_paid = 0;
	$scope.taxDeductedAtSource = {'display':''};
	adv_selfA_tax_count = 0;

	$('.loader').show();
	$(window).load(function() {
		console.log('ID : '+$scope.user.id);
		console.log('Client ID : '+$scope.user.client_id);
    	client_id=$scope.user.client_id
		
		$.ajax({
			url:'/get_client/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				if (response.client_id!=0 && $scope.user.id != '') {
					// client_id = response.client_id;
					// r_id = client_id;
					// $scope.get_taxes_paid();
				}
				else if ($scope.user.id == ''){
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}
				if ($scope.user.id != '') {
					$.ajax({
						url:'/get_client_byID/',
						type:'POST',
						dataType: 'json',
						async:false,
						data:{'username':$scope.user.id},
						success: function(response){
							// console.log(response);
							// client_id = response.client_id;
							// $scope.return_detail_fy = response.return_year;
							$.ajax({
								url:'/get_return_details/',
								type:'POST',
								dataType: 'json',
								async:false,
								data:{'client_id':client_id},
								success: function(response){
									console.log(response);
									$scope.return_detail_fy = response.return_year;
									selected_fy = response.return_year;
								}
							});
							email = response.email;
							r_id = client_id;
							$scope.get_taxes_paid();
						}
					});
				}
				else{
					$scope.display_logout = {"display" : "block"};
					$scope.display_login = {"display" : "none"};
				}

				
			}
		});
		//Advance tax -> before 31-mar
	});

	setTimeout(function(){
		$scope.total_tax_paid = $scope.money_format(total_tax_paid);
		$scope.$apply();
	}, 1000);

	$scope.gotodeductiondetails=function(){
    	window.location.href = redirect_url+'/deductions/'+client_id;
  	}

    $scope.gotoReview = function() {
        window.location.href = $scope.url+'/NewReview/'+client_id;
    };

	$scope.get_taxes_paid = function(){
		$.ajax({
			url:'/get_26AS_tds/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				var container = angular.element( document.querySelector( '#tds_on_salary' ));
				var container2 = angular.element( document.querySelector( '#tds_otherthan_salary' ));
				var container3 = angular.element( document.querySelector( '#tax_collected_source' ));

				var TAN = '';
				var name_employer = '';
				var income_under_head = '';
				var tax_deducted = '';
				var total_tax_deducted = 0;
				var total_tax_collected = 0;
				var count_section192 = 0;
				var count_partB = 0;
				var count_tax_other_salary = 0;
				for (var i = 0; i < response.no_of_tds; i++) {
					TAN = response.tan_pan[i];
					name_employer = response.d_c_name[i];
					income_under_head = response.amt_paid[i];
					tax_deducted = response.tds_deposited[i];

					//change condition section == 192 is 'Tax Deducted at Source on Salary'
					// if (response.part[i]=='A') {
					if (response.section[i]=='192') {
						count_section192 += 1;
						markup = "<tr><td>"+TAN+"</td><td>"+name_employer+"</td><td>"+$scope.money_format(income_under_head)+"</td><td>"+$scope.money_format(tax_deducted)+"</td></tr>";
						var temp = $compile(markup)($scope);
						container.append( temp );
						total_tax_deducted += tax_deducted;
					}
					else {
						count_tax_other_salary += 1;
						markup = "<tr><td>"+TAN+"</td><td>"+name_employer+"</td><td>"+$scope.money_format(income_under_head)+"</td>";
						markup += "<td>"+$scope.money_format(tax_deducted)+"</td><td>"+$scope.money_format(tax_deducted)+"</td></tr>";
						var temp = $compile(markup)($scope);
						container2.append( temp );
						total_tax_deducted += tax_deducted;
					}
					if ( response.part[i]=='B') {
						count_partB += 1;
						markup = "<tr><td>"+TAN+"</td><td>"+name_employer+"</td><td>"+$scope.money_format(income_under_head)+"</td><td>"+$scope.money_format(tax_deducted)+"</td></tr>";
						var temp = $compile(markup)($scope);
						container3.append( temp );
						total_tax_collected += tax_deducted;
					}
				}
				// if(response.countA<=0){
				if(count_section192==0)
					$scope.taxDeductedAtSource = {'display':'none'};
				if(response.countA<=0){
					markup = "<tr><td></td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>";
					var temp = $compile(markup)($scope);
					container2.append( temp );
				}
				if (count_partB==0)
					$scope.tax_collected_source_div = {'display':'none'};
				if (count_tax_other_salary==0)
					$scope.tax_other_salary = {'display':'none'};

				$scope.total_tax_deducted = $scope.money_format(total_tax_deducted);
				$scope.total_tax_collected = $scope.money_format(total_tax_collected);
				// $scope.$apply();


			}
		});

		$.ajax({
			url:'/get_26AS_taxes_advTax/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'r_id':r_id},
			success: function(response){
				console.log(response);
				var container = angular.element( document.querySelector( '#adv_tax' ));
				var deposit_date = '';
				var BSR_code = '';
				var challan_no = '';
				var total_tax = '';
				for (var i = 0; i < response.count_adv; i++) {
					adv_selfA_tax_count += 1;
					deposit_date = response.d_date[i];
					BSR_code = response.bsr_code[i];
					challan_no = response.challan_no[i];
					total_tax = response.tax_p[i];
					total_tax_paid += total_tax;

					var markup = "<tr><td>"+BSR_code+"</td><td>"+deposit_date+"</td><td>"+challan_no+"</td><td>"+$scope.money_format(total_tax)+"</td></tr>";
					var temp = $compile(markup)($scope);
					container.append( temp );
				}
				if (adv_selfA_tax_count == 0)
					$scope.adv_selfA_tax_div = {'display':'none'}
				else
					$scope.adv_selfA_tax_div = {'display':'block'}
			}
		});

		$.ajax({
			url:'/get_26AS_taxes_self_a/',
			type:'POST',
			async:false,
			dataType: 'json',
			data:{'r_id':r_id},
			success: function(response){
				// console.log(response);
				var container = angular.element( document.querySelector( '#self_a_tax' ));
				var deposit_date = '';
				var BSR_code = '';
				var challan_no = '';
				var total_tax = '';
				for (var i = 0; i < response.count_adv; i++) {
					adv_selfA_tax_count += 1;
					deposit_date = response.d_date[i];
					BSR_code = response.bsr_code[i];
					challan_no = response.challan_no[i];
					total_tax = response.tax_p[i];
					total_tax_paid += total_tax;

					var markup = "<tr><td>"+BSR_code+"</td><td>"+deposit_date+"</td><td>"+challan_no+"</td><td>"+$scope.money_format(total_tax)+"</td></tr>";
					var temp = $compile(markup)($scope);
					container.append( temp );
				}
				if (adv_selfA_tax_count == 0)
					$scope.adv_selfA_tax_div = {'display':'none'}
				else
					$scope.adv_selfA_tax_div = {'display':'block'}

				$('.loader').hide();
			}
		});

		
	}

	$scope.money_format = function(val){
		val = val.toString();
		var n = val.indexOf(".");
		var x = "";
		if(n>=0)
			x = val.split(".")[0];
		else
			x = val;

		if(val=='NA' || val=='')
			return 0

		if(!isNaN(x)){
			// x = x.toString();
			var n_minus = x.indexOf("-");
			if(n_minus>=0)
				x = x.replace("-","");
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
			    lastThree = ',' + lastThree;
			var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
			if(n_minus>=0)
				return "-"+res;
			else
				return res;
		}else
			return val;
	}

}]);

