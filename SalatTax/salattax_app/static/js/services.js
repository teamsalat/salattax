
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('services_controller', ['$scope', function($scope,$http,CONFIG) {
	
	redirect_url = $scope.url;
	var client_id = 1;
	
	$scope.$watch("demoInput", function(){
		UserId = $scope.UserId;
		$scope.user_s.no_form16 = '0';
		$scope.user_s.no_house = '0';
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				console.log(response);
				if (response.client_id!=0) {
					client_id = response.client_id;
					// console.log(client_id)
					$scope.get_all_details(client_id);
				}else{ }
				
				$scope.$apply();
			}
		});
	},900);

	$scope.get_all_details = function(client_id){
		$.ajax({
			url:'/get_form16_data/',
			type:'POST',
			dataType: 'json',
			data:{'data':'data','client_id':client_id},
			success: function(response){
				no_of_company = JSON.stringify(response['no_of_company']).replace(/\"/g, "");
				console.log(no_of_company);
				$scope.user_s.no_form16 = no_of_company;
				console.log($scope.user_s);
				$scope.$apply();
				select_form16();
				all_total();
			}
		});
		$.ajax({
			url:'/get_house_property/',
			type:'POST',
			dataType: 'json',
			data:{'data':'data','client_id':client_id},
			success: function(response){
				no_of_hp = JSON.stringify(response.no_of_hp).replace(/\"/g, "");
				console.log(no_of_hp);
				$scope.user_s.no_house = no_of_hp;
				house();
				$scope.$apply();
				all_total();
			}
		});
		$.ajax({
			url:'/get_capital_gain/',
			type:'POST',
			dataType: 'json',
			data:{'client_id':client_id},
			success: function(response){
				console.log(response);
				if(response.equity == 1){
					$scope.user_s.sold_mf = '300';
					yfund('300');
				}
				if(response.shares == 1){
					$scope.user_s.sold_shares = '300';
					ynshare('300');
				}
				$scope.$apply();
				all_total();
			}
		});
		// alert(client_id);
	}

	$scope.pay_efiling = function(){
		if (client_id == 1 || client_id == 0) {
			$('#signup_model').modal('show');
		}else{
			amount_pay = get_total_amt();
			// alert(amount_pay);
			window.location.href = redirect_url+'/subscribe_payumoney/'+client_id+'amt'+amount_pay+'amt'+'efiling';
		}
	}


}]);

