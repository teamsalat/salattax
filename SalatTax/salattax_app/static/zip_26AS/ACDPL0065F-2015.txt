^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
03-01-2019^ACDPL0065F^ACTIVE^2014-2015^2015-2016^PRERANA RAJENDRA LUNKAD^867^^LAXMI ROAD^BUDHWAR PETH^PUNE^MAHARASHTRA^411002

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^CHORDIA ASHOK DHANRAJ^PNEA15885C^^^^^224400.00^22440.00^22440.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^24-Jan-2015^F^16-May-2015^-^41140.00^4114.00^4114.00^
^2^194A^24-Oct-2014^F^26-Jan-2015^-^56100.00^5610.00^5610.00^
^3^194A^24-Jul-2014^F^11-Nov-2014^-^56100.00^5610.00^5610.00^
^4^194A^24-Apr-2014^F^14-Jul-2014^-^56100.00^5610.00^5610.00^
^5^194A^01-Apr-2014^F^14-Jul-2014^-^14960.00^1496.00^1496.00^

2^BHARAT KESHAVALAL SHAH^PNEB02352A^^^^^136000.00^13600.00^13600.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^07-Mar-2015^F^23-May-2015^-^51000.00^5100.00^5100.00^
^2^194A^01-Dec-2014^F^22-Jan-2015^-^51000.00^5100.00^5100.00^
^3^194A^08-Oct-2014^F^22-Jan-2015^-^34000.00^3400.00^3400.00^

3^HIRALAL NATHAMAL LUNKAD^PNEH05054A^^^^^200000.00^20000.00^20000.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194J^25-Mar-2015^F^11-May-2015^-^200000.00^20000.00^20000.00^

4^NOBLE CONSTRUCTION COMPANY -M/S^PNEN00188G^^^^^40000.00^4000.00^4000.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^26-Feb-2015^F^08-May-2015^-^10000.00^1000.00^1000.00^
^2^194A^24-Nov-2014^F^26-Jan-2015^-^30000.00^3000.00^3000.00^

5^NAIKNAVARE PROFILE ENVIRON HOUSING PRIVATE LIMITED^PNEN07523F^^^^^25500.00^2550.00^2550.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^02-May-2014^F^20-Jul-2014^-^25500.00^2550.00^2550.00^

6^NAIKNAVARE PROFILE ENVIRON HOUSING LLP^PNEN10322E^^^^^76500.00^7650.00^7650.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^03-Mar-2015^F^14-May-2015^-^25500.00^2550.00^2550.00^
^2^194A^01-Feb-2015^F^14-May-2015^-^25500.00^2550.00^2550.00^
^3^194A^20-Aug-2014^F^20-Oct-2014^-^25500.00^2550.00^2550.00^

7^PRAVIN MASALAWALE^PNEP00887F^^^^^6000.00^600.00^600.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^27-Mar-2015^F^18-May-2015^-^3000.00^300.00^300.00^
^2^194A^07-Oct-2014^F^26-Jan-2015^-^3000.00^300.00^300.00^

8^RAJENDRA SHANKARRAO KUTTE^PNER12046G^^^^^10000.00^1000.00^1000.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^28-Mar-2015^F^04-May-2015^-^10000.00^1000.00^1000.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
1^0021^300^22960.00^0.00^0.00^0.00^22960.00^0202976^21-Aug-2015^00178^-


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


