^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
04-12-2018^GLCPS4699C^ACTIVE^2017-2018^2018-2019^ANIKETH BALKRISHNA SHETTY^FLAT NO 9 B 4 BUILDING^MANI RATNA COMPLEX^TAWARI COLONY NR SHAMRAO^VITTAL BANK PARVATI^PUNE^MAHARASHTRA^411009

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^SQUATS FITNESS PRIVATE LIMITED^BPLS17437A^^^^^2748078.16^274807.52^274807.52^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194J^31-Mar-2018^F^23-May-2018^-^310282.59^31028.26^31028.26^
^2^194J^28-Feb-2018^F^23-May-2018^-^223299.57^22329.93^22329.93^
^3^194J^31-Jan-2018^F^23-May-2018^-^297803.00^29780.33^29780.33^
^4^194J^31-Dec-2017^F^22-Feb-2018^-^214652.00^21465.00^21465.00^
^5^194J^30-Nov-2017^F^22-Feb-2018^-^309412.00^30941.00^30941.00^
^6^194J^31-Oct-2017^F^22-Feb-2018^-^173880.00^17388.00^17388.00^
^7^194J^30-Sep-2017^F^22-Nov-2017^-^254434.00^25443.00^25443.00^
^8^194J^31-Aug-2017^F^22-Nov-2017^-^190055.00^19006.00^19006.00^
^9^194J^31-Jul-2017^F^22-Nov-2017^-^179999.00^18000.00^18000.00^
^10^194J^30-Jun-2017^F^28-Jul-2017^-^202152.00^20215.00^20215.00^
^11^194J^31-May-2017^F^28-Jul-2017^-^197348.00^19735.00^19735.00^
^12^194J^30-Apr-2017^F^28-Jul-2017^-^194761.00^19476.00^19476.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
^^^*********** No Transactions Present **********^


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


