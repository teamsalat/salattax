^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
16-05-2019^ASLPA5667L^ACTIVE^2015-2016^2016-2017^PULKIT   ANAND^10/8^ALKA PURI^^^ALLAHABAD^UTTAR PRADESH^211001

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^INNOVACCER MANAGEMENT PRIVATE LIMITED^DELI09165C^^^^^208334.00^14514.00^14514.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^31-May-2015^F^25-Jul-2015^-^100927.00^7665.00^7665.00^
^2^192^31-May-2015^F^25-Jul-2015^-^104167.00^7665.00^7665.00^
^3^192^31-May-2015^F^25-Jul-2015^B^-100927.00^-7665.00^-7665.00^
^4^192^30-Apr-2015^F^25-Jul-2015^-^108543.00^6848.00^6848.00^
^5^192^30-Apr-2015^F^25-Jul-2015^-^104167.00^6849.00^6849.00^
^6^192^30-Apr-2015^F^25-Jul-2015^B^-108543.00^-6848.00^-6848.00^

2^INNOVACCER ANALYTICS PRIVATE LIMITED^DELS52262A^^^^^1444488.00^249389.00^249389.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^31-Mar-2016^F^18-May-2016^-^322700.00^76317.00^76317.00^
^2^192^29-Feb-2016^F^18-May-2016^-^118200.00^52803.00^52803.00^
^3^192^31-Jan-2016^F^18-May-2016^-^118200.00^16835.00^16835.00^
^4^192^31-Dec-2015^F^20-Jan-2016^-^112296.00^16835.00^16835.00^
^5^192^31-Dec-2015^F^20-Jan-2016^-^118200.00^16835.00^16835.00^
^6^192^31-Dec-2015^F^20-Jan-2016^B^-112296.00^-16835.00^-16835.00^
^7^192^30-Nov-2015^F^20-Jan-2016^-^230520.00^44523.00^44523.00^
^8^192^30-Nov-2015^F^20-Jan-2016^-^232320.00^44523.00^44523.00^
^9^192^30-Nov-2015^F^20-Jan-2016^B^-230520.00^-44523.00^-44523.00^
^10^192^31-Oct-2015^F^20-Jan-2016^-^116400.00^11416.00^11416.00^
^11^192^31-Oct-2015^F^20-Jan-2016^-^118200.00^11416.00^11416.00^
^12^192^31-Oct-2015^F^20-Jan-2016^B^-116400.00^-11416.00^-11416.00^
^13^192^30-Sep-2015^F^29-Oct-2015^-^104167.00^7664.00^7664.00^
^14^192^30-Sep-2015^F^29-Oct-2015^-^104167.00^7665.00^7665.00^
^15^192^30-Sep-2015^F^29-Oct-2015^B^-104167.00^-7664.00^-7664.00^
^16^192^30-Aug-2015^F^29-Oct-2015^-^104167.00^7664.00^7664.00^
^17^192^30-Aug-2015^F^29-Oct-2015^B^-104167.00^-7664.00^-7664.00^
^18^192^30-Aug-2015^F^29-Oct-2015^-^104167.00^7665.00^7665.00^
^19^192^30-Jul-2015^F^29-Oct-2015^-^104167.00^7664.00^7664.00^
^20^192^30-Jul-2015^F^29-Oct-2015^B^-104167.00^-7664.00^-7664.00^
^21^192^30-Jul-2015^F^29-Oct-2015^-^104167.00^7665.00^7665.00^
^22^192^30-Jun-2015^F^29-Oct-2015^-^104167.00^7664.00^7664.00^
^23^192^30-Jun-2015^F^29-Oct-2015^-^104167.00^7665.00^7665.00^
^24^192^30-Jun-2015^F^29-Oct-2015^B^-104167.00^-7664.00^-7664.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
^^^*********** No Transactions Present **********^


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


