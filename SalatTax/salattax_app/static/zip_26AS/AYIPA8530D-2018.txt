^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
14-12-2018^AYIPA8530D^ACTIVE^2017-2018^2018-2019^RISHI RAJKUMAR AGRAWAL^94^SHATATARKA^AUNDH ROAD^KHADKI^PUNE^MAHARASHTRA^411020

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^SUBHASH RAMKISAN TAYAL^PNES14668D^^^^^116130.00^2400.00^2400.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^14-Jun-2017^F^27-Jul-2017^-^38130.00^800.00^800.00^
^2^192^17-May-2017^F^27-Jul-2017^-^39000.00^800.00^800.00^
^3^192^10-Apr-2017^F^27-Jul-2017^-^39000.00^800.00^800.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
^^^*********** No Transactions Present **********^


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
1^2017-18^ECS                 ^-^PAN^14120.0^476.0^12-Oct-2017^-


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


