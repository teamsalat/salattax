^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
18-12-2018^ACAPK1080G^ACTIVE^2017-2018^2018-2019^BHUPENDRA VIJAY KETKAR^1164 SADASHIV PETH^SHREE GANESH SADAN^TILAK ROAD^^PUNE^MAHARASHTRA^411030

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^STAR HEALTH AND ALLIED IN SURANCE COMPANY LIMITED^CHES24709G^^^^^20940.00^1049.00^1049.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194D^30-Jan-2018^F^17-Jun-2018^-^2526.00^127.00^127.00^
^2^194D^29-Dec-2017^F^12-Feb-2018^-^6813.00^341.00^341.00^
^3^194D^31-Aug-2017^F^13-Nov-2017^-^2858.00^143.00^143.00^
^4^194D^31-Jul-2017^F^13-Nov-2017^-^2858.00^143.00^143.00^
^5^194D^31-May-2017^F^08-Aug-2017^-^3906.00^196.00^196.00^
^6^194D^28-Apr-2017^F^08-Aug-2017^-^1979.00^99.00^99.00^

2^HOUSING DEVELOPMENT FINANCE CORPORATION (HDFC) LIMITED^MUMH00305E^^^^^32500.00^1625.00^1625.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194H^05-Jan-2018^F^03-Jun-2018^-^10000.00^500.00^500.00^
^2^194H^06-Dec-2017^F^09-Feb-2018^-^10000.00^500.00^500.00^
^3^194H^06-Jun-2017^F^05-Aug-2017^-^12500.00^625.00^625.00^

3^FINNOVATORS SOLUTIONS PRIVATE LIMITED^PNEF02000F^^^^^1483539.00^194804.00^194804.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^31-Mar-2018^F^29-May-2018^-^773472.00^5270.00^5270.00^
^2^192^31-Mar-2018^F^29-May-2018^-^749494.00^166267.00^166267.00^
^3^192^31-Mar-2018^F^29-May-2018^B^-749709.00^-166267.00^-166267.00^
^4^192^31-Mar-2018^F^29-May-2018^-^23978.00^5270.00^5270.00^
^5^192^31-Mar-2018^F^29-May-2018^B^-773472.00^-5270.00^-5270.00^
^6^192^31-Mar-2018^F^29-May-2018^-^749709.00^166267.00^166267.00^
^7^192^28-Feb-2018^F^29-May-2018^-^63400.00^2000.00^2000.00^
^8^192^31-Jan-2018^F^29-May-2018^-^63400.00^2000.00^2000.00^
^9^192^31-Dec-2017^F^05-Feb-2018^-^63400.00^2000.00^2000.00^
^10^192^30-Nov-2017^F^05-Feb-2018^-^63400.00^2000.00^2000.00^
^11^192^31-Oct-2017^F^05-Feb-2018^-^63400.00^2000.00^2000.00^
^12^194A^31-Oct-2017^F^05-Feb-2018^-^5000.00^500.00^500.00^
^13^192^30-Sep-2017^F^28-Oct-2017^-^63400.00^2000.00^2000.00^
^14^194A^29-Sep-2017^F^28-Oct-2017^-^5000.00^500.00^500.00^
^15^192^31-Aug-2017^F^28-Oct-2017^-^63400.00^2000.00^2000.00^
^16^194A^31-Aug-2017^F^28-Oct-2017^-^5000.00^500.00^500.00^
^17^194A^22-Aug-2017^F^28-Oct-2017^-^7667.00^767.00^767.00^
^18^194A^01-Aug-2017^F^28-Oct-2017^-^5000.00^500.00^500.00^
^19^192^31-Jul-2017^F^28-Oct-2017^-^63400.00^2000.00^2000.00^
^20^192^30-Jun-2017^F^31-Jul-2017^-^63400.00^2000.00^2000.00^
^21^194A^30-Jun-2017^F^31-Jul-2017^-^5000.00^500.00^500.00^
^22^192^31-May-2017^F^31-Jul-2017^-^48400.00^500.00^500.00^
^23^194A^01-May-2017^F^31-Jul-2017^-^5000.00^500.00^500.00^
^24^192^30-Apr-2017^F^31-Jul-2017^-^48400.00^500.00^500.00^
^25^194A^29-Apr-2017^F^31-Jul-2017^-^5000.00^500.00^500.00^

4^LIFE INSURANCE CORPORATION OF INDIA^PNEL03889E^^^^^119428.87^5973.00^5973.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194D^31-Mar-2018^F^05-Jun-2018^-^8393.14^420.00^420.00^
^2^194D^28-Mar-2018^F^05-Jun-2018^-^11839.06^592.00^592.00^
^3^194D^31-Jan-2018^F^05-Jun-2018^-^5800.00^290.00^290.00^
^4^194D^31-Dec-2017^F^03-Feb-2018^-^44034.18^2202.00^2202.00^
^5^194D^31-Dec-2017^F^03-Feb-2018^-^44034.18^2202.00^2202.00^
^6^194D^31-Dec-2017^F^03-Feb-2018^B^-44034.18^-2202.00^-2202.00^
^7^194D^17-Oct-2017^F^03-Feb-2018^-^9580.00^479.00^479.00^
^8^194D^05-Sep-2017^F^30-Oct-2017^-^7740.07^387.00^387.00^
^9^194D^03-Aug-2017^F^30-Oct-2017^-^9200.00^460.00^460.00^
^10^194D^03-Jul-2017^F^30-Oct-2017^-^6465.83^324.00^324.00^
^11^194D^03-Jun-2017^F^05-Aug-2017^-^7424.40^819.00^819.00^
^12^194D^03-May-2017^F^05-Aug-2017^-^8952.19^0.00^0.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
1^0021^300^62004.00^0.00^0.00^0.00^62004.00^0242465^30-Jul-2018^07356^-
2^0021^300^165000.00^0.00^0.00^0.00^165000.00^0510308^04-May-2018^05361^-


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


