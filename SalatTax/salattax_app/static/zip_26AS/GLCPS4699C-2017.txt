^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
04-12-2018^GLCPS4699C^ACTIVE^2016-2017^2017-2018^ANIKETH BALKRISHNA SHETTY^FLAT NO 9 B 4 BUILDING^MANI RATNA COMPLEX^TAWARI COLONY NR SHAMRAO^VITTAL BANK PARVATI^PUNE^MAHARASHTRA^411009

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^SQUATS FITNESS PRIVATE LIMITED^BPLS17437A^^^^^1641048.00^164106.00^164106.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194J^31-Mar-2017^F^21-May-2017^-^200600.00^20060.00^20060.00^
^2^194J^31-Mar-2017^F^21-May-2017^-^10000.00^1000.00^1000.00^
^3^194J^28-Feb-2017^F^21-May-2017^-^126835.00^12683.00^12683.00^
^4^194J^28-Feb-2017^F^21-May-2017^-^10000.00^1000.00^1000.00^
^5^194J^31-Jan-2017^F^21-May-2017^-^180237.00^18024.00^18024.00^
^6^194J^31-Jan-2017^F^21-May-2017^-^10000.00^1000.00^1000.00^
^7^194J^31-Dec-2016^F^29-Jan-2017^-^133782.00^13378.00^13378.00^
^8^194J^31-Dec-2016^F^29-Jan-2017^-^124468.00^12447.00^12447.00^
^9^194J^31-Dec-2016^F^29-Jan-2017^B^-133782.00^-13378.00^-13378.00^
^10^194J^31-Dec-2016^F^29-Jan-2017^-^10000.00^1000.00^1000.00^
^11^194J^30-Nov-2016^F^29-Jan-2017^-^137773.00^13777.00^13777.00^
^12^194J^30-Nov-2016^F^29-Jan-2017^-^10000.00^1000.00^1000.00^
^13^194J^31-Oct-2016^F^29-Jan-2017^-^88696.00^8870.00^8870.00^
^14^194J^31-Oct-2016^F^29-Jan-2017^-^10000.00^1000.00^1000.00^
^15^194J^30-Sep-2016^F^01-Nov-2016^-^142280.00^14228.00^14228.00^
^16^194J^30-Sep-2016^F^15-Nov-2017^-^43664.00^4366.00^4366.00^
^17^194J^30-Sep-2016^F^01-Nov-2016^-^10000.00^1000.00^1000.00^
^18^194J^31-Aug-2016^F^01-Nov-2016^-^146939.00^14694.00^14694.00^
^19^194J^31-Aug-2016^F^13-Dec-2017^-^10000.00^1000.00^1000.00^
^20^194J^31-Jul-2016^F^01-Nov-2016^-^107691.00^10769.00^10769.00^
^21^194J^31-Jul-2016^F^01-Nov-2016^-^103034.00^10303.00^10303.00^
^22^194J^31-Jul-2016^F^01-Nov-2016^B^-107691.00^-10769.00^-10769.00^
^23^194J^31-Jul-2016^F^01-Nov-2016^-^10000.00^1000.00^1000.00^
^24^194J^30-May-2016^F^04-Aug-2016^-^106287.00^10629.00^10629.00^
^25^194J^30-Apr-2016^F^04-Aug-2016^-^101611.00^10163.00^10163.00^
^26^194J^30-Apr-2016^F^04-Aug-2016^-^48624.00^4862.00^4862.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
^^^*********** No Transactions Present **********^


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


