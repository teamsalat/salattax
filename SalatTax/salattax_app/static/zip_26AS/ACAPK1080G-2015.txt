^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
18-12-2018^ACAPK1080G^ACTIVE^2014-2015^2015-2016^BHUPENDRA VIJAY KETKAR^1164 SADASHIV PETH^SHREE GANESH SADAN^TILAK ROAD^^PUNE^MAHARASHTRA^411030

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^STAR HEALTH AND ALLIED IN SURANCE COMPANY LIMITED^CHES24709G^^^^^14720.00^1473.00^1473.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194D^30-Dec-2014^F^18-Jan-2015^-^5715.00^572.00^572.00^
^2^194D^30-Dec-2014^F^18-Jan-2015^-^5715.00^572.00^572.00^
^3^194D^30-Dec-2014^F^18-Jan-2015^B^-5715.00^-572.00^-572.00^
^4^194D^30-Dec-2014^F^18-Jan-2015^-^5715.00^572.00^572.00^
^5^194D^30-Dec-2014^F^18-Jan-2015^B^-5715.00^-572.00^-572.00^
^6^194D^30-Dec-2014^F^18-Jan-2015^-^5715.00^572.00^572.00^
^7^194D^30-Dec-2014^F^18-Jan-2015^B^-5715.00^-572.00^-572.00^
^8^194D^30-Dec-2014^F^18-Jan-2015^-^5715.00^572.00^572.00^
^9^194D^30-Dec-2014^F^18-Jan-2015^B^-5715.00^-572.00^-572.00^
^10^194D^31-Jul-2014^F^27-Oct-2014^-^2250.00^225.00^225.00^
^11^194D^30-Jun-2014^F^19-Jul-2014^-^3510.00^351.00^351.00^
^12^194D^31-May-2014^F^19-Jul-2014^-^1595.00^160.00^160.00^
^13^194D^31-May-2014^F^19-Jul-2014^-^1595.00^160.00^160.00^
^14^194D^31-May-2014^F^19-Jul-2014^B^-1595.00^-160.00^-160.00^
^15^194D^31-May-2014^F^19-Jul-2014^-^1595.00^160.00^160.00^
^16^194D^31-May-2014^F^19-Jul-2014^B^-1595.00^-160.00^-160.00^
^17^194D^31-May-2014^F^19-Jul-2014^-^1595.00^160.00^160.00^
^18^194D^31-May-2014^F^19-Jul-2014^B^-1595.00^-160.00^-160.00^
^19^194D^31-May-2014^F^19-Jul-2014^-^1595.00^160.00^160.00^
^20^194D^31-May-2014^F^19-Jul-2014^B^-1595.00^-160.00^-160.00^
^21^194D^31-May-2014^F^19-Jul-2014^-^1650.00^165.00^165.00^

2^BAJAJ CAPITAL LIMITED^DELB00254C^^^^^623.00^62.00^62.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194H^10-Oct-2014^F^17-Jan-2015^-^623.00^62.00^62.00^

3^HOUSING DEVELOPMENT FINANCE CORPORATION (HDFC) LIMITED^MUMH00305E^^^^^57500.00^5750.00^5750.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194H^04-Feb-2015^F^23-May-2015^-^32500.00^3250.00^3250.00^
^2^194H^04-Aug-2014^F^27-Oct-2014^-^25000.00^2500.00^2500.00^

4^ICICI PRUDENTIAL MUTUAL FUND^MUMP16274G^^^^^7500.00^75.00^75.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194C^24-Apr-2014^F^18-Jul-2014^-^7500.00^75.00^75.00^

5^LIFE INSURANCE CORPORATION OF INDIA^PNEL03889E^^^^^170983.04^17098.00^17098.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194D^31-Mar-2015^F^20-May-2015^-^25924.99^2592.00^2592.00^
^2^194D^02-Feb-2015^F^20-May-2015^-^9811.28^981.00^981.00^
^3^194D^21-Jan-2015^F^20-May-2015^-^71487.07^7149.00^7149.00^
^4^194D^03-Sep-2014^F^13-Oct-2014^-^10241.50^1024.00^1024.00^
^5^194D^04-Aug-2014^F^13-Oct-2014^-^4771.34^477.00^477.00^
^6^194D^22-Jul-2014^F^13-Oct-2014^-^12445.14^1245.00^1245.00^
^7^194D^17-Jun-2014^F^19-Jul-2014^-^14396.69^1439.00^1439.00^
^8^194D^20-May-2014^F^19-Jul-2014^-^21905.03^2191.00^2191.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
1^0021^400^10.00^0.00^0.00^0.00^10.00^0004329^18-Aug-2016^58728^-
2^0021^300^18809.00^0.00^0.00^0.00^18809.00^6390340^28-Aug-2015^62436^-


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
1^2014-15^ECS                 ^-^PAN^600.0^NA^18-Dec-2014^-
2^2013-14^ECS                 ^-^PAN^3050.0^NA^19-Apr-2014^-


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
1^003 - Purchase of Mutual Fund units >= Rs. 2,00,000^HDFC MUTUAL FUND , "HDFC Ho  use" 2nd Floor, H.T. Pare  kh Marg 165-166, Backbay Reclamation, Churchgate Mumbai 190186 19 400020^04-Mar-2015^Joint^2^225000.00^Cheque^-
2^003 - Purchase of Mutual Fund units >= Rs. 2,00,000^HDFC MUTUAL FUND , "HDFC Ho  use" 2nd Floor, H.T. Pare  kh Marg 165-166, Backbay Reclamation, Churchgate Mumbai 190186 19 400020^03-Mar-2015^Joint^2^275000.00^Cheque^-


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


