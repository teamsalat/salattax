^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
07-12-2018^AOEPV7343H^ACTIVE^2017-2018^2018-2019^NIYATI BHARAT VORA^B4, FLAT # 9,^HERMES HERITAGE PHASE II,^SHASTRI NAGAR,^NAGAR ROAD,^PUNE^MAHARASHTRA^411006

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^VEDANTA LIMITED^MRIS01730B^^^^^923642.00^88370.00^88370.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^31-Mar-2018^F^12-Jun-2018^-^45216.00^21361.00^21361.00^
^2^192^31-Dec-2017^F^08-Feb-2018^-^88510.00^2541.00^2541.00^
^3^192^30-Nov-2017^F^08-Feb-2018^-^73510.00^2541.00^2541.00^
^4^192^31-Oct-2017^F^08-Feb-2018^-^73510.00^2541.00^2541.00^
^5^192^30-Sep-2017^F^06-Nov-2017^-^73510.00^2541.00^2541.00^
^6^192^31-Aug-2017^F^06-Nov-2017^-^73510.00^2541.00^2541.00^
^7^192^31-Jul-2017^F^06-Nov-2017^-^80845.00^2541.00^2541.00^
^8^192^30-Jun-2017^F^04-Aug-2017^-^276591.00^44351.00^44351.00^
^9^192^31-May-2017^F^04-Aug-2017^-^69220.00^1632.00^1632.00^
^10^192^30-Apr-2017^F^04-Aug-2017^-^69220.00^5780.00^5780.00^

2^PIRAMAL ENTERPRISES LIMITED^MUMN07675D^^^^^303823.23^0.00^0.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^31-Mar-2018^F^25-May-2018^-^97208.00^0.00^0.00^
^2^192^28-Feb-2018^F^25-May-2018^-^206615.23^0.00^0.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
1^0021^300^7233.00^0.00^217.00^0.00^7450.00^6390340^19-Aug-2018^01629^-


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
1^2017-18^ECS                 ^-^PAN^37210.0^1428.0^09-Nov-2017^-


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


