^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
21-12-2018^AARPB7024Q^ACTIVE^2014-2015^2015-2016^SATISH SURAJMAL BAGMAR^SURAJ^MATRUCHHAYA SOCIETY^NAGAR ROAD^^PUNE^MAHARASHTRA^411014

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^HDFC BANK LIMITED^MUMH03189E^^^^^13027.00^1302.70^1302.70^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^2^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^3^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^4^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^5^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^6^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^7^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^8^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^9^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^10^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^11^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^12^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^13^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^14^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^15^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^16^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^17^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^18^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^19^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^20^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^21^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^22^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^23^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^24^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^25^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^26^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^27^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^28^194A^31-Mar-2015^F^24-May-2015^-^3932.00^1302.70^1302.70^
^29^194A^31-Mar-2015^F^24-May-2015^B^-3932.00^-1302.70^-1302.70^
^30^194A^08-Jan-2015^F^24-May-2015^-^9095.00^0.00^0.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
1^0021^300^5191.00^0.00^156.00^0.00^5400.00^0004329^22-Sep-2015^06493^-
2^0021^100^48544.00^1456.00^0.00^0.00^50000.00^0004329^13-Mar-2015^20132^-
3^0021^100^48544.00^0.00^1456.00^0.00^50000.00^0013283^10-Dec-2014^05001^-
4^0021^100^50000.00^0.00^0.00^0.00^50000.00^0013283^13-Sep-2014^03308^-


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


