^Form 26AS^

File Creation Date^Permanent Account Number (PAN)^Current Status of PAN^Financial Year^Assessment Year^Name of Assessee^Address Line 1^Address Line 2^Address Line 3^Address Line 4^Address Line 5^Statecode^Pin Code
10-04-2019^BRHPB8482J^ACTIVE^2014-2015^2015-2016^FABIEN   BOILLON^A16^ANAND CHAAYA^TATA PRESS LANE P.BALLU^PRABHADEVI^MUMBAI^MAHARASHTRA^400025

^PART A - Details of Tax Deducted at Source^
Sr. No.^Name of Deductor^TAN of Deductor^^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
1^NEUTRAL GLASS AND ALLIED INDUSTRIES PRIVATE LIMITED^MUMN10438B^^^^^1468350.00^272698.00^272698.00^
^Sr. No.^Section^Transaction Date^Status of Booking^Date of Booking^Remarks^Amount Paid / Credited(Rs.)^Tax Deducted(Rs.)^TDS Deposited(Rs.)
^1^192^31-Mar-2015^F^22-May-2015^-^117250.00^21541.00^21541.00^
^2^192^31-Mar-2015^F^30-Aug-2015^-^100.00^20.00^20.00^
^3^192^28-Feb-2015^F^22-May-2015^-^178600.00^39395.00^39395.00^
^4^192^31-Jan-2015^U^22-May-2015^-^117250.00^21004.00^21004.00^
^5^192^31-Jan-2015^F^15-Jun-2015^-^117150.00^20984.00^20984.00^
^6^192^31-Jan-2015^O^15-Jun-2015^B^-117250.00^-21004.00^-21004.00^
^7^192^31-Jan-2015^O^15-Jun-2015^-^117250.00^21004.00^21004.00^
^8^192^31-Jan-2015^U^22-May-2015^B^-117250.00^-21004.00^-21004.00^
^9^192^31-Dec-2014^F^21-Jan-2015^-^117250.00^21005.00^21005.00^
^10^192^30-Nov-2014^F^21-Jan-2015^-^117250.00^21004.00^21004.00^
^11^192^31-Oct-2014^F^21-Jan-2015^-^117250.00^21005.00^21005.00^
^12^192^30-Sep-2014^F^19-Oct-2014^-^70350.00^576.00^576.00^
^13^192^30-Sep-2014^F^19-Oct-2014^-^46900.00^20428.00^20428.00^
^14^192^31-Aug-2014^F^19-Oct-2014^-^117250.00^21005.00^21005.00^
^15^192^31-Jul-2014^F^19-Oct-2014^-^117250.00^21004.00^21004.00^
^16^192^30-Jun-2014^F^19-Jul-2014^-^117250.00^21577.00^21577.00^
^17^192^31-May-2014^F^19-Jul-2014^-^117250.00^21577.00^21577.00^
^18^192^30-Apr-2014^F^19-Jul-2014^-^117250.00^21577.00^21577.00^


^PART A1 - Details of Tax Deducted at Source for 15G / 15H^
Sr. No.^Name of Deductor^TAN of Deductor^^^^Total Amount Paid / Credited(Rs.)^Total Tax Deducted(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)^
Sr. No.^Acknowledgement Number^Name of Deductor^PAN of Deductor^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART B - Details of Tax Collected at Source^
Sr. No.^Name of Collector^TAN of Collector^^^^^Total Amount Paid / Debited(Rs.)^Total Tax Collected(Rs.)^Total TCS Deposited(Rs.)
^^^*********** No Transactions Present **********^


^PART C - Details of Tax Paid (other than TDS or TCS)^
Sr. No.^Major Head^Minor Head^Tax(Rs.)^Surcharge(Rs.)^Education Cess(Rs.)^Others(Rs.)^Total Tax(Rs.)^BSR Code^Date of  Deposit^Challan Serial Number^Remarks
^^^*********** No Transactions Present **********^


^PART D - Details of Paid Refund^
Sr. No.^Assessment Year^Mode^Refund Issued^Nature of Refund^Amount of Refund(Rs.)^Interest(Rs.)^Date of Payment^Remarks
^^^*********** No Transactions Present **********^


^PART E - Details of AIR Transaction^
Sr. No.^Type of Transaction^Name of AIR Filer^Transaction Date^Single / Joint Party Transaction^Number of Parties^Amount(Rs.)^Mode^Remarks
^^^*********** No Transactions Present **********^


^PART F - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Buyer/Tenant of Property)^
Sr. No.^Acknowledgement Number^Name of Deductee^PAN of Deductee^Transaction Date^Total Transaction Amount(Rs.)^Total TDS Deposited(Rs.)^Total Amount Deposited other than TDS(Rs.)
^^^*********** No Transactions Present **********^


^PART G - TDS Defaults (Processing of Statements)^
Sr. No.^Financial Year^Short Payment^Short Deduction^Interest on  TDS Payments Default^Interest on TDS Deduction Default^Late Filing Fee u/s 234E^Interest u/s 220(2)^Total Default
^^^*********** No Transactions Present **********^


