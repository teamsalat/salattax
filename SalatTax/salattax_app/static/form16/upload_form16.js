

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('uploadForm_controller', ['$scope', function($scope,$http,CONFIG) {
	// var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
	// var TEMP=JSON.stringify(CONFIG['TEMP']).replace(/\"/g, "");
	// alert(TEMP);
	redirect_url = $scope.url;

	var client_id = 1;

	$scope.$watch("demoInput", function(){
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(response);
				if (response.client_id!=0) {
					client_id = response.client_id;
				}
			}
		});
	},100);

  	$scope.validation = function(type,value,$event) {
		if((type == "no_of_form16") && value!==undefined){
			if(value!=''){ 
				$scope.no_of_form16_error= ""; 
				return true;
			}else{
				$scope.no_of_form16_error= "Please Select Number of Form16 Upload";
				return false;
			}
		}else if((type == "no_of_form16") && value==undefined){
			$scope.no_of_form16_error= "Please Select Number of Form16 Upload";
			return false;
		}

		//Password
		if((type == "form1_passwordA" || type=="form1_passwordB" || type=="form2_passwordA" || type=="form2_passwordB" || type=="form3_passwordA" || type=="form3_passwordB") && value!==undefined){
			if(value!=''){ 
				if(type == "form1_passwordA"){
					$scope.form1_passwordA_error= ""; 
					return true;
				}else if(type == "form1_passwordB"){
					$scope.form1_passwordB_error= ""; 
					return true;
				}else if(type == "form2_passwordA"){
					$scope.form2_passwordA_error= ""; 
					return true;
				}else if(type == "form2_passwordB"){
					$scope.form2_passwordB_error= ""; 
					return true;
				}else if(type == "form3_passwordA"){
					$scope.form3_passwordA_error= ""; 
					return true;
				}else if(type == "form3_passwordB"){
					$scope.form3_passwordB_error= ""; 
					return true;
		        }
			}else{
				return true;
			}
		}else if((type == "form1_passwordA" || type=="form1_passwordB" || type=="form2_passwordA" || type=="form2_passwordB" || type=="form3_passwordA" || type=="form3_passwordB") && value==undefined){
			return true;	
		}
	}

	$scope.gotoSaveForm16 = function(user,$event) {
		var status=$scope.validation_check();
		if(status==true){
			if (switch_form16_1 == 1 && switch_form16_2 == 1 && switch_form16_3 == 1) {
				start_proceed();
				$.ajax({
					url:'/client_fin_info/',
					type:'POST',
					dataType: 'json',
					data:{'client_id':client_id },
					success: function(response){
						console.log(response);
						window.location.href = redirect_url+'/personal_information/';
					}
				});
			}
			
		}
	}

	$scope.validation_check = function() {
		var no_of_form16_status = $scope.validation("no_of_form16",$scope.user.no_of_form16);
		var count_of_form16 = $scope.user.no_of_form16;

		// alert('Checking validation'+count_of_form16);
		if(count_of_form16==1){
			if($scope.form16_1_error =='')
				return true;
			else{
				$scope.form16_1_error = 'Upload Form16 in PDF Format';
				return false;
			}
		}else if(count_of_form16==2){
			if($scope.form16_2_error =='')
				return true;
			else{
				$scope.form16_2_error = 'Upload Form16 in PDF Format';
				return false;
			}
		}else if(count_of_form16==3){
			if($scope.form16_3_error =='')
				return true;
			else{
				$scope.form16_3_error = 'Upload Form16 in PDF Format';
				return false;
			}
		}else{
			return false;
		}
	}

	$scope.reset_filename = function() {
		form16_1_no = 1;
		count_run1 = 0;
	}
	$scope.reset_filename2 = function() {
		form16_2_no = 1;
		count_run2 = 0;
	}
	$scope.reset_filename3 = function() {
		form16_3_no = 1;
		count_run3 = 0;
	}

	var form16_1_no = 1;
	var switch_form16_1 = 1;
	$scope.onLoad_form16_1 = function (e, reader, file, fileList, fileOjects, fileObj) {
		$scope.form1_passwordA_error= "";
		$scope.form1_passwordB_error= "";
		switch_form16_1 = 0;
		var file_name ='form16_Part'+form16_1_no;
		form16_1_no++;
		file_count = fileList.length;
		passwordA = $scope.user.form1_passwordA
		passwordB = $scope.user.form1_passwordB

		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		var expr = /.pdf/;
		
		var check_pdf = filetype.match(expr);
		// alert('check_pdf');
		
		if(check_pdf != null){
			$.ajax({
				url:'/file_upload/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'pan':file_name ,'type':'1'},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
					if(response['status'] == 'success' && file_count==form16_1_no-1 && count_run1==0){
						count_run1 = 1;
						$.ajax({
							url:'/form16_run_jar/',
							type:'POST',
							dataType: 'json',
							data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'1','client_id':client_id },
							success: function(response){
								console.log(response);
								output = JSON.stringify(response['output']).replace(/\"/g, "");
								full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
								switch_form16_1 = 1;
								if (output==0) {
									alert('Saved in DB');
									$.ajax({
										url:'/get_DB_data/',
										type:'POST',
										dataType: 'json',
										data:{'path':'pdf_name','client_id':client_id },
										success: function(response){
											console.log(response);
										}
									});
								}
								else if (output == 1) {
									alert('Part A missing');
								}
								else if (output == 2) {
									alert('Part B missing');
								}
								else if (output == -1) {
									alert('Something Went Wrong.');
									if(full_output.indexOf("InvalidPasswordException")!=-1){
										$scope.user.form16_1 = '';
										$scope.form1_passwordA_error= "Please Enter Correct Password";
										$scope.$apply();
									}
								}
							}
						});
					}
				}
			});
			$scope.form16_1_error= "";
			return true;
		}else{
			$scope.form16_1_error= "Upload Only PDF file.";
			return false;
		}
		$scope.$apply();
	};

	var form16_2_no = 1;
	var switch_form16_2 = 1;
	$scope.onLoad_form16_2 = function (e, reader, file, fileList, fileOjects, fileObj) {
		$scope.form2_passwordA_error= "";
		$scope.form2_passwordB_error= "";
		switch_form16_2 = 0;
		var file_name ='form16_Part'+form16_2_no;
		form16_2_no++;
		file_count = fileList.length;
		passwordA = $scope.user.form2_passwordA
		passwordB = $scope.user.form2_passwordB
		alert(passwordA);
		alert(passwordB);

		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		var expr = /.pdf/;
		
		var check_pdf = filetype.match(expr);
		// alert('Uploading2...'+pan);
		if(check_pdf != null){
			$.ajax({
				url:'/file_upload/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'pan':file_name,'type':'2'},
				success: function(response){
					console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
					if(response['status'] == 'success' && file_count==form16_2_no-1 && count_run2 ==0){
						count_run2 = 1;
						$.ajax({
							url:'/form16_run_jar/',
							type:'POST',
							dataType: 'json',
							data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'2' },
							success: function(response){
								console.log(response);
								output = JSON.stringify(response['output']).replace(/\"/g, "");
								full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
								switch_form16_2 = 1;
								if (output==0) {
									alert('Saved in DB');
									$.ajax({
										url:'/get_DB_data/',
										type:'POST',
										dataType: 'json',
										data:{'path':'pdf_name','client_id':client_id },
										success: function(response){
											console.log(response);
										}
									});
								}
								else if (output == 1) {
									alert('Part A missing');
								}
								else if (output == 2) {
									alert('Part B missing');
								}
								else if (output == -1) {
									alert('Something Went Wrong.');
									if(full_output.indexOf("InvalidPasswordException")!=-1){
										$scope.form2_passwordA_error= "Please Enter Correct Password";
										$scope.$apply();
									}
								}
							}
						});
					}
					//alert(JSON.stringify(response['status']).replace(/\"/g, ""));
				}
			});
			$scope.form16_2_error= "";
			return true;
		}else{
			$scope.form16_2_error= "Upload Only PDF file.";
			return false;
		}
	};

	var form16_3_no = 1;
	var switch_form16_3 = 1;
	$scope.onLoad_form16_3 = function (e, reader, file, fileList, fileOjects, fileObj) {
		switch_form16_3 = 0;
		var file_name ='form16_Part'+form16_3_no;
		form16_3_no++;
		file_count = fileList.length;
		passwordA = $scope.user.form3_passwordA
		passwordB = $scope.user.form3_passwordB

		var data=JSON.stringify(fileObj);
		var parse_data = JSON.parse(data);
		var filetype = parse_data['filetype'];
		var expr = /.pdf/;
		
		var check_pdf = filetype.match(expr);
		// alert('Uploading3...'+pan);
		$scope.form16_3_error= "";
		if(check_pdf != null){
			$.ajax({
				url:'/file_upload/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'pan':file_name,'type':'3'},
				success: function(response){
					// console.log(response);
					status = JSON.stringify(response['status']).replace(/\"/g, "");
					pdf_name = JSON.stringify(response['status1']).replace(/\"/g, "");
					if(status == 'success' && file_count==form16_3_no-1 && count_run3 == 0){
						count_run3 = 1;
						$.ajax({
							url:'/form16_run_jar/',
							type:'POST',
							dataType: 'json',
							data:{'path':pdf_name,'file_count':file_count,'passwordA':passwordA,'passwordB':passwordB,'company':'3' },
							success: function(response){
								console.log(response);
								output = JSON.stringify(response['output']).replace(/\"/g, "");
								full_output = JSON.stringify(response['full_output']).replace(/\"/g, "");
								switch_form16_3 = 1;
								if (output==0) {
									alert('Saved in DB');
									$.ajax({
										url:'/get_DB_data/',
										type:'POST',
										dataType: 'json',
										data:{'path':'pdf_name','client_id':client_id },
										success: function(response){
											// console.log(response);
										}
									});
								}
								else if (output == 1) {
									alert('Part A missing');
								}
								else if (output == 2) {
									alert('Part B missing');
								}
								else if (output == -1) {
									alert('Something Went Wrong.');
									if(full_output.indexOf("InvalidPasswordException")!=-1){
										$scope.form3_passwordA_error= "Please Enter Correct Password";
										$scope.$apply();
									}
								}
							}
						});
					}
				}
			});
			$scope.form16_3_error= "";
			return true;
		}else{
			$scope.form16_3_error= "Upload Only PDF file.";
			return false;
		}
	};

}]);