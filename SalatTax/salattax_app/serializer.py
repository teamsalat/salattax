from rest_framework import serializers
from .models import Modules,Module_Subscription,RFPaymentDetails,Invoice_Details,tax_variables,RFChargesDetails,Income,SalaryBreakUp,PerquisiteBreakUp,ProfitinLieuBreakUp,allowances,Property_Owner_Details,House_Property_Details,TenantDetails
import logging
stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

class RFPaymentDetailsSerializer(serializers.ModelSerializer):
	RF_Module_subscription=serializers.SerializerMethodField()
	EC_Module_subscription=serializers.SerializerMethodField()

	def get_RF_Module_subscription(self, obj):
		try:
			RF_module_id=Modules.objects.get(module_name='Return Filing').id
			obj=Module_Subscription.objects.get(module_Id=RF_module_id,module_detail1=obj.id,invoice_no__contains='-0')
			modulesub=Module_SubscriptionSerializer(obj)
			return modulesub.data
		except Exception as e:
			log.info('Serializing RF_Module_subscription :'+str(e))
			return None

	def get_EC_Module_subscription(self, obj):
		try:
			EC_module_id=Modules.objects.get(module_name='Tax Consultation').id
			obj=Module_Subscription.objects.get(module_Id=EC_module_id,invoice_no__contains='-0')
			modulesub=Module_SubscriptionSerializer(obj)
			return modulesub.data
		except Exception as e:
			log.info('Serializing EC_Module_subscription :'+str(e))
			return None

	class Meta:
		model=RFPaymentDetails
		fields=['id','R_Id','RF_Module_subscription','EC_Module_subscription']

class Module_SubscriptionSerializer(serializers.ModelSerializer):
	class Meta:
		model=Module_Subscription
		fields=['client_id','business_partner_id','module_cost','invoice_no','module_detail1']

class InvoiceDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model=Invoice_Details
		fields=['client_id','invoice_no','invoice_date','invoice_amount','payment_status']
 
class tax_variablesSerializer(serializers.ModelSerializer):
	class Meta:
		model=tax_variables
		fields=['R_Id','salary_income','house_property','business_or_profession','capital_gain','foreign_assets','taxable_income_exceed_50l']

class RFChargesSerializer(serializers.ModelSerializer):
	class Meta:
		model=RFChargesDetails
		fields=['id','return_complexity','charges']

class SalaryBreakupSerializer(serializers.ModelSerializer):
	class Meta:
		model=SalaryBreakUp
		fields=['R_Id','company_name','basic_salary','conveyance_allowance','annuity_or_pension','commuted_pension','gratuity','fees_commission','advance_of_salary','leave_encashment','others']

class PerquisiteBreakUpSerializer(serializers.ModelSerializer):
	class Meta:
		model=PerquisiteBreakUp
		fields=['R_Id','company_name','accommodation','cars_or_other_automotive','utilitiy_bills','concessional_travel','free_meals','free_education','other_benefits']

class ProfitinLieuBreakUpSerializer(serializers.ModelSerializer):
	class Meta:
		model=ProfitinLieuBreakUp
		fields=['R_Id','company_name','termination_compensation','keyman_insurance','other_profit_in_lieu_salary','other']

class allowancesSerializer(serializers.ModelSerializer):
	class Meta:
		model=allowances
		fields=['R_id','company_name','lta','hra','travel_expense_incurred','gratuity','earned_leave_encashment','other_allowance']

class IncomeSerializer(serializers.ModelSerializer):
	SalaryBreakup_Details=serializers.SerializerMethodField()
	PerquisiteBreakUp_Details=serializers.SerializerMethodField()
	ProfitinLieuBreakUp_Details=serializers.SerializerMethodField()
	allowances_Details=serializers.SerializerMethodField()

	def get_SalaryBreakup_Details(self, obj):
		try:
			obj=SalaryBreakUp.objects.get(R_Id=obj.R_Id,company_name=obj.company_name)
			salary_breakup=SalaryBreakupSerializer(obj)
			return salary_breakup.data
		except Exception as e:
			log.info('Serializing SalaryBreakup :'+str(e))
			return None


	def get_PerquisiteBreakUp_Details(self, obj):
		try:
			obj=PerquisiteBreakUp.objects.get(R_Id=obj.R_Id,company_name=obj.company_name)
			perquisite_breakUp=PerquisiteBreakUpSerializer(obj)
			return perquisite_breakUp.data
		except Exception as e:
			log.info('Serializing PerquisiteBreakUp :'+str(e))
			return None

	def get_ProfitinLieuBreakUp_Details(self, obj):
		try:
			obj=ProfitinLieuBreakUp.objects.get(R_Id=obj.R_Id,company_name=obj.company_name)
			profitinLieu_breakUp=ProfitinLieuBreakUpSerializer(obj)
			return profitinLieu_breakUp.data
		except Exception as e:
			log.info('Serializing profitinLieu_breakUp :'+str(e))
			return None

	def get_allowances_Details(self, obj):
		try:
			obj=allowances.objects.get(R_id=obj.R_Id,company_name=obj.company_name)
			allowances_breakUp=allowancesSerializer(obj)
			return allowances_breakUp.data
		except Exception as e:
			log.info('Serializing allowances_breakUp :'+str(e))
			return None

	class Meta:
		model=Income
		fields=['R_Id','company_name','LTA','Form16_HRA','Other_allowances','SalaryBreakup_Details','PerquisiteBreakUp_Details','ProfitinLieuBreakUp_Details','allowances_Details']


class PropertyOwnerDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model=Property_Owner_Details
		fields=['R_Id','Property_Id','co_owner_no','Name_of_Co_Owner','PAN_of_Co_owner','Percentage_Share_in_Property']
class TenantDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model=TenantDetails
		fields=['R_Id','Property_Id','tenant_no','tenant_name','tenant_pan']

class HousePropertyDetailsSerializer(serializers.ModelSerializer):
	co_owner_details=serializers.SerializerMethodField()
	tenant_details=serializers.SerializerMethodField()

	def get_co_owner_details(self,obj):
		try:
			obj=Property_Owner_Details.objects.filter(Property_Id=obj.Property_Id,R_Id=obj.R_Id)
			co_owner_data=PropertyOwnerDetailsSerializer(obj,many='true')
			return co_owner_data.data
		except Exception as ex:
			log.info('serializing co owner details :'+str(ex))
			return None

	def get_tenant_details(self,obj):
		try:
			obj=TenantDetails.objects.filter(Property_Id=obj.Property_Id,R_Id=obj.R_Id)
			TenantDetails_data=TenantDetailsSerializer(obj,many='true')
			return TenantDetails_data.data
		except Exception as ex:
			log.info('serializing tenant details :'+str(ex))
			return None

	class Meta:
		model=House_Property_Details
		fields=['R_Id','Property_Id','co_owner_details','tenant_details']
