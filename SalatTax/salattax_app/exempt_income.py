from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import logging
import json
from .models import SalatTaxUser, VI_Deductions, Client_fin_info1, Employer_Details
from .models import Exempt_Income, Shares_LT, Equity_LT, Personal_Details, Return_Details
#for url request
import requests
import xml.etree.ElementTree as ET

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

@csrf_exempt
def save_exempt_income(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			req_data = json.loads(data)

			client_id=req_data['client_id']
			house_rent_allownce=req_data['house_rent_allownce']
			leave_travel_allownce=req_data['leave_travel_allownce']
			# other_allownce=req_data['other_allownce']
			dividend=req_data['dividend']
			agri_income=req_data['agri_income']
			other_exempt_income=req_data['other_exempt_income']

			if house_rent_allownce=='':
				house_rent_allownce=0
			if leave_travel_allownce=='':
				leave_travel_allownce=0
			# if other_allownce=='':
				# other_allownce=0
			if dividend=='':
				dividend=0
			if agri_income=='':
				agri_income=0
			if other_exempt_income=='':
				other_exempt_income=0

			result['status']= 'start'
			R_ID = get_rid(client_id)
			if not Exempt_Income.objects.filter(R_Id=R_ID).exists():
				Exempt_Income.objects.create(
					R_Id = R_ID,
					Tax_Free_Interest = house_rent_allownce,
					LTCG_Equity= 0,
					Tax_Free_Dividend = dividend,
					Agriculture_Income_Id = agri_income,
					Other_exempt_income_Id = other_exempt_income,
				).save()
			else:
				Exempt_Income_instance = Exempt_Income.objects.get(R_Id=R_ID)
				Exempt_Income_instance.Tax_Free_Interest=house_rent_allownce
				Exempt_Income_instance.LTCG_Equity=0
				Exempt_Income_instance.Tax_Free_Dividend=dividend
				Exempt_Income_instance.Agriculture_Income_Id=agri_income
				Exempt_Income_instance.Other_exempt_income_Id=other_exempt_income
				Exempt_Income_instance.save()

			###### Update VI_Deductions #########
			personal_instance=Personal_Details.objects.get(P_Id=client_id)

			if Employer_Details.objects.filter(P_id=personal_instance).exists():
				Employer_Obj=Employer_Details.objects.filter(P_id=personal_instance)
				
				S_80C=0
				S_80CCD=0
				S_80D=0
				S_80G=0
				S_80DD=0
				S_80CCD1=0
				S_80CCD2=0

				for employer in Employer_Obj:

					employer_name=employer.Name_of_employer
					if Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).exists():
						form16 = Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).latest('time_stamp')
						###According to new update superannuation=80C
						###According to new update number_80d_self_family=80D
						S80C = form16.superannuation
						S80D = form16.number_80d_self_family

						if S80C!=None:
							S_80C+=S80C
						if S80D!=None:
							S_80D+=S80D

				if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
					VI_Deductions.objects.create(
						R_Id = R_ID,
						VI_80_C = S_80C,
						VI_80_CCD_1B= S_80CCD,
						VI_80_D = S_80D,
						VI_80_G = S_80G,
						VI_80_DD = S_80DD,
						VI_80_CCD_1 = S_80CCD1,
						VI_80_CCD_2 = S_80CCD2,
					).save()
				else:
					Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)
					Deductions_instance.VI_80_C = S_80C
					Deductions_instance.VI_80_D = S_80D
					Deductions_instance.save()



			result['status']= 'success'
			result['req_data']= req_data
		except Exception as ex:
			result['status']= 'Error : %s' % ex

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_exempt_income(request):
    if request.method=='POST':
        result={}

        try:
            client_id=request.POST.get('client_id')

            dividend, agri_income, other_exempt_income, HRA = ('',)*4
            lt_shares_gain, lt_equity_gain = (0,)*2

            R_ID = get_rid(client_id)

            if Exempt_Income.objects.filter(R_Id = R_ID).exists():
                for client in Exempt_Income.objects.filter(R_Id = R_ID):
                    dividend = (client.Tax_Free_Dividend or 0)
                    HRA = (client.Tax_Free_Interest or 0)
                    agri_income = (client.Agriculture_Income_Id or 0)
                    other_exempt_income = client.Other_exempt_income_Id or 0

            if Shares_LT.objects.filter(R_Id=R_ID).exists():
            	Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
            	lt_shares_gain= Shares_LT_instance.Expenditure

            if Equity_LT.objects.filter(R_Id=R_ID).exists():
            	Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
            	lt_equity_gain= Equity_LT_instance.Expenditure

            result['status'] = 'success'
            result['client_id'] = client_id
            result['dividend'] = dividend
            result['HRA'] = HRA
            result['agri_income'] = agri_income
            result['other_exempt_income'] = other_exempt_income
            result['LTA'] = lt_shares_gain + lt_equity_gain
        except Exception as ex:
            result['status'] = 'Error in json data: %s' % ex

        return HttpResponse(json.dumps(result), content_type='application/json')

    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_id(request,id):
	if SalatTaxUser.objects.filter(id=id).exists():
		salattaxid = SalatTaxUser.objects.filter(id=id).first()
	else:
		salattaxid = None

	return render(request,'exempt_income.html',
		{
		'id':salattaxid,
		})

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance).exists():
		Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
		R_ID = Return_Details_instance.R_id
		# for rd in Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019'):
		# 	R_ID = rd.R_id

	return R_ID


