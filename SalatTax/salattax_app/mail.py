  # for Email
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from mimetypes import guess_type
import hashlib
from getpass import getpass
from smtplib import SMTP

import boto3
from botocore.exceptions import ClientError
from email.mime.application import MIMEApplication

import traceback
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import logging
import json
from email import encoders
import smtplib
from email.encoders import encode_base64

from .models import Business_Partner,Personal_Details,Mail_Send_Log, SalatTaxUser

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

import os, sys
from .path import *

header='<html><title>Salat Tax Account</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet><style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style><body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0><table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%><tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff><tr><td style="border-top:10px solid #00A0E3"><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center><tr><td style="padding:20px 0"><table border=0 cellpadding=0 cellspacing=0 class=logo style=width:220px align=left><tr><td><a href="" target=_blank><img alt="Salat Tax" src="https:salattax.com/static/images/logo_blue.png" border=0></a></table></table><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style="width:640px;border-top:thin solid #00A0E3" align=center><tr><td style="padding:20px 0 10px;font-weight:700;font-size:18px;font-family:Verdana"class=header-title noto=""sans=""><span style=font-size:14px;font-family:verdana;font-weight:400;line-height:20px>'
footer='<br>Sincere Regards,<br><span style=color:#00A0E3>Salat Tax</span></span><tr><td style=padding-bottom:20px;text-align:center class=header-divider></table><br><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style="width:700px;border-bottom:10px solid #00A0E3"align=center><tr><td style="padding:0 0px 20px 20px"><table class=container style="width:640px;border-top:thin solid #00A0E3"><tr><td style=font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:10px class=footer-copy>If you have any queries regarding this email or mutual funds in general please contact on us at <span style=color:#00A0E3>salattax@gmail.com</span> or call us at <span style=color:#00A0E3>020 - 41225277</span>.</table></table></table></table></html>'

# class Error(Exception):
#       pass

# class typeerror(Error):
#       pass

# @csrf_exempt
# def return_filing_success_mail(request):
#       body=''
#       msg=''
#       result={}
#       try:
#             subject="Salat Tax E-filing Process Complete"
#             client_id=request.POST.get('client_id')

#             if type(client_id)=='str':
#                   raise typeerror
            
#       except typeerror:
#             msg='type error'
#       except Exception as ex:
#             msg='Error in return_filing_success_mail '+traceback.format_exc()
#             log.info(msg)
#       result['status']=msg
#       return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def return_filing_success_mail(request):
      body=''
      msg=''
      result={}
      try:
            subject="Salat Tax E-filing Process Complete"
            client_id=request.POST.get('client_id')
            xml_path=request.POST.get('xml_path')
            ITR_type=request.POST.get('ITR_type')

            log.info(xml_path)
            log.info(ITR_type)

            P_Id=Personal_Details.objects.get(P_Id=client_id).id

            return_submission_mail_partner_to_client(P_Id)

            return_submission_mail_to_partner(P_Id)

            return_submission_mail_to_salat(P_Id,ITR_type,xml_path)

            msg='mail send sucessfully'
            # if Business_Partner.objects.filter(id=b_id).exists():
            #       business_name=Business_Partner.objects.get(id=b_id).partner_business_name

            # if Personal_Details.objects.filter(P_Id=client_id).exists():
            #       client_name=Personal_Details.objects.get(P_Id=client_id).name

            # client_name=' '.join((client_name.split('|')))
            # body +='<br>Business Name :'+business_name+'.<br>'
            # body +='<br>Client Name :'+client_name+'.<br>'
            # body +='<br>Return Filing Process Completed<br>'

            """This email is to verify your email address which you provided for sign in on 
            <span style=color:#00A0E3> 
            <a href="https://salattax.com/" target="_blank">salattax.com</a> 
            </span>

            <span><a style="font-weight:600;color:#00A0E3;cursor:pointer;text-align:center;text-decoration: none;" href=# target="_blank"></a>
            </span>.

            <br>Your one time password (OTP) is <b></b>'+otp+'
            <br>If you did not request this just ignore this email.<br>"""
            
            # reciever='rukhsartamboli11@gmail.com'
           
            # if mail(reciever,subject,body)==0:
            #       msg='mail send sucessfully'

            result['status']=msg
            return HttpResponse(json.dumps(result), content_type='application/json')
      except Exception as ex:
            log.error('Error in return_filing_success_mail '+traceback.format_exc())

def mail(reciever,subject,body): 
      html=''
      flag=0
      try:      
            msg = MIMEMultipart('alternative')
            msg['Subject'] = subject
            msg['From'] = sender

            # log.info(isinstance(reciever, list))
            if isinstance(reciever, list):
                  msg['To'] = ", ".join(reciever)
            else:
                  msg['To'] = reciever

            html += header
            html += body
            html += footer
            # <img src="../static/images/logo_blue.png" alt="Salat-logo" height="100">

            # Create the body of the message (a plain-text and an HTML version).
            # Record the MIME types of both parts - text/plain and text/html.
            part2 = MIMEText(html, 'html')

            # Attach parts into message container.
            # According to RFC 2046, the last part of a multipart message, in this case
            # the HTML message, is best and preferred.

            msg.attach(part2)
            # Send the message via local SMTP server.
            mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
            mail.ehlo()
            mail.starttls()
            mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
            mail.sendmail(sender, reciever, msg.as_string()) 

      except Exception as ex:
            flag=1
            log.error('Error in sending mail '+traceback.format_exc())

      return flag

@csrf_exempt
def return_filing_failure_mail(request):
      msg=''
      body=''
      business_name=''
      client_name=''
      result={}
      try:
            subject="Salat Tax E-filing Process Failed"
            client_id=request.POST.get('client_id')
            b_id=request.POST.get('business_partner_id')
            error=request.POST.get('error')

            reciever = ['dev.salattech@gmail.com','salattax@gmail.com']
            # reciever='rukhsartamboli11@gmail.com'

            if Business_Partner.objects.filter(id=b_id).exists():
                  business_name=Business_Partner.objects.get(id=b_id).partner_business_name

            if Personal_Details.objects.filter(P_Id=client_id).exists():
                  client_name=Personal_Details.objects.get(P_Id=client_id).name

            body +='<br>Business Name :'+business_name+'.<br>'
            body +='<br>Client Name :'+client_name+'.<br>'
            body +='<br>'+error+'.<br>'
           
            if mail(reciever,subject,body)==0:
                  msg='mail send sucessfully'

            result['status']=msg
            return HttpResponse(json.dumps(result), content_type='application/json')
      except Exception as ex:
            log.error('Error in return_filing_success_mail '+traceback.format_exc())

def partner_onboard_mail_to_partner(b_id):
      log.info('partner_onboard_mail_to_partner')
      body=''
      msg=''
      result={}
      try:
            subject="Welcome! Thank you for partnering with Salat Tax"

            if Business_Partner.objects.filter(id=b_id).exists():
                  bp_inst=Business_Partner.objects.get(id=b_id)
                  partner_business_name=bp_inst.partner_business_name
                  partner_name=bp_inst.name
                  partner_city=bp_inst.city
                  partner_mobile=bp_inst.mobile
                  partner_email=bp_inst.email

            partner_name=partner_name.title()  

            sender_mail=partners_mail 
            
            body +="""<html>
            <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                  <title>Salat Investments</title>
                  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                  <style type="text/css">/* ----- Reset ----- */


            html, body {
                  margin: 0 !important;
                  padding: 0 !important;
                  height: 100% !important;
                  width: 100% !important;
                  font-family: 'Verdana', sans-serif;
            }
            * {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
            }
            div[style*="margin: 16px 0"] {
                  margin: 0 !important;
            }
            table, td {
                  mso-table-lspace: 0pt !important;
                  mso-table-rspace: 0pt !important;
            }
            table {
                  border-spacing: 0 !important;
                  border-collapse: collapse !important;
                  table-layout: fixed !important;
                  margin: 0 auto !important;
            }
            table table table {
                  table-layout: auto;
            }
            img {
                  -ms-interpolation-mode: bicubic;
                  width: 100%;
                  height: auto !important;
            }
            .yshortcuts a {
                  border-bottom: none !important;
            }
            .mobile-link--footer a, a[x-apple-data-detectors] {
                  color: inherit !important;
                  text-decoration: underline !important;
            }
             @media (max-width: 700px) {
            /* ----- Grid ----- */
            .container {
                  width: 100% !important;
            }
            .container-outer {
                  padding: 0 !important;
            }
            /* ----- Header ----- */
            .logo {
                  float: none;
                  text-align: center;
            }
            .header-title {
                  text-align: center !important;
                  font-size: 20px !important;
            }
            .header-divider {
                  padding-bottom: 0px !important;
                  text-align: center !important;
            }
            /* ----- Article ----- */
            .article-thumb, .article-title, .article-content, .article-button {
                  text-align: center !important;
                  padding-left: 15px !important;
            }
            .article-thumb {
                  padding: 30px 0 15px 0 !important;
            }
            .article-title {
                  padding: 0 0 15px 0 !important;
            }
            .article-content {
                  padding: 0 15px 0 15px !important;
            }
            .article-button {
                  padding: 20px 0 0 0 !important;
            }
            .article-button > table {
                  float: none;
            }
            /* ----- Footer ----- */
            .footer-copy {
                  text-align: center !important;
            }
            }
                  </style>
            </head>
            <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
            <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                  <tbody>
                        <tr>
                              <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                              <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                    <tbody>
                                          <tr>
                                                <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding:5px 0;"> 
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td><br />
                                                                                    <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --><!-- Container 640px -->

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                  <p style="text-align: left;"><span style="font-size:16px;">Hi """+partner_name+""",</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Thank you for partnering with Salat Tax.</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Salat Tax helps you file income tax returns in fast, secure and professional way. Currently only ITR-1 and ITR-2 are supported.</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Please follow below steps to start filing returns</span></p>

                                                                  <ol>
                                                                        <li style="text-align: left;"><span style="font-size:16px;">Visit <a href="https://salattax.com">salattax.com</a> and login using your partner credentials which you created during registration. <br />
                                                                        <br />
                                                                        <img alt="" src="https://bucket.mlcdn.com/a/440/440943/images/d6eb8037bf23396a63c52623da2ff500ee94009c.gif" style="width: 550px; height: 309px;" /></span><br />
                                                                         </li>
                                                                        <li style="text-align: left;"><span style="font-size:16px;">On successful login you will be redirected to your partner dashboard.<br />
                                                                        <br />
                                                                        <img alt="Partner Dashboard" src="https://bucket.mlcdn.com/a/440/440943/images/de36871036d59fb4062895a339af50dcf360a2bd.png" style="width: 550px; height: 309px;" /></span><br />
                                                                         </li>
                                                                        <li style="text-align: left;"><span style="font-size:16px;">Click on Add New Client and complete one time registration for the client.<br />
                                                                        <br />
                                                                        <img alt="Add New Client" src="https://bucket.mlcdn.com/a/440/440943/images/ea20cfc7ac5c276ce7aa12f61967daf5fad83a5f.gif" style="width: 550px; height: 309px;" /></span><br />
                                                                         </li>
                                                                        <li style="text-align: left;"><span style="font-size:16px;">You will have to take OTPs from client which will be triggered to his email and mobile no registered with income tax department.<br />
                                                                        <br />
                                                                        <img alt="One time Client Registration" src="https://bucket.mlcdn.com/a/440/440943/images/2ab11fa9a33c0f441a77712bb81de3ce1f088b6d.png" style="width: 550px; height: 309px;" /></span><br />
                                                                         </li>
                                                                        <li style="text-align: left;"><span style="font-size:16px;">Once done you will be redirected to the return filing portal where all the basic data will be pre populated and you can start filling the remaining data.</span><br />
                                                                         </li>
                                                                  </ol>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">In case of any queries regarding return filing please email partners@salattax.com quoting your name and clients PAN.</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Sincere regards,<br />
                                                                  Team Salat</span></p>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding: 0px 0 20px 0;">
                                                                  <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  <!--- Footer end----></td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --></td>
                                          </tr>
                                    </tbody>
                              </table>
                              <!-- Container 700px End --></td>
                        </tr>
                  </tbody>
            </table>
            <!-- Wrapper 100% End --></body>
            </html>
            """
            
            mail_sender="Partner Support | Salat Tax <"+sender_mail+">"
            reciever=partner_email
            attachfile=''
            mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)
            #if mail_status=='Success':
            if mail_status['status']==True:
                  msg='mail sent sucessfully'

            pid=''
            mail_type='ToPartner_PartnerOnboard'
            mail_sender=sender_mail
            insert_into_mail_log(mail_sender,reciever,bp_inst,pid,mail_status,mail_type)

            result['status']=msg
      except Exception as ex:
            log.error('Error in partner_onboard_mail_to_partner %s' % traceback.format_exc())

def partner_onboard_mail_to_salat(b_id):
      log.info('partner_onboard_mail_to_salat')
      body=''
      msg=''
      result={}
      try:
            subject="New Partner Added!"

            if Business_Partner.objects.filter(id=b_id).exists():
                  bp_inst=Business_Partner.objects.get(id=b_id)
                  partner_business_name=bp_inst.partner_business_name
                  partner_name=bp_inst.name
                  partner_city=bp_inst.city
                  partner_mobile=bp_inst.mobile
                  partner_email=bp_inst.email

            partner_name=partner_name.title()

            sender_mail=partners_mail
            
            body +="""<html>
            <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                  <title>Salat Investments</title>
                  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                  <style type="text/css">/* ----- Reset ----- */


            html, body {
                  margin: 0 !important;
                  padding: 0 !important;
                  height: 100% !important;
                  width: 100% !important;
                  font-family: 'Verdana', sans-serif;
            }
            * {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
            }
            div[style*="margin: 16px 0"] {
                  margin: 0 !important;
            }
            table, td {
                  mso-table-lspace: 0pt !important;
                  mso-table-rspace: 0pt !important;
            }
            table {
                  border-spacing: 0 !important;
                  border-collapse: collapse !important;
                  table-layout: fixed !important;
                  margin: 0 auto !important;
            }
            table table table {
                  table-layout: auto;
            }
            img {
                  -ms-interpolation-mode: bicubic;
                  width: 100%;
                  height: auto !important;
            }
            .yshortcuts a {
                  border-bottom: none !important;
            }
            .mobile-link--footer a, a[x-apple-data-detectors] {
                  color: inherit !important;
                  text-decoration: underline !important;
            }
             @media (max-width: 700px) {
            /* ----- Grid ----- */
            .container {
                  width: 100% !important;
            }
            .container-outer {
                  padding: 0 !important;
            }
            /* ----- Header ----- */
            .logo {
                  float: none;
                  text-align: center;
            }
            .header-title {
                  text-align: center !important;
                  font-size: 20px !important;
            }
            .header-divider {
                  padding-bottom: 0px !important;
                  text-align: center !important;
            }
            /* ----- Article ----- */
            .article-thumb, .article-title, .article-content, .article-button {
                  text-align: center !important;
                  padding-left: 15px !important;
            }
            .article-thumb {
                  padding: 30px 0 15px 0 !important;
            }
            .article-title {
                  padding: 0 0 15px 0 !important;
            }
            .article-content {
                  padding: 0 15px 0 15px !important;
            }
            .article-button {
                  padding: 20px 0 0 0 !important;
            }
            .article-button > table {
                  float: none;
            }
            /* ----- Footer ----- */
            .footer-copy {
                  text-align: center !important;
            }
            }
                  </style>
            </head>
            <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
            <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                  <tbody>
                        <tr>
                              <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                              <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                    <tbody>
                                          <tr>
                                                <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding:5px 0;"> 
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td><br />
                                                                                    <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --><!-- Container 640px -->

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                  <p style="text-align: left;"><span style="font-size:16px;">Hi Prerana,</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">A new partner with below details is added.</span></p>

                                                                  <p style="text-align: left;">Name - """+partner_name+"""<br />
                                                                  Business - """+partner_business_name+"""<br />
                                                                  City - """+partner_city+"""<br />
                                                                  Mobile - """+partner_mobile+"""<br />
                                                                  Email - """+partner_email+"""<br />
                                                                   </p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Please call up and say Hi!</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Sincere regards,<br />
                                                                  Team Salat</span></p>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding: 0px 0 20px 0;">
                                                                  <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  <!--- Footer end----></td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --></td>
                                          </tr>
                                    </tbody>
                              </table>
                              <!-- Container 700px End --></td>
                        </tr>
                  </tbody>
            </table>
            <!-- Wrapper 100% End --></body>
            </html>

            """
            
            mail_sender=sender_mail
            reciever= sender ##'salat.technologies@gmail.com'
            attachfile=''
            mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)
            #if mail_status=='Success':
            if mail_status['status']==True:
                  msg='mail sent sucessfully'

            pid=''
            mail_type='ToAdmin_PartnerOnboard'
            mail_sender=sender_mail
            insert_into_mail_log(mail_sender,reciever,bp_inst,pid,mail_status,mail_type)

            result['status']=msg
      except Exception as ex:
            log.error('Error in partner_onboard_mail_to_salat %s' % raceback.format_exc())

def partner_deposit_mail_to_partner(b_id,deposit_amount):
      log.info('partner_deposit_mail_to_partner')
      body=''
      msg=''
      result={}
      try:
            deposit_amount=str(deposit_amount)

            if Business_Partner.objects.filter(id=b_id).exists():
                  bp_inst=Business_Partner.objects.get(id=b_id)
                  partner_business_name=bp_inst.partner_business_name
                  partner_name=bp_inst.name
                  partner_city=bp_inst.city
                  partner_mobile=bp_inst.mobile
                  partner_email=bp_inst.email

            partner_name=partner_name.title()

            sender_mail=partners_mail

            subject="Deposit of Rs. "+deposit_amount+" received"
            
            body +="""<html>
            <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                  <title>Salat Investments</title>
                  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                  <style type="text/css">/* ----- Reset ----- */


            html, body {
                  margin: 0 !important;
                  padding: 0 !important;
                  height: 100% !important;
                  width: 100% !important;
                  font-family: 'Verdana', sans-serif;
            }
            * {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
            }
            div[style*="margin: 16px 0"] {
                  margin: 0 !important;
            }
            table, td {
                  mso-table-lspace: 0pt !important;
                  mso-table-rspace: 0pt !important;
            }
            table {
                  border-spacing: 0 !important;
                  border-collapse: collapse !important;
                  table-layout: fixed !important;
                  margin: 0 auto !important;
            }
            table table table {
                  table-layout: auto;
            }
            img {
                  -ms-interpolation-mode: bicubic;
                  width: 100%;
                  height: auto !important;
            }
            .yshortcuts a {
                  border-bottom: none !important;
            }
            .mobile-link--footer a, a[x-apple-data-detectors] {
                  color: inherit !important;
                  text-decoration: underline !important;
            }
             @media (max-width: 700px) {
            /* ----- Grid ----- */
            .container {
                  width: 100% !important;
            }
            .container-outer {
                  padding: 0 !important;
            }
            /* ----- Header ----- */
            .logo {
                  float: none;
                  text-align: center;
            }
            .header-title {
                  text-align: center !important;
                  font-size: 20px !important;
            }
            .header-divider {
                  padding-bottom: 0px !important;
                  text-align: center !important;
            }
            /* ----- Article ----- */
            .article-thumb, .article-title, .article-content, .article-button {
                  text-align: center !important;
                  padding-left: 15px !important;
            }
            .article-thumb {
                  padding: 30px 0 15px 0 !important;
            }
            .article-title {
                  padding: 0 0 15px 0 !important;
            }
            .article-content {
                  padding: 0 15px 0 15px !important;
            }
            .article-button {
                  padding: 20px 0 0 0 !important;
            }
            .article-button > table {
                  float: none;
            }
            /* ----- Footer ----- */
            .footer-copy {
                  text-align: center !important;
            }
            }
                  </style>
            </head>
            <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
            <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                  <tbody>
                        <tr>
                              <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                              <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                    <tbody>
                                          <tr>
                                                <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding:5px 0;"> 
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td><br />
                                                                                    <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --><!-- Container 640px -->

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                  <p style="text-align: left;"><span style="font-size:16px;">Hi """+partner_name+""",</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">We have received an amount of Rs. """+deposit_amount+""" towards deposit for income tax return filing solution. </span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">This deposit enables you to collect payment from clients in cash form. Salat's share in the return filing charges will be cut from your deposit amount and the transaction will be auto settled.</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">In case of any queries please email partners@salattax.com</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Sincere regards,<br />
                                                                  Team Salat</span></p>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding: 0px 0 20px 0;">
                                                                  <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  <!--- Footer end----></td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --></td>
                                          </tr>
                                    </tbody>
                              </table>
                              <!-- Container 700px End --></td>
                        </tr>
                  </tbody>
            </table>
            <!-- Wrapper 100% End --></body>
            </html>
            """
            
            mail_sender="Partner Support | Salat Tax <"+sender_mail+">"
            reciever=partner_email
            attachfile=''
            mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)
            #if mail_status=='Success':
            if mail_status['status']==True:
                  msg='mail sent sucessfully'

            pid=''
            mail_type='ToPartner_PartnerDeposit'
            mail_sender=sender_mail
            insert_into_mail_log(mail_sender,reciever,bp_inst,pid,mail_status,mail_type)

            result['status']=msg
      except Exception as ex:
            log.error('Error in partner_deposit_mail_to_partner %s' % traceback.format_exc())

def partner_deposit_mail_to_salat(b_id,deposit_amount):
      log.info('partner_deposit_mail_to_salat')
      body=''
      msg=''
      result={}
      try:
            deposit_amount=str(deposit_amount)

            if Business_Partner.objects.filter(id=b_id).exists():
                  bp_inst=Business_Partner.objects.get(id=b_id)
                  partner_business_name=bp_inst.partner_business_name
                  partner_name=bp_inst.name
                  partner_city=bp_inst.city
                  partner_mobile=bp_inst.mobile
                  partner_email=bp_inst.email

            partner_name=partner_name.title()

            sender_mail=partners_mail 

            subject="Deposit of Rs. "+deposit_amount+" received"
            
            body +="""<html>
            <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                  <title>Salat Investments</title>
                  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                  <style type="text/css">/* ----- Reset ----- */


            html, body {
                  margin: 0 !important;
                  padding: 0 !important;
                  height: 100% !important;
                  width: 100% !important;
                  font-family: 'Verdana', sans-serif;
            }
            * {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
            }
            div[style*="margin: 16px 0"] {
                  margin: 0 !important;
            }
            table, td {
                  mso-table-lspace: 0pt !important;
                  mso-table-rspace: 0pt !important;
            }
            table {
                  border-spacing: 0 !important;
                  border-collapse: collapse !important;
                  table-layout: fixed !important;
                  margin: 0 auto !important;
            }
            table table table {
                  table-layout: auto;
            }
            img {
                  -ms-interpolation-mode: bicubic;
                  width: 100%;
                  height: auto !important;
            }
            .yshortcuts a {
                  border-bottom: none !important;
            }
            .mobile-link--footer a, a[x-apple-data-detectors] {
                  color: inherit !important;
                  text-decoration: underline !important;
            }
             @media (max-width: 700px) {
            /* ----- Grid ----- */
            .container {
                  width: 100% !important;
            }
            .container-outer {
                  padding: 0 !important;
            }
            /* ----- Header ----- */
            .logo {
                  float: none;
                  text-align: center;
            }
            .header-title {
                  text-align: center !important;
                  font-size: 20px !important;
            }
            .header-divider {
                  padding-bottom: 0px !important;
                  text-align: center !important;
            }
            /* ----- Article ----- */
            .article-thumb, .article-title, .article-content, .article-button {
                  text-align: center !important;
                  padding-left: 15px !important;
            }
            .article-thumb {
                  padding: 30px 0 15px 0 !important;
            }
            .article-title {
                  padding: 0 0 15px 0 !important;
            }
            .article-content {
                  padding: 0 15px 0 15px !important;
            }
            .article-button {
                  padding: 20px 0 0 0 !important;
            }
            .article-button > table {
                  float: none;
            }
            /* ----- Footer ----- */
            .footer-copy {
                  text-align: center !important;
            }
            }
                  </style>
            </head>
            <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
            <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                  <tbody>
                        <tr>
                              <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                              <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                    <tbody>
                                          <tr>
                                                <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding:5px 0;"> 
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td><br />
                                                                                    <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --><!-- Container 640px -->

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                      <tbody>
                                                            <tr>
                                                                  <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                  <p style="text-align: left;"><span style="font-size:16px;">Hi Prerana,</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">We have received an amount of Rs. """+deposit_amount+""" towards deposit for income tax return filing solution. Below are the details.</span></p>

                                                                  <p style="text-align: left;">Partner Name - """+partner_name+"""<br />
                                                                  Deposit Amount - <span style="font-size:16px;">"""+deposit_amount+"""</span></p>

                                                                  <p style="text-align: left;"><span style="font-size:16px;">Sincere regards,<br />
                                                                  Team Salat</span></p>
                                                                  </td>
                                                            </tr>
                                                      </tbody>
                                                </table>

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                      <tbody>
                                                            <tr>
                                                                  <td style="padding: 0px 0 20px 0;">
                                                                  <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                        <tbody>
                                                                              <tr>
                                                                                    <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                              </tr>
                                                                        </tbody>
                                                                  </table>
                                                                  <!--- Footer end----></td>
                                                            </tr>
                                                      </tbody>
                                                </table>
                                                <!-- Container 640px End --></td>
                                          </tr>
                                    </tbody>
                              </table>
                              <!-- Container 700px End --></td>
                        </tr>
                  </tbody>
            </table>
            <!-- Wrapper 100% End --></body>
            </html>
            """
            
            mail_sender=sender_mail
            reciever= sender ##'salat.technologies@gmail.com'
            attachfile=''
            mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)
            #if mail_status=='Success':
            if mail_status['status']==True:
                  msg='mail send sucessfully'

            pid=''
            mail_type='ToAdmin_PartnerDeposit'
            mail_sender=sender_mail
            insert_into_mail_log(mail_sender,reciever,bp_inst,pid,mail_status,mail_type)

            result['status']=msg
      except Exception as ex:
            log.error('Error in partner_deposit_mail_to_salat %s' % traceback.format_exc())

def return_submission_mail_partner_to_client(p_id):
      log.info('return_submission_mail_partner_to_client')
      body=''
      msg=''
      result={}
      try:
            subject="Income Tax return submitted successfully for processing"

            msg='mail not send'
            if Personal_Details.objects.filter(id=p_id).exists():
                  Person_inst = Personal_Details.objects.get(id=p_id)
                  clinet_mail=Person_inst.email
                  clinet_name=Person_inst.name
                  salattax_user_id=Person_inst.P_Id
                  business_partner=Person_inst.business_partner_id

                  referralCode=''
                  if salattax_user_id!=None:
                        SalatTaxUser_obj=SalatTaxUser.objects.get(id=salattax_user_id)
                        referralCode=SalatTaxUser_obj.referral_code
                        clinet_mail=SalatTaxUser_obj.email
                        if referralCode==None:
                              referralCode=''

                  client_first_name=''
                  if clinet_name!=None:
                        tem_name=clinet_name.split('|')
                        client_first_name=tem_name[0]

                  if business_partner!=None and clinet_mail!=None:
                        partner_name=business_partner.name
                        partner_email=business_partner.email

                        client_first_name=client_first_name.title()  
                        sender_mail=sender 
            
                        body +="""<html>
                        <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                              <title>Salat Investments</title>
                              <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                              <style type="text/css">/* ----- Reset ----- */

                        html, body {
                              margin: 0 !important;
                              padding: 0 !important;
                              height: 100% !important;
                              width: 100% !important;
                              font-family: 'Verdana', sans-serif;
                        }
                        * {
                              -ms-text-size-adjust: 100%;
                              -webkit-text-size-adjust: 100%;
                        }
                        div[style*="margin: 16px 0"] {
                              margin: 0 !important;
                        }
                        table, td {
                              mso-table-lspace: 0pt !important;
                              mso-table-rspace: 0pt !important;
                        }
                        table {
                              border-spacing: 0 !important;
                              border-collapse: collapse !important;
                              table-layout: fixed !important;
                              margin: 0 auto !important;
                        }
                        table table table {
                              table-layout: auto;
                        }
                        img {
                              -ms-interpolation-mode: bicubic;
                              width: 100%;
                              height: auto !important;
                        }
                        .yshortcuts a {
                              border-bottom: none !important;
                        }
                        .mobile-link--footer a, a[x-apple-data-detectors] {
                              color: inherit !important;
                              text-decoration: underline !important;
                        }
                         @media (max-width: 700px) {
                        /* ----- Grid ----- */
                        .container {
                              width: 100% !important;
                        }
                        .container-outer {
                              padding: 0 !important;
                        }
                        /* ----- Header ----- */
                        .logo {
                              float: none;
                              text-align: center;
                        }
                        .header-title {
                              text-align: center !important;
                              font-size: 20px !important;
                        }
                        .header-divider {
                              padding-bottom: 0px !important;
                              text-align: center !important;
                        }
                        /* ----- Article ----- */
                        .article-thumb, .article-title, .article-content, .article-button {
                              text-align: center !important;
                              padding-left: 15px !important;
                        }
                        .article-thumb {
                              padding: 30px 0 15px 0 !important;
                        }
                        .article-title {
                              padding: 0 0 15px 0 !important;
                        }
                        .article-content {
                              padding: 0 15px 0 15px !important;
                        }
                        .article-button {
                              padding: 20px 0 0 0 !important;
                        }
                        .article-button > table {
                              float: none;
                        }
                        /* ----- Footer ----- */
                        .footer-copy {
                              text-align: center !important;
                        }
                        }
                              </style>
                        </head>
                        <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
                        <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                              <tbody>
                                    <tr>
                                          <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                                          <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                                <tbody>
                                                      <tr>
                                                            <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td style="padding:5px 0;"> 
                                                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                                    <tbody>
                                                                                          <tr>
                                                                                                <td><br />
                                                                                                <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                                          </tr>
                                                                                    </tbody>
                                                                              </table>
                                                                              </td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>
                                                            <!-- Container 640px End --><!-- Container 640px -->

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                              <p style="text-align: left;"><span style="font-size:16px;">Hi """+client_first_name+""",</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Your Income Tax return for Financial Year 2018-19 has been submitted successfully for processing. </span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;"><strong>Please note that your IT return is not filed yet.</strong></span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">What happens next?</span></p>

                                                                              <ol>
                                                                                    <li style="text-align: left;"><span style="font-size:16px;">Our team will upload your return on income tax portal in next 48 hours and you will be notified via email.</span></li>
                                                                                    <li style="text-align: left;"><span style="font-size:16px;">In case you have chosen priority return filing you get to skip the queue and your return will be filed in just 2 hours. If you do not wish to wait you can <u>Upgrade to Priority Return Filing.</u></span></li>
                                                                                    <li style="text-align: left;"><span style="font-size:16px;">Once your return is filed your ITRV will be emailed to you with instructions on how to verify the return. Once the verification is done it completes the return filing process.</span></li>
                                                                              </ol>

                                                                              <p style="text-align: left;"><span style="font-size:16px;"><span style="color:#ff6699;"><u><strong>Hard work done! Now time to earn rewards.</strong></u></span><br />
                                                                              Salat Tax is the most rewarding return filing portal.</span></p>

                                                                              <p style="text-align: center;"><span style="font-size:16px;"><img alt="" src="https://bucket.mlcdn.com/a/440/440943/images/5ee7c17a4fdefa95a20895c225ed69a4f5892a00.png" style="width: 400px; height: 207px;" /></span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Just share this link (<u>https://salattax.com/"""+referralCode+"""</u>) with your friends and start earning 20% referral reward* when your friends file returns on salattax.com. Your friends also get a 20% off* on return filing fees.</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">In case of any queries regarding your return please email to support@salattax.com quoting your PAN.</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Sincere regards,<br />
                                                                              Team Salat</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:12px;">*</span> <span style="font-size:12px;">Terms and Conditions Apply</span></p>
                                                                              </td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td style="padding: 0px 0 20px 0;">
                                                                              <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                                    <tbody>
                                                                                          <tr>
                                                                                                <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                                          </tr>
                                                                                    </tbody>
                                                                              </table>
                                                                              <!--- Footer end----></td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>
                                                            <!-- Container 640px End --></td>
                                                      </tr>
                                                </tbody>
                                          </table>
                                          <!-- Container 700px End --></td>
                                    </tr>
                              </tbody>
                        </table>
                        <!-- Wrapper 100% End --></body>
                        </html>
                        """
                        
                        mail_sender="Salat Tax <"+sender_mail+">"
                        reciever=clinet_mail
                        attachfile=''
                        mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)
                        #if mail_status=='Success':
                        if mail_status['status']==True:
                              msg='mail send sucessfully'

                        pid=Person_inst.id
                        mail_type='ToClient_ReturnSubmission'
                        mail_sender=sender_mail
                        #reciever='tushar.patil23692@gmail.com'
                        insert_into_mail_log(mail_sender,reciever,business_partner,Person_inst,mail_status,mail_type)

            else:
                  log.info('p_id not exists')
            result['status']=msg
            log.info(msg)
      except Exception as ex:
            log.error('Error in return_submission_mail_partner_to_client %s' % traceback.format_exc())

def return_submission_mail_to_partner(p_id):
      log.info('return_submission_mail_to_partner')
      body=''
      msg=''
      result={}
      try:
            msg='mail not send'
            if Personal_Details.objects.filter(id=p_id).exists():
                  log.info('Personal_Details')
                  Person_inst = Personal_Details.objects.get(id=p_id)
                  clinet_mail=Person_inst.email
                  clinet_name=Person_inst.name
                  salattax_user_id=Person_inst.P_Id
                  business_partner=Person_inst.business_partner_id

                  referralCode=''
                  if salattax_user_id!=None:
                        SalatTaxUser_obj=SalatTaxUser.objects.get(id=salattax_user_id)
                        referralCode=SalatTaxUser_obj.referral_code
                        clinet_mail=SalatTaxUser_obj.email
                        if referralCode==None:
                              referralCode=''

                  client_first_name=''
                  if clinet_name!=None:
                        temp_name=clinet_name.replace('|',' ')
                        clinetName=temp_name

                  subject="Income Tax return for "+clinetName+" is submitted successfully for processing"

                  if business_partner!=None:
                        log.info('business_partner')
                        partner_name=business_partner.name
                        partner_email=business_partner.email

                        partner_name=partner_name.title()
                        clinetName=clinetName.title()  
                        sender_mail=partners_mail
            
                        body +="""<html>
                        <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                        <title>Salat Investments</title>
                        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                        <style type="text/css">/* ----- Reset ----- */


                        html, body {
                              margin: 0 !important;
                              padding: 0 !important;
                              height: 100% !important;
                              width: 100% !important;
                              font-family: 'Verdana', sans-serif;
                        }
                        * {
                              -ms-text-size-adjust: 100%;
                              -webkit-text-size-adjust: 100%;
                        }
                        div[style*="margin: 16px 0"] {
                              margin: 0 !important;
                        }
                        table, td {
                              mso-table-lspace: 0pt !important;
                              mso-table-rspace: 0pt !important;
                        }
                        table {
                              border-spacing: 0 !important;
                              border-collapse: collapse !important;
                              table-layout: fixed !important;
                              margin: 0 auto !important;
                        }
                        table table table {
                              table-layout: auto;
                        }
                        img {
                              -ms-interpolation-mode: bicubic;
                              width: 100%;
                              height: auto !important;
                        }
                        .yshortcuts a {
                              border-bottom: none !important;
                        }
                        .mobile-link--footer a, a[x-apple-data-detectors] {
                              color: inherit !important;
                              text-decoration: underline !important;
                        }
                         @media (max-width: 700px) {
                        /* ----- Grid ----- */
                        .container {
                              width: 100% !important;
                        }
                        .container-outer {
                              padding: 0 !important;
                        }
                        /* ----- Header ----- */
                        .logo {
                              float: none;
                              text-align: center;
                        }
                        .header-title {
                              text-align: center !important;
                              font-size: 20px !important;
                        }
                        .header-divider {
                              padding-bottom: 0px !important;
                              text-align: center !important;
                        }
                        /* ----- Article ----- */
                        .article-thumb, .article-title, .article-content, .article-button {
                              text-align: center !important;
                              padding-left: 15px !important;
                        }
                        .article-thumb {
                              padding: 30px 0 15px 0 !important;
                        }
                        .article-title {
                              padding: 0 0 15px 0 !important;
                        }
                        .article-content {
                              padding: 0 15px 0 15px !important;
                        }
                        .article-button {
                              padding: 20px 0 0 0 !important;
                        }
                        .article-button > table {
                              float: none;
                        }
                        /* ----- Footer ----- */
                        .footer-copy {
                              text-align: center !important;
                        }
                        }
                              </style>
                        </head>
                        <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
                        <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                              <tbody>
                                    <tr>
                                          <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                                          <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                                <tbody>
                                                      <tr>
                                                            <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td style="padding:5px 0;"> 
                                                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                                    <tbody>
                                                                                          <tr>
                                                                                                <td><br />
                                                                                                <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                                          </tr>
                                                                                    </tbody>
                                                                              </table>
                                                                              </td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>
                                                            <!-- Container 640px End --><!-- Container 640px -->

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                              <p style="text-align: left;"><span style="font-size:16px;">Hi """+partner_name+""",</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Income Tax return for """+clinetName+""" for Financial Year 2018-19 has been submitted successfully for processing. </span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;"><strong>Please note that the IT return is not filed yet.</strong></span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">What happens next?</span></p>

                                                                              <ol>
                                                                                    <li style="text-align: left;"><span style="font-size:16px;">Our team will upload clients return on income tax portal in next 48 hours You and your client will be notified via email.</span></li>
                                                                                    <li style="text-align: left;"><span style="font-size:16px;">In case you have chosen priority return filing you get to skip the queue and your return will be filed in just 2 hours. If you do not wish to wait you can <u>Upgrade to Priority Return Filing.</u></span></li>
                                                                                    <li style="text-align: left;"><span style="font-size:16px;">Once the return is filed, clients ITRV will be emailed to you and client with instructions on how to verify the return. Once the verification is done it completes the return filing process.</span></li>
                                                                              </ol>

                                                                              <p style="text-align: left;"><span style="font-size:16px;"><font color="#ff6699"><b><u>You can earn more money from your network.</u></b></font></span></p>

                                                                              <p style="text-align: center;"><span style="font-size:16px;"><img alt="" src="https://bucket.mlcdn.com/a/440/440943/images/431f61218a801cc209b9ca0e81407581742c1f38.png" style="width: 400px; height: 207px;" /></span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Just share your partner referral link (<u>https://salattax.com/"""+referralCode+"""</u>) on facebook, linkedin etc and start earning 20% referral reward* when anyone files returns on salattax.com using your referral link. Your friends also get a 20% off* on return filing fees.</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">In case of any queries regarding your return please email to patners@salattax.com quoting your name and clients PAN.</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Sincere regards,<br />
                                                                              Team Salat</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:12px;">*</span> <span style="font-size:12px;">Terms and Conditions Apply</span></p>
                                                                              </td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td style="padding: 0px 0 20px 0;">
                                                                              <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                                    <tbody>
                                                                                          <tr>
                                                                                                <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                                          </tr>
                                                                                    </tbody>
                                                                              </table>
                                                                              <!--- Footer end----></td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>
                                                            <!-- Container 640px End --></td>
                                                      </tr>
                                                </tbody>
                                          </table>
                                          <!-- Container 700px End --></td>
                                    </tr>
                              </tbody>
                        </table>
                        <!-- Wrapper 100% End --></body>
                        </html>
                        """
                        
                        log.info('mail_sender')
                        mail_sender="Partner Support | Salat Tax <"+sender_mail+">"
                        reciever=partner_email
                        attachfile=''
                        #reciever='tushar.patil23692@gmail.com'
                        mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)
                        #if mail_status=='Success':
                        if mail_status['status']==True:
                              msg='mail sent sucessfully'

                        pid=Person_inst.id
                        mail_type='ToPartner_ReturnSubmission'
                        mail_sender=sender_mail
                        insert_into_mail_log(mail_sender,reciever,business_partner,Person_inst,mail_status,mail_type)

            else:
                  log.info('p_id not exists')

            result['status']=msg
            log.info(msg)
      except Exception as ex:
            log.error('Error in return_submission_mail_to_partner %s' % traceback.format_exc())

def return_submission_mail_to_salat(p_id,ITR_type,xml_path):
      log.info('return_submission_mail_to_salat')
      body=''
      msg=''
      result={}
      try:
            itr_type=ITR_type
            msg='mail not send'
            if Personal_Details.objects.filter(id=p_id).exists():
                  log.info('p_id exists')
                  Person_inst = Personal_Details.objects.get(id=p_id)
                  clinet_mail=Person_inst.email
                  clinet_name=Person_inst.name
                  client_pan=Person_inst.pan
                  salattax_user_id=Person_inst.P_Id
                  business_partner=Person_inst.business_partner_id

                  referralCode=''
                  if salattax_user_id!=None:
                        SalatTaxUser_obj=SalatTaxUser.objects.get(id=salattax_user_id)
                        referralCode=SalatTaxUser_obj.referral_code
                        clinet_mail=SalatTaxUser_obj.email
                        if referralCode==None:
                              referralCode=''

                  client_first_name=''
                  if clinet_name!=None:
                        temp_name=clinet_name.replace('|',' ')
                        clinetName=temp_name

                  subject="To Salat Income Tax return for "+clinetName+" is submitted successfully for processing"

                  if business_partner!=None:
                        log.info('business_partner exists')
                        partner_name=business_partner.name
                        partner_email=business_partner.email

                        partner_name=partner_name.title()
                        clinetName=clinetName.title()  
                        sender_mail=partners_mail
            
                        body +="""<html>
                        <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                        <title>Salat Investments</title>
                        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
                        <style type="text/css">/* ----- Reset ----- */


                        html, body {
                              margin: 0 !important;
                              padding: 0 !important;
                              height: 100% !important;
                              width: 100% !important;
                              font-family: 'Verdana', sans-serif;
                        }
                        * {
                              -ms-text-size-adjust: 100%;
                              -webkit-text-size-adjust: 100%;
                        }
                        div[style*="margin: 16px 0"] {
                              margin: 0 !important;
                        }
                        table, td {
                              mso-table-lspace: 0pt !important;
                              mso-table-rspace: 0pt !important;
                        }
                        table {
                              border-spacing: 0 !important;
                              border-collapse: collapse !important;
                              table-layout: fixed !important;
                              margin: 0 auto !important;
                        }
                        table table table {
                              table-layout: auto;
                        }
                        img {
                              -ms-interpolation-mode: bicubic;
                              width: 100%;
                              height: auto !important;
                        }
                        .yshortcuts a {
                              border-bottom: none !important;
                        }
                        .mobile-link--footer a, a[x-apple-data-detectors] {
                              color: inherit !important;
                              text-decoration: underline !important;
                        }
                         @media (max-width: 700px) {
                        /* ----- Grid ----- */
                        .container {
                              width: 100% !important;
                        }
                        .container-outer {
                              padding: 0 !important;
                        }
                        /* ----- Header ----- */
                        .logo {
                              float: none;
                              text-align: center;
                        }
                        .header-title {
                              text-align: center !important;
                              font-size: 20px !important;
                        }
                        .header-divider {
                              padding-bottom: 0px !important;
                              text-align: center !important;
                        }
                        /* ----- Article ----- */
                        .article-thumb, .article-title, .article-content, .article-button {
                              text-align: center !important;
                              padding-left: 15px !important;
                        }
                        .article-thumb {
                              padding: 30px 0 15px 0 !important;
                        }
                        .article-title {
                              padding: 0 0 15px 0 !important;
                        }
                        .article-content {
                              padding: 0 15px 0 15px !important;
                        }
                        .article-button {
                              padding: 20px 0 0 0 !important;
                        }
                        .article-button > table {
                              float: none;
                        }
                        /* ----- Footer ----- */
                        .footer-copy {
                              text-align: center !important;
                        }
                        }
                              </style>
                        </head>
                        <body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0"><!-- Wrapper 100% -->
                        <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                              <tbody>
                                    <tr>
                                          <td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top"><!-- Container 700px -->
                                          <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">
                                                <tbody>
                                                      <tr>
                                                            <td style="border-top: 10px solid #00A0E3;"><!-- Container 640px --><!-- Container 640px -->
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td style="padding:5px 0;"> 
                                                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">
                                                                                    <tbody>
                                                                                          <tr>
                                                                                                <td><br />
                                                                                                <img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>
                                                                                          </tr>
                                                                                    </tbody>
                                                                              </table>
                                                                              </td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>
                                                            <!-- Container 640px End --><!-- Container 640px -->

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="border-top:thin solid #009ee3;width:640px;" width="640">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td class="header-divider" style="padding-bottom:5px; text-align:center;">
                                                                              <p style="text-align: left;"><span style="font-size:16px;">Upload XML for following client.</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Filing Type - <span style="color:#FF0000;"><strong>Priority</strong></span> / Regular</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Client Name - """+clinetName+"""<br />
                                                                              Client PAN - """+client_pan+"""<br />
                                                                              ITR - """+itr_type+"""</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Partner Name - """+partner_name+"""</span></p>

                                                                              <p style="text-align: left;"><span style="font-size:16px;">Attachments: XML</span></p>
                                                                              </td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">
                                                                  <tbody>
                                                                        <tr>
                                                                              <td style="padding: 0px 0 20px 0;">
                                                                              <table class="container" style="width: 640px;border-top:thin solid #009ee3">
                                                                                    <tbody>
                                                                                          <tr>
                                                                                                <td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding tax filing or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>
                                                                                          </tr>
                                                                                    </tbody>
                                                                              </table>
                                                                              <!--- Footer end----></td>
                                                                        </tr>
                                                                  </tbody>
                                                            </table>
                                                            <!-- Container 640px End --></td>
                                                      </tr>
                                                </tbody>
                                          </table>
                                          <!-- Container 700px End --></td>
                                    </tr>
                              </tbody>
                        </table>
                        <!-- Wrapper 100% End --></body>
                        </html>
                        """
                        
                        mail_sender=sender_mail
                        reciever= sender #partner_email
                        attachfile='' #xml_path
                        attachfile=static_path+'/xml/AWYSD4534D_ITR-1_2019_N_1234.xml'
                        #reciever='tushar.patil23692@gmail.com'
                        
                        mail_status=mail_single_html(mail_sender,reciever,subject,body,attachfile)

                        #mail_status=mail_single_html_test(mail_sender,reciever,subject,body,attachfile)

                        #if mail_status=='Success':
                        if mail_status['status']==True:
                              msg='mail sent sucessfully'

                        pid=Person_inst.id
                        mail_type='ToPartner_ReturnSubmission'
                        mail_sender=sender_mail
                        #reciever= 'dnyanesw@gmail.com' #'tushar.patil23692@gmail.com'
                        insert_into_mail_log(mail_sender,reciever,business_partner,Person_inst,mail_status,mail_type)

                  else:
                        log.info('business_partner not exists')

            else:
                  log.info('p_id not exists')

            result['status']=msg
            log.info(msg)
      except Exception as ex:
            log.error('Error in return_submission_mail_to_salat %s' % traceback.format_exc())

def mail_single_html_live(mail_sender,reciever,subject,body,attachfile): 
      log.info('mail_single_html')
      html=''
      flag=0
      mail_status=''
      try:   
            msg = MIMEMultipart('alternative')
            msg['Subject'] = subject
            msg['From'] = mail_sender

            # log.info(isinstance(reciever, list))
            if isinstance(reciever, list):
                  msg['To'] = ", ".join(reciever)
            else:
                  msg['To'] = reciever

            #html += header
            html += body
            #html += footer
            # <img src="../static/images/logo_blue.png" alt="Salat-logo" height="100">

            # Create the body of the message (a plain-text and an HTML version).
            # Record the MIME types of both parts - text/plain and text/html.
            part2 = MIMEText(html, 'html')

            # Attach parts into message container.
            # According to RFC 2046, the last part of a multipart message, in this case
            # the HTML message, is best and preferred.

            msg.attach(part2)

            ##File Attach code
            filename=attachfile
            if filename!='' and filename!=None:
                  if os.path.exists(filename):
                        attachment = open(filename, "rb")
                        part = MIMEBase('application', 'octet-stream')
                        part.set_payload((attachment).read())
                        encoders.encode_base64(part)
                        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                        msg.attach(part)

            # Send the message via local SMTP server.
            mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
            mail.ehlo()
            mail.starttls()
            mail.login(aws_ses_access_key_id, aws_ses_secret_access_key)
            mail.sendmail(sender, reciever, msg.as_string()) 
            mail_status='Success'
      except Exception as ex:
            flag=1
            mail_status='Failed::: '+traceback.format_exc()
            log.error('Error in sending mail %s' % traceback.format_exc())

      return mail_status

def mail_single_html(mail_sender,reciever,subject,html,attached_file):
      #(StartCronInst,sender_mail,mail_sub)
      log.info('mail_data_upload_report_ifa')

      #mail_sender='salat.technologies@gmail.com'
      reciever='tushar.patil23692@gmail.com'

      return_result = {'status':'','status_report':''}

      try:
            # If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
            AWS_REGION = "us-east-1"

            mail_sender = mail_sender
            reciever = reciever

            msg = MIMEMultipart('alternative')
            
            # The character encoding for the email.
            CHARSET = "utf-8"

            # Create a new SES resource and specify a region.
            client = boto3.client('ses',region_name=AWS_REGION)

            # Create a multipart/mixed parent container.
            msg = MIMEMultipart('mixed')

            # Add subject, from and to lines.
            msg['From'] = mail_sender
            msg['To'] = reciever

            # The subject line for the email.
            msg['Subject'] = subject

            # Create a multipart/alternative child container.
            msg_body = MIMEMultipart('alternative')

            # Encode the text and HTML content and set the character encoding. This step is
            # necessary if you're sending a message with characters outside the ASCII range.
            ##textpart = MIMEText(BODY_TEXT.encode(CHARSET), 'plain', CHARSET)
            htmlpart = MIMEText(html.encode(CHARSET), 'html', CHARSET)

            # Add the text and HTML parts to the child container.
            ##msg_body.attach(textpart)
            msg_body.attach(htmlpart)

            # Attach the multipart/alternative child container to the multipart/mixed
            # parent container.
            msg.attach(msg_body)

            # Define the attachment part and encode it using MIMEApplication.
            if attached_file!='':
                  if os.path.exists(attached_file): 
                        attachment = MIMEApplication(open(attached_file, 'rb').read())

                        # Add a header to tell the email client to treat this part as an attachment,
                        # and to give the attachment a name.
                        attachment.add_header('Content-Disposition','attachment',filename=os.path.basename(attached_file))

                        # Add the attachment to the parent container.
                        msg.attach(attachment)

            try:
                  #Provide the contents of the email.
                  client = boto3.client('ses',aws_access_key_id=aws_ses_access_key_id, aws_secret_access_key=aws_ses_secret_access_key,region_name=aws_ses_region_name)
                  response = client.send_raw_email(
                        Source=mail_sender,
                        Destinations=[reciever],
                        RawMessage={'Data':msg.as_string(),},
                  )

            # Display an error if something goes wrong.
            except ClientError as e:
                  ex = e.response['Error']['Message']
                  ex=str(ex)
                  #log.info(ex)
                  log.error('Error in sending mail_single_html_test : '+traceback.format_exc())

                  return_result['status']=False
                  return_result['status_report']=e.response['Error']['Message']
                  log.info(return_result)
                  return return_result
            else:
                  log.info("Bulk Upload Report Email sent to IFA! Message ID:"),
                  log.info(response['MessageId'])

                  return_result['status']=True
                  log.info(return_result)
                  return return_result

      except Exception as ex:
            ex = str(ex)
            log.error('Error in sending mail_single_html_test : '+traceback.format_exc())
            return_result['status']=False
            return_result['status_report']=ex
            log.info(return_result)
            return return_result

def insert_into_mail_log(mail_sender,reciever,bp_inst,pid,mail_status,mail_type):
      log.info('insert_into_mail_log')
      try:

            if mail_status['status']==True:
                  status='Success'
            else:
                  status='Failed::: '+mail_status['status_report']

            if 'ToClient' in mail_type:
                  MailSendLogInstance=Mail_Send_Log(business_partner_id=bp_inst,
                        p_id=pid,
                        sender_mail=mail_sender,
                        receiver_mail=reciever,
                        status=status,
                        mail_type=mail_type)
                  MailSendLogInstance.save()
            elif 'ToPartner' in mail_type or 'ToAdmin' in mail_type:
                  MailSendLogInstance=Mail_Send_Log(business_partner_id=bp_inst,
                        sender_mail=mail_sender,
                        receiver_mail=reciever,
                        status=status,
                        mail_type=mail_type)
                  MailSendLogInstance.save()

      except Exception as ex:
            log.error('Error in insert_into_mail_log %s' % traceback.format_exc())