from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import logging
import json
import requests
import traceback
from .path import *
from .models import Personal_Details,Return_Details,Business_Partner,RFPaymentDetails

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)
def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=int(client_id))
	if Return_Details.objects.filter(P_id=personal_instance).exists():
		Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
		R_ID = Return_Details_instance.R_id
	return R_ID

@csrf_exempt
def chooseFillingTypeFun(request):
	result={}
	res=0
	try:
		client_id=request.POST.get('client_id')
		R_Id=get_rid(client_id)
		b_id=request.POST.get('b_id')
		expert_review_charge=request.POST.get('expert_review_charge')

		business_partner_instance=Business_Partner.objects.get(id=b_id)
		if RFPaymentDetails.objects.filter(R_Id=R_Id).exists():
			RFpaymentdetails_instance=RFPaymentDetails.objects.get(R_Id=R_Id)
			RFpaymentdetails_instance.business_partner_id=business_partner_instance
			RFpaymentdetails_instance.expert_review_charge=expert_review_charge
			RFpaymentdetails_instance.save()
			log.info('RF Payment Entry Updated')
		elif not RFPaymentDetails.objects.filter(R_Id=R_Id).exists():
			RFPaymentDetails.objects.create(
				R_Id=R_Id,
				business_partner_id=business_partner_instance,
				expert_review_charge=expert_review_charge,

			).save()
			log.info('RF Payment Entry Created')
	except Exception as ex:
		result['data']='Error in RF payment details chooseFillingType api '+traceback.format_exc()
	result['status']=res
	return HttpResponse(json.dumps(result),content_type="application/json")
