#!/usr/bin/env bash
from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone

import os, sys
from PIL import Image
# Python logging package
import logging
import json
import webdriver
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time
from io import BytesIO
from StringIO import StringIO
# import subprocess
from subprocess import Popen, PIPE, STDOUT
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import requests
from django.core.urlresolvers import reverse
import os.path
from zipfile import ZipFile
import zipfile
import datetime
import re
import xlwt
import xlrd

import thread
import time

from django.db.models import Avg, Max, Min, Sum
from datetime import datetime
from .models import Business_Partner,salatclient,temp_ITR_value,ERI_user,salat_registration,Return_Details
from .models import tds_tcs,tax_paid,refunds,Personal_Details,Address_Details,user_tracking,Bank_Details
from .models import Original_return_Details
import traceback
from dateutil.parser import parse
stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)
from .path import *

def driver_call():
    chrome_options = webdriver.ChromeOptions()
    downloadPath = selenium_path+'/zip_26AS'
    chrome_options.add_argument('--headless')
    prefs = {'download.default_directory' : downloadPath,
    "download.prompt_for_download": False,
    "download.directory_upgrade": True,
    "safebrowsing.enabled": True}
    chrome_options.add_experimental_option('prefs', prefs)

    driver = webdriver.Chrome(executable_path=chrome_executable_path, chrome_options=chrome_options)
    # driver = webdriver.Chrome(executable_path='/usr/lib/chromium-browser/chromedriver', chrome_options=chrome_options,
      # service_args=['--verbose', '--log-path=/tmp/chromedriver1.log'])

    driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
    command_result = driver.execute("send_command", params)
    # chromedriver=2.35.528139
    # platform=Linux 4.4.0-1062-aws x86_64

    return driver

def captcha_text_read(captch_img_path):
	# where jar is running copy tessdata folder and set TESSDATA_PREFIX
	# export TESSDATA_PREFIX=/home/salattax/SalatTax/tessdata
	# echo $TESSDATA_PREFIX
	return_captcha = ''
	p = Popen(['java', '-jar','captcha_arg.jar',captch_img_path], stdout=PIPE, stderr=STDOUT)
	log.info(p.stdout)
	return_captcha = ''
	for line in p.stdout:
		return_captcha = line
	# return subprocess.call(['java', '-jar', '/media/salat/DATA D/Ubuntu_related_file/captcha_arg.jar','/home/salat/Downloads/images_captcha/screenshot.png'])
	return return_captcha

def captch_multiple_eri(driver,captch_img_path,eri_password):
    try:
        captcha_id = driver.find_element_by_id("captchaImg")
        location = captcha_id.location
        size = captcha_id.size
        img = driver.get_screenshot_as_png()
        img = Image.open(StringIO(img))
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
        img.save(captch_img_path)

        password = driver.find_element_by_id("Login_password")
        password.send_keys(eri_password)

        captcha_text_1 = captcha_text_read(captch_img_path)
        driver.find_element_by_id("Login_captchaCode").send_keys(captcha_text_1)
        
        time.sleep(1)
        try:
            driver.find_element_by_class_name('error')
            error = 1
            return error
        except Exception as ex:
            error = 0
            return error
    except Exception as ex:
        error = 0
        return error

def screenshot(driver,f_name):
    captch_img_path1 = selenium_path+'/images_captcha/'
    id = driver.find_element_by_tag_name("body")
    location = id.location
    size = id.size
    img = driver.get_screenshot_as_png()
    img = Image.open(StringIO(img))
    left = location['x']
    top = location['y']
    right = location['x'] + size['width']
    bottom = location['y'] + size['height']
    img = img.crop((int(left), int(top), int(right), int(bottom)))
    img.save(captch_img_path1+f_name+'.png')

def captch_ITR_download(driver,captch_img_path,id):
    try:
        driver.find_element_by_id(id).clear()
        captcha_id = driver.find_element_by_id("captchaImg")
        location = captcha_id.location
        size = captcha_id.size
        img = driver.get_screenshot_as_png()
        img = Image.open(StringIO(img))
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
        img.save(captch_img_path)

        captcha_text_2 = captcha_text_read(captch_img_path)
        driver.find_element_by_id(id).send_keys(captcha_text_2)
        time.sleep(1)
        try:
            element = driver.find_elements_by_class_name('error')
            for line in element:
                check_element = line.text
                if (check_element.find('Invalid Code') == 0):
                    error = 1
                else:
                    error = 0
            return error
        except Exception:
            error = 0
            return error
    except Exception:
        error = 0
        return error

def captch_add_client(driver,captch_img_path):
    driver.find_element_by_class_name('refreshImg').click()
    try:
        driver.find_element_by_id("AddClientDetails_captchaCode").clear()
        captcha_id = driver.find_element_by_id("captchaImg")
        location = captcha_id.location
        size = captcha_id.size
        img = driver.get_screenshot_as_png()
        img = Image.open(StringIO(img))
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
        img.save(captch_img_path)

        captcha_text_2 = captcha_text_read(captch_img_path)
        driver.find_element_by_id("AddClientDetails_captchaCode").send_keys(captcha_text_2)
        try:
            element = driver.find_elements_by_class_name('error')
            for line in element:
                check_element = line.text
                if (check_element.find('Invalid Code') == 0):
                    error = 1
                else:
                    error = 0
                if (check_element.find(' PAN is not registered ') >= 0):
                    error = 'PAN not registered'
            return error
        except Exception:
            error = 0
            return error
    except Exception:
        error = 0
        return error

def captch_view_form26AS(driver,captch_img_path):
    try:
        driver.find_element_by_id("View26AS_captchaCode").clear()
        captcha_id = driver.find_element_by_id("captchaImg")
        location = captcha_id.location
        size = captcha_id.size
        img = driver.get_screenshot_as_png()
        img = Image.open(StringIO(img))
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']
        img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
        img.save(captch_img_path)
        time.sleep(1)

        captcha_text_2 = captcha_text_read(captch_img_path)
        driver.find_element_by_id("View26AS_captchaCode").send_keys(captcha_text_2)
        try:
            element = driver.find_elements_by_class_name('error')
            for line in element:
                check_element = line.text
                if (check_element.find('Invalid Code') == 0):
                    error = 1
                else:
                    error = 0
                if (check_element.find(' PAN is not registered ') >= 0):
                    error = 'PAN not registered'
            return error
        except Exception:
            error = 0
            return error
    except Exception:
        error = 0
        return error

def login_ITR(driver,eri_username,eri_password):
    captch_img_path = selenium_path+'/images_captcha/screenshot.png'
    driver.get("https://www.incometaxindiaefiling.gov.in/home")

    # close popup
    try:
        driver.find_element_by_class_name('ui-icon-closethick').click()
    except Exception:
        log.info('Close Tick Not found in home page')

    # click login
    try:
        login_here = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section[1]/div/app-register-options/ul/app-register[1]/li/h1/input')[0]
        login_here.click()
    except Exception:
        error = 0
    try:
        login_here = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section[1]/div/app-register-options/ul/app-register[1]/li/h1/input')[0]
        driver.execute_script("arguments[0].click();", login_here)
    except Exception:
        error = 0
    try:
        login_here = driver.find_elements_by_xpath('//*[@id="header"]/div[1]/div[2]/div/div/a[1]')[0]
        login_here.click()
    except Exception:
        error = 0

    time.sleep(1)
    username = driver.find_element_by_id("Login_userName")
    password = driver.find_element_by_id("Login_password")

    username.send_keys(eri_username)
    password.send_keys(eri_password)
    try:
        driver.find_element_by_xpath('/html/body/div/div/table/tbody/tr[1]/td[2]/strong')
        return 2
    except Exception:
        error = 0

    error = 1
    count = 1
    while error == 1:
        error = captch_multiple_eri(driver,captch_img_path,eri_password)
        screenshot(driver,'test'+str(count))
        count = count + 1

    log.info('Count of Captcha try : '+ str(count) )
    already_login = 1
    try:
        time.sleep(1)
        continue_login = driver.find_elements_by_xpath('//*[@id="ForcedLogin"]/table[2]/tbody/tr/td[1]/input')[0]
        driver.execute_script("arguments[0].click();", continue_login)
        already_login = 1
    except Exception:
        already_login = 0
    try:
        continue_login1 = driver.find_elements_by_xpath('//*[@id="formContainerDialog1"]/table/tbody/tr[2]/td/div/input')[0]
        driver.execute_script("arguments[0].click();", continue_login1)
        already_login = 1
    except Exception:
        already_login = 0

    try:
        skip = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_2"]')[0]
        driver.execute_script("arguments[0].click();", skip)
    except Exception:
        already_login = 0

    time.sleep(1)
    screenshot(driver,'login')
    return 0

def unique(list1):
    # intilize a null list
    unique_list = []
    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            if x.startswith( '1' ):
                unique_list.append(x)
            if x.startswith( '2' ):
                unique_list.append(x)
    
    return unique_list

def get_partA_old(worksheet,start,end,pan,part,year):
    section_list = []
    for curr_row_tan1 in xrange(start+2,end):
        if worksheet.cell(curr_row_tan1,2).value != '':
            section_list.append(worksheet.cell(curr_row_tan1,2).value)
    section_list = unique(section_list)

    for section in section_list:
        tan1_list = []
        Deductor_Collector_Name = worksheet.cell(start,1).value
        TAN_PAN = worksheet.cell(start,2).value
        no_of_transaction = 0
        amount_paid = 0.0
        tax_deducted = 0.0
        tds_tcs_deposited = 0.0
        DB_entry = 0
        for curr_row_tan1 in xrange(start+2,end):
            if worksheet.cell(curr_row_tan1,2).value == section:
                DB_entry = 1
                # log.info( len(worksheet.cell(curr_row_tan1,7).value) )
                if(len(worksheet.cell(curr_row_tan1,7).value) > 4):
                    no_of_transaction += 1
                    # log.info(worksheet.cell(curr_row_tan1,9).value)
                    amount_paid += float( worksheet.cell(curr_row_tan1,7).value )
                    tax_deducted += float( worksheet.cell(curr_row_tan1,8).value )
                    tds_tcs_deposited += float( worksheet.cell(curr_row_tan1,9).value )

        if DB_entry == 1:
            if not tds_tcs.objects.filter(R_Id=pan,part=part,TAN_PAN=TAN_PAN,section=section,year=year).exists():
                log.info('part '+part+' saved in DB for '+pan)
                tds_tcs.objects.create(
                    R_Id=pan,
                    part=part,
                    Deductor_Collector_Name=Deductor_Collector_Name,
                    TAN_PAN=TAN_PAN,
                    section=section,
                    no_of_transaction=no_of_transaction,
                    amount_paid=amount_paid,
                    tax_deducted=tax_deducted,
                    tds_tcs_deposited=tds_tcs_deposited,
                    year=year,
                ).save()
    return 1

def get_partA(worksheet,start,end,pan,part,year,R_ID):
    section_list = []
    for curr_row_tan1 in xrange(start+2,end):
        if worksheet.cell(curr_row_tan1,2).value != '':
            section_list.append(worksheet.cell(curr_row_tan1,2).value)
    section_list = unique(section_list)

    for section in section_list:
        tan1_list = []
        Deductor_Collector_Name = worksheet.cell(start,1).value
        TAN_PAN = worksheet.cell(start,2).value
        no_of_transaction = 0
        amount_paid = 0.0
        tax_deducted = 0.0
        tds_tcs_deposited = 0.0
        DB_entry = 0
        for curr_row_tan1 in xrange(start+2,end):
            if worksheet.cell(curr_row_tan1,2).value == section:
                DB_entry = 1
                # log.info( len(worksheet.cell(curr_row_tan1,7).value) )
                if(len(worksheet.cell(curr_row_tan1,7).value) > 4):
                    no_of_transaction += 1
                    # log.info(worksheet.cell(curr_row_tan1,9).value)
                    amount_paid += float( worksheet.cell(curr_row_tan1,7).value )
                    tax_deducted += float( worksheet.cell(curr_row_tan1,8).value )
                    tds_tcs_deposited += float( worksheet.cell(curr_row_tan1,9).value )

        if DB_entry == 1:
            if not tds_tcs.objects.filter(R_Id=R_ID,part=part,TAN_PAN=TAN_PAN,section=section,year=year).exists():
                log.info('part '+part+' saved in DB for '+str(pan)+' R_Id '+str(R_ID)+' year '+str(year) )
                tds_tcs.objects.create(
                    R_Id=R_ID,
                    part=part,
                    Deductor_Collector_Name=Deductor_Collector_Name,
                    TAN_PAN=TAN_PAN,
                    section=section,
                    no_of_transaction=no_of_transaction,
                    amount_paid=amount_paid,
                    tax_deducted=tax_deducted,
                    tds_tcs_deposited=tds_tcs_deposited,
                    year=year,
                ).save()
    return 1

# Define a function for the thread
def server_side_processing( pan,dob,client_id,b_id, delay):
    log.info('server_side_processing')
    try:
        # log.info('thread')
        # log.info(thread.active_count())
        log.info('curl '+server_curl+'/multiple_eri_test/'+pan+'='+dob+'='+client_id+'='+b_id)
        os.system('curl '+server_curl+'/multiple_eri_test/'+pan+'='+dob+'='+client_id+'='+b_id)
        count = 0
        while count < 1:
            time.sleep(delay)
            count += 1
            log.info( "%s: %s" % ( pan, time.ctime(time.time()) ) )
    except Exception as ex:
        log.error('Error in server_side_processing : %s' % traceback.format_exc())

@csrf_exempt
def multiple_eri_test(request,data):
    log.info('multiple_eri_test')
    result = {}
    try:
        log.info(data)
        pan = data.split('=')[0]
        dob = data.split('=')[1]

        dob = dob.replace('@','/')
        dob = dob.replace('@','/')
        client_id = data.split('=')[2]
        b_id = data.split('=')[3]
        log.info(b_id)
        result['status']="start"
        driver = driver_call()
        multiple_eri_ITR1 = multiple_eri_ITR(driver,pan,dob,client_id,b_id,request)
        #multiple_eri_ITR1 = iTR_V_Acknowledgement(driver,pan,dob,client_id,b_id,request)
        # multiple_eri_ITR1 = excel_to_db(pan,37)
        if multiple_eri_ITR1 == 'error':
            log.info(multiple_eri_ITR1)
            if salatclient.objects.filter(PAN=pan).exists():
                for sc in salatclient.objects.filter(PAN=pan):
                    if sc.ERI_id is None:
                        eri_username = ''
                    else:
                        eri_username = sc.ERI_id
                    if eri_username != '':
                        if ERI_user.objects.filter(status='active',user_id=eri_username).exists():
                            ERI_user_instance = ERI_user.objects.get(status='active',user_id=eri_username)
                            ERI_user_instance.status = 'inactive'
                            ERI_user_instance.user_pan = ''
                            ERI_user_instance.save()
            multiple_eri_ITR1 = multiple_eri_ITR(driver,pan,dob,client_id,b_id,request)
            #multiple_eri_ITR1 = iTR_V_Acknowledgement(driver,pan,dob,client_id,b_id,request)
        
        log.info(multiple_eri_ITR1)

        driver.close()
        result['status'] = multiple_eri_ITR1
        return HttpResponse(json.dumps(result), content_type='application/json')
    except Exception as ex:
        result['status']='error'
        log.error('Error in multiple_eri_test : %s' % traceback.format_exc())
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def multiple_eri(request):
    log.info('multiple_eri1')
    result = {}
    if request.method=='POST':
        client_id = request.POST.get('client_id')
        b_id=request.POST.get('b_id')
        try:
            pan = request.POST.get('pan')
            dob = request.POST.get('dob')
            option = request.POST.get('option')
            trigger_otp = 0
            salat_registration1 = ''

            if ERI_user.objects.filter(status='active').exists():
                for eri in ERI_user.objects.filter(status='active'):
                    time_diff = timezone.now() - eri.date_time
                    if time_diff.seconds / 60 > 10 :
                        ERI_user_instance = ERI_user.objects.get(eri_pan=eri.eri_pan)
                        ERI_user_instance.status = 'inactive'
                        ERI_user_instance.save()
                        log.info(eri.eri_pan + " ERI_user Status changed to 'inactive'")

            if option == 'salatclient':
                dob = dob.replace('/','@')
                dob = dob.replace('/','@')
                # MYDIR = os.path.dirname(__file__)
                # os.system('curl https://salattax.com/multiple_eri_test/'+pan+'='+dob+'='+client_id)
                thread.start_new_thread( server_side_processing, (pan,dob,client_id,b_id, 2, ) )
                # give Execute Permission to above file
                dob = dob.replace('@','/')
                dob = dob.replace('@','/')
            else:
                driver = driver_call()
                salat_registration1 = salat_registration_fun(driver,pan,dob,client_id,b_id)
                driver.close()
                
            result['pan'] = pan
            result['dob'] = dob
            result['option'] = option
            result['status'] = salat_registration1
        except Exception as ex:
            log.error('Error in multiple_eri : %s' % traceback.format_exc())
            user_error_tracking(client_id,'multiple ERI : %s' % ex,request)

            result['status'] = 'Error multiple ERI : %s' % ex
            log.error('Error in : '+traceback.format_exc())
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status'] = "Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def inactive_eri_user(pan):
    log.info('inactive_eri_user')
    try:
        if ERI_user.objects.filter(user_pan=pan).exists():
            ERI_user.objects.filter(user_pan=pan).update(user_pan='-',status='inactive')
            log.info('inactive_eri_user success')
    except Exception as ex:
        log.error('Error in inactive_eri_user : %s' % traceback.format_exc())

def multiple_eri_ITR(driver,pan,dob,client_id,b_id,request):
    log.info('multiple_eri_ITR')
    eri_username = ''
    eri_password = ''

    try:
        log.info(b_id)
        business_partner_instance=Business_Partner.objects.get(id=b_id)
        if not salat_registration.objects.filter(pan=pan).exists():
            salat_registration.objects.create(
                pan = pan,
                date_time = datetime.now(),
                login = 0,
            ).save()
        else:
            salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
            salat_registration_instance.date_time = datetime.now()
            salat_registration_instance.save()
        # else:
            # salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
            # time_diff = timezone.now() - salat_registration_instance.date_time
            # if time_diff.seconds / 60 > 10 :
            #   # difference is greater than 10 minutes
            #   salat_registration.objects.create(
            #       pan = pan,
            #       date_time = datetime.now(),
            #       login = 0,
            #   ).save()

        if salatclient.objects.filter(PAN=pan).exists():
            for sc in salatclient.objects.filter(PAN=pan):
                if sc.ERI_id is None:
                    eri_username = ''
                else:
                    eri_username = sc.ERI_id
                if eri_username != '':
                    if ERI_user.objects.filter(status='inactive',user_id=eri_username).exists():
                        for eri in ERI_user.objects.filter(status='inactive',user_id=eri_username):
                            eri_username = eri.user_id
                            eri_password = eri.password
                    else:
                        return 'No Free ERI'
                else:
                    return 'No ERI Assigned to the PAN'
        else:
            return 'PAN Not registered as Salat Client'

        # eri_username = 'ERIA101868'
        # eri_password = "Derivatives4$"
        log.info('Start Multiple_ITR : '+eri_username)

        ERI_user_instance = ERI_user.objects.get(user_id=eri_username)
        ERI_user_instance.status = 'active'
        ERI_user_instance.user_pan = pan
        ERI_user_instance.date_time = datetime.now()
        ERI_user_instance.save()

        captch_img_path = selenium_path+'/images_captcha/download_ITR.png'
        login_status = 2
        while login_status == 2:
            login_status = login_ITR(driver,eri_username,eri_password)
            log.info(login_status)

        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        salat_registration_instance.login = 1
        salat_registration_instance.save()

        time.sleep(1)
        try:
            my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
            hover = ActionChains(driver).move_to_element(my_account)
            hover.perform()
            time.sleep(1)
        except Exception as ex:
            log.error('Error my_account : %s' +traceback.format_exc())
            error = 0
            return 'error'
        # screenshot(driver,'test')
        try:
            add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl[2]/dt/a')[0]
            driver.execute_script("arguments[0].click();", add_client)
            PAN = driver.find_element_by_id("ViewReturnsItdHdEri_panNo")
            # Assuming PAN IS valid
            PAN.send_keys(pan)
        except Exception as ex:
            log.error('Error add_client : %s' +traceback.format_exc())
            error = 0
            return 'error'
        webdriver.ActionChains(driver).move_to_element(PAN).send_keys(Keys.ENTER).send_keys(Keys.ENTER).perform()
        pan_exist = 1
        no_of_ITR = 0
        try:
            element = driver.find_elements_by_class_name('error')
            for line in element:
                check_element = line.text
                if (check_element.find('This PAN is not added as a Client') == 0):
                    pan_exist = 0
        except Exception:
            error = 0

        result={}
        pan_status = ''
        dob_status = ''
        otp_status = ''
        result['status'] = 'pending'

        salatclient_instance = salatclient.objects.get(PAN=pan)
        if pan_exist == 0:
            result['pan_exist'] = 'This PAN is not added as Salat Client.'
            salat_registration_instance.is_salat_client = 'Not Salat Client'
            salat_registration_instance.save()
        else:
            result['pan_exist'] = 'This PAN is added as Salat Client.'
            salat_registration_instance.is_salat_client = 'Salat Client'
            salat_registration_instance.save()
            salatclient_instance.Is_salat_client=1
            salatclient_instance.Is_ERI_reg=1
            salatclient_instance.save()

        log.info('PAN : '+ pan + ' '+ eri_username)
        log.info('pan_exist : '+str(pan_exist) )
        if pan_exist == 1:
            d,m,y=dob.split("/")
            pan1_dob=y+'-'+m+'-'+d
            if not Personal_Details.objects.filter(P_Id=client_id).exists():
                Personal_Details.objects.create(
                    P_Id=client_id,
                    business_partner_id=business_partner_instance,
                    pan=pan,
                    pan_type='PAN',
                    dob= pan1_dob
                ).save()
                log.info(pan + " Entry Created")
            else:
                pd_instance=Personal_Details.objects.get(P_Id=client_id)
                pd_instance.pan = pan
                pd_instance.business_partner_id=business_partner_instance
                pd_instance.pan_type='PAN'
                pd_instance.dob= pan1_dob
                pd_instance.save()
                log.info(pan + " Entry Updated")
            error = 1
            run_itr = 0
            run_26as = 0
            # log.info(salat_registration_instance.processed_itr_count)
            # log.info(salat_registration_instance.downloaded_itr_count)
            if(salat_registration_instance.processed_itr_count is None):
                log.info('if 1')
                run_itr = 1
            else:
                if(int(salat_registration_instance.processed_itr_count)<5):
                    log.info(salat_registration_instance.processed_itr_count)
                    if(int(salat_registration_instance.processed_itr_count)!=int(salat_registration_instance.downloaded_itr_count) ):
                        run_itr = 1
            if(salat_registration_instance.processed_26as_count is None):
                log.info('if 1')
                run_26as = 1
            else:
                log.info('else')
                if(int(salat_registration_instance.processed_26as_count)<5):
                    run_26as = 1
                    if(salat_registration_instance.processed_26as_count!=salat_registration_instance.downloaded_26as_count):
                        log.info('else if if')
                        run_26as = 1

            # log.info(salat_registration_instance.processed_26as_count)
            # log.info(salat_registration_instance.downloaded_26as_count)
            log.info(run_itr)
            log.info(run_26as)
            
            if run_itr == 1 :
                log.info('ITR DOWNLOAD WHILE LOOP ...')
                while error == 1:
                    error = captch_ITR_download(driver,captch_img_path,'ViewReturnsItdHdEri_captchaCode')
                log.info('End WHILE LOOP ...')

                try:
                    count = driver.find_elements_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr')
                    log.info('Count of ITR : '+ str(len(count)))

                    ITR_count = 0
                    year_row = 0
                    ack_no_row = 0
                    year_position = 0
                    ack_no_position = 0
                    filing_date_row = 0
                    filing_type_row = 0
                    for i in range(2, len(count)+1):
                        curr_year = ''
                        prev_year = ''
                        if year_row == 0:
                            try:
                                for j in range(1,9):
                                    if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='A.Y.':
                                        year_row = j
                                        time.sleep(1)
                                        break
                            except Exception as e:
                                log.info( 'Exception in year_row : %s' +traceback.format_exc())
                        if year_row != 0:
                            curr_year = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(year_row)+']').text
                            if i != 2 :
                                prev_year = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i-1)+']/td['+str(year_row)+']').text
                                if(curr_year == prev_year):
                                    ITR_count = ITR_count
                                else:
                                    ITR_count = ITR_count + 1
                            else:
                                ITR_count = ITR_count + 1

                    salat_registration_instance.itr_count = str(ITR_count)
                    salat_registration_instance.save()
                    # Updating Original Return Details
                    for i in range(2, len(count)+1):
                        curr_year = ''
                        if ack_no_position == 0 or year_position == 0 or filing_date_row == 0 or filing_type_row == 0:
                            try:
                                for j in range(1,9):
                                    if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='A.Y.':
                                        year_position = j
                                    if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='Ack. No.':
                                        ack_no_position = j
                                        # print 'ack_no : '+str(j)
                                    if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='Filing Date':
                                        filing_date_row = j
                                        # print 'filing_date_row : '+str(j)
                                    if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='Filing Type':
                                        filing_type_row = j
                                        # print 'filing_type_row : '+str(j)
                                time.sleep(1)
                            except Exception as e:
                                log.error('Exception in ack_no_position : %s' % traceback.format_exc())
                        if ack_no_position != 0 and year_position != 0 and filing_date_row != 0 and filing_type_row != 0:
                            curr_year = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(year_position)+']').text
                            filing_date = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(filing_date_row)+']').text
                            filing_type = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(filing_type_row)+']').text
                            curr_ack = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(ack_no_position)+']').text

                            if filing_type == 'Original':
                                log.info(str(curr_year) + ', FilingDate :' + str( filing_date ) + ', Type :' + str( filing_type ) + ', AckNo :' + str( curr_ack ))
                                update_original_return(curr_year,filing_date,filing_type,curr_ack,client_id,request)

                                
                    if len(count)>1:
                        ack_no_row = 0
                        for i in range(2, len(count)+1):
                            log.info(i)
                            link = '//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td[6]/a'
                            ITR = ""
                            if ack_no_row == 0:
                                try:
                                    for j in range(1,9):
                                        if driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[1]/th['+str(j)+']').text=='Ack. No.':
                                            ack_no_row = j
                                            time.sleep(1)
                                            break
                                except Exception as e:
                                    log.info('Exception in ack_no_row : %s' +traceback.format_exc())

                            try:
                                link = '//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td['+str(ack_no_row)+']/a'
                                ITR = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr['+str(i)+']/td[3]').text
                                ack_no = driver.find_elements_by_xpath(link)[0]
                                driver.execute_script("arguments[0].click();", ack_no)
                            except Exception as e:
                                error = 1
                                try:
                                    link = '//*[@id="ViewMyReturnsITR"]/table[2]/tbody/tr['+str(i)+']/td['+str(ack_no_row)+']/a'
                                    ITR = driver.find_element_by_xpath('//*[@id="ViewMyReturnsITR"]/table[2]/tbody/tr['+str(i)+']/td[3]').text
                                    ack_no = driver.find_elements_by_xpath(link)[0]
                                    driver.execute_script("arguments[0].click();", ack_no)
                                except Exception as e:
                                    error = 2
                            
                            try:
                                get_pdf = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div[1]/table[2]/tbody/tr[2]/td[3]/a[3]')
                                webdriver.ActionChains(driver).move_to_element(get_pdf[0]).send_keys(Keys.ALT).click(get_pdf[0]).send_keys(Keys.ALT).perform()
                            except Exception as e:
                                error = 1
                                try:
                                    get_pdf = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div[1]/table[2]/tbody/tr[2]/td[3]/a[2]')
                                    webdriver.ActionChains(driver).move_to_element(get_pdf[0]).send_keys(Keys.ALT).click(get_pdf[0]).send_keys(Keys.ALT).perform()
                                except Exception as e:
                                    error = 2
                            
                            downloadPath = selenium_path+'/ITR'
                            driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
                            params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
                            command_result = driver.execute("send_command", params)

                            time.sleep(1)
                            try:
                                back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[2]/div[2]/table/tbody/tr/td/div/a/input').click()
                            except Exception as e:
                                try:
                                    back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[3]/table/tbody/tr/td/div/a/input').click()
                                except Exception as e:
                                    # back = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri_0"]')
                                    try:
                                        back = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri_0"]')
                                    except Exception as e:
                                        error = 2
                            # back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[2]/div[2]/table/tbody/tr/td/div/a/input').click()
                            
                            file_arr = os.listdir(selenium_path+"/ITR")
                            for file in file_arr:
                                if file.find(pan)>=0:
                                    my_list = file.split("_")
                                    rename = my_list[1]+"_"+my_list[2]+"_"+ITR+".pdf"
                                    old_file = os.path.join(selenium_path+"/ITR", file)
                                    new_file = os.path.join(selenium_path+"/ITR_excel", rename)
                                    no_of_ITR = no_of_ITR + 1
                                    log.info(rename+' no_of_ITR -> '+str(no_of_ITR))
                                    log.info('New ITR File Name : '+new_file)
                                    log.info("file exist : "+str(os.path.exists(new_file)) )
                                    if(str(os.path.exists(new_file)) == 'False'):
                                        os.rename(old_file, new_file)
                                    else:
                                        os.remove(old_file)
                            if no_of_ITR>5:
                                time.sleep(2)
                                break
                    
                    salat_registration_instance.downloaded_itr_count = str(no_of_ITR)
                    salat_registration_instance.save()
                    if(no_of_ITR>0):
                        log.info('call read_ITR')
                        functionName='read_ITR'
                        os.system('bash -c "source '+multipleEri_sh+' '+functionName+' '+pan+' '+str(client_id)+'" &')
                        #read_ITR(pan,client_id)
                    else:
                        salat_registration_instance.processed_itr_count = str(0)
                        salat_registration_instance.save()
                    
                    result['status'] = 'complete'
                except Exception as ex:
                    user_error_tracking(client_id,'Download ITR : %s' % ex,request)
                    log.error('Error in Download ITR : %s' +traceback.format_exc())
            else:
                log.info('ITR Downloaded and processed')

            if run_26as == 1 :
                try:
                    log.info('run_26as==1')
                    if(salat_registration_instance.processed_26as_count is None):
                        download_26as_status = download_26as(driver,pan)
                    else:
                        if(int(salat_registration_instance.downloaded_26as_count)<5):
                            download_26as_status = download_26as(driver,pan)
                        else:
                            download_26as_status = 'success'
                    if(download_26as_status == 'error'):
                        return 'error'
                    log.info(download_26as_status)
                    if(download_26as_status == 'success'):
                        unzip_txt_status = unzip_txt(pan,dob)
                        log.info(unzip_txt_status)
                        if(unzip_txt_status == 'success'):
                            log.info('call excel_to_db')
                            business_partner_id=business_partner_instance.id
                            functionName='excel_to_db'
                            os.system('bash -c "source '+multipleEri_sh+' '+functionName+' '+pan+' '+str(client_id)+' '+str(business_partner_id)+'" &')
                            #excel_to_db_status = excel_to_db(pan,client_id,business_partner_instance)
                            #log.info(excel_to_db_status)
                        else:
                            log.info('26AS unzip failed')
                
                except Exception as ex:
                    user_error_tracking(client_id,'Download 26AS : %s' % ex,request)
                    log.error('Error in Download 26AS : %s' +traceback.format_exc())
                    return 'error'
            else:
                log.info('26AS Downloaded and processed')
        else:
            salatclient_instance.Is_salat_client=0
            salatclient_instance.Is_ERI_reg=0
            salatclient_instance.save()
            result['status'] = 'This PAN is not added as Salat Client.'

        ERI_user_instance.status = 'inactive'
        ERI_user_instance.user_pan = ''
        ERI_user_instance.save()

        result['no_of_ITR'] = no_of_ITR
        result['pan_status'] = pan_status
        result['dob_status'] = dob_status
        result['otp_status'] = otp_status
        return result

    except Exception as ex:
        try:
            inactive_eri_user(pan)
        except Exception as ex:
            inactive_eri_resp=0
        log.error('Error in multiple_eri_ITR : %s' % traceback.format_exc())
        return 'error'


def read_ITR(pan,client_id):

    log.info('read_ITR')

    try:

        MYDIR = os.path.dirname(__file__)
        pdf_path = selenium_path+"/ITR_excel"
        pdf = ""
        full_output = ''
        file_path = ''
        count_process_file = 0
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        process_file = 0
        if salat_registration_instance.processed_itr_count is None:
            process_file = 1
        elif int(salat_registration_instance.processed_itr_count) < 5:
            if int(salat_registration_instance.itr_count) >= 5 :
                process_file = 1
        log.info(process_file)
        #testing
        # for file in os.listdir(pdf_path):
            # if(file.find('.xls') != -1 and file.find(pan) != -1 and file.find('2018') != -1):
            #   log.info(file)
            #   filename = selenium_path+"/ITR_excel/"+file
            #   workbook = xlrd.open_workbook(filename)
            #   worksheet = workbook.sheet_by_name('Static Data')

            #   num_rows = worksheet.nrows - 1
            #   num_col = worksheet.ncols - 1
            #   curr_row = 0
                
            #   while curr_row < num_rows:
            #       curr_row += 1
            #       row = worksheet.row(curr_row) # get each row, list type
            #       if Personal_Details.objects.filter(P_Id=client_id).exists():
            #           pd_instance=Personal_Details.objects.get(P_Id=client_id)
            #           pd_instance.aadhar_no= row[13].value
            #           name = row[0].value.replace(" ","|")
            #           name = name.upper()
            #           pd_instance.name = name
            #           pd_instance.mobile = row[11].value
            #           pd_instance.save()

            #       personal_instance=Personal_Details.objects.get(P_Id=client_id)
            #       if not Address_Details.objects.filter(p_id=personal_instance).exists():
            #           Address_Details.objects.create(
            #               p_id=personal_instance,
            #               flat_door_block_no=row[3].value,
            #               name_of_premises_building=row[4].value,
            #               road_street_postoffice=row[6].value,
            #               area_locality=row[5].value,
            #               pincode=row[10].value,
            #               town_city=row[7].value,
            #               state=row[8].value,
            #               country='INDIA',
            #               current_place='',
            #           ).save()
            #           log.info("Address Entry Created")
            #       else:
            #           Address_Details_instance=Address_Details.objects.get(p_id=personal_instance)
            #           Address_Details_instance.flat_door_block_no=row[3].value
            #           Address_Details_instance.name_of_premises_building=row[4].value
            #           Address_Details_instance.road_street_postoffice=row[6].value
            #           Address_Details_instance.area_locality=row[5].value
            #           Address_Details_instance.pincode=row[10].value
            #           Address_Details_instance.town_city=row[7].value
            #           Address_Details_instance.state=row[8].value
            #           Address_Details_instance.save()
            #           log.info("Address Entry Updated")
        
        #testing

        if process_file == 1 :
            for file in os.listdir(pdf_path):
                try:
                    if(file.find('.pdf') != -1 and file.find(pan) != -1):
                        pdf = pdf+" "+file

                        jar_path = MYDIR+'/form16/read_itr.jar'
                        file_path = selenium_path+"/ITR_excel/"+file

                        p = Popen(['java', '-jar',jar_path,file_path], stdout=PIPE, stderr=STDOUT)
                        for line in p.stdout:
                            output = line
                            full_output = full_output+line
                except Exception as ex:
                    # user_error_tracking(client_id,'processing ITR read_itr : %s' % ex,request)
                    log.error('Error in processing ITR read_itr  : %s' +traceback.format_exc())
                
            for file in os.listdir(pdf_path):
                try:
                    if(file.find('.xls') != -1 and file.find(pan) != -1):
                        count_process_file = count_process_file + 1
                        log.info(file)
                        filename = selenium_path+"/ITR_excel/"+file
                        workbook = xlrd.open_workbook(filename)
                        worksheet = workbook.sheet_by_name('PDF Data')

                        num_rows = worksheet.nrows - 1
                        num_col = worksheet.ncols - 1
                        curr_row = 0

                        while curr_row < num_rows:
                            curr_row += 1
                            row = worksheet.row(curr_row) # get each row, list type
                            if temp_ITR_value.objects.filter(PAN=pan,year=row[0].value).exists():
                                temp_ITR_value.objects.filter(PAN=pan,year=row[0].value).delete()
                            
                            if not temp_ITR_value.objects.filter(PAN=pan,year=row[0].value).exists():
                                temp_ITR_value.objects.create(
                                    PAN=pan,
                                    year=row[0].value,
                                    ITR=row[1].value,
                                    income_from_salary=row[2].value,
                                    income_from_hp=row[3].value,
                                    interest_payable_borrowed_capital=row[4].value,
                                    capital_gain=row[5].value,
                                    schedule_si_income=row[6].value,
                                    income_from_other_src=row[7].value,
                                    interest_gross=row[8].value,
                                    gross_total_income=row[9].value,
                                    deduction=row[10].value,
                                    c_80=row[11].value,
                                    d_80=row[12].value,
                                    tta_80=row[13].value,
                                    ccd_1b_80=row[14].value,
                                    ccg_80=row[15].value,
                                    e_80=row[16].value,
                                    ee_80=row[17].value,
                                    g_80=row[18].value,
                                    gg_80=row[19].value,
                                    total_income=row[20].value,
                                    schedule_si_tax=row[21].value,
                                    total_tax_interest=row[22].value,
                                    tax_paid=row[23].value,
                                    adv_tax_self_ass_tax=row[24].value,
                                    total_tcs=row[25].value,
                                    total_tds_ITR=row[26].value,
                                    no_of_hp = row[27].value,
                                    ccd_2_80 = row[28].value,
                                    business_or_profession = row[29].value
                                ).save()

                    if(file.find('.xls') != -1 and file.find(pan) != -1 and file.find('2018') != -1):
                        filename = selenium_path+"/ITR_excel/"+file
                        workbook = xlrd.open_workbook(filename)
                        worksheet = workbook.sheet_by_name('Static Data')

                        num_rows = worksheet.nrows - 1
                        num_col = worksheet.ncols - 1
                        curr_row = 0
                        
                        while curr_row < num_rows:
                            curr_row += 1
                            row = worksheet.row(curr_row) # get each row, list type
                            if Personal_Details.objects.filter(P_Id=client_id).exists():
                                pd_instance=Personal_Details.objects.get(P_Id=client_id)
                                pd_instance.aadhar_no= row[13].value
                                name = row[0].value.replace(" ","|")
                                name = name.upper()
                                pd_instance.name = name
                                pd_instance.mobile = row[11].value
                                pd_instance.father_name = row[14].value
                                pd_instance.save()

                            personal_instance=Personal_Details.objects.get(P_Id=client_id)
                            if not Address_Details.objects.filter(p_id=personal_instance).exists():
                                Address_Details.objects.create(
                                    p_id=personal_instance,
                                    flat_door_block_no=row[3].value,
                                    name_of_premises_building=row[4].value,
                                    road_street_postoffice=row[6].value,
                                    area_locality=row[5].value,
                                    pincode=row[10].value,
                                    town_city=row[7].value,
                                    state=row[8].value,
                                    country='INDIA',
                                    current_place='',
                                ).save()
                                log.info("Address Entry Created")
                            else:
                                Address_Details_instance=Address_Details.objects.get(p_id=personal_instance)
                                Address_Details_instance.flat_door_block_no=row[3].value
                                Address_Details_instance.name_of_premises_building=row[4].value
                                Address_Details_instance.road_street_postoffice=row[6].value
                                Address_Details_instance.area_locality=row[5].value
                                Address_Details_instance.pincode=row[10].value
                                Address_Details_instance.town_city=row[7].value
                                Address_Details_instance.state=row[8].value
                                Address_Details_instance.save()
                                log.info("Address Entry Updated")

                        worksheet_b = workbook.sheet_by_name('Bank Data')

                        num_rows_b = worksheet_b.nrows - 1
                        num_col_b = worksheet_b.ncols - 1
                        curr_row_b = 0
                        while curr_row_b < num_rows_b:
                            curr_row_b += 1
                            row = worksheet_b.row(curr_row_b) # get each row, list type
                            personal_instance=Personal_Details.objects.get(P_Id=client_id)
                            if row[1].value != '':
                                if not Bank_Details.objects.filter(P_id=personal_instance,Bank_id=curr_row_b-1).exists():
                                    Bank_Details.objects.create(
                                        P_id=personal_instance,
                                        Bank_id=curr_row_b-1,
                                        Default_bank=row[0].value,
                                        IFSC_code=row[1].value,
                                        Account_no=row[2].value,
                                        Bank_proof='active'
                                    ).save()
                                    log.info("Bank Entry Created")
                                else:
                                    Bank_Details_instance=Bank_Details.objects.get(P_id=personal_instance,Bank_id=curr_row_b-1)
                                    Bank_Details_instance.Bank_id=curr_row_b-1
                                    Bank_Details_instance.Default_bank=row[0].value
                                    Bank_Details_instance.IFSC_code=row[1].value
                                    Bank_Details_instance.Account_no=row[2].value
                                    Bank_Details_instance.Bank_proof='active'
                                    Bank_Details_instance.save()
                                    log.info("Bank Entry Updated")
                            else:
                                log.info("Blank Bank Entry")
                except Exception as ex:
                    # user_error_tracking(client_id,'processing ITR saving data : %s' % ex,request)
                    log.error('Error in processing ITR saving data  : %s' +traceback.format_exc())

        # count_process_file
        log.info(count_process_file)
        #salat_registration.objects.filter(pan=pan).update(processed_itr_count=str(count_process_file))
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        salat_registration_instance.processed_itr_count = str(count_process_file)
        salat_registration_instance.save()

    except Exception as ex:
        try:
            inactive_eri_user(pan)
        except Exception as ex:
            inactive_eri_resp=0
        log.error('Error in read_ITR : %s' % traceback.format_exc())


def download_26as(driver,pan):

    log.info('download_26as')

    try:

        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        log.info('start 26AS '+pan)
        captch_img_path = selenium_path+'/images_captcha/view_form26AS.png'
        time.sleep(1)
        my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
        hover = ActionChains(driver).move_to_element(my_account)
        hover.perform()

        viewform26as = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl/dt/a')[0]
        driver.execute_script("arguments[0].click();", viewform26as)

        PAN = driver.find_element_by_id("View26AS_panNumber")
        PAN.send_keys(pan)
        error = 1
        error_captcha_count = 1
        while error == 1:
            error_captcha_count += 1
            error = captch_view_form26AS(driver,captch_img_path)

        log.info('Count of Captcha try : '+ str(error_captcha_count) )
        time.sleep(1)
        try:
            time.sleep(2)
            # driver.implicitly_wait(2)
            # try:
            # //*[@id="content"]/div[2]/p[2]/a
            # //*[@id="content"]/div[2]/p[2]/a
            try:
                continue_view = driver.find_elements_by_xpath('//*[@id="view26AS"]/table/tbody/tr[4]/td/input')[0]
            except Exception as e:
                continue_view = driver.find_elements_by_xpath('//*[@id="content"]/div[2]/p[2]/a')[0]

            driver.execute_script("arguments[0].click();", continue_view)
            # except Exception as e:
            #   log.info('continue_view Error : %s' % e)
            time.sleep(2)
            driver.switch_to_window(driver.window_handles[1])
            time.sleep(1)
        except Exception as e:
            log.error('continue_view Error : %s' +traceback.format_exc())
            return 'error'

        try:
            time.sleep(2)
            i_agree = driver.find_element_by_xpath('//*[@id="Details"]')
            i_agree.click()
            log.info('start redirect ... ')
            time.sleep(2)
            #driver.implicitly_wait(5)
            #driver.set_page_load_timeout(5)
            driver.get("https://services.tdscpc.gov.in/serv/tapn/view26AS.xhtml")
            time.sleep(2)
        except Exception as e:
            log.error('Error redirect view26AS : %s' +traceback.format_exc())
            return 'error'

        captcha_save = 1
        try:
            time.sleep(2)
            driver.set_page_load_timeout(2)
            no_of_26as = 0
            for year in xrange(2015,2020):
                select_AssessmentYear = Select(driver.find_element_by_id('AssessmentYearDropDown'))
                select_AssessmentYear.select_by_value(str(year))
                driver.implicitly_wait(1)
                select_viewType = Select(driver.find_element_by_id('viewType'))
                select_viewType.select_by_value('Text')

                downloadPath = selenium_path+'/zip_26AS'
                driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
                params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
                command_result = driver.execute("send_command", params)
                time.sleep(1)
                driver.implicitly_wait(2)
                view_download = driver.find_elements_by_xpath('//*[@id="btnSubmit"]')[0]
                driver.execute_script("arguments[0].click();", view_download)
                log.info(str(year)+' 26AS Downloaded. ')
                no_of_26as = no_of_26as + 1
                time.sleep(2)
                driver.implicitly_wait(2)

            #salat_registration.objects.filter(pan=pan).update(downloaded_26as_count=no_of_26as)
            salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
            salat_registration_instance.downloaded_26as_count = no_of_26as
            salat_registration_instance.save()
        except Exception as e:
            log.error('Error Download 26AS  : %s' +traceback.format_exc())
            return 'error'
        
        return 'success'

    except Exception as ex:
        try:
            inactive_eri_user(pan)
        except Exception as ex:
            inactive_eri_resp=0
        log.error('Error in download_26as : %s' % traceback.format_exc())
        return 'error'


def unzip_txt(pan,dob):
    log.info('unzip_txt')

    try:
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        password = re.sub('[^A-Za-z0-9]+', '', dob)
        log.info(password)
        unzip_file = selenium_path+"/zip_26AS/"
        log.info('Zip file list :')
        for file in os.listdir(unzip_file):
            if(file.find('.zip') != -1 and file.find(pan) != -1):
                log.info(file)
                with zipfile.ZipFile(selenium_path+"/zip_26AS/"+file,"r") as zip_ref:
                    zip_ref.extractall(path=unzip_file,pwd=password)
        # result['unzip']= 'success'

        xls_count=0
        for file in os.listdir(unzip_file):
            if(file.find('.txt') != -1 and file.find(pan) != -1):
                # Create workbook and worksheet 
                txt_file = unzip_file+file
                wbk = xlwt.Workbook() 
                sheet = wbk.add_sheet('26AS')
                row = 0  # row counter
                f = open(txt_file)
                for line in f: 
                    colCount = 0
                    for column in line.split("^"): 
                        sheet.write(row, colCount, column)
                        colCount += 1
                    row += 1

                wbk.save(unzip_file+file.split(".")[0]+'.xls')
                xls_count+=1

        log.info(xls_count)
        if xls_count!=0:
            #salat_registration.objects.filter(pan=pan).update(downloaded_26as_count=xls_count)
            salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
            salat_registration_instance.downloaded_26as_count = xls_count
            salat_registration_instance.save()
            return 'success'
        else:
            log.info('26AS unzip failed')
            return 'Failed'

    except Exception as ex:
        return 'Error'
        log.error('Error in unzip_txt : %s' +traceback.format_exc())

def excel_to_db_old(pan,client_id):
    R_ID = 0
    salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
    log.info('start excel_to_db')
    try:
        max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
        personal_instance=Personal_Details.objects.get(P_Id=client_id)
        if not Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
            Return_Details.objects.create(
                R_id = max_r_id['R_id__max']+1,
                P_id = personal_instance,
                FY = '2018-2019',
                AY = '2017-2018',
            ).save()

        # log.info('start excel_to_db')
        if Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
            for client in Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019'):
                R_ID = client.R_id
    except Exception as ex:
        log.error( 'Error multiple ERI DB  : %s' +traceback.format_exc())

    # log.info('start excel_to_db')
    excel_file = selenium_path+"/zip_26AS/"
    process_26asfile = 0
    # log.info('start excel_to_db')
    try:
        for file in os.listdir(excel_file):
            if(file.find('.xls') != -1 and file.find(pan+"-") != -1):
                log.info(file)
                filename = excel_file+file
                year = file.replace(pan+"-","")
                year = year.replace('.xls',"")
                log.info('year-> '+year+', File Name-> '+file)
                workbook = xlrd.open_workbook(filename)
                worksheet = workbook.sheet_by_name('26AS')

                num_rows = worksheet.nrows - 1
                curr_row = -1

                partA_status, partA1_status, partA2_status, partB_status, partC_status, partD_status = (0,)*6
                partE_status, partF_status, partG_status, part_A_count = (0,)*4

                no_of_TAN, no_of_TAN_partA1, no_of_TAN_partA2, no_of_TAN_partB, partA_start, partA_end = (0,)*6

                partA1_start, partA1_end, partA2_start, partA2_end, partB_start, partB_end , partC_start = (0,)*7
                partC_end, partD_start, partD_end, isunicode, A_no_trans, A1_no_trans, A2_no_trans , B_no_trans = (0,)*8
                while curr_row < num_rows:
                    curr_row += 1
                    row = worksheet.row(curr_row) # get each row, list type
                    if( isinstance(row[1].value, unicode) ):
                        isunicode = 1
                        row_value = row[1].value.encode('utf-8')
                    else:
                        isunicode = 0
                        row_value = row[1].value

                    if ( row[1].value == 'PART A - Details of Tax Deducted at Source'):
                        partA_status = 1
                        partA_start = curr_row
                        row2 = worksheet.row(curr_row+2) # get each row, list type
                        if (row2[3].value == '*********** No Transactions Present **********'):
                            A_no_trans = 1
                    if row[1].value == 'PART A1 - Details of Tax Deducted at Source for 15G / 15H':
                        partA_status = 0
                        partA1_status = 1
                        partA1_start = curr_row
                        partA_end = curr_row
                        row2 = worksheet.row(curr_row+2) # get each row, list type
                        if (row2[3].value == '*********** No Transactions Present **********'):
                            A1_no_trans = 1
                    if row[1].value == 'PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)' :
                        partA1_status = 0
                        partA2_status = 1
                        partA2_start = curr_row
                        partA1_end = curr_row
                        row2 = worksheet.row(curr_row+2) # get each row, list type
                        if (row2[3].value == '*********** No Transactions Present **********'):
                            A2_no_trans = 1
                    if row[1].value == 'PART B - Details of Tax Collected at Source' :
                        partA2_status = 0
                        partB_status = 1
                        partB_start = curr_row
                        partA2_end = curr_row
                        row2 = worksheet.row(curr_row+2) # get each row, list type
                        if (row2[3].value == '*********** No Transactions Present **********'):
                            B_no_trans = 1
                    if row[1].value == 'PART C - Details of Tax Paid (other than TDS or TCS)' :
                        partB_status = 0
                        partC_status = 1
                        partC_start = curr_row
                        partB_end = curr_row
                    if row[1].value == 'PART D - Details of Paid Refund':
                        partC_status = 0
                        partD_status = 1
                        partD_start = curr_row
                        partC_end = curr_row
                    if row[1].value == 'PART E - Details of AIR Transaction':
                        partD_status = 0
                        partD_end = curr_row

                    if partA_status == 1:
                        part_A_count += 1
                        if (row[0].value!='' ):
                            if row[0].value!='\n':
                                no_of_TAN += 1

                    if partA1_status == 1:
                        if (row[0].value!=''):
                            if row[0].value!='\n':
                                no_of_TAN_partA1 += 1

                    if partA2_status == 1:
                        if (row[0].value!=''):
                            if row[0].value!='\n':
                                no_of_TAN_partA2 += 1
                    
                    if partB_status == 1:
                        if (row[0].value!=''):
                            if row[0].value!='\n':
                                no_of_TAN_partB += 1

                    if partC_status == 1:
                        if (row[0].value==1):
                            if row[0].value!='\n':
                                partC_start = curr_row

                    if partD_status == 1:
                        # part_D_count += 1
                        if (row[0].value==1):
                            if row[0].value!='\n':
                                partD_start = curr_row

                log.info('no_of_TAN : '+str(no_of_TAN))
                if A_no_trans != 1:
                    if no_of_TAN-1!= 0:
                        for i in xrange(1,no_of_TAN):
                            start = 0
                            end =0
                            for x in xrange(partA_start,partA_end):
                                nestr = worksheet.cell(x,0).value
                                nestr = re.findall(r'\d+', nestr)
                                for n in nestr:
                                    if (str(n) == str(i)):
                                        start = x
                                    if (str(n) == str(i+1)):
                                        end = x
                                if (i == no_of_TAN-1):
                                    end = partA_end
                            if end!=0:
                                get_partA(worksheet,start,end,pan,'A',year)

                # log.info('no_of_TAN_partA1 : '+str(no_of_TAN_partA1))
                if A1_no_trans != 1:
                    if no_of_TAN_partA1-1!= 0:
                        for i in xrange(1,no_of_TAN_partA1):
                            start = 0
                            end =0
                            for x in xrange(partA1_start,partA1_end):
                                nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
                                for n in nestr:
                                    if (str(n) == str(i)):
                                        start = x
                                    if (str(n) == str(i+1)):
                                        end = x
                                if (i == no_of_TAN_partA1-1):
                                    end = partA1_end
                            if end!=0:
                                get_partA(worksheet,start,end,pan,'A1',year)

                log.info('no_of_TAN_partA2 : '+str(no_of_TAN_partA2))
                if A2_no_trans != 1:
                    if no_of_TAN_partA2-1!= 0:
                        for i in xrange(1,no_of_TAN_partA2):
                            start = 0
                            end =0
                            for x in xrange(partA2_start,partA2_end):
                                nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
                                for n in nestr:
                                    if (str(n) == str(i)):
                                        start = x
                                    if (str(n) == str(i+1)):
                                        end = x
                                if (i == no_of_TAN_partA2-1):
                                    end = partA2_end
                            get_partA(worksheet,start,end,pan,'A2',year)
                
                # log.info('no_of_TAN_partB : '+str(B_no_trans) )
                if B_no_trans != 1:
                    if no_of_TAN_partB-1!= 0:
                        for i in xrange(1,no_of_TAN_partB):
                            start = 0
                            end =0
                            for x in xrange(partB_start,partB_end):
                                nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
                                for n in nestr:
                                    if (str(n) == str(i)):
                                        start = x
                                    if (str(n) == str(i+1)):
                                        end = x
                                if (i == no_of_TAN_partB-1):
                                    end = partB_end
                            get_partA(worksheet,start,end,pan,'B',year)
                
                log.info('partC_start ->'+str(partC_start)+' '+str(partC_end) )
                if partC_start+2 < partC_end-2:
                    for curr_row_partC in xrange(partC_start+2,partC_end-2):
                        if worksheet.cell(curr_row_partC,1).value != '':
                            major_head = worksheet.cell_value(curr_row_partC,1)
                            minor_head = worksheet.cell_value(curr_row_partC,2)
                            total_tax = worksheet.cell_value(curr_row_partC,7)
                            BSR_code = worksheet.cell_value(curr_row_partC,8)

                            try:
                                total_tax = int(float(total_tax) )
                            except ValueError:
                                total_tax = 0
                                log.info(total_tax+" Not a int")

                            cell = worksheet.cell(curr_row_partC, 9)
                            if cell.ctype == xlrd.XL_CELL_DATE:
                                date = datetime.datetime(1899, 12, 30)
                                get_ = datetime.timedelta(int(cell.value))
                                get_col2 = str(date + get_)[:10]
                                d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
                                get_col = d.strftime('%Y-%m-%d %H:%M')
                            else:
                                get_col = parse(cell.value)

                            deposit_date = get_col
                            challan_serial_no = worksheet.cell_value(curr_row_partC,10)
                            log.info(challan_serial_no)
                            if not tax_paid.objects.filter(R_Id=R_ID,challan_serial_no=challan_serial_no).exists():
                                tax_paid.objects.create(
                                    R_Id=R_ID,
                                    major_head=major_head,
                                    minor_head=minor_head,
                                    total_tax=total_tax,
                                    BSR_code=BSR_code,
                                    deposit_date=deposit_date,
                                    challan_serial_no=challan_serial_no
                                ).save()

                log.info('partD_start ->'+str(partD_start)+' '+str(partD_end) )
                if partD_start+2 < partD_end-2:
                    date_container = 0
                    amt_container = 0
                    int_container = 0
                    for curr_row_partD in xrange(partD_start+2,partD_end-2):
                        # partD_list = []
                        if worksheet.cell(curr_row_partD,1).value != '':
                            if(curr_row_partD == partD_start+2):
                                for x in xrange(1,num_rows):
                                    cell_value = worksheet.cell(curr_row_partD-1, x).value.encode('ascii','ignore')
                                    if(cell_value == ''):
                                        break
                                    if(cell_value.find('Date') != -1 ):
                                        date_container = x
                                    if(cell_value.find('Amount') != -1 ):
                                        amt_container = x
                                    if(cell_value.find('Interest') != -1 ):
                                        int_container = x
                            Assessment_Year = worksheet.cell(curr_row_partD,1).value
                            mode = worksheet.cell(curr_row_partD,2).value
                            # 3, 4 = amt, int
                            amount_of_refund = worksheet.cell(curr_row_partD,amt_container).value
                            interest = worksheet.cell(curr_row_partD,int_container).value
                            # log.info(Assessment_Year)
                        
                            cell = worksheet.cell(curr_row_partD, date_container)
                            if cell.ctype == xlrd.XL_CELL_DATE:
                                date = datetime.datetime(1899, 12, 30)
                                get_ = datetime.timedelta(int(cell.value))
                                get_col2 = str(date + get_)[:10]
                                d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
                                get_col = d.strftime('%Y-%m-%d %H:%M')
                            else:
                                get_col = parse(cell.value)

                            payment_date = get_col
                            if not refunds.objects.filter(R_Id=pan,payment_date=payment_date).exists():
                                refunds.objects.create(
                                    R_Id=pan,
                                    Assessment_Year=Assessment_Year,
                                    mode=mode,
                                    amount_of_refund=amount_of_refund,
                                    interest=interest,
                                    payment_date=payment_date,
                                ).save()
                process_26asfile = process_26asfile + 1
    except Exception as ex:
        user_error_tracking(client_id,'excel_to_db : %s' % ex,request)
        log.error( 'Error excel_to_db  : %s' +traceback.format_exc())

    #salat_registration.objects.filter(pan=pan).update(processed_26as_count=str(process_26asfile))
    salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
    salat_registration_instance.processed_26as_count = str(process_26asfile)
    salat_registration_instance.save()

    return 'success'

def excel_to_db(pan,client_id,business_partner_id):
    log.info('start excel_to_db')
    try:
    
        log.info('start excel_to_db '+pan)
        
        business_partner_instance=Business_Partner.objects.filter(id=business_partner_id).latest("id")
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        personal_instance=Personal_Details.objects.get(P_Id=client_id)
        excel_file = selenium_path+"/zip_26AS/"
        process_26asfile = 0
        if pan != '':
            log.info('excel_to_db 1')
            try:
                for file in os.listdir(excel_file):
                    if(file.find('.xls') != -1 and file.find(pan+"-") != -1):
                        log.info(file)
                        filename = excel_file+file
                        year = file.replace(pan+"-","")
                        year = year.replace('.xls',"")
                        log.info('year-> '+year+', File Name-> '+file)
                        workbook = xlrd.open_workbook(filename)
                        worksheet = workbook.sheet_by_name('26AS')

                        num_rows = worksheet.nrows - 1
                        curr_row = -1

                        partA_status, partA1_status, partA2_status, partB_status, partC_status, partD_status = (0,)*6
                        partE_status, partF_status, partG_status, part_A_count = (0,)*4

                        no_of_TAN, no_of_TAN_partA1, no_of_TAN_partA2, no_of_TAN_partB, partA_start, partA_end = (0,)*6

                        partA1_start, partA1_end, partA2_start, partA2_end, partB_start, partB_end , partC_start = (0,)*7
                        partC_end, partD_start, partD_end, isunicode, A_no_trans, A1_no_trans, A2_no_trans , B_no_trans = (0,)*8
                        while curr_row < num_rows:
                            curr_row += 1
                            row = worksheet.row(curr_row) # get each row, list type
                            if( isinstance(row[1].value, unicode) ):
                                isunicode = 1
                                row_value = row[1].value.encode('utf-8')
                            else:
                                isunicode = 0
                                row_value = row[1].value

                            if ( row[1].value == 'PART A - Details of Tax Deducted at Source'):
                                partA_status = 1
                                partA_start = curr_row
                                row2 = worksheet.row(curr_row+2) # get each row, list type
                                if (row2[3].value == '*********** No Transactions Present **********'):
                                    A_no_trans = 1
                            if row[1].value == 'PART A1 - Details of Tax Deducted at Source for 15G / 15H':
                                partA_status = 0
                                partA1_status = 1
                                partA1_start = curr_row
                                partA_end = curr_row
                                row2 = worksheet.row(curr_row+2) # get each row, list type
                                if (row2[3].value == '*********** No Transactions Present **********'):
                                    A1_no_trans = 1
                            if row[1].value == 'PART A2 - Details of Tax Deducted at Source on Sale of Immovable Property u/s 194IA/ TDS on Rent of Property u/s 194IB (For Seller/Landlord of Property)' :
                                partA1_status = 0
                                partA2_status = 1
                                partA2_start = curr_row
                                partA1_end = curr_row
                                row2 = worksheet.row(curr_row+2) # get each row, list type
                                if (row2[3].value == '*********** No Transactions Present **********'):
                                    A2_no_trans = 1
                            if row[1].value == 'PART B - Details of Tax Collected at Source' :
                                partA2_status = 0
                                partB_status = 1
                                partB_start = curr_row
                                partA2_end = curr_row
                                row2 = worksheet.row(curr_row+2) # get each row, list type
                                if (row2[3].value == '*********** No Transactions Present **********'):
                                    B_no_trans = 1
                            if row[1].value == 'PART C - Details of Tax Paid (other than TDS or TCS)' :
                                partB_status = 0
                                partC_status = 1
                                partC_start = curr_row
                                partB_end = curr_row
                            if row[1].value == 'PART D - Details of Paid Refund':
                                partC_status = 0
                                partD_status = 1
                                partD_start = curr_row
                                partC_end = curr_row
                            if row[1].value == 'PART E - Details of AIR Transaction':
                                partD_status = 0
                                partD_end = curr_row

                            if partA_status == 1:
                                part_A_count += 1
                                if (row[0].value!='' ):
                                    if row[0].value!='\n':
                                        no_of_TAN += 1

                            if partA1_status == 1:
                                if (row[0].value!=''):
                                    if row[0].value!='\n':
                                        no_of_TAN_partA1 += 1

                            if partA2_status == 1:
                                if (row[0].value!=''):
                                    if row[0].value!='\n':
                                        no_of_TAN_partA2 += 1
                            
                            if partB_status == 1:
                                if (row[0].value!=''):
                                    if row[0].value!='\n':
                                        no_of_TAN_partB += 1

                            if partC_status == 1:
                                if (row[0].value==1):
                                    if row[0].value!='\n':
                                        partC_start = curr_row

                            if partD_status == 1:
                                # part_D_count += 1
                                if (row[0].value==1):
                                    if row[0].value!='\n':
                                        partD_start = curr_row

                        R_ID = pan
                        try:
                            ay = str(year)+'-'+str(int(year)+1)
                            fy = str(int(year)-1)+'-'+str(int(year))
                            # log.info('ay : '+ay+' , fy : '+fy)
                            max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
                            log.info(max_r_id)
                            if max_r_id['R_id__max']==None:
                                latest_r_id=0
                            else:
                                latest_r_id=max_r_id['R_id__max']

                            if not Return_Details.objects.filter(P_id=personal_instance,FY = fy).exists():
                                Return_Details.objects.create(
                                    R_id = latest_r_id+1,
                                    P_id = personal_instance,
                                    business_partner_id=business_partner_instance,
                                    FY = fy,
                                    AY = ay,
                                ).save()
                                log.info('Return_Details created '+str(latest_r_id+1)+ ' for fy: '+str(fy)+' and year: '+str(year) )

                            # log.info('start excel_to_db')
                            if Return_Details.objects.filter(P_id=personal_instance,FY = fy).exists():
                                for client in Return_Details.objects.filter(P_id=personal_instance,FY = fy):
                                    R_ID = client.R_id

                            if tds_tcs.objects.filter(R_Id=R_ID,year=year).exists():
                                tds_tcs.objects.filter(R_Id=R_ID,year=year).delete()

                        except Exception as ex:
                            log.error('Error in : '+traceback.format_exc())
                        
                        log.info('no_of_TAN : '+str(no_of_TAN))
                        if A_no_trans != 1:
                            if no_of_TAN-1!= 0:
                                for i in xrange(1,no_of_TAN):
                                    start = 0
                                    end =0
                                    for x in xrange(partA_start,partA_end):
                                        nestr = worksheet.cell(x,0).value
                                        nestr = re.findall(r'\d+', nestr)
                                        for n in nestr:
                                            if (str(n) == str(i)):
                                                start = x
                                            if (str(n) == str(i+1)):
                                                end = x
                                        if (i == no_of_TAN-1):
                                            end = partA_end
                                    if end!=0:
                                        get_partA(worksheet,start,end,pan,'A',year,R_ID)

                        # log.info('no_of_TAN_partA1 : '+str(no_of_TAN_partA1))
                        if A1_no_trans != 1:
                            if no_of_TAN_partA1-1!= 0:
                                for i in xrange(1,no_of_TAN_partA1):
                                    start = 0
                                    end =0
                                    for x in xrange(partA1_start,partA1_end):
                                        nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
                                        for n in nestr:
                                            if (str(n) == str(i)):
                                                start = x
                                            if (str(n) == str(i+1)):
                                                end = x
                                        if (i == no_of_TAN_partA1-1):
                                            end = partA1_end
                                    if end!=0:
                                        get_partA(worksheet,start,end,pan,'A1',year,R_ID)

                        # log.info('no_of_TAN_partA2 : '+str(no_of_TAN_partA2))
                        if A2_no_trans != 1:
                            if no_of_TAN_partA2-1!= 0:
                                for i in xrange(1,no_of_TAN_partA2):
                                    start = 0
                                    end =0
                                    for x in xrange(partA2_start,partA2_end):
                                        nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
                                        for n in nestr:
                                            if (str(n) == str(i)):
                                                start = x
                                            if (str(n) == str(i+1)):
                                                end = x
                                        if (i == no_of_TAN_partA2-1):
                                            end = partA2_end
                                    get_partA(worksheet,start,end,pan,'A2',year,R_ID)
                        
                        # log.info('no_of_TAN_partB : '+str(B_no_trans) )
                        if B_no_trans != 1:
                            if no_of_TAN_partB-1!= 0:
                                for i in xrange(1,no_of_TAN_partB):
                                    start = 0
                                    end =0
                                    for x in xrange(partB_start,partB_end):
                                        nestr = re.findall(r'\d+', worksheet.cell(x,0).value)
                                        for n in nestr:
                                            if (str(n) == str(i)):
                                                start = x
                                            if (str(n) == str(i+1)):
                                                end = x
                                        if (i == no_of_TAN_partB-1):
                                            end = partB_end
                                    get_partA(worksheet,start,end,pan,'B',year,R_ID)
                        
                        log.info('partC_start ->'+str(partC_start)+' '+str(partC_end) )
                        if partC_start+2 < partC_end-2:
                            for curr_row_partC in xrange(partC_start+2,partC_end-2):

                                if worksheet.cell(curr_row_partC,1).value != '':
                                    # if tax_paid.objects.filter(R_Id=R_ID).exists():
                                    #   tax_paid.objects.filter(R_Id=R_ID).delete()
                                        
                                    major_head = worksheet.cell_value(curr_row_partC,1)
                                    minor_head = worksheet.cell_value(curr_row_partC,2)
                                    total_tax = worksheet.cell_value(curr_row_partC,7)
                                    BSR_code = worksheet.cell_value(curr_row_partC,8)

                                    try:
                                        total_tax = int(float(total_tax) )
                                    except ValueError:
                                        total_tax = 0
                                        log.info(total_tax+" Not a int")

                                    cell = worksheet.cell(curr_row_partC, 9)
                                    if cell.ctype == xlrd.XL_CELL_DATE:
                                        date = datetime.datetime(1899, 12, 30)
                                        get_ = datetime.timedelta(int(cell.value))
                                        get_col2 = str(date + get_)[:10]
                                        d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
                                        get_col = d.strftime('%Y-%m-%d %H:%M')
                                    else:
                                        get_col = parse(cell.value)

                                    deposit_date = get_col
                                    challan_serial_no = worksheet.cell_value(curr_row_partC,10)
                                    log.info(challan_serial_no)
                                
                                    if not tax_paid.objects.filter(R_Id=R_ID,challan_serial_no=challan_serial_no).exists():
                                        tax_paid.objects.create(
                                            R_Id=R_ID,
                                            major_head=major_head,
                                            minor_head=minor_head,
                                            total_tax=total_tax,
                                            BSR_code=BSR_code,
                                            deposit_date=deposit_date,
                                            challan_serial_no=challan_serial_no
                                        ).save()
                                        log.info('tax_paid created for R_ID :'+str(R_ID))

                        log.info('partD_start ->'+str(partD_start)+' '+str(partD_end) )
                        if partD_start+2 < partD_end-2:
                            date_container = 0
                            amt_container = 0
                            int_container = 0
                            for curr_row_partD in xrange(partD_start+2,partD_end-2):
                                # partD_list = []
                                if worksheet.cell(curr_row_partD,1).value != '':
                                    if(curr_row_partD == partD_start+2):
                                        for x in xrange(1,num_rows):
                                            cell_value = worksheet.cell(curr_row_partD-1, x).value.encode('ascii','ignore')
                                            if(cell_value == ''):
                                                break
                                            if(cell_value.find('Date') != -1 ):
                                                date_container = x
                                            if(cell_value.find('Amount') != -1 ):
                                                amt_container = x
                                            if(cell_value.find('Interest') != -1 ):
                                                int_container = x
                                    Assessment_Year = worksheet.cell(curr_row_partD,1).value
                                    mode = worksheet.cell(curr_row_partD,2).value
                                    # 3, 4 = amt, int
                                    amount_of_refund = worksheet.cell(curr_row_partD,amt_container).value
                                    interest = worksheet.cell(curr_row_partD,int_container).value
                                    # log.info(Assessment_Year)
                                
                                    cell = worksheet.cell(curr_row_partD, date_container)
                                    if cell.ctype == xlrd.XL_CELL_DATE:
                                        date = datetime.datetime(1899, 12, 30)
                                        get_ = datetime.timedelta(int(cell.value))
                                        get_col2 = str(date + get_)[:10]
                                        d = datetime.datetime.strptime(get_col2, '%Y-%m-%d')
                                        get_col = d.strftime('%Y-%m-%d %H:%M')
                                    else:
                                        get_col = parse(cell.value)

                                    payment_date = get_col
                                    if not refunds.objects.filter(R_Id=pan,payment_date=payment_date).exists():
                                        refunds.objects.create(
                                            R_Id=pan,
                                            Assessment_Year=Assessment_Year,
                                            mode=mode,
                                            amount_of_refund=amount_of_refund,
                                            interest=interest,
                                            payment_date=payment_date,
                                        ).save()
                        process_26asfile = process_26asfile + 1
            except Exception as ex:
                # user_error_tracking(client_id,'excel_to_db : %s' % ex,request)
                log.error('Error in excel_to_db  : %s' +traceback.format_exc())
                return 'Error excel_to_db : %s' % ex 
        else:
            log.info('PAN is Empty')

        log.info(process_26asfile)
        #salat_registration.objects.filter(pan=pan).update(processed_26as_count=str(process_26asfile))
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        salat_registration_instance.processed_26as_count = str(process_26asfile)
        salat_registration_instance.save()

    except Exception as ex:
        try:
            inactive_eri_user(pan)
        except Exception as ex:
            inactive_eri_resp=0
        log.error('Error in excel_to_db : %s' +traceback.format_exc())
        return 'Error excel_to_db : %s' % ex 

def salat_registration_fun(driver,pan,dob,client_id,b_id):
    log.info('salat_registration_fun')
    result={}
    eri_username = ''
    eri_password = ''

    try:

        business_partner_instance=Business_Partner.objects.get(id=b_id)
        if ERI_user.objects.filter(status='inactive').exists():
            for eri in ERI_user.objects.filter(status='inactive'):
                eri_username = eri.user_id
                eri_password = eri.password
        else:
            return 'No Free ERI'

        ERI_user_instance = ERI_user.objects.get(user_id=eri_username)
        ERI_user_instance.status = 'active'
        ERI_user_instance.user_pan = pan
        ERI_user_instance.date_time = datetime.now()
        ERI_user_instance.save()
        if not salatclient.objects.filter(PAN=pan).exists():
            salatclient.objects.create(
                PAN = pan,
            ).save()

        if not salat_registration.objects.filter(pan=pan).exists():
            salat_registration.objects.create(
                pan = pan,
                date_time = datetime.now(),
            ).save()
        else:
            salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
            time_diff = timezone.now() - salat_registration_instance.date_time
            if time_diff.seconds / 60 > 10 :
                # difference is greater than 10 minutes
                salat_registration.objects.create(
                    pan = pan,
                    date_time = datetime.now(),
                ).save()

        log.info('Start salat_registration '+eri_username)
        captch_img_path = selenium_path+'/images_captcha/download_ITR.png'
        login_status = 2
        while login_status == 2:
            login_status = login_ITR(driver,eri_username,eri_password)
        
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        salat_registration_instance.login = 1
        salat_registration_instance.save()
        screenshot(driver,'after_login')
        my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
        hover = ActionChains(driver).move_to_element(my_account)
        hover.perform()
        time.sleep(1)
        # //*[@id="header"]/div[2]/ul/li[5]/div/dl[1]/dt/a
        # //*[@id="header"]/div[2]/ul/li[2]/div/dl[2]/dt/a

        add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl[2]/dt/a')[0]   
        driver.execute_script("arguments[0].click();", add_client)
        PAN = driver.find_element_by_id("ViewReturnsItdHdEri_panNo")
        # Assuming PAN IS valid
        PAN.send_keys(pan)
        webdriver.ActionChains(driver).move_to_element(PAN).send_keys(Keys.ENTER).send_keys(Keys.ENTER).perform()
        pan_exist = 1
        no_of_ITR = 0
        try:
            element = driver.find_elements_by_class_name('error')
            for line in element:
                check_element = line.text
                if (check_element.find('This PAN is not added as a Client') == 0):
                    pan_exist = 0
        except Exception:
            error = 0

        result={}
        pan_status = ''
        dob_status = ''
        otp_status = ''
        result['status'] = 'pending'

        salatclient_instance=salatclient.objects.get(PAN=pan)
        if pan_exist == 0:
            result['pan_exist'] = 'This PAN is not added as Salat Client.'
            salat_registration_instance.is_salat_client = 'Not Salat Client'
            salat_registration_instance.save()
        else:
            result['pan_exist'] = 'This PAN is added as Salat Client.'
            salat_registration_instance.is_salat_client = 'Salat Client'
            salat_registration_instance.save()
            salatclient_instance.Is_salat_client=1
            salatclient_instance.Is_ERI_reg=1
            salatclient_instance.ERI_id=eri_username
            salatclient_instance.save()

        log.info(pan_exist)
        if pan_exist != 1:
            manage_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[5]/p/a')[0]
            hover = ActionChains(driver).move_to_element(manage_client)
            hover.perform()

            time.sleep(1)
            add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[5]/div/dl/dt/a')[0]
            driver.execute_script("arguments[0].click();", add_client)
            # Enter PAN And DOB
            PAN = driver.find_element_by_id("AddClientDetails_eriClient_id_panNo")
            DOB = driver.find_element_by_id("dateField")

            # Assuming PAN AND DOB are valid
            PAN.send_keys(pan)
            DOB.send_keys(dob)
            error = 1
            while error == 1:
                time.sleep(1)
                error = captch_add_client(driver,captch_img_path)
                element = driver.find_elements_by_class_name('error')
                for line in element:
                    check_element = line.text
                    if (check_element.find('PAN does not exist.') >= 0):
                        pan_status = 'PAN does not exist'
                        salatclient_instance.Is_salat_client=2
                        salatclient_instance.Is_ERI_reg=2
                        salatclient_instance.save()
                        error = 0
                    if (check_element.find('This PAN is not registered with e-Filing.') >= 0):
                        pan_status = 'This PAN is not registered with e-Filing.'
                        salatclient_instance.Is_salat_client=0
                        salatclient_instance.Is_ERI_reg=0
                        salatclient_instance.save()
                        error = 0
                    if (check_element.find('Invalid Date') >= 0):
                        dob_status = 'Invalid Date'
                        error = 0
            log.info('pan_status '+pan_status)
            if pan_status != '':
                salat_registration_instance.is_salat_client = pan_status
                salat_registration_instance.save()

            if(pan_status != 'PAN does not exist' and dob_status != 'Invalid Date'and pan_status != 'This PAN is not registered with e-Filing.'):
                time.sleep(1)
                salat_registration_instance.otp_trigger = 1
                salat_registration_instance.save()
                d,m,y=dob.split("/")
                pan1_dob=y+'-'+m+'-'+d
                if not Personal_Details.objects.filter(P_Id=client_id).exists():
                    Personal_Details.objects.create(
                        P_Id=client_id,
                        business_partner_id=business_partner_instance,
                        pan=pan,
                        pan_type='PAN',
                        dob= pan1_dob
                    ).save()
                    log.info(pan + " Entry Created")
                else:
                    pd_instance=Personal_Details.objects.get(P_Id=client_id)
                    pd_instance.pan = pan
                    pd_instance.business_partner_id=business_partner_instance
                    pd_instance.pan_type='PAN'
                    pd_instance.dob= pan1_dob
                    pd_instance.save()
                    log.info(pan + " Entry Updated")
                try:
                    driver.find_element_by_id('VerifyAddClientDetails_mobilePin')
                    otp_status = 'success'
                    salatclient_instance.Is_salat_client=0
                    salatclient_instance.Is_ERI_reg=0
                    salatclient_instance.save()
                except Exception:
                    otp_status = 'fail'

                MYDIR = os.path.dirname(__file__)
                filename_mail = MYDIR+'/'+pan+'_salat_mail_otp.txt'
                filename_mobile = MYDIR+'/'+pan+'_salat_mobile_otp.txt'

                error_otp = 1
                timeout = time.time() + 60*10   # 5 minutes from now
                while error_otp == 1:
                    if time.time() > timeout:
                        otp_status = 'Enter OTP'
                        break
                    try:
                        file = open(filename_mail,'r')
                        mail_otp = file.read() 
                        file2 = open(filename_mobile, 'r')
                        mobile_otp = file2.read()
                        log.info(mail_otp)
                        log.info(mobile_otp)
                        driver.find_element_by_id("VerifyAddClientDetails_emailPin").send_keys(mail_otp)
                        driver.find_element_by_id("VerifyAddClientDetails_mobilePin").send_keys(mobile_otp)
                        error_otp = 0
                        os.remove(filename_mail)
                        os.remove(filename_mobile)
                    except Exception:
                        error_otp = 1
                        otp_status = 'waiting'
                        break

                if (error_otp == 0):
                    try:
                        submit_otp = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_0"]')[0]
                        driver.execute_script("arguments[0].click();", submit_otp)
                        try:
                            time.sleep(2)
                            otp_error = driver.find_elements_by_xpath('//*[@id="VerifyAddClientDetails"]/table/tbody/tr[3]/td/div[1]')[0]
                            otp_status = 'Invalid Mail OTP'
                        except Exception:
                            otp_status = 'success'
                        try:
                            time.sleep(2)
                            otp_error = driver.find_elements_by_xpath('//*[@id="VerifyAddClientDetails"]/table/tbody/tr[2]/td/div[2]')[0]
                            otp_status = 'Invalid Mobile OTP'
                        except Exception:
                            otp_status = 'success'
                    except TimeoutException:
                        otp_status = 'fail'
            else:
                salatclient_instance.Is_salat_client=2
                salatclient_instance.Is_ERI_reg=2
                salatclient_instance.save()
            if(otp_status=='success'):
                salat_registration_instance.otp_submitted = 1
                salat_registration_instance.save()
                salatclient_instance.Is_salat_client=1
                salatclient_instance.Is_ERI_reg=1
                salatclient_instance.ERI_id= eri_username
                salatclient_instance.save()
                result['status'] = 'Successfully added as Salat Client'

        ERI_user_instance.status = 'inactive'
        ERI_user_instance.user_pan = ''
        ERI_user_instance.save()
        log.info('PAN : '+ pan + ' ,ERI :'+ eri_username + ' ,Status: inactive, otp_status : '+ otp_status )
        result['pan_status'] = pan_status
        result['dob_status'] = dob_status
        result['otp_status'] = otp_status
        return result

    except Exception as ex:
        try:
            inactive_eri_user(pan)
        except Exception as ex:
            inactive_eri_resp=0
        log.error('Error in salat_registration_fun : %s' +traceback.format_exc())
        return 'error'

def user_error_tracking(client_id,event_name,request):
    if(request.user_agent.is_pc ) :
        device_platform = 'Desktop'
    elif(request.user_agent.is_mobile ):
        device_platform = 'Mobile'
    else:
        device_platform = 'Other'

    browser = request.user_agent.browser.family
    user_tracking.objects.create(
        client_id = client_id,
        event = 'Selenium Error',
        event_name = event_name,
        device_platform = device_platform,
        browser = browser,
        device_type = request.user_agent.device.family
    ).save()

def update_original_return(curr_year,filing_date,filing_type,curr_ack,client_id,request):
    try:
        FY = ''
        s,end =curr_year.split("-")
        s = int(s)-1
        end = int('20'+end)-1
        FY = str(s)+'-'+str(end)
        d,m,y=filing_date.split("/")
        filing_date1 = y+'-'+m+'-'+d
        personal_details_instance=Personal_Details.objects.get(P_Id=client_id)
        if not Original_return_Details.objects.filter(P_id=personal_details_instance,FY=FY).exists():
            Original_return_Details.objects.create(
                P_id=personal_details_instance,
                FY=FY,
                Receipt_no_original_return=curr_ack,
                Date_of_original_return_filing = filing_date1,
            ).save()
            log.info('Original_return_Details entry created')
        else:
            or_details_instance=Original_return_Details.objects.get(P_id=personal_details_instance,FY=FY)
            or_details_instance.Receipt_no_original_return=curr_ack
            or_details_instance.Date_of_original_return_filing=filing_date1
            or_details_instance.save()
            log.info('Original_return_Details entry updated')

    except Exception as ex:
        user_error_tracking(client_id,'Update Original Return : %s' % ex,request)
        log.error('Error in update original_return  : %s' +traceback.format_exc())

def iTR_V_Acknowledgement(driver,pan,dob,client_id,b_id,R_id,request):
    eri_username = ''
    eri_password = ''
    AssessmentYear=''
    if Return_Details.objects.filter(R_id=R_id).exists():
        return_Details_obj=Return_Details.objects.filter(R_id=R_id).latest('id')
        AssessmentYear=return_Details_obj.AY
    log.info(AssessmentYear)
    business_partner_instance=Business_Partner.objects.get(id=b_id)
    if not salat_registration.objects.filter(pan=pan).exists():
        salat_registration.objects.create(
            pan = pan,
            date_time = datetime.now(),
            login = 0,
        ).save()
    else:
        salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
        salat_registration_instance.date_time = datetime.now()
        salat_registration_instance.save()

    if salatclient.objects.filter(PAN=pan).exists():
        for sc in salatclient.objects.filter(PAN=pan):
            if sc.ERI_id is None:
                eri_username = ''
            else:
                eri_username = sc.ERI_id
            if eri_username != '':
                if ERI_user.objects.filter(status='inactive',user_id=eri_username).exists():
                    for eri in ERI_user.objects.filter(status='inactive',user_id=eri_username):
                        eri_username = eri.user_id
                        eri_password = eri.password
                else:
                    return 'No Free ERI'
            else:
                return 'No ERI Assigned to the PAN'
    else:
        return 'PAN Not registered as Salat Client'

    # eri_username = 'ERIA101868'
    # eri_password = "Derivatives4$"
    log.info('Start Multiple_ITR : '+eri_username)

    ERI_user_instance = ERI_user.objects.get(user_id=eri_username)
    ERI_user_instance.status = 'active'
    ERI_user_instance.user_pan = pan
    ERI_user_instance.date_time = datetime.now()
    ERI_user_instance.save()

    captch_img_path = selenium_path+'/images_captcha/download_ITR.png'
    login_status = 2
    while login_status == 2:
        login_status = login_ITR(driver,eri_username,eri_password)
        log.info(login_status)

    salat_registration_instance = salat_registration.objects.filter(pan=pan).order_by('-date_time')[0]
    salat_registration_instance.login = 1
    salat_registration_instance.save()

    time.sleep(1)
    try:
        my_account = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/p/a')[0]
        hover = ActionChains(driver).move_to_element(my_account)
        hover.perform()
        time.sleep(1)
    except Exception as ex:
        log.info('Error my_account : %s' %ex)
        error = 0
        return 'error'
    # screenshot(driver,'test')
    try:
        add_client = driver.find_elements_by_xpath('//*[@id="header"]/div[2]/ul/li[2]/div/dl[2]/dt/a')[0]
        driver.execute_script("arguments[0].click();", add_client)
        PAN = driver.find_element_by_id("ViewReturnsItdHdEri_panNo")
        # Assuming PAN IS valid
        PAN.send_keys(pan)
    except Exception as ex:
        log.info('Error add_client : %s' %ex)
        error = 0
        return 'error'
    webdriver.ActionChains(driver).move_to_element(PAN).send_keys(Keys.ENTER).send_keys(Keys.ENTER).perform()
    pan_exist = 1
    no_of_ITR = 0
    try:
        element = driver.find_elements_by_class_name('error')
        for line in element:
            check_element = line.text
            if (check_element.find('This PAN is not added as a Client') == 0):
                pan_exist = 0
    except Exception:
        error = 0

    result={}
    pan_status = ''
    dob_status = ''
    otp_status = ''
    result['status'] = 'pending'

    salatclient_instance = salatclient.objects.get(PAN=pan)
    if pan_exist == 0:
        result['pan_exist'] = 'This PAN is not added as Salat Client.'
        salat_registration_instance.is_salat_client = 'Not Salat Client'
        salat_registration_instance.save()
    else:
        result['pan_exist'] = 'This PAN is added as Salat Client.'
        salat_registration_instance.is_salat_client = 'Salat Client'
        salat_registration_instance.save()
        salatclient_instance.Is_salat_client=1
        salatclient_instance.Is_ERI_reg=1
        salatclient_instance.save()

    log.info('PAN : '+ pan + ' '+ eri_username)
    log.info('pan_exist : '+str(pan_exist) )
    if pan_exist == 1:
        d,m,y=dob.split("/")
        pan1_dob=y+'-'+m+'-'+d
        if not Personal_Details.objects.filter(P_Id=client_id).exists():
            Personal_Details.objects.create(
                P_Id=client_id,
                business_partner_id=business_partner_instance,
                pan=pan,
                pan_type='PAN',
                dob= pan1_dob
            ).save()
            log.info(pan + " Entry Created")
        else:
            pd_instance=Personal_Details.objects.get(P_Id=client_id)
            pd_instance.pan = pan
            pd_instance.business_partner_id=business_partner_instance
            pd_instance.pan_type='PAN'
            pd_instance.dob= pan1_dob
            pd_instance.save()
            log.info(pan + " Entry Updated")
        error = 1
        run_itr = 0
        AY=''
        log.info('ITR DOWNLOAD WHILE LOOP ...')
        while error == 1:
            error = captch_ITR_download(driver,captch_img_path,'ViewReturnsItdHdEri_captchaCode')
        log.info('End WHILE LOOP ...')
        screenshot(driver,'testtt')
        try:
            link = '//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[2]/td[8]/a'
            ITR = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[2]/td[3]').text
            AY = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[2]/td[2]').text
            ack_no = driver.find_elements_by_xpath(link)[0]
            driver.execute_script("arguments[0].click();", ack_no)
            screenshot(driver,'testtttt')
        except Exception as e:
            error = 1
            try:
                link = '//*[@id="ViewMyReturnsITR"]/table[2]/tbody/tr[2]/td[8]/a'
                ITR = driver.find_element_by_xpath('//*[@id="ViewMyReturnsITR"]/table[2]/tbody/tr['+str(i)+']/td[3]').text
                AY = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri"]/table[2]/tbody/tr[2]/td[2]').text
                ack_no = driver.find_elements_by_xpath(link)[0]
                driver.execute_script("arguments[0].click();", ack_no)
            except Exception as e:
                error = 2
        
        try:
            get_pdf = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div[1]/table[2]/tbody/tr[2]/td[3]/a[1]')
            webdriver.ActionChains(driver).move_to_element(get_pdf[0]).send_keys(Keys.ALT).click(get_pdf[0]).send_keys(Keys.ALT).perform()
        except Exception as e:
            error = 1
            try:
                get_pdf = driver.find_elements_by_xpath('//*[@id="dynamicContent"]/div[2]/div[1]/table[2]/tbody/tr[2]/td[3]/a[1]')
                webdriver.ActionChains(driver).move_to_element(get_pdf[0]).send_keys(Keys.ALT).click(get_pdf[0]).send_keys(Keys.ALT).perform()
            except Exception as e:
                error = 2
        if AssessmentYear[0:5]+AssessmentYear[7:9]==AY:
            downloadPath = selenium_path+'/ITR'
            driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
            params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
            command_result = driver.execute("send_command", params)

            time.sleep(1)
            try:
                back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[2]/div[2]/table/tbody/tr/td/div/a/input').click()
            except Exception as e:
                try:
                    back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[3]/table/tbody/tr/td/div/a/input').click()
                except Exception as e:
                    # back = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri_0"]')
                    try:
                        back = driver.find_element_by_xpath('//*[@id="ViewReturnsItdHdEri_0"]')
                    except Exception as e:
                        error = 2
            # back = driver.find_element_by_xpath('//*[@id="dynamicContent"]/div[2]/div[2]/table/tbody/tr/td/div/a/input').click()
            #AssessmentYear[0:5]+AssessmentYear[7:9]#2019-20

            file_arr = os.listdir(selenium_path+"/ITR")
            for file in file_arr:
                if file.find(pan[4:9])>=0:
                    my_list = file.split("_")
                    #rename = my_list[1]+"_"+my_list[2]+"_iTR_V_A_"+ITR+".pdf"
                    # ddd=datetime.now().split(" ")[0]
                    # log.info(ddd)
                    # get_financial_year(ddd)
                    rename = "ITRV_"+pan+"_RID"+str(R_id)+"_"+str(AY)+".pdf"
                    old_file = os.path.join(selenium_path+"/ITR", file)
                    new_file = os.path.join(selenium_path+"/ITR_excel", rename)
                    no_of_ITR = no_of_ITR + 1
                    log.info(rename+' no_of_ITR -> '+str(no_of_ITR))
                    log.info("file exist : "+str(os.path.exists(new_file)) )
                    # if(str(os.path.exists(new_file)) == 'False'):
                    #   os.rename(old_file, new_file)
                    # else:
                    #   os.remove(old_file)
                    os.rename(old_file, new_file)
                    os.remove(old_file)
            result['ITR_V'] = 'success'         
        else:
            result['ITR_V'] = 'fail'                
    else:
        salatclient_instance.Is_salat_client=0
        salatclient_instance.Is_ERI_reg=0
        salatclient_instance.save()
        result['status'] = 'This PAN is not added as Salat Client.'

    ERI_user_instance.status = 'inactive'
    ERI_user_instance.user_pan = ''
    ERI_user_instance.save()

    result['no_of_ITR'] = no_of_ITR
    result['pan_status'] = pan_status
    result['dob_status'] = dob_status
    result['otp_status'] = otp_status
    return result

@csrf_exempt
def multiple_eri_test1(request,data):
    log.info(data)
    pan = data.split('=')[0]
    dob = data.split('=')[1]

    dob = dob.replace('@','/')
    dob = dob.replace('@','/')
    client_id = data.split('=')[2]
    b_id = data.split('=')[3]
    R_id= data.split('=')[4]
    log.info(b_id)
    result = {}
    result['status']="start"
    driver = driver_call()
    #multiple_eri_ITR1 = multiple_eri_ITR(driver,pan,dob,client_id,b_id,request)
    multiple_eri_ITR1 = iTR_V_Acknowledgement(driver,pan,dob,client_id,b_id,R_id,request)
    # multiple_eri_ITR1 = excel_to_db(pan,37)
    if multiple_eri_ITR1 == 'error':
        log.info(multiple_eri_ITR1)
        if salatclient.objects.filter(PAN=pan).exists():
            for sc in salatclient.objects.filter(PAN=pan):
                if sc.ERI_id is None:
                    eri_username = ''
                else:
                    eri_username = sc.ERI_id
                if eri_username != '':
                    if ERI_user.objects.filter(status='active',user_id=eri_username).exists():
                        ERI_user_instance = ERI_user.objects.get(status='active',user_id=eri_username)
                        ERI_user_instance.status = 'inactive'
                        ERI_user_instance.user_pan = ''
                        ERI_user_instance.save()
        #multiple_eri_ITR1 = multiple_eri_ITR(driver,pan,dob,client_id,b_id,request)
        multiple_eri_ITR1 = iTR_V_Acknowledgement(driver,pan,dob,client_id,b_id,R_id,request)
    
    log.info(multiple_eri_ITR1)

    driver.close()
    result['status'] = multiple_eri_ITR1
    return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def multiple_eri1(request):
    log.info('multiple_eri1')
    result = {}
    if request.method=='POST':
        # client_id = request.POST.get('client_id')
        # b_id=request.POST.get('b_id')
        try:
            # pan = request.POST.get('pan')
            # dob = request.POST.get('dob')
            # R_id = request.POST.get('R_id')
            # option = request.POST.get('option')

            R_id=request.POST.get('R_id')
            b_id=request.POST.get('b_id')
            option = request.POST.get('option')
            if Return_Details.objects.filter(R_id=R_id).exists():
                return_Details_obj=Return_Details.objects.filter(R_id=R_id).latest('id')
                pan=str(return_Details_obj.P_id.pan)
                dob=str(return_Details_obj.P_id.dob).split('-')[2]+'/'+str(return_Details_obj.P_id.dob).split('-')[1]+'/'+str(return_Details_obj.P_id.dob).split('-')[0]
                log.info(dob)
                client_id=str(return_Details_obj.P_id.P_Id)

            trigger_otp = 0
            salat_registration1 = ''

            if ERI_user.objects.filter(status='active').exists():
                for eri in ERI_user.objects.filter(status='active'):
                    time_diff = timezone.now() - eri.date_time
                    if time_diff.seconds / 60 > 10 :
                        ERI_user_instance = ERI_user.objects.get(eri_pan=eri.eri_pan)
                        ERI_user_instance.status = 'inactive'
                        ERI_user_instance.save()
                        log.info(eri.eri_pan + " ERI_user Status changed to 'inactive'")

            if option == 'salatclient':
                dob = dob.replace('/','@')
                dob = dob.replace('/','@')
                # MYDIR = os.path.dirname(__file__)
                # os.system('curl https://salattax.com/multiple_eri_test/'+pan+'='+dob+'='+client_id)
                thread.start_new_thread( server_side_processing1, (pan,dob,client_id,b_id,R_id, 2, ) )
                # give Execute Permission to above file
                dob = dob.replace('@','/')
                dob = dob.replace('@','/')
            else:
                driver = driver_call()
                salat_registration1 = salat_registration_fun(driver,pan,dob,client_id,b_id)
                driver.close()
                
            result['pan'] = pan
            result['dob'] = dob
            result['option'] = option
            result['status'] = salat_registration1
        except Exception as ex:
            log.error('Error in multiple_eri1 : %s' % traceback.format_exc())
            user_error_tracking(client_id,'multiple ERI : %s' % ex,request)

            result['status'] = 'Error multiple ERI : %s' % ex
            log.error('Error in : '+traceback.format_exc())
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status'] = "Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

# Define a function for the thread
def server_side_processing1( pan,dob,client_id,b_id,R_id, delay):
    # log.info('thread')
    # log.info(thread.active_count())
    log.info('curl '+server_curl+'/multiple_eri_test1/'+pan+'='+dob+'='+client_id+'='+b_id+'='+R_id)
    os.system('curl '+server_curl+'/multiple_eri_test1/'+pan+'='+dob+'='+client_id+'='+b_id+'='+R_id)
    count = 0
    while count < 1:
        time.sleep(delay)
        count += 1
        log.info( "%s: %s" % ( pan, time.ctime(time.time()) ) )

@csrf_exempt
def test_url(request,data):
    log.info('test_url')
    result={}
    try:
        log.info(data)
        client_id=data
        if Personal_Details.objects.filter(P_Id=client_id).exists():

            pd_instance=Personal_Details.objects.get(P_Id=client_id)
            pan=pd_instance.pan
            business_partner_id=pd_instance.business_partner_id.id
            dob=pd_instance.dob

            log.info(pan)
            log.info(dob)

            if not (dob is None) and (dob !=''):
                dob_obj=datetime.strptime(str(dob),'%Y-%m-%d')
                dob=dob_obj.strftime('%d-%m-%Y')

            log.info(dob)
            dob=str(dob)

            unzip_txt_status = unzip_txt(pan,dob)
            if(unzip_txt_status == 'success'):
                excel_to_db(pan,client_id,business_partner_id)
                result['status'] = 'Success'
            else:
                result['status'] = 'Error'
                log.info('26AS unzip failed')
        else:
            result['status'] = 'Invalid Client ID'

        return HttpResponse(json.dumps(result), content_type='application/json')
    except Exception as ex:
        result['status'] = 'Error'
        log.error('Error in test_url : %s' +traceback.format_exc())
        return HttpResponse(json.dumps(result), content_type='application/json')

def run(*args):
    if args[0]=='read_ITR':
        read_ITR(args[1],args[2])
    elif args[0]=='excel_to_db':
        excel_to_db(args[1],args[2],args[3])

