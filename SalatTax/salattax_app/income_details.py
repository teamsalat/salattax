from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db.models import Avg, Max, Min, Sum
import logging
import json
import traceback

from .models import SalatTaxUser
from .models import State_Code,Income
from .models import SalatTaxUser, VI_Deductions, Client_fin_info1, Variables_80d
from .models import Client_fin_info1,tds_tcs,Shares_LT,Shares_ST, VI_Deductions, Employer_Details
from .models import House_Property_Details,Property_Owner_Details,Other_Income_Common,Personal_Details
from .models import Equity_LT,Equity_ST,Debt_MF_LT,Debt_MF_ST,Listed_Debentures_LT,Listed_Debentures_ST
from .models import TenantDetails,Return_Details,tax_variables,SalaryBreakUp,allowances,PerquisiteBreakUp,ProfitinLieuBreakUp
#for url request 
import requests
import re

from .serializer import IncomeSerializer,HousePropertyDetailsSerializer
from django.db.models import Avg, Max, Min, Sum
# import xml.etree.ElementTree as ET

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

#pincode
@csrf_exempt
def get_cityState_pin(request):
    try:
        if request.method=='POST':
            result={}
            try:
                pin=request.POST.get('pin')
                resp = requests.get("https://api.data.gov.in/resource/0a076478-3fd3-4e2c-b2d2-581876f56d77?format=json&api-key=579b464db66ec23bdd0000019f9deeedda054c8066636274a8b93734&filters[pincode]=%s" % pin)

                result['city_state']=resp.json()
            except Exception as ex:
                # result['city_state']=resp.json()
                result['status']='Error : %s' % ex

            return HttpResponse(json.dumps(result), content_type='application/json')
        else:
            result['status']="Fail"
            return HttpResponse(json.dumps(result), content_type='application/json')
    except Exception as ex:
        result['status']= 'Error in json data: %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_state(request,id):
    if SalatTaxUser.objects.filter(id=id).exists():
        salattaxid = SalatTaxUser.objects.filter(id=id).first()
    else:
        salattaxid = None

    state_list = State_Code.objects.all()
    return render(request,'income_details.html',
        {
        'state_list':state_list,
        'id':salattaxid,
        'client_id':id
        })

@csrf_exempt
def save_house_property(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)

            client_id=req_data['client_id']
            propertyID=req_data['propertyID']
            propertyType=req_data['propertyType']
            address=req_data['address']
            pin=req_data['pin']
            city=req_data['city']
            share_in_property=req_data['share_in_property'] #
            state=req_data['state']
            co_owned=req_data['co_owned']
            # co_owned_name=req_data['co_owned_name']
            # co_owned_pan=req_data['co_owned_pan']
            # percent_share=req_data['percent_share']
            municipal_tax=req_data['municipal_tax'] #
            interest_payable=req_data['interest_payable'] #
            rent_received=req_data['rent_received'] #
            Country = 'INDIA'
            if municipal_tax=='':
                municipal_tax = 0
            if interest_payable=='':
                interest_payable = 0
            if share_in_property=='':
                share_in_property = 0
            if rent_received=='':
                rent_received = 0

            result['status']= 'start'
            R_ID = get_rid(client_id)

            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                tax_variables.objects.create(
                    R_Id = R_ID,
                    house_property = 'yes',
                ).save()
            else:
                tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                tax_variables_instance.house_property = 'yes'
                tax_variables_instance.save()


            if not House_Property_Details.objects.filter(R_Id=R_ID,Property_Id=propertyID).exists():
                House_Property_Details.objects.create(
                    R_Id = R_ID,
                    Property_Id = propertyID,
                    Name_of_the_Premises_Building_Village= address,
                    Town_City = city,
                    State = state,
                    Country = Country,
                    PIN_Code = pin,
                    Your_percentage_of_share_in_Property = share_in_property,
                    Rent_Received = rent_received,
                    Type_of_Hosue_Property = propertyType,
                    Name_of_Tenant = '',
                    PAN_of_Tenant = '',
                    Property_Tax = municipal_tax,
                    Interest_on_Home_loan = interest_payable,
                ).save()
            else:
                House_Property_instance = House_Property_Details.objects.get(R_Id=R_ID,Property_Id=propertyID)
                House_Property_instance.Name_of_the_Premises_Building_Village=address
                House_Property_instance.Town_City=city
                House_Property_instance.State=state
                House_Property_instance.PIN_Code=pin
                House_Property_instance.Your_percentage_of_share_in_Property=share_in_property
                House_Property_instance.Type_of_Hosue_Property=propertyType
                House_Property_instance.Rent_Received=rent_received
                House_Property_instance.Name_of_Tenant=''
                House_Property_instance.PAN_of_Tenant=''
                House_Property_instance.Property_Tax=municipal_tax
                House_Property_instance.Interest_on_Home_loan=interest_payable
                House_Property_instance.save()

            # if co_owned=='yes':
            #   if not Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=propertyID).exists():
            #       Property_Owner_Details.objects.create(
            #           R_Id = R_ID,
            #           Property_Id = propertyID,
            #           Name_of_Co_Owner= co_owned_name,
            #           PAN_of_Co_owner = co_owned_pan,
            #           Percentage_Share_in_Property = percent_share,
            #       ).save()
            #   else:
            #       Property_Owner_instance = Property_Owner_Details.objects.get(R_Id=R_ID,Property_Id=propertyID)
            #       Property_Owner_instance.Name_of_Co_Owner=co_owned_name
            #       Property_Owner_instance.PAN_of_Co_owner=co_owned_pan
            #       Property_Owner_instance.Percentage_Share_in_Property=percent_share
            #       Property_Owner_instance.save()
            # else:
            #   if Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=propertyID).exists():
            #       Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=propertyID).delete()

            result['status']= 'success'
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % traceback.format_exc()
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_other_sources(request):
	if request.method=='POST':
		result={}
		try:
			data=request.POST.get('data')
			req_data = json.loads(data)
			client_id=req_data['client_id']
			R_ID = get_rid(client_id)

			FY=req_data['FY']
			saving_bank_int=req_data['saving_bank_int']
			fd_interest=req_data['fd_interest']
			other_interest=req_data['other_interest']
			commission=req_data['commission']
			other_income=req_data['other_income']
			family_pension=req_data['family_pension']
			post_office_interest=req_data['post_office_interest']

			R_ID = get_rid(client_id)
			return_year = '2018-2019'
			if Return_Details.objects.filter(R_id=R_ID).exists():
				Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
				return_year = Return_Details_instance.FY
			return_year = return_year.replace('-20','-')

			# if FY=='':
			# 	FY=2018
			FY = return_year
			if saving_bank_int=='':
				saving_bank_int=0
			if fd_interest=='':
				fd_interest=0
			if other_interest=='':
				other_interest=0
			if commission=='':
				commission=0
			if other_income=='':
				other_income=0
			if family_pension=='':
				family_pension=0
			if post_office_interest=='':
				post_office_interest=0

			result['status']= 'start'
			if not Other_Income_Common.objects.filter(R_Id=R_ID).exists():
				Other_Income_Common.objects.create(
					R_Id = R_ID,
					FY = FY,
					Interest_Deposits= 0,
					Interest_Savings = saving_bank_int,
					Commission = commission,
					Other_Income = other_income,
					Other_Interest = other_interest,
					family_pension = family_pension,
					post_office_interest=post_office_interest,
					FD = fd_interest,
				).save()
			else:
				Other_Income_Common_instance = Other_Income_Common.objects.get(R_Id=R_ID)
				Other_Income_Common_instance.FY=FY
				Other_Income_Common_instance.Interest_Deposits=0
				Other_Income_Common_instance.Interest_Savings=saving_bank_int
				Other_Income_Common_instance.Commission=commission
				Other_Income_Common_instance.Other_Income=other_income
				Other_Income_Common_instance.Other_Interest=other_interest
				Other_Income_Common_instance.FD=fd_interest
				Other_Income_Common_instance.family_pension=family_pension
				Other_Income_Common_instance.post_office_interest=post_office_interest
				Other_Income_Common_instance.save()

			log.info(saving_bank_int)
			log.info(fd_interest)
			log.info(post_office_interest)

			if calculate_age(client_id)<60:
				ttb=0
				tta=int(saving_bank_int)
			else:
				ttb=int(saving_bank_int)+int(fd_interest)+int(post_office_interest)
				tta=0

			if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
				VI_Deductions.objects.create(
					R_Id = R_ID,
					VI_80_TTA = tta,
					VI_80_TTB = ttb
				).save()
			else:
				Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)
				Deductions_instance.VI_80_TTA = tta
				Deductions_instance.VI_80_TTB = ttb
				Deductions_instance.save()

			result['status']= 'success'
			result['req_data']= req_data
		except Exception as ex:
			result['status']= 'Error in other source api: %' + traceback.format_exc()
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')


def calculate_age(client_id):
	age=''
	try:
		for client in Personal_Details.objects.filter(P_Id = client_id):
			if client.dob is None:
				dob = ''
			else:
				dob = client.dob.strftime('%d-%m-%Y')

			if dob == '':
				age = 50
			else:
				age1 = dob.split('-')[2]
				age = 2019 - int(age1)

		return age
	except Exception as ex:
		log.info('Error in caculating age '+ex)

@csrf_exempt
def save_Employer_Details(request):
    log.info('save employer details')
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)
            client_id=req_data['client_id']
            emp_name=req_data['emp_name']
            c_tan=req_data['c_tan']
            c_pan=req_data['c_pan']
            c_address=req_data['c_address']
            c_pin=req_data['c_pin']
            c_city=req_data['c_city']
            c_state=req_data['c_state']
            # c_salary = 0
            c_salary = int( req_data['c_salary'] )
            c_perq = int( req_data['c_perq'] )
            c_profits_lieu = int( req_data['c_profits_lieu'] )
            c_Ptax = int( req_data['c_Ptax'] )
            c_Eallowance = int( req_data['c_Eallowance'] )
            c_lta = int( req_data['c_lta'] )
            c_hra = int( req_data['c_hra'] )
            c_OtherAllowance = int( req_data['c_OtherAllowance'] )
            log.info(c_Eallowance)
            c_employer_category = req_data['c_employer_category']
            earn_salary=req_data["earn_salary"]
            log.info(earn_salary)

            c_salary1 = 0
            if Client_fin_info1.objects.filter(client_id = client_id,company_name=emp_name).exclude(flag=2).exists():
                for form16 in Client_fin_info1.objects.filter(client_id = client_id,company_name=emp_name).exclude(flag=2):
                    # log.info(form16.salary_as_per_provision)
                    c_salary1 = float(form16.salary_as_per_provision or 0)
            else:
                c_salary1 = c_salary

            log.info('salary '+str(c_salary1))

            result['status']= 'start'
            personal_instance=Personal_Details.objects.get(P_Id=client_id)
            
            R_ID = get_rid(client_id)
            return_year = '2018-2019'
            if Return_Details.objects.filter(R_id=R_ID).exists():
                Return_Details_instance=Return_Details.objects.filter(R_id=R_ID).order_by('-updated_time')[0]
                return_year = Return_Details_instance.FY
            return_year = return_year.replace('-20','-')

            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                # log.info('salary income set to yes')
                tax_variables.objects.create(
                    R_Id = R_ID,
                    salary_income = earn_salary,
                ).save()
            else:
                # log.info('salary income set to yes')
                tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                tax_variables_instance.salary_income = earn_salary
                tax_variables_instance.save()

            if(earn_salary=='yes'):
                if Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=emp_name).exists():
                    Employer_Details_instance = Employer_Details.objects.get(P_id=personal_instance,Name_of_employer=emp_name)
                    Employer_Details_instance.PAN_of_employer = c_pan
                    Employer_Details_instance.TAN_of_employer = c_tan
                    Employer_Details_instance.Address_of_employer=c_address
                    Employer_Details_instance.Employer_pin=c_pin
                    Employer_Details_instance.Employer_city=c_city
                    Employer_Details_instance.Employer_state=c_state
                    Employer_Details_instance.Employer_category=c_employer_category
                    Employer_Details_instance.save()
                    result['Employer_Details status'] = 'exists'
                else:
                    if Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer='',PAN_of_employer=None,TAN_of_employer=None,Address_of_employer=None,Employer_city=None,Employer_pin=None).exists():
                        Employer_Details_instance = Employer_Details.objects.get(P_id=personal_instance,Name_of_employer='',PAN_of_employer=None,TAN_of_employer=None,Address_of_employer=None,Employer_city=None,Employer_pin=None)
                        Employer_Details_instance.FY = return_year
                        Employer_Details_instance.Name_of_employer = emp_name
                        Employer_Details_instance.PAN_of_employer = c_pan
                        Employer_Details_instance.TAN_of_employer = c_tan
                        Employer_Details_instance.Address_of_employer=c_address
                        Employer_Details_instance.Employer_pin=c_pin
                        Employer_Details_instance.Employer_city=c_city
                        Employer_Details_instance.Employer_state=c_state
                        Employer_Details_instance.Employer_category=c_employer_category
                        Employer_Details_instance.save()
                        result['Employer_Details status']= 'exists'
                    else:
                        Employer_Details.objects.create(
                            P_id=personal_instance,
                            FY=return_year,
                            Name_of_employer=emp_name,
                            PAN_of_employer=c_pan,
                            TAN_of_employer=c_tan,
                            Address_of_employer= c_address,
                            Employer_category = c_employer_category,
                            residential_status = 'RES',
                            Employer_state = c_state,
                            Employer_pin = c_pin,
                            Employer_city = c_city,
                        ).save()
                        result['Employer_Details status']= 'not exists'
                if not Income.objects.filter(R_Id=R_ID,company_name=emp_name).exists():
                    Income.objects.create(
                        R_Id = R_ID,
                        company_name = emp_name,
                        Salary = c_salary1,
                        Value_of_perquisites = c_perq,
                        Profits_in_lieu_of_salary = c_profits_lieu,
                        Entertainment_Allowance = c_Eallowance,
                        LTA = c_lta,
                        Form16_HRA = c_hra,
                        Other_allowances = c_OtherAllowance,
                        Profession_Tax = c_Ptax,
                    ).save()
                    result['Income status']= 'not exists'
                else:
                    Income_instance = Income.objects.get(R_Id=R_ID,company_name=emp_name)
                    Income_instance.Salary = c_salary
                    Income_instance.Value_of_perquisites = c_perq
                    Income_instance.Profits_in_lieu_of_salary = c_profits_lieu
                    Income_instance.Entertainment_Allowance = c_Eallowance
                    Income_instance.LTA = c_lta
                    Income_instance.Form16_HRA = c_hra
                    Income_instance.Other_allowances = c_OtherAllowance
                    Income_instance.Profession_Tax = c_Ptax
                    Income_instance.save()
                    result['Income status']= 'exists'
            else:
                log.info('no company data')
                
            result['status']= 'success'
            result['R_ID'] = R_ID
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def get_city(resp):
    try:
        if len(resp['records']) == 0:
            return ''
        else:
            return resp['records'][0]['taluk']
    except Exception as ex:
        return ''

@csrf_exempt
def edit_Employer_Details(request):
    log.info('edit employer details')
    if request.method=='POST':
        result={}
        try:
            option = request.POST.get('option')
            client_id = request.POST.get('client_id')
            #form16_no = request.POST.get('form16_no')
            c_name, state, c_pincode = ('',)*3

            state_list = State_Code.objects.all()
            personal_instance=Personal_Details.objects.get(P_Id=client_id)

            result['status']= 'start'
            if option == 'add':
                R_ID = get_rid(client_id)
                if Client_fin_info1.objects.filter(client_id = client_id).exclude(flag=2).exists():
                    form16 = Client_fin_info1.objects.filter(client_id = client_id).exclude(flag=2).latest('time_stamp')
                    c_name = form16.company_name
                    for s in state_list:
                        if re.search(s.state, form16.company_address, re.IGNORECASE):
                            state = s.state
                    pin_list = re.findall(r"\d{6}", form16.company_address)
                    c_pincode = pin_list[0]
                    c_city=''
                    #if c_pincode != '':
                    if c_pincode == '1':
                        resp = requests.get("https://api.data.gov.in/resource/0a076478-3fd3-4e2c-b2d2-581876f56d77?format=json&api-key=579b464db66ec23bdd0000019f9deeedda054c8066636274a8b93734&filters[pincode]=%s" % c_pincode)
                        c_city = get_city(resp.json())

                    if not Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=c_name).exists():
                        Employer_Details.objects.create(
                            P_id=personal_instance,
                            #F16_no=form16_no,
                            FY=form16.ay,
                            Name_of_employer=form16.company_name,
                            PAN_of_employer= form16.company_pan,
                            TAN_of_employer= form16.company_tan,
                            Address_of_employer= (form16.company_address or ''),
                            Employer_category = 'Other',
                            residential_status = 'RES',
                            Employer_state = state,
                            Employer_pin = c_pincode,
                            Employer_city = c_city,
                        ).save()
                    if not Income.objects.filter(R_Id=R_ID,company_name=form16.company_name).exists():
                        Income.objects.create(
                            R_Id = R_ID,
                            company_name = form16.company_name,
                            Salary = float(form16.salary_as_per_provision or 0),
                            Value_of_perquisites = form16.value_of_perquisites,
                            Profits_in_lieu_of_salary = form16.profits_in_lieu_of_salary,
                            Entertainment_Allowance = form16.entertainment_allowance,
                            LTA = 0,
                            Form16_HRA = (form16.hra_allowed or 0),
                            Other_allowances = (form16.conveyance_allowance_claimed or 0), ##conveyance_allowance_claimed==Total_allowance
                            Profession_Tax = form16.profession_tax,
                        ).save()
                    if not allowances.objects.filter(R_id=R_ID,company_name=form16.company_name).exists():
                        
                        allowance_lta=form16.lta
                        allowance_hra=form16.hra_claimed
                        allowance_lse=form16.lse

                        if allowance_lta!=None:
                            allowance_lta=int(allowance_lta)
                        if allowance_hra!=None:
                            allowance_hra=int(allowance_hra)
                        if allowance_lse!=None:
                            allowance_lse=int(allowance_lse)

                        allowances.objects.create(
                            R_id = R_ID,
                            company_name = form16.company_name,
                            lta = str(allowance_lta or '0'),
                            hra = str(allowance_hra or '0'),
                            earned_leave_encashment = str(allowance_lse or '0'),
                        ).save()

                    # log.info('allowances1')
                    # log.info(R_ID)
                    # log.info(form16.company_name)
                    # if not allowances.objects.filter(R_id=R_ID,company_name=form16.company_name).exists():
                    #   log.info('allowances2')
                    #   allowances.objects.create(
                    #       R_id=R_ID,
                    #       company_name=company_name,
                    #       other_allowance=(form16.conveyance_allowance_claimed or 0),
                    #   ).save()
                    #   log.info('allowances3')
                    # else:
                    #   log.info('allowances4')
                    #   allowances_obj=allowances.objects.filter(R_Id=R_ID,company_name=form16.company_name)
                    #   log.info(allowances_obj.count())

                    result['status']= 'success'
                else:
                    result['status']= 'NO Entry in Client_fin_info1'

                ###### Update VI_Deductions ########################
                if Employer_Details.objects.filter(P_id=personal_instance).exists():
                    Employer_Obj=Employer_Details.objects.filter(P_id=personal_instance)

                    S_80C=0
                    S_80CCD=0
                    S_80D=0
                    S_80G=0
                    S_80DD=0
                    S_80CCD1=0
                    S_80CCD2=0

                    for employer in Employer_Obj:

                        employer_name=employer.Name_of_employer
                        if Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).exists():
                            form16 = Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).latest('time_stamp')
                            ###According to new update superannuation=80C
                            ###According to new update number_80d_self_family=80D
                            S80C = form16.superannuation
                            S80D = form16.number_80d_self_family

                            if S80C!=None:
                                S_80C+=S80C
                            if S80D!=None:
                                S_80D+=S80D

                    if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
                        VI_Deductions.objects.create(
                            R_Id = R_ID,
                            VI_80_C = S_80C,
                            VI_80_CCD_1B= S_80CCD,
                            VI_80_D = S_80D,
                            VI_80_G = S_80G,
                            VI_80_DD = S_80DD,
                            VI_80_CCD_1 = S_80CCD1,
                            VI_80_CCD_2 = S_80CCD2,
                        ).save()
                    else:
                        Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)
                        Deductions_instance.VI_80_C = S_80C
                        Deductions_instance.VI_80_D = S_80D
                        Deductions_instance.save()

                    ############# Insert/Update 80D in Variables_80d ##########

                    S_HealthCheckup=0
                    P_HealthCheckup=0
                    S_InsurancePremium=S_80D
                    P_InsurancePremium=0
                    S_MedicalExpenditure=0
                    P_MedicalExpenditure=0

                    if not Variables_80d.objects.filter(R_Id=R_ID).exists():
                        Variables_80d.objects.create(
                            R_Id = R_ID,
                            self_health_checkup = S_HealthCheckup,
                            parent_health_checkup= P_HealthCheckup,
                            self_insurance_premium = S_InsurancePremium,
                            parent_insurance_premium = P_InsurancePremium,
                            self_medical_expenditure = S_MedicalExpenditure,
                            parent_medical_expenditure = P_MedicalExpenditure,
                        ).save()
                    else:
                        Variables_80d_inst = Variables_80d.objects.get(R_Id=R_ID)
                        Variables_80d_inst.self_insurance_premium = S_InsurancePremium
                        Variables_80d_inst.save()

               
                ###create r_id if not exist
                max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
                if max_r_id['R_id__max']==None:
                    latest_r_id=0
                else:
                    latest_r_id=max_r_id['R_id__max']+1
                    
                if not Return_Details.objects.filter(P_id=personal_instance).exists():
                    Return_Details.objects.create(
                        R_id = latest_r_id,
                        P_id = personal_instance,
                        FY = '2018-2019',
                        AY = '2019-2020',
                    ).save()
                    log.info("Return_Details Entry Created")
                else:
                    Return_Details_instance=Return_Details.objects.filter(P_id=personal_instance).order_by('-updated_time')[0]
                    log.info('Return ID : ' + str(Return_Details_instance.R_id) )

                ##############################################

            if option == 'delete':
                company_name = request.POST.get('company_name')
                if Client_fin_info1.objects.filter(client_id=client_id,company_name=company_name,flag=2).exists():
                    Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=company_name).delete()
                    
                    if Income.objects.filter(R_Id=R_ID,company_name=company_name).exists():
                        Income.objects.filter(R_Id=R_ID,company_name=company_name).delete()
                    
                    if not allowances.objects.filter(R_id=R_ID,company_name=company_name).exists():
                        allowances.objects.filter(R_id=R_ID,company_name=company_name).delete()

                    option = 'deleted'

                result['status']= 'success'

            if option == 'delete_all':
                R_ID = get_rid(client_id)
                if Employer_Details.objects.filter(P_id=personal_instance).exists():
                    Employer_Details.objects.filter(P_id=personal_instance).delete()
                if Income.objects.filter(R_Id=R_ID).exists():
                    Income.objects.filter(R_Id=R_ID).delete()
                if allowances.objects.filter(R_id=R_ID).exists():
                    allowances.objects.filter(R_id=R_ID).delete()
                if not tax_variables.objects.filter(R_Id=R_ID).exists():
                    # log.info('salary income set to no')
                    tax_variables.objects.create(
                        R_Id = R_ID,
                        salary_income = 'no',
                    ).save()
                else:
                    # log.info('salary income set to no')
                    tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                    tax_variables_instance.salary_income = 'no'
                    tax_variables_instance.save()
                
                ###Delete All Form16 data
                Client_fin_info1.objects.filter(client_id = client_id,flag=0).update(flag=2)

                result['status']= 'Deleted'


            ##Check_form16_is_exist_or_not  
            form16_exist=0  
            if Employer_Details.objects.filter(P_id=personal_instance).exists():
                Employer_Obj=Employer_Details.objects.filter(P_id=personal_instance)        
                for employer in Employer_Obj:
                    employer_name=employer.Name_of_employer
                    if Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name,flag=0).exists():
                        form16_exist=1


            result['form16_exist']= form16_exist            
            result['option']= option
            result['client_id']= client_id
            result['c_name']= c_name
        except Exception as ex:
            log.error('Error in edit_Employer_Details : '+request.get_host()+': '+traceback.format_exc())
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_shares(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)
            client_id=req_data['client_id']
            log.info(client_id)
            R_ID = get_rid(client_id)

            st_shares_tpv=req_data['st_shares_tpv']
            st_shares_tsv=req_data['st_shares_tsv']
            st_shares_gain=req_data['st_shares_gain']
            lt_shares_tpv=req_data['lt_shares_tpv']
            lt_shares_tsv=req_data['lt_shares_tsv']
            lt_shares_gain=req_data['lt_shares_gain']

            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                tax_variables.objects.create(
                    R_Id = R_ID,
                    capital_gain = 'yes',
                ).save()
            else:
                tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                tax_variables_instance.capital_gain = 'yes'
                tax_variables_instance.save()


            result['status']= 'start'
            if not Shares_ST.objects.filter(R_Id=R_ID).exists():
                Shares_ST.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = st_shares_tsv,
                    Cost_of_Acquisition= st_shares_tpv,
                    Expenditure = st_shares_gain,
                ).save()
            else:
                Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
                Shares_ST_instance.Total_Sell_Value=st_shares_tsv
                Shares_ST_instance.Cost_of_Acquisition=st_shares_tpv
                Shares_ST_instance.Expenditure=st_shares_gain
                Shares_ST_instance.save()

            if not Shares_LT.objects.filter(R_Id=R_ID).exists():
                Shares_LT.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = lt_shares_tsv,
                    Cost_of_Acquisition= lt_shares_tpv,
                    Expenditure = lt_shares_gain,
                ).save()
            else:
                Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
                Shares_LT_instance.Total_Sell_Value=lt_shares_tsv
                Shares_LT_instance.Cost_of_Acquisition=lt_shares_tpv
                Shares_LT_instance.Expenditure=lt_shares_gain
                Shares_LT_instance.save()

            result['status']= 'success'
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_equity(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)
            client_id=req_data['client_id']
            R_ID = get_rid(client_id)

            st_equity_tpv=req_data['st_equity_tpv']
            st_equity_tsv=req_data['st_equity_tsv']
            st_equity_gain=req_data['st_equity_gain']
            lt_equity_tpv=req_data['lt_equity_tpv']
            lt_equity_tsv=req_data['lt_equity_tsv']
            lt_equity_gain=req_data['lt_equity_gain']

            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                tax_variables.objects.create(
                    R_Id = R_ID,
                    capital_gain = 'yes',
                ).save()
            else:
                tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                tax_variables_instance.capital_gain = 'yes'
                tax_variables_instance.save()

            result['status']= 'start'
            if not Equity_ST.objects.filter(R_Id=R_ID).exists():
                Equity_ST.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = st_equity_tsv,
                    Cost_of_Acquisition= st_equity_tpv,
                    Expenditure = st_equity_gain,
                ).save()
            else:
                equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
                equity_ST_instance.Total_Sell_Value=st_equity_tsv
                equity_ST_instance.Cost_of_Acquisition=st_equity_tpv
                equity_ST_instance.Expenditure=st_equity_gain
                equity_ST_instance.save()

            if not Equity_LT.objects.filter(R_Id=R_ID).exists():
                Equity_LT.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = lt_equity_tsv,
                    Cost_of_Acquisition= lt_equity_tpv,
                    Expenditure = lt_equity_gain,
                ).save()
            else:
                equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
                equity_LT_instance.Total_Sell_Value=lt_equity_tsv
                equity_LT_instance.Cost_of_Acquisition=lt_equity_tpv
                equity_LT_instance.Expenditure=lt_equity_gain
                equity_LT_instance.save()

            result['status']= 'success'
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_debt_MF(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)
            client_id=req_data['client_id']
            R_ID = get_rid(client_id)

            st_debt_MF_tpv=req_data['st_debt_MF_tpv']
            st_debt_MF_tsv=req_data['st_debt_MF_tsv']
            st_debt_MF_gain=req_data['st_debt_MF_gain']
            lt_debt_MF_tpv=req_data['lt_debt_MF_tpv']
            lt_debt_MF_tsv=req_data['lt_debt_MF_tsv']
            lt_debt_MF_gain=req_data['lt_debt_MF_gain']

            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                tax_variables.objects.create(
                    R_Id = R_ID,
                    capital_gain = 'yes',
                ).save()
            else:
                tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                tax_variables_instance.capital_gain = 'yes'
                tax_variables_instance.save()

            result['status']= 'start'
            if not Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
                Debt_MF_ST.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = st_debt_MF_tsv,
                    Cost_of_Acquisition= st_debt_MF_tpv,
                    Expenditure = st_debt_MF_gain,
                ).save()
            else:
                Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
                Debt_MF_ST_instance.Total_Sell_Value=st_debt_MF_tsv
                Debt_MF_ST_instance.Cost_of_Acquisition=st_debt_MF_tpv
                Debt_MF_ST_instance.Expenditure=st_debt_MF_gain
                Debt_MF_ST_instance.save()

            if not Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
                Debt_MF_LT.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = lt_debt_MF_tsv,
                    Cost_of_Acquisition= lt_debt_MF_tpv,
                    Expenditure = lt_debt_MF_gain,
                ).save()
            else:
                Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
                Debt_MF_LT_instance.Total_Sell_Value=lt_debt_MF_tsv
                Debt_MF_LT_instance.Cost_of_Acquisition=lt_debt_MF_tpv
                Debt_MF_LT_instance.Expenditure=lt_debt_MF_gain
                Debt_MF_LT_instance.save()

            result['status']= 'success'
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_listed_d(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)

            client_id=req_data['client_id']
            R_ID = get_rid(client_id)

            st_listed_d_tpv=req_data['st_listed_d_tpv']
            st_listed_d_tsv=req_data['st_listed_d_tsv']
            st_listed_d_gain=req_data['st_listed_d_gain']
            lt_listed_d_tpv=req_data['lt_listed_d_tpv']
            lt_listed_d_tsv=req_data['lt_listed_d_tsv']
            lt_listed_d_gain=req_data['lt_listed_d_gain']

            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                tax_variables.objects.create(
                    R_Id = R_ID,
                    capital_gain = 'yes',
                ).save()
            else:
                tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                tax_variables_instance.capital_gain = 'yes'
                tax_variables_instance.save()

            result['status']= 'start'
            if not Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
                Listed_Debentures_ST.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = st_listed_d_tsv,
                    Cost_of_Acquisition= st_listed_d_tpv,
                    Expenditure = st_listed_d_gain,
                ).save()
            else:
                listed_d_ST_instance = Listed_Debentures_ST.objects.get(R_Id=R_ID)
                listed_d_ST_instance.Total_Sell_Value=st_listed_d_tsv
                listed_d_ST_instance.Cost_of_Acquisition=st_listed_d_tpv
                listed_d_ST_instance.Expenditure=st_listed_d_gain
                listed_d_ST_instance.save()

            if not Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
                Listed_Debentures_LT.objects.create(
                    R_Id = R_ID,
                    Total_Sell_Value = lt_listed_d_tsv,
                    Cost_of_Acquisition= lt_listed_d_tpv,
                    Expenditure = lt_listed_d_gain,
                ).save()
            else:
                listed_d_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
                listed_d_LT_instance.Total_Sell_Value=lt_listed_d_tsv
                listed_d_LT_instance.Cost_of_Acquisition=lt_listed_d_tpv
                listed_d_LT_instance.Expenditure=lt_listed_d_gain
                listed_d_LT_instance.save()

            result['status']= 'success'
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_form16_data(request):
    try:
        result={}
        if request.method=='POST':
            try:
                data=request.POST.get('data')
                client_id=request.POST.get('client_id')
                no_of_company = 0
                # no_of_company = Client_fin_info1.objects.filter(client_id=client_id).exclude(flag=2).count()
                name = []
                PAN = []
                TAN = []
                address = []
                state = []
                pin = []
                city = []
                salary = []
                vop = []
                profit_lieu = []
                p_tax = []
                entertainment_a = []
                LTA = []
                HRA = []
                other = []
                calculated_salary = []
                date = []
                employer_category=[]

                log.info(client_id)
                personal_instance=Personal_Details.objects.get(P_Id=client_id)
                R_ID = get_rid(client_id)
                # for form16 in Client_fin_info1.objects.filter(client_id = client_id).exclude(flag=2):
                    # temp_hra = float(form16.hra_allowed or 0)
                    # temp_lta = float(form16.lta or 0)
                    # # temp_other = float(form16.other_allowance or 0)
                    # temp_other = float(form16.other_allowance or 0) + float(form16.medical_allowance or 0) + float(form16.conveyance_allowance_allowed or 0)
                    # name.append(form16.company_name)
                    # ED_address, ED_state, ED_pin, ED_city, ED_TAN = ('',)*5
                    # for ED in Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=form16.company_name):
                    #   ED_address = ED.Address_of_employer
                    #   ED_state = ED.Employer_state
                    #   ED_pin = ED.Employer_pin
                    #   ED_city = ED.Employer_city
                    #   ED_TAN = ED.PAN_of_employer
                    # I_Salary, I_vop, I_profit_lieu, I_Eallowance, I_lta, I_hra, I_otherA, I_pTax = (0,)*8
                    # for I in Income.objects.filter(R_Id=client_id,company_name=form16.company_name):
                    #   I_Salary = I.Salary
                    #   I_vop = I.Value_of_perquisites
                    #   I_profit_lieu = I.Profits_in_lieu_of_salary
                    #   I_Eallowance = I.Entertainment_Allowance
                    #   I_lta = I.LTA
                    #   I_hra = I.Form16_HRA
                    #   I_otherA = I.Other_allowances
                    #   I_pTax = I.Profession_Tax
                    # PAN.append(form16.employee_pan)
                    # TAN.append(ED_TAN)
                    # address.append(ED_address)
                    # state.append(ED_state)
                    # pin.append(ED_pin)
                    # city.append(ED_city)
                    # salary.append( float(I_Salary or 0) )
                    # # salary.append( float(form16.salary_as_per_provision or 0)-abs(temp_hra-temp_lta-temp_other) )
                    # vop.append( float(I_vop or 0) )
                    # # vop.append( float(form16.value_of_perquisites or 0) )
                    # profit_lieu.append( float(I_profit_lieu or 0) )
                    # p_tax.append( float(I_pTax or 0) )
                    # entertainment_a.append( float(I_Eallowance or 0) )
                    # LTA.append( I_lta )
                    # HRA.append( I_hra )
                    # other.append( I_otherA )
                if Employer_Details.objects.filter(P_id=personal_instance).exists():
                    # log.info('exists')
                    for ED in Employer_Details.objects.filter(P_id=personal_instance):
                        name.append(ED.Name_of_employer or '')
                        TAN.append(ED.TAN_of_employer or '')
                        PAN.append(ED.PAN_of_employer or '')
                        city.append(ED.Employer_city or '')
                        address.append(ED.Address_of_employer or '')
                        state.append(ED.Employer_state or '')
                        pin.append(ED.Employer_pin or '')
                        date.append(str(ED.created_time) or '')
                        employer_category.append(str(ED.Employer_category) or '')
                        no_of_company += 1
                        if Income.objects.filter(R_Id=R_ID,company_name=ED.Name_of_employer).exists():
                            for I in Income.objects.filter(R_Id=R_ID,company_name=ED.Name_of_employer):
                                # calculated_salary.append(float(I.Salary or 0)+abs(float(I.Value_of_perquisites or 0)+float(I.Profits_in_lieu_of_salary or 0))-abs(float(I.Form16_HRA or 0)-float(I.LTA or 0) - float(I.Other_allowances or 0) ) )
                                # abs () change - to + for correction only (05apr2019) 
                                calculated_salary.append(float(I.Salary or 0)+abs(float(I.Value_of_perquisites or 0)+float(I.Profits_in_lieu_of_salary or 0))-abs(float(I.Other_allowances or 0) ) )
                                # log.info(float(I.Salary or 0)+abs(float(I.Value_of_perquisites or 0)+float(I.Profits_in_lieu_of_salary or 0))-abs(float(I.Form16_HRA or 0))-abs(float(I.LTA or 0)) - abs(float(I.Other_allowances or 0) ) )
                                # log.info(str(I.Salary or 0)+' + '+str(abs(float(I.Value_of_perquisites or 0)+float(I.Profits_in_lieu_of_salary or 0)))+' , '+str(abs(float(I.Form16_HRA or 0)-float(I.LTA or 0) - float(I.Other_allowances or 0) )) )
                                # calculated_salary = float(form16.salary_as_per_provision or 0)-abs(c_hra-c_lta-c_OtherAllowance)
                                salary.append(float(I.Salary or 0) )
                                vop.append( float(I.Value_of_perquisites or 0) )
                                profit_lieu.append( float(I.Profits_in_lieu_of_salary or 0) )
                                p_tax.append( float(I.Profession_Tax or 0) )
                                entertainment_a.append( float(I.Entertainment_Allowance or 0) )
                                LTA.append( float(I.LTA or 0) )
                                HRA.append( float(I.Form16_HRA or 0) )
                                other.append( float(I.Other_allowances or 0) )
                        else:
                            salary.append( 0 )
                            calculated_salary.append( 0 )
                            vop.append( 0 )
                            profit_lieu.append( 0 )
                            p_tax.append( 0 )
                            entertainment_a.append( 0 )
                            LTA.append( 0 )
                            HRA.append( 0 )
                            other.append( 0 )
                else:
                    # log.info('doesnot exists')
                    no_of_company=0

                # PAN.append(form16.employee_pan)
                # salary.append( float(form16.salary_as_per_provision or 0)-abs(temp_hra-temp_lta-temp_other) )
                # salary1.append( float(form16.salary_as_per_provision or 0) )
                
                result['status']='success'
                result['no_of_company']= no_of_company
                result['company_name']= name
                result['PAN']= PAN
                result['R_ID'] =R_ID
                result['TAN']= TAN
                result['address']= address
                result['state']= state
                result['pin']= pin
                result['city']= city
                result['salary']= salary
                result['calculated_salary']= calculated_salary
                result['vop']= vop
                result['profit_lieu']= profit_lieu
                result['p_tax']= p_tax
                result['entertainment_a']= entertainment_a
                result['LTA']= LTA
                result['HRA']= HRA
                result['other']= other
                result['date']= date
                result['employer_category']= employer_category
            except Exception as ex:
                result['status']='Error : %s' % ex
            return HttpResponse(json.dumps(result), content_type='application/json')
        else:
            result['status']="Fail"
            return HttpResponse(json.dumps(result), content_type='application/json')
    except Exception as ex:
        result['status']= 'Error in json data: %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_other_sources(request):
	try:
		if request.method=='POST':
			result={}
			try:
				client_id =request.POST.get('client_id')
				pan = ''
				if Personal_Details.objects.filter(P_Id = client_id).exists():
					for client in Personal_Details.objects.filter(P_Id = client_id):
						pan = client.pan

				R_ID = get_rid(client_id)

				Interest_Savings = 0
				FD = 0
				other_interest = 0
				Commission = 0
				Other_Income = 0
				family_pension = 0
				post_office_interest = 0

				if not Other_Income_Common.objects.filter(R_Id=R_ID).exists():
					fd_interest_from_tds=0
					other_interest_from_tds=0

					bank_list=['bank','BANK']
					tds_data=tds_tcs.objects.filter(R_Id=R_ID,section='194A')
					for data in tds_data:
						deductor_name=data.Deductor_Collector_Name
						amount=data.amount_paid
						if any(keyword in deductor_name for keyword in bank_list):
							fd_interest_from_tds=fd_interest_from_tds+amount
						else:
							other_interest_from_tds=fd_interest_from_tds+amount

					if fd_interest_from_tds!=0 and other_interest_from_tds!=0:
						Other_Income_Common.objects.create(
							R_Id=R_ID,
							Interest_Savings=other_interest_from_tds,
							FD=fd_interest_from_tds,
							Commission=0,
							Other_Income=0,
							family_pension=0,
							post_office_interest=0

						).save()

				log.info(R_ID)
				if Other_Income_Common.objects.filter(R_Id=R_ID).exists():
					for OIC in Other_Income_Common.objects.filter(R_Id=R_ID):
						Interest_Savings += OIC.Interest_Savings
						Commission = OIC.Commission
						Other_Income = OIC.Other_Income
						other_interest = OIC.Other_Interest
						FD = OIC.FD
						family_pension = OIC.family_pension
						post_office_interest=OIC.post_office_interest

				# if other_interest==0:
				# 	for tds in tds_tcs.objects.filter(R_Id = R_ID):
				# 		if tds.section=='194A':
				# 			other_interest += float(tds.amount_paid or 0)

				result['pan'] = pan
				result['R_ID'] = R_ID
				result['status'] = 'success'
				result['Interest_Savings']= Interest_Savings
				# result['FD']= FD
				# result['other_interest']= other_interest
				result['FD']= FD
				result['other_interest']= other_interest
				result['Commission']= Commission
				result['Other_Income']= Other_Income
				result['family_pension']= family_pension
				result['post_office_interest']= post_office_interest
			except Exception as ex:
				result['status']='Error : %s' % ex
			return HttpResponse(json.dumps(result), content_type='application/json')
		else:
			result['status']="Fail"
			return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		result['status']= 'Error in json data: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_capital_gain(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            R_ID = get_rid(client_id)

            shares, st_shares_tpv, st_shares_tsv, st_shares_gain= (0,)*4
            lt_shares_tpv, lt_shares_tsv, lt_shares_gain= (0,)*3
            equity, st_equity_tpv, st_equity_tsv, st_equity_gain = (0,)*4
            lt_equity_tpv, lt_equity_tsv, lt_equity_gain = (0,)*3
            debt_MF, st_debt_MF_tpv, st_debt_MF_tsv, st_debt_MF_gain = (0,)*4
            lt_debt_MF_tpv, lt_debt_MF_tsv, lt_debt_MF_gain = (0,)*3
            listed_d, st_listed_d_tpv, st_listed_d_tsv, st_listed_d_gain = (0,)*4
            lt_listed_d_tpv, lt_listed_d_tsv, lt_listed_d_gain = (0,)*3

            if Shares_ST.objects.filter(R_Id=R_ID).exists():
                shares = 1
                Shares_ST_instance = Shares_ST.objects.get(R_Id=R_ID)
                st_shares_tpv= Shares_ST_instance.Cost_of_Acquisition
                st_shares_tsv= Shares_ST_instance.Total_Sell_Value
                st_shares_gain= Shares_ST_instance.Expenditure

            if Shares_LT.objects.filter(R_Id=R_ID).exists():
                shares = 1
                Shares_LT_instance = Shares_LT.objects.get(R_Id=R_ID)
                lt_shares_tpv= Shares_LT_instance.Cost_of_Acquisition
                lt_shares_tsv= Shares_LT_instance.Total_Sell_Value
                lt_shares_gain= Shares_LT_instance.Expenditure

            if Equity_ST.objects.filter(R_Id=R_ID).exists():
                equity = 1
                Equity_ST_instance = Equity_ST.objects.get(R_Id=R_ID)
                st_equity_tpv= Equity_ST_instance.Cost_of_Acquisition
                st_equity_tsv= Equity_ST_instance.Total_Sell_Value
                st_equity_gain= Equity_ST_instance.Expenditure

            if Equity_LT.objects.filter(R_Id=R_ID).exists():
                equity = 1
                Equity_LT_instance = Equity_LT.objects.get(R_Id=R_ID)
                lt_equity_tpv= Equity_LT_instance.Cost_of_Acquisition
                lt_equity_tsv= Equity_LT_instance.Total_Sell_Value
                lt_equity_gain= Equity_LT_instance.Expenditure

            if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
                debt_MF = 1
                Debt_MF_ST_instance = Debt_MF_ST.objects.get(R_Id=R_ID)
                st_debt_MF_tpv= Debt_MF_ST_instance.Cost_of_Acquisition
                st_debt_MF_tsv= Debt_MF_ST_instance.Total_Sell_Value
                st_debt_MF_gain= Debt_MF_ST_instance.Expenditure

            if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
                debt_MF = 1
                Debt_MF_LT_instance = Debt_MF_LT.objects.get(R_Id=R_ID)
                lt_debt_MF_tpv= Debt_MF_LT_instance.Cost_of_Acquisition
                lt_debt_MF_tsv= Debt_MF_LT_instance.Total_Sell_Value
                lt_debt_MF_gain= Debt_MF_LT_instance.Expenditure

            if Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
                listed_d = 1
                Listed_Debentures_ST_instance = Listed_Debentures_ST.objects.get(R_Id=R_ID)
                st_listed_d_tpv= Listed_Debentures_ST_instance.Cost_of_Acquisition
                st_listed_d_tsv= Listed_Debentures_ST_instance.Total_Sell_Value
                st_listed_d_gain= Listed_Debentures_ST_instance.Expenditure

            if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
                listed_d = 1
                Listed_Debentures_LT_instance = Listed_Debentures_LT.objects.get(R_Id=R_ID)
                lt_listed_d_tpv= Listed_Debentures_LT_instance.Cost_of_Acquisition
                lt_listed_d_tsv= Listed_Debentures_LT_instance.Total_Sell_Value
                lt_listed_d_gain= Listed_Debentures_LT_instance.Expenditure

            result['status']= 'success'
            result['client_id']= client_id
            result['shares']= shares
            result['st_shares_tpv']= st_shares_tpv
            result['st_shares_tsv']= st_shares_tsv
            result['st_shares_gain']= st_shares_gain
            result['lt_shares_tpv']= lt_shares_tpv
            result['lt_shares_tsv']= lt_shares_tsv
            result['lt_shares_gain']= lt_shares_gain
            result['equity']= equity
            result['st_equity_tpv']= st_equity_tpv
            result['st_equity_tsv']= st_equity_tsv
            result['st_equity_gain']= st_equity_gain
            result['lt_equity_tpv']= lt_equity_tpv
            result['lt_equity_tsv']= lt_equity_tsv
            result['lt_equity_gain']= lt_equity_gain
            result['debt_MF']= debt_MF
            result['st_debt_MF_tpv']= st_debt_MF_tpv
            result['st_debt_MF_tsv']= st_debt_MF_tsv
            result['st_debt_MF_gain']= st_debt_MF_gain
            result['lt_debt_MF_tpv']= lt_debt_MF_tpv
            result['lt_debt_MF_tsv']= lt_debt_MF_tsv
            result['lt_debt_MF_gain']= lt_debt_MF_gain
            result['listed_d']= listed_d
            result['st_listed_d_tpv']= st_listed_d_tpv
            result['st_listed_d_tsv']= st_listed_d_tsv
            result['st_listed_d_gain']= st_listed_d_gain
            result['lt_listed_d_tpv']= lt_listed_d_tpv
            result['lt_listed_d_tsv']= lt_listed_d_tsv
            result['lt_listed_d_gain']= lt_listed_d_gain
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def delete_form16(request):
	result={}
	if request.method=='POST':
		try:
			pan=request.POST.get('pan')
			company_name=request.POST.get('company_name')
			client_id=request.POST.get('client_id')
			flag = 0
			R_ID = get_rid(client_id)

			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=company_name).exists():

				###### Delete VI_Deductions ########################
				log.info('Delete VI_Deductions1')
				Employer_Obj=Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=company_name).latest('id')
				employer_name=Employer_Obj.Name_of_employer
				if Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).exists():
					form16 = Client_fin_info1.objects.filter(client_id = client_id,company_name=employer_name).latest('time_stamp')
					###According to new update superannuation=80C
					###According to new update number_80d_self_family=80D
					S80C = form16.superannuation
					S80D = form16.number_80d_self_family

					if VI_Deductions.objects.filter(R_Id=R_ID).exists():
						Deductions_instance = VI_Deductions.objects.get(R_Id=R_ID)

						S_80C=Deductions_instance.VI_80_C
						S_80D=Deductions_instance.VI_80_D

						if S80C!=None and S_80C!=None:
							S_80C=S_80C-S80C
							Deductions_instance.VI_80_C = S_80C
							log.info('Delete VI_80_C')
						if S80D!=None and S_80D!=None:
							S_80D=S_80D-S80D
							Deductions_instance.VI_80_D = S_80D
							log.info('Delete VI_80_D')

						Deductions_instance.save()

					############# Delete 80D in Variables_80d ##########
					log.info('Delete Variables_80d1')
					if Variables_80d.objects.filter(R_Id=R_ID).exists():
						log.info('Delete Variables_80d2')
						Variables_80d_inst = Variables_80d.objects.get(R_Id=R_ID)

						S_InsurancePremium=Variables_80d_inst.self_insurance_premium
						
						if S80D!=None and S_InsurancePremium!=None:
							S_InsurancePremium=S_InsurancePremium-S80D
							Variables_80d_inst.self_insurance_premium = S_InsurancePremium
							log.info('Delete self_insurance_premium')
						Variables_80d_inst.save()

					##############################################

				Employer_Details.objects.filter(P_id=personal_instance,Name_of_employer=company_name).delete()
			
			if Income.objects.filter(R_Id=R_ID,company_name=company_name).exists():
				Income.objects.filter(R_Id=R_ID,company_name=company_name).delete()
			if Client_fin_info1.objects.filter(client_id=client_id,company_name=company_name,flag=0).exists():
				C_fin_info_instance = Client_fin_info1.objects.get(client_id=client_id,company_name=company_name,flag=0)
				C_fin_info_instance.flag=2
				flag = 2
				C_fin_info_instance.save()

			result['status']='success'
			result['pan']= pan
			result['flag']= flag
			result['client_id']= client_id
			result['company_name']= company_name

		except Exception as ex:
			log.error('Error in delete_form16 : '+request.get_host()+': '+traceback.format_exc())
			result['status']='Error : %s' % ex
	else:
		result['status']="Fail"
	return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def delete_house_property(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            option=request.POST.get('option')

            R_ID = get_rid(client_id)

            if option == 'all':
                if House_Property_Details.objects.filter(R_Id=R_ID).exists():
                    House_Property_Details.objects.filter(R_Id=R_ID).delete()
                if Property_Owner_Details.objects.filter(R_Id=R_ID).exists():
                    Property_Owner_Details.objects.filter(R_Id=R_ID).delete()
            if option == '2':
                if House_Property_Details.objects.filter(R_Id=R_ID,Property_Id=2).exists():
                    House_Property_Details.objects.filter(R_Id=R_ID,Property_Id=2).delete()
                if Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=2).exists():
                    Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=2).delete()
            if option == '3':
                if House_Property_Details.objects.filter(R_Id=R_ID,Property_Id=3).exists():
                    House_Property_Details.objects.filter(R_Id=R_ID,Property_Id=3).delete()
                if Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=3).exists():
                    Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=3).delete()

            result['status']= 'success'
            result['client_id']= client_id
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_house_property(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            R_ID = get_rid(client_id)

            no_of_hp = House_Property_Details.objects.filter(R_Id=R_ID).count()
            result['no_of_hp']= no_of_hp

            Premises = []
            Town_City = []
            State = []
            PIN = []
            per_s_in_p = []
            rent = []
            property_type = []
            Property_Tax = []
            Interest = []
            Is_coOwned = []
            coOwned_per_s = []
            coOwned_name = []
            coOwned_pan = []

            # for initializing house property as 'no'
            # 'yes' will depend on house property exists or not
            # if not tax_variables.objects.filter(R_Id=R_ID).exists():
            #   tax_variables.objects.create(
            #       R_Id = R_ID,
            #       house_property = 'no',
            #   ).save()
            # else:
            #   tax_variables.objects.filter(R_Id=R_ID).update(house_property = 'no')

            for hp in House_Property_Details.objects.filter(R_Id=R_ID):
                Premises.append( hp.Name_of_the_Premises_Building_Village )
                Town_City.append( hp.Town_City )
                State.append( hp.State )
                PIN.append( hp.PIN_Code )
                per_s_in_p.append( hp.Your_percentage_of_share_in_Property )
                rent.append( hp.Rent_Received )
                property_type.append( hp.Type_of_Hosue_Property )
                Property_Tax.append( hp.Property_Tax )
                Interest.append( hp.Interest_on_Home_loan )
                if Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=hp.Property_Id).exists():
                    Is_coOwned.append('yes')
                    for pod in Property_Owner_Details.objects.filter(R_Id=R_ID,Property_Id=hp.Property_Id):
                        coOwned_per_s.append( pod.Percentage_Share_in_Property )
                        coOwned_name.append( pod.Name_of_Co_Owner )
                        coOwned_pan.append( pod.PAN_of_Co_owner )
                else:
                    Is_coOwned.append('no')
                    coOwned_per_s.append( '' )
                    coOwned_name.append( '' )
                    coOwned_pan.append( '' )

            result['status'] = 'success'
            result['Premises'] = Premises
            result['Town_City'] = Town_City
            result['State'] = State
            result['PIN'] = PIN
            result['per_s_in_p'] = per_s_in_p
            result['rent'] = rent
            result['property_type'] = property_type
            result['Property_Tax'] = Property_Tax
            result['Interest'] = Interest
            result['Is_coOwned'] = Is_coOwned
            result['coOwned_per_s'] = coOwned_per_s
            result['coOwned_name'] = coOwned_name
            result['coOwned_pan'] = coOwned_pan
        except Exception as ex:
            result['status']='Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Fail"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def delete_capital_gain(request):
    if request.method=='POST':
        result={}
        try:
            client_id=request.POST.get('client_id')
            type = request.POST.get('type')
            R_ID = get_rid(client_id)

            result['status']= 'start'
            if type == 'shares':
                if Shares_ST.objects.filter(R_Id=R_ID).exists():
                    Shares_ST.objects.filter(R_Id=R_ID).delete()
                if Shares_LT.objects.filter(R_Id=R_ID).exists():
                    Shares_LT.objects.filter(R_Id=R_ID).delete()
            if type == 'equity':
                if Equity_ST.objects.filter(R_Id=R_ID).exists():
                    Equity_ST.objects.filter(R_Id=R_ID).delete()
                if Equity_LT.objects.filter(R_Id=R_ID).exists():
                    Equity_LT.objects.filter(R_Id=R_ID).delete()
            if type == 'debt_MF':
                if Debt_MF_ST.objects.filter(R_Id=R_ID).exists():
                    Debt_MF_ST.objects.filter(R_Id=R_ID).delete()
                if Debt_MF_LT.objects.filter(R_Id=R_ID).exists():
                    Debt_MF_LT.objects.filter(R_Id=R_ID).delete()
            if type == 'listed_d':
                if Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
                    Listed_Debentures_ST.objects.filter(R_Id=R_ID).delete()
                if Listed_Debentures_LT.objects.filter(R_Id=R_ID).exists():
                    Listed_Debentures_LT.objects.filter(R_Id=R_ID).delete()

            if not Shares_ST.objects.filter(R_Id=R_ID).exists() and not Equity_ST.objects.filter(R_Id=R_ID).exists() and not Debt_MF_ST.objects.filter(R_Id=R_ID).exists() and not Listed_Debentures_ST.objects.filter(R_Id=R_ID).exists():
                if not tax_variables.objects.filter(R_Id=R_ID).exists():
                    tax_variables.objects.create(
                        R_Id = R_ID,
                        capital_gain = 'no',
                    ).save()
                else:
                    tax_variables_instance=tax_variables.objects.get(R_Id=R_ID)
                    tax_variables_instance.capital_gain = 'no'
                    tax_variables_instance.save()

            result['status']= 'success'
            result['client_id']= client_id
            result['type']= type
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_business_profession_income(request):
    if request.method=='POST':
        result={}
        try:
            data=request.POST.get('data')
            req_data = json.loads(data)
            client_id=req_data['client_id']
            R_ID = get_rid(client_id)

            business_profession_income=req_data['business_profession_income']

            result['status']= 'start'
            if not tax_variables.objects.filter(R_Id=R_ID).exists():
                tax_variables.objects.create(
                    R_Id = R_ID,
                    business_or_profession = business_profession_income,
                ).save()
            else:
                tax_variables.objects.filter(R_Id=R_ID).update(business_or_profession = business_profession_income)


            result['status']= 'success'
            result['req_data']= req_data
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def get_rid(client_id):
    R_ID = 0
    personal_instance=Personal_Details.objects.get(P_Id=client_id)
    if Return_Details.objects.filter(P_id=personal_instance,FY='2018-2019').exists():
        Return_Details_instance=Return_Details.objects.get(P_id=personal_instance,FY='2018-2019')
        R_ID = Return_Details_instance.R_id
    else:
        log.info('R_ID Entry not exists')
        # for rd in Return_Details.objects.filter(P_id=personal_instance):
        #   log.info(rd.R_id)

    return R_ID

@csrf_exempt
def get_return_details(request):
    result={}
    try:
        if request.method=='POST':
            try:
                client_id =request.POST.get('client_id')
                log.info(client_id)
                R_ID = get_rid(client_id)
                return_year = '2018-2019'
                if Return_Details.objects.filter(R_id=R_ID).exists():
                    Return_Details_instance=Return_Details.objects.get(R_id=R_ID)
                    return_year = Return_Details_instance.FY

                result['R_ID'] = R_ID
                result['return_year'] = return_year
            except Exception as ex:
                result['status']='Error getting return details: ' + traceback.format_exc()
            return HttpResponse(json.dumps(result), content_type='application/json')
        else:
            result['status']="Fail"
            return HttpResponse(json.dumps(result), content_type='application/json')
    except Exception as ex:
        result['status']= 'Error getting return details: ' + traceback.format_exc()
        return HttpResponse(json.dumps(result), content_type='application/json')



@csrf_exempt
def save_breakups(request):
	result={}
	try:
		if request.method=='POST':
			try:
				client_id =request.POST.get('client_id')
				log.info('salary break up')
				# log.info(client_id)
				R_ID = get_rid(client_id)
				# log.info(R_ID)
				keyword=request.POST.get('keyword')
				company=request.POST.get('company')
				company_name=request.POST.get('company_name')
				log.info(company_name)
				data=request.POST.get('data')
				req_data=json.loads(data)

				log.info(req_data)

				if keyword=='salary_breakup':
					msg=save_salary_breakup(R_ID,req_data,company,company_name)
				elif keyword=='perquisites_breakup':
					msg=save_perquisites_breakup(R_ID,req_data,company,company_name)
				elif keyword=='profits_in_lieu_salary_breakup':
					msg=save_profits_in_lieu_salary_breakup(R_ID,req_data,company,company_name)
				elif keyword=='allowances_breakup':
					msg=save_allowances_breakup(R_ID,req_data,company,company_name)

				result['status'] = msg
			except Exception as ex:
				result['status']='Error in saving breakups : %s' % traceback.format_exc()
			return HttpResponse(json.dumps(result), content_type='application/json')
		else:
			result['status']="Fail"
			return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		result['status']= 'Error in saving breakups : %s' % traceback.format_exc()
		return HttpResponse(json.dumps(result), content_type='application/json')


def save_breakup_in_income_table(R_ID,company_name,HRA,LTA):
	try:
		income_data=Income.objects.filter(R_Id=R_ID,company_name=company_name)
		if income_data.exists():
			income_instance=Income.objects.get(R_Id=R_ID,company_name=company_name)
			income_instance.R_Id=R_ID
			income_instance.Form16_HRA=HRA
			income_instance.LTA=LTA
			income_instance.save()

			msg='Income entry Updated'
		
		elif not income_data.exists():
			
			Income.objects.create(

				R_Id=R_ID,
				company_name=company_name,
				Form16_HRA=HRA,
				LTA=LTA,
				
			).save()

			msg='Income entry created'

		flag=0
	except Exception as ex:
		log.info('Error in saving brakups in income details: '+traceback.format_exc())
		flag='Error in saving brakups in income details: '+traceback.format_exc()
	return flag


salary_breakup_variable=['basic_salary','conveyance_allowance','HRA','LTA','leave_encashment','others']
def save_salary_breakup(R_ID,req_data,company,company_name):
	result={}
	msg=''
	flag=1
	try:
		log.info('save_salary_breakup')

		if company_name:
			salary_breakup=SalaryBreakUp.objects.filter(R_Id=R_ID,company_name=company_name)
			if salary_breakup.exists():
				salary_breakup_instance=SalaryBreakUp.objects.get(R_Id=R_ID,company_name=company_name)
				salary_breakup_instance.R_Id=R_ID
				salary_breakup_instance.company_name=company_name
				salary_breakup_instance.basic_salary=int(req_data[company+'_'+salary_breakup_variable[0]])
				salary_breakup_instance.conveyance_allowance=int(req_data[company+'_'+salary_breakup_variable[1]])
				salary_breakup_instance.leave_encashment=int(req_data[company+'_'+salary_breakup_variable[4]])
				salary_breakup_instance.others=int(req_data[company+'_'+salary_breakup_variable[5]])
				salary_breakup_instance.save()

				msg='Salary Breakup entry Updated'
			
			elif not salary_breakup.exists():

				# log.info(req_data[company+'_'+salary_breakup_variable[5]])
				SalaryBreakUp.objects.create(

					R_Id=R_ID,
					company_name=company_name,
					basic_salary=int(req_data[company+'_'+salary_breakup_variable[0]]),
					conveyance_allowance=int(req_data[company+'_'+salary_breakup_variable[1]]),
					leave_encashment=int(req_data[company+'_'+salary_breakup_variable[4]]),
					others=int(req_data[company+'_'+salary_breakup_variable[5]]),

				).save()

				msg='Salary Breakup entry created'

			flag=save_breakup_in_income_table(R_ID,company_name,int(req_data[company+'_'+salary_breakup_variable[2]]),int(req_data[company+'_'+salary_breakup_variable[3]]))
		else:
			flag='Please first fill company name'

	except Exception as ex:
		flag='Error in salary brekup api: %s' % traceback.format_exc()

	log.info(flag)
	return flag


perquisites_breakup_variable=['accommodation','cars_or_other_automotive','utilitiy_bills','concessional_travel','free_meals','free_education','other_benefits']
def save_perquisites_breakup(R_ID,req_data,company,company_name):
	result={}
	msg=''
	flag=1
	try:
		log.info('save_perquisites_breakup')
		if company_name:
			Perquisite_breakup=PerquisiteBreakUp.objects.filter(R_Id=R_ID,company_name=company_name)
			if Perquisite_breakup.exists():
				Perquisite_breakup_instance=PerquisiteBreakUp.objects.get(R_Id=R_ID,company_name=company_name)
				Perquisite_breakup_instance.R_Id=R_ID
				Perquisite_breakup_instance.company_name=company_name
				Perquisite_breakup_instance.accommodation=int(req_data[company+'_'+perquisites_breakup_variable[0]])
				Perquisite_breakup_instance.cars_or_other_automotive=int(req_data[company+'_'+perquisites_breakup_variable[1]])
				# Perquisite_breakup_instance.domestic_help=int(req_data[company+'_'+perquisites_breakup_variable[3]])
				Perquisite_breakup_instance.utilitiy_bills=int(req_data[company+'_'+perquisites_breakup_variable[2]])
				# Perquisite_breakup_instance.concessional_loans=int(req_data[company+'_'+perquisites_breakup_variable[6]])
				# Perquisite_breakup_instance.holiday_expenses=int(req_data[company+'_'+perquisites_breakup_variable[7]])
				Perquisite_breakup_instance.concessional_travel=int(req_data[company+'_'+perquisites_breakup_variable[3]])
				Perquisite_breakup_instance.free_meals=int(req_data[company+'_'+perquisites_breakup_variable[4]])
				Perquisite_breakup_instance.free_education=int(req_data[company+'_'+perquisites_breakup_variable[5]])
				# Perquisite_breakup_instance.gifts_vouchers=int(req_data[company+'_'+perquisites_breakup_variable[11]])
				# Perquisite_breakup_instance.credit_card_expenses=int(req_data[company+'_'+perquisites_breakup_variable[12]])
				# Perquisite_breakup_instance.club_expenses=int(req_data[company+'_'+perquisites_breakup_variable[13]])
				Perquisite_breakup_instance.other_benefits=int(req_data[company+'_'+perquisites_breakup_variable[6]])
				Perquisite_breakup_instance.save()

				msg='Perquisite BreakUp entry Updated'
			
			elif not Perquisite_breakup.exists():

				# log.info(req_data[company+'_'+salary_breakup_variable[5]])
				PerquisiteBreakUp.objects.create(

					R_Id=R_ID,
					company_name=company_name,
					accommodation=int(req_data[company+'_'+perquisites_breakup_variable[0]]),
				    cars_or_other_automotive=int(req_data[company+'_'+perquisites_breakup_variable[1]]),
				    # domestic_help=int(req_data[company+'_'+perquisites_breakup_variable[3]]),
		            utilitiy_bills=int(req_data[company+'_'+perquisites_breakup_variable[2]]),
				    # concessional_loans=int(req_data[company+'_'+perquisites_breakup_variable[6]]),
				    # holiday_expenses=int(req_data[company+'_'+perquisites_breakup_variable[7]]),
					concessional_travel=int(req_data[company+'_'+perquisites_breakup_variable[3]]),
					free_meals=int(req_data[company+'_'+perquisites_breakup_variable[4]]),
					free_education=int(req_data[company+'_'+perquisites_breakup_variable[5]]),
					# gifts_vouchers=int(req_data[company+'_'+perquisites_breakup_variable[11]]),
					# credit_card_expenses=int(req_data[company+'_'+perquisites_breakup_variable[12]]),
					# club_expenses=int(req_data[company+'_'+perquisites_breakup_variable[13]]),
					other_benefits=int(req_data[company+'_'+perquisites_breakup_variable[6]])

				).save()

				msg='Perquisite BreakUp entry created'

			flag=0
		else:
			flag='Please first fill company name'
	except Exception as ex:
		flag='Error in PerquisiteBreakUp api: %s' % traceback.format_exc()

	return flag


profits_in_lieu_salary_breakup_variable=['termination_compensation','keyman_insurance','other_profit_in_lieu_salary','other']
def save_profits_in_lieu_salary_breakup(R_ID,req_data,company,company_name):
	result={}
	msg=''
	flag=1
	try:
		log.info('save_profits_in_lieu_salary_breakup')
		if company_name:
			ProfitinLieu_breakup=ProfitinLieuBreakUp.objects.filter(R_Id=R_ID,company_name=company_name)
			if ProfitinLieu_breakup.exists():
				ProfitinLieu_breakup_instance=ProfitinLieuBreakUp.objects.get(R_Id=R_ID,company_name=company_name)
				ProfitinLieu_breakup_instance.R_Id=R_ID
				ProfitinLieu_breakup_instance.company_name=company_name
				ProfitinLieu_breakup_instance.termination_compensation=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[0]])
				ProfitinLieu_breakup_instance.keyman_insurance=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[1]])
				ProfitinLieu_breakup_instance.other_profit_in_lieu_salary=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[2]])
				ProfitinLieu_breakup_instance.other=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[3]])
				ProfitinLieu_breakup_instance.save()

				msg='ProfitinLieu BreakUp entry Updated'
			
			elif not ProfitinLieu_breakup.exists():

				# log.info(req_data[company+'_'+salary_breakup_variable[5]])
				ProfitinLieuBreakUp.objects.create(

					R_Id=R_ID,
					company_name=company_name,
					termination_compensation=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[0]]),
				    keyman_insurance=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[1]]),
				    other_profit_in_lieu_salary=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[2]]),
					other=int(req_data[company+'_'+profits_in_lieu_salary_breakup_variable[3]])

				).save()

				msg='ProfitinLieu BreakUp entry created'

			flag=0
		else:
			flag='Please first fill company name'
	except Exception as ex:
		flag='Error in ProfitinLieu BreakUp api: %s' % traceback.format_exc()

	return flag


allowances_breakup_variable=['LTA_allowance','earned_leave_encashment','HRA_allowance','other_allowance']
def save_allowances_breakup(R_ID,req_data,company,company_name):
	result={}
	msg=''
	flag=1
	try:
		log.info('save_allowances_breakup')
		if company_name:
			allowances_breakup=allowances.objects.filter(R_id=R_ID,company_name=company_name)
			if allowances_breakup.exists():
				allowances_breakup_instance=allowances.objects.get(R_id=R_ID,company_name=company_name)
				allowances_breakup_instance.R_id=R_ID
				allowances_breakup_instance.company_name=company_name
				allowances_breakup_instance.earned_leave_encashment=int(req_data[company+'_'+allowances_breakup_variable[1]])
				allowances_breakup_instance.lta=int(req_data[company+'_'+allowances_breakup_variable[0]])
				allowances_breakup_instance.hra=int(req_data[company+'_'+allowances_breakup_variable[2]])
				allowances_breakup_instance.other_allowance=int(req_data[company+'_'+allowances_breakup_variable[3]])
				allowances_breakup_instance.save()

				msg='allowances breakup entry Updated'
			
			elif not allowances_breakup.exists():

				# log.info(req_data[company+'_'+salary_breakup_variable[5]])
				allowances.objects.create(

					R_id=R_ID,
					company_name=company_name,
				    earned_leave_encashment=int(req_data[company+'_'+allowances_breakup_variable[1]]),
				    lta=int(req_data[company+'_'+allowances_breakup_variable[0]]),
				    hra=int(req_data[company+'_'+allowances_breakup_variable[2]]),
				    other_allowance=int(req_data[company+'_'+allowances_breakup_variable[3]]),
				).save()

				msg='allowances breakup entry created'

			flag=0
			# flag=save_breakup_in_income_table(R_ID,company_name,int(req_data[company+'_'+allowances_breakup_variable[2]]),int(req_data[company+'_'+allowances_breakup_variable[0]]))
		else:
			flag='Please first fill company name'

	except Exception as ex:
		flag='Error in allowances breakup api: %s' % traceback.format_exc()

	return flag

@csrf_exempt
def get_breakup_details(request):
	result={}
	try:
		client_id=request.POST.get('client_id')
		R_ID=get_rid(client_id)
		log.info(client_id)
		log.info(R_ID)

		income_data=Income.objects.filter(R_Id=R_ID)
		income_data_serializer=IncomeSerializer(income_data,many='true')

		result['data']=income_data_serializer.data
		result['status']=0
	except Exception as e:
		log.info('Error in getting breakup details :'+traceback.format_exc())
		result['status']=1
		result['data']='Error in getting breakup details :'+traceback.format_exc()
	return HttpResponse(json.dumps(result),content_type='application/json')



co_owner_variable=['co_owner_no','Name_of_Co_Owner','PAN_of_Co_owner','Percentage_Share_in_Property']
@csrf_exempt
def co_owner_api(request):
	# log.info('co_owner_api')
	result={}
	try:
		data=request.POST.get('data')
		req_data=json.loads(data)
		property_no=request.POST.get('no_of_propery')
		no_of_co_owner=request.POST.get('no_of_co_owner')
		co_owner_flag=request.POST.get('co_owner_flag')
		client_id=request.POST.get('client_id')
		R_ID=get_rid(client_id)
		# log.info(co_owner_flag)
		# log.info(R_ID)
		delete_existing_data('co_owner',R_ID,property_no,no_of_co_owner)

		if co_owner_flag=='yes':
			for x in range(int(no_of_co_owner)):
				no=x+1
				keyword='property'+property_no+'_co_owner'+str(no)
				co_owner_no=req_data[keyword+'_'+co_owner_variable[0]]
				msg_keyword='property '+property_no+' co_owner '+str(co_owner_no)
			
				Property_Owner_data=Property_Owner_Details.objects.filter(Property_Id=property_no,co_owner_no=co_owner_no,R_Id=R_ID)
				
				if req_data[keyword+'_'+co_owner_variable[3]]!='':
					percent_share=int(req_data[keyword+'_'+co_owner_variable[3]])
				else:
					percent_share=0

				if Property_Owner_data.exists():
					Property_Owner_instance=Property_Owner_Details.objects.get(Property_Id=property_no,co_owner_no=co_owner_no,R_Id=R_ID)
					Property_Owner_instance.co_owner_no=co_owner_no
					Property_Owner_instance.Name_of_Co_Owner=req_data[keyword+'_'+co_owner_variable[1]]
					Property_Owner_instance.PAN_of_Co_owner=req_data[keyword+'_'+co_owner_variable[2]]
					Property_Owner_instance.Percentage_Share_in_Property=percent_share
					Property_Owner_instance.save()

					log.info(msg_keyword+' entry updated')

				elif not Property_Owner_data.exists():
					Property_Owner_Details.objects.create(

						Property_Id=property_no,
						R_Id=R_ID,
						co_owner_no=co_owner_no,
						Name_of_Co_Owner=req_data[keyword+'_'+co_owner_variable[1]],
						PAN_of_Co_owner=req_data[keyword+'_'+co_owner_variable[2]],
						Percentage_Share_in_Property=percent_share,

					).save()

					log.info(msg=msg_keyword+' entry created')
		else:
			log.info('Is this co-owned? '+co_owner_flag)
			delete_existing_data('co_owner',R_ID,property_no,0)
			

		result['data']='success'
		result['status']=0
	except Exception as e:
		log.info('Error in co owner api :'+traceback.format_exc())
		result['status']=1
		result['data']='Error in in co owner api :'+traceback.format_exc()
	return HttpResponse(json.dumps(result),content_type='application/json')


def delete_existing_data(keyword,R_ID,no1,no2):
	try:
		log.info(no1)
		log.info(no2)
		start_index=int(no2)+1
		end_index=6
		for no in range(start_index,end_index):
			# log.info(no)
			if keyword=='co_owner':
				co_owner_details=Property_Owner_Details.objects.filter(Property_Id=no1,co_owner_no=no,R_Id=R_ID)
				if co_owner_details.exists():
					co_owner_details.delete()
					log.info('co_owner{} Deleted'.format(no))
			elif keyword=='tenant':
				tenant_details=TenantDetails.objects.filter(Property_Id=no1,tenant_no=no,R_Id=R_ID)
				if tenant_details.exists():
					tenant_details.delete()
					log.info('Tenant{} Deleted'.format(no))

	except Exception as ex:
		log.error('Error when deleting '+keyword+' data :'+traceback.format_exc())


@csrf_exempt
def get_co_owner_details(request):
	result={}
	try:
		client_id=request.POST.get('client_id')
		R_ID=get_rid(client_id)
		# log.info(client_id)
		# log.info(R_ID)

		house_property_details=House_Property_Details.objects.filter(R_Id=R_ID)
		house_property_details_serializer=HousePropertyDetailsSerializer(house_property_details,many='true')

		result['data']=house_property_details_serializer.data
		result['status']=0
	except Exception as e:
		log.info('Error in getting co_owner details :'+traceback.format_exc())
		result['status']=1
		result['data']='Error in getting co_owner details :'+traceback.format_exc()
	return HttpResponse(json.dumps(result),content_type='application/json')


tenant_variable=['tenant_no','tenant_name','tenant_pan']
@csrf_exempt
def tenant_api(request):
	result={}
	try:
		data=request.POST.get('data')
		req_data=json.loads(data)
		property_no=request.POST.get('no_of_propery')
		no_of_tenant=request.POST.get('no_of_tenant')
		tenant_flag=request.POST.get('tenant_flag')
		client_id=request.POST.get('client_id')
		R_ID=get_rid(client_id)
		# log.info(co_owner_flag)
		# log.info(R_ID)
		delete_existing_data('tenant',R_ID,property_no,no_of_tenant)

		# log.info(tenant_flag)
		if tenant_flag=='let_out':
			for x in range(int(no_of_tenant)):
				no=x+1
				keyword='property'+property_no+'_tenant'+str(no)
				co_owner_no=req_data[keyword+'_'+tenant_variable[0]]
				msg_keyword='property '+property_no+' tenant '+str(co_owner_no)
			
				TenantDetails_data=TenantDetails.objects.filter(Property_Id=property_no,tenant_no=co_owner_no,R_Id=R_ID)
			
				if TenantDetails_data.exists():
					Property_Owner_instance=TenantDetails.objects.get(Property_Id=property_no,tenant_no=co_owner_no,R_Id=R_ID)
					Property_Owner_instance.tenant_no=co_owner_no
					Property_Owner_instance.tenant_name=req_data[keyword+'_'+tenant_variable[1]]
					Property_Owner_instance.tenant_pan=req_data[keyword+'_'+tenant_variable[2]]
					Property_Owner_instance.save()

					log.info(msg_keyword+' entry updated')

				elif not TenantDetails_data.exists():
					TenantDetails.objects.create(

						Property_Id=property_no,
						R_Id=R_ID,
						tenant_no=co_owner_no,
						tenant_name=req_data[keyword+'_'+tenant_variable[1]],
						tenant_pan=req_data[keyword+'_'+tenant_variable[2]],

					).save()

					log.info(msg=msg_keyword+' entry created')
		else:
			log.info('House property type? '+tenant_flag)
			delete_existing_data('tenant',R_ID,property_no,0)
			

		result['data']='success'
		result['status']=0
	except Exception as e:
		log.info('Error in tenant api :'+traceback.format_exc())
		result['status']=1
		result['data']='Error in tenant api :'+traceback.format_exc()
	return HttpResponse(json.dumps(result),content_type='application/json')




