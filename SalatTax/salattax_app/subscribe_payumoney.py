from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect,csrf_exempt
from django.template.context_processors import csrf
from django.http import JsonResponse
import os, sys
from PIL import Image
# Python logging package
import logging
import json
import random
import string
import hashlib
from .models import SalatTaxUser,Personal_Details,Module_Subscription,Invoice_Details,Return_Details
from .models import accommodation_details,home_loan,allowances,details_80c,details_80d,VI_Deductions
from .models import Employer_Details,tds_tcs,child_info,user_tracking,salat_registration
from .models import Investment_Details,Mail_Trigger,Module_Subscription,Modules,RFPaymentDetails
from django.template import Context, Template,RequestContext
# from Crypto.Hash import SHA512
# for Email
import smtplib
from salattax_efiling import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import hashlib
from django.db.models import Avg, Max, Min, Sum,Q

from dateutil.parser import parse

import traceback
stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)
from .path import *
import locale
from datetime import date, datetime
from django.db.models.functions import Now
import traceback
# import tinyurl

# from crontab import CronTab
# cron   = CronTab()
# cron = CronTab()
# add new cron job
# job  = cron.new(command=server_curl+'/schedule_mail/123')
# job settings
# job.minutes.every(4)

@csrf_exempt
def get_payment_details(request,data):
	# log.info(request)
	log.info(data)
	arr_list = data.split('amt')
	# log.info(len(arr_list))

	client_id = data.split('amt')[0]
	amount = data.split('amt')[1]
	# s=string.lowercase+string.digits+string.uppercase
	log.info(amount.isdigit())
	fname = ''
	lname = ''
	mobile = ''
	email = ''
	if Personal_Details.objects.filter(P_Id = client_id).exists():
		for client in Personal_Details.objects.filter(P_Id = client_id):
			fname = client.name.split('|')[0]
			if len(client.name.split('|'))==2:
				lname = client.name.split('|')[1]
			if len(client.name.split('|'))==3:
				lname = client.name.split('|')[2]
			mobile = client.mobile
			email = client.email or ''

	if email == '' :
		for a in SalatTaxUser.objects.filter(id=client_id):
			email = a.email

	if len(arr_list) == 3:
		module_Id = 2 # for E-filing module_Id = 2
		module_detail = 'E-filing'
	else:
		module_Id = 1 # for Tax Savings Account module_Id = 1
		if(amount.isdigit()):
			if(int(amount)<=1400):
				module_detail = 'TSA - Starter'
			else:
				module_detail = 'TSA - Professional'
		else:
			if '990' in amount:
				module_detail = 'TSA - Starter'
			else:
				module_detail = 'TSA - Professional'
				# module_detail = 'Professional'
	log.info(module_detail)
	# amount = 10

	if module_detail!='E-filing':
		if not Module_Subscription.objects.filter(client_id=client_id,module_Id=module_Id,year=2019,module_detail1=module_detail,module_cost=amount).exists():
			Module_Subscription.objects.create(
				client_id=client_id,
				module_Id=module_Id,
				year=2019,
				module_cost=amount,
				module_detail1=module_detail
			).save()

	# Merchant key: iv05Eqin
	# Merchant Salt: X31IOZyzi6
	# password : Budget08$
	# PAYU_BASE_URL = "https://sandboxsecure.payu.in/_payment" https://secure.payu.in/_payment
	# MERCHANT_KEY = "iv05Eqin"
	# key=""
	# SALT = "X31IOZyzi6"
	PAYU_BASE_URL = PAYUMONEY_BASE_URL
	MERCHANT_KEY = "iv05Eqin"
	key=""
	SALT = "X31IOZyzi6"
	action = ''
	posted={}
	hash_object = hashlib.sha256(b'randint(0,20)')
	txnid=hash_object.hexdigest()[0:20]
	hashh = ''
	posted['txnid']=txnid
	hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|||||"
	posted['key']=MERCHANT_KEY
	posted['amount']=amount
	posted['productinfo']=module_detail
	posted['firstname']=fname
	posted['phone']=mobile
	posted['email']=email
	posted['lastname']=lname
	posted['address1']='address1'
	posted['address2']='address2'
	posted['city']='city'
	posted['state']='state'
	posted['country']='country'
	posted['zipcode']=''
	posted['udf1']=client_id
	posted['udf2']=''
	posted['udf3']=''
	posted['udf4']=''
	posted['udf5']=''
	posted['PG']='PG'
	posted['salt']=SALT
	posted['surl']= payum_redirection+'/success_page/'+client_id
	posted['furl']= payum_redirection+'/Failure/'
	posted['curl']= payum_redirection+'/curl/'

	log.info(salat_url+'/Failure/')
	hash_string=''
	hashVarsSeq=hashSequence.split('|')
	for i in hashVarsSeq:
		try:
			hash_string+=str(posted[i])
		except Exception:
			hash_string+=''
		hash_string+='|'
	hash_string+=SALT
	log.info(hash_string)
	hashh=hashlib.sha512(hash_string).hexdigest().lower()
	action =PAYU_BASE_URL
	if(posted.get("key")!=None and posted.get("txnid")!=None and posted.get("productinfo")!=None and posted.get("firstname")!=None and posted.get("email")!=None):
		log.info('if')
		return render(request,'subscribe_payumoney.html',
        {
        'posted':posted,
        'amount':999.0,
        'hashh':hashh,
        'txnid':txnid,
        'MERCHANT_KEY':MERCHANT_KEY,
        'hash_string':hash_string,
        'action':PAYUMONEY_BASE_URL
        })
	else:
		# return render_to_response('subscribe_payumoney.html',RequestContext(request,{"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"." }))
		log.info('else')
		return render(request,'subscribe_payumoney.html',{
        'posted':posted,
        'amount':999.0,
        'hashh':hashh,
        'txnid':txnid,
        'MERCHANT_KEY':MERCHANT_KEY,
        'hash_string':hash_string,
        'action':".",
        })

@csrf_protect
@csrf_exempt
def success(request,id):
	try:
		username = request.session.get('login')
		client_id = 0
		if username is None:
			pass
		else:
			if username!='':
				atuser = User.objects.get(username=username)
				for c in SalatTaxUser.objects.filter(user=atuser):
					client_id = c.id
		# log.info(client_id)
		# c = {}
		# c.update(csrf(request))
		client_id=id
		log.info(request)
		status=''
		firstname=''
		amount=''
		txnid=''
		posted_hash=''
		key=''
		productinfo=''
		email=''
		salt=''
		status=request.POST["status"]
		firstname=request.POST["firstname"]
		amount=request.POST["amount"]
		txnid=request.POST["txnid"]
		posted_hash=request.POST["hash"]
		key=request.POST["key"]
		productinfo=request.POST["productinfo"]
		email=request.POST["email"]
		salt="X31IOZyzi6"
		amount_compare = amount.encode('utf-8')
		amount_compare = int(float(amount_compare) )
		log.info( status )
		try:
			additionalCharges=request.POST["additionalCharges"]
			retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
		except Exception:
			retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
		hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
		if(hashh !=posted_hash):
			log.info( "Invalid Transaction. Please try again")
			log.info( "Your Transaction ID for this transaction is ",txnid)
			# log.info( type(amount_compare))
		else:
			log.info( "Thank You. Your order status is ", status)
			log.info( "Your Transaction ID for this transaction is ",txnid)
			log.info( "We have received a payment of Rs. ", amount ,". Your order will soon be shipped.")
			# log.info( type(amount_compare))
		# return render_to_response('success.html',RequestContext(request,{"txnid":txnid,"status":status,"amount":amount}))
		log.info( status )
		log.info(productinfo)

		if status == 'success':
			invoice_count=Invoice_Details.objects.filter(~Q(invoice_no='')).count()
			invoice_no=generate_invoice_no(str(invoice_count+1))
			if productinfo=='E-filing':
				log.info(productinfo)
				R_Id=get_rid(client_id)

				# update invoice no in Return filing module subscription entry
				RF_payment_details_id=RFPaymentDetails.objects.get(R_Id=R_Id).id
				module_data_id=Modules.objects.get(module_name='Return Filing').id
				module_subscription_data=Module_Subscription.objects.filter(module_Id=module_data_id,client_id=client_id,module_detail1=RF_payment_details_id,invoice_no='')
				
				if module_subscription_data.exists():
					module_subscription_instance=Module_Subscription.objects.get(module_Id=int(module_data_id),client_id=int(client_id),module_detail1=RF_payment_details_id,invoice_no='')
					module_subscription_instance.invoice_no=invoice_no
					module_subscription_instance.module_detail1=RF_payment_details_id
					module_subscription_instance.save()

					log.info('RF Module Subscription Entry Updated')

				invoice_details_data=Invoice_Details.objects.filter(client_id=client_id,invoice_no='',payment_status='Pending')
				if invoice_details_data.exists():
					invoice_details_instance=Invoice_Details.objects.get(client_id=client_id,invoice_no='',payment_status='Pending')
					invoice_details_instance.invoice_no=invoice_no
					invoice_details_instance.invoice_amount=amount_compare
					invoice_details_instance.invoice_date=Now()
					invoice_details_instance.transaction_id=txnid
					invoice_details_instance.payment_status=status
					invoice_details_instance.save()

					log.info('Invoice details Entry Updated')

				# update invoice no in tax consultaion module subscription entry
				consulation_module_data_id=Modules.objects.get(module_name='Tax Consultation').id
				consulation_module_subscription_data=Module_Subscription.objects.filter(module_Id=consulation_module_data_id,client_id=client_id,invoice_no='')
				
				if consulation_module_subscription_data.exists():
					module_subscription_instance=Module_Subscription.objects.get(module_Id=int(consulation_module_data_id),client_id=int(client_id),invoice_no='')
					module_subscription_instance.invoice_no=invoice_no
					module_subscription_instance.save()

					log.info('EC Module Subscription Entry Updated')


				invoice_details_data=Invoice_Details.objects.filter(client_id=client_id,invoice_no='',payment_status='Pending')
				if invoice_details_data.exists():
					invoice_details_instance=Invoice_Details.objects.get(client_id=client_id,invoice_no='',payment_status='Pending')
					invoice_details_instance.invoice_no=invoice_no
					invoice_details_instance.invoice_amount=amount_compare
					invoice_details_instance.invoice_date=Now()
					invoice_details_instance.transaction_id=txnid
					invoice_details_instance.payment_status=status
					invoice_details_instance.save()

					log.info('Invoice details Entry Updated')

			else:
				plan = 'Assisted Plan'
				if 'Professional' in productinfo:
					plan = 'Do It Yourself Plan'
				send_subscribed_mail(request,client_id,plan)
				
				if Module_Subscription.objects.filter(client_id=client_id,year=current_year(),module_detail1=productinfo,module_cost=amount_compare).exists():
					# max_invoice = 1001
					# if Invoice_Details.objects.all().exists():
					# 	max_invoice = Invoice_Details.objects.all().aggregate(Max('invoice_no'))['invoice_no__max']
					# 	max_invoice = int(max_invoice)+1

					Module_Subscription.objects.filter(client_id=client_id,year=current_year(),module_detail1=productinfo,module_cost=amount_compare).update(invoice_no = max_invoice)
					# for client in Module_Subscription.objects.filter(client_id=client_id,year=2018,module_detail1=productinfo,module_cost=amount_compare):
						# client.invoice_no = 1001
						# client.save()
					log.info("Module_Subscription Entry Updated")
					if not Invoice_Details.objects.filter(client_id=client_id,invoice_no=max_invoice,transaction_id=txnid).exists():
						Invoice_Details.objects.create(
							client_id=client_id,
							invoice_no=invoice_no,
							invoice_amount=amount_compare,
							transaction_id=txnid,
							payment_status=status,
							mode_of_payment='Online'
						).save()

			return render(request,'success_page.html',
		        {
		        'status':status,
		        'txnid':txnid,
		        'amount':amount,
		        'productinfo':productinfo,
		        'client_id':client_id
		        })
		else:
			log.info('transaction failure')

	except Exception as ex:
		log.info('Error when redirection to success payment page '+traceback.format_exc())

def current_year():
	now=datetime.now()
	return now.year

def generate_invoice_no(number):
	try:
		now=datetime.now()
		current_year=now.year
		next_year=now.year+1
		line=str(next_year)
		n=2
		list_chunks_by_2=[line[i:i+n] for i in range(0, len(line), n)]
		year_combination=str(current_year)+list_chunks_by_2[1]

		invoice_no=number.zfill(4)
		return year_combination+'-'+invoice_no
	except Exception as ex:
		log.info('getting year combination '+traceback.format_exc())

def get_rid(client_id):
	R_ID = 0
	personal_instance=Personal_Details.objects.get(P_Id=client_id)
	if Return_Details.objects.filter(P_id=personal_instance,FY='2018-2019').exists():
		Return_Details_instance=Return_Details.objects.get(P_id=personal_instance,FY='2018-2019')
		R_ID = Return_Details_instance.R_id
	else:
		log.info('R_ID Entry not exists')
		# for rd in Return_Details.objects.filter(P_id=personal_instance):
		# 	log.info(rd.R_id)

	return R_ID

@csrf_protect
@csrf_exempt
def failure(request):
	# c = {}
	# c.update(csrf(request))
	status=request.POST["status"]
	log.info(status)
	firstname=request.POST["firstname"]
	amount=request.POST["amount"]
	txnid=request.POST["txnid"]
	posted_hash=request.POST["hash"]
	key=request.POST["key"]
	productinfo=request.POST["productinfo"]
	email=request.POST["email"]
	salt=""
	try:
		additionalCharges=request.POST["additionalCharges"]
		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	except Exception:
		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
	if(hashh !=posted_hash):
		log.info( "Invalid Transaction. Please try again")
	else:
		log.info( "Thank You. Your order status is ", status)
		log.info( "Your Transaction ID for this transaction is ",txnid)
 	# return render_to_response("Failure.html",RequestContext(request,c))

	return render(request,'Failure.html',
        {
        'status':status,
        'txnid':txnid,
        'amount':amount,
        'productinfo':productinfo,
        })

@csrf_exempt
def send_feedback(request):
	if request.method=='POST':
		result={}
		try:
			email = request.POST.get('email')
			full_name = request.POST.get('full_name')
			message_text = request.POST.get('message')

			get_user_email = email
			sender = 'salattax@gmail.com'
			reciever = get_user_email
			msg = MIMEMultipart('alternative')
			msg['Subject'] = "Salat Tax Feedback"
			msg['From'] = sender
			msg['To'] = reciever

			html ='<html><title>Salat Tax</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet><style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style><body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0><table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%><tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff><tr><td style="border-top:10px solid #00A0E3"><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center><tr><td style="padding:20px 0"><table border=0 cellpadding=0 cellspacing=0 class=logo style=width:220px align=left><tr><td><a href="" target=_blank><img alt="Salat Tax" src="https://salattax.com/static/images/logo_blue.png" border=0></a></table></table><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style="width:640px;border-top:thin solid #00A0E3" align=center><tr><td style="padding:20px 0 10px;font-weight:700;font-size:18px;font-family:Verdana"class=header-title noto=""sans=""><span style=font-size:14px;font-family:verdana;font-weight:400;line-height:20px>This email is Feedback from '+full_name+'<span><a style="font-weight:600;color:#00A0E3;cursor:pointer;text-align:center;text-decoration: none;" href=# target="_blank"></a></span>.<br>Feedback : <b></b><br>'+message_text+'<br><br>Sincere Regards,<br><span style=color:#00A0E3>Salat Tax</span></span><tr><td style=padding-bottom:20px;text-align:center class=header-divider></table><br><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style="width:700px;border-bottom:10px solid #00A0E3"align=center><tr><td style="padding:0 0px 20px 20px"><table class=container style="width:640px;border-top:thin solid #00A0E3"><tr><td style=font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:1px class=footer-copy></table></table></table></table></html>'
			part2 = MIMEText(html, 'html')

			msg.attach(part2)
			# Send the message via local SMTP server.
			mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
			mail.ehlo()
			mail.starttls()
			mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
			mail.sendmail(sender, reciever, msg.as_string())

			result['status']= 'success'
			result['email']= email
			result['full_name']= full_name
			result['message_text']= message_text
		except Exception as ex:
			result['status']= 'Error send_feedback: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def save_tax_plan(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			data_pass = request.POST.get('data_pass')
			req_data = json.loads(data_pass)
			log.info(req_data)

			emp_state = req_data['emp_state']
			financial_year = req_data['financial_year']
			doj1 = req_data['doj1']
			doj2 = req_data['doj2']
			doj3 = req_data['doj3']
			ld1 = req_data['ld1']
			ld2 = req_data['ld2']
			type_c1 = req_data['type_c1']
			type_c2 = req_data['type_c2']
			type_c3 = req_data['type_c3']
			type_c4 = req_data['type_c4']
			dob_c1 = req_data['dob_c1']
			dob_c2 = req_data['dob_c2']
			dob_c3 = req_data['dob_c3']
			dob_c4 = req_data['dob_c4']
			hra1 = ''
			hra2 = ''
			hra3 = ''
			if(financial_year == '1' or financial_year == '2' or financial_year == '3') :
				bs1 = req_data['bs1']
				hra1 = req_data['hra1']
				doj1 = doj1.split('/')[2] +'-'+ doj1.split('/')[1] +'-'+ doj1.split('/')[0]
			if(financial_year == '2' or financial_year == '3') :
				bs2 = req_data['bs2']
				hra2 = req_data['hra2']
				ld1 = ld1.split('/')[2] +'-'+ ld1.split('/')[1] +'-'+ ld1.split('/')[0]
				doj2 = doj2.split('/')[2] +'-'+ doj2.split('/')[1] +'-'+ doj2.split('/')[0]
			if(financial_year == '3') :
				bs3 = req_data['bs3']
				hra3 = req_data['hra3']
				ld2 = ld2.split('/')[2] +'-'+ ld2.split('/')[1] +'-'+ ld2.split('/')[0]
				doj3 = doj3.split('/')[2] +'-'+ doj3.split('/')[1] +'-'+ doj3.split('/')[0]
				ld3 = '2019-03-31';
			if(financial_year == '1'):
				ld1 = '2019-03-31'
			if(financial_year == '2'):
				ld2 = '2019-03-31'
			current_city = req_data['current_city']
			parent_age = req_data['parent_age']
			children_value = req_data['children_value']
			if(children_value == '1' or children_value == '2' or children_value == '3' or children_value == '4') :
				dob_c1 = dob_c1.split('/')[2] +'-'+ dob_c1.split('/')[1] +'-'+ dob_c1.split('/')[0]
			if(children_value == '2' or children_value == '3' or children_value == '4') :
				dob_c2 = dob_c2.split('/')[2] +'-'+ dob_c2.split('/')[1] +'-'+ dob_c2.split('/')[0]
			if(children_value == '3' or children_value == '4') :
				dob_c3 = dob_c3.split('/')[2] +'-'+ dob_c3.split('/')[1] +'-'+ dob_c3.split('/')[0]
			if(children_value == '4') :
				dob_c4 = dob_c4.split('/')[2] +'-'+ dob_c4.split('/')[1] +'-'+ dob_c4.split('/')[0]
			
			state_of_accommodation = req_data['state_of_accommodation']
			rent_paid = req_data['rent_paid']
			total_travel_expense = req_data['total_travel_expense']
			home_loan_taken = req_data['home_loan_taken']
			incurred_travel_expense = req_data['incurred_travel_expense']
			principal_amt = req_data['principal_amt']
			interest_amt = req_data['interest_amt']
			epf = req_data['epf']
			school_fee = req_data['school_fee']
			lic = req_data['lic']
			elss = req_data['elss']
			ulip = req_data['ulip']
			fd = req_data['fd']
			ppf = req_data['ppf']
			other_80c = req_data['other_80c']
			nsc = req_data['nsc']
			sukanya = req_data['sukanya']
			post_office_deposit = req_data['post_office_deposit']
			r_principal_home_loan = req_data['r_principal_home_loan']
			stamp_duty = req_data['stamp_duty']
			nps = req_data['nps']
			mi_your = req_data['mi_your']
			mi_parent = req_data['mi_parent']
			mc_your = req_data['mc_your']
			education_loan = req_data['education_loan']
			Disibility = req_data['Disibility']
			max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
			R_ID = 0

			if(state_of_accommodation != 'Rented'):
				rent_paid = 'NA'
			if(home_loan_taken == 'no'):
				principal_amt = 'NA'
				interest_amt = 'NA'
			if(incurred_travel_expense == 'no'):
				total_travel_expense = 'NA'

			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if not Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
				Return_Details.objects.create(
					R_id = max_r_id['R_id__max']+1,
					P_id = personal_instance,
					FY = '2018-2019',
					AY = '2017-2018',
				).save()

			if(children_value == '0'):
				if child_info.objects.filter(p_id=client_id).exists():
					child_info.objects.filter(p_id=client_id).delete()
			if(children_value == '1'):
				if child_info.objects.filter(p_id=client_id,child_no='2').exists():
					child_info.objects.filter(p_id=client_id,child_no='2').delete()
				if child_info.objects.filter(p_id=client_id,child_no='3').exists():
					child_info.objects.filter(p_id=client_id,child_no='3').delete()
				if child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.filter(p_id=client_id,child_no='4').delete()
			if(children_value == '2'):
				if child_info.objects.filter(p_id=client_id,child_no='3').exists():
					child_info.objects.filter(p_id=client_id,child_no='3').delete()
				if child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.filter(p_id=client_id,child_no='4').delete()
			if(children_value == '3'):
				if child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.filter(p_id=client_id,child_no='4').delete()

			log.info('children_value')
			if(children_value == '1' or children_value == '2' or children_value == '3' or children_value == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='1').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='1',
						child_type=type_c1,
						Child_dob=dob_c1,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='1')
					child_info_i.child_no = '1'
					child_info_i.child_type = type_c1
					child_info_i.Child_dob = dob_c1
					child_info_i.save()
			if(children_value == '2' or children_value == '3' or children_value == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='2').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='2',
						child_type=type_c2,
						Child_dob=dob_c2,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='2')
					child_info_i.child_no = '2'
					child_info_i.child_type = type_c2
					child_info_i.Child_dob = dob_c2
					child_info_i.save()
			if(children_value == '3' or children_value == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='3').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='3',
						child_type=type_c3,
						Child_dob=dob_c3,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='3')
					child_info_i.child_no = '3'
					child_info_i.child_type = type_c3
					child_info_i.Child_dob = dob_c3
					child_info_i.save()
			if(children_value == '4'):
				if not child_info.objects.filter(p_id=client_id,child_no='4').exists():
					child_info.objects.create(
						p_id=client_id,
						child_no='4',
						child_type=type_c4,
						Child_dob=dob_c4,
					).save()
				else:
					child_info_i=child_info.objects.get(p_id=client_id,child_no='4')
					child_info_i.child_no = '4'
					child_info_i.child_type = type_c4
					child_info_i.Child_dob = dob_c4
					child_info_i.save()

			if Personal_Details.objects.filter(P_Id=client_id).exists():
				personal_details_instance=Personal_Details.objects.get(P_Id=client_id)
				personal_details_instance.dependent_parent_age = parent_age
				personal_details_instance.no_of_child = children_value
				personal_details_instance.state_of_employment = emp_state
				personal_details_instance.disability = Disibility
				personal_details_instance.save()

			# log.info('accommodation_details')
			if not accommodation_details.objects.filter(p_id=client_id).exists():
				accommodation_details.objects.create(
                    p_id=client_id,
                    FY='2018-19',
                    type_of_accommodation=state_of_accommodation,
                    current_place=current_city,
                    annual_rent_paid=rent_paid,
                ).save()
			else:
				accommodation_details_i=accommodation_details.objects.get(p_id=client_id)
				accommodation_details_i.FY = '2018-19'
				accommodation_details_i.type_of_accommodation = state_of_accommodation
				accommodation_details_i.current_place = current_city
				accommodation_details_i.annual_rent_paid = rent_paid
				accommodation_details_i.save()

			if Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
				for client in Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019'):
					R_ID = client.R_id

			log.info('home_loan')
			if not home_loan.objects.filter(R_id=R_ID).exists():
				home_loan.objects.create(
                    R_id=R_ID,
                    principal_amount=principal_amt,
                    interest_amount=interest_amt,
                ).save()
			else:
				home_loan_i=home_loan.objects.get(R_id=R_ID)
				home_loan_i.principal_amount = principal_amt
				home_loan_i.interest_amount = interest_amt
				home_loan_i.save()

			# log.info('allowances')
			if not allowances.objects.filter(R_id=R_ID).exists():
				allowances.objects.create(
                    R_id=R_ID,
                    travel_expense_incurred=total_travel_expense,
                ).save()
			else:
				allowances_i=allowances.objects.get(R_id=R_ID)
				allowances_i.travel_expense_incurred = total_travel_expense
				allowances_i.save()

			log.info('details_80c')
			if not details_80c.objects.filter(R_id=R_ID).exists():
				details_80c.objects.create(
                    R_id=R_ID,
                    elss=elss,
                    epf=epf,
                    bank_fd=fd,
                    school_fee=school_fee,
                    ppf=ppf,
                    life_insurance=lic,
                    ulip=ulip,
                    nsc=nsc,
                    sukanya_samriddhi_scheme=sukanya,
                    post_office_deposit=post_office_deposit,
                    repayment_principal_home_loan=r_principal_home_loan,
                    stamp_duty=stamp_duty,
                    home_loan=home_loan_taken,
                ).save()
			else:
				details_80c_i=details_80c.objects.get(R_id=R_ID)
				details_80c_i.elss = elss
				details_80c_i.epf = epf
				details_80c_i.bank_fd = fd
				details_80c_i.school_fee = school_fee
				details_80c_i.ppf = ppf
				details_80c_i.life_insurance = lic
				details_80c_i.ulip = ulip
				details_80c_i.nsc = nsc
				details_80c_i.sukanya_samriddhi_scheme = sukanya
				details_80c_i.post_office_deposit = post_office_deposit
				details_80c_i.repayment_principal_home_loan = r_principal_home_loan
				details_80c_i.stamp_duty = stamp_duty
				details_80c_i.home_loan = home_loan_taken
				details_80c_i.save()

			# log.info('details_80d')
			if not details_80d.objects.filter(R_id=R_ID).exists():
				details_80d.objects.create(
                    R_id=R_ID,
                    mi_self=mi_your,
                    mi_parents=mi_parent,
                    mc_self=mc_your,
                ).save()
			else:
				details_80d_i=details_80d.objects.get(R_id=R_ID)
				details_80d_i.mi_self = mi_your
				details_80d_i.mi_parents = mi_parent
				details_80d_i.mc_self = mc_your
				details_80d_i.save()

			log.info('VI_Deductions')
			if not VI_Deductions.objects.filter(R_Id=R_ID).exists():
				VI_Deductions.objects.create(
                    R_Id = R_ID,
                    VI_80_CCD_1B = nps,
                    VI_80_E = education_loan,
                ).save()
			else:
				VI_Deductions_i=VI_Deductions.objects.get(R_Id=R_ID)
				VI_Deductions_i.VI_80_CCD_1B = nps
				VI_Deductions_i.VI_80_E = education_loan
				VI_Deductions_i.save()

			log.info('delete financial_year')
			if(financial_year == '0'):
				if Employer_Details.objects.filter(P_id=personal_instance).exists():
					Employer_Details.objects.filter(P_id=personal_instance).delete()
			if(financial_year == '1'):
				if Employer_Details.objects.filter(P_id=personal_instance,F16_no = '2').exists():
					Employer_Details.objects.filter(P_id=personal_instance,F16_no = '2').delete()
				if Employer_Details.objects.filter(P_id=personal_instance,F16_no = '3').exists():
					Employer_Details.objects.filter(P_id=personal_instance,F16_no = '3').delete()
			if(financial_year == '2'):
				if Employer_Details.objects.filter(P_id=personal_instance,F16_no = '3').exists():
					Employer_Details.objects.filter(P_id=personal_instance,F16_no = '3').delete()

			if(financial_year == '1' or financial_year == '2' or financial_year == '3') :
				log.info('financial_year1')
				if not Employer_Details.objects.filter(P_id=personal_instance,F16_no = '1').exists():
					Employer_Details.objects.create(
	                    P_id = personal_instance,
	                    FY = '2018-19',
	                    F16_no = '1',
	                    start_date = doj1,
	                    end_date = ld1,
	                    monthly_basic_salary = bs1,
	                    monthly_hra = hra1,
	                ).save()
				else:
					Employer_Details_i = Employer_Details.objects.get(P_id=personal_instance,F16_no = '1')
					Employer_Details_i.FY = '2018-19'
					Employer_Details_i.F16_no = '1'
					Employer_Details_i.start_date = doj1
					Employer_Details_i.end_date = ld1
					Employer_Details_i.monthly_basic_salary = bs1
					Employer_Details_i.monthly_hra = hra1
					Employer_Details_i.save()
			if(financial_year == '2' or financial_year == '3') :
				log.info('financial_year2')
				if not Employer_Details.objects.filter(P_id=personal_instance,F16_no = '2').exists():
					Employer_Details.objects.create(
	                    P_id = personal_instance,
	                    FY = '2018-19',
	                    F16_no = '2',
	                    start_date = doj2,
	                    end_date = ld2,
	                    monthly_basic_salary = bs2,
	                    monthly_hra = hra2,
	                ).save()
				else:
					Employer_Details_i = Employer_Details.objects.get(P_id=personal_instance,F16_no = '2')
					Employer_Details_i.FY = '2018-19'
					Employer_Details_i.F16_no = '2'
					Employer_Details_i.start_date = doj2
					Employer_Details_i.end_date = ld2
					Employer_Details_i.monthly_basic_salary = bs2
					Employer_Details_i.monthly_hra = hra2
					Employer_Details_i.save()
			if(financial_year == '3') :
				log.info('financial_year3')
				if not Employer_Details.objects.filter(P_id=personal_instance,F16_no = '3').exists():
					Employer_Details.objects.create(
	                    P_id = personal_instance,
	                    FY = '2018-19',
	                    F16_no = '3',
	                    start_date = doj3,
	                    monthly_basic_salary = bs3,
	                    monthly_hra = hra3,
	                ).save()
				else:
					Employer_Details_i = Employer_Details.objects.get(P_id=personal_instance,F16_no = '3')
					Employer_Details_i.FY = '2018-19'
					Employer_Details_i.F16_no = '3'
					Employer_Details_i.start_date = doj3
					Employer_Details_i.monthly_basic_salary = bs3
					Employer_Details_i.monthly_hra = hra3
					Employer_Details_i.save()

			result['status']= 'success'
			result['client_id']= client_id
			result['data_pass']= data_pass
			result['max_r_id']= max_r_id['R_id__max']
		except Exception as ex:
			log.info( 'Error save_tax_plan: %s' % ex)
			result['status']= 'Error save_tax_plan: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_tax_plan(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			financial_year = 0
			start_date = []
			end_date = []
			m_basic_salary = []
			m_hra = []
			emp_state = ''
			state_of_accommodation = ''
			current_city = ''
			rent_paid = ''
			parent_age = ''
			no_of_child = ''
			disability = ''
			total_travel_expense = ''
			principal_amt = ''
			interest_amt = ''
			home_loan_taken = ''
			epf = ''
			school_fee = ''
			lic = ''
			elss = ''
			ulip = ''
			fd = ''
			ppf = ''
			other_80c = ''
			nps = ''
			mi_your = ''
			mi_parent = ''
			mc_your = ''
			education_loan = ''
			child_type = []
			child_dob = []
			child_age = []
			max_r_id = Return_Details.objects.all().aggregate(Max('R_id'))
			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if not Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
				Return_Details.objects.create(
					R_id = max_r_id['R_id__max']+1,
					P_id = personal_instance,
					FY = '2018-2019',
					AY = '2017-2018',
				).save()			

			personal_instance=Personal_Details.objects.get(P_Id=client_id)
			if Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019').exists():
				for client in Return_Details.objects.filter(P_id=personal_instance,FY = '2018-2019'):
					R_ID = client.R_id

			if Employer_Details.objects.filter(P_id=personal_instance,FY = '2018-19').exists():
				for ed in Employer_Details.objects.filter(P_id=personal_instance,FY = '2018-19'):
					financial_year = financial_year + 1
					if(ed.start_date is None):
						start_date.append('')
					else:
						s_date = ''
						try:
							s_date = ed.start_date.strftime('%d/%m/%Y')
						except Exception as ex:
							log.info( 'Error in date: %s' % ex)
							try:
								s_date = ed.start_date.split('-')[2] +'/'+ ed.start_date.split('-')[1] +'/'+ ed.start_date.split('-')[0]
							except Exception as ex:
								log.info( 'Error in date: %s' % ex)
						start_date.append(s_date)
					if(ed.end_date is None):
						end_date.append('')
					else:
						e_date = ''
						try:
							e_date = ed.end_date.strftime('%d/%m/%Y')
						except Exception as ex:
							log.info( 'Error in date: %s' % ex)
							try:
								s_date = ed.end_date.split('-')[2] +'/'+ ed.end_date.split('-')[1] +'/'+ ed.end_date.split('-')[0]
							except Exception as ex:
								log.info( 'Error in date: %s' % ex)
						end_date.append(e_date)
					m_basic_salary.append(ed.monthly_basic_salary)
					m_hra.append(ed.monthly_hra)

			if accommodation_details.objects.filter(p_id=client_id).exists():
				for ad in accommodation_details.objects.filter(p_id=client_id):
					state_of_accommodation = ad.type_of_accommodation
					current_city= ad.current_place
					rent_paid= ad.annual_rent_paid

			if Personal_Details.objects.filter(P_Id=client_id).exists():
				for pd in Personal_Details.objects.filter(P_Id=client_id):
					parent_age = pd.dependent_parent_age
					no_of_child= pd.no_of_child
					emp_state= pd.state_of_employment
					disability= pd.disability

			if allowances.objects.filter(R_id=R_ID).exists():
				for a in allowances.objects.filter(R_id=R_ID):
					total_travel_expense = a.travel_expense_incurred

			if home_loan.objects.filter(R_id=R_ID).exists():
				for hl in home_loan.objects.filter(R_id=R_ID):
					principal_amt = hl.principal_amount
					interest_amt = hl.interest_amount

			if details_80c.objects.filter(R_id=R_ID).exists():
				for d in details_80c.objects.filter(R_id=R_ID):
					elss= d.elss or 0
					epf= d.epf or 0
					fd= d.bank_fd or 0
					school_fee= d.school_fee or 0
					ppf= d.ppf or 0
					lic= d.life_insurance or 0
					ulip= d.ulip or 0
					nsc= d.nsc or 0
					sukanya= d.sukanya_samriddhi_scheme or 0
					post_office_deposit= d.post_office_deposit or 0
					r_principal_home_loan= d.repayment_principal_home_loan or 0
					stamp_duty= d.stamp_duty or 0
					home_loan_taken= d.home_loan or 0

			if VI_Deductions.objects.filter(R_Id=R_ID).exists():
				for d in VI_Deductions.objects.filter(R_Id=R_ID):
					nps = d.VI_80_CCD_1B
					education_loan = d.VI_80_E

			if details_80d.objects.filter(R_id=R_ID).exists():
				for d in details_80d.objects.filter(R_id=R_ID):
					mi_your = d.mi_self
					mi_parent = d.mi_parents
					mc_your = d.mc_self

			if child_info.objects.filter(p_id=client_id).exists():
				for child in child_info.objects.filter(p_id=client_id):
					child_type.append(child.child_type)
					if(child.Child_dob is None):
						child_dob.append('')
					else:
						child_dob.append(child.Child_dob.strftime('%d/%m/%Y'))
					child_age.append(child.Child_age)

			result['status']= 'success'
			result['client_id']= client_id
			result['R_ID']= R_ID
			result['financial_year']= financial_year
			result['start_date']= start_date
			result['end_date']= end_date
			result['m_basic_salary']= m_basic_salary
			result['m_hra']= m_hra
			result['emp_state']= emp_state
			result['state_of_accommodation']= state_of_accommodation
			result['current_city']= current_city
			result['rent_paid']= rent_paid
			result['parent_age']= parent_age
			result['no_of_child']= no_of_child
			result['disability']= disability
			result['total_travel_expense']= total_travel_expense
			result['principal_amt']= principal_amt
			result['interest_amt']= interest_amt
			result['elss']= elss
			result['epf']= epf
			result['fd']= fd
			result['school_fee']= school_fee
			result['ppf']= ppf
			result['lic']= lic
			result['ulip']= ulip
			result['nsc']= nsc
			result['sukanya']= sukanya
			result['post_office_deposit']= post_office_deposit
			result['r_principal_home_loan']= r_principal_home_loan
			result['stamp_duty']= stamp_duty
			result['home_loan_taken']= home_loan_taken
			result['nps']= nps
			result['education_loan']= education_loan
			result['mi_your']= mi_your
			result['mi_parent']= mi_parent
			result['mc_your']= mc_your
			result['child_type']= child_type
			result['child_dob']= child_dob
			result['child_age']= child_age
		except Exception as ex:
			log.info('Error get_tax_plan:' +traceback.format_exc())
			result['status']= 'Error get_tax_plan: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def show_26as(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			pan = request.POST.get('pan')
			year = []
			tds_26as = []
			part = []
			Deductor_Collector_Name = []
			section = []
			no_of_transaction = []
			amount_paid = []
			tax_deducted = []
			TAN_PAN = []
			count = 0

			if tds_tcs.objects.filter(R_Id=pan).exists():
				for ed in tds_tcs.objects.filter(R_Id=pan):
					count = count + 1
					year.append(ed.year)
					tds_26as.append(ed.tds_tcs_deposited)
					part.append(ed.part)
					Deductor_Collector_Name.append(ed.Deductor_Collector_Name)
					TAN_PAN.append(ed.TAN_PAN)
					section.append(ed.section)
					no_of_transaction.append(ed.no_of_transaction)
					amount_paid.append(ed.amount_paid)
					tax_deducted.append(ed.tax_deducted)

			result['status']= 'success'
			result['client_id']= client_id
			result['pan']= pan
			result['count']= count
			result['year']= year
			result['tds_26as']= tds_26as
			result['part']= part
			result['Deductor_Collector_Name']= Deductor_Collector_Name
			result['TAN_PAN']= TAN_PAN
			result['section']= section
			result['no_of_transaction']= no_of_transaction
			result['amount_paid']= amount_paid
			result['tax_deducted']= tax_deducted
		except Exception as ex:
			result['status']= 'Error show 26as: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def send_communication_feedback(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			message_text = request.POST.get('message_feedback')

			get_user_email = 'jagrutikadam15@gmail.com'
			sender = 'salattax@gmail.com'
			reciever = get_user_email
			msg = MIMEMultipart('alternative')
			msg['Subject'] = "Salat Tax Feedback"
			msg['From'] = sender
			msg['To'] = reciever

			html ='<html><title>Salat Tax</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet><style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style><body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0><table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%><tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff><tr><td style="border-top:10px solid #00A0E3"><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center><tr><td style="padding:20px 0"><table border=0 cellpadding=0 cellspacing=0 class=logo style=width:220px align=left><tr><td><a href="" target=_blank><img alt="Salat Tax" src="https://salattax.com/static/images/logo_blue.png" border=0></a></table></table><table border=0 cellpadding=0 cellspacing=0 width=640 class=container style="width:640px;border-top:thin solid #00A0E3" align=center><tr><td style="padding:20px 0 10px;font-weight:700;font-size:18px;font-family:Verdana"class=header-title noto=""sans=""><span style=font-size:14px;font-family:verdana;font-weight:400;line-height:20px>This email is Feedback from '+client_id+'<span><a style="font-weight:600;color:#00A0E3;cursor:pointer;text-align:center;text-decoration: none;" href=# target="_blank"></a></span>.<br>Feedback : <b></b><br>'+message_text+'<br><br>Sincere Regards,<br><span style=color:#00A0E3>Salat Tax</span></span><tr><td style=padding-bottom:20px;text-align:center class=header-divider></table><br><table border=0 cellpadding=0 cellspacing=0 width=700 class=container style="width:700px;border-bottom:10px solid #00A0E3"align=center><tr><td style="padding:0 0px 20px 20px"><table class=container style="width:640px;border-top:thin solid #00A0E3"><tr><td style=font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:1px class=footer-copy></table></table></table></table></html>'
			part2 = MIMEText(html, 'html')

			msg.attach(part2)
			# Send the message via local SMTP server.
			mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
			mail.ehlo()
			mail.starttls()
			mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
			mail.sendmail(sender, reciever, msg.as_string())

			result['status']= 'success'
			result['message_text']= message_text
		except Exception as ex:
			result['status']= 'Error send_feedback: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def update_financial_goal(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			selected_goal = request.POST.get('selected_goal')
			pan = request.POST.get('pan')

			personal_instance=Personal_Details.objects.get(P_Id=client_id)

			if Personal_Details.objects.filter(P_Id=client_id).exists():
				personal_details_instance=Personal_Details.objects.get(P_Id=client_id)
				personal_details_instance.financial_goal = selected_goal
				# personal_details_instance.no_of_child = children_value
				# personal_details_instance.state_of_employment = emp_state
				# personal_details_instance.disability = Disibility
				personal_details_instance.save()

			result['status']= 'success'
			result['client_id']= client_id
		except Exception as ex:
			result['status']= 'Error Update financial goal: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_payment_info(request):
	if request.method=='POST':
		result={}
		try:
			client_id = request.POST.get('client_id')
			option = request.POST.get('option')
			R_Id=get_rid(client_id)
			paid = 0

			if option == 'efiling':
				RF_payment_details_id=RFPaymentDetails.objects.get(R_Id=R_Id).id
				module_data_id=Modules.objects.get(module_name='Return Filing').id
				invoice_no = 0
				if Invoice_Details.objects.filter(client_id=client_id,payment_status='success').exists():
					for in_d in Invoice_Details.objects.filter(client_id=client_id,payment_status='success'):
						invoice_no = in_d.invoice_no
						if Module_Subscription.objects.filter(invoice_no=invoice_no,module_Id=module_data_id,module_detail1=RF_payment_details_id).exists():
							for ms in Module_Subscription.objects.filter(invoice_no=invoice_no,module_Id=module_data_id,module_detail1=RF_payment_details_id):
								paid = 1
						# else:
							# log.info('No Module_Subscription entry for E-filing, invoice_no : '+str(invoice_no))
				else:
					log.info('No Invoice_Details entry')
			else:
				if Invoice_Details.objects.filter(client_id=client_id,payment_status='success').exists():
					paid = 1

			result['status']= 'success'
			result['client_id']= client_id
			result['paid']= paid
			result['option']= option
		except Exception as ex:
			result['status']= 'Error get payment info: %s' % traceback.format_exc()
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def send_subscribed_mail(request,client_id,plan):
	username = request.session.get('login')
	client_id = 0
	email = ''
	if username is None:
		pass
	else:
		if username!='':
			atuser = User.objects.get(username=username)
			for c in SalatTaxUser.objects.filter(user=atuser):
				# client_id = c.id
				email = c.email

	# log.info(username)
	log.info(email)

	if(email!=''):
		# get_user_email = email
		sender = 'salattax@gmail.com'
		reciever = email
		msg = MIMEMultipart('alternative')
		msg['Subject'] = "Your Salat Tax Savings Account is now active"
		msg['From'] = sender
		msg['To'] = reciever

		html ='<html><title>Salat Tax</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic"rel=stylesheet>';
		html += '<style>body,html{margin:0!important;padding:0!important;height:100%!important;width:100%!important;font-family:Verdana,sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0!important}table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic;width:100%;height:auto!important}.yshortcuts a{border-bottom:none!important}.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}@media (max-width:700px){.container{width:100%!important}.container-outer{padding:0!important}.logo{float:none;text-align:center}.header-title{text-align:left!important;font-size:20px!important}.header-divider{padding-bottom:30px!important;text-align:center!important}.article-button,.article-content,.article-thumb,.article-title{text-align:center!important;padding-left:15px!important}.article-thumb{padding:30px 0 15px 0!important}.article-title{padding:0 0 15px 0!important}.article-content{padding:0 15px 0 15px!important}.article-button{padding:20px 0 0 0!important}.article-button>table{float:none}.footer-copy{text-align:center!important}}</style>';
		html += '<body bgcolor=#f5f5f5 leftmargin=0 marginheight=0 marginwidth=0 style=margin:0;padding:0 topmargin=0>';
		html += '<table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#f5f5f5 height=100%>';
		html += '<tr><td style="padding:30px 0 30px 0"class=container-outer align=center valign=top>';
		html += '<table border=0 cellpadding=0 cellspacing=0 width=700 class=container style=width:700px bgcolor=#ffffff>';
		html += '<tr><td style="border-top:10px solid #00A0E3">';
		html += '<table border=0 cellpadding=0 cellspacing=0 width=640 class=container style=width:640px align=center>';
		html += '<tr><td style="padding:20px 0">';
		html += '<table border=0 cellpadding=0 cellspacing=0 class=logo style=width:220px align=left>';
		html += '<tr><td><a href="" target=_blank><img alt="Salat Tax" src="https://salattax.com/static/images/logo_blue.png" border=0></a>';
		html += '</table></table>';
		html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="m_-6119505529415927820container" style="width:640px;border-top:thin solid #009ee3" width="640">';
		
		html += '<tbody>';
		html += '<tr>';
		html += '<td class="m_-6119505529415927820header-divider" style="padding-bottom:5px;text-align:center">';
		html += '<p style="text-align:left"><span style="font-size:14px">Hi&nbsp;</span></p>';
		html += '<p style="text-align:left"><span style="font-size:13px"><span style="font-size:14px">Congratulations! Your Salat Tax Savings Account is now active. You have subscribed to a "'+plan+'".<br><br>';
		html += 'You are now a part of smart group of people who wish to pay minimum taxes through prudent tax planning. So well done!<span style="font-family:verdana">&nbsp;</span></span><img alt="Well done!" src="https://ci4.googleusercontent.com/proxy/aIMjbHZcxrm0PnKT7FOtJmYH-Ml3hPX-jP75WPOF-6Rld8lgycxtQT5T0WyyZ-OyFNLlGNzakFJZNwZcVJCvltqL1Pn4qpkIwGIrgjrTO6Npbcyf1nxSHb1swLGP3_iRvJNf3TGSyge_hg1RHA=s0-d-e1-ft#https://bucket.mlcdn.com/a/440/440943/images/6aca1643d9e782c172fd96798ae4320bcbcd6028.jpeg" style="text-align:center" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01;left: 825px;top: 490px;"><div id=":v7" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div></span></p>';
		html += '<p style="text-align:left"><span style="font-size:13px"><span style="font-size:14px">This is how you can make most of your Salat Tax Savings Account</span>.</span></p>';
		html += '<ol>';
		html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><u><span style="font-size:16px">Check out your Tax Analytics Report</span></u></span><br><span style="font-size:14px">Tax Analytics Report contains detailed analysis of your tax leakages for up to last 5 years. You can see for yourself what could have been done better and how you could have saved more in taxes. You will surely love this!</span></span><br>&nbsp;</li>';
		html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><u><span style="font-size:16px">Plan your taxes smartly with Salat Tax Planner</span></u></span><br><span style="font-size:14px">With Salat Tax Planner you can very easily plan you taxes for current financial year. Just fill out the information asked and tailor made tax saving recommendations will be given to you.&nbsp;</span></span><br>&nbsp;</li>';
		html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><span style="font-size:16px"><u>Free Income Tax Return Filing</u></span></span><br><span style="font-size:14px">You are eligible for free IT return filing for FY 2018-19. The return filing portal will&nbsp;be&nbsp;opened in the month of June. We will keep you updated when it is up.</span></span><br>&nbsp;</li>';
		html += '<li style="text-align:left"><span style="font-size:13px"><span style="color:#00a3e8"><span style="font-size:16px"><u>Earn Rewards</u></span></span><br><span style="font-size:14px">You can make Salat Tax Savings Account free for you. Just share the coupon code <strong>EARN18</strong> with your friends and you shall get a Rs.<strong>200 cashback</strong> when any of your friend opens an account using your code. Your friend shall also get a Rs. 200 discount on the subscription amount.</span></span><br>&nbsp;</li>';
		html += '</ol>';
		html += '<p style="text-align:justify"><span style="font-size:14px">So Happy Tax Saving!</span></p>';
		html += '<p style="text-align:left"><span style="font-size:14px">Team Salat Tax</span></p>';
		html += '</td>';
		html += '</tr>';
		html += '</tbody>';
		
		html += '</table>';
		html += '<br><table align="center" border="0" cellpadding="0" cellspacing="0" class="m_-1230022283182079595container" style="width:700px;border-bottom:10px solid #00a0e3" width="700"><tbody><tr><td style="padding:0px 0 20px 0"><table class="m_-1230022283182079595container" style="width:640px;border-top:thin solid #009ee3"><tbody><tr><td class="m_-1230022283182079595footer-copy" style="font-size:12px;color:#000;font-family:verdana;line-height:20px!important;padding-top:10px">If you have any queries regarding the product or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="m_-1230022283182079595email"> </a><a href="http://salatinvestments.com/sendy/unsubscribe-success.php?c=98" style="color:#00a0e3;text-decoration:none;font-size:13px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://salatinvestments.com/sendy/unsubscribe-success.php?c%3D98&amp;source=gmail&amp;ust=1551152760728000&amp;usg=AFQjCNGEMpTGfk9Qxtp-KjGrWx6DEKBRPw"> unsubscribe here</a> </td></tr></tbody></table>';
		html += '</td></tr></tbody></table></table></table></table></html>';
		part2 = MIMEText(html, 'html')

		msg.attach(part2)
		# Send the message via local SMTP server.
		mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
		mail.ehlo()
		mail.starttls()
		mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
		mail.sendmail(sender, reciever, msg.as_string())

		log.info('success')

@csrf_exempt
def send_marketing_mail(request):
	result={}
	if request.method=='POST':
		try:
			log.info( 'Start send_marketing_mail')
			option = request.POST.get('option')
			client_id = []
			email = []
			detail = []
			send_mail = 0
			trigger_mail = 0
			# option - tax_leakage_report - Tax Leakage Report 1
			# option - tax_leakage_report2 - Tax Leakage Report 2
			# option - sales_and_tax_leakage_report - Sales 3 Tax Leakage Report
			# option - sales1 - Sales 1
			# option - sales2 - Sales 2
			# option - sales4 - Sales 4

			# log.info(option)
			if Personal_Details.objects.exclude(pan='').exists():
				for client in Personal_Details.objects.exclude(pan=''):
					client_id.append(str(client.P_Id) +' - '+ (client.name or '') )

					# log.info(client.P_Id)
					salat_client = 0
					went_on_last_slide = 0
					subscribed = 0
					if salat_registration.objects.filter(pan=client.pan).exists():
						salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
						if salat_registration_instance.is_salat_client == 'Salat Client':
							salat_client = 1
					if user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide').exists():
						went_on_last_slide = 1
					if user_tracking.objects.filter(client_id=client.P_Id,event_name='success',event='payment').exists():
						subscribed = 1

					if option == 'tax_leakage_report':
						mail_content='Tax Leakage Report 1'
						if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
							trigger_mail = 1
					if option == 'sales_and_tax_leakage_report':
						mail_content='Sales 3 Tax Leakage Report'
						trigger_mail_cond1 = 0
						trigger_mail_cond2 = 0
						if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
							trigger_mail_cond1 = 1
						if Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content='Tax Leakage Report 1').exists():
							Mail_Trigger_i = Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content='Tax Leakage Report 1').order_by('-created_time')[0]
							time_diff = timezone.now() - Mail_Trigger_i.created_time
							minute = int(time_diff.seconds / 60)
							hour = minute / 60
							if( hour >= 23):
								trigger_mail_cond2 = 1
						if (trigger_mail_cond1 ==1 and trigger_mail_cond2 == 1):
							trigger_mail = 1
					if option == 'sales1':
						if salat_client ==0:
							if SalatTaxUser.objects.filter(id = client.P_Id).exists():
								for stu in SalatTaxUser.objects.filter(id=client.P_Id):
									log.info(stu.email + ' '+ str(client.P_Id))


					# log.info(str(client.P_Id) + ' : '+str(salat_client) + ' : '+str(went_on_last_slide)+ ' : '+str(subscribed)+ ' : '+str(trigger_mail))
					if salat_client ==1 and went_on_last_slide ==1 and subscribed == 0 and trigger_mail == 1:
						if SalatTaxUser.objects.filter(id = client.P_Id).exists():
							for stu in SalatTaxUser.objects.filter(id=client.P_Id):
								# detail.append(str(stu.id) +' - '+ stu.user.username+' - '+ stu.email)
								try:
									if option == 'tax_leakage_report':
										# log.info('*** '+str(client.P_Id) + ' mail_content : '+str(mail_content))
										if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
											sent_mail = tax_leakage_report(request, client.P_Id,stu.email)
											Mail_Trigger.objects.create(
												P_Id = client.P_Id,
												mail_content = mail_content,
												mail_id = stu.email,
												sent = sent_mail,
											).save()
									if option == 'sales_and_tax_leakage_report':
										if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
											sent_mail = sales_and_tax_leakage_report(request, client.P_Id,stu.email)
											Mail_Trigger.objects.create(
												P_Id = client.P_Id,
												mail_content = mail_content,
												mail_id = stu.email,
												sent = sent_mail,
											).save()
									send_mail = 1
								except Exception as e:
									send_mail = 2
									# log.info( 'Error sending mail: %s' % e)
									log.error('Error in sending mail: '+traceback.format_exc())
							
			result['status']= 'success'
			result['option']= option
			result['client_id']= client_id
			# result['email']= email
			result['send_mail'] = send_mail
			result['detail'] = detail
		except Exception as ex:
			result['status']= 'Error send_marketing_mail: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def tax_leakage_report(request,client_id,email):
	pan = ''
	p_age = 'NA'
	invested_share = 'no'
	shares_year = 1
	# username = request.session.get('login')
	# email = ''
	# if username is None:
	# 	pass
	# else:
	# 	if username!='':
	# 		atuser = User.objects.get(username=username)
	# 		for c in SalatTaxUser.objects.filter(user=atuser):
	# 			# client_id = c.id
	# 			email = c.email

	# log.info(username)
	# log.info(email)
	if Personal_Details.objects.filter(P_Id=client_id).exists():
		for client in Personal_Details.objects.filter(P_Id=client_id):
			pan = client.pan
			p_age = client.dependent_parent_age or ''
	if Investment_Details.objects.filter(P_Id=client_id).exists():
		for i_d in Investment_Details.objects.filter(P_Id = client_id):
			shares_year = i_d.year_of_first_share_sold or 1
			invested_share = 'yes'
	# log.info(pan)
	log.info('trigger tax_leakage_report for : '+str(client_id))

	if(email!=''):
		get_report = get_screenshot(client_id,pan)
		if get_report == 1 :
			s_tax = 0
			count_salattax = 0
			total_tax_leakage = 0
			for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
				if(itr.year!='2013'):
					s_tax = cal_salat_tax(pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
					total_tax_leakage = total_tax_leakage + (s_tax['tax_leakage'] *-1)
					count_salattax = count_salattax + 1
			# get_user_email = email
			if count_salattax >=2:
				sender = 'salattax@gmail.com'
				reciever = email
				msg = MIMEMultipart('alternative')
				# msg['Subject'] = "Want to save Rs. [Tax Leakage] in taxes? Open your Tax Savings Account today."
				msg['Subject'] = "Your Tax Leakage Report is ready."
				# msg['From'] = sender
				msg['From'] =formataddr((str(Header('Team | Salat Tax', 'utf-8')), sender))
				msg['To'] = reciever

				html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">';
				html += '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />';
				html += '<title>Salat Investments</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><style type="text/css">';
				html += 'html, body {margin:0 !important;padding:0 !important;height:100% !important;width:100% !important;font-family:"Verdana", sans-serif;}* {  -ms-text-size-adjust: 100%;  -webkit-text-size-adjust: 100%;}div[style*="margin: 16px 0"] {margin: 0 !important;}table, td {mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;}';
				html += 'table {border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;}table table table {table-layout: auto;}img {-ms-interpolation-mode: bicubic;width: 100%;height: auto !important;}.yshortcuts a {border-bottom: none !important;}.mobile-link--footer a, a[x-apple-data-detectors] {color: inherit !important;text-decoration: underline !important;}';
				html += '@media (max-width: 700px) {.container {width: 100% !important;}.container-outer {padding: 0 !important;}.logo {float: none;text-align: center;}.header-title {text-align: center !important;font-size: 20px !important;}.header-divider {padding-bottom: 0px !important;text-align: center !important;}';
				html += '.article-thumb, .article-title, .article-content, .article-button {text-align: center !important;padding-left: 15px !important;}.article-thumb {padding: 30px 0 15px 0 !important;}.article-title {padding: 0 0 15px 0 !important;}.article-content {padding: 0 15px 0 15px !important;}.article-button {padding: 20px 0 0 0 !important;}.article-button > table {float: none;}';
				html += '.footer-copy {text-align: center !important;}}</style></head>';
				html += '<body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0">';

				html += '<table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">';
				html += '<tbody>';
				html += '<tr>';
				html += '<td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top">';
				html += '<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700">';
				html += '<tbody>';
				html += '<tr>';
				html += '<td style="border-top: 10px solid #00A0E3;">';
				html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640">';
				html += '<tbody><tr>';
				html += '<td style="padding:5px 0;">';

				html += '<table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">';
				html += '<tbody>';
				html += '<tr>';
				html += '<td><br />';
				html += '<img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td>';
				html += '</tr>';
				html += '</tbody>';
				html += '</table>';
				html += '</td>';
				html += '</tr>';
				html += '</tbody>';
				html += '</table>';
				html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; border-top:thin solid #009ee3" width="640">';
				html += '<tbody>';
				html += '<tr>';
				html += '<td class="header-divider" style="padding-bottom:5px; text-align:center;">';
				html += '<p style="text-align: left;"><span style="font-size:14px;">Hi</span></p>';
				html += '<p style="text-align: left;"><span style="font-size:14px;">Thank you for trying out Salat Tax Leakage Utility. </span></p>';
				html += '<p style="text-align: left;"><span style="font-size:14px;">Below is the snapshot of your Tax Leakage. </span></p>';
				html += '<p style="text-align: left;"><span style="font-size:13px;"><img alt="Tax Leakage Report!" src="https://salattax.com/static/images_tax_report/'+pan+'.png" style="text-align: center;" /></span></p>';
				html += '<p style="text-align: left;"><span style="font-size: 14px;">So with right tax planning you could have saved Rs. '+str(money_format(total_tax_leakage))+' over last '+str(count_salattax)+' years.</span></p>';
				html += '<p style="text-align: left;"><span style="font-size:14px;">Would you like to </span></p>';
				html += '<ul>';
				html += '<li style="text-align: left;"><span style="font-size:14px;">Get a detailed report on how you could have saved the above tax leakage</span></li>';
				html += '<li style="text-align: left;"><span style="font-size:14px;">Plan your taxes for the current financial year to minimise your tax leakage</span></li>';
				html += '</ul>';

				html += '<p style="text-align: left;"><br />';
				html += '<span style="font-size:14px;">Open a Salat Tax Savings Account now and get above and more.</span></p>';
				html += '<p style="text-align:center;"><a href="https://salattax.com/tax_saving_account?utm_source=taxleakageuser&utm_medium=email&utm_campaign=s4#tax-plan-account"><img alt="Open my Tax Savings Account" src="https://bucket.mlcdn.com/a/440/440943/images/8c89093afd7ab0ea1fa3322e5877699acbbcefcb.png" style="width: 274px; height: 65px;" /></a><br />';
				html += '</p>';
				html += '<p style="text-align:justify;"></p>';
				html += '</td>';
				html += '</tr>';
				html += '</tbody>';
				html += '</table>';
				html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">';
				html += '<tbody>';
				html += '<tr>';
				html += '<td style="padding: 0px 0 20px 0;">';
				html += '<table class="container" style="width: 640px;border-top:thin solid #009ee3">';
				html += '<tbody>';
				html += '<tr>';
				html += '<td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding the product or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>';
				html += '</tr>';
				html += '</tbody>';
				html += '</table>';
				html += '</td>';
				html += '</tr>';
				html += '</tbody>';
				html += '</table>';
				html += '</td>';
				html += '</tr>';
				html += '</tbody>';
				html += '</table>';
				html += '</td>';
				html += '</tr>';
				html += '</tbody>';
				
				html += '</table>';
				html += '</html>';

				part2 = MIMEText(html, 'html')

				msg.attach(part2)
				# Send the message via local SMTP server.
				mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
				mail.ehlo()
				mail.starttls()
				mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
				mail.sendmail(sender, reciever, msg.as_string())
				log.info('tax_leakage_report Mail sent to ID and PAN: '+str(client_id)+' , '+pan+' '+reciever)
				return 1
			else:
				log.info('Not Enough Data for client ID and PAN: '+str(client_id)+' , '+pan)
				return 0
		else:
			log.info('Error in screenshot for Client ID : '+str(client_id)+ ' '+str(get_report) )
			return 0
	else:
		log.info('No Email Found for Client ID : '+client_id)
		return 0

def sales_and_tax_leakage_report(request,client_id,email):
	# email = ''
	pan = ''
	p_age = 'NA'
	invested_share = 'no'
	shares_year = 1

	# for c in SalatTaxUser.objects.filter(id=client_id):
		# client_id = c.id
		# email = c.email

	# log.info(email)
	if Personal_Details.objects.filter(P_Id=client_id).exists():
		for client in Personal_Details.objects.filter(P_Id=client_id):
			pan = client.pan
			p_age = client.dependent_parent_age
	if Investment_Details.objects.filter(P_Id=client_id).exists():
		for i_d in Investment_Details.objects.filter(P_Id = client_id):
			shares_year = i_d.year_of_first_share_sold or 1
			invested_share = 'yes'
	# log.info(pan)

	if(email!='' and pan!=''):
		s_tax = 0
		count_salattax = 0
		total_tax_leakage = 0
		total_tax_paid = 0
		total_salat_tax = 0
		for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
			if(itr.year!='2013'):
				s_tax = cal_salat_tax(pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
				total_tax_leakage = total_tax_leakage + (s_tax['tax_leakage'] *-1)
				total_tax_paid = total_tax_paid + s_tax['tax_paid']
				total_salat_tax = total_salat_tax + s_tax['ToTax']
				count_salattax = count_salattax + 1
		# log.info( money_format(total_tax_leakage) )

		if count_salattax >=2:
			# get_user_email = email
			sender = 'salattax@gmail.com'
			reciever = email
			msg = MIMEMultipart('alternative')
			msg['Subject'] = "Want to save Rs. "+str(money_format(total_tax_leakage))+" in taxes? Open your Tax Savings Account today."
			# msg['Subject'] = "Your Tax Leakage Report is ready."
			# msg['From'] = sender
			msg['From'] =formataddr((str(Header('Team | Salat Tax', 'utf-8')), sender))
			msg['To'] = reciever

			html ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">'
			html += '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />'
			html += '<title>Salat Investments</title><link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><style type="text/css">'
			html += 'html, body {margin: 0 !important;padding: 0 !important;height: 100% !important;width: 100% !important;font-family: "Verdana", sans-serif;}* {-ms-text-size-adjust: 100%;-webkit-text-size-adjust:100%;}'
			html += 'div[style*="margin: 16px 0"] {margin: 0 !important;}table, td {mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;}table {border-spacing: 0 !important;border-collapse: collapse !important;table-layout: fixed !important;margin: 0 auto !important;}'
			html += 'table table table {table-layout: auto;}img {-ms-interpolation-mode: bicubic;width: 100%;height: auto !important;}.yshortcuts a {border-bottom: none !important;}.mobile-link--footer a, a[x-apple-data-detectors] {color: inherit !important;text-decoration: underline !important;}'
			html += '@media (max-width: 700px) {.container {width: 100% !important;}.container-outer {padding: 0 !important;}.logo {float: none;text-align: center;}.header-title {text-align: center !important;font-size: 20px !important;}'
			html += '.header-divider {padding-bottom: 0px !important;text-align: center !important;}.article-thumb, .article-title, .article-content, .article-button {text-align: center !important;padding-left: 15px !important;}.article-thumb {padding: 30px 0 15px 0 !important;}'
			html += '.article-title {padding: 0 0 15px 0 !important;}.article-content {padding: 0 15px 0 15px !important;}.article-button {padding: 20px 0 0 0 !important;}.article-button > table {float: none;}.footer-copy {text-align: center !important;}}'
			html += '</style></head>'

			html += '<body bgcolor="#f5f5f5" leftmargin="0" marginheight="0" marginwidth="0" style="margin: 0; padding: 0;" topmargin="0">';
			html += '<table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" class="container-outer" style="padding: 30px 0 30px 0;" valign="top">';
			html += '<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px;" width="700"><tbody><tr><td style="border-top: 10px solid #00A0E3;">';
			html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; " width="640"><tbody><tr><td style="padding:5px 0;"><table align="left" border="0" cellpadding="0" cellspacing="0" class="logo" style="width: 120px; " width="120">';
			html += '<tbody><tr><td><br /><img alt="Salat Tax" src="https://bucket.mlcdn.com/a/440/440943/images/58fb45a18b344f86c31453b5507b3f3a82a00012.png" style="border-width: 0px; border-style: solid; width: 180px; height: 67px;" /></td></tr></tbody>';
			html += '</table></td></tr></tbody></table>';
			html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 640px; border-top:thin solid #009ee3" width="640">';
			html += '<tbody>';
			html += '<tr>';
			html += '<td class="header-divider" style="padding-bottom:5px; text-align:center;">';
			html += '<p style="text-align: left;"><span style="font-size:14px;">Hi</span></p>';
			html += '<p style="text-align: left;"><span style="font-size:14px;">You checked your Tax Leakage on salattax.com</span></p>';
			
			# html += '<p style="text-align:left"><span style="font-size:14px">Over the last '+str(count_salattax)+' years you have paid Rs. <strong>'+str(money_format(total_tax_paid) )+'</strong>&nbsp;in income taxes.</span></p>';
			# html += '<p style="text-align: left;"><span style="font-size:14px;">Had you done proper tax planning then the miniumum tax you should have paid would have been Rs.<span style="color:#0099ff;"> <strong>[Salat Tax]</strong></span></span></p>';
			# html += '<p style="text-align: left;"><span style="font-size:14px;">So you have actually paid Rs. <strong><span style="color:#FF0000;">[Tax Leakage]</span></strong> as excess tax.</span></p>';
			# html += '<p style="text-align: left;"><span style="font-size:14px;">Imagine what you can do with Rs. [Tax Leakage]?</span></p>';
			
			html += '<p style="text-align:left"><span style="font-size:14px">Over the last '+str(count_salattax)+' years you have paid Rs. <strong>'+str(money_format(total_tax_paid) )+'</strong>&nbsp;in income taxes.</span></p>';
			# html += '<p style="text-align:left"><span style="font-size:14px">Over the last [x] years you have paid Rs. <strong>[Total Tax Amount]</strong>&nbsp;in income taxes.</span></p>';
			html += '<p style="text-align:left"><span style="font-size:14px">Had you done proper tax planning then the miniumum tax you should have paid would have been Rs.<span style="color:#0099ff"> <strong>'+str(money_format(total_salat_tax) )+'</strong></span></span></p>';
			# html += '<p style="text-align:left"><span style="font-size:14px">Had you done proper tax planning then the miniumum tax you should have paid would have been Rs.<span style="color:#0099ff"> <strong>[Salat Tax]</strong></span></span></p>';
			html += '<p style="text-align:left"><span style="font-size:14px">So you have actually paid Rs. <strong><span style="color:#ff0000">'+str(money_format(total_tax_leakage) )+'</span></strong> as excess tax.</span></p>';
			# html += '<p style="text-align:left"><span style="font-size:14px">So you have actually paid Rs. <strong><span style="color:#ff0000">[Tax Leakage]</span></strong> as excess tax.</span></p>';
			html += '<p style="text-align:left"><span style="font-size:14px">Imagine what you can do with Rs. '+str(money_format(total_tax_leakage) )+'?</span></p>';
			# html += '<p style="text-align:left"><span style="font-size:14px">Imagine what you can do with Rs. [Tax Leakage]?</span></p>';
			
			html += '<p style="text-align:left"><span style="font-size:14px">By opening a <a href="https://salattax.com/tax_saving_account?utm_source=taxleakageuser&amp;utm_medium=email&amp;utm_campaign=s5#tax-plan-account" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://salattax.com/tax_saving_account?utm_source%3Dtaxleakageuser%26utm_medium%3Demail%26utm_campaign%3Ds5%23tax-plan-account&amp;source=gmail&amp;ust=1552047193411000&amp;usg=AFQjCNGsZ8L1wuLE0z8Qjb-4QDuDNpmCSw">Salat Tax Savings Account</a> you can&nbsp;</span></p>';
			html += '<ul>';
			html += '<li style="text-align: left;"><span style="font-size:14px;">Get a detailed report on how you could have saved the above tax leakage</span></li>';
			html += '<li style="text-align: left;"><span style="font-size:14px;">Plan your taxes for the current financial year to save taxes</span></li>';
			html += '<li style="text-align:left"><span style="font-size:14px">File your IT returns for FY 2018-19 for free</span>&nbsp;<span style="font-size:14px">and more</span></li>';
			html += '</ul>';
			html += '<p style="text-align: left;"><br />';
			html += '<span style="font-size:14px;">Join hundreds of smart individuals who are paying minimum taxes using Salat Tax Savings Account.</span></p>';
			html += '<p style="text-align:center;"><a href="https://salattax.com/tax_saving_account?utm_source=taxleakageuser&utm_medium=email&utm_campaign=s5#tax-plan-account"><img alt="Open my Tax Savings Account" src="https://bucket.mlcdn.com/a/440/440943/images/8c89093afd7ab0ea1fa3322e5877699acbbcefcb.png" style="width: 274px; height: 65px;" /></a><br /></p>';
			html += '';
			html += '<p style="text-align:justify;"></p>';
			html += '</td>';
			html += '</tr>';
			html += '</tbody>';
			html += '</table>';
			html += '';
			html += '<table align="center" border="0" cellpadding="0" cellspacing="0" class="container" style="width: 700px; border-bottom: 10px solid #00A0E3;" width="700">';
			html += '<tbody>';
			html += '<tr>';
			html += '<td style="padding: 0px 0 20px 0;">';
			html += '<table class="container" style="width: 640px;border-top:thin solid #009ee3">';
			html += '<tbody>';
			html += '<tr>';
			html += '<td class="footer-copy" style="font-size: 12px; color: #000; font-family: verdana; line-height:20px!important; padding-top:10px">If you have any queries regarding the product or this email just reply to this email. If you do not want to receive emails which can help you save taxes then you can <a class="email"> <unsubscribe style="color:#00A0E3; cursor:pointer; text-decoration:none;font-size:13px;"> unsubscribe here</unsubscribe> </a></td>';
			html += '</tr>';
			html += '</tbody>';
			html += '</table>';
			html += '</td>';
			html += '</tr>';
			html += '</tbody>';
			html += '</table>';
			html += '</td>';
			html += '</tr>';
			html += '</tbody>';
			html += '</table>';
			html += '</td>';
			html += '</tr>';
			html += '</tbody>';
			html += '</table>';
			html += '</body>';
			html += '</html>';

			part2 = MIMEText(html, 'html')

			msg.attach(part2)
			# Send the message via local SMTP server.
			mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com')
			mail.ehlo()
			mail.starttls()
			mail.login('AKIAJKDFKN6LBVA7IBLA', 'Arj4SHLAPCQ83tbYD+/Wzc46DTUcoFYmx8sDgEtxm8cb')
			mail.sendmail(sender, reciever, msg.as_string())
			log.info('sales_and_tax_leakage_report Mail sent to ID and PAN: '+str(client_id)+' , '+pan+' '+reciever)
			return 1
		else:
			log.info('Not Enough Data for client ID and PAN: '+str(client_id)+' , '+pan)
			return 0
	else:
		log.info('No Data for client ID :'+str(client_id))
		return 0


@csrf_exempt
def selenium_tax_report(request,data):
	# log.info(data)
	return render(request,'selenium_tax_report.html',{
    	'client_id':data,
    	})
@csrf_exempt
def your_tax_report(request,data):
	# log.info(data)
	return render(request,'your_tax_report.html',{
    	'client_id':data,
    	})

def money_format(val):
	locale.setlocale(locale.LC_MONETARY, 'en_IN')
	result = locale.currency(val, grouping=True)
	result = result.split(' ')[1]
	result = result.split('.')[0]
	return result

def get_screenshot(client_id,pan):
	try:
		file_name = selenium_path+'/images_tax_report/'+str(pan)+'.png'
		file = open(file_name,'r')
		log.info(pan + ' screenshot exist')
	except Exception as ex:
		# log.info('Error get_screenshot : %s' % ex)
		log.info(pan + ' screenshot does not exist')

		# /html/body/form/div/div/div[2]/div/div/div[3]/button[4]
		driver_t = driver_call()
		driver_t.set_window_size(1500, 1000)
		driver_t.maximize_window()
		try:
			driver_t.get("https://salattax.com/selenium_tax_report/"+str(client_id))
		except Exception as ex:
			return 'Error get_screenshot : %s' % ex

		time.sleep(1)
		try:
			dot_click = driver_t.find_elements_by_xpath('/html/body/form/div/div/div[2]/div/div/div[3]/button[4]')[0]
			dot_click.click()
		except Exception as ex:
			return 'Error get_screenshot : %s' % ex

		time.sleep(4)
		try:
			img_path1 = selenium_path+'/images_tax_report/'+str(pan)
			id = driver_t.find_element_by_id("owl_div")
			location = id.location
			size = id.size
			img = driver_t.get_screenshot_as_png()
			img = Image.open(StringIO(img))
			left = location['x']
			top = location['y']
			right = location['x'] + size['width']
			bottom = location['y'] + size['height']
			img = img.crop((int(left), int(top), int(right), int(bottom)))
			img.save(img_path1+'.png')
		except Exception as ex:
			return 'Error get_screenshot : %s' % ex

		driver_t.close()
	return 1

def get_all_screenshot():
	detail = []
	if Personal_Details.objects.exclude(pan='').exists():
		count = 0
		for client in Personal_Details.objects.exclude(pan=''):
			salat_client = 0
			went_on_last_slide = 0
			if salat_registration.objects.filter(pan=client.pan).exists():
				salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
				if salat_registration_instance.is_salat_client == 'Salat Client':
					salat_client = 1
			if user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide').exists():
				went_on_last_slide = 1

			# log.info(str(client.P_Id) +' - '+ str(salat_client) +' - '+ str(went_on_last_slide) )
			if salat_client ==1 and went_on_last_slide ==1 :
				pan = client.pan
				p_age = client.dependent_parent_age
				invested_share = 'no'
				shares_year = 1

				# if Personal_Details.objects.filter(P_Id=client.P_Id).exists():
				# 	for client in Personal_Details.objects.filter(P_Id=client.P_Id):
				# 		pan = client.pan
				# 		p_age = client.dependent_parent_age
				if Investment_Details.objects.filter(P_Id=client.P_Id).exists():
					for i_d in Investment_Details.objects.filter(P_Id = client.P_Id):
						shares_year = i_d.year_of_first_share_sold
						invested_share = 'yes'

				s_tax = 0
				count_salattax = 0
				for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
					if(itr.year!='2013'):
						s_tax = cal_salat_tax(pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
						count_salattax = count_salattax + 1

				if count_salattax >=2:
					detail.append(str(client.P_Id) +' - '+ pan)
					get_screenshot(client.P_Id, pan)

	log.info(detail)
				
	return 1

@csrf_exempt
def schedule_mail(request,data):
	result = {}
	# log.info(data)
	log.info( "%s: %s" % ( 'Start send_marketing_mail for '+data, time.ctime(time.time()) ) )
	option = data
	client_id = []
	# email = []
	# detail = []
	send_mail = 0
	trigger_mail = 0
	# option - tax_leakage_report - Tax Leakage Report 1
	# option - tax_leakage_report2 - Tax Leakage Report 2
	# option - sales_and_tax_leakage_report - Sales 3 Tax Leakage Report
	# option - sales1 - Sales 1
	# option - sales2 - Sales 2
	# option - sales4 - Sales 4

	if Personal_Details.objects.exclude(pan='').exists():
		for client in Personal_Details.objects.exclude(pan=''):
			client_id.append(str(client.P_Id) +' - '+ (client.name or '') )

			salat_client = 0
			went_on_last_slide = 0
			subscribed = 0
			if salat_registration.objects.filter(pan=client.pan).exists():
				salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
				if salat_registration_instance.is_salat_client == 'Salat Client':
					salat_client = 1
			if user_tracking.objects.filter(client_id=client.P_Id,event_name='Seen Last Slide').exists():
				went_on_last_slide = 1
			if user_tracking.objects.filter(client_id=client.P_Id,event_name='success',event='payment').exists():
				subscribed = 1

			if option == 'tax_leakage_report':
				mail_content='Tax Leakage Report 1'
				if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
					trigger_mail = 1
			if option == 'sales_and_tax_leakage_report':
				mail_content='Sales 3 Tax Leakage Report'
				trigger_mail_cond1 = 0
				trigger_mail_cond2 = 0
				if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
					trigger_mail_cond1 = 1
				if Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content='Tax Leakage Report 1').exists():
					Mail_Trigger_i = Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content='Tax Leakage Report 1').order_by('-created_time')[0]
					time_diff = timezone.now() - Mail_Trigger_i.created_time
					minute = int(time_diff.seconds / 60)
					hour = minute / 60
					if( hour >= 23):
						trigger_mail_cond2 = 1
				if (trigger_mail_cond1 ==1 and trigger_mail_cond2 == 1):
					trigger_mail = 1
			if option == 'sales1':
				if salat_client == 0:
					# log.info(str(salat_client) + ' '+ str(client.P_Id))
					if SalatTaxUser.objects.filter(id = client.P_Id).exists():
						for stu in SalatTaxUser.objects.filter(id=client.P_Id):
							SalatTaxUser_i = SalatTaxUser.objects.filter(id=client.P_Id).order_by('-created')[0]
							time_diff = timezone.now() - SalatTaxUser_i.created
							minute = int(time_diff.seconds / 60)
							hour = minute / 60
							log.info(stu.email + ' '+ str(client.P_Id)+' '+str(hour)+' '+str(minute) )
							if( hour >= 23):
								log.info(stu.email + ' '+ str(client.P_Id)+' '+str(hour) )

			# log.info(str(client.P_Id) + ' : '+str(salat_client) + ' : '+str(went_on_last_slide)+ ' : '+str(subscribed)+ ' : '+str(trigger_mail))
			if salat_client ==1 and went_on_last_slide ==1 and subscribed == 0 and trigger_mail == 1:
				if SalatTaxUser.objects.filter(id = client.P_Id).exists():
					for stu in SalatTaxUser.objects.filter(id=client.P_Id):
						# detail.append(str(stu.id) +' - '+ stu.user.username+' - '+ stu.email)
						try:
							if option == 'tax_leakage_report':
								# log.info('*** '+str(client.P_Id) + ' mail_content : '+str(mail_content))
								if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
									sent_mail = tax_leakage_report(request, client.P_Id,stu.email)
									Mail_Trigger.objects.create(
										P_Id = client.P_Id,
										mail_content = mail_content,
										mail_id = stu.email,
										sent = sent_mail,
									).save()
							if option == 'sales_and_tax_leakage_report':
								if not Mail_Trigger.objects.filter(P_Id = client.P_Id,mail_content=mail_content).exists():
									sent_mail = sales_and_tax_leakage_report(request, client.P_Id,stu.email)
									Mail_Trigger.objects.create(
										P_Id = client.P_Id,
										mail_content = mail_content,
										mail_id = stu.email,
										sent = sent_mail,
									).save()
							send_mail = 1
						except Exception as e:
							send_mail = 2
							# log.info( 'Error sending mail: %s' % e)
							log.error('Error in sending mail: '+traceback.format_exc())
					
	result['status']= 'success'
	result['option']= option
	result['client_id']= client_id
	# result['email']= email
	result['send_mail'] = send_mail
	# result['detail'] = detail
	return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def random_query(request):
	if request.method=='POST':
		result={}
		try:
			option = request.POST.get('option')
			detail = []

			for ut in user_tracking.objects.exclude(event_name=''):
				if( "utm_medium=email" in ut.event_name):
					# https://salattax.com/tax_saving_account?utm_source=taxleakageuser&utm_medium=email&utm_campaign=s4#tax-plan-account
					clientID = ut.client_id
					for ut1 in user_tracking.objects.filter(client_id=ut.client_id):
						if(ut1.client_id != '1' and ut1.client_id != 1):
							clientID = ut1.client_id

					detail.append(ut.session_key +' - '+ str(clientID))

			result['detail'] = detail
		except Exception as ex:
			result['status']= 'Error send_marketing_mail: %s' % ex
		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def send_sms_tax_leakage(request,data):
	try:
		result = {}
		# a = 5
		# log.info( '%04d' % int(data) )
		# output: 05

		mobile_list = []
		id_list = []
		url_list = []
		sms_status = ''

		alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m']
		# l = list(new_id)
		# for x in range(len(l)):
		# 	log.info(alpha[ int(l[x]) ] )
		# sms_status = send_sms('8828210714','https://salattax.com/send_sms_tax_leakage/19','19')
		log.info('send_sms_tax_leakage')

		if Personal_Details.objects.exclude(mobile='').exists():
			send_sms_flag = 0
			for client in Personal_Details.objects.exclude(mobile=''):
				if salat_registration.objects.filter(pan=client.pan).exists():
					salat_registration_instance = salat_registration.objects.filter(pan=client.pan).order_by('-date_time')[0]
					if salat_registration_instance.is_salat_client == 'Salat Client':
						if user_tracking.objects.filter(client_id=client.P_Id).exists():
							for ut in user_tracking.objects.filter(client_id=client.P_Id):
								if "/tax_report/" in ut.event_name and ut.event == 'viewed':
									send_sms_flag = 1
							if send_sms_flag == 1 and client.mobile is not None:
								mobile_list.append(client.mobile)
								l = list('%04d' % int(client.P_Id))
								query_str = ''
								for x in range(len(l)):
									query_str = query_str + alpha[ int(l[x]) ]
								id_list.append( query_str )
								# if client.P_Id != 36 and client.P_Id != 39 and client.P_Id != 58 and client.P_Id != 64 and client.P_Id != 78:
								sms_status = send_sms(client.mobile,'https://salattax.com/your_tax_report/'+query_str,client.P_Id)
								url_list.append( str(client.P_Id)+' https://salattax.com/your_tax_report/'+query_str +' ' +sms_status)

		result['mobile_list'] = mobile_list
		result['id_list'] = id_list
		result['sms_status'] = sms_status
		result['url_list'] = url_list
		# result['tu'] = tu
	except Exception as ex:
		log.info('Error send_sms_tax_leakage:' +traceback.format_exc())
		result['status']= 'Error send_sms_tax_leakage: %s' % ex

	return HttpResponse(json.dumps(result), content_type='application/json')

def send_sms(number,url,client_id):
	Mail_Trigger.objects.create(
		P_Id = client_id,
		mail_content = 'Thank you for checking your Tax Leakage '+url,
		mail_id = number,
	).save()
	log.info('Sending SMS to '+number)
	log.info(url)
	pan = ''
	p_age = 'NA'
	invested_share = 'no'
	shares_year = 1
	if Personal_Details.objects.filter(P_Id=client_id).exists():
		for client in Personal_Details.objects.filter(P_Id=client_id):
			pan = client.pan
			p_age = client.dependent_parent_age or ''
	if Investment_Details.objects.filter(P_Id=client_id).exists():
		for i_d in Investment_Details.objects.filter(P_Id = client_id):
			shares_year = i_d.year_of_first_share_sold or 1
			invested_share = 'yes'

	s_tax = 0
	count_salattax = 0
	total_tax_leakage = 0
	for itr in temp_ITR_value.objects.filter(PAN = pan).order_by('-year').reverse():
		if(itr.year!='2013'):
			s_tax = cal_salat_tax(pan,itr.year,itr.ITR, p_age, invested_share, shares_year)
			total_tax_leakage = total_tax_leakage + (s_tax['tax_leakage'] *-1)
			count_salattax = count_salattax + 1
	# $sms_from = 'SALAT INVST';
	# $sms_userid = 'swapnil.bagmar';
	# $sms_password= 'salattech@1988';
	# $sms_sid='iSALAT';
	# https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=yourapicode&senderid=TESTIN&channel=2&DCS=0&flashsms=0&number=91989xxxxxxx&text=test message&route=clickhere
	# tu = tinyurl.create_one(url)
	if not Mail_Trigger.objects.filter(P_Id=client_id,mail_id = number).exists():
		text_msg = 'Thank you for checking your Tax Leakage on salattax.com. Your tax leakage for last '+str(count_salattax)+' years is Rs. '+str(money_format(total_tax_leakage))+'. Click for more details '+url
		resp = requests.get("https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=028778d4-aa73-4a35-8ebe-8b7b825025da&senderid=iSALAT&channel=2&DCS=0&flashsms=0&number="+number+"&text="+text_msg)
		log.info(resp.json()['ErrorMessage'])
		if resp.json()['ErrorMessage'] == 'Success':
			for mt in Mail_Trigger.objects.filter(P_Id=client_id,mail_id = number):
				mt.sent = '1'
				mt.save()

	# return resp.json()
	return str(count_salattax) +'-'+ str(money_format(total_tax_leakage))

#	card no - 4012001037141112
#	Expire - 05/20
#	CVV - 123
#	Name = Test
