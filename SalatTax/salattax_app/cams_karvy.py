from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView # Import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm 
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
# from .models import other_details,personal_info,income_details
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

import requests
# Python logging package
import logging
import json

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
import os
import time
from PIL import Image
from io import BytesIO
from StringIO import StringIO
from subprocess import Popen, PIPE, STDOUT
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import imaplib
import email
from .path import *

stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

def driver_call():
	chrome_options = webdriver.ChromeOptions()
	downloadPath = '/home/salatTax/SalatTax/static/zip_26AS'
	chrome_options.add_argument('--headless')
	prefs = {'download.default_directory' : downloadPath,
	"download.prompt_for_download": False,
	"download.directory_upgrade": True,
	"safebrowsing.enabled": True}
	chrome_options.add_experimental_option('prefs', prefs)
	driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver', chrome_options=chrome_options,
	  service_args=['--verbose', '--log-path=/tmp/chromedriver1.log'])

	driver.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
	params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': downloadPath}}
	command_result = driver.execute("send_command", params)

	return driver

@csrf_exempt
def trigger_mail_karvy(request):
	if request.method=='POST':
		result={}
		try:
			mail=request.POST.get('mail')
			pan=request.POST.get('pan')
			result['mail']= mail
			result['pan']= pan
			driver = driver_call()
			# driver.close()
			driver.get("https://www.karvymfs.com/platformservice/")

			trigger = login_karvy(driver)

			result['status']="success"
		except Exception as ex:
			result['status']= 'Error : %s' % ex
			log.error('Error : %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_mail_attachment(request):
	if request.method=='POST':
		result={}
		try:
			unreadmail = readEmail()
			result['status']="success"
			result['unreadmail']= unreadmail
		except Exception as ex:
			result['status']= 'Error : %s' % ex
			log.error('Error : %s' % ex)

		return HttpResponse(json.dumps(result), content_type='application/json')
	else:
		result['status']="Error"
		return HttpResponse(json.dumps(result), content_type='application/json')

def captcha_text_read(captch_img_path):
	p = Popen(['java', '-jar','captcha_arg.jar',captch_img_path], stdout=PIPE, stderr=STDOUT)
	return_captcha = ''
	for line in p.stdout:
		return_captcha = line
	return return_captcha

def captch_ref_karvy(driver,captch_img_path):
	try:
		captcha_id = driver.find_element_by_id("imgCaptcha")
		location = captcha_id.location
		size = captcha_id.size
		img = driver.get_screenshot_as_png()
		img = Image.open(StringIO(img))
		left = location['x']
		top = location['y']
		right = location['x'] + size['width']
		bottom = location['y'] + size['height']
		img = img.crop((int(left)-2, int(top)-2, int(right)-2, int(bottom)-2))
		img.save(captch_img_path)
		time.sleep(1)
		screenshot(driver,'captcha')

		captcha_text_1 = captcha_text_read(captch_img_path)
		driver.find_element_by_id("txtCaptcha").send_keys('captcha_text_1')
		try:
			driver.find_element_by_class_name('red')
			error = 1
			return error
		except Exception:
			error = 0
			return error
	except Exception as ex:
		error = 0
		return ex

def screenshot(driver,f_name):
	captch_img_path1 = selenium_path+'/camsKarvy/'
	id = driver.find_element_by_tag_name("body")
	location = id.location
	size = id.size
	img = driver.get_screenshot_as_png()
	img = Image.open(StringIO(img))
	left = location['x']
	top = location['y']
	right = location['x'] + size['width']
	bottom = location['y'] + size['height']
	img = img.crop((int(left), int(top), int(right), int(bottom)))
	img.save(captch_img_path1+f_name+'.png')

def login_karvy(driver):
	log.info('start login_karvy')
	captch_img_path = '/home/salatTax/SalatTax/static/camsKarvy/captcha.png'
	screenshot(driver,'screenshot1')

	detailed = driver.find_elements_by_xpath('//*[@id="rdDetailed"]')[0]
	driver.execute_script("arguments[0].click();", detailed)
	specificPeriod = driver.find_elements_by_xpath('//*[@id="radAnyDate"]')[0]
	driver.execute_script("arguments[0].click();", specificPeriod)
	screenshot(driver,'screenshot2')

	driver.find_element_by_id('txtFromDt').clear()
	from_date = driver.find_element_by_id("txtFromDt")
	from_date.send_keys("01/01/1990")
	screenshot(driver,'screenshot3')

	email = driver.find_element_by_id("txtEmail")
	email.send_keys("jagrutikadam15@gmail.com")
	password = driver.find_element_by_id("txtPassword")
	password.send_keys("EGRPK2447C")
	reenterpassword = driver.find_element_by_id("txtRePassword")
	reenterpassword.send_keys("EGRPK2447C")
	screenshot(driver,'screenshot4')

	# error = 1
	# while error == 1:
	# 	error = captch_ref_karvy(driver,captch_img_path)
		# print 'ERROR : '+ str(error)
	
	driver.close()

def downloaAttachmentsKarvy(mail,outputdir):
    if mail.get_content_maintype() != 'multipart':
        return
    for part in mail.walk():
        if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
            open(outputdir + '/' + part.get_filename(), 'wb').write(part.get_payload(decode=True))

def readEmail():
	# telnet imap.gmail.com 993
	outputdir = '/home/salatTax/SalatTax/static/camsKarvy'
	msrvr = imaplib.IMAP4_SSL('imap.gmail.com',993)
	unm = 'jagrutikadam15'
	pwd = 'yjwqbxxlqanznvyu'
	msrvr.login(unm,pwd)
	# disable 2-step Verification (Only works on localhost)
	# allow less secure apps : ON
	# On server : 1. enable 2-step Verification. 2. Generate App Password 
	# 3. Use app Password instead of gmail password
	subject = ''

	stat,cnt = msrvr.select('Inbox')
	# resp, items = msrvr.search(None, "(ALL)")
	resp, items = msrvr.search(None, "UNSEEN")
	# messages = msrvr.fetch(items, '(BODY.PEEK[HEADER])')
	items = items[0].split()
	for emailid in items:
		# messages = msrvr.fetch(emailid, '(BODY.PEEK[HEADER.FIELDS (SUBJECT)])')
		# msg = msrvr.fetch(emailid, '(RFC822)') # for all mail content
		# if "Consolidated Account Statement - KARVY Mailback Request" in messages[1][0][1]: 
			# print messages[1][0][1]
			# downloaAttachmentsInEmail(msrvr, emailid, outputdir)
		resp, data = msrvr.fetch(emailid, "(BODY.PEEK[])")
		email_body = data[0][1]
		mail = email.message_from_string(email_body)
		subject += mail['subject']+', '
		if "Consolidated Account Statement - KARVY Mailback Request" in mail['subject']: 
			downloaAttachmentsKarvy(mail,outputdir)

	return subject





