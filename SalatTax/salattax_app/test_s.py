#!/usr/bin/env bash
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
import os
import time
from PIL import Image
from io import BytesIO
from StringIO import StringIO
import subprocess
from subprocess import Popen, PIPE, STDOUT
from selenium.webdriver import ChromeOptions, Chrome
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from django.http import HttpResponse

# client_registration.html
# chromedriver = "/usr/local/bin/chromedriver"
# os.environ["webdriver.chrome.driver"] = chromedriver
# driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")


def login(driver):
	captch_img_path = '/home/salat/Downloads/images_captcha/screenshot.png'
	# driver = webdriver.Chrome(chromedriver)
	driver.get("https://www.incometaxindiaefiling.gov.in/home")

	# close popup
	driver.find_element_by_class_name('ui-icon-closethick').click()
	# Click login Here
	# login_button = driver.find_element_by_xpath("//input[@value='Login Here']")
	login_here = driver.find_elements_by_xpath('//*[@id="staticContentsUrl"]/section/div/app-register-options/ul/app-register[2]/li/h1/input')[0]
	driver.execute_script("arguments[0].click();", login_here)

	# Enter Username And Password
	username = driver.find_element_by_id("Login_userName")
	password = driver.find_element_by_id("Login_password")

	username.send_keys("ERIA101868")
	password.send_keys("Derivatives4$")

	# captcha screenshot

	captcha_id = driver.find_element_by_id("captchaImg")
	location = captcha_id.location
	size = captcha_id.size
	img = driver.get_screenshot_as_png()
	img = Image.open(StringIO(img))
	left = location['x']
	top = location['y']
	right = location['x'] + size['width']
	bottom = location['y'] + size['height']
	img = img.crop((int(left)-1, int(top)-1, int(right)-1, int(bottom)-1))
	img.save('/home/salat/Downloads/images_captcha/screenshot.png')
	time.sleep(1)

	# captcha check and Login button
	error = 0
	already_login = 0

	captcha_code = driver.find_elements_by_xpath('//*[@id="button1"]')[0]
	# captcha_text = driver.find_element_by_id("Login_captchaCode")
	captcha_text_1 = captcha_text_read(captch_img_path)
	driver.find_element_by_id("Login_captchaCode").send_keys(captcha_text_1)
	# driver.execute_script("arguments[0].click();", captcha_code)

	try:
		driver.find_element_by_class_name('error')
		error = 1
	except Exception:
		error = 0

	print 'WHILE LOOP ...'
	while error == 1:
		error = captch_ref(driver,captch_img_path)
		print 'ERROR : '
		print error

	if error == 1:
		driver.find_element_by_id("Login_captchaCode").clear()
		captch_ref(driver,captch_img_path)
		# driver.find_element_by_class_name('refreshImg').click()
		# driver.find_element_by_id("Login_captchaCode").send_keys("abcdef")
	else:
		error = 0
		# driver.find_element_by_id("Login_captchaCode").send_keys("abcdef")

	try:
		time.sleep(1)
		continue_login = driver.find_elements_by_xpath('//*[@id="ForcedLogin"]/table[2]/tbody/tr/td[1]/input')[0]
		driver.execute_script("arguments[0].click();", continue_login)
		already_login = 1
	except Exception:
		already_login = 0
	try:
		time.sleep(1)
		continue_login1 = driver.find_elements_by_xpath('//*[@id="formContainerDialog1"]/table/tbody/tr[2]/td/div/input')[0]
		driver.execute_script("arguments[0].click();", continue_login1)
		already_login = 1
	except Exception:
		already_login = 0

	try:
		time.sleep(1)
		skip = driver.find_elements_by_xpath('//*[@id="UpdateContactDtls_2"]')[0]
		driver.execute_script("arguments[0].click();", skip)
	except Exception:
		already_login = 0

	time.sleep(5)
	# print(error)


# add_client()
def test(request):
	# chromedriver = "/usr/local/bin/chromedriver"
	# os.environ["webdriver.chrome.driver"] = chromedriver
	# chrome_options = Options()  
	# chrome_options.add_argument("--headless")  
	# chrome_options.binary_location = '/Applications/Google Chrome   Canary.app/Contents/MacOS/Google Chrome Canary'
	# driver = webdriver.Chrome(executable_path="/usr/lib/chromium-browser/chromedriver",   chrome_options=chrome_options)
	# driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--headless')
	chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors. 
	driver = webdriver.Chrome(executable_path='/usr/lib/chromium-browser/chromedriver', chrome_options=chrome_options)
	# Option 2 - with pyvirtualdisplay
	# from pyvirtualdisplay import Display 
	# display = Display(visible=0, size=(1024, 768)) 
	# display.start() 
	# driver = webdriver.Chrome(driver_path='/home/dev/chromedriver', 
	#   service_args=['--verbose', '--log-path=/tmp/chromedriver.log'])

	# Log path added via service_args to see errors if something goes wrong (always a good idea - many of the errors I encountered were described in the logs)
	# And now you can add your website / app testing functionality: 
	driver.get("https://www.incometaxindiaefiling.gov.in/home")
	# print(driver.title)
	return HttpResponse(driver.title)

# def save_cookie(driver, path):
#     with open(path, 'wb') as filehandler:
#         pickle.dump(driver.get_cookies(), filehandler)

# def load_cookie(driver, path):
#      with open(path, 'rb') as cookiesfile:
#          cookies = pickle.load(cookiesfile)
#          for cookie in cookies:
#              driver.add_cookie(cookie)

# test(driver)
# create_driver_session('7d0bb9637122dd2e1b3e6939f2ebc867', 'http://127.0.0.1:47749')
# check_register = register(driver)
# print check_register
# add_client(driver)
# captcha_text_read()
# login(driver)


# set TESSDATA_PREFIX environment variable
# put tessdata folder in root path

# https://stackoverflow.com/questions/22476112/using-chromedriver-with-selenium-python-ubuntu
# subprocess.call(['java', '-jar', '/media/salat/DATA D/Ubuntu_related_file/captcha_arg.jar','/home/salat/Downloads/images_captcha/screenshot.png'])
# p = Popen(['java', '-jar','https://salattax.com/captcha/captcha_arg.jar','/home/salat/Downloads/images_captcha/screenshot.png'], stdout=PIPE, stderr=STDOUT)
# for line in p.stdout:
# 	print(line)
	# output = os.getcwd()
	# print("Path output : "+output)
# subprocess.call(['java', '-jar', 'https://salattax.com/captcha/captcha_arg.jar','/home/salat/Downloads/images_captcha/screenshot.png'])

# note :
# -- 1 - missing PART A
# -- 2 - missing PART B
# -- 0 - success


