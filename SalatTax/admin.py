# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from import_export import resources
from .models import demo,Personal_Details,Address_Details,Return_Details,Original_return_Details,Employer_Details
from .models import Employer_Details,Bank_Details,Listed_Debentures_ST,Agriculture_Income
from .models import Form16,Income,Other_Income_Common,Other_Income_Rare,Exempt_Income,VI_Deductions
from .models import Shares_LT,Shares_ST,Equity_LT,Equity_ST,Debt_MF_LT,Debt_MF_ST,Listed_Debentures_LT
from .models import Other_Exempt_Income,House_Property_Details,Property_Owner_Details
from .models import salatclient,tds_tcs,tax_paid,refunds
from .models import SalatTaxRole,SalatTaxUser,ResetPassword,Client_fin_info1,Client_fin_info
from .models import State_Details,client_info,sign_up_otp,Donation_Details,computation,Bank_List,State_Code
from .models import temp_ITR_value,Investment_Details,Modules,Invoice_Details,Module_Subscription
from .models import accommodation_details,child_info,details_80c,home_loan,details_80d,allowances,ERI_user
from .models import salat_registration,Chapter,SubChapter,SubSection
from import_export.admin import ImportExportModelAdmin

class SalatTaxRoleAdmin(admin.ModelAdmin):
	list_display = ('rolename','permission','created','updated')
class SalatTaxUserAdmin(admin.ModelAdmin):
	list_display = ('user','phone','email','firstname','lastname','role','created','updated')
class ResetPasswordAdmin(admin.ModelAdmin):
	list_display = ('salattaxuser','email','url','status','timestamp')

class demoAdmin(admin.ModelAdmin):
	list_display = ('name',)

class Personal_DetailsAdmin(ImportExportModelAdmin):
	list_display = ('P_Id','Business_Id','Partner_Id','pan','pan_type','dob','pan_image','aadhar_no',
		'aadhar_image','name','email','mobile','father_name','gender','dependent_parent_age',
		'no_of_child','state_of_employment','disability','financial_goal','created_time','updated_time',
		'updated_by')
	# list_editable = ( 'P_Id', )


class Personal_DetailsAdmin_Import(resources.ModelResource):
    class Meta:
        model = Personal_Details
        fields = ('P_Id','Business_Id','Partner_Id','pan','pan_type','dob','pan_image','aadhar_no',
		'aadhar_image','name','email','mobile','father_name','gender','dependent_parent_age',
		'no_of_child','state_of_employment','disability','financial_goal','created_time','updated_time',
		'updated_by')

class Address_DetailsAdmin(admin.ModelAdmin):
	list_display = ('p_id','flat_door_block_no','name_of_premises_building','road_street_postoffice',
		'area_locality','pincode','town_city','state','country','current_place',
		'created_time','updated_time')

class Return_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_id','P_id','FY','AY','return_filing_category','return_filing_type',
		'created_time','updated_time')

class Original_return_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_id','FY','Receipt_no_original_return','Date_of_original_return_filing',
		'notice_no','notice_date','created_time','updated_time')

class Employer_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_id','FY','F16_no','Name_of_employer','TAN_of_employer','PAN_of_employer',
		'Address_of_employer','Employer_city','Employer_state','Employer_pin','Employer_category',
		'residential_status','start_date','end_date','monthly_basic_salary','monthly_hra',
		'created_time','updated_time')

class Bank_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_id','Bank_id','Default_bank','IFSC_code','Account_no','Account_type',
		'Bank_proof','created_time','updated_time')

class Form16Admin(admin.ModelAdmin):
	list_display = ('Salary_as_per_provision','Value_of_Perquisites','Profits_in_lieu_of_Salary',
		'Total_Gross_Salary','Gross_Salary_netof_Allowance','NSC','HRA_Allowed','Medical_Allowance',
		'Conveyance_Allowance_Allowed','LTA','LSE','HRA_Claimed','Conveyance_Allowance_Claimed',
		'Income_House_Property','Home_Loan_Interest','Income_Capital_Gains','Income_Other_Sources',
		'Gratuity','ELSS','EPF','PPF','VPF','LIC','NSC_Interest','Superannuation',
		'Home_Loan_Principal','FD','ULIP','Child_Edu_Fee','Sukanya_Samriddhi_Scheme','Stamp_Duty',
		'Form_80CCC','NPS_Employee','NPS_Employer','Form_80CCD','Form_80CCF','Form_80D_Self_Family',
		'Form_80D_Parents','Medical_Insurance','Form_80E','Form_80G','Form_80TTA',
		'Entertainment_Allowance','Profession_Tax','VIA_Deductions','Basic_Salary',
		'Dearness_Allowance','Personal_Allowance','Bonus','Performance_Pay','Special_Allowance',
		'Food_Allowance','Extra_Salary','RGESS','Exemption_Less_Allowance','Total_Less_Allowance',
		'Attire','Professional_Pursuit','Telephone_allowance','Other_Allowance',
		'Other_Any_Other_Income','cid','emp_type','Email','form16_path','password','P_D','S_C','C',
		'R','self_house','HRA_given','basic','DA','rent','HRA_given_value','stay','home_loan',
		'interest_paid','rent_earn','disability','disability_type','time_stamp','salat_tax',
		'tax_leakage','flag','created_time','updated_time')

class IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','company_name','Salary','Allowances_not_exempt','Value_of_perquisites',
		'Profits_in_lieu_of_salary','Entertainment_Allowance','LTA','Tax_paid_on_non_monetary_perquisite',
		'Form16_HRA','Other_allowances','Profession_Tax','created_time','updated_time')

class Other_Income_CommonAdmin(admin.ModelAdmin):
	list_display = ('R_Id','FY','Interest_Deposits','Interest_Savings','Commission','Other_Income',
		'Other_Interest','FD','created_time','updated_time')

class Other_Income_RareAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Taxable_Dividend','Rental_Income_other_than_house_property','Winnings',
		'Amount_borrowed_or_repaid_on_hundi','Dividend_over_10_lacs','Investment_income_NRI',
		'created_time','updated_time')

class Exempt_IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Tax_Free_Interest','LTCG_Equity','Tax_Free_Dividend',
		'Agriculture_Income_Id','Other_exempt_income_Id','created_time','updated_time')

class VI_DeductionsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','VI_80_C','VI_80_CCC','VI_80_CCD_1','VI_80_CCD_1B','VI_80_CCD_2',
		'VI_80_CCG','VI_80_D','VI_80_DD','VI_80_DDB','VI_80_E','VI_80_EE','VI_80_G','VI_80_GG',
		'VI_80_GGA','VI_80_GGC','VI_80_QQB','VI_80_RRB','VI_80_TTA','VI_80_U','created_time',
		'updated_time')

class Agriculture_IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Agriculture_Income_Id','Gross_Agriculture_Receipts',
		'Expenditure_Incurred_on_Agriculture','Unabsorbed_agriculture_loss','created_time','updated_time')

class Other_Exempt_IncomeAdmin(admin.ModelAdmin):
	list_display = ('R_Id','P_Id','FY','Other_Exempt_Income_Id','Other_Exempt_Income_Nature',
		'Other_Exempt_Income_Amount')

class Shares_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Shares_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Equity_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Equity_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Debt_MF_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Indexed_Cost_of_Acquisition','Expenditure')

class Debt_MF_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')
	
class Listed_Debentures_LTAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class Listed_Debentures_STAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Total_Sell_Value','Cost_of_Acquisition','Expenditure')

class House_Property_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Property_Id','Name_of_the_Premises_Building_Village','Road_Street_Post_Office',
		'Area_Locality','Town_City','State','Country','PIN_Code','Your_percentage_of_share_in_Property',
		'Type_of_Hosue_Property','Rent_Received','Rent_Cannot_be_Realised','Property_Tax','Name_of_Tenant',
		'PAN_of_Tenant','Interest_on_Home_loan')

class Property_Owner_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Property_Id','Name_of_Co_Owner','PAN_of_Co_owner','Percentage_Share_in_Property')

class salatclientAdmin(ImportExportModelAdmin):
	list_display = ('PAN','Is_salat_client','Is_ERI_reg','ERI_id','Reg_date')


class salatclientAdmin_Import(resources.ModelResource):
    class Meta:
        model = Personal_Details
        fields = ('PAN','Is_salat_client','Is_ERI_reg','ERI_id','Reg_date')


class tds_tcsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','part','Deductor_Collector_Name','TAN_PAN','section','no_of_transaction',
		'amount_paid','tax_deducted','tds_tcs_deposited','year')

class tax_paidAdmin(admin.ModelAdmin):
	list_display = ('R_Id','major_head','minor_head','total_tax','BSR_code','deposit_date','challan_serial_no')

class refundsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','Assessment_Year','mode','amount_of_refund','interest','payment_date')
	
class Client_fin_info1Admin(admin.ModelAdmin):
	list_display = ('company_name','company_address','employee_name','employee_address','employee_pan','ay',
		'period_from','period_to','amount_paid_credited','amount_of_tax_deducted','salary_as_per_provision',
		'value_of_perquisites','profits_in_lieu_of_salary','total_gross_salary','gross_salary_netof_allowance',
		'nsc','hra_allowed','medical_allowance','conveyance_allowance_allowed','lta','lse','hra_claimed',
		'conveyance_allowance_claimed','income_house_property','home_loan_interest','income_capital_gains',
		'income_other_sources','gratuity','elss','epf','ppf','vpf','lic','nsc_interest','superannuation',
		'home_loan_principal','fd','ulip','child_edu_fee','sukanya_samriddhi_scheme','stamp_duty',
		'number_80ccc','nps_employee','nps_employer','number_80ccd','number_80ccf','number_80d_self_family',
		'number_80d_parents','medical_insurance','number_80e','number_80g','number_80tta',
		'entertainment_allowance','profession_tax','via_deductions','basic_salary','dearness_allowance',
		'personal_allowance','bonus','performance_pay','special_allowance','food_allowance','extra_salary',
		'rgess','exemption_less_allowance','total_less_allowance','attire','professional_pursuit',
		'telephone_allowance','other_allowance','other_any_other_income','cid','emp_type','email',
		'form16_path','password','p_d','s_c','c','r','self_house','hra_given','basic','da','rent',
		'hra_given_value','stay','home_loan','interest_paid','rent_earn','disability','disability_type',
		'time_stamp','salat_tax','tax_leakage','flag','client_id')

class Client_fin_infoAdmin(admin.ModelAdmin):
	list_display = ('company_name','company_address','employee_name','employee_address','employee_pan',
		'ay','period_from','period_to','amount_paid_credited','amount_of_tax_deducted','salary_as_per_provision',
		'value_of_perquisites','profits_in_lieu_of_salary','total_gross_salary','gross_salary_netof_allowance',
		'nsc','hra_allowed','medical_allowance','conveyance_allowance_allowed','lta','lse','hra_claimed',
		'conveyance_allowance_claimed','income_house_property','home_loan_interest','income_capital_gains',
		'income_other_sources','gratuity','elss','epf','ppf','vpf','lic','nsc_interest','superannuation',
		'home_loan_principal','fd','ulip','child_edu_fee','sukanya_samriddhi_scheme','stamp_duty','number_80ccc',
		'nps_employee','nps_employer','number_80ccd','number_80ccf','number_80d_self_family','number_80d_parents',
		'medical_insurance','number_80e','number_80g','number_80tta','entertainment_allowance','profession_tax',
		'via_deductions','basic_salary','dearness_allowance','personal_allowance','bonus','performance_pay',
		'special_allowance','food_allowance','extra_salary','rgess','exemption_less_allowance',
		'total_less_allowance','attire','professional_pursuit','telephone_allowance','other_allowance',
		'other_any_other_income','cid','emp_type','email','form16_path','password','p_d','s_c','c','r',
		'self_house','hra_given','basic','da','rent','hra_given_value','stay','home_loan','interest_paid',
		'rent_earn','disability','disability_type','time_stamp','salat_tax','tax_leakage','flag')

class client_infoAdmin(admin.ModelAdmin):
	list_display = ('username','email','password','created_time')

class sign_up_otpAdmin(admin.ModelAdmin):
	list_display = ('username','mailid','otp','verified','created_time')

class Donation_DetailsAdmin(admin.ModelAdmin):
	list_display = ('R_Id','donation_id','donee_name','address','city_town','state_code','pin_code',
		'donee_pan','amt_of_donation')

class computationAdmin(admin.ModelAdmin):
	list_display = ('R_Id','salary_income','property_income','capital_gains','gross_total_income',
		'total_deduction','taxable_income','IT_normal_rates','IT_special_rates','rebate','surcharge',
		'add_education_cess','total_tax','interest_on_tax','taxpayable_refund')

class temp_ITR_valueAdmin(ImportExportModelAdmin):
	list_display = ('PAN','year','ITR','income_from_salary','no_of_hp','income_from_hp',
		'interest_payable_borrowed_capital','capital_gain','schedule_si_income','income_from_other_src',
		'business_or_profession','interest_gross','gross_total_income','deduction','c_80','d_80','tta_80',
		'ccd_1b_80','ccg_80','e_80','ee_80','g_80','gg_80','ccd_2_80','total_income','schedule_si_tax',
		'total_tax_interest','tax_paid','adv_tax_self_ass_tax','total_tcs','total_tds_ITR','total_tds_26AS')
	# list_editable = ( 'P_Id', )

class temp_ITR_valueAdmin_Import(resources.ModelResource):
    class Meta:
        model = temp_ITR_value
        fields = ('PAN','year','ITR','income_from_salary','no_of_hp','income_from_hp',
		'interest_payable_borrowed_capital','capital_gain','schedule_si_income','income_from_other_src',
		'business_or_profession','interest_gross','gross_total_income','deduction','c_80','d_80','tta_80',
		'ccd_1b_80','ccg_80','e_80','ee_80','g_80','gg_80','ccd_2_80','total_income','schedule_si_tax',
		'total_tax_interest','tax_paid','adv_tax_self_ass_tax','total_tcs','total_tds_ITR','total_tds_26AS')

class temp_ITR_valueAdmin_Import(resources.ModelResource):
    class Meta:
        model = temp_ITR_value
        fields = ('PAN','year','ITR','income_from_salary','no_of_hp','income_from_hp',
		'interest_payable_borrowed_capital','capital_gain','schedule_si_income','income_from_other_src',
		'business_or_profession','interest_gross','gross_total_income','deduction','c_80','d_80','tta_80',
		'ccd_1b_80','ccg_80','e_80','ee_80','g_80','gg_80','ccd_2_80','total_income','schedule_si_tax',
		'total_tax_interest','tax_paid','adv_tax_self_ass_tax','total_tcs','total_tds_ITR','total_tds_26AS')

class Investment_DetailsAdmin(admin.ModelAdmin):
	list_display = ('P_Id','year_of_first_share_sold')

class ModulesAdmin(admin.ModelAdmin):
	list_display = ('module_Id','module_name')

class Invoice_DetailsAdmin(admin.ModelAdmin):
	list_display = ('client_id','invoice_no','invoice_date','invoice_amount','CGST','SGST','IGST','discount',
		'offer_code','transaction_id','payment_status','mode_of_payment')

class Module_SubscriptionAdmin(admin.ModelAdmin):
	list_display = ('client_id','module_Id','invoice_no','year','module_cost','module_detail1')

class child_infoAdmin(admin.ModelAdmin):
	list_display = ('p_id','child_no','child_type','Child_dob','Child_age')

class accommodation_detailsAdmin(admin.ModelAdmin):
	list_display = ('p_id','FY','type_of_accommodation','current_place','annual_rent_paid')

class details_80cAdmin(admin.ModelAdmin):
	list_display = ('R_id','elss','epf','bank_fd','school_fee','ppf','life_insurance','ulip','home_loan')

class home_loanAdmin(admin.ModelAdmin):
	list_display = ('R_id','principal_amount','interest_amount','rate')

class details_80dAdmin(admin.ModelAdmin):
	list_display = ('R_id','mi_self','mi_parents','mc_self','mc_parents')

class allowancesAdmin(admin.ModelAdmin):
	list_display = ('R_id','entertainment_allowance','lta','hra','other_allowance','travel_expense_incurred')

class ERI_userAdmin(admin.ModelAdmin):
	list_display = ('eri_name','eri_pan','eri_dob','password','user_id','status','user_pan','date_time')

class salat_registrationAdmin(admin.ModelAdmin):
	list_display = ('pan','date_time','login','is_salat_client','otp_trigger','otp_submitted','itr_count',
		'downloaded_itr_count','processed_itr_count','downloaded_26as_count','processed_26as_count')

class ChapterAdmin(admin.ModelAdmin):
	list_display = ('title','slug','date_completed','completed')

class SubChapterAdmin(admin.ModelAdmin):
	list_display = ('chapter','title','slug','completed')

class SubSectionAdmin(admin.ModelAdmin):
	list_display = ('sub_chapter','title','slug','completed')



admin.site.register(SalatTaxRole, SalatTaxRoleAdmin)
admin.site.register(SalatTaxUser, SalatTaxUserAdmin)
admin.site.register(ResetPassword, ResetPasswordAdmin)
admin.site.register(demo, demoAdmin)
admin.site.register(Personal_Details, Personal_DetailsAdmin)
admin.site.register(Address_Details, Address_DetailsAdmin)
admin.site.register(Return_Details, Return_DetailsAdmin)
admin.site.register(Original_return_Details, Original_return_DetailsAdmin)
admin.site.register(Employer_Details, Employer_DetailsAdmin)
admin.site.register(Bank_Details, Bank_DetailsAdmin)
admin.site.register(Form16, Form16Admin)
admin.site.register(Income, IncomeAdmin)
admin.site.register(Other_Income_Common, Other_Income_CommonAdmin)
admin.site.register(Other_Income_Rare, Other_Income_RareAdmin)
admin.site.register(Exempt_Income, Exempt_IncomeAdmin)
admin.site.register(VI_Deductions, VI_DeductionsAdmin)
admin.site.register(Agriculture_Income, Agriculture_IncomeAdmin)
admin.site.register(Other_Exempt_Income, Other_Exempt_IncomeAdmin)
admin.site.register(Shares_LT, Shares_LTAdmin)
admin.site.register(Shares_ST, Shares_STAdmin)
admin.site.register(Equity_LT, Equity_LTAdmin)
admin.site.register(Equity_ST, Equity_STAdmin)
admin.site.register(Debt_MF_LT, Debt_MF_LTAdmin)
admin.site.register(Debt_MF_ST, Debt_MF_STAdmin)
admin.site.register(Listed_Debentures_LT, Listed_Debentures_LTAdmin)
admin.site.register(Listed_Debentures_ST, Listed_Debentures_STAdmin)
admin.site.register(House_Property_Details, House_Property_DetailsAdmin)
admin.site.register(Property_Owner_Details, Property_Owner_DetailsAdmin)
admin.site.register(salatclient, salatclientAdmin)
admin.site.register(tds_tcs, tds_tcsAdmin)
admin.site.register(tax_paid, tax_paidAdmin)
admin.site.register(refunds, refundsAdmin)
admin.site.register(Client_fin_info1, Client_fin_info1Admin)
admin.site.register(Client_fin_info, Client_fin_infoAdmin)
admin.site.register(client_info, client_infoAdmin)
admin.site.register(sign_up_otp, sign_up_otpAdmin)
admin.site.register(Donation_Details, Donation_DetailsAdmin)
admin.site.register(computation, computationAdmin)
admin.site.register(temp_ITR_value, temp_ITR_valueAdmin)
admin.site.register(Investment_Details, Investment_DetailsAdmin)
admin.site.register(Modules, ModulesAdmin)
admin.site.register(Invoice_Details, Invoice_DetailsAdmin)
admin.site.register(Module_Subscription, Module_SubscriptionAdmin)
admin.site.register(accommodation_details, accommodation_detailsAdmin)
admin.site.register(child_info, child_infoAdmin)
admin.site.register(details_80c, details_80cAdmin)
admin.site.register(home_loan, home_loanAdmin)
admin.site.register(details_80d, details_80dAdmin)
admin.site.register(allowances, allowancesAdmin)
admin.site.register(ERI_user, ERI_userAdmin)
admin.site.register(salat_registration, salat_registrationAdmin)
admin.site.register(Chapter, ChapterAdmin)
admin.site.register(SubChapter, SubChapterAdmin)
admin.site.register(SubSection, SubSectionAdmin)


class State_Details_Import(resources.ModelResource):
    class Meta:
        model = State_Details
        fields = ('id','nse_state', 'bse_state','cvl_state', 'ckyc_state', 'state')

class State_DetailsAdmin(ImportExportModelAdmin):
	list_display = ('id','nse_state', 'bse_state', 'cvl_state', 'ckyc_state', 'state')
	search_fields = ('nse_state', 'bse_state', 'cvl_state', 'ckyc_state', 'state')
	resource_class = State_Details_Import

class State_Code_Import(resources.ModelResource):
    class Meta:
        model = State_Code
        fields = ('id','code', 'state')

class State_CodeAdmin(ImportExportModelAdmin):
	list_display = ('id','code', 'state')
	search_fields = ('code', 'state')
	resource_class = State_Code_Import

class Bank_List_Import(resources.ModelResource):
    class Meta:
        model = Bank_List
        fields = ('id','nse_bank_code', 'bank_name')

class Bank_ListAdmin(ImportExportModelAdmin):
	list_display = ('id','nse_bank_code', 'bank_name')
	search_fields = ('nse_bank_code', 'bank_name')
	resource_class = Bank_List_Import


admin.site.register(State_Details, State_DetailsAdmin)
admin.site.register(Bank_List, Bank_ListAdmin)
admin.site.register(State_Code, State_CodeAdmin)

from .models import Visitor, Pageview
from .settings import TRACK_PAGEVIEWS
from datetime import timedelta

class VisitorAdmin(admin.ModelAdmin):
    date_hierarchy = 'start_time'

    list_display = ('session_key', 'user', 'start_time', 'session_over',
        'pretty_time_on_site', 'ip_address', 'user_agent','expiry_time','end_time')
    list_filter = ('user', 'ip_address')

    def session_over(self, obj):
        return obj.session_ended() or obj.session_expired()
    session_over.boolean = True

    def pretty_time_on_site(self, obj):
        if obj.time_on_site is not None:
            return timedelta(seconds=obj.time_on_site)
    pretty_time_on_site.short_description = 'Time on site'


admin.site.register(Visitor, VisitorAdmin)


class PageviewAdmin(admin.ModelAdmin):
    date_hierarchy = 'view_time'

    list_display = ('visitor','url','referer','query_string','method', 'view_time')


if TRACK_PAGEVIEWS:
    admin.site.register(Pageview, PageviewAdmin)