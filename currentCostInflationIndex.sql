-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: salatTax
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `salattax_app_currentcostinflationindex`
--

DROP TABLE IF EXISTS `salattax_app_currentcostinflationindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salattax_app_currentcostinflationindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `financialYear` varchar(250) DEFAULT NULL,
  `costInflationIndex` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salattax_app_currentcostinflationindex`
--

LOCK TABLES `salattax_app_currentcostinflationindex` WRITE;
/*!40000 ALTER TABLE `salattax_app_currentcostinflationindex` DISABLE KEYS */;
INSERT INTO `salattax_app_currentcostinflationindex` VALUES (1,'2001-2002',100),(2,'2002-2003',105),(3,'2003-2004',109),(4,'2004-2005',113),(5,'2005-2006',117),(6,'2006-2007',122),(7,'2007-2008',129),(8,'2008-2009',137),(9,'2009-2010',148),(10,'2010-2011',167),(11,'2011-2012',184),(12,'2012-2013',200),(13,'2013-2014',220),(14,'2014-2015',240),(15,'2015-2016',254),(16,'2016-2017',264),(17,'2017-2018',272),(18,'2018-2019',280);
/*!40000 ALTER TABLE `salattax_app_currentcostinflationindex` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-02 13:22:32
